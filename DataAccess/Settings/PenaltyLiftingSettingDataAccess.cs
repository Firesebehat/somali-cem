﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;

namespace CUSTOR.DataAccess
{
    public class PenaltyLiftingSettingDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],Description,[Parent],[IsParent],[DelayPenaltyPercentage],[UnderStatementPercentage],[LackOfStatementPenaltyPercentage],[HidingIncomePenaltyPercentage],[Date],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PenalityLifting]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PenalityLifting");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public PenaltyLiftingSetting GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            PenaltyLiftingSetting objPenalityLifting = new PenaltyLiftingSetting();
            string strGetRecord = @"SELECT [Id],[Parent],Description,[IsParent],[DelayPenaltyPercentage],[UnderStatementPercentage],[LackOfStatementPenaltyPercentage],[HidingIncomePenaltyPercentage],[Date],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PenalityLifting] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PenalityLifting");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objPenalityLifting.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objPenalityLifting.Description = "";
                    else
                        objPenalityLifting.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["Parent"].Equals(DBNull.Value))
                        objPenalityLifting.Parent = 0;
                    else
                        objPenalityLifting.Parent = (int)dTable.Rows[0]["Parent"];
                    if (dTable.Rows[0]["IsParent"].Equals(DBNull.Value))
                        objPenalityLifting.IsParent = false;
                    else
                        objPenalityLifting.IsParent = (bool)dTable.Rows[0]["IsParent"];
                    if (dTable.Rows[0]["DelayPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.DelayPenaltyPercentage = 0;
                    else
                        objPenalityLifting.DelayPenaltyPercentage = (int)dTable.Rows[0]["DelayPenaltyPercentage"];
                    if (dTable.Rows[0]["UnderStatementPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.UnderStatementPercentage = 0;
                    else
                        objPenalityLifting.UnderStatementPercentage = (int)dTable.Rows[0]["UnderStatementPercentage"];
                    if (dTable.Rows[0]["LackOfStatementPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.LackOfStatementPenaltyPercentage = 0;
                    else
                        objPenalityLifting.LackOfStatementPenaltyPercentage = (int)dTable.Rows[0]["LackOfStatementPenaltyPercentage"];
                    if (dTable.Rows[0]["HidingIncomePenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.HidingIncomePenaltyPercentage = 0;
                    else
                        objPenalityLifting.HidingIncomePenaltyPercentage = (int)dTable.Rows[0]["HidingIncomePenaltyPercentage"];
                    if (dTable.Rows[0]["Date"].Equals(DBNull.Value))
                        objPenalityLifting.Date = DateTime.MinValue;
                    else
                        objPenalityLifting.Date = (DateTime)dTable.Rows[0]["Date"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objPenalityLifting.IsActive = false;
                    else
                        objPenalityLifting.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objPenalityLifting.IsDeleted = false;
                    else
                        objPenalityLifting.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CenterId"].Equals(DBNull.Value))
                        objPenalityLifting.CenterId = 0;
                    else
                        objPenalityLifting.CenterId = (int)dTable.Rows[0]["CenterId"];
                    if (dTable.Rows[0]["CenterName"].Equals(DBNull.Value))
                        objPenalityLifting.CenterName = string.Empty;
                    else
                        objPenalityLifting.CenterName = (string)dTable.Rows[0]["CenterName"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedUserId = string.Empty;
                    else
                        objPenalityLifting.CreatedUserId = (string)dTable.Rows[0]["CreatedUserId"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedDate = DateTime.MinValue;
                    else
                        objPenalityLifting.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedBy = string.Empty;
                    else
                        objPenalityLifting.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedUserId = string.Empty;
                    else
                        objPenalityLifting.UpdatedUserId = (string)dTable.Rows[0]["UpdatedUserId"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedDate = DateTime.MinValue;
                    else
                        objPenalityLifting.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedBy = string.Empty;
                    else
                        objPenalityLifting.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objPenalityLifting;
        }

        public bool Insert(PenaltyLiftingSetting objPenalityLifting)
        {
            bool inserted = false;
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strInsert = @"INSERT INTO [dbo].[PenalityLifting]
                                            ([Description],[DescriptionSoundX],[DescriptionSortV],[Parent],[IsParent],[DelayPenaltyPercentage],[UnderStatementPercentage],[LackOfStatementPenaltyPercentage],[HidingIncomePenaltyPercentage],[Date],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],[CreatedBy]) OUTPUT INSERTED.ID
                                     VALUES (@Description,@DescriptionSoundX,@DescriptionSortV,@Parent,@IsParent,@DelayPenaltyPercentage,@UnderStatementPercentage,@LackOfStatementPenaltyPercentage,@HidingIncomePenaltyPercentage,@Date,@IsActive,@IsDeleted,@CenterId,@CenterName,@CreatedUserId,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Description", objPenalityLifting.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionSortV", objPenalityLifting.DescriptionSortV));
                command.Parameters.Add(new SqlParameter("@DescriptionSoundX", objPenalityLifting.DescriptionSoundX));
                command.Parameters.Add(new SqlParameter("@Parent", objPenalityLifting.Parent));
                command.Parameters.Add(new SqlParameter("@IsParent", objPenalityLifting.IsParent));
                command.Parameters.Add(new SqlParameter("@DelayPenaltyPercentage", objPenalityLifting.DelayPenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@UnderStatementPercentage", objPenalityLifting.UnderStatementPercentage));
                command.Parameters.Add(new SqlParameter("@LackOfStatementPenaltyPercentage", objPenalityLifting.LackOfStatementPenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@HidingIncomePenaltyPercentage", objPenalityLifting.HidingIncomePenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@Date", objPenalityLifting.Date));
                command.Parameters.Add(new SqlParameter("@IsActive", objPenalityLifting.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objPenalityLifting.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CenterId", objPenalityLifting.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objPenalityLifting.CenterName));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objPenalityLifting.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objPenalityLifting.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objPenalityLifting.CreatedBy));

                connection.Open();
                objPenalityLifting.Id = (int)command.ExecuteScalar();
                if(objPenalityLifting.Id > 0)
                {
                    inserted =  true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return inserted;
        }

        public bool Update(PenaltyLiftingSetting objPenalityLifting)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            bool updated = false;
            string strUpdate = @"UPDATE [dbo].[PenalityLifting] SET [Description]=@Description, [DescriptionSoundX]=@DescriptionSoundX,[DescriptionSortV]=@DescriptionSortV ,[Parent]=@Parent,    [IsParent]=@IsParent,    [DelayPenaltyPercentage]=@DelayPenaltyPercentage,    [UnderStatementPercentage]=@UnderStatementPercentage,    [LackOfStatementPenaltyPercentage]=@LackOfStatementPenaltyPercentage,    [HidingIncomePenaltyPercentage]=@HidingIncomePenaltyPercentage,    [Date]=@Date,    [IsActive]=@IsActive,    [IsDeleted]=@IsDeleted,    [CenterId]=@CenterId,    [CenterName]=@CenterName, [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objPenalityLifting.Id));
                command.Parameters.Add(new SqlParameter("@Description", objPenalityLifting.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionSortV", objPenalityLifting.DescriptionSortV));
                command.Parameters.Add(new SqlParameter("@DescriptionSoundX", objPenalityLifting.DescriptionSoundX));
                command.Parameters.Add(new SqlParameter("@Parent", objPenalityLifting.Parent));
                command.Parameters.Add(new SqlParameter("@IsParent", objPenalityLifting.IsParent));
                command.Parameters.Add(new SqlParameter("@DelayPenaltyPercentage", objPenalityLifting.DelayPenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@UnderStatementPercentage", objPenalityLifting.UnderStatementPercentage));
                command.Parameters.Add(new SqlParameter("@LackOfStatementPenaltyPercentage", objPenalityLifting.LackOfStatementPenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@HidingIncomePenaltyPercentage", objPenalityLifting.HidingIncomePenaltyPercentage));
                command.Parameters.Add(new SqlParameter("@Date", objPenalityLifting.Date));
                command.Parameters.Add(new SqlParameter("@IsActive", objPenalityLifting.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objPenalityLifting.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CenterId", objPenalityLifting.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objPenalityLifting.CenterName));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objPenalityLifting.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objPenalityLifting.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objPenalityLifting.UpdatedBy));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                if(_rowsAffected > 0)
                {
                    updated = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return updated;
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[PenalityLifting] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<PenaltyLiftingSetting> GetList()
        {
            List<PenaltyLiftingSetting> RecordsList = new List<PenaltyLiftingSetting>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Description],[Parent],[IsParent],[DelayPenaltyPercentage],[UnderStatementPercentage],[LackOfStatementPenaltyPercentage],[HidingIncomePenaltyPercentage],[Date],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PenalityLifting]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PenaltyLiftingSetting objPenalityLifting = new PenaltyLiftingSetting();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPenalityLifting.Id = 0;
                    else
                        objPenalityLifting.Id = (int)dr["Id"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objPenalityLifting.Description = "";
                    else
                        objPenalityLifting.Description = (string)dr["Description"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objPenalityLifting.Parent = 0;
                    else
                        objPenalityLifting.Parent = (int)dr["Parent"];
                    if (dr["IsParent"].Equals(DBNull.Value))
                        objPenalityLifting.IsParent = false;
                    else
                        objPenalityLifting.IsParent = (bool)dr["IsParent"];
                    if (dr["DelayPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.DelayPenaltyPercentage = 0;
                    else
                        objPenalityLifting.DelayPenaltyPercentage = (int)dr["DelayPenaltyPercentage"];
                    if (dr["UnderStatementPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.UnderStatementPercentage = 0;
                    else
                        objPenalityLifting.UnderStatementPercentage = (int)dr["UnderStatementPercentage"];
                    if (dr["LackOfStatementPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.LackOfStatementPenaltyPercentage = 0;
                    else
                        objPenalityLifting.LackOfStatementPenaltyPercentage = (int)dr["LackOfStatementPenaltyPercentage"];
                    if (dr["HidingIncomePenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.HidingIncomePenaltyPercentage = 0;
                    else
                        objPenalityLifting.HidingIncomePenaltyPercentage = (int)dr["HidingIncomePenaltyPercentage"];
                    if (dr["Date"].Equals(DBNull.Value))
                        objPenalityLifting.Date = DateTime.MinValue;
                    else
                        objPenalityLifting.Date = (DateTime)dr["Date"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objPenalityLifting.IsActive = false;
                    else
                        objPenalityLifting.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objPenalityLifting.IsDeleted = false;
                    else
                        objPenalityLifting.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CenterId"].Equals(DBNull.Value))
                        objPenalityLifting.CenterId = 0;
                    else
                        objPenalityLifting.CenterId = (int)dr["CenterId"];
                    if (dr["CenterName"].Equals(DBNull.Value))
                        objPenalityLifting.CenterName = string.Empty;
                    else
                        objPenalityLifting.CenterName = (string)dr["CenterName"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedUserId = string.Empty;
                    else
                        objPenalityLifting.CreatedUserId = (string)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedDate = DateTime.MinValue;
                    else
                        objPenalityLifting.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objPenalityLifting.CreatedBy = string.Empty;
                    else
                        objPenalityLifting.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedUserId = string.Empty;
                    else
                        objPenalityLifting.UpdatedUserId = (string)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedDate = DateTime.MinValue;
                    else
                        objPenalityLifting.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objPenalityLifting.UpdatedBy = string.Empty;
                    else
                        objPenalityLifting.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objPenalityLifting);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<PenaltyLiftingSettingView> GetPenaltyRightOffParentList()
        {
            List<PenaltyLiftingSettingView> RecordsList = new List<PenaltyLiftingSettingView>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = @"SELECT [Id],[Description],[Parent],[IsParent],Date FROM [dbo].[PenalityLifting] where isParent=1 ORDER BY  [Id] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PenaltyLiftingSettingView objPenalityLifting = new PenaltyLiftingSettingView();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPenalityLifting.Id = 0;
                    else
                        objPenalityLifting.Id = (int)dr["Id"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objPenalityLifting.Description = "";
                    else
                        objPenalityLifting.Description = (string)dr["Description"];
                    if (dr["Date"].Equals(DBNull.Value))
                        objPenalityLifting.Date = "";
                    else
                        objPenalityLifting.Date = EthiopicDateTime.GetEthiopianDate((DateTime)dr["Date"]);
                    RecordsList.Add(objPenalityLifting);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool GetPenaltyRightOffExistance(string descriptionSoundX)
        {
            bool existed = false;
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = @"SELECT * FROM [dbo].[PenalityLifting] where DescriptionSoundX =@descriptionSoundX ORDER BY  [Id] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@descriptionSoundX", descriptionSoundX));
                dr = command.ExecuteReader();
                if(dr.HasRows)
                {
                    existed = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return existed;
        }

        public List<PenaltyLiftingSetting> GetPenaltyRightOffParentListByParent(int parentPenaltyRightOff)
        {
            List<PenaltyLiftingSetting> RecordsList = new List<PenaltyLiftingSetting>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = @"SELECT [Id],[Description],[DelayPenaltyPercentage],[UnderStatementPercentage],[LackOfStatementPenaltyPercentage],[HidingIncomePenaltyPercentage] FROM [dbo].[PenalityLifting] where parent=@parent ORDER BY  [Id] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@parent", parentPenaltyRightOff));
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PenaltyLiftingSetting objPenalityLifting = new PenaltyLiftingSetting();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPenalityLifting.Id = 0;
                    else
                        objPenalityLifting.Id = (int)dr["Id"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objPenalityLifting.Description = "";
                    else
                        objPenalityLifting.Description = (string)dr["Description"];
                    
                    if (dr["DelayPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.DelayPenaltyPercentage = 0;
                    else
                        objPenalityLifting.DelayPenaltyPercentage = (int)dr["DelayPenaltyPercentage"];
                    if (dr["UnderStatementPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.UnderStatementPercentage = 0;
                    else
                        objPenalityLifting.UnderStatementPercentage = (int)dr["UnderStatementPercentage"];
                    if (dr["LackOfStatementPenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.LackOfStatementPenaltyPercentage = 0;
                    else
                        objPenalityLifting.LackOfStatementPenaltyPercentage = (int)dr["LackOfStatementPenaltyPercentage"];
                    if (dr["HidingIncomePenaltyPercentage"].Equals(DBNull.Value))
                        objPenalityLifting.HidingIncomePenaltyPercentage = 0;
                    else
                        objPenalityLifting.HidingIncomePenaltyPercentage = (int)dr["HidingIncomePenaltyPercentage"];
                    
                    RecordsList.Add(objPenalityLifting);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[PenalityLifting] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("PenalityLifting::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}
