﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class AdminSettingsDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[SiteId],[Key],[Value] FROM [dbo].[AdminSettings] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AdminSettings");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public AdminSettingss GetRecord(string Key,int siteId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            AdminSettingss objAdminSettings = new AdminSettingss();
            string strGetRecord = @"SELECT [Id],[SiteId],[Key],[Value] FROM [dbo].[AdminSettings] where [key] = @key and [SiteId] = @siteId ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@key", Key);
            command.Parameters.AddWithValue("@siteId", siteId);
            DataTable dTable = new DataTable("AdminSettings");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAdminSettings.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["SiteId"].Equals(DBNull.Value))
                        objAdminSettings.SiteId = 0;
                    else
                        objAdminSettings.SiteId = (int)dTable.Rows[0]["SiteId"];
                    if (dTable.Rows[0]["Key"].Equals(DBNull.Value))
                        objAdminSettings.Key = string.Empty;
                    else
                        objAdminSettings.Key = (string)dTable.Rows[0]["Key"];
                    if (dTable.Rows[0]["Value"].Equals(DBNull.Value))
                        objAdminSettings.Value = string.Empty;
                    else
                        objAdminSettings.Value = (string)dTable.Rows[0]["Value"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAdminSettings;
        }

        public bool Insert(AdminSettingss objAdminSettings, SqlCommand command)
        {

            string strInsert = @"declare @x as table(id int); INSERT INTO [dbo].[AdminSettings]
                                            ([SiteId],[Key],[Value]) output inserted.Id into @x
                                     VALUES    (@SiteId,@Key,@Value) select * from @x";

            command.CommandText = strInsert;
            command.Parameters.Clear();
            try
            {
                command.Parameters.Add(new SqlParameter("@SiteId", objAdminSettings.SiteId));
                command.Parameters.Add(new SqlParameter("@Key", objAdminSettings.Key));
                command.Parameters.Add(new SqlParameter("@Value", objAdminSettings.Value));
                //command.Parameters.Add(new SqlParameter("@Id", objAdminSettings.Id));


                objAdminSettings.Id = (int)command.ExecuteScalar();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                
            }
        }

        public bool Update(AdminSettingss objAdminSettings, AdminSettingHistory settingHistory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[AdminSettings] SET     [SiteId]=@SiteId,    [Key]=@Key,    [Value]=@Value where [Id] = @Id ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;
            connection.Open();

            SqlTransaction tran = connection.BeginTransaction();
            command.Transaction = tran;
            try
            {

                if (!Exists(objAdminSettings, objAdminSettings.SiteId, command))
                {
                    Insert(objAdminSettings, command);
                }
                else//update
                {
                    command.CommandText = strUpdate;
                    command.Parameters.Clear();
                    command.Parameters.Add(new SqlParameter("@Id", objAdminSettings.Id));
                    command.Parameters.Add(new SqlParameter("@SiteId", objAdminSettings.SiteId));
                    command.Parameters.Add(new SqlParameter("@Key", objAdminSettings.Key));
                    command.Parameters.Add(new SqlParameter("@Value", objAdminSettings.Value));
                    int rawaffected = command.ExecuteNonQuery();
                }
                AdminSettingHistoryDataAccess dataAccess = new AdminSettingHistoryDataAccess();
                settingHistory.SettingId = objAdminSettings.Id;
                dataAccess.Insert(settingHistory,command);
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw new Exception("AdminSettings::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                tran.Dispose();
            }
        }

        public bool Delete(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[AdminSettings] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<AdminSettingss> GetList(int siteId)
        {
            List<AdminSettingss> RecordsList = new List<AdminSettingss>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[SiteId],[Key],[Value] FROM [dbo].[AdminSettings] where SiteId = @SiteId ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@SiteId", siteId);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    AdminSettingss objAdminSettings = new AdminSettingss();
                    if (dr["Id"].Equals(DBNull.Value))
                        objAdminSettings.Id = 0;
                    else
                        objAdminSettings.Id = (int)dr["Id"];
                    if (dr["SiteId"].Equals(DBNull.Value))
                        objAdminSettings.SiteId = 0;
                    else
                        objAdminSettings.SiteId = (int)dr["SiteId"];
                    if (dr["Key"].Equals(DBNull.Value))
                        objAdminSettings.Key = string.Empty;
                    else
                        objAdminSettings.Key = (string)dr["Key"];
                    if (dr["Value"].Equals(DBNull.Value))
                        objAdminSettings.Value = string.Empty;
                    else
                        objAdminSettings.Value = (string)dr["Value"];
                    RecordsList.Add(objAdminSettings);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(AdminSettingss  adminSettingss, int siteId, SqlCommand command)
        {

            string strExists = @"SELECT [Id]  FROM [dbo].[AdminSettings] where [Key] = @key and SiteId = @SiteId";

            command.CommandText = strExists;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@key", adminSettingss.Key);
            command.Parameters.AddWithValue("@siteId", siteId);
            try
            {

                var result = command.ExecuteScalar();
                if (result != DBNull.Value && result != null)
                    adminSettingss.Id = (int)result;
                return adminSettingss.Id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettings::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                
            }
        }


    }
}