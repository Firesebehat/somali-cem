﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using CUSTOR.Domain;
using CUSTORCommon;
using CUSTORCommon.Ethiopic;
using CUSTORCommon.Enumerations;

namespace CUSTOR.DataAccess
{
    public class SettingsDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [id],[AppId],[key],[value],[picture],[CenterId],
                                        [CenterName],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],
                                        [UpdatedDate],[UpdatedBy] FROM [dbo].Settings]  ORDER BY  [id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AppSettings");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public List<int> GetSettingsByCategoryandDateFrom(int category, DateTime datefrom)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<int> SettingsIdCollection = new List<int>();
            string strGetRecord = @"SELECT  Id,[key],[value],DateFrom,DateTo,Category FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom=@datefrom order by Id desc";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@datefrom", datefrom));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SettingsIdCollection.Add((int)dr["Id"]);
                    }
                }
                else
                {
                    SettingsIdCollection = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }

            return SettingsIdCollection;
        }

        public List<SettingsAppliedYears> GetYearsToWhichSettingsApply(int category)
        {
            List<SettingsAppliedYears> RecordsList = new List<SettingsAppliedYears>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT  CreatedDate FROM [dbo].[Settings]  WHERE [category]=@category 
                                        Group by CreatedDate order by CreatedDate desc ";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@category", category));
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    SettingsAppliedYears settingYears = new SettingsAppliedYears();
                    if (!dr["CreatedDate"].Equals(DBNull.Value)) {
                        DateTime date = (DateTime)dr["CreatedDate"];
                        settingYears.EthiopicYear = EthiopicDateTime.GetEthiopicYear(date.Day,date.Month, date.Year);
                        settingYears.SettingStartsFrom = date;
                    }
                    RecordsList.Add(settingYears);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<int> GetLatestSettingsId(int category, int numberKeys)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<int> SettingsIdCollection = new List<int>();
            string strGetRecord = @"SELECT top "+ numberKeys + @" Id,[key],[value],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category order by Id desc";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SettingsIdCollection.Add((int)dr["Id"]);
                    }
                }
                else
                {
                    SettingsIdCollection = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }

            return SettingsIdCollection;
        }

        public List<AgriculutreSettingsView> GetSettingsByAgricultureCategory(int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<AgriculutreSettingsView> objAgricultureSettingsList = new List<AgriculutreSettingsView>();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category order by CreatedDate asc";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            int count = 0;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                AgriculutreSettingsView objAgricultureSettings = new AgriculutreSettingsView();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (count != 0 && ((count % 3) == 0))
                        {
                            objAgricultureSettingsList.Add(objAgricultureSettings);
                            objAgricultureSettings = new AgriculutreSettingsView();
                            if ((string)dr["Key"] == "AgricultureCategoryCode")
                            {
                                objAgricultureSettings.AgricultureCategoryCode = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "AgricultureTaxDelayFine")
                            {
                                objAgricultureSettings.AgricultureTaxDelayFine = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "AgricultureTaxArchiveFine")
                            {
                                objAgricultureSettings.AgricultureTaxArchiveFine = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objAgricultureSettings.AgricultureDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objAgricultureSettings.AgricultureDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                        else
                        {
                            if ((string)dr["Key"] == "AgricultureCategoryCode")
                            {
                                objAgricultureSettings.AgricultureCategoryCode = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "AgricultureTaxDelayFine")
                            {
                                objAgricultureSettings.AgricultureTaxDelayFine = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "AgricultureTaxArchiveFine")
                            {
                                objAgricultureSettings.AgricultureTaxArchiveFine = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objAgricultureSettings.AgricultureDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objAgricultureSettings.AgricultureDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                    }
                    objAgricultureSettingsList.Add(objAgricultureSettings);
                    for (int index = 0; index < objAgricultureSettingsList.Count; index++)
                    {
                        objAgricultureSettingsList[index].AgricultureDateFromEthiopic = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objAgricultureSettingsList[index].AgricultureDateFromEthiopic));
                    }
                }
                else
                {
                    objAgricultureSettingsList = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            
            return objAgricultureSettingsList;
        }

        public List<RentalSettingsView> GetSettingsByRentalCategory(int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<RentalSettingsView> objRentalSettingsList = new List<RentalSettingsView>();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            RentalSettingsView objRentalSettings = new RentalSettingsView();
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                int count = 0;
                string exemption = "";
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (count != 0 && ((count % 1) == 0))
                        {
                            objRentalSettingsList.Add(objRentalSettings);
                            objRentalSettings = new RentalSettingsView();
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentA")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.A));
                            }
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentB")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.B));

                            }
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentC")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.C));
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objRentalSettings.DateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objRentalSettings.DateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                        else
                        {
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentA")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.A));
                            }
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentB")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.B));

                            }
                            if ((string)dr["Key"] == "RentalTaxFromIncomePercentC")
                            {
                                objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                                objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.C));
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objRentalSettings.DateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objRentalSettings.DateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                    }
                    objRentalSettingsList.Add(objRentalSettings);
                    for (int index = 0; index < objRentalSettingsList.Count; index++)
                    {
                        objRentalSettingsList[index].DateFromEthiopic = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objRentalSettingsList[index].DateFromEthiopic));
                    }
                }
                else
                {
                    objRentalSettingsList = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            
            return objRentalSettingsList;
        }

        public List<PenalitySettingsView> GetSettingsByPenalityCategory(int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<PenalitySettingsView> objPenalitySettingsViewList = new List<PenalitySettingsView>();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            int count = 0;
            PenalitySettingsView objPenalitySettings = new PenalitySettingsView();
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (count != 0 && ((count % 9) == 0))
                        {
                            objPenalitySettingsViewList.Add(objPenalitySettings);
                            objPenalitySettings = new PenalitySettingsView();
                            if ((string)dr["Key"] == "OneMonthDelayedDeclaration")
                            {
                                objPenalitySettings.OneMonthDelayedDeclaration = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MoreThanOneMonthDelayedDeclaration")
                            {
                                objPenalitySettings.MoreThanOneMonthDelayedDeclaration = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "NotDeclaringYearlyIncome")
                            {
                                objPenalitySettings.NotDeclaringYearlyIncome = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "OneMonthUnderStatement")
                            {
                                objPenalitySettings.OneMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "TwoMonthUnderStatement")
                            {
                                objPenalitySettings.TwoMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "ThreeMonthUnderStatement")
                            {
                                objPenalitySettings.ThreeMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "NoFinancialRecord")
                            {
                                objPenalitySettings.NoFinancialRecord = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "BankInterestRate")
                            {
                                objPenalitySettings.BankInterestRate = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "RevenueInterestRate")
                            {
                                objPenalitySettings.RevenueInterestRate = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objPenalitySettings.PenalityDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objPenalitySettings.PenalityDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                        else
                        {
                            if ((string)dr["Key"] == "OneMonthDelayedDeclaration")
                            {
                                objPenalitySettings.OneMonthDelayedDeclaration = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MoreThanOneMonthDelayedDeclaration")
                            {
                                objPenalitySettings.MoreThanOneMonthDelayedDeclaration = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "NotDeclaringYearlyIncome")
                            {
                                objPenalitySettings.NotDeclaringYearlyIncome = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "OneMonthUnderStatement")
                            {
                                objPenalitySettings.OneMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "TwoMonthUnderStatement")
                            {
                                objPenalitySettings.TwoMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "ThreeMonthUnderStatement")
                            {
                                objPenalitySettings.ThreeMonthUnderStatement = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "NoFinancialRecord")
                            {
                                objPenalitySettings.NoFinancialRecord = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "BankInterestRate")
                            {
                                objPenalitySettings.BankInterestRate = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "RevenueInterestRate")
                            {
                                objPenalitySettings.RevenueInterestRate = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objPenalitySettings.PenalityDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objPenalitySettings.PenalityDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                    }
                    objPenalitySettingsViewList.Add(objPenalitySettings);
                    for (int index = 0; index < objPenalitySettingsViewList.Count; index++)
                    {
                        objPenalitySettingsViewList[index].PenalityDateFromEthiopic = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objPenalitySettingsViewList[index].PenalityDateFromEthiopic));
                    }
                }
                else
                {
                    objPenalitySettingsViewList = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            
            return objPenalitySettingsViewList;
        }

        public List<SalarySettingsView> GetSettingsBySalaryCategory(int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<SalarySettingsView> objSalarySettingsList = new List<SalarySettingsView>();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            int count = 0;
            SalarySettingsView objSalarySettings = new SalarySettingsView();
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (count != 0 && ((count % 5) == 0))
                        {
                            objSalarySettingsList.Add(objSalarySettings);
                            objSalarySettings = new SalarySettingsView();
                            if ((string)dr["Key"] == "TransportAllowancePercent")
                            {
                                objSalarySettings.TransportAllowancePercent = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "TransportAllowanceInBirr")
                            {
                                objSalarySettings.TransportAllowanceInBirr = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "DelegationAllowance")
                            {
                                objSalarySettings.DelegationAllowance = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "ProvidentFund")
                            {
                                objSalarySettings.ProvidentFund = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MonthlyCostSharing")
                            {
                                objSalarySettings.MonthlyCostSharing = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objSalarySettings.SalarySettingDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objSalarySettings.SalarySettingDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                        else
                        {
                            if ((string)dr["Key"] == "TransportAllowancePercent")
                            {
                                objSalarySettings.TransportAllowancePercent = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "TransportAllowanceInBirr")
                            {
                                objSalarySettings.TransportAllowanceInBirr = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "DelegationAllowance")
                            {
                                objSalarySettings.DelegationAllowance = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "ProvidentFund")
                            {
                                objSalarySettings.ProvidentFund = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MonthlyCostSharing")
                            {
                                objSalarySettings.MonthlyCostSharing = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objSalarySettings.SalarySettingDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objSalarySettings.SalarySettingDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                    }
                    objSalarySettingsList.Add(objSalarySettings);
                    for (int index = 0; index < objSalarySettingsList.Count; index++)
                    {
                        objSalarySettingsList[index].SalarySettingDateFromEthiopic = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objSalarySettingsList[index].SalarySettingDateFromEthiopic));
                    }
                }
                else
                {
                    objSalarySettingsList = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            

            return objSalarySettingsList;
        }

        public List<LandUseSettingsView> GetSettingsByLandUseCategory(int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            List<LandUseSettingsView> objLandUseSettingsList = new List<LandUseSettingsView>();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            int count = 0;
            LandUseSettingsView objLandUseSettings = new LandUseSettingsView();
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (count != 0 && ((count % 3) == 0))
                        {
                            objLandUseSettingsList.Add(objLandUseSettings);
                            objLandUseSettings = new LandUseSettingsView();
                            if ((string)dr["Key"] == "NonCooperativeAssociation")
                            {
                                objLandUseSettings.NonCooperativeAssociation = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "CooperativeAssociation")
                            {
                                objLandUseSettings.CooperativeAssociation = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MonthlyDelayedDeclaration")
                            {
                                objLandUseSettings.MonthlyDelayedDeclaration = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objLandUseSettings.LandUseDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objLandUseSettings.LandUseDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                        else
                        {
                            if ((string)dr["Key"] == "NonCooperativeAssociation")
                            {
                                objLandUseSettings.NonCooperativeAssociation = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "CooperativeAssociation")
                            {
                                objLandUseSettings.CooperativeAssociation = (string)dr["Value"];
                            }
                            if ((string)dr["Key"] == "MonthlyDelayedDeclaration")
                            {
                                objLandUseSettings.MonthlyDelayedDeclaration = (string)dr["Value"];
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objLandUseSettings.LandUseDateFrom = dr["DateFrom"].ToString();
                            }
                            if (!dr["DateFrom"].Equals(DBNull.Value))
                            {
                                objLandUseSettings.LandUseDateFromEthiopic = dr["DateFrom"].ToString();
                            }
                            count++;
                        }
                    }
                    objLandUseSettingsList.Add(objLandUseSettings);
                    for (int index = 0; index < objLandUseSettingsList.Count; index++)
                    {
                        objLandUseSettingsList[index].LandUseDateFromEthiopic = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objLandUseSettingsList[index].LandUseDateFromEthiopic));
                    }
                }
                else
                {
                    objLandUseSettingsList = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objLandUseSettingsList;
        }

        public AgriculutreSettingsView GetAgriculutreSettingsInfo(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            AgriculutreSettingsView objAgricultureSettings = new AgriculutreSettingsView();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((string)dr["Key"] == "AgricultureCategoryCode")
                        {
                            objAgricultureSettings.AgricultureCategoryCode = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "AgricultureTaxDelayFine")
                        {
                            objAgricultureSettings.AgricultureTaxDelayFine = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "AgricultureTaxArchiveFine")
                        {
                            objAgricultureSettings.AgricultureTaxArchiveFine = (string)dr["Value"];
                        }
                        if (!dr["DateFrom"].Equals(DBNull.Value))
                        {
                            objAgricultureSettings.AgricultureDateFrom = dr["DateFrom"].ToString();
                        }
                        if (!dr["DateTo"].Equals(DBNull.Value))
                        {
                            objAgricultureSettings.AgricultureDateTo = ((DateTime)dr["DateTo"]).ToShortDateString();
                        }
                    }
                }
                else
                {
                    objAgricultureSettings = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objAgricultureSettings;
        }

        public RentalSettingsView GetRentalSettingsInfo(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            RentalSettingsView objRentalSettings = new RentalSettingsView();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((string)dr["Key"] == "RentalTaxFromIncomePercentA")
                        {
                            objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                            objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish,Convert.ToInt16(Enums.eGrade.A));
                        }
                        if ((string)dr["Key"] == "RentalTaxFromIncomePercentB")
                        {
                            objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                            objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.B));

                        }
                        if ((string)dr["Key"] == "RentalTaxFromIncomePercentC")
                        {
                            objRentalSettings.RentalTaxFromIncomePercent = (string)dr["Value"];
                            objRentalSettings.Grade = EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, Convert.ToInt16(Enums.eGrade.C));
                        }

                        if (!dr["DateFrom"].Equals(DBNull.Value))
                        {
                            objRentalSettings.DateFrom = dr["DateFrom"].ToString();
                        }
                        if (!dr["DateTo"].Equals(DBNull.Value))
                        {
                            objRentalSettings.DateTo = ((DateTime)dr["DateTo"]).ToShortDateString();
                        }
                    }
                }
                else
                {
                    objRentalSettings = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objRentalSettings;
        }

        public PenalitySettingsView GetPenalitySettingsInfo(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            PenalitySettingsView objPenalitySettings = new PenalitySettingsView();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((string)dr["Key"] == "OneMonthDelayedDeclaration")
                        {
                            objPenalitySettings.OneMonthDelayedDeclaration = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "MoreThanOneMonthDelayedDeclaration")
                        {
                            objPenalitySettings.MoreThanOneMonthDelayedDeclaration = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "NotDeclaringYearlyIncome")
                        {
                            objPenalitySettings.NotDeclaringYearlyIncome = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "OneMonthUnderStatement")
                        {
                            objPenalitySettings.OneMonthUnderStatement = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "TwoMonthUnderStatement")
                        {
                            objPenalitySettings.TwoMonthUnderStatement = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "ThreeMonthUnderStatement")
                        {
                            objPenalitySettings.ThreeMonthUnderStatement = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "NoFinancialRecord")
                        {
                            objPenalitySettings.NoFinancialRecord = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "BankInterestRate")
                        {
                            objPenalitySettings.BankInterestRate = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "RevenueInterestRate")
                        {
                            objPenalitySettings.RevenueInterestRate = (string)dr["Value"];
                        }
                        if (!dr["DateFrom"].Equals(DBNull.Value))
                        {
                            objPenalitySettings.PenalityDateFrom = dr["DateFrom"].ToString();
                        }

                        if (!dr["DateTo"].Equals(DBNull.Value))
                        {
                            objPenalitySettings.PenalityDateTo = ((DateTime)dr["DateTo"]).ToShortDateString();
                        }
                    }
                }
                else
                {
                    objPenalitySettings = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objPenalitySettings;
        }

        public SalarySettingsView GetSalarySettingsInfo(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SalarySettingsView objSalarySettings = new SalarySettingsView();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((string)dr["Key"] == "TransportAllowancePercent")
                        {
                            objSalarySettings.TransportAllowancePercent = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "TransportAllowanceInBirr")
                        {
                            objSalarySettings.TransportAllowanceInBirr = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "DelegationAllowance")
                        {
                            objSalarySettings.DelegationAllowance = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "ProvidentFund")
                        {
                            objSalarySettings.ProvidentFund = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "MonthlyCostSharing")
                        {
                            objSalarySettings.MonthlyCostSharing = (string)dr["Value"];
                        }
                        if (!dr["DateFrom"].Equals(DBNull.Value))
                        {
                            objSalarySettings.SalarySettingDateFrom = dr["DateFrom"].ToString();
                        }
                        if (!dr["DateTo"].Equals(DBNull.Value))
                        {
                            objSalarySettings.SalarySettingDateTo = ((DateTime)dr["DateTo"]).ToShortDateString();
                        }
                    }
                }
                else
                {
                    objSalarySettings = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objSalarySettings;
        }

        public bool DeleteSettings(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SalarySettingsView objSalarySettings = new SalarySettingsView();
            string strGetRecord = @"Delete FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Connection = connection;
            bool deleted = false;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                int rowsAffected = command.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    deleted = true;
                }
                return deleted;
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public LandUseSettingsView GetLandUserSettingsInfo(DateTime startingDate, int category)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            LandUseSettingsView objLandUseSettings = new LandUseSettingsView();
            string strGetRecord = @"SELECT [id],[AppId],[key],[value],[picture],DateFrom,DateTo FROM [dbo].[Settings] 
                                    WHERE [category]=@category and DateFrom = @startingDate";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataReader dr = null;
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@category", category));
                command.Parameters.Add(new SqlParameter("@startingDate", startingDate));
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if ((string)dr["Key"] == "NonCooperativeAssociation")
                        {
                            objLandUseSettings.NonCooperativeAssociation = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "CooperativeAssociation")
                        {
                            objLandUseSettings.CooperativeAssociation = (string)dr["Value"];
                        }
                        if ((string)dr["Key"] == "MonthlyDelayedDeclaration")
                        {
                            objLandUseSettings.MonthlyDelayedDeclaration = (string)dr["Value"];
                        }
                        if (!dr["DateFrom"].Equals(DBNull.Value))
                        {
                            objLandUseSettings.LandUseDateFrom = dr["DateFrom"].ToString();
                        }
                        if (!dr["DateTo"].Equals(DBNull.Value))
                        {
                            objLandUseSettings.LandUseDateTo = ((DateTime)dr["DateTo"]).ToShortDateString();
                        }
                    }
                }
                else
                {
                    objLandUseSettings = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objLandUseSettings;
        }

        public bool Insert(Setting objAppSettings)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            bool inserted = false;
            string strInsert = @"INSERT INTO [dbo].[Settings]
                                            ([AppId],[key],[value],[DateFrom],[DateTo],[Category],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],[CreatedBy]) OUTPUT INSERTED.ID 
                                     VALUES (@AppId,@key,@value,@DateFrom,@DateTo,@Category,@CenterId,@CenterName,@CreatedUserId,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AppId", objAppSettings.AppId));
                command.Parameters.Add(new SqlParameter("@key", objAppSettings.Key));
                command.Parameters.Add(new SqlParameter("@value", objAppSettings.Value));
                command.Parameters.Add(new SqlParameter("@DateFrom", objAppSettings.DateFrom));
                if(objAppSettings.DateTo != DateTime.MinValue)
                command.Parameters.Add(new SqlParameter("@DateTo", objAppSettings.DateTo));
                else
                    command.Parameters.Add(new SqlParameter("@DateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@Category", objAppSettings.Category));
                //command.Parameters.Add(new SqlParameter("@picture", objAppSettings.Picture));
                command.Parameters.Add(new SqlParameter("@CenterId", objAppSettings.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objAppSettings.CenterName));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objAppSettings.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objAppSettings.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objAppSettings.CreatedBy));

                connection.Open();
                objAppSettings.Id = (int)command.ExecuteScalar();
                if (objAppSettings.Id > 0)
                {
                    inserted = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return inserted;
        }

        public bool UpdateByKey(Setting objAppSettings)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Settings] SET [AppId]=@AppId,[key]=@key,[value]=@value,[DateFrom]=@DateFrom,[DateTo]=@DateTo,Category=@Category,[CenterId]=@CenterId,
                                [CenterName]=@CenterName,[UpdatedUserId]=@UpdatedUserId,
                                [UpdatedDate]=@UpdatedDate, [UpdatedBy]=@UpdatedBy WHERE [key]=@key and [DateFrom]=@DateFrom";
            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", objAppSettings.Id));
                command.Parameters.Add(new SqlParameter("@AppId", objAppSettings.AppId));
                command.Parameters.Add(new SqlParameter("@key", objAppSettings.Key));
                command.Parameters.Add(new SqlParameter("@value", objAppSettings.Value));
                command.Parameters.Add(new SqlParameter("@DateFrom", objAppSettings.DateFrom));

                if (objAppSettings.DateTo != DateTime.MinValue)
                    command.Parameters.Add(new SqlParameter("@DateTo", objAppSettings.DateTo));
                else
                    command.Parameters.Add(new SqlParameter("@DateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@Category", objAppSettings.Category));
                command.Parameters.Add(new SqlParameter("@CenterId", objAppSettings.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objAppSettings.CenterName));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objAppSettings.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objAppSettings.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objAppSettings.UpdatedBy));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateEndingDateofKey(Setting objAppSettings)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Settings] SET [DateTo]=@DateTo WHERE Id=@id";
            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@id", objAppSettings.Id));
                command.Parameters.Add(new SqlParameter("@DateTo", objAppSettings.DateTo));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<Setting> GetList()
        {
            List<Setting> RecordsList = new List<Setting>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [id],[AppId],[key],[value],[picture],[CenterId],[CenterName],[CreatedUserId],[CreatedDate],
                                      [CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[Settings]  ORDER BY  [id] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Setting objAppSettings = new Setting();
                    if (dr["id"].Equals(DBNull.Value))
                        objAppSettings.Id = 0;
                    else
                        objAppSettings.Id = (int)dr["id"];
                    if (dr["AppId"].Equals(DBNull.Value))
                        objAppSettings.AppId = string.Empty;
                    else
                        objAppSettings.AppId = (string)dr["AppId"];
                    if (dr["key"].Equals(DBNull.Value))
                        objAppSettings.Key = string.Empty;
                    else
                        objAppSettings.Key = (string)dr["key"];
                    if (dr["value"].Equals(DBNull.Value))
                        objAppSettings.Value = string.Empty;
                    else
                        objAppSettings.Value = (string)dr["value"];
                    if (dr["picture"].Equals(DBNull.Value))
                        objAppSettings.Picture = null;
                    else
                        objAppSettings.Picture = (byte[])dr["picture"];
                    if (dr["CenterId"].Equals(DBNull.Value))
                        objAppSettings.CenterId = 0;
                    else
                        objAppSettings.CenterId = (int)dr["CenterId"];
                    if (dr["CenterName"].Equals(DBNull.Value))
                        objAppSettings.CenterName = string.Empty;
                    else
                        objAppSettings.CenterName = (string)dr["CenterName"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objAppSettings.CreatedUserId = string.Empty;
                    else
                        objAppSettings.CreatedUserId = (string)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objAppSettings.CreatedDate = DateTime.MinValue;
                    else
                        objAppSettings.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objAppSettings.CreatedBy = string.Empty;
                    else
                        objAppSettings.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objAppSettings.UpdatedUserId = string.Empty;
                    else
                        objAppSettings.UpdatedUserId = (string)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objAppSettings.UpdatedDate = DateTime.MinValue;
                    else
                        objAppSettings.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objAppSettings.UpdatedBy = string.Empty;
                    else
                        objAppSettings.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objAppSettings);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT id FROM [dbo].[Settings] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("AppSettings::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}
