﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class AdminSettingHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[SettingId],[OldValue],[NewValue],[Remark],[CreatedDate],[CreatedBy],[CreatedByName] FROM [dbo].[AdminSettingHistory] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AdminSettingHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public AdminSettingHistory GetRecord(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            AdminSettingHistory objAdminSettingHistory = new AdminSettingHistory();
            string strGetRecord = @"SELECT [Id],[SettingId],[OldValue],[NewValue],[Remark],[CreatedDate],[CreatedBy],[CreatedByName] FROM [dbo].[AdminSettingHistory] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AdminSettingHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAdminSettingHistory.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["SettingId"].Equals(DBNull.Value))
                        objAdminSettingHistory.SettingId = 0;
                    else
                        objAdminSettingHistory.SettingId = (int)dTable.Rows[0]["SettingId"];
                    if (dTable.Rows[0]["OldValue"].Equals(DBNull.Value))
                        objAdminSettingHistory.OldValue = string.Empty;
                    else
                        objAdminSettingHistory.OldValue = (string)dTable.Rows[0]["OldValue"];
                    if (dTable.Rows[0]["NewValue"].Equals(DBNull.Value))
                        objAdminSettingHistory.NewValue = string.Empty;
                    else
                        objAdminSettingHistory.NewValue = (string)dTable.Rows[0]["NewValue"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objAdminSettingHistory.Remark = string.Empty;
                    else
                        objAdminSettingHistory.Remark = (string)dTable.Rows[0]["Remark"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedDate = DateTime.MinValue;
                    else
                        objAdminSettingHistory.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedBy = string.Empty;
                    else
                        objAdminSettingHistory.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["CreatedByName"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedByName = null;
                    else
                        objAdminSettingHistory.CreatedByName = (string)dTable.Rows[0]["CreatedByName"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAdminSettingHistory;
        }

        public void Insert(AdminSettingHistory objAdminSettingHistory)
        {
            throw new NotImplementedException();
        }

        public bool Insert(AdminSettingHistory objAdminSettingHistory,SqlCommand command)
        {

            string strInsert = @"INSERT INTO [dbo].[AdminSettingHistory]
                                            ([SettingId],[OldValue],[NewValue],[Remark],[CreatedDate],[CreatedBy],[CreatedByName])
                                     VALUES    (@SettingId,@OldValue,@NewValue,@Remark,@CreatedDate,@CreatedBy,@CreatedByName)";

            command.Parameters.Clear();
            command.CommandText = strInsert;
            
            try
            {
                command.Parameters.Add(new SqlParameter("@SettingId", objAdminSettingHistory.SettingId));
                if(!string.IsNullOrEmpty(objAdminSettingHistory.OldValue ))
                    command.Parameters.Add(new SqlParameter("@OldValue", objAdminSettingHistory.OldValue));
                else
                    command.Parameters.Add(new SqlParameter("@OldValue", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@NewValue", objAdminSettingHistory.NewValue));

                if (!string.IsNullOrEmpty(objAdminSettingHistory.Remark))
                    command.Parameters.Add(new SqlParameter("@Remark", objAdminSettingHistory.Remark));
                else
                    command.Parameters.Add(new SqlParameter("@Remark", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@CreatedDate", objAdminSettingHistory.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objAdminSettingHistory.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedByName", objAdminSettingHistory.CreatedByName));
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::Insert::Error!" + ex.Message, ex);
            }
            
        }

        public bool Update(AdminSettingHistory objAdminSettingHistory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[AdminSettingHistory] SET     [SettingId]=@SettingId,    [OldValue]=@OldValue,    [NewValue]=@NewValue,    [Remark]=@Remark,    [CreatedDate]=@CreatedDate,    [CreatedBy]=@CreatedBy,    [CreatedByName]=@CreatedByName ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objAdminSettingHistory.Id));
                command.Parameters.Add(new SqlParameter("@SettingId", objAdminSettingHistory.SettingId));
                command.Parameters.Add(new SqlParameter("@OldValue", objAdminSettingHistory.OldValue));
                command.Parameters.Add(new SqlParameter("@NewValue", objAdminSettingHistory.NewValue));
                command.Parameters.Add(new SqlParameter("@Remark", objAdminSettingHistory.Remark));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objAdminSettingHistory.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objAdminSettingHistory.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedByName", objAdminSettingHistory.CreatedByName));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string key,SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[AdminSettingHistory] where key = @key ";

            command.CommandText = strDelete;
            command.Parameters.Clear();

            command.Parameters.AddWithValue("@key", key);
            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::Delete::Error!" + ex.Message, ex);
            }
            
        }



        public List<AdminSettingHistory> GetList(int siteId)
        {
            List<AdminSettingHistory> RecordsList = new List<AdminSettingHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[SettingId],[OldValue],[NewValue],[Remark],[CreatedDate],[CreatedBy],[CreatedByName] FROM [dbo].[AdminSettingHistory] ORDER BY Id desc ";

            if (siteId > 0)
                strGetAllRecords += " inner join [dbo].[AdminSettings] where [dbo].[AdminSettings].SiteId = @siteId";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@siteId", siteId);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    AdminSettingHistory objAdminSettingHistory = new AdminSettingHistory();
                    if (dr["Id"].Equals(DBNull.Value))
                        objAdminSettingHistory.Id = 0;
                    else
                        objAdminSettingHistory.Id = (int)dr["Id"];
                    if (dr["SettingId"].Equals(DBNull.Value))
                        objAdminSettingHistory.SettingId = 0;
                    else
                        objAdminSettingHistory.SettingId = (int)dr["SettingId"];
                    if (dr["OldValue"].Equals(DBNull.Value))
                        objAdminSettingHistory.OldValue = "False";
                    else
                        objAdminSettingHistory.OldValue = (string)dr["OldValue"];
                    objAdminSettingHistory.OldValue = objAdminSettingHistory.OldValue == "1" ? "True" : "False";

                    if (dr["NewValue"].Equals(DBNull.Value))
                        objAdminSettingHistory.NewValue = "False";
                    else
                        objAdminSettingHistory.NewValue = (string)dr["NewValue"];
                    objAdminSettingHistory.NewValue = objAdminSettingHistory.NewValue == "1" ? "True" : "False";

                    if (dr["Remark"].Equals(DBNull.Value))
                        objAdminSettingHistory.Remark = string.Empty;
                    else
                        objAdminSettingHistory.Remark = (string)dr["Remark"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedDate = DateTime.MinValue;
                    else
                        objAdminSettingHistory.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedBy = string.Empty;
                    else
                        objAdminSettingHistory.CreatedBy = Convert.ToString( dr["CreatedBy"]);
                    if (dr["CreatedByName"].Equals(DBNull.Value))
                        objAdminSettingHistory.CreatedByName = null;
                    else
                        objAdminSettingHistory.CreatedByName = (string)dr["CreatedByName"];
                    RecordsList.Add(objAdminSettingHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[AdminSettingHistory] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdminSettingHistory::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}