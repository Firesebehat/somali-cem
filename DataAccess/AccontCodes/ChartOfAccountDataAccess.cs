﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class ChartOfAccountsDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[Code],[Parent],[Hirachy],[AccountType],[Name],[NameEng],[NameSortValue],[NameSoundx],[IsMain],[IsBudgetAccount],[IsCapitalBudgetAccount],[FatherCode],[IsActive],[CreatedBy],[CreatedUserId],[CreatedDate],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[IsDeleted] FROM [dbo].[ChartOfAccounts]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("ChartOfAccounts");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public ChartOfAccountsDomain GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            ChartOfAccountsDomain objChartOfAccounts = new ChartOfAccountsDomain();
            string strGetRecord = @"SELECT [Id],[Code],[Parent],[Hirachy],[AccountType],[Name],[NameEng],[NameSortValue],[NameSoundx],[IsMain],[IsBudgetAccount],[IsCapitalBudgetAccount],[FatherCode],[IsActive],[CreatedBy],[CreatedUserId],[CreatedDate],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[IsDeleted] FROM [dbo].[ChartOfAccounts] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("ChartOfAccounts");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objChartOfAccounts.Id = (int)dTable.Rows[0]["Id"];
                    objChartOfAccounts.Code = (string)dTable.Rows[0]["Code"];
                    objChartOfAccounts.Parent = (int)dTable.Rows[0]["Parent"];
                    if (dTable.Rows[0]["Hirachy"].Equals(DBNull.Value))
                        objChartOfAccounts.Hirachy = 0;
                    else
                        objChartOfAccounts.Hirachy = (int)dTable.Rows[0]["Hirachy"];
                    if (dTable.Rows[0]["AccountType"].Equals(DBNull.Value))
                        objChartOfAccounts.AccountType = 0;
                    else
                        objChartOfAccounts.AccountType = (int)dTable.Rows[0]["AccountType"];
                    if (dTable.Rows[0]["Name"].Equals(DBNull.Value))
                        objChartOfAccounts.Name = string.Empty;
                    else
                        objChartOfAccounts.Name = (string)dTable.Rows[0]["Name"];
                    if (dTable.Rows[0]["NameEng"].Equals(DBNull.Value))
                        objChartOfAccounts.NameEng = string.Empty;
                    else
                        objChartOfAccounts.NameEng = (string)dTable.Rows[0]["NameEng"];
                    if (dTable.Rows[0]["NameSortValue"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSortValue = string.Empty;
                    else
                        objChartOfAccounts.NameSortValue = (string)dTable.Rows[0]["NameSortValue"];
                    if (dTable.Rows[0]["NameSoundx"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSoundx = string.Empty;
                    else
                        objChartOfAccounts.NameSoundx = (string)dTable.Rows[0]["NameSoundx"];
                    if (dTable.Rows[0]["Parent"].Equals(DBNull.Value))
                        objChartOfAccounts.Parent = 0;
                    else
                        objChartOfAccounts.Parent = (int)dTable.Rows[0]["Parent"];
                    if (dTable.Rows[0]["IsMain"].Equals(DBNull.Value))
                        objChartOfAccounts.IsMain = false;
                    else
                        objChartOfAccounts.IsMain = (bool)dTable.Rows[0]["IsMain"];
                    if (dTable.Rows[0]["IsBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsBudgetAccount = false;
                    else
                        objChartOfAccounts.IsBudgetAccount = (bool)dTable.Rows[0]["IsBudgetAccount"];
                    if (dTable.Rows[0]["IsCapitalBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsCapitalBudgetAccount = false;
                    else
                        objChartOfAccounts.IsCapitalBudgetAccount = (bool)dTable.Rows[0]["IsCapitalBudgetAccount"];
                    if (dTable.Rows[0]["FatherCode"].Equals(DBNull.Value))
                        objChartOfAccounts.FatherCode = string.Empty;
                    else
                        objChartOfAccounts.FatherCode = (string)dTable.Rows[0]["FatherCode"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objChartOfAccounts.IsActive = false;
                    else
                        objChartOfAccounts.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedBy = string.Empty;
                    else
                        objChartOfAccounts.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedUserId = string.Empty;
                    else
                        objChartOfAccounts.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedDate = DateTime.MinValue;
                    else
                        objChartOfAccounts.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedUserId =string.Empty;
                    else
                        objChartOfAccounts.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedDate = DateTime.MinValue;
                    else
                        objChartOfAccounts.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedBy = string.Empty;
                    else
                        objChartOfAccounts.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objChartOfAccounts.IsDeleted = false;
                    else
                        objChartOfAccounts.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objChartOfAccounts;
        }

        public bool Insert(ChartOfAccountsDomain objChartOfAccounts)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[ChartOfAccounts]
                                            ([Code],[Parent],[Hirachy],[AccountType],[Name],[NameEng],[NameSortValue],[NameSoundx],[IsMain],[IsBudgetAccount],[IsCapitalBudgetAccount],[FatherCode],[IsActive],[CreatedBy],[CreatedUserId],[CreatedDate],[IsDeleted])
                                     VALUES    (@Code,@Parent,@Hirachy,@AccountType,@Name,@NameEng,@NameSortValue,@NameSoundx,@IsMain,@IsBudgetAccount,@IsCapitalBudgetAccount,@FatherCode,@IsActive,@CreatedBy,@CreatedUserId,@CreatedDate,@IsDeleted)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Code", objChartOfAccounts.Code));
                command.Parameters.Add(new SqlParameter("@Parent", objChartOfAccounts.Parent));
                command.Parameters.Add(new SqlParameter("@Hirachy", objChartOfAccounts.Hirachy));
                command.Parameters.Add(new SqlParameter("@AccountType", objChartOfAccounts.AccountType));
                command.Parameters.Add(new SqlParameter("@Name", objChartOfAccounts.Name));
                command.Parameters.Add(new SqlParameter("@NameEng", objChartOfAccounts.NameEng));
                command.Parameters.Add(new SqlParameter("@NameSortValue", objChartOfAccounts.NameSortValue));
                command.Parameters.Add(new SqlParameter("@NameSoundx", objChartOfAccounts.NameSoundx));
                command.Parameters.Add(new SqlParameter("@IsMain", objChartOfAccounts.IsMain));
                command.Parameters.Add(new SqlParameter("@IsBudgetAccount", objChartOfAccounts.IsBudgetAccount));
                command.Parameters.Add(new SqlParameter("@IsCapitalBudgetAccount", objChartOfAccounts.IsCapitalBudgetAccount));
                command.Parameters.Add(new SqlParameter("@FatherCode", objChartOfAccounts.FatherCode));
                command.Parameters.Add(new SqlParameter("@IsActive", objChartOfAccounts.IsActive));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objChartOfAccounts.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objChartOfAccounts.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objChartOfAccounts.CreatedDate));
                
                command.Parameters.Add(new SqlParameter("@IsDeleted", objChartOfAccounts.IsDeleted));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(ChartOfAccountsDomain objChartOfAccounts)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[ChartOfAccounts] SET     [Code]=@Code,    [Parent]=@Parent,    [Hirachy]=@Hirachy,    [AccountType]=@AccountType,    [Name]=@Name,    [NameEng]=@NameEng,    [NameSortValue]=@NameSortValue,    [NameSoundx]=@NameSoundx,    [IsMain]=@IsMain,    [IsBudgetAccount]=@IsBudgetAccount,    [IsCapitalBudgetAccount]=@IsCapitalBudgetAccount,    [FatherCode]=@FatherCode,    [IsActive]=@IsActive,          [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy,    [IsDeleted]=@IsDeleted WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objChartOfAccounts.Id));
                command.Parameters.Add(new SqlParameter("@Code", objChartOfAccounts.Code));
                command.Parameters.Add(new SqlParameter("@Parent", objChartOfAccounts.Parent));
                command.Parameters.Add(new SqlParameter("@Hirachy", objChartOfAccounts.Hirachy));
                command.Parameters.Add(new SqlParameter("@AccountType", objChartOfAccounts.AccountType));
                command.Parameters.Add(new SqlParameter("@Name", objChartOfAccounts.Name));
                command.Parameters.Add(new SqlParameter("@NameEng", objChartOfAccounts.NameEng));
                command.Parameters.Add(new SqlParameter("@NameSortValue", objChartOfAccounts.NameSortValue));
                command.Parameters.Add(new SqlParameter("@NameSoundx", objChartOfAccounts.NameSoundx));
                command.Parameters.Add(new SqlParameter("@IsMain", objChartOfAccounts.IsMain));
                command.Parameters.Add(new SqlParameter("@IsBudgetAccount", objChartOfAccounts.IsBudgetAccount));
                command.Parameters.Add(new SqlParameter("@IsCapitalBudgetAccount", objChartOfAccounts.IsCapitalBudgetAccount));
                command.Parameters.Add(new SqlParameter("@FatherCode", objChartOfAccounts.FatherCode));
                command.Parameters.Add(new SqlParameter("@IsActive", objChartOfAccounts.IsActive));
                
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objChartOfAccounts.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objChartOfAccounts.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objChartOfAccounts.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objChartOfAccounts.IsDeleted));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[ChartOfAccounts] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<ChartOfAccountsDomain> GetList()
        {
            List<ChartOfAccountsDomain> RecordsList = new List<ChartOfAccountsDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Parent],[Hirachy],[AccountType],[Name],[NameEng],[NameSortValue],[NameSoundx],[IsMain],[IsBudgetAccount],[IsCapitalBudgetAccount],[FatherCode],[IsActive],[CreatedBy],[CreatedUserId],[CreatedDate],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[IsDeleted] FROM [dbo].[ChartOfAccounts]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    ChartOfAccountsDomain objChartOfAccounts = new ChartOfAccountsDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objChartOfAccounts.Id = 0;
                    else
                        objChartOfAccounts.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objChartOfAccounts.Code = string.Empty;
                    else
                        objChartOfAccounts.Code = (string)dr["Code"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objChartOfAccounts.Parent = -1;
                    else
                        objChartOfAccounts.Parent = (int)dr["Parent"];
                    if (dr["Hirachy"].Equals(DBNull.Value))
                        objChartOfAccounts.Hirachy = 0;
                    else
                        objChartOfAccounts.Hirachy = (int)dr["Hirachy"];
                    if (dr["AccountType"].Equals(DBNull.Value))
                        objChartOfAccounts.AccountType = 0;
                    else
                        objChartOfAccounts.AccountType = (int)dr["AccountType"];
                    if (dr["Name"].Equals(DBNull.Value))
                        objChartOfAccounts.Name = string.Empty;
                    else
                        objChartOfAccounts.Name = (string)dr["Name"];
                    if (dr["NameEng"].Equals(DBNull.Value))
                        objChartOfAccounts.NameEng = string.Empty;
                    else
                        objChartOfAccounts.NameEng = (string)dr["NameEng"];
                    if (dr["NameSortValue"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSortValue = string.Empty;
                    else
                        objChartOfAccounts.NameSortValue = (string)dr["NameSortValue"];
                    if (dr["NameSoundx"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSoundx = string.Empty;
                    else
                        objChartOfAccounts.NameSoundx = (string)dr["NameSoundx"];
                    if (dr["IsMain"].Equals(DBNull.Value))
                        objChartOfAccounts.IsMain = false;
                    else
                        objChartOfAccounts.IsMain = (bool)dr["IsMain"];
                    if (dr["IsBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsBudgetAccount = false;
                    else
                        objChartOfAccounts.IsBudgetAccount = (bool)dr["IsBudgetAccount"];
                    if (dr["IsCapitalBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsCapitalBudgetAccount = false;
                    else
                        objChartOfAccounts.IsCapitalBudgetAccount = (bool)dr["IsCapitalBudgetAccount"];
                    if (dr["FatherCode"].Equals(DBNull.Value))
                        objChartOfAccounts.FatherCode = string.Empty;
                    else
                        objChartOfAccounts.FatherCode = (string)dr["FatherCode"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objChartOfAccounts.IsActive = false;
                    else
                        objChartOfAccounts.IsActive = (bool)dr["IsActive"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedBy = string.Empty;
                    else
                        objChartOfAccounts.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedUserId = string.Empty;
                    else
                        objChartOfAccounts.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objChartOfAccounts.CreatedDate = DateTime.MinValue;
                    else
                        objChartOfAccounts.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedUserId = string.Empty;
                    else
                        objChartOfAccounts.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedDate = DateTime.MinValue;
                    else
                        objChartOfAccounts.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objChartOfAccounts.UpdatedBy = string.Empty;
                    else
                        objChartOfAccounts.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objChartOfAccounts.IsDeleted = false;
                    else
                        objChartOfAccounts.IsDeleted = (bool)dr["IsDeleted"];
                    RecordsList.Add(objChartOfAccounts);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[ChartOfAccounts] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(ChartOfAccountsDomain obj)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id)  FROM [dbo].[ChartOfAccounts] WHERE  ([Code]=@Code or [NameSortValue] = @NameSortValue)";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Code", obj.Code);
            command.Parameters.AddWithValue("@Name", obj.Name);
            //command.Parameters.AddWithValue("@NameEng", obj.NameEng);
            command.Parameters.AddWithValue("@NameSortValue", obj.NameSortValue);
            try
            {
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool UpdateExists(ChartOfAccountsDomain obj)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id)  FROM [dbo].[ChartOfAccounts] WHERE 
                             ( [NameSortValue] = @NameSortValue or Code = @Code) and Id <> @Id ";

            //if(obj.NameEng != string.Empty)
            //{
            //    strExists += " or ([Code] = @Code and[NameEng] = @NameEng and Id<> @Id); ";
            //}

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Code", obj.Code);
            //command.Parameters.AddWithValue("@NameEng", obj.NameEng);
            command.Parameters.AddWithValue("@NameSortValue", obj.NameSortValue);
            command.Parameters.AddWithValue("@Id", obj.Id);
            try
            {
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public List<ChartOfAccountsDomain> SearchChartOfAccounts(string nameSort, string nameEng, string code, int parent)
        {
            List<ChartOfAccountsDomain> RecordsList = new List<ChartOfAccountsDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Parent],[Hirachy],[AccountType],[Name],[NameEng],[NameSortValue],[NameSoundx],[IsMain],[IsBudgetAccount],[IsCapitalBudgetAccount],[FatherCode] FROM [dbo].[ChartOfAccounts] 
                                         where 1=1";
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.Connection = connection;
            if (nameSort != string.Empty)
            {
                strGetAllRecords += " and NameSortValue like @NameSortValue";
                command.Parameters.AddWithValue("@NameSortValue", "%" + nameSort + "%");
            }
            if (nameEng != string.Empty)
            {
                strGetAllRecords += " and NameEng = @NameEng";
                command.Parameters.AddWithValue("@NameEng", nameEng);
            }
            if (code != string.Empty)
            {
                strGetAllRecords += " and Code LIKE @Code";
                command.Parameters.AddWithValue("@Code", code + "%");
            }
            if (parent != 0)
            {
                strGetAllRecords += " and Parent =  @Parent";
                command.Parameters.AddWithValue("@Parent", parent);
            }

            strGetAllRecords += " ORDER BY  [Code] DESC";
            command.CommandText = strGetAllRecords;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    ChartOfAccountsDomain objChartOfAccounts = new ChartOfAccountsDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objChartOfAccounts.Id = 0;
                    else
                        objChartOfAccounts.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objChartOfAccounts.Code = string.Empty;
                    else
                        objChartOfAccounts.Code = (string)dr["Code"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objChartOfAccounts.Parent = -1;
                    else
                        objChartOfAccounts.Parent = (int)dr["Parent"];
                    if (dr["Hirachy"].Equals(DBNull.Value))
                        objChartOfAccounts.Hirachy = 0;
                    else
                        objChartOfAccounts.Hirachy = (int)dr["Hirachy"];
                    if (dr["AccountType"].Equals(DBNull.Value))
                        objChartOfAccounts.AccountType = 0;
                    else
                        objChartOfAccounts.AccountType = (int)dr["AccountType"];
                    if (dr["Name"].Equals(DBNull.Value))
                        objChartOfAccounts.Name = string.Empty;
                    else
                        objChartOfAccounts.Name = (string)dr["Name"];
                    if (dr["NameEng"].Equals(DBNull.Value))
                        objChartOfAccounts.NameEng = string.Empty;
                    else
                        objChartOfAccounts.NameEng = (string)dr["NameEng"];
                    if (dr["NameSortValue"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSortValue = string.Empty;
                    else
                        objChartOfAccounts.NameSortValue = (string)dr["NameSortValue"];
                    if (dr["NameSoundx"].Equals(DBNull.Value))
                        objChartOfAccounts.NameSoundx = string.Empty;
                    else
                        objChartOfAccounts.NameSoundx = (string)dr["NameSoundx"];
                    if (dr["IsMain"].Equals(DBNull.Value))
                        objChartOfAccounts.IsMain = false;
                    else
                        objChartOfAccounts.IsMain = (bool)dr["IsMain"];
                    if (dr["IsBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsBudgetAccount = false;
                    else
                        objChartOfAccounts.IsBudgetAccount = (bool)dr["IsBudgetAccount"];
                    if (dr["IsCapitalBudgetAccount"].Equals(DBNull.Value))
                        objChartOfAccounts.IsCapitalBudgetAccount = false;
                    else
                        objChartOfAccounts.IsCapitalBudgetAccount = (bool)dr["IsCapitalBudgetAccount"];
                    if (dr["FatherCode"].Equals(DBNull.Value))
                        objChartOfAccounts.FatherCode = string.Empty;
                    else
                        objChartOfAccounts.FatherCode = (string)dr["FatherCode"];
                    RecordsList.Add(objChartOfAccounts);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ChartOfAccounts::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
    }
}