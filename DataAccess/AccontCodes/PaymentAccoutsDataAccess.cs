﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;

namespace CUSTOR.DataAccess
{
    public class PaymentAccountsDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[TaxType],[PaymentType],[AccountCode],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PaymentAccounts]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PaymentAccounts");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public PaymentAccounts GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            PaymentAccounts objPaymentAccounts = new PaymentAccounts();
            string strGetRecord = @"SELECT [Id],[TaxType],[PaymentType],[AccountCode],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PaymentAccounts] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PaymentAccounts");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objPaymentAccounts.Id = (int)dTable.Rows[0]["Id"];
                    objPaymentAccounts.TaxType = (int)dTable.Rows[0]["TaxType"];
                    objPaymentAccounts.PaymentType = (int)dTable.Rows[0]["PaymentType"];
                    objPaymentAccounts.AccountCode = (int)dTable.Rows[0]["AccountCode"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedUserId = string.Empty;
                    else
                        objPaymentAccounts.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedDate = DateTime.MinValue;
                    else
                        objPaymentAccounts.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedBy = string.Empty;
                    else
                        objPaymentAccounts.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedUserId = string.Empty;
                    else
                        objPaymentAccounts.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedDate = DateTime.MinValue;
                    else
                        objPaymentAccounts.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedBy = string.Empty;
                    else
                        objPaymentAccounts.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objPaymentAccounts;
        }

        public bool Insert(PaymentAccounts objPaymentAccounts)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[PaymentAccounts]
                                            ([TaxType],[PaymentType],[AccountCode],[CreatedUserId],[CreatedDate],[CreatedBy])
                                     VALUES    (@TaxType,@PaymentType,@AccountCode,@CreatedUserId,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TaxType", objPaymentAccounts.TaxType));
                command.Parameters.Add(new SqlParameter("@PaymentType", objPaymentAccounts.PaymentType));
                command.Parameters.Add(new SqlParameter("@AccountCode", objPaymentAccounts.AccountCode));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objPaymentAccounts.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objPaymentAccounts.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objPaymentAccounts.CreatedBy));



                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(PaymentAccounts objPaymentAccounts)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[PaymentAccounts] SET     [TaxType]=@TaxType,    [PaymentType]=@PaymentType,    [AccountCode]=@AccountCode,     [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objPaymentAccounts.Id));
                command.Parameters.Add(new SqlParameter("@TaxType", objPaymentAccounts.TaxType));
                command.Parameters.Add(new SqlParameter("@PaymentType", objPaymentAccounts.PaymentType));
                command.Parameters.Add(new SqlParameter("@AccountCode", objPaymentAccounts.AccountCode));
                
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objPaymentAccounts.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objPaymentAccounts.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objPaymentAccounts.UpdatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[PaymentAccounts] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<PaymentAccounts> GetList()
        {
            List<PaymentAccounts> RecordsList = new List<PaymentAccounts>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[TaxType],[PaymentType],[AccountCode],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[PaymentAccounts]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PaymentAccounts objPaymentAccounts = new PaymentAccounts();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPaymentAccounts.Id = 0;
                    else
                        objPaymentAccounts.Id = (int)dr["Id"];
                    if (dr["TaxType"].Equals(DBNull.Value))
                        objPaymentAccounts.TaxType = 0;
                    else
                        objPaymentAccounts.TaxType = (int)dr["TaxType"];
                    if (dr["PaymentType"].Equals(DBNull.Value))
                        objPaymentAccounts.PaymentType = 0;
                    else
                        objPaymentAccounts.PaymentType = (int)dr["PaymentType"];
                    if (dr["AccountCode"].Equals(DBNull.Value))
                        objPaymentAccounts.AccountCode = 0;
                    else
                        objPaymentAccounts.AccountCode = (int)dr["AccountCode"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedUserId = string.Empty;
                    else
                        objPaymentAccounts.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedDate = DateTime.MinValue;
                    else
                        objPaymentAccounts.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objPaymentAccounts.CreatedBy = string.Empty;
                    else
                        objPaymentAccounts.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedUserId = string.Empty;
                    else
                        objPaymentAccounts.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedDate = DateTime.MinValue;
                    else
                        objPaymentAccounts.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objPaymentAccounts.UpdatedBy = string.Empty;
                    else
                        objPaymentAccounts.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objPaymentAccounts);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<PaymentAccounts> GetList(int lan)
        {
            List<PaymentAccounts> RecordsList = new List<PaymentAccounts>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT P.[Id],[TaxType],[PaymentType],[AccountCode],p.[CreatedUserId],case @lan when 1 then ch.Name else ch.NameEng end as AccountCodeName
                             FROM [dbo].[PaymentAccounts]  p INNER JOIN ChartOfAccounts ch on ch.Id = p.AccountCode ORDER BY  p.[Id] Desc";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@lan", lan);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PaymentAccounts objPaymentAccounts = new PaymentAccounts();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPaymentAccounts.Id = 0;
                    else
                        objPaymentAccounts.Id = (int)dr["Id"];
                    if (dr["TaxType"].Equals(DBNull.Value))
                        objPaymentAccounts.TaxType = 0;
                    else
                        objPaymentAccounts.TaxType = (int)dr["TaxType"];
                    objPaymentAccounts.TaxTypeName = EnumHelper.GetEnumDescription(typeof(CUSTORCommon.Enumerations.TaxType.eTaxType), Language.eLanguage.eAmharic, objPaymentAccounts.TaxType);

                    if (dr["PaymentType"].Equals(DBNull.Value))
                        objPaymentAccounts.PaymentType = 0;
                    else
                        objPaymentAccounts.PaymentType = (int)dr["PaymentType"];
                    objPaymentAccounts.TaxTypeName = EnumHelper.GetEnumDescription(typeof(PaymentType.ePaymentType), Language.eLanguage.eAmharic, objPaymentAccounts.PaymentType);
                    if (dr["AccountCode"].Equals(DBNull.Value))
                        objPaymentAccounts.AccountCode = 0;
                    else
                        objPaymentAccounts.AccountCode = (int)dr["AccountCode"];
                    if (dr["AccountCodeName"].Equals(DBNull.Value))
                        objPaymentAccounts.AccountCodeName = string.Empty;
                    else
                        objPaymentAccounts.AccountCodeName = (string)dr["AccountCodeName"];
                   
                    RecordsList.Add(objPaymentAccounts);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(PaymentAccounts obj)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[PaymentAccounts] WHERE  AccountCode=@AccountCode and PaymentType = @PaymentType and TaxType=@TaxType";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AccountCode", obj.AccountCode));
                command.Parameters.Add(new SqlParameter("@PaymentType", obj.PaymentType));
                command.Parameters.Add(new SqlParameter("@TaxType", obj.TaxType));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool UpdateExists(PaymentAccounts obj)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[PaymentAccounts] WHERE  AccountCode=@AccountCode and PaymentType = @PaymentType and TaxType=@TaxType and Id <> @Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", obj.Id));
                command.Parameters.Add(new SqlParameter("@AccountCode", obj.AccountCode));
                command.Parameters.Add(new SqlParameter("@PaymentType", obj.PaymentType));
                command.Parameters.Add(new SqlParameter("@TaxType", obj.TaxType));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public List<PaymentAccounts> GetListByTaxTypeId(int taxTypeid)
        {
            List<PaymentAccounts> RecordsList = new List<PaymentAccounts>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[TaxType],[PaymentType],[AccountCode] 
                                        FROM [dbo].[PaymentAccounts] where 1=1 and TaxType = @taxTypeId ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@taxTypeId", taxTypeid));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PaymentAccounts objPaymentAccounts = new PaymentAccounts();
                    if (dr["Id"].Equals(DBNull.Value))
                        objPaymentAccounts.Id = 0;
                    else
                        objPaymentAccounts.Id = (int)dr["Id"];
                    if (dr["TaxType"].Equals(DBNull.Value))
                        objPaymentAccounts.TaxType = 0;
                    else
                        objPaymentAccounts.TaxType = (int)dr["TaxType"];
                    if (dr["PaymentType"].Equals(DBNull.Value))
                        objPaymentAccounts.PaymentType = 0;
                    else
                        objPaymentAccounts.PaymentType = (int)dr["PaymentType"];
                    if (dr["AccountCode"].Equals(DBNull.Value))
                        objPaymentAccounts.AccountCode = 0;
                    else
                        objPaymentAccounts.AccountCode = (int)dr["AccountCode"];
                    RecordsList.Add(objPaymentAccounts);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PaymentAccounts::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
    }
}