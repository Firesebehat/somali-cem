﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class NationalityDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[parent],[id],[AmDescriptionSortValue],[AmDescriptionSoundxValue] FROM [dbo].[Nationality] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Nationality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Nationality GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Nationality objNationality = new Nationality();
            string strGetRecord = @"SELECT [code],[description],[amdescription],[parent],[id],[AmDescriptionSortValue],[AmDescriptionSoundxValue] FROM [dbo].[Nationality] where Id = @Id ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Id", ID);
            DataTable dTable = new DataTable("Nationality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objNationality.Code = string.Empty;
                    else
                        objNationality.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objNationality.Description = string.Empty;
                    else
                        objNationality.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amdescription"].Equals(DBNull.Value))
                        objNationality.Amdescription = string.Empty;
                    else
                        objNationality.Amdescription = (string)dTable.Rows[0]["amdescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objNationality.Parent = string.Empty;
                    else
                        objNationality.Parent = (string)dTable.Rows[0]["parent"];
                    objNationality.Id = (int)dTable.Rows[0]["id"];
                    if (dTable.Rows[0]["AmDescriptionSortValue"].Equals(DBNull.Value))
                        objNationality.AmDescriptionSortValue = string.Empty;
                    else
                        objNationality.AmDescriptionSortValue = (string)dTable.Rows[0]["AmDescriptionSortValue"];
                    if (dTable.Rows[0]["AmDescriptionSoundxValue"].Equals(DBNull.Value))
                        objNationality.AmDescriptionSoundxValue = string.Empty;
                    else
                        objNationality.AmDescriptionSoundxValue = (string)dTable.Rows[0]["AmDescriptionSoundxValue"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objNationality;
        }

        public bool Insert(Nationality objNationality)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Nationality]
                                            ([code],[description],[amdescription],[AmDescriptionSortValue],[AmDescriptionSoundxValue])
                                     VALUES    (@code,@description,@amdescription,@AmDescriptionSortValue,@AmDescriptionSoundxValue)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objNationality.Code));
                command.Parameters.Add(new SqlParameter("@description", objNationality.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objNationality.Amdescription));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSortValue", objNationality.AmDescriptionSortValue));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSoundxValue", objNationality.AmDescriptionSoundxValue));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Nationality objNationality)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Nationality] SET       [description]=@description,    [amdescription]=@amdescription,       [AmDescriptionSortValue]=@AmDescriptionSortValue,    [AmDescriptionSoundxValue]=@AmDescriptionSoundxValue where Id = @Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objNationality.Id));
                command.Parameters.Add(new SqlParameter("@description", objNationality.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objNationality.Amdescription));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSortValue", objNationality.AmDescriptionSortValue));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSoundxValue", objNationality.AmDescriptionSoundxValue));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Nationality] where Id = @Id ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Id", ID);
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Nationality> GetList()
        {
            List<Nationality> RecordsList = new List<Nationality>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[parent],[id],[AmDescriptionSortValue],[AmDescriptionSoundxValue] FROM [dbo].[Nationality] order by description desc ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Nationality objNationality = new Nationality();
                    if (dr["code"].Equals(DBNull.Value))
                        objNationality.Code = string.Empty;
                    else
                        objNationality.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objNationality.Description = string.Empty;
                    else
                        objNationality.Description = (string)dr["description"];
                    if (dr["amdescription"].Equals(DBNull.Value))
                        objNationality.Amdescription = string.Empty;
                    else
                        objNationality.Amdescription = (string)dr["amdescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objNationality.Parent = string.Empty;
                    else
                        objNationality.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objNationality.Id = 0;
                    else
                        objNationality.Id = (int)dr["id"];
                    if (dr["AmDescriptionSortValue"].Equals(DBNull.Value))
                        objNationality.AmDescriptionSortValue = string.Empty;
                    else
                        objNationality.AmDescriptionSortValue = (string)dr["AmDescriptionSortValue"];
                    if (dr["AmDescriptionSoundxValue"].Equals(DBNull.Value))
                        objNationality.AmDescriptionSoundxValue = string.Empty;
                    else
                        objNationality.AmDescriptionSoundxValue = (string)dr["AmDescriptionSoundxValue"];
                    RecordsList.Add(objNationality);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string nameEng, string nameSortAm)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id)  FROM [dbo].[Nationality] where AmDescriptionSortValue = @AmDescriptionSortValue or Description=@Description ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@AmDescriptionSortValue", nameSortAm);
            command.Parameters.AddWithValue("@Description", nameEng);
            command.Connection = connection;

            try
            {
                connection.Open();
                int Id = (int)command.ExecuteScalar();
                return Id > 0 ;
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string nameEng, string nameSortAm, int Id)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id)  FROM [dbo].[Nationality] where (AmDescriptionSortValue = @AmDescriptionSortValue or Description=@Description) and Id <> @Id ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@AmDescriptionSortValue", nameSortAm);
            command.Parameters.AddWithValue("@Description", nameEng);
            command.Parameters.AddWithValue("@Id", Id);
            command.Connection = connection;

            try
            {
                connection.Open();
                int count = (int)command.ExecuteScalar();
                return count > 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string GetNextCode()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT (MAX(Convert(int,Code))) + 1 FROM [dbo].[Nationality]";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw new Exception("Nationality::GetNextCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

    }
}