﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class MiscellaneousPaymentAccountDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [id],[payment_reason],[payment_reason_eng],[account_name],[account_code],[code] FROM [dbo].[MiscellaneousPaymentAccount]  ORDER BY  [id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("MiscellaneousPaymentAccount");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public MiscellaneousPaymentAccount GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            MiscellaneousPaymentAccount objMiscellaneousPaymentAccount = new MiscellaneousPaymentAccount();
            string strGetRecord = @"SELECT [id],[payment_reason],[payment_reason_eng],[account_name],[account_code],[code] FROM [dbo].[MiscellaneousPaymentAccount] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("MiscellaneousPaymentAccount");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objMiscellaneousPaymentAccount.Id = (int)dTable.Rows[0]["id"];
                    objMiscellaneousPaymentAccount.Payment_reason = (string)dTable.Rows[0]["payment_reason"];
                    if (dTable.Rows[0]["payment_reason_eng"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Payment_reason_eng = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Payment_reason_eng = (string)dTable.Rows[0]["payment_reason_eng"];
                    if (dTable.Rows[0]["account_name"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Account_name = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Account_name = (string)dTable.Rows[0]["account_name"];
                    objMiscellaneousPaymentAccount.Account_code = (string)dTable.Rows[0]["account_code"];
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Code = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Code = (string)dTable.Rows[0]["code"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objMiscellaneousPaymentAccount;
        }

        public bool Insert(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[MiscellaneousPaymentAccount]
                                            ([id],[payment_reason],[payment_reason_eng],[account_name],[account_code],[code])
                                     VALUES    (@id,@payment_reason,@payment_reason_eng,@account_name,@account_code,@code)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", objMiscellaneousPaymentAccount.Id));
                command.Parameters.Add(new SqlParameter("@payment_reason", objMiscellaneousPaymentAccount.Payment_reason));
                command.Parameters.Add(new SqlParameter("@payment_reason_eng", objMiscellaneousPaymentAccount.Payment_reason_eng));
                command.Parameters.Add(new SqlParameter("@account_name", objMiscellaneousPaymentAccount.Account_name));
                command.Parameters.Add(new SqlParameter("@account_code", objMiscellaneousPaymentAccount.Account_code));
                command.Parameters.Add(new SqlParameter("@code", objMiscellaneousPaymentAccount.Code));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[MiscellaneousPaymentAccount] SET    [payment_reason]=@payment_reason,    [payment_reason_eng]=@payment_reason_eng,    [account_name]=@account_name,    [account_code]=@account_code,    [code]=@code WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", objMiscellaneousPaymentAccount.Id));
                command.Parameters.Add(new SqlParameter("@payment_reason", objMiscellaneousPaymentAccount.Payment_reason));
                command.Parameters.Add(new SqlParameter("@payment_reason_eng", objMiscellaneousPaymentAccount.Payment_reason_eng));
                command.Parameters.Add(new SqlParameter("@account_name", objMiscellaneousPaymentAccount.Account_name));
                command.Parameters.Add(new SqlParameter("@account_code", objMiscellaneousPaymentAccount.Account_code));
                command.Parameters.Add(new SqlParameter("@code", objMiscellaneousPaymentAccount.Code));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[MiscellaneousPaymentAccount] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<MiscellaneousPaymentAccount> GetList()
        {
            List<MiscellaneousPaymentAccount> RecordsList = new List<MiscellaneousPaymentAccount>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [id],[payment_reason],[payment_reason_eng],[account_name],[account_code],[code] FROM [dbo].[MiscellaneousPaymentAccount]  ORDER BY  [id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MiscellaneousPaymentAccount objMiscellaneousPaymentAccount = new MiscellaneousPaymentAccount();
                    if (dr["id"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Id = 0;
                    else
                        objMiscellaneousPaymentAccount.Id = (int)dr["id"];
                    if (dr["payment_reason"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Payment_reason = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Payment_reason = (string)dr["payment_reason"];
                    if (dr["payment_reason_eng"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Payment_reason_eng = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Payment_reason_eng = (string)dr["payment_reason_eng"];
                    if (dr["account_name"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Account_name = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Account_name = (string)dr["account_name"];
                    if (dr["account_code"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Account_code = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Account_code = (string)dr["account_code"];
                    if (dr["code"].Equals(DBNull.Value))
                        objMiscellaneousPaymentAccount.Code = string.Empty;
                    else
                        objMiscellaneousPaymentAccount.Code = (string)dr["code"];
                    RecordsList.Add(objMiscellaneousPaymentAccount);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT id FROM [dbo].[MiscellaneousPaymentAccount] WHERE Account_code=@Account_code";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Account_code", objMiscellaneousPaymentAccount.Account_code));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("MiscellaneousPaymentAccount::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}
