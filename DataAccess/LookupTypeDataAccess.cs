﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class LookupTypeDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[Description],[Amdescription],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookupType]  ORDER BY  [Id] DESC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("LookupType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public LookupType GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            LookupType objLookupType = new LookupType();
            string strGetRecord = @"SELECT [Id],[Description],[Amdescription],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookupType] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("LookupType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objLookupType.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objLookupType.Description = string.Empty;
                    else
                        objLookupType.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["Amdescription"].Equals(DBNull.Value))
                        objLookupType.Amdescription = string.Empty;
                    else
                        objLookupType.Amdescription = (string)dTable.Rows[0]["Amdescription"];
                    if (dTable.Rows[0]["SortValue"].Equals(DBNull.Value))
                        objLookupType.SortValue = string.Empty;
                    else
                        objLookupType.SortValue = (string)dTable.Rows[0]["SortValue"];
                    if (dTable.Rows[0]["Soundx"].Equals(DBNull.Value))
                        objLookupType.Soundx = string.Empty;
                    else
                        objLookupType.Soundx = (string)dTable.Rows[0]["Soundx"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objLookupType.IsActive = false;
                    else
                        objLookupType.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objLookupType.CreatedDate = DateTime.MinValue;
                    else
                        objLookupType.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objLookupType.CreatedBy = string.Empty;
                    else
                        objLookupType.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objLookupType.UpdatedDate = DateTime.MinValue;
                    else
                        objLookupType.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objLookupType.UpdatedBy = string.Empty;
                    else
                        objLookupType.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objLookupType;
        }

        public bool Insert(LookupType objLookupType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[LookupType]
                                            ([Description],[Amdescription],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy])
                                     VALUES    (@Description,@Amdescription,@SortValue,@Soundx,@IsActive,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Description", objLookupType.Description));
                command.Parameters.Add(new SqlParameter("@Amdescription", objLookupType.Amdescription));
                command.Parameters.Add(new SqlParameter("@SortValue", objLookupType.SortValue));
                command.Parameters.Add(new SqlParameter("@Soundx", objLookupType.Soundx));
                command.Parameters.Add(new SqlParameter("@IsActive", objLookupType.IsActive));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objLookupType.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objLookupType.CreatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(LookupType objLookupType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[LookupType] SET     [Description]=@Description,    [Amdescription]=@Amdescription,    [SortValue]=@SortValue,    [Soundx]=@Soundx,    [IsActive]=@IsActive,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objLookupType.Id));
                command.Parameters.Add(new SqlParameter("@Description", objLookupType.Description));
                command.Parameters.Add(new SqlParameter("@Amdescription", objLookupType.Amdescription));
                command.Parameters.Add(new SqlParameter("@SortValue", objLookupType.SortValue));
                command.Parameters.Add(new SqlParameter("@Soundx", objLookupType.Soundx));
                command.Parameters.Add(new SqlParameter("@IsActive", objLookupType.IsActive));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objLookupType.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objLookupType.UpdatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[LookupType] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<LookupType> GetList()
        {
            List<LookupType> RecordsList = new List<LookupType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Description],[Amdescription],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookupType]  ORDER BY  [Id] DESC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    LookupType objLookupType = new LookupType();
                    if (dr["Id"].Equals(DBNull.Value))
                        objLookupType.Id = 0;
                    else
                        objLookupType.Id = (int)dr["Id"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objLookupType.Description = string.Empty;
                    else
                        objLookupType.Description = (string)dr["Description"];
                    if (dr["Amdescription"].Equals(DBNull.Value))
                        objLookupType.Amdescription = string.Empty;
                    else
                        objLookupType.Amdescription = (string)dr["Amdescription"];
                    if (dr["SortValue"].Equals(DBNull.Value))
                        objLookupType.SortValue = string.Empty;
                    else
                        objLookupType.SortValue = (string)dr["SortValue"];
                    if (dr["Soundx"].Equals(DBNull.Value))
                        objLookupType.Soundx = string.Empty;
                    else
                        objLookupType.Soundx = (string)dr["Soundx"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objLookupType.IsActive = false;
                    else
                        objLookupType.IsActive = (bool)dr["IsActive"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objLookupType.CreatedDate = DateTime.MinValue;
                    else
                        objLookupType.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objLookupType.CreatedBy = string.Empty;
                    else
                        objLookupType.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objLookupType.UpdatedDate = DateTime.MinValue;
                    else
                        objLookupType.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objLookupType.UpdatedBy = string.Empty;
                    else
                        objLookupType.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objLookupType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool UpdateExists(LookupType lookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[LookupType] WHERE (Description = @Description  and  [Id] <> @Id)";

            if(lookup.SortValue !=string.Empty)
            {
                strExists += " or SortValue = @SortValue";
            }
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", lookup.Id));
                command.Parameters.Add(new SqlParameter("@Description", lookup.Description));
                command.Parameters.Add(new SqlParameter("@SortValue", lookup.SortValue));

                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(LookupType lookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[LookupType] WHERE (Description = @Description)";
            if(lookup.SortValue != string.Empty)
            {
                strExists += " or SortValue = @SortValue";
            }
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", lookup.Id));
                command.Parameters.Add(new SqlParameter("@Description", lookup.Description));
                command.Parameters.Add(new SqlParameter("@SortValue", lookup.SortValue));

                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}