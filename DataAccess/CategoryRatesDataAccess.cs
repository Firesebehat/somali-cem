﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;

namespace CUSTOR.DataAccess
{
    public class CategoryRatesDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[CategoryId],[GrossProfit],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],[SalesRate],[TOTCode],[TOTRate],[ExciseCode],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[MinAge],[MaxAge],[WorkingDays],[Unit] FROM [dbo].[CategoryRates]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("CategoryRates");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public CategoryRates GetRecordD(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CategoryRates objCategoryRates = new CategoryRates();
            string strGetRecord = @"SELECT [Id],[CategoryId],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],[SalesRate],[TOTRate],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[MinAge],[MaxAge] FROM [dbo].[CategoryRates] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("CategoryRates");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objCategoryRates.Id = (int)dTable.Rows[0]["Id"];
                    objCategoryRates.CategoryId = (int)dTable.Rows[0]["CategoryId"];
                    if (dTable.Rows[0]["GrossProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfitRate = 0;
                    else
                        objCategoryRates.GrossProfitRate = (double)dTable.Rows[0]["GrossProfitRate"];
                    if (dTable.Rows[0]["AnnualSales"].Equals(DBNull.Value))
                        objCategoryRates.AnnualSales = 0;
                    else
                        objCategoryRates.AnnualSales = (decimal)dTable.Rows[0]["AnnualSales"];
                    if (dTable.Rows[0]["NetProfit"].Equals(DBNull.Value))
                        objCategoryRates.NetProfit = 0;
                    else
                        objCategoryRates.NetProfit = (decimal)dTable.Rows[0]["NetProfit"];
                    if (dTable.Rows[0]["ProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.NetProfitRate = 0;
                    else
                        objCategoryRates.NetProfitRate = (double)dTable.Rows[0]["ProfitRate"];
                    if (dTable.Rows[0]["DriverSalary"].Equals(DBNull.Value))
                        objCategoryRates.DriverSalary = 0;
                    else
                        objCategoryRates.DriverSalary = (decimal)dTable.Rows[0]["DriverSalary"];
                    if (dTable.Rows[0]["AssistantSalary"].Equals(DBNull.Value))
                        objCategoryRates.AssistantSalary = 0;
                    else
                        objCategoryRates.AssistantSalary = (decimal)dTable.Rows[0]["AssistantSalary"];
                    if (dTable.Rows[0]["SalesRate"].Equals(DBNull.Value))
                        objCategoryRates.SalesRate = 0;
                    else
                        objCategoryRates.SalesRate = (double)dTable.Rows[0]["SalesRate"];
                    if (dTable.Rows[0]["TOTRate"].Equals(DBNull.Value))
                        objCategoryRates.TOTRate = 0;
                    else
                        objCategoryRates.TOTRate = (double)dTable.Rows[0]["TOTRate"];
                    if (dTable.Rows[0]["ExciseRate"].Equals(DBNull.Value))
                        objCategoryRates.ExciseRate = 0;
                    else
                        objCategoryRates.ExciseRate = (double)dTable.Rows[0]["ExciseRate"];
                    if (dTable.Rows[0]["MinCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MinCapacity = 0;
                    else
                        objCategoryRates.MinCapacity = (double)dTable.Rows[0]["MinCapacity"];
                    if (dTable.Rows[0]["MaxCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MaxCapacity = 0;
                    else
                        objCategoryRates.MaxCapacity = (double)dTable.Rows[0]["MaxCapacity"];
                    if (dTable.Rows[0]["AppliedDateFrom"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateFrom = DateTime.MinValue;
                    else
                        objCategoryRates.AppliedDateFrom = (DateTime)dTable.Rows[0]["AppliedDateFrom"];
                    if (dTable.Rows[0]["AppliedDateTo"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateTo = DateTime.MinValue;
                    else
                        objCategoryRates.AppliedDateTo = (DateTime)dTable.Rows[0]["AppliedDateTo"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objCategoryRates.IsActive = false;
                    else
                        objCategoryRates.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objCategoryRates.IsDeleted = false;
                    else
                        objCategoryRates.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.CreatedUserId = string.Empty;
                    else
                        objCategoryRates.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objCategoryRates.CreatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objCategoryRates.CreatedBy = string.Empty;
                    else
                        objCategoryRates.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedUserId = string.Empty;
                    else
                        objCategoryRates.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedBy = string.Empty;
                    else
                        objCategoryRates.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["MinAge"].Equals(DBNull.Value))
                        objCategoryRates.MinAge = 0;
                    else
                        objCategoryRates.MinAge = (int)dTable.Rows[0]["MinAge"];
                    if (dTable.Rows[0]["MaxAge"].Equals(DBNull.Value))
                        objCategoryRates.MaxAge = 0;
                    else
                        objCategoryRates.MaxAge = (int)dTable.Rows[0]["MaxAge"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCategoryRates;
        }

        public CategoryRates GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CategoryRates objCategoryRates = new CategoryRates();
            string strGetRecord = @"SELECT [Id],[CategoryId],[GrossProfit],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],
                                   [SalesRate],[TOTCode],[TOTRate],[ExciseCode],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],
                                   [IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[MinAge],[MaxAge],[WorkingDays],[Unit],
                                   [BudgetYear],[Above15Years] FROM [dbo].[CategoryRates]  WHERE  Id = @Id order by Id desc";

           

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("CategoryRates");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objCategoryRates.Id = (int)dTable.Rows[0]["Id"];
                    objCategoryRates.CategoryId = (int)dTable.Rows[0]["CategoryId"];
                    if (dTable.Rows[0]["GrossProfit"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfit = 0;
                    else
                        objCategoryRates.GrossProfit = (decimal)dTable.Rows[0]["GrossProfit"];
                    if (dTable.Rows[0]["GrossProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfitRate = 0;
                    else
                        objCategoryRates.GrossProfitRate = (double)dTable.Rows[0]["GrossProfitRate"];
                    if (dTable.Rows[0]["AnnualSales"].Equals(DBNull.Value))
                        objCategoryRates.AnnualSales = 0;
                    else
                        objCategoryRates.AnnualSales = (decimal)dTable.Rows[0]["AnnualSales"];
                    if (dTable.Rows[0]["NetProfit"].Equals(DBNull.Value))
                        objCategoryRates.NetProfit = 0;
                    else
                        objCategoryRates.NetProfit = (decimal)dTable.Rows[0]["NetProfit"];
                    if (dTable.Rows[0]["NetProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.NetProfitRate = 0;
                    else
                        objCategoryRates.NetProfitRate = (double)dTable.Rows[0]["NetProfitRate"];
                    if (dTable.Rows[0]["DriverSalary"].Equals(DBNull.Value))
                        objCategoryRates.DriverSalary = 0;
                    else
                        objCategoryRates.DriverSalary = (decimal)dTable.Rows[0]["DriverSalary"];
                    if (dTable.Rows[0]["AssistantSalary"].Equals(DBNull.Value))
                        objCategoryRates.AssistantSalary = 0;
                    else
                        objCategoryRates.AssistantSalary = (decimal)dTable.Rows[0]["AssistantSalary"];
                    if (dTable.Rows[0]["SalesRate"].Equals(DBNull.Value))
                        objCategoryRates.SalesRate = 0;
                    else
                        objCategoryRates.SalesRate = (double)dTable.Rows[0]["SalesRate"];
                    if (dTable.Rows[0]["TOTCode"].Equals(DBNull.Value))
                        objCategoryRates.TOTCode = string.Empty;
                    else
                        objCategoryRates.TOTCode = (string)dTable.Rows[0]["TOTCode"];
                    if (dTable.Rows[0]["TOTRate"].Equals(DBNull.Value))
                        objCategoryRates.TOTRate = 0;
                    else
                        objCategoryRates.TOTRate = (double)dTable.Rows[0]["TOTRate"];
                    if (dTable.Rows[0]["ExciseCode"].Equals(DBNull.Value))
                        objCategoryRates.ExciseCode = string.Empty;
                    else
                        objCategoryRates.ExciseCode = (string)dTable.Rows[0]["ExciseCode"];
                    if (dTable.Rows[0]["ExciseRate"].Equals(DBNull.Value))
                        objCategoryRates.ExciseRate = 0;
                    else
                        objCategoryRates.ExciseRate = (double)dTable.Rows[0]["ExciseRate"];
                    if (dTable.Rows[0]["MinCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MinCapacity = 0;
                    else
                        objCategoryRates.MinCapacity = (double)dTable.Rows[0]["MinCapacity"];
                    if (dTable.Rows[0]["MaxCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MaxCapacity = 0;
                    else
                        objCategoryRates.MaxCapacity = (double)dTable.Rows[0]["MaxCapacity"];
                    if (dTable.Rows[0]["AppliedDateFrom"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateFrom = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateFrom = (DateTime)dTable.Rows[0]["AppliedDateFrom"];
                        objCategoryRates.AppliedDateFromText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateFrom);
                    }
                    if (dTable.Rows[0]["AppliedDateTo"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateTo = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateTo = (DateTime)dTable.Rows[0]["AppliedDateTo"];
                        objCategoryRates.AppliedDateToText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateTo);
                    }
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objCategoryRates.IsActive = false;
                    else
                        objCategoryRates.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objCategoryRates.IsDeleted = false;
                    else
                        objCategoryRates.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.CreatedUserId = string.Empty;
                    else
                        objCategoryRates.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objCategoryRates.CreatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objCategoryRates.CreatedBy = string.Empty;
                    else
                        objCategoryRates.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedUserId = string.Empty;
                    else
                        objCategoryRates.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedBy = string.Empty;
                    else
                        objCategoryRates.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["MinAge"].Equals(DBNull.Value))
                        objCategoryRates.MinAge = 0;
                    else
                        objCategoryRates.MinAge = (int)dTable.Rows[0]["MinAge"];
                    if (dTable.Rows[0]["MaxAge"].Equals(DBNull.Value))
                        objCategoryRates.MaxAge = 0;
                    else
                        objCategoryRates.MaxAge = (int)dTable.Rows[0]["MaxAge"];
                    if (dTable.Rows[0]["WorkingDays"].Equals(DBNull.Value))
                        objCategoryRates.WorkingDays = 0;
                    else
                        objCategoryRates.WorkingDays = (int)dTable.Rows[0]["WorkingDays"];
                    if (dTable.Rows[0]["Unit"].Equals(DBNull.Value))
                        objCategoryRates.Unit = 0;
                    else
                        objCategoryRates.Unit = (int)dTable.Rows[0]["Unit"];
                    if (dTable.Rows[0]["BudgetYear"].Equals(DBNull.Value))
                        objCategoryRates.BudgetYear = 0;
                    else
                        objCategoryRates.BudgetYear = (int)dTable.Rows[0]["BudgetYear"];
                    if (dTable.Rows[0]["Above15Years"].Equals(DBNull.Value))
                        objCategoryRates.Above15Years = 0;
                    else
                        objCategoryRates.Above15Years = (int)dTable.Rows[0]["Above15Years"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCategoryRates;
        }
        public CategoryRates GetLatestCategoryRate(int id)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CategoryRates objCategoryRates = new CategoryRates();
            string strGetRecord = @" SELECT top(1) [Id],[CategoryId],[GrossProfit],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],
                                   [SalesRate],[TOTCode],[TOTRate],[ExciseCode],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],
                                   [IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[MinAge],[MaxAge],[WorkingDays],[Unit],
                                   [BudgetYear],[Above15Years] FROM [dbo].[CategoryRates] WHERE  Id = @Id and IsNULL(IsDeleted,0)=0 order by [Id] desc";



            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("CategoryRates");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", id));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objCategoryRates.Id = (int)dTable.Rows[0]["Id"];
                    objCategoryRates.CategoryId = (int)dTable.Rows[0]["CategoryId"];
                    if (dTable.Rows[0]["GrossProfit"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfit = 0;
                    else
                        objCategoryRates.GrossProfit = (decimal)dTable.Rows[0]["GrossProfit"];
                    if (dTable.Rows[0]["GrossProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfitRate = 0;
                    else
                        objCategoryRates.GrossProfitRate = (double)dTable.Rows[0]["GrossProfitRate"];
                    if (dTable.Rows[0]["AnnualSales"].Equals(DBNull.Value))
                        objCategoryRates.AnnualSales = 0;
                    else
                        objCategoryRates.AnnualSales = (decimal)dTable.Rows[0]["AnnualSales"];
                    if (dTable.Rows[0]["NetProfit"].Equals(DBNull.Value))
                        objCategoryRates.NetProfit = 0;
                    else
                        objCategoryRates.NetProfit = (decimal)dTable.Rows[0]["NetProfit"];
                    if (dTable.Rows[0]["NetProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.NetProfitRate = 0;
                    else
                        objCategoryRates.NetProfitRate = (double)dTable.Rows[0]["NetProfitRate"];
                    if (dTable.Rows[0]["DriverSalary"].Equals(DBNull.Value))
                        objCategoryRates.DriverSalary = 0;
                    else
                        objCategoryRates.DriverSalary = (decimal)dTable.Rows[0]["DriverSalary"];
                    if (dTable.Rows[0]["AssistantSalary"].Equals(DBNull.Value))
                        objCategoryRates.AssistantSalary = 0;
                    else
                        objCategoryRates.AssistantSalary = (decimal)dTable.Rows[0]["AssistantSalary"];
                    if (dTable.Rows[0]["SalesRate"].Equals(DBNull.Value))
                        objCategoryRates.SalesRate = 0;
                    else
                        objCategoryRates.SalesRate = (double)dTable.Rows[0]["SalesRate"];
                    if (dTable.Rows[0]["TOTCode"].Equals(DBNull.Value))
                        objCategoryRates.TOTCode = string.Empty;
                    else
                        objCategoryRates.TOTCode = (string)dTable.Rows[0]["TOTCode"];
                    if (dTable.Rows[0]["TOTRate"].Equals(DBNull.Value))
                        objCategoryRates.TOTRate = 0;
                    else
                        objCategoryRates.TOTRate = (double)dTable.Rows[0]["TOTRate"];
                    if (dTable.Rows[0]["ExciseCode"].Equals(DBNull.Value))
                        objCategoryRates.ExciseCode = string.Empty;
                    else
                        objCategoryRates.ExciseCode = (string)dTable.Rows[0]["ExciseCode"];
                    if (dTable.Rows[0]["ExciseRate"].Equals(DBNull.Value))
                        objCategoryRates.ExciseRate = 0;
                    else
                        objCategoryRates.ExciseRate = (double)dTable.Rows[0]["ExciseRate"];
                    if (dTable.Rows[0]["MinCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MinCapacity = 0;
                    else
                        objCategoryRates.MinCapacity = (double)dTable.Rows[0]["MinCapacity"];
                    if (dTable.Rows[0]["MaxCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MaxCapacity = 0;
                    else
                        objCategoryRates.MaxCapacity = (double)dTable.Rows[0]["MaxCapacity"];
                    if (dTable.Rows[0]["AppliedDateFrom"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateFrom = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateFrom = (DateTime)dTable.Rows[0]["AppliedDateFrom"];
                        objCategoryRates.AppliedDateFromText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateFrom);
                    }
                    if (dTable.Rows[0]["AppliedDateTo"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateTo = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateTo = (DateTime)dTable.Rows[0]["AppliedDateTo"];
                        objCategoryRates.AppliedDateToText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateTo);
                    }
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objCategoryRates.IsActive = false;
                    else
                        objCategoryRates.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objCategoryRates.IsDeleted = false;
                    else
                        objCategoryRates.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.CreatedUserId = string.Empty;
                    else
                        objCategoryRates.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objCategoryRates.CreatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objCategoryRates.CreatedBy = string.Empty;
                    else
                        objCategoryRates.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedUserId = string.Empty;
                    else
                        objCategoryRates.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedBy = string.Empty;
                    else
                        objCategoryRates.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["MinAge"].Equals(DBNull.Value))
                        objCategoryRates.MinAge = 0;
                    else
                        objCategoryRates.MinAge = (int)dTable.Rows[0]["MinAge"];
                    if (dTable.Rows[0]["MaxAge"].Equals(DBNull.Value))
                        objCategoryRates.MaxAge = 0;
                    else
                        objCategoryRates.MaxAge = (int)dTable.Rows[0]["MaxAge"];
                    if (dTable.Rows[0]["WorkingDays"].Equals(DBNull.Value))
                        objCategoryRates.WorkingDays = 0;
                    else
                        objCategoryRates.WorkingDays = (int)dTable.Rows[0]["WorkingDays"];
                    if (dTable.Rows[0]["Unit"].Equals(DBNull.Value))
                        objCategoryRates.Unit = 0;
                    else
                        objCategoryRates.Unit = (int)dTable.Rows[0]["Unit"];
                    if (dTable.Rows[0]["BudgetYear"].Equals(DBNull.Value))
                        objCategoryRates.BudgetYear = 0;
                    else
                        objCategoryRates.BudgetYear = (int)dTable.Rows[0]["BudgetYear"];
                    if (dTable.Rows[0]["Above15Years"].Equals(DBNull.Value))
                        objCategoryRates.Above15Years = 0;
                    else
                        objCategoryRates.Above15Years = (int)dTable.Rows[0]["Above15Years"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCategoryRates;
        }

        public bool Insert(CategoryRates objCategoryRates)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[CategoryRates]
                                            ([CategoryId],[GrossProfit],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],[SalesRate],[TOTCode],[TOTRate],[ExciseCode],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[MinAge],[MaxAge],[WorkingDays],[Unit],[BudgetYear])
                                     VALUES    (@CategoryId,@GrossProfit,@GrossProfitRate,@AnnualSales,@NetProfit,@NetProfitRate,@DriverSalary,@AssistantSalary,@SalesRate,@TOTCode,@TOTRate,@ExciseCode,@ExciseRate,@MinCapacity,@MaxCapacity,@AppliedDateFrom, @AppliedDateTo, @IsActive,@IsDeleted,@CreatedUserId,@CreatedDate,@CreatedBy,@MinAge,@MaxAge,@WorkingDays,@Unit,@BudgetYear)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CategoryId", objCategoryRates.CategoryId));
                if(objCategoryRates.GrossProfit == 0)
                  command.Parameters.Add(new SqlParameter("@GrossProfit", DBNull.Value));
                else
                   command.Parameters.Add(new SqlParameter("@GrossProfit", objCategoryRates.GrossProfit));

                if(objCategoryRates.GrossProfitRate == 0)
                command.Parameters.Add(new SqlParameter("@GrossProfitRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@GrossProfitRate", objCategoryRates.GrossProfitRate));

                if(objCategoryRates.AnnualSales == 0)
                command.Parameters.Add(new SqlParameter("@AnnualSales", objCategoryRates.AnnualSales));
                else
                    command.Parameters.Add(new SqlParameter("@AnnualSales", DBNull.Value));

                if(objCategoryRates.NetProfit==0)
                command.Parameters.Add(new SqlParameter("@NetProfit", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@NetProfit", objCategoryRates.NetProfit));

                if(objCategoryRates.NetProfitRate==0)
                command.Parameters.Add(new SqlParameter("@NetProfitRate", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@NetProfitRate", objCategoryRates.NetProfitRate));

                if(objCategoryRates.DriverSalary==0)
                command.Parameters.Add(new SqlParameter("@DriverSalary", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@DriverSalary", objCategoryRates.DriverSalary));

                if(objCategoryRates.AssistantSalary==0)
                command.Parameters.Add(new SqlParameter("@AssistantSalary", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@AssistantSalary", objCategoryRates.AssistantSalary));

                if(objCategoryRates.SalesRate==0)
                    command.Parameters.Add(new SqlParameter("@SalesRate", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@SalesRate", objCategoryRates.SalesRate));

                if(objCategoryRates.TOTCode==string.Empty)
                command.Parameters.Add(new SqlParameter("@TOTCode", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@TOTCode", objCategoryRates.TOTCode));

                if (objCategoryRates.TOTRate == 0)
                    command.Parameters.Add(new SqlParameter("@TOTRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@TOTRate", objCategoryRates.TOTRate));

                if(objCategoryRates.ExciseCode==string.Empty)
                command.Parameters.Add(new SqlParameter("@ExciseCode", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@ExciseCode", objCategoryRates.ExciseCode));

                if(objCategoryRates.ExciseRate==0)
                command.Parameters.Add(new SqlParameter("@ExciseRate", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@ExciseRate", objCategoryRates.ExciseRate));

                if(objCategoryRates.MinCapacity==0)
                command.Parameters.Add(new SqlParameter("@MinCapacity", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@MinCapacity", objCategoryRates.MinCapacity));

                if(objCategoryRates.MaxCapacity == 0)
                command.Parameters.Add(new SqlParameter("@MaxCapacity", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@MaxCapacity", objCategoryRates.MaxCapacity));

                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objCategoryRates.AppliedDateFrom));


                if (objCategoryRates.AppliedDateTo != DateTime.MinValue)
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", objCategoryRates.AppliedDateTo));
                else
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@IsActive", objCategoryRates.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objCategoryRates.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objCategoryRates.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objCategoryRates.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objCategoryRates.CreatedBy));
                
                if (objCategoryRates.MinAge ==0)
                command.Parameters.Add(new SqlParameter("@MinAge", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@MinAge", objCategoryRates.MinAge));

                if(objCategoryRates.MaxAge==0)
                command.Parameters.Add(new SqlParameter("@MaxAge", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@MaxAge", objCategoryRates.MaxAge));

                if(objCategoryRates.WorkingDays ==0)
                command.Parameters.Add(new SqlParameter("@WorkingDays", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@WorkingDays", objCategoryRates.WorkingDays));

                if(objCategoryRates.Unit==0)
                command.Parameters.Add(new SqlParameter("@Unit", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@Unit", objCategoryRates.Unit));

                command.Parameters.Add(new SqlParameter("@BudgetYear", objCategoryRates.BudgetYear));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(CategoryRates objCategoryRates)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[CategoryRates] SET        [GrossProfit]=@GrossProfit,    [GrossProfitRate]=@GrossProfitRate,    [AnnualSales]=@AnnualSales, 
                               [NetProfit]=@NetProfit,    [NetProfitRate]=@NetProfitRate,    [DriverSalary]=@DriverSalary,    [AssistantSalary]=@AssistantSalary,    
                               [SalesRate]=@SalesRate,    [TOTCode]=@TOTCode,    [TOTRate]=@TOTRate,    [ExciseCode]=@ExciseCode,    [ExciseRate]=@ExciseRate,    
                               [MinCapacity]=@MinCapacity,    [MaxCapacity]=@MaxCapacity,    [AppliedDateFrom]=@AppliedDateFrom, [AppliedDateTo]=@AppliedDateTo,    [IsActive]=@IsActive,    [IsDeleted]=@IsDeleted,        [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy,    [MinAge]=@MinAge,    [MaxAge]=@MaxAge,    [WorkingDays]=@WorkingDays,    [Unit]=@Unit, [BudgetYear]=@BudgetYear WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objCategoryRates.Id));
                if (objCategoryRates.GrossProfit == 0)
                    command.Parameters.Add(new SqlParameter("@GrossProfit", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@GrossProfit", objCategoryRates.GrossProfit));

                if (objCategoryRates.GrossProfitRate == 0)
                    command.Parameters.Add(new SqlParameter("@GrossProfitRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@GrossProfitRate", objCategoryRates.GrossProfitRate));

                if (objCategoryRates.AnnualSales == 0)
                    command.Parameters.Add(new SqlParameter("@AnnualSales",DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@AnnualSales", objCategoryRates.AnnualSales));

                if (objCategoryRates.NetProfit == 0)
                    command.Parameters.Add(new SqlParameter("@NetProfit", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@NetProfit", objCategoryRates.NetProfit));

                if (objCategoryRates.NetProfitRate == 0)
                    command.Parameters.Add(new SqlParameter("@NetProfitRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@NetProfitRate", objCategoryRates.NetProfitRate));

                if (objCategoryRates.DriverSalary == 0)
                    command.Parameters.Add(new SqlParameter("@DriverSalary", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@DriverSalary", objCategoryRates.DriverSalary));

                if (objCategoryRates.AssistantSalary == 0)
                    command.Parameters.Add(new SqlParameter("@AssistantSalary", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@AssistantSalary", objCategoryRates.AssistantSalary));

                if (objCategoryRates.SalesRate == 0)
                    command.Parameters.Add(new SqlParameter("@SalesRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@SalesRate", objCategoryRates.SalesRate));

                if (objCategoryRates.TOTCode == string.Empty)
                    command.Parameters.Add(new SqlParameter("@TOTCode", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@TOTCode", objCategoryRates.TOTCode));

                if (objCategoryRates.TOTRate == 0)
                    command.Parameters.Add(new SqlParameter("@TOTRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@TOTRate", objCategoryRates.TOTRate));

                if (objCategoryRates.ExciseCode == string.Empty)
                    command.Parameters.Add(new SqlParameter("@ExciseCode", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@ExciseCode", objCategoryRates.ExciseCode));

                if (objCategoryRates.ExciseRate == 0)
                    command.Parameters.Add(new SqlParameter("@ExciseRate", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@ExciseRate", objCategoryRates.ExciseRate));

                if (objCategoryRates.MinCapacity == 0)
                    command.Parameters.Add(new SqlParameter("@MinCapacity", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@MinCapacity", objCategoryRates.MinCapacity));

                if (objCategoryRates.MaxCapacity == 0)
                    command.Parameters.Add(new SqlParameter("@MaxCapacity", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@MaxCapacity", objCategoryRates.MaxCapacity));

                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objCategoryRates.AppliedDateFrom));

                if(objCategoryRates.AppliedDateTo != DateTime.MinValue)
                command.Parameters.Add(new SqlParameter("@AppliedDateTo", objCategoryRates.AppliedDateTo));
                else
                command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));

               

                command.Parameters.Add(new SqlParameter("@IsActive", objCategoryRates.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objCategoryRates.IsDeleted));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objCategoryRates.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objCategoryRates.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objCategoryRates.UpdatedBy));

                if (objCategoryRates.MinAge == 0)
                    command.Parameters.Add(new SqlParameter("@MinAge", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@MinAge", objCategoryRates.MinAge));

                if (objCategoryRates.MaxAge == 0)
                    command.Parameters.Add(new SqlParameter("@MaxAge", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@MaxAge", objCategoryRates.MaxAge));

                if (objCategoryRates.WorkingDays == 0)
                    command.Parameters.Add(new SqlParameter("@WorkingDays", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@WorkingDays", objCategoryRates.WorkingDays));

                if (objCategoryRates.Unit == 0)
                    command.Parameters.Add(new SqlParameter("@Unit", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@Unit", objCategoryRates.Unit));

                command.Parameters.Add(new SqlParameter("@BudgetYear", objCategoryRates.BudgetYear));



                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateClosingDate(CategoryRates objCategoryRates)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[CategoryRates] SET    [AppliedDateTo]=@AppliedDateTo,    [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objCategoryRates.Id));
                command.Parameters.Add(new SqlParameter("@AppliedDateTo", objCategoryRates.AppliedDateTo));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objCategoryRates.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objCategoryRates.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objCategoryRates.UpdatedBy));
               
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[CategoryRates] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<CategoryRates> GetList(int CategoryId)
        {
            List<CategoryRates> RecordsList = new List<CategoryRates>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CategoryId],[GrossProfit],[GrossProfitRate],[AnnualSales],[NetProfit],[NetProfitRate],[DriverSalary],[AssistantSalary],[SalesRate],[TOTCode],[TOTRate],[ExciseCode],[ExciseRate],[MinCapacity],[MaxCapacity],[AppliedDateFrom],[AppliedDateTo],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[MinAge],[MaxAge],[WorkingDays],[Unit],[BudgetYear] FROM [dbo].[CategoryRates]  
                                        where  [CategoryId] = @CateoryId   ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@CateoryId", CategoryId);
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryRates objCategoryRates = new CategoryRates();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategoryRates.Id = 0;
                    else
                        objCategoryRates.Id = (int)dr["Id"];
                    if (dr["CategoryId"].Equals(DBNull.Value))
                        objCategoryRates.CategoryId = 0;
                    else
                        objCategoryRates.CategoryId = (int)dr["CategoryId"];
                    if (dr["GrossProfit"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfit = 0;
                    else
                        objCategoryRates.GrossProfit = (decimal)dr["GrossProfit"];
                    if (dr["GrossProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.GrossProfitRate = 0;
                    else
                        objCategoryRates.GrossProfitRate = (double)dr["GrossProfitRate"];
                    if (dr["AnnualSales"].Equals(DBNull.Value))
                        objCategoryRates.AnnualSales = 0;
                    else
                        objCategoryRates.AnnualSales = (decimal)dr["AnnualSales"];
                    if (dr["NetProfit"].Equals(DBNull.Value))
                        objCategoryRates.NetProfit = 0;
                    else
                        objCategoryRates.NetProfit = (decimal)dr["NetProfit"];
                    if (dr["NetProfitRate"].Equals(DBNull.Value))
                        objCategoryRates.NetProfitRate = 0;
                    else
                        objCategoryRates.NetProfitRate = (double)dr["NetProfitRate"];
                    if (dr["DriverSalary"].Equals(DBNull.Value))
                        objCategoryRates.DriverSalary = 0;
                    else
                        objCategoryRates.DriverSalary = (decimal)dr["DriverSalary"];
                    if (dr["AssistantSalary"].Equals(DBNull.Value))
                        objCategoryRates.AssistantSalary = 0;
                    else
                        objCategoryRates.AssistantSalary = (decimal)dr["AssistantSalary"];
                    if (dr["SalesRate"].Equals(DBNull.Value))
                        objCategoryRates.SalesRate = 0;
                    else
                        objCategoryRates.SalesRate = (double)dr["SalesRate"];
                    if (dr["TOTCode"].Equals(DBNull.Value))
                        objCategoryRates.TOTCode = string.Empty;
                    else
                        objCategoryRates.TOTCode = (string)dr["TOTCode"];
                    if (dr["TOTRate"].Equals(DBNull.Value))
                        objCategoryRates.TOTRate = 0;
                    else
                        objCategoryRates.TOTRate = (double)dr["TOTRate"];
                    if (dr["ExciseCode"].Equals(DBNull.Value))
                        objCategoryRates.ExciseCode = string.Empty;
                    else
                        objCategoryRates.ExciseCode = (string)dr["ExciseCode"];
                    if (dr["ExciseRate"].Equals(DBNull.Value))
                        objCategoryRates.ExciseRate = 0;
                    else
                        objCategoryRates.ExciseRate = (double)dr["ExciseRate"];
                    if (dr["MinCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MinCapacity = 0;
                    else
                        objCategoryRates.MinCapacity = (double)dr["MinCapacity"];
                    if (dr["MaxCapacity"].Equals(DBNull.Value))
                        objCategoryRates.MaxCapacity = 0;
                    else
                        objCategoryRates.MaxCapacity = (double)dr["MaxCapacity"];
                    if (dr["AppliedDateFrom"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateFrom = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateFrom = (DateTime)dr["AppliedDateFrom"];
                        objCategoryRates.AppliedDateFromText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateFrom);

                    }
                    if (dr["AppliedDateTo"].Equals(DBNull.Value))
                        objCategoryRates.AppliedDateTo = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.AppliedDateTo = (DateTime)dr["AppliedDateTo"];
                        objCategoryRates.AppliedDateToText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.AppliedDateTo);

                    }
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objCategoryRates.IsActive = false;
                    else
                        objCategoryRates.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objCategoryRates.IsDeleted = false;
                    else
                        objCategoryRates.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.CreatedUserId  = string.Empty;
                    else
                        objCategoryRates.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objCategoryRates.CreatedDate = DateTime.MinValue;
                    else
                    {
                        objCategoryRates.CreatedDate = (DateTime)dr["CreatedDate"];
                        objCategoryRates.CreatedDateText = EthiopicDateTime.GetEthiopianDate(objCategoryRates.CreatedDate);
                    }
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objCategoryRates.CreatedBy = string.Empty;
                    else
                        objCategoryRates.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedUserId = string.Empty;
                    else
                        objCategoryRates.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedDate = DateTime.MinValue;
                    else
                        objCategoryRates.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objCategoryRates.UpdatedBy = string.Empty;
                    else
                        objCategoryRates.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["MinAge"].Equals(DBNull.Value))
                        objCategoryRates.MinAge = 0;
                    else
                        objCategoryRates.MinAge = (int)dr["MinAge"];
                    if (dr["MaxAge"].Equals(DBNull.Value))
                        objCategoryRates.MaxAge = 0;
                    else
                        objCategoryRates.MaxAge = (int)dr["MaxAge"];
                    if (dr["WorkingDays"].Equals(DBNull.Value))
                        objCategoryRates.WorkingDays = 0;
                    else
                        objCategoryRates.WorkingDays = (int)dr["WorkingDays"];
                    if (dr["Unit"].Equals(DBNull.Value))
                        objCategoryRates.Unit = 0;
                    else
                        objCategoryRates.Unit = (int)dr["Unit"];
                    if (dr["BudgetYear"].Equals(DBNull.Value))
                        objCategoryRates.BudgetYear = 0;
                    else
                        objCategoryRates.BudgetYear = (int)dr["BudgetYear"];
                    RecordsList.Add(objCategoryRates);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[CategoryRates] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CategoryRates::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}