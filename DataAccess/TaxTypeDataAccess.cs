﻿using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CUSTOR.DataAccess
{
    public class TaxTypeDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[TaxTypeName],[TaxTypeNameEng],[DueDate],[DateFrom],[DateTo],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[TaxType]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("TaxType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

      

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[TaxType] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxType::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<Domain.TaxType> GetList(TaxCategory taxCategory, int lanId)
        {
            List<Domain.TaxType> RecordsList = new List<Domain.TaxType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],  [TaxTypeName],[TaxTypeNameEng] FROM [dbo].[TaxType] where taxcategory = @taxcategory ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@taxcategory", (int)taxCategory);
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Domain.TaxType objTaxType = new Domain.TaxType();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxType.Id = 0;
                    else
                        objTaxType.Id = (int)dr["Id"];

                    if(lanId == (int)Language.eLanguage.eAmharic)
                    {
                        objTaxType.TaxTypeName = (string)dr["TaxTypeName"];
                    }
                    else if (lanId == (int)Language.eLanguage.eEnglish)
                    {
                        objTaxType.TaxTypeName = (string)dr["TaxTypeNameEng"];
                    }
                    
                    RecordsList.Add(objTaxType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        //gets execise, profit, and turnover taxes only
        public List<Domain.TaxType> GetServiceTaxTypes()
        {
            List<Domain.TaxType> RecordsList = new List<Domain.TaxType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],  [TaxTypeName],[TaxTypeNameEng] FROM [dbo].[TaxType] where Id in (2,3,4) ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Domain.TaxType objTaxType = new Domain.TaxType();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxType.Id = 0;
                    else
                        objTaxType.Id = (int)dr["Id"];
                        objTaxType.TaxTypeName = (string)dr["TaxTypeNameEng"];
                    RecordsList.Add(objTaxType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[TaxType] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}
