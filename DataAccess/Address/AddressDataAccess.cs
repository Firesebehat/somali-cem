﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class AddressDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox],[Fax],[Email],[ResidenceCountry],[Username],[EventDateTime],[UpdatedUsername],[UpdatedEventDatetime],[OwnerId] FROM [dbo].[Address]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Address");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Address::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Address GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Address objAddress = new Address();
            string strGetRecord = @"SELECT [Id],[IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox],[Fax],[Email],[ResidenceCountry],[Username],[EventDateTime],[UpdatedUsername],[UpdatedEventDatetime],[OtherAddress] FROM [dbo].[Address] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Address");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAddress.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["IsMainOffice"].Equals(DBNull.Value))
                        objAddress.IsMainOffice = false;
                    else
                        objAddress.IsMainOffice = (bool)dTable.Rows[0]["IsMainOffice"];
                    if (dTable.Rows[0]["BranchName"].Equals(DBNull.Value))
                        objAddress.BranchName = string.Empty;
                    else
                        objAddress.BranchName = (string)dTable.Rows[0]["BranchName"];
                    objAddress.AddressType = (int)dTable.Rows[0]["AddressType"];
                    objAddress.Region = (string)dTable.Rows[0]["Region"];
                    if (dTable.Rows[0]["City"].Equals(DBNull.Value))
                        objAddress.City = string.Empty;
                    else
                        objAddress.City = (string)dTable.Rows[0]["City"];
                    objAddress.Zone = (string)dTable.Rows[0]["Zone"];
                    objAddress.Wereda = (string)dTable.Rows[0]["Wereda"];
                    objAddress.Kebele = (string)dTable.Rows[0]["Kebele"];
                    if (dTable.Rows[0]["HouseNo"].Equals(DBNull.Value))
                        objAddress.HouseNo = string.Empty;
                    else
                        objAddress.HouseNo = (string)dTable.Rows[0]["HouseNo"];
                    if (dTable.Rows[0]["TeleNo"].Equals(DBNull.Value))
                        objAddress.TeleNo = string.Empty;
                    else
                        objAddress.TeleNo = (string)dTable.Rows[0]["TeleNo"];
                    if (dTable.Rows[0]["POBox"].Equals(DBNull.Value))
                        objAddress.POBox = string.Empty;
                    else
                        objAddress.POBox = (string)dTable.Rows[0]["POBox"];
                    if (dTable.Rows[0]["Fax"].Equals(DBNull.Value))
                        objAddress.Fax = string.Empty;
                    else
                        objAddress.Fax = (string)dTable.Rows[0]["Fax"];
                    if (dTable.Rows[0]["Email"].Equals(DBNull.Value))
                        objAddress.Email = string.Empty;
                    else
                        objAddress.Email = (string)dTable.Rows[0]["Email"];
                    if (dTable.Rows[0]["ResidenceCountry"].Equals(DBNull.Value))
                        objAddress.ResidenceCountry = string.Empty;
                    else
                        objAddress.ResidenceCountry = (string)dTable.Rows[0]["ResidenceCountry"];
                    if (dTable.Rows[0]["OtherAddress"].Equals(DBNull.Value))
                        objAddress.OtherAddress = string.Empty;
                    else
                        objAddress.OtherAddress = (string)dTable.Rows[0]["OtherAddress"];
                    if (dTable.Rows[0]["Username"].Equals(DBNull.Value))
                        objAddress.Username = string.Empty;
                    else
                        objAddress.Username = (string)dTable.Rows[0]["Username"];
                    if (dTable.Rows[0]["EventDateTime"].Equals(DBNull.Value))
                        objAddress.EventDateTime = DateTime.MinValue;
                    else
                        objAddress.EventDateTime = (DateTime)dTable.Rows[0]["EventDateTime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objAddress.UpdatedUsername = string.Empty;
                    else
                        objAddress.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objAddress.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objAddress.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Address::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAddress;
        }
        public Address GetAddresssByCustomerId(int addressId, int addressType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Address objAddress = new Address();
            string strGetRecord = @"SELECT [Id],[IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox],[Fax],[Email],
                                    Remark,OtherAddress,[ResidenceCountry],[Username],[EventDateTime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[Address] 
                                    WHERE [Id]=@addressId and AddressType=@AddressType";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Address");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@addressId", addressId));
                command.Parameters.Add(new SqlParameter("@addressType", addressType));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAddress.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["IsMainOffice"].Equals(DBNull.Value))
                        objAddress.IsMainOffice = false;
                    else
                        objAddress.IsMainOffice = (bool)dTable.Rows[0]["IsMainOffice"];
                    if (dTable.Rows[0]["BranchName"].Equals(DBNull.Value))
                        objAddress.BranchName = string.Empty;
                    else
                        objAddress.BranchName = (string)dTable.Rows[0]["BranchName"];
                    objAddress.AddressType = (int)dTable.Rows[0]["AddressType"];
                    objAddress.Region = (string)dTable.Rows[0]["Region"];
                    if (dTable.Rows[0]["City"].Equals(DBNull.Value))
                        objAddress.City = string.Empty;
                    else
                        objAddress.City = (string)dTable.Rows[0]["City"];
                    objAddress.Zone = (string)dTable.Rows[0]["Zone"];
                    objAddress.Wereda = (string)dTable.Rows[0]["Wereda"];
                    objAddress.Kebele = (string)dTable.Rows[0]["Kebele"];
                    if (dTable.Rows[0]["HouseNo"].Equals(DBNull.Value))
                        objAddress.HouseNo = string.Empty;
                    else
                        objAddress.HouseNo = (string)dTable.Rows[0]["HouseNo"];
                    if (dTable.Rows[0]["TeleNo"].Equals(DBNull.Value))
                        objAddress.TeleNo = string.Empty;
                    else
                        objAddress.TeleNo = (string)dTable.Rows[0]["TeleNo"];
                    if (dTable.Rows[0]["POBox"].Equals(DBNull.Value))
                        objAddress.POBox = string.Empty;
                    else
                        objAddress.POBox = (string)dTable.Rows[0]["POBox"];
                    if (dTable.Rows[0]["Fax"].Equals(DBNull.Value))
                        objAddress.Fax = string.Empty;
                    else
                        objAddress.Fax = (string)dTable.Rows[0]["Fax"];
                    if (dTable.Rows[0]["Email"].Equals(DBNull.Value))
                        objAddress.Email = string.Empty;
                    else
                        objAddress.Email = (string)dTable.Rows[0]["Email"];
                    if (dTable.Rows[0]["ResidenceCountry"].Equals(DBNull.Value))
                        objAddress.ResidenceCountry = string.Empty;
                    else
                        objAddress.ResidenceCountry = (string)dTable.Rows[0]["ResidenceCountry"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objAddress.Remark = string.Empty;
                    else
                        objAddress.Remark = (string)dTable.Rows[0]["Remark"];
                    if (dTable.Rows[0]["OtherAddress"].Equals(DBNull.Value))
                        objAddress.OtherAddress = string.Empty;
                    else
                        objAddress.OtherAddress = (string)dTable.Rows[0]["OtherAddress"];
                    if (dTable.Rows[0]["Username"].Equals(DBNull.Value))
                        objAddress.Username = string.Empty;
                    else
                        objAddress.Username = (string)dTable.Rows[0]["Username"];
                    if (dTable.Rows[0]["EventDateTime"].Equals(DBNull.Value))
                        objAddress.EventDateTime = DateTime.MinValue;
                    else
                        objAddress.EventDateTime = (DateTime)dTable.Rows[0]["EventDateTime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objAddress.UpdatedUsername = string.Empty;
                    else
                        objAddress.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objAddress.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objAddress.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Address::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAddress;
        }

        public bool Insert(Address objAddress)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            bool inserted = false;
            string strInsert = @"INSERT INTO [dbo].[Address]
                                            ([IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox], [Fax], [Email],[ResidenceCountry],[OtherAddress],[Username],[EventDateTime]) OUTPUT INSERTED.ID 
                                     VALUES (@IsMainOffice, @BranchName, @AddressType,  @Region, @City, @Zone, @Wereda,@Kebele, @HouseNo, @TeleNo, @POBox,  @Fax,  @Email, @ResidenceCountry, @OtherAddress, @Username, @EventDateTime)";
            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@IsMainOffice", objAddress.IsMainOffice));
                command.Parameters.Add(new SqlParameter("@BranchName", objAddress.BranchName));
                command.Parameters.Add(new SqlParameter("@AddressType", objAddress.AddressType));
                command.Parameters.Add(new SqlParameter("@Region", objAddress.Region));
                command.Parameters.Add(new SqlParameter("@City", objAddress.City));
                command.Parameters.Add(new SqlParameter("@Zone", objAddress.Zone));
                command.Parameters.Add(new SqlParameter("@Wereda", objAddress.Wereda));
                command.Parameters.Add(new SqlParameter("@Kebele", objAddress.Kebele));
                command.Parameters.Add(new SqlParameter("@HouseNo", objAddress.HouseNo));
                command.Parameters.Add(new SqlParameter("@TeleNo", objAddress.TeleNo));
                command.Parameters.Add(new SqlParameter("@POBox", objAddress.POBox));
                command.Parameters.Add(new SqlParameter("@Fax", objAddress.Fax));
                command.Parameters.Add(new SqlParameter("@Email", objAddress.Email));
               // command.Parameters.Add(new SqlParameter("@Remark", objAddress.Remark));
                command.Parameters.Add(new SqlParameter("@OtherAddress", objAddress.OtherAddress));
                command.Parameters.Add(new SqlParameter("@ResidenceCountry", objAddress.ResidenceCountry));
               // command.Parameters.Add(new SqlParameter("@IsDeleted", objAddress.isDeleted));
                command.Parameters.Add(new SqlParameter("@Username", objAddress.Username));
                command.Parameters.Add(new SqlParameter("@EventDateTime", objAddress.EventDateTime));

                connection.Open();
                objAddress.Id = (int)command.ExecuteScalar();
                if (objAddress.Id > 0)
                {
                    inserted = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return inserted;
        }
      
        public int Insert(Address objAddress,SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"DECLARE @Q AS TABLE(ID INT);INSERT INTO [dbo].[Address]
                                            ([IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox],[Fax],[Email],[OtherAddress],[Username],[EventDateTime]) OUTPUT  INSERTED.Id  INTO @Q
                                     VALUES    (@IsMainOffice,@BranchName,@AddressType,@Region,@City,@Zone,@Wereda,@Kebele,@HouseNo,@TeleNo,@POBox,@Fax,@Email,@OtherAddress,@Username,@EventDateTime) SELECT * FROM @Q";

            command.Parameters.Clear();
            command.CommandText = strInsert;
            try
            {
                if(!objAddress.IsMainOffice)
                command.Parameters.Add(new SqlParameter("@IsMainOffice", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@IsMainOffice", objAddress.IsMainOffice));

                if(objAddress.BranchName == null)
                command.Parameters.Add(new SqlParameter("@BranchName", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@BranchName", objAddress.BranchName));

                command.Parameters.Add(new SqlParameter("@AddressType", objAddress.AddressType));

                command.Parameters.Add(new SqlParameter("@Region", objAddress.Region));
                if(objAddress.City==null)
                command.Parameters.Add(new SqlParameter("@City", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@City", objAddress.City));

                command.Parameters.Add(new SqlParameter("@Zone", objAddress.Zone));
                command.Parameters.Add(new SqlParameter("@Wereda", objAddress.Wereda));
                command.Parameters.Add(new SqlParameter("@Kebele", objAddress.Kebele));
                command.Parameters.Add(new SqlParameter("@HouseNo", objAddress.HouseNo));
                command.Parameters.Add(new SqlParameter("@TeleNo", objAddress.TeleNo));

                if(objAddress.POBox == null)
                command.Parameters.Add(new SqlParameter("@POBox", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@POBox", objAddress.POBox));

                if(objAddress.Fax ==null)
                command.Parameters.Add(new SqlParameter("@Fax", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@Fax", objAddress.Fax));

                if(objAddress.Email == null)
                command.Parameters.Add(new SqlParameter("@Email", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@Email", objAddress.Email));

                if(objAddress.OtherAddress == null)
                command.Parameters.Add(new SqlParameter("@OtherAddress", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@OtherAddress", objAddress.OtherAddress));

                command.Parameters.Add(new SqlParameter("@Username", objAddress.Username));
                command.Parameters.Add(new SqlParameter("@EventDateTime", objAddress.EventDateTime));

                int addressId = (int)command.ExecuteScalar();
                return addressId;
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
            }
        }
        public bool Update(Address objAddress)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Address] SET [IsMainOffice]=@IsMainOffice,    [BranchName]=@BranchName,    [AddressType]=@AddressType,    [Region]=@Region,    [City]=@City,    [Zone]=@Zone,    [Wereda]=@Wereda,    [Kebele]=@Kebele,    [HouseNo]=@HouseNo,    [TeleNo]=@TeleNo,    [POBox]=@POBox,    [Fax]=@Fax,    [Email]=@Email,    [ResidenceCountry]=@ResidenceCountry, OtherAddress=@OtherAddress, [UpdatedUsername]=@UpdatedUsername, [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objAddress.Id));
                command.Parameters.Add(new SqlParameter("@IsMainOffice", objAddress.IsMainOffice));
                command.Parameters.Add(new SqlParameter("@BranchName", objAddress.BranchName));
                command.Parameters.Add(new SqlParameter("@AddressType", objAddress.AddressType));
                command.Parameters.Add(new SqlParameter("@Region", objAddress.Region));
                command.Parameters.Add(new SqlParameter("@City", objAddress.City));
                command.Parameters.Add(new SqlParameter("@Zone", objAddress.Zone));
                command.Parameters.Add(new SqlParameter("@Wereda", objAddress.Wereda));
                command.Parameters.Add(new SqlParameter("@Kebele", objAddress.Kebele));
                command.Parameters.Add(new SqlParameter("@HouseNo", objAddress.HouseNo));
                command.Parameters.Add(new SqlParameter("@TeleNo", objAddress.TeleNo));
                command.Parameters.Add(new SqlParameter("@POBox", objAddress.POBox));
                command.Parameters.Add(new SqlParameter("@Fax", objAddress.Fax));
                command.Parameters.Add(new SqlParameter("@Email", objAddress.Email));
                //command.Parameters.Add(new SqlParameter("@Remark", objAddress.Remark));
                command.Parameters.Add(new SqlParameter("@OtherAddress", objAddress.OtherAddress));
                command.Parameters.Add(new SqlParameter("@ResidenceCountry", objAddress.ResidenceCountry));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objAddress.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objAddress.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Update(Address objAddress, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Address] SET     [IsMainOffice]=@IsMainOffice,    [BranchName]=@BranchName,    [AddressType]=@AddressType,    [Region]=@Region,    [City]=@City,    [Zone]=@Zone,    [Wereda]=@Wereda,    [Kebele]=@Kebele,    [HouseNo]=@HouseNo,    [TeleNo]=@TeleNo,    [POBox]=@POBox,    [Fax]=@Fax,    [Email]=@Email,[ResidenceCountry]=@ResidenceCountry,    [OtherAddress]=@OtherAddress,  [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [Id]=@Id";

            command.CommandText = strUpdate;
            command.Parameters.Clear();
            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objAddress.Id));
                if(objAddress.IsMainOffice==false)
                command.Parameters.Add(new SqlParameter("@IsMainOffice", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@IsMainOffice", objAddress.IsMainOffice));
                if (objAddress.BranchName==null)
                command.Parameters.Add(new SqlParameter("@BranchName",DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@BranchName", objAddress.BranchName));

                command.Parameters.Add(new SqlParameter("@AddressType", objAddress.AddressType));
                command.Parameters.Add(new SqlParameter("@Region", objAddress.Region));

                if(objAddress.City == null)
                command.Parameters.Add(new SqlParameter("@City", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@City", objAddress.City));

                command.Parameters.Add(new SqlParameter("@Zone", objAddress.Zone));
                command.Parameters.Add(new SqlParameter("@Wereda", objAddress.Wereda));
                command.Parameters.Add(new SqlParameter("@Kebele", objAddress.Kebele));
                command.Parameters.Add(new SqlParameter("@HouseNo", objAddress.HouseNo));
                command.Parameters.Add(new SqlParameter("@TeleNo", objAddress.TeleNo));

                if(objAddress.POBox == null)
                command.Parameters.Add(new SqlParameter("@POBox", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@POBox", objAddress.POBox));

                if(objAddress.Fax == null)
                command.Parameters.Add(new SqlParameter("@Fax", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@Fax", objAddress.Fax));

                if(objAddress.Email == null)
                command.Parameters.Add(new SqlParameter("@Email", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@Email", objAddress.Email));

                if (objAddress.ResidenceCountry == null)
                    command.Parameters.Add(new SqlParameter("@ResidenceCountry", DBNull.Value));
                else
                    command.Parameters.Add(new SqlParameter("@ResidenceCountry", objAddress.ResidenceCountry));

                if (objAddress.OtherAddress==null)
                command.Parameters.Add(new SqlParameter("@OtherAddress", DBNull.Value));
                else
                command.Parameters.Add(new SqlParameter("@OtherAddress", objAddress.OtherAddress));

                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objAddress.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objAddress.UpdatedEventDatetime));

                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Update::Error!" + ex.Message, ex);
            }
            
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Address] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Address> GetList()
        {
            List<Address> RecordsList = new List<Address>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[IsMainOffice],[BranchName],[AddressType],[Region],[City],[Zone],[Wereda],[Kebele],[HouseNo],[TeleNo],[POBox],[Fax],[Email],[ResidenceCountry],[Username],[EventDateTime],[UpdatedUsername],[UpdatedEventDatetime],[OwnerId] FROM [dbo].[Address]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Address objAddress = new Address();
                    if (dr["Id"].Equals(DBNull.Value))
                        objAddress.Id = 0;
                    else
                        objAddress.Id = (int)dr["Id"];
                    if (dr["IsMainOffice"].Equals(DBNull.Value))
                        objAddress.IsMainOffice = false;
                    else
                        objAddress.IsMainOffice = (bool)dr["IsMainOffice"];
                    if (dr["BranchName"].Equals(DBNull.Value))
                        objAddress.BranchName = string.Empty;
                    else
                        objAddress.BranchName = (string)dr["BranchName"];
                    if (dr["AddressType"].Equals(DBNull.Value))
                        objAddress.AddressType = 0;
                    else
                        objAddress.AddressType = (int)dr["AddressType"];
                    if (dr["Region"].Equals(DBNull.Value))
                        objAddress.Region = string.Empty;
                    else
                        objAddress.Region = (string)dr["Region"];
                    if (dr["City"].Equals(DBNull.Value))
                        objAddress.City = string.Empty;
                    else
                        objAddress.City = (string)dr["City"];
                    if (dr["Zone"].Equals(DBNull.Value))
                        objAddress.Zone = string.Empty;
                    else
                        objAddress.Zone = (string)dr["Zone"];
                    if (dr["Wereda"].Equals(DBNull.Value))
                        objAddress.Wereda = string.Empty;
                    else
                        objAddress.Wereda = (string)dr["Wereda"];
                    if (dr["Kebele"].Equals(DBNull.Value))
                        objAddress.Kebele = string.Empty;
                    else
                        objAddress.Kebele = (string)dr["Kebele"];
                    if (dr["HouseNo"].Equals(DBNull.Value))
                        objAddress.HouseNo = string.Empty;
                    else
                        objAddress.HouseNo = (string)dr["HouseNo"];
                    if (dr["TeleNo"].Equals(DBNull.Value))
                        objAddress.TeleNo = string.Empty;
                    else
                        objAddress.TeleNo = (string)dr["TeleNo"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objAddress.POBox = string.Empty;
                    else
                        objAddress.POBox = (string)dr["POBox"];
                    if (dr["Fax"].Equals(DBNull.Value))
                        objAddress.Fax = string.Empty;
                    else
                        objAddress.Fax = (string)dr["Fax"];
                    if (dr["Email"].Equals(DBNull.Value))
                        objAddress.Email = string.Empty;
                    else
                        objAddress.Email = (string)dr["Email"];
                    if (dr["ResidenceCountry"].Equals(DBNull.Value))
                        objAddress.ResidenceCountry = string.Empty;
                    else
                        objAddress.ResidenceCountry = (string)dr["ResidenceCountry"];
                    if (dr["Username"].Equals(DBNull.Value))
                        objAddress.Username = string.Empty;
                    else
                        objAddress.Username = (string)dr["Username"];
                    if (dr["EventDateTime"].Equals(DBNull.Value))
                        objAddress.EventDateTime = DateTime.MinValue;
                    else
                        objAddress.EventDateTime = (DateTime)dr["EventDateTime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objAddress.UpdatedUsername = string.Empty;
                    else
                        objAddress.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objAddress.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objAddress.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    if (dr["OwnerId"].Equals(DBNull.Value))
                        objAddress.OwnerId = 0;
                    else
                        objAddress.OwnerId = (int)dr["OwnerId"];
                    RecordsList.Add(objAddress);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Address::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[Address] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Address::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}