﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class KebeleDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Kebele] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Kebele");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Kebele GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Kebele objKebele = new Kebele();
            string strGetRecord = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Kebele] where code = @code ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@code", ID);
            DataTable dTable = new DataTable("Kebele");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objKebele.Code = string.Empty;
                    else
                        objKebele.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objKebele.Description = string.Empty;
                    else
                        objKebele.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amDescription"].Equals(DBNull.Value))
                        objKebele.AmDescription = string.Empty;
                    else
                        objKebele.AmDescription = (string)dTable.Rows[0]["amDescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objKebele.Parent = string.Empty;
                    else
                        objKebele.Parent = (string)dTable.Rows[0]["parent"];
                    objKebele.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objKebele;
        }

        public bool Insert(Kebele objKebele)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Kebele]
                                            ([code],[description],[amDescription],[parent])
                                     VALUES    (@code,@description,@amDescription,@parent)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objKebele.Code));
                command.Parameters.Add(new SqlParameter("@description", objKebele.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objKebele.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objKebele.Parent));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Kebele objKebele)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Kebele] SET     [code]=@code,    [description]=@description,    [amDescription]=@amDescription,    [parent]=@parent where Code = @Code ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objKebele.Code));
                command.Parameters.Add(new SqlParameter("@description", objKebele.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objKebele.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objKebele.Parent));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Kebele] WHERE CODE = @CODE";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@CODE", ID);
            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Kebele> GetList()
        {
            List<Kebele> RecordsList = new List<Kebele>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Kebele] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Kebele objKebele = new Kebele();
                    if (dr["code"].Equals(DBNull.Value))
                        objKebele.Code = string.Empty;
                    else
                        objKebele.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objKebele.Description = string.Empty;
                    else
                        objKebele.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objKebele.AmDescription = string.Empty;
                    else
                        objKebele.AmDescription = (string)dr["amDescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objKebele.Parent = string.Empty;
                    else
                        objKebele.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objKebele.Id = 0;
                    else
                        objKebele.Id = (int)dr["id"];
                    RecordsList.Add(objKebele);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Kebele> GetKebeles(string woredaCode,int Lang)
        {
            List<Kebele> RecordsList = new List<Kebele>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code], case @Language when 0 then [description] else [amDescription] end as Description,[parent],[id] FROM [dbo].[Kebele] where [parent]=@Parent ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Parent", woredaCode);
            command.Parameters.AddWithValue("@Language", Lang);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Kebele objKebele = new Kebele();
                    if (dr["code"].Equals(DBNull.Value))
                        objKebele.Code = string.Empty;
                    else
                        objKebele.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objKebele.Description = string.Empty;
                    else
                        objKebele.Description = (string)dr["description"];
                   
                    RecordsList.Add(objKebele);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<Kebele> GetKebeles(string woredaCode)
        {
            List<Kebele> RecordsList = new List<Kebele>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],  [description]  as Description,[parent],[id],amDescription FROM [dbo].[Kebele] where [parent]=@Parent ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Parent", woredaCode);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Kebele objKebele = new Kebele();
                    if (dr["code"].Equals(DBNull.Value))
                        objKebele.Code = string.Empty;
                    else
                        objKebele.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objKebele.Description = string.Empty;
                    else
                        objKebele.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objKebele.AmDescription = string.Empty;
                    else
                        objKebele.AmDescription = (string)dr["amDescription"];

                    RecordsList.Add(objKebele);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[Kebele] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return false;
        }

        public bool Exists(string name, string sortVal, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Kebele] 
                                WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and parent=@parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CKebele::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string name, string sortVal, string code, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Kebele] WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and Convert(int, [code]) <> @Code and parent = @parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@Code", code));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CKebele::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string GetNextCode()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT (MAX(Convert(int,Code))) + 1 FROM [dbo].[Kebele]";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetNextCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string GetParentRegionCode(string KebeleParentCode)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            //string strExists = @"SELECT Parent FROM [dbo].[tblZone] WHERE [Code]=@KebeleParentCode";

            string strSQL = @"Select top 1 Region.Code 
                            FROM Region,Zone,Kebele
                            where Kebele.Parent=Zone.Code and Zone.Parent=Region.Code and Kebele.Code=@KebeleParentCode";

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;
            connection.Open();
            try
            {
                command.Parameters.Add(new SqlParameter("@KebeleParentCode", KebeleParentCode));
                return ((string)command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetParentRegionCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string GetParentZoneCode(string KebeleParentCode)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            //string strExists = @"SELECT Parent FROM [dbo].[tblZone] WHERE [Code]=@KebeleParentCode";

            string strSQL = @"Select top 1 Zone.Code 
                            FROM Region,Zone,Kebele
                            where Kebele.Parent=Zone.Code and Zone.Parent=Region.Code and Kebele.Code=@KebeleParentCode";

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;
            connection.Open();
            try
            {
                command.Parameters.Add(new SqlParameter("@KebeleParentCode", KebeleParentCode));
                return ((string)command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Kebele::GetParentRegionCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

    }
}