﻿using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CUSTOR.DataAccess
{
    public class WoredaDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Woreda] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Woreda");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Woreda GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Woreda objWoreda = new Woreda();
            string strGetRecord = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Woreda] where code = @ID";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@ID", ID);
            DataTable dTable = new DataTable("Woreda");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objWoreda.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objWoreda.Description = string.Empty;
                    else
                        objWoreda.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amDescription"].Equals(DBNull.Value))
                        objWoreda.AmDescription = string.Empty;
                    else
                        objWoreda.AmDescription = (string)dTable.Rows[0]["amDescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objWoreda.Parent = string.Empty;
                    else
                        objWoreda.Parent = (string)dTable.Rows[0]["parent"];
                    objWoreda.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objWoreda;
        }

        public bool Insert(Woreda objWoreda)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Woreda]
                                            ([code],[description],[amDescription],[parent])
                                     VALUES    (@code,@description,@amDescription,@parent)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objWoreda.Code));
                command.Parameters.Add(new SqlParameter("@description", objWoreda.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objWoreda.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objWoreda.Parent));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Woreda objWoreda)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Woreda] SET     [code]=@code,    [description]=@description,    [amDescription]=@amDescription,  
                                [parent]=@parent where code = @code";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objWoreda.Code));
                command.Parameters.Add(new SqlParameter("@description", objWoreda.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objWoreda.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objWoreda.Parent));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Woreda] where Code = @Code ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Code", ID);
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Woreda> GetList()
        {
            List<Woreda> RecordsList = new List<Woreda>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Woreda] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Woreda objWoreda = new Woreda();
                    if (dr["code"].Equals(DBNull.Value))
                        objWoreda.Code = string.Empty;
                    else
                        objWoreda.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objWoreda.Description = string.Empty;
                    else
                        objWoreda.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objWoreda.AmDescription = string.Empty;
                    else
                        objWoreda.AmDescription = (string)dr["amDescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objWoreda.Parent = string.Empty;
                    else
                        objWoreda.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objWoreda.Id = 0;
                    else
                        objWoreda.Id = (int)dr["id"];
                    RecordsList.Add(objWoreda);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[Woreda] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {

            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return true;
        }
        public List<Woreda> GetWoredas(string strZoneCode, int Lan)
        {
            string strSQL = "SELECT code,case @Language when 0 then description else amDescription end as description from Woreda Where Parent=@Parent";
            List<Woreda> Woredas = new List<Woreda>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strZoneCode);
                        command.Parameters.AddWithValue("@Language", Lan);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Woreda Woreda;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Woreda = new Woreda();
                                    Woreda.Code = Convert.ToString(dataReader["code"]);
                                    Woreda.Description = Convert.ToString(dataReader["description"]);
                                    
                                    Woredas.Add(Woreda);
                                }
                        }
                    }
                }
                return Woredas;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetWoredas: " + ex.Message);

            }
        }
        public List<Woreda> GetWoredas(string strZoneCode)
        {
            string strSQL = "SELECT code, description,amDescription from Woreda Where Parent=@Parent";
            List<Woreda> Woredas = new List<Woreda>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strZoneCode);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Woreda Woreda;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Woreda = new Woreda();
                                    Woreda.Code = Convert.ToString(dataReader["code"]);
                                    Woreda.Description = Convert.ToString(dataReader["description"]);
                                    Woreda.AmDescription = Convert.ToString(dataReader["amDescription"]);
                                    Woredas.Add(Woreda);
                                }
                        }
                    }
                }
                return Woredas;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetWoredas: " + ex.Message);

            }
        }
        public string GetParentRegionCode(string Code)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Parent FROM [dbo].[Zone] WHERE [Code]=@Code";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            connection.Open();
            try
            {
                command.Parameters.Add(new SqlParameter("@Code", Code));
                return ((string)command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::GetParentRegionCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string name, string sortVal, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Woreda] 
                                WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and parent=@parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CWoreda::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string name, string sortVal, string code, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Woreda] WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and  Convert(int,[code]) <> @Code and parent = @parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@Code", code));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CWoreda::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public string GetNextCode()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT (MAX(Convert(int,Code))) + 1 FROM [dbo].[Woreda]";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Woreda::GetNextCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}