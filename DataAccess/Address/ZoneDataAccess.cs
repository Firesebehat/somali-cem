﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class ZoneDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Zone] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Zone");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Zone::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Zone GetRecord(string code)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Zone objZone = new Zone();
            string strGetRecord = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Zone] where code = @code ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Zone");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            command.Parameters.AddWithValue("@code", code);
            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objZone.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objZone.Description = string.Empty;
                    else
                        objZone.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amDescription"].Equals(DBNull.Value))
                        objZone.AmDescription = string.Empty;
                    else
                        objZone.AmDescription = (string)dTable.Rows[0]["amDescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objZone.Parent = string.Empty;
                    else
                        objZone.Parent = (string)dTable.Rows[0]["parent"];
                    objZone.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Zone::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objZone;
        }

        public bool Insert(Zone objZone)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Zone]
                                            ([code],[description],[amDescription],[parent],[id])
                                     VALUES    (@code,@description,@amDescription,@parent,@id)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objZone.Code));
                command.Parameters.Add(new SqlParameter("@description", objZone.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objZone.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objZone.Parent));
                command.Parameters.Add(new SqlParameter("@id", objZone.Id));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Zone::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Zone objZone)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Zone] SET        [description]=@description,    [amDescription]=@amDescription,    [parent]=@parent where code = @code ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };

            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objZone.Code));
                command.Parameters.Add(new SqlParameter("@description", objZone.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objZone.AmDescription));
                command.Parameters.Add(new SqlParameter("@parent", objZone.Parent));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Zone::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Zone] WHERE [Code]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CZone::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }




        public List<Zone> GetList(string regionCode)
        {
            List<Zone> RecordsList = new List<Zone>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Zone] where Parent= @Parent ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Parent", regionCode);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Zone objZone = new Zone();
                    if (dr["code"].Equals(DBNull.Value))
                        objZone.Code = string.Empty;
                    else
                        objZone.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objZone.Description = string.Empty;
                    else
                        objZone.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objZone.AmDescription = string.Empty;
                    else
                        objZone.AmDescription = (string)dr["amDescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objZone.Parent = string.Empty;
                    else
                        objZone.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objZone.Id = 0;
                    else
                        objZone.Id = (int)dr["id"];
                    RecordsList.Add(objZone);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Zone::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[Zone] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
            }
            catch (Exception ex)
            {
                throw new Exception("Zone::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return false;
        }
        public List<Zone> GetZones(string strRegionCode, int Lan)
        {
            string strSQL = "SELECT code,case @Language when 1 then amDescription else description end as description from Zone Where Parent=@Parent";
            List<Zone> Zones = new List<Zone>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strRegionCode);
                        command.Parameters.AddWithValue("@Language", Lan);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Zone Zone;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Zone = new Zone();
                                    Zone.Code = Convert.ToString(dataReader["code"]);
                                    Zone.Description = Convert.ToString(dataReader["description"]);
                                    Zones.Add(Zone);
                                }
                        }
                    }
                }
                return Zones;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetZones: " + ex.Message);

            }
        }

        public bool Exists(string name, string sortVal, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Zone] 
                                WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and parent=@parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CZone::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string name, string sortVal, string code, string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(id) FROM [dbo].[Zone] WHERE ([description]=@description or [amDescriptionSort] = @amDescriptionSort) and [code] <> @Code and parent = @parent";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@description", name));
                command.Parameters.Add(new SqlParameter("@amDescriptionSort", sortVal));
                command.Parameters.Add(new SqlParameter("@Code", code));
                command.Parameters.Add(new SqlParameter("@parent", parent));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CZone::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public string GetNextCode()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT (MAX(Convert(int,Code))) + 1 FROM [dbo].[Zone]";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                return command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetNextCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}