﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class RegionDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Region] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Region");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Region GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Region objRegion = new Region();
            string strGetRecord = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Region] where Code = @ID ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Region");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            command.Parameters.AddWithValue("@ID", ID);
            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objRegion.Code = string.Empty;
                    else
                        objRegion.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objRegion.Description = string.Empty;
                    else
                        objRegion.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amDescription"].Equals(DBNull.Value))
                        objRegion.AmDescription = string.Empty;
                    else
                        objRegion.AmDescription = (string)dTable.Rows[0]["amDescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objRegion.Parent = string.Empty;
                    else
                        objRegion.Parent = (string)dTable.Rows[0]["parent"];
                    objRegion.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objRegion;
        }

        public bool Insert(Region objRegion)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Region]
                                            ([code],[description],[amDescription], AmDescriptionSort)
                                     VALUES    (@code,@description,@amDescription,@AmDescriptionSort)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objRegion.Code));
                command.Parameters.Add(new SqlParameter("@description", objRegion.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objRegion.AmDescription));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSort", objRegion.AmDescriptionSort));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Region objRegion)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Region] SET     [code]=@code,    [description]=@description,    [amDescription]=@amDescription,    [AmDescriptionSort]=@AmDescriptionSort where  [Code]=@Code ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objRegion.Code));
                command.Parameters.Add(new SqlParameter("@description", objRegion.Description));
                command.Parameters.Add(new SqlParameter("@amDescription", objRegion.AmDescription));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSort", objRegion.AmDescriptionSort));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Region] where Code = @Code ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Code", ID);
            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Region> GetList()
        {
            List<Region> RecordsList = new List<Region>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amDescription],[parent],[id] FROM [dbo].[Region] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Region objRegion = new Region();
                    if (dr["code"].Equals(DBNull.Value))
                        objRegion.Code = string.Empty;
                    else
                        objRegion.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objRegion.Description = string.Empty;
                    else
                        objRegion.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objRegion.AmDescription = string.Empty;
                    else
                        objRegion.AmDescription = (string)dr["amDescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objRegion.Parent = string.Empty;
                    else
                        objRegion.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objRegion.Id = 0;
                    else
                        objRegion.Id = (int)dr["id"];
                    RecordsList.Add(objRegion);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<Region> GetRegions(int Lan)
        {
            List<Region> RecordsList = new List<Region>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code], case @Language when 0 then [description] else [amDescription] end as [Description], [parent],[id] FROM [dbo].[Region] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Language", Lan);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Region objRegion = new Region();
                    if (dr["code"].Equals(DBNull.Value))
                        objRegion.Code = string.Empty;
                    else
                        objRegion.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objRegion.Description = string.Empty;
                    else
                        objRegion.Description = (string)dr["description"];
                   
                    RecordsList.Add(objRegion);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<Region> GetRegions()
        {
            List<Region> RecordsList = new List<Region>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT code, [Description], [parent],[id],amDescription FROM [dbo].[Region] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Region objRegion = new Region();
                    if (dr["code"].Equals(DBNull.Value))
                        objRegion.Code = string.Empty;
                    else
                        objRegion.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objRegion.Description = string.Empty;
                    else
                        objRegion.Description = (string)dr["description"];
                    if (dr["amDescription"].Equals(DBNull.Value))
                        objRegion.AmDescription = string.Empty;
                    else
                        objRegion.AmDescription = (string)dr["amDescription"];

                    RecordsList.Add(objRegion);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[Region] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return false;
        }

        public string GetNextCode()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT (MAX(Convert(int,Code))) + 1 FROM [dbo].[Region]";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                return command.ExecuteScalar().ToString();

            }
            catch (Exception ex)
            {
                throw new Exception("Region::GetNextCode::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string engDesc, string sortValue)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT Code FROM [dbo].[Region] WHERE [description]=@description or [AmDescriptionSort]=@AmDescriptionSort";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@description", engDesc));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSort", sortValue));
                dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(string engDesc, string sortVaL, string Id)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strExists = @"SELECT Count(Id) FROM [dbo].[Region] WHERE ([description]=@description or [AmDescriptionSort]=@AmDescriptionSort) and Convert(int,Code) <> @Code";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@description", engDesc));
                command.Parameters.Add(new SqlParameter("@AmDescriptionSort", sortVaL));
                command.Parameters.Add(new SqlParameter("@Code", Id));
                return  (int)command.ExecuteScalar() > 0;
                
            }
            catch (Exception ex)
            {
                throw new Exception("Region::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}