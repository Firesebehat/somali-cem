﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;

namespace CUSTOR.DataAccess
{
    public class TaxExemptionDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[TaxAmount],[TaxPercent],[IncomeFrom],[IncomeTo],[TaxPayerGrade],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[CreatedUserId],[UpdatedUserId],[OverdueDaysFrom],[OverdueDaysTo] FROM [dbo].[TaxExemption]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("TaxExemption");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public TaxExemption GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            TaxExemption objTaxExemption = new TaxExemption();
            string strGetRecord = @"SELECT [Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[TaxAmount],[TaxPercent],[IncomeFrom],[IncomeTo],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[CreatedUserId],[UpdatedUserId],[Description] FROM [dbo].[TaxExemption] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("TaxExemption");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objTaxExemption.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["TaxType"].Equals(DBNull.Value))
                        objTaxExemption.TaxType = 0;
                    else
                        objTaxExemption.TaxType = (int)dTable.Rows[0]["TaxType"];
                    objTaxExemption.AppliedDateFrom = (DateTime)dTable.Rows[0]["AppliedDateFrom"];
                    if (dTable.Rows[0]["AppliedDateTo"].Equals(DBNull.Value))
                        objTaxExemption.AppliedDateTo = DateTime.MinValue;
                    else
                        objTaxExemption.AppliedDateTo = (DateTime)dTable.Rows[0]["AppliedDateTo"];
                    if (dTable.Rows[0]["TaxAmount"].Equals(DBNull.Value))
                        objTaxExemption.TaxAmount = 0;
                    else
                        objTaxExemption.TaxAmount = (double)dTable.Rows[0]["TaxAmount"];
                    if (dTable.Rows[0]["TaxPercent"].Equals(DBNull.Value))
                        objTaxExemption.TaxPercent = 0;
                    else
                        objTaxExemption.TaxPercent = (double)dTable.Rows[0]["TaxPercent"];
                    if (dTable.Rows[0]["IncomeFrom"].Equals(DBNull.Value))
                        objTaxExemption.IncomeFrom = 0;
                    else
                        objTaxExemption.IncomeFrom = (decimal)dTable.Rows[0]["IncomeFrom"];
                    if (dTable.Rows[0]["IncomeTo"].Equals(DBNull.Value))
                        objTaxExemption.IncomeTo = 0;
                    else
                        objTaxExemption.IncomeTo = (decimal)dTable.Rows[0]["IncomeTo"];

                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objTaxExemption.IsActive = false;
                    else
                        objTaxExemption.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objTaxExemption.IsDeleted = false;
                    else
                        objTaxExemption.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CenterId"].Equals(DBNull.Value))
                        objTaxExemption.CenterId = 0;
                    else
                        objTaxExemption.CenterId = (int)dTable.Rows[0]["CenterId"];
                    if (dTable.Rows[0]["CenterName"].Equals(DBNull.Value))
                        objTaxExemption.CenterName = string.Empty;
                    else
                        objTaxExemption.CenterName = (string)dTable.Rows[0]["CenterName"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objTaxExemption.CreatedDate = DateTime.MinValue;
                    else
                        objTaxExemption.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objTaxExemption.CreatedBy = string.Empty;
                    else
                        objTaxExemption.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedDate = DateTime.MinValue;
                    else
                        objTaxExemption.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedBy = string.Empty;
                    else
                        objTaxExemption.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objTaxExemption.CreatedUserId = Guid.Empty;
                    else
                        objTaxExemption.CreatedUserId = (Guid)dTable.Rows[0]["CreatedUserId"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedUserId = Guid.Empty;
                    else
                        objTaxExemption.UpdatedUserId = (Guid)dTable.Rows[0]["UpdatedUserId"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objTaxExemption.Description = string.Empty;
                    else
                        objTaxExemption.Description = (string)dTable.Rows[0]["Description"];

                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objTaxExemption;
        }
        public List<TaxExemptedCategory> GetExemptedCategoryList(string parentId)
        {
            List<TaxExemptedCategory> RecordsList = new List<TaxExemptedCategory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @" SELECT [Id],[CategoryId],[ExemptionId],[IsDeleted], CategoryCode = (select Code from category where  CategoryId = Category.Id )
                 FROM [dbo].[TaxExemptedCategory] where IsNull(IsDeleted,0)=0 and ExemptionId = @ExemptionId ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@ExemptionId", parentId);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxExemptedCategory objTaxExemptedCategory = new TaxExemptedCategory();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxExemptedCategory.Id = 0;
                    else
                        objTaxExemptedCategory.Id = (int)dr["Id"];
                    if (dr["CategoryId"].Equals(DBNull.Value))
                        objTaxExemptedCategory.CategoryId = 0;
                    else
                        objTaxExemptedCategory.CategoryId = (int)dr["CategoryId"];
                    if (dr["CategoryCode"].Equals(DBNull.Value))
                        objTaxExemptedCategory.CategoryCode = string.Empty;
                    else
                        objTaxExemptedCategory.CategoryCode = (string)dr["CategoryCode"];
                    if (dr["ExemptionId"].Equals(DBNull.Value))
                        objTaxExemptedCategory.ExemptionId = 0;
                    else
                        objTaxExemptedCategory.ExemptionId = (int)dr["ExemptionId"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objTaxExemptedCategory.IsDeleted = false;
                    else
                        objTaxExemptedCategory.IsDeleted = (bool)dr["IsDeleted"];
                    RecordsList.Add(objTaxExemptedCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemptedCategory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<ExemptedTaxpayerGrade> GetExemptedGradeList(int parentExemptionId)
        {
            List<ExemptedTaxpayerGrade> RecordsList = new List<ExemptedTaxpayerGrade>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[ExemptionId],[TaxpayerGrade],[IsDeleted] FROM [dbo].[ExemptedTaxpayerGrade] where IsNull(IsDeleted,0)=0 and ExemptionId = @ExemptionId ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@ExemptionId", parentExemptionId);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    ExemptedTaxpayerGrade objExemptedTaxpayerGrade = new ExemptedTaxpayerGrade();
                    if (dr["Id"].Equals(DBNull.Value))
                        objExemptedTaxpayerGrade.Id = 0;
                    else
                        objExemptedTaxpayerGrade.Id = (int)dr["Id"];
                    if (dr["ExemptionId"].Equals(DBNull.Value))
                        objExemptedTaxpayerGrade.ExemptionId = 0;
                    else
                        objExemptedTaxpayerGrade.ExemptionId = (int)dr["ExemptionId"];
                    if (dr["TaxpayerGrade"].Equals(DBNull.Value))
                        objExemptedTaxpayerGrade.TaxpayerGrade = 0;
                    else
                        objExemptedTaxpayerGrade.TaxpayerGrade = (int)dr["TaxpayerGrade"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objExemptedTaxpayerGrade.IsDeleted = false;
                    else
                        objExemptedTaxpayerGrade.IsDeleted = (bool)dr["IsDeleted"];
                    RecordsList.Add(objExemptedTaxpayerGrade);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExemptedTaxpayerGrade::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Insert(TaxExemption objTaxExemption, SqlCommand command)
        {
            command.Parameters.Clear();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"DECLARE @T AS TABLE(ID INT); INSERT INTO [dbo].[TaxExemption]
                                            ([TaxType],[AppliedDateFrom],[AppliedDateTo],[TaxAmount],[TaxPercent],[IncomeFrom],[IncomeTo],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[CreatedUserId],[Description]) output Inserted.Id into @T
                                     VALUES    (@TaxType,@AppliedDateFrom,@AppliedDateTo,@TaxAmount,@TaxPercent,@IncomeFrom,@IncomeTo,@IsActive,@IsDeleted,@CenterId,@CenterName,@CreatedDate,@CreatedBy,@CreatedUserId,@Description) select * from @T";

            command.CommandText = strInsert;

            try
            {

                command.Parameters.Add(new SqlParameter("@TaxType", objTaxExemption.TaxType));
                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objTaxExemption.AppliedDateFrom));

                if (objTaxExemption.AppliedDateTo != DateTime.MinValue)
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", objTaxExemption.AppliedDateTo));
                else
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@TaxAmount", objTaxExemption.TaxAmount));
                command.Parameters.Add(new SqlParameter("@TaxPercent", objTaxExemption.TaxPercent));
                command.Parameters.Add(new SqlParameter("@IncomeFrom", objTaxExemption.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", objTaxExemption.IncomeTo));
                command.Parameters.Add(new SqlParameter("@IsActive", objTaxExemption.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objTaxExemption.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CenterId", objTaxExemption.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objTaxExemption.CenterName));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objTaxExemption.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objTaxExemption.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objTaxExemption.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@Description", objTaxExemption.Description));
                connection.Open();
                objTaxExemption.Id = (int)command.ExecuteScalar();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Insert::Error!" + ex.Message, ex);
            }

        }
        public bool InsertExemptedTaxpayers(TaxExemption objTaxExemption, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[ExemptedTaxpayerGrade]
                                            ([ExemptionId],[TaxpayerGrade])
                                     VALUES    (@ExemptionId,@TaxpayerGrade)";

            command.CommandText = strInsert;

            try
            {
                command.Parameters.Clear();

                command.Parameters.Add(new SqlParameter("@ExemptionId", objTaxExemption.Id));
                command.Parameters.Add(new SqlParameter("@TaxpayerGrade", objTaxExemption.TaxPayerGrade));


                connection.Open();
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Insert::Error!" + ex.Message, ex);
            }

        }

        public bool InsertExemptedCategories(TaxExemption objTaxExemption, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @" INSERT INTO  [dbo].[TaxExemptedCategory]
                                            ([ExemptionId],[CategoryId])
                                     VALUES    (@ExemptionId,@CategoryId)";

            command.CommandText = strInsert;

            try
            {
                command.Parameters.Clear();

                command.Parameters.Add(new SqlParameter("@ExemptionId", objTaxExemption.Id));
                command.Parameters.Add(new SqlParameter("@CategoryId", objTaxExemption.CategoryId));


                connection.Open();
                command.ExecuteScalar();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Insert::Error!" + ex.Message, ex);
            }

        }

        public bool Update(TaxExemption objTaxExemption, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[TaxExemption] SET     [TaxType]=@TaxType,    [AppliedDateFrom]=@AppliedDateFrom,    [AppliedDateTo]=@AppliedDateTo,    [TaxAmount]=@TaxAmount,    [TaxPercent]=@TaxPercent,  
                    [IncomeFrom]=@IncomeFrom,    [IncomeTo]=@IncomeTo,       [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy,  [UpdatedUserId]=@UpdatedUserId,[Description] = @Description  WHERE [Id]=@Id";

            command.CommandText = strUpdate;
            command.Parameters.Clear();
            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objTaxExemption.Id));
                command.Parameters.Add(new SqlParameter("@TaxType", objTaxExemption.TaxType));
                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objTaxExemption.AppliedDateFrom));
                if (objTaxExemption.AppliedDateTo != DateTime.MinValue)
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", objTaxExemption.AppliedDateTo));
                else
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@TaxAmount", objTaxExemption.TaxAmount));
                command.Parameters.Add(new SqlParameter("@TaxPercent", objTaxExemption.TaxPercent));
                command.Parameters.Add(new SqlParameter("@IncomeFrom", objTaxExemption.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", objTaxExemption.IncomeTo));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objTaxExemption.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objTaxExemption.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objTaxExemption.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@Description", objTaxExemption.Description));
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Update::Error!" + ex.Message, ex);
            }
            finally
            {

            }
        }

        public bool Delete(int ID, SqlCommand command)
        {
            string strDelete = @"Update [dbo].[TaxExemption] set IsDeleted = 1 WHERE [Id]=@Id";
            command.Parameters.AddWithValue("@Id", ID);
            command.CommandText = strDelete;
            try
            {
                int _rowsAffected = command.ExecuteNonQuery();
                strDelete = @"Update [dbo].[ExemptedTaxpayerGrade] set isdeleted = 1 where [ExemptionId]=@Id";
                command.CommandText = strDelete;
                _rowsAffected = command.ExecuteNonQuery();


                strDelete = @"update [dbo].[TaxExemptedCategory] set isdeleted = 1 WHERE [ExemptionId]=@Id";
                command.CommandText = strDelete;
                _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Delete::Error!" + ex.Message, ex);
            }

        }
        public bool DeleteExemptionChildTables(int parentExemptionId, SqlCommand command)
        {
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@Id", parentExemptionId);
            string strDelete = string.Empty;
            command.CommandText = strDelete;
            try
            {
                strDelete = @"Update [dbo].[ExemptedTaxpayerGrade] set isdeleted = 1 where [ExemptionId]=@Id";
                command.CommandText = strDelete;
                int _rowsAffected = command.ExecuteNonQuery();


                strDelete = @"update [dbo].[TaxExemptedCategory] set isdeleted = 1 WHERE [ExemptionId]=@Id";
                command.CommandText = strDelete;
                _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Delete::Error!" + ex.Message, ex);
            }

        }


        public List<TaxExemption> GetList()
        {
            List<TaxExemption> RecordsList = new List<TaxExemption>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[TaxAmount],[TaxPercent],[IncomeFrom],[IncomeTo],[TaxPayerGrade],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[CreatedUserId],[UpdatedUserId],[OverdueDaysFrom],[OverdueDaysTo] FROM [dbo].[TaxExemption]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxExemption objTaxExemption = new TaxExemption();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxExemption.Id = 0;
                    else
                        objTaxExemption.Id = (int)dr["Id"];
                    if (dr["TaxType"].Equals(DBNull.Value))
                        objTaxExemption.TaxType = 0;
                    else
                        objTaxExemption.TaxType = (int)dr["TaxType"];
                    if (dr["AppliedDateFrom"].Equals(DBNull.Value))
                        objTaxExemption.AppliedDateFrom = DateTime.MinValue;
                    else
                        objTaxExemption.AppliedDateFrom = (DateTime)dr["AppliedDateFrom"];
                    if (dr["AppliedDateTo"].Equals(DBNull.Value))
                        objTaxExemption.AppliedDateTo = DateTime.MinValue;
                    else
                        objTaxExemption.AppliedDateTo = (DateTime)dr["AppliedDateTo"];
                    if (dr["TaxAmount"].Equals(DBNull.Value))
                        objTaxExemption.TaxAmount = 0;
                    else
                        objTaxExemption.TaxAmount = (double)dr["TaxAmount"];
                    if (dr["TaxPercent"].Equals(DBNull.Value))
                        objTaxExemption.TaxPercent = 0;
                    else
                        objTaxExemption.TaxPercent = (double)dr["TaxPercent"];
                    if (dr["IncomeFrom"].Equals(DBNull.Value))
                        objTaxExemption.IncomeFrom = 0;
                    else
                        objTaxExemption.IncomeFrom = (decimal)dr["IncomeFrom"];
                    if (dr["IncomeTo"].Equals(DBNull.Value))
                        objTaxExemption.IncomeTo = 0;
                    else
                        objTaxExemption.IncomeTo = (decimal)dr["IncomeTo"];
                    if (dr["TaxPayerGrade"].Equals(DBNull.Value))
                        objTaxExemption.TaxPayerGrade = 0;
                    else
                        objTaxExemption.TaxPayerGrade = (int)dr["TaxPayerGrade"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objTaxExemption.IsActive = false;
                    else
                        objTaxExemption.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objTaxExemption.IsDeleted = false;
                    else
                        objTaxExemption.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CenterId"].Equals(DBNull.Value))
                        objTaxExemption.CenterId = 0;
                    else
                        objTaxExemption.CenterId = (int)dr["CenterId"];
                    if (dr["CenterName"].Equals(DBNull.Value))
                        objTaxExemption.CenterName = string.Empty;
                    else
                        objTaxExemption.CenterName = (string)dr["CenterName"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objTaxExemption.CreatedDate = DateTime.MinValue;
                    else
                        objTaxExemption.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objTaxExemption.CreatedBy = string.Empty;
                    else
                        objTaxExemption.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedDate = DateTime.MinValue;
                    else
                        objTaxExemption.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedBy = string.Empty;
                    else
                        objTaxExemption.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objTaxExemption.CreatedUserId = Guid.Empty;
                    else
                        objTaxExemption.CreatedUserId = (Guid)dr["CreatedUserId"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objTaxExemption.UpdatedUserId = Guid.Empty;
                    else
                        objTaxExemption.UpdatedUserId = (Guid)dr["UpdatedUserId"];
                    if (dr["OverdueDaysFrom"].Equals(DBNull.Value))
                        objTaxExemption.OverdueDaysFrom = 0;
                    else
                        objTaxExemption.OverdueDaysFrom = (int)dr["OverdueDaysFrom"];
                    if (dr["OverdueDaysTo"].Equals(DBNull.Value))
                        objTaxExemption.OverdueDaysTo = 0;
                    else
                        objTaxExemption.OverdueDaysTo = (int)dr["OverdueDaysTo"];
                    RecordsList.Add(objTaxExemption);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<TaxExemption> GetExemptionDetails()
        {
            List<TaxExemption> RecordsList = new List<TaxExemption>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT  dbo. ExemptedCategories([dbo].[TaxExemption].Id) as Code,  TaxExemption.Id, [dbo].[TaxType].TaxTypeName,[AppliedDateFrom],[AppliedDateTo],[TaxAmount],[TaxPercent],[IncomeFrom],[IncomeTo],
								(select dbo.ExemptedTaxpayerGrades([dbo].[TaxExemption].Id)) Grade, TaxExemption.[IsActive],[dbo].[TaxExemption].[IsDeleted],[CenterId],[CenterName],
								[dbo].[TaxExemption].[CreatedDate],[dbo].[TaxExemption].[CreatedBy],[dbo].[TaxExemption].[UpdatedDate],[OverdueDaysFrom],[OverdueDaysTo] FROM [dbo].[TaxExemption]
								inner join TaxType on TaxExemption.TaxType = TaxType.Id Where IsNull([dbo].[TaxExemption].[IsDeleted],0) =0";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxExemption objTaxExemption = new TaxExemption();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxExemption.Id = 0;
                    else
                        objTaxExemption.Id = (int)dr["Id"];
                    if (dr["TaxTypeName"].Equals(DBNull.Value))
                        objTaxExemption.TaxTypeName = string.Empty;
                    else
                        objTaxExemption.TaxTypeName = (string)dr["TaxTypeName"];
                    if (dr["AppliedDateFrom"].Equals(DBNull.Value))
                        objTaxExemption.AppliedDateFrom = DateTime.MinValue;
                    else
                    {
                        objTaxExemption.AppliedDateFrom = (DateTime)dr["AppliedDateFrom"];
                        objTaxExemption.AppliedDateFromText = EthiopicDateTime.GetEthiopianDate(objTaxExemption.AppliedDateFrom);

                    }
                    if (dr["AppliedDateTo"].Equals(DBNull.Value))
                        objTaxExemption.AppliedDateTo = DateTime.MinValue;
                    else
                    {
                        objTaxExemption.AppliedDateTo = (DateTime)dr["AppliedDateTo"];
                        objTaxExemption.AppliedDatToText = EthiopicDateTime.GetEthiopianDate(objTaxExemption.AppliedDateTo);
                    }
                    if (dr["TaxAmount"].Equals(DBNull.Value))
                        objTaxExemption.TaxAmount = 0;
                    else
                        objTaxExemption.TaxAmount = (double)dr["TaxAmount"];
                    if (dr["TaxPercent"].Equals(DBNull.Value))
                        objTaxExemption.TaxPercent = 0;
                    else
                        objTaxExemption.TaxPercent = (double)dr["TaxPercent"];
                    if (dr["IncomeFrom"].Equals(DBNull.Value))
                        objTaxExemption.IncomeFrom = 0;
                    else
                        objTaxExemption.IncomeFrom = (decimal)dr["IncomeFrom"];
                    if (dr["IncomeTo"].Equals(DBNull.Value))
                        objTaxExemption.IncomeTo = 0;
                    else
                        objTaxExemption.IncomeTo = (decimal)dr["IncomeTo"];


                    if (dr["OverdueDaysFrom"].Equals(DBNull.Value))
                        objTaxExemption.OverdueDaysFrom = 0;
                    else
                        objTaxExemption.OverdueDaysFrom = (int)dr["OverdueDaysFrom"];
                    if (dr["OverdueDaysTo"].Equals(DBNull.Value))
                        objTaxExemption.OverdueDaysTo = 0;
                    else
                        objTaxExemption.OverdueDaysTo = (int)dr["OverdueDaysTo"];
                    if (dr["Grade"].Equals(DBNull.Value))
                        objTaxExemption.TaxPayerGradeText = string.Empty;
                    else
                        objTaxExemption.TaxPayerGradeText = (string)dr["Grade"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objTaxExemption.CategoryText = string.Empty;
                    else
                        objTaxExemption.CategoryText = (string)dr["Code"];
                    RecordsList.Add(objTaxExemption);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[TaxExemption] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}