﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class UserSiteTransferHistorDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [UserId],[SiteIdOld],[SiteIdNew],[TransferedBy],[CreatedUserName],[TransferedDate],[Remark] FROM [dbo].[UserSiteTransferHistor] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("UserSiteTransferHistor");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public UserSiteTransferHistor GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            UserSiteTransferHistor objUserSiteTransferHistor = new UserSiteTransferHistor();
            string strGetRecord = @"SELECT [UserId],[SiteIdOld],[SiteIdNew],[TransferedBy],[CreatedUserName],[TransferedDate],[Remark] FROM [dbo].[UserSiteTransferHistor] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("UserSiteTransferHistor");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["UserId"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.UserId = Guid.Empty;
                    else
                        objUserSiteTransferHistor.UserId = (Guid)dTable.Rows[0]["UserId"];
                    if (dTable.Rows[0]["SiteIdOld"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteIdOld = 0;
                    else
                        objUserSiteTransferHistor.SiteIdOld = (int)dTable.Rows[0]["SiteIdOld"];
                    if (dTable.Rows[0]["SiteIdNew"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteIdNew = 0;
                    else
                        objUserSiteTransferHistor.SiteIdNew = (int)dTable.Rows[0]["SiteIdNew"];
                    if (dTable.Rows[0]["TransferedBy"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedBy = Guid.Empty;
                    else
                        objUserSiteTransferHistor.TransferedBy = (Guid)dTable.Rows[0]["TransferedBy"];
                    if (dTable.Rows[0]["CreatedUserName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.CreatedUserName = string.Empty;
                    else
                        objUserSiteTransferHistor.CreatedUserName = (string)dTable.Rows[0]["CreatedUserName"];
                    if (dTable.Rows[0]["TransferedDate"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedDate = DateTime.MinValue;
                    else
                        objUserSiteTransferHistor.TransferedDate = (DateTime)dTable.Rows[0]["TransferedDate"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.Remark = string.Empty;
                    else
                        objUserSiteTransferHistor.Remark = (string)dTable.Rows[0]["Remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objUserSiteTransferHistor;
        }

        public bool UpdateSiteTransfer(UserSiteTransferHistor objUserSiteTransferHistor)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[UserSiteTransferHistor]
                                            ([UserId],[SiteIdOld],[SiteIdNew],[TransferedBy],[CreatedUserName],[TransferedDate],[Remark])
                                     VALUES    (@UserId,@SiteIdOld,@SiteIdNew,@TransferedBy,@CreatedUserName,@TransferedDate,@Remark)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;
            connection.Open();
            SqlTransaction tran = connection.BeginTransaction();
            command.Transaction = tran;
            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", objUserSiteTransferHistor.UserId));
                command.Parameters.Add(new SqlParameter("@SiteIdOld", objUserSiteTransferHistor.SiteIdOld));
                command.Parameters.Add(new SqlParameter("@SiteIdNew", objUserSiteTransferHistor.SiteIdNew));
                command.Parameters.Add(new SqlParameter("@TransferedBy", objUserSiteTransferHistor.TransferedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserName", objUserSiteTransferHistor.CreatedUserName));
                command.Parameters.Add(new SqlParameter("@TransferedDate", objUserSiteTransferHistor.TransferedDate));
                command.Parameters.Add(new SqlParameter("@Remark", objUserSiteTransferHistor.Remark));


                
                int _rowsAffected = command.ExecuteNonQuery();
                if(_rowsAffected > 0)
                {
                    UpdateTaxCenter(objUserSiteTransferHistor.UserId, objUserSiteTransferHistor.SiteIdNew, command);
                }
                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw new Exception("UserSiteTransferHistor::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                tran.Dispose();
                  
            }
        }


        public bool Update(UserSiteTransferHistor objUserSiteTransferHistor)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[UserSiteTransferHistor] SET     [UserId]=@UserId,    [SiteIdOld]=@SiteIdOld,    [SiteIdNew]=@SiteIdNew,    [TransferedBy]=@TransferedBy,    [CreatedUserName]=@CreatedUserName,    [TransferedDate]=@TransferedDate,    [Remark]=@Remark ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", objUserSiteTransferHistor.UserId));
                command.Parameters.Add(new SqlParameter("@SiteIdOld", objUserSiteTransferHistor.SiteIdOld));
                command.Parameters.Add(new SqlParameter("@SiteIdNew", objUserSiteTransferHistor.SiteIdNew));
                command.Parameters.Add(new SqlParameter("@TransferedBy", objUserSiteTransferHistor.TransferedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserName", objUserSiteTransferHistor.CreatedUserName));
                command.Parameters.Add(new SqlParameter("@TransferedDate", objUserSiteTransferHistor.TransferedDate));
                command.Parameters.Add(new SqlParameter("@Remark", objUserSiteTransferHistor.Remark));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[UserSiteTransferHistor] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<UserSiteTransferHistor> GetList()
        {
            List<UserSiteTransferHistor> RecordsList = new List<UserSiteTransferHistor>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [UserId],[SiteIdOld],[SiteIdNew],[TransferedBy],[CreatedUserName],[TransferedDate],[Remark] FROM [dbo].[UserSiteTransferHistor] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserSiteTransferHistor objUserSiteTransferHistor = new UserSiteTransferHistor();
                    if (dr["UserId"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.UserId = Guid.Empty;
                    else
                        objUserSiteTransferHistor.UserId = (Guid)dr["UserId"];
                    if (dr["SiteIdOld"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteIdOld = 0;
                    else
                        objUserSiteTransferHistor.SiteIdOld = (int)dr["SiteIdOld"];
                    if (dr["SiteIdNew"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteIdNew = 0;
                    else
                        objUserSiteTransferHistor.SiteIdNew = (int)dr["SiteIdNew"];
                    if (dr["TransferedBy"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedBy = Guid.Empty;
                    else
                        objUserSiteTransferHistor.TransferedBy = (Guid)dr["TransferedBy"];
                    if (dr["CreatedUserName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.CreatedUserName = string.Empty;
                    else
                        objUserSiteTransferHistor.CreatedUserName = (string)dr["CreatedUserName"];
                    if (dr["TransferedDate"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedDate = DateTime.MinValue;
                    else
                        objUserSiteTransferHistor.TransferedDate = (DateTime)dr["TransferedDate"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.Remark = string.Empty;
                    else
                        objUserSiteTransferHistor.Remark = (string)dr["Remark"];
                    RecordsList.Add(objUserSiteTransferHistor);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[UserSiteTransferHistor] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateTaxCenter(Guid UserId, int taxCenterId, SqlCommand command)
        {

            string strUpdate = @"UPDATE [dbo].[aspnet_Users] SET     [TaxCenterId]=@TaxCenterId Where UserId = @UserId";
            command.CommandText = strUpdate;
            command.Parameters.Clear();
            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", UserId));
                command.Parameters.Add(new SqlParameter("@TaxCenterId", taxCenterId));
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::Update::Error!" + ex.Message, ex);
            }
            
        }
        public List<UserSiteTransferHistor> GetSiteTransferDetails()
        {
            List<UserSiteTransferHistor> RecordsList = new List<UserSiteTransferHistor>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT distinct [dbo].[aspnet_Users].[UserName],  A.CenterNameEnglish as SiteIdOldName, B.CenterNameEnglish as SiteIdNewName, 
				[dbo].[UserSiteTransferHistor].[CreatedUserName] as TransferedByName, TransferedDate,[dbo].[UserSiteTransferHistor].[Remark] FROM [dbo].[UserSiteTransferHistor] 
				INNER JOIN [dbo].[aspnet_Users] on [dbo].[UserSiteTransferHistor] .UserId = [dbo].[aspnet_Users].UserId
				INNER JOIN [dbo].[TaxCenter] A on [dbo].[UserSiteTransferHistor].SiteIdOld = A.[Id]
				INNER JOIN [dbo].[TaxCenter] B on [dbo].[UserSiteTransferHistor].SiteIdNew = B.[Id] order by TransferedDate desc ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserSiteTransferHistor objUserSiteTransferHistor = new UserSiteTransferHistor();
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.UserName  = string.Empty;
                    else
                        objUserSiteTransferHistor.UserName = (string)dr["UserName"];
                    if (dr["SiteIdOldName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteOldName = string.Empty;
                    else
                        objUserSiteTransferHistor.SiteOldName = (string)dr["SiteIdOldName"];

                    if (dr["SiteIdNewName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.SiteNewName = string.Empty;
                    else
                        objUserSiteTransferHistor.SiteNewName = (string)dr["SiteIdNewName"];
                    if (dr["TransferedByName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedByName = string.Empty;
                    else
                        objUserSiteTransferHistor.TransferedByName = (string)dr["TransferedByName"];
                    if (dr["TransferedByName"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedByName = string.Empty;
                    else
                        objUserSiteTransferHistor.TransferedByName = (string)dr["TransferedByName"];
                    if (dr["TransferedDate"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.TransferedDate = DateTime.MinValue;
                    else
                        objUserSiteTransferHistor.TransferedDate = (DateTime)dr["TransferedDate"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objUserSiteTransferHistor.Remark = string.Empty;
                    else
                        objUserSiteTransferHistor.Remark = (string)dr["Remark"];
                    RecordsList.Add(objUserSiteTransferHistor);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("UserSiteTransferHistor::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
    }
}