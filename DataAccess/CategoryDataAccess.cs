﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class CategoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[Code],[Description],[Parent],[LicensePayment],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[WorkingDays] FROM [dbo].[Category]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Category");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public CategoryDomain GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CategoryDomain objCategory = new CategoryDomain();
            string strGetRecord = @"SELECT [Id],[Code],[Description],[DescriptionEng],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy],[OtherService] FROM [dbo].[Category] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Category");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objCategory.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dTable.Rows[0]["Code"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["DescriptionEng"].Equals(DBNull.Value))
                        objCategory.DescriptionEng = string.Empty;
                    else
                        objCategory.DescriptionEng = (string)dTable.Rows[0]["DescriptionEng"];
                    if (dTable.Rows[0]["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dTable.Rows[0]["Parent"];
                    
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dTable.Rows[0]["CreatedUserId"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dTable.Rows[0]["UpdatedUserId"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];

                    if (dTable.Rows[0]["OtherService"].Equals(DBNull.Value))
                        objCategory.OtherServices = 0;
                    else
                        objCategory.OtherServices = (int)dTable.Rows[0]["OtherService"];

                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCategory;
        }
        public CategoryDomain GetCategoryByCode(string code)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CategoryDomain objCategory = new CategoryDomain();
            string strGetRecord = @"SELECT [Id],[Code],[Description],[DescriptionEng],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[Category] WHERE [Code]=@Code";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Category");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Code", code));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objCategory.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dTable.Rows[0]["Code"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["DescriptionEng"].Equals(DBNull.Value))
                        objCategory.DescriptionEng = string.Empty;
                    else
                        objCategory.DescriptionEng = (string)dTable.Rows[0]["DescriptionEng"];
                    if (dTable.Rows[0]["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dTable.Rows[0]["Parent"];

                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dTable.Rows[0]["CreatedUserId"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dTable.Rows[0]["UpdatedUserId"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];

                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCategory;
        }

        public string GetDescription(int categoryId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Description] from Category where  [Id] = @Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Id", categoryId);
            command.Connection = connection;

            try
            {
                connection.Open();
                return (string)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetDescription::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Insert(CategoryDomain objCategory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Category]
                                            ([Code],[Description],[DescriptionEng],[SortValue],[SoundxValue],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[OtherService])
                                     VALUES    (@Code,@Description,@DescriptionEng,@SortValue,@SoundxValue,@Parent,@IsActive,@IsDeleted,@CreatedUserId,@CreatedDate,@CreatedBy,@OtherService)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Code", objCategory.Code));
                command.Parameters.Add(new SqlParameter("@Description", objCategory.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionEng", objCategory.DescriptionEng));
                command.Parameters.Add(new SqlParameter("@SortValue", objCategory.SortValue));
                command.Parameters.Add(new SqlParameter("@SoundxValue", objCategory.SoundxValue));
                command.Parameters.Add(new SqlParameter("@Parent", objCategory.Parent));
                command.Parameters.Add(new SqlParameter("@IsActive", objCategory.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objCategory.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objCategory.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objCategory.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objCategory.CreatedBy));
                command.Parameters.Add(new SqlParameter("@OtherService", objCategory.OtherServices));



                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Category::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(CategoryDomain objCategory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Category] SET     [Code]=@Code,    [Description]=@Description,SortValue = @SortValue,SoundxValue=@SoundxValue,[DescriptionEng]=@DescriptionEng,    [Parent]=@Parent, [IsActive]=@IsActive,    [IsDeleted]=@IsDeleted,   [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy, [OtherService] = @OtherService WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objCategory.Id));
                command.Parameters.Add(new SqlParameter("@Code", objCategory.Code));
                command.Parameters.Add(new SqlParameter("@Description", objCategory.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionEng", objCategory.DescriptionEng));
                command.Parameters.Add(new SqlParameter("@SortValue", objCategory.SortValue));
                command.Parameters.Add(new SqlParameter("@SoundxValue", objCategory.SoundxValue));
                command.Parameters.Add(new SqlParameter("@Parent", objCategory.Parent));
                command.Parameters.Add(new SqlParameter("@IsActive", objCategory.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objCategory.IsDeleted));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objCategory.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objCategory.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objCategory.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@OtherService", objCategory.OtherServices));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Category::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Category] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Category::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<CategoryDomain> GetList()
        {
            List<CategoryDomain> RecordsList = new List<CategoryDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Description],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[Category]  ORDER BY  [Code] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryDomain objCategory = new CategoryDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategory.Id = 0;
                    else
                        objCategory.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dr["Description"];
                    if (objCategory.Description.Length > 30)
                        objCategory.DescriptionShort = objCategory.Description.Substring(0, 19);
                    else
                        objCategory.DescriptionShort = objCategory.Description;
                    objCategory.Description += "/ " + objCategory.Code;
                    if (dr["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dr["Parent"];
                    
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dr["UpdatedBy"];
                    
                    RecordsList.Add(objCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<CategoryDomain> GetList(bool otherService)
        {
            List<CategoryDomain> RecordsList = new List<CategoryDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Description],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],
                                        [UpdatedDate],[UpdatedBy] FROM [dbo].[Category] where 1=1 ";
            if (otherService)
                strGetAllRecords += " and OtherService = 1";
            else
                strGetAllRecords += " and OtherService = 0";

            strGetAllRecords += " ORDER BY  [Code] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryDomain objCategory = new CategoryDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategory.Id = 0;
                    else
                        objCategory.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dr["Description"];
                    if (objCategory.Description.Length > 30)
                        objCategory.DescriptionShort = objCategory.Description.Substring(0, 19);
                    else
                        objCategory.DescriptionShort = objCategory.Description;
                    objCategory.Description += "/ " + objCategory.Code;
                    if (dr["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dr["Parent"];

                    if (dr["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dr["UpdatedBy"];

                    RecordsList.Add(objCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<CategoryDomain> SearchCategory(string text,bool otherService)
        {
            List<CategoryDomain> RecordsList = new List<CategoryDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Description],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[Category] 
                                       where  ([SortValue] like  '" +text +"%' or code like '" +text +"%' ) ";
            if (otherService)
                strGetAllRecords += " and OtherService = 1   ORDER BY  [Code] ASC";
            else
                strGetAllRecords += " and OtherService = 0   ORDER BY  [Code] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryDomain objCategory = new CategoryDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategory.Id = 0;
                    else
                        objCategory.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dr["Description"];
                    if (objCategory.Description.Length > 30)
                        objCategory.DescriptionShort = objCategory.Description.Substring(0, 19);
                    else
                        objCategory.DescriptionShort = objCategory.Description;
                    objCategory.Description += "/ " + objCategory.Code;
                    if (dr["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dr["Parent"];

                    if (dr["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dr["UpdatedBy"];

                    RecordsList.Add(objCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<CategoryDomain> GetList(string Parent)
        {
            List<CategoryDomain> RecordsList = new List<CategoryDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[Description],[Parent],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[Category] where Parent = @Parent  ORDER BY  [Code] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Parent", Parent);
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryDomain objCategory = new CategoryDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategory.Id = 0;
                    else
                        objCategory.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objCategory.Description = string.Empty;
                    else
                        objCategory.Description = (string)dr["Description"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objCategory.Parent = 0;
                    else
                        objCategory.Parent = (int)dr["Parent"];
                    
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objCategory.IsActive = false;
                    else
                        objCategory.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objCategory.IsDeleted = false;
                    else
                        objCategory.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objCategory.CreatedUserId = 0;
                    else
                        objCategory.CreatedUserId = (int)dr["CreatedUserId"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objCategory.CreatedDate = DateTime.MinValue;
                    else
                        objCategory.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objCategory.CreatedBy = string.Empty;
                    else
                        objCategory.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objCategory.UpdatedUserId = 0;
                    else
                        objCategory.UpdatedUserId = (int)dr["UpdatedUserId"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objCategory.UpdatedDate = DateTime.MinValue;
                    else
                        objCategory.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objCategory.UpdatedBy = string.Empty;
                    else
                        objCategory.UpdatedBy = (string)dr["UpdatedBy"];
                    
                    RecordsList.Add(objCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<CategoryDomain> GetParentCategorys()
        {
            List<CategoryDomain> RecordsList = new List<CategoryDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code] from Category where Parent ='-1' ORDER BY  [Id] Desc";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CategoryDomain objCategory = new CategoryDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objCategory.Id = 0;
                    else
                        objCategory.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objCategory.Code = string.Empty;
                    else
                        objCategory.Code = (string)dr["Code"];
                    
                    RecordsList.Add(objCategory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Category::GetParentCategory::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string Code, string SortValue, string DescriptionEng)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Category] WHERE [Code]=@Code or [SortValue] = @SortValue ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Code", Code);
            command.Parameters.AddWithValue("@SortValue", SortValue);
            command.Parameters.AddWithValue("@DescriptionEng", DescriptionEng);
            try
            {
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Category::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateExists(int Id, string Code, string SortValue, string DescriptionEng)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Category] WHERE Id <> @Id and ( [Code]=@Code or 
                                [SortValue] = @SortValue )";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@Id", Id);
            command.Parameters.AddWithValue("@Code", Code);
            command.Parameters.AddWithValue("@SortValue", SortValue);
            command.Parameters.AddWithValue("@DescriptionEng", DescriptionEng);
            try
            {
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Category::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

    }
}