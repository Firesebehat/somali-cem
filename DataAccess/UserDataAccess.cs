﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class UserDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public List<Domain.User> GetUsers()
        {
            List<Domain.User> RecordsList = new List<Domain.User>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            #region Query
            string strSQL = @"SELECT distinct UserName, FullName, CenterNameEnglish TaxCenterName,TaxCenterId FROM ASPNET_USERS INNER JOIN  TaxCenter ON aspnet_Users.TaxCenterId = TaxCenter.Id";
            #endregion
            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Domain.User objUser = new Domain.User();
                   
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];
                    if (dr["FullName"].Equals(DBNull.Value))
                        objUser.FullName = string.Empty;
                    else
                        objUser.FullName = (string)dr["FullName"];
                    if (dr["FullName"].Equals(DBNull.Value))
                        objUser.FullName = string.Empty;
                    else
                        objUser.FullName = (string)dr["FullName"];
                    if (dr["TaxCenterName"].Equals(DBNull.Value))
                        objUser.TaxCenterName = string.Empty;
                    else
                        objUser.TaxCenterName = (string)dr["TaxCenterName"];
                    if (dr["TaxCenterId"].Equals(DBNull.Value))
                        objUser.TaxCenterId = -1;
                    else
                        objUser.TaxCenterId = (int)dr["TaxCenterId"];

                    RecordsList.Add(objUser);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("User::GetAdminUsers::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [ApplicationId],[UserId],[UserName],[LoweredUserName],[MobileAlias],[IsAnonymous],[LastActivityDate],[TaxCenterId],[UserGroupId] FROM [dbo].[User]  ORDER BY  [UserId] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("User::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public bool InsertUseLogInHistory(string userName, string ip, string mac)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            User objUser = new User();
            string strGetRecord = @"INSERT INTO Logged_User_History (UserName,IPAddress,MACAddress,LoggingDate) VALUES (@UserName,@IPAddress,@MACAddress,@LoggingDate)";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserName", userName));
                command.Parameters.Add(new SqlParameter("@IPAddress", ip));
                command.Parameters.Add(new SqlParameter("@MACAddress", mac));
                command.Parameters.Add(new SqlParameter("@LoggingDate", DateTime.Now));
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("User::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return true;
        }

        public User GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            User objUser = new User();
            string strGetRecord = @"SELECT [ApplicationId],[UserId],[UserName],[LoweredUserName],[MobileAlias],[IsAnonymous],[LastActivityDate],[TaxCenterId],[UserGroupId] FROM [dbo].[User] WHERE [UserId]=@UserId";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objUser.ApplicationId = (Guid)dTable.Rows[0]["ApplicationId"];
                    objUser.UserId = (Guid)dTable.Rows[0]["UserId"];
                    objUser.UserName = (string)dTable.Rows[0]["UserName"];
                    objUser.LoweredUserName = (string)dTable.Rows[0]["LoweredUserName"];
                    if (dTable.Rows[0]["MobileAlias"].Equals(DBNull.Value))
                        objUser.MobileAlias = string.Empty;
                    else
                        objUser.MobileAlias = (string)dTable.Rows[0]["MobileAlias"];
                    objUser.IsAnonymous = (bool)dTable.Rows[0]["IsAnonymous"];
                    objUser.LastActivityDate = (DateTime)dTable.Rows[0]["LastActivityDate"];
                    if (dTable.Rows[0]["TaxCenterId"].Equals(DBNull.Value))
                        objUser.TaxCenterId = 0;
                    else
                        objUser.TaxCenterId = (int)dTable.Rows[0]["TaxCenterId"];
                    if (dTable.Rows[0]["UserGroupId"].Equals(DBNull.Value))
                        objUser.UserGroupId = 0;
                    else
                        objUser.UserGroupId = (int)dTable.Rows[0]["UserGroupId"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("User::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objUser;
        }
        public User GetUserByName(string UserName)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            User objUser = new User();
            string strGetRecord = @"SELECT * FROM aspnet_Users WHERE [UserName]=@UserName";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserName", UserName));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objUser.ApplicationId = (Guid)dTable.Rows[0]["ApplicationId"];
                    objUser.UserId = (Guid)dTable.Rows[0]["UserId"];
                    objUser.UserName = (string)dTable.Rows[0]["UserName"];
                    objUser.LoweredUserName = (string)dTable.Rows[0]["LoweredUserName"];
                    if (dTable.Rows[0]["MobileAlias"].Equals(DBNull.Value))
                        objUser.MobileAlias = string.Empty;
                    else
                        objUser.MobileAlias = (string)dTable.Rows[0]["MobileAlias"];
                    objUser.IsAnonymous = (bool)dTable.Rows[0]["IsAnonymous"];
                    objUser.LastActivityDate = (DateTime)dTable.Rows[0]["LastActivityDate"];
                    if (dTable.Rows[0]["TaxCenterId"].Equals(DBNull.Value))
                        objUser.TaxCenterId = 0;
                    else
                        objUser.TaxCenterId = (int)dTable.Rows[0]["TaxCenterId"];
                    if (dTable.Rows[0]["UserGroupId"].Equals(DBNull.Value))
                        objUser.UserGroupId = 0;
                    else
                        objUser.UserGroupId = (int)dTable.Rows[0]["UserGroupId"];           
                    if (dTable.Rows[0]["MobileNo"].Equals(DBNull.Value))
                        objUser.MobileNo = string.Empty;
                    else
                        objUser.MobileNo = (string)dTable.Rows[0]["MobileNo"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("User::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objUser;
        }

        public int GetNextUserNo()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"select (next value for [dbo].[seq_UserNo] )";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw new Exception("User::GetNextUserNo::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<UserProfile> GetAdminUsers(string strName, bool isActiveUser, string OrgCode, int intStartRowIndex, int intMaxRows)
        {
            List<UserProfile> RecordsList = new List<UserProfile>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            #region Query
            string strSQL = @"SELECT 
                            distinct 
						RowNumber, 
						UserName, 
						Email, 
						IsApproved, 
						IsLockedOut, 
						CreateDate, 
						LastLoginDate, 
						LastActivityDate,
						CreatedBy, 
						ApprovedBy
						
				FROM 
					(
						SELECT distinct	ROW_NUMBER() OVER (ORDER BY aspnet_Users.UserName DESC) AS RowNumber, 
								aspnet_Users.UserName, 
								aspnet_Membership.Email, 
								aspnet_Membership.IsApproved, 
								aspnet_Membership.IsLockedOut, 
								aspnet_Membership.CreateDate, 
								aspnet_Membership.LastLoginDate, 
								aspnet_Users.LastActivityDate,
								aspnet_Users.CreatedBy, 
								aspnet_Users.ApprovedBy,
                                aspnet_Users.Site
						FROM	aspnet_Membership 
						INNER   JOIN aspnet_Users 
						ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						
						WHERE	UserName LIKE @UserName 
					 ) as u
				WHERE	u.RowNumber > @startRowIndex 
				AND		u.RowNumber <= (@startRowIndex + @maximumRows) ";
            if (isActiveUser)
                strSQL += " and isApproved = 1";
            if (OrgCode != string.Empty && OrgCode != "0")
            {
                strSQL += " AND u.Site = @OrgCode";
            }
            strSQL += " order by UserName";
            #endregion

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.AddWithValue("@UserName", strName);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
                command.Parameters.AddWithValue("@IsApproved", isActiveUser);
                command.Parameters.AddWithValue("@OrgCode", OrgCode);

                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserProfile objUser = new UserProfile();
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];

                    if (dr["RowNumber"].Equals(DBNull.Value))
                        objUser.RowNumber = 0;
                    else
                        objUser.RowNumber = Convert.ToInt32(dr["RowNumber"]);

                    if (dr["Email"].Equals(DBNull.Value))
                        objUser.Email = string.Empty;
                    else
                        objUser.Email = (string)dr["Email"];
                    if (dr["IsApproved"].Equals(DBNull.Value))
                        objUser.IsApproved = false;
                    else
                        objUser.IsApproved = (bool)dr["IsApproved"];
                    if (dr["IsLockedOut"].Equals(DBNull.Value))
                        objUser.IsLockedOut = false;
                    else
                        objUser.IsLockedOut = (bool)dr["IsLockedOut"];

                    if (dr["CreateDate"].Equals(DBNull.Value))
                        objUser.CreateDate = DateTime.Now;
                    else
                        objUser.CreateDate = (DateTime)dr["CreateDate"];
                    if (dr["LastLoginDate"].Equals(DBNull.Value))
                        objUser.LastLoginDate = DateTime.Now;
                    else
                        objUser.LastLoginDate = (DateTime)dr["LastLoginDate"];

                    if (dr["LastActivityDate"].Equals(DBNull.Value))
                        objUser.LastActivityDate = DateTime.Now;
                    else
                        objUser.LastActivityDate = (DateTime)dr["LastActivityDate"];

                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objUser.CreatedBy = string.Empty;
                    else
                        objUser.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["ApprovedBy"].Equals(DBNull.Value))
                        objUser.ApprovedBy = string.Empty;
                    else
                        objUser.ApprovedBy = (string)dr["ApprovedBy"];

                    // if(IsAdmin(objUser.UserName) || IsSuperAdmin(objUser.UserName) || IsRegionalAdmin(objUser.UserName))
                    RecordsList.Add(objUser);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("User::GetAdminUsers::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Insert(User objUser)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[User]
                                            ([ApplicationId],[UserId],[UserName],[LoweredUserName],[MobileAlias],[IsAnonymous],[LastActivityDate],[TaxCenterId],[UserGroupId])
                                     VALUES    (@ApplicationId, ISNULL(@UserId, (newid())),@UserName,@LoweredUserName,@MobileAlias, ISNULL(@IsAnonymous, ((0))),@LastActivityDate,@TaxCenterId,@UserGroupId)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicationId", objUser.ApplicationId));
                command.Parameters.Add(new SqlParameter("@UserId", objUser.UserId));
                command.Parameters.Add(new SqlParameter("@UserName", objUser.UserName));
                command.Parameters.Add(new SqlParameter("@LoweredUserName", objUser.LoweredUserName));
                command.Parameters.Add(new SqlParameter("@MobileAlias", objUser.MobileAlias));
                command.Parameters.Add(new SqlParameter("@IsAnonymous", objUser.IsAnonymous));
                command.Parameters.Add(new SqlParameter("@LastActivityDate", objUser.LastActivityDate));
                command.Parameters.Add(new SqlParameter("@TaxCenterId", objUser.TaxCenterId));
                command.Parameters.Add(new SqlParameter("@UserGroupId", objUser.UserGroupId));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("User::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(User objUser)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[User] SET     [ApplicationId]=@ApplicationId,    [UserId]=@UserId,    [UserName]=@UserName,    [LoweredUserName]=@LoweredUserName,    [MobileAlias]=@MobileAlias,    [IsAnonymous]=@IsAnonymous,    [LastActivityDate]=@LastActivityDate,    [TaxCenterId]=@TaxCenterId,    [UserGroupId]=@UserGroupId WHERE [UserId]=@UserId";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicationId", objUser.ApplicationId));
                command.Parameters.Add(new SqlParameter("@UserId", objUser.UserId));
                command.Parameters.Add(new SqlParameter("@UserName", objUser.UserName));
                command.Parameters.Add(new SqlParameter("@LoweredUserName", objUser.LoweredUserName));
                command.Parameters.Add(new SqlParameter("@MobileAlias", objUser.MobileAlias));
                command.Parameters.Add(new SqlParameter("@IsAnonymous", objUser.IsAnonymous));
                command.Parameters.Add(new SqlParameter("@LastActivityDate", objUser.LastActivityDate));
                command.Parameters.Add(new SqlParameter("@TaxCenterId", objUser.TaxCenterId));
                command.Parameters.Add(new SqlParameter("@UserGroupId", objUser.UserGroupId));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("User::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[User] WHERE [UserId]=@UserId";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("User::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<User> GetList()
        {
            List<User> RecordsList = new List<User>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [ApplicationId],[UserId],[UserName],[LoweredUserName],[MobileAlias],[IsAnonymous],[LastActivityDate],[TaxCenterId],[UserGroupId] FROM [dbo].[User]  ORDER BY  [UserId] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    User objUser = new User();
                    if (dr["ApplicationId"].Equals(DBNull.Value))
                        objUser.ApplicationId = Guid.Empty;
                    else
                        objUser.ApplicationId = (Guid)dr["ApplicationId"];
                    if (dr["UserId"].Equals(DBNull.Value))
                        objUser.UserId = Guid.Empty;
                    else
                        objUser.UserId = (Guid)dr["UserId"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];
                    if (dr["LoweredUserName"].Equals(DBNull.Value))
                        objUser.LoweredUserName = string.Empty;
                    else
                        objUser.LoweredUserName = (string)dr["LoweredUserName"];
                    if (dr["MobileAlias"].Equals(DBNull.Value))
                        objUser.MobileAlias = string.Empty;
                    else
                        objUser.MobileAlias = (string)dr["MobileAlias"];
                    if (dr["IsAnonymous"].Equals(DBNull.Value))
                        objUser.IsAnonymous = false;
                    else
                        objUser.IsAnonymous = (bool)dr["IsAnonymous"];
                    if (dr["LastActivityDate"].Equals(DBNull.Value))
                        objUser.LastActivityDate = DateTime.MinValue;
                    else
                        objUser.LastActivityDate = (DateTime)dr["LastActivityDate"];
                    if (dr["TaxCenterId"].Equals(DBNull.Value))
                        objUser.TaxCenterId = 0;
                    else
                        objUser.TaxCenterId = (int)dr["TaxCenterId"];
                    if (dr["UserGroupId"].Equals(DBNull.Value))
                        objUser.UserGroupId = 0;
                    else
                        objUser.UserGroupId = (int)dr["UserGroupId"];
                    RecordsList.Add(objUser);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("User::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Role> GetRoles()
        {
            List<Role> RecordsList = new List<Role>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"select RoleName, count(uir.UserId) UserCount,[dbo].[UsersOfRole](ur.roleId) UsersOfRole,
			[Description] from aspnet_Roles   ur left outer join aspnet_UsersInRoles uir 
			on uir.RoleId = ur.RoleId group by RoleName, ur.RoleId,[Description]  ORDER BY  [ur].[RoleName] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Role objRole = new Role();
                    if (dr["RoleName"].Equals(DBNull.Value))
                        objRole.RoleName = string.Empty;
                    else
                        objRole.RoleName = (string)dr["RoleName"];
                    if (dr["UsersOfRole"].Equals(DBNull.Value))
                        objRole.UsersOfRole = string.Empty;
                    else
                        objRole.UsersOfRole = (string)dr["UsersOfRole"];
                    if (dr["UserCount"].Equals(DBNull.Value))
                        objRole.UserCount = 0;
                    else
                        objRole.UserCount = (int)dr["UserCount"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objRole.Description = string.Empty;
                    else
                        objRole.Description = (string)dr["Description"];

                    RecordsList.Add(objRole);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("User::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT UserId FROM [dbo].[User] WHERE [UserId]=@UserId";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserId", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("User::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public void UpdateUser(User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[aspnet_Users] SET     [FullName]=@FullName,    [TaxCenterId]=@TaxCenterId,    [UserGroupId]=@UserGroupId ,MobileNo=@MobileNo
                                  WHERE [UserName]=@UserName";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FullName", user.FullName));
                command.Parameters.Add(new SqlParameter("@UserGroupId", user.UserGroupId));
                command.Parameters.Add(new SqlParameter("@TaxCenterId", user.TaxCenterId));
                command.Parameters.Add(new SqlParameter("@UserName", user.UserName));
                command.Parameters.Add(new SqlParameter("@MobileNo", user.MobileNo));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("User::UpdateUserExension::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public void UpdateUserExension(User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[aspnet_Users] SET     [FullName]=@FullName,    [TaxCenterId]=@TaxCenterId,    [UserGroupId]=@UserGroupId,  
                                  [UserHash] = @UserHash WHERE [UserName]=@UserName";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FullName", user.FullName));
                command.Parameters.Add(new SqlParameter("@UserGroupId", user.UserGroupId));
                command.Parameters.Add(new SqlParameter("@TaxCenterId", user.TaxCenterId));
                command.Parameters.Add(new SqlParameter("@UserName", user.UserName));
                command.Parameters.Add(new SqlParameter("@UserHash", user.UserHash));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("User::UpdateUserExension::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        public bool UpdateHash(User user)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[aspnet_users] SET  [UserHash] = @UserHash WHERE [UserName]=@UserName";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserHash", user.UserHash));
                command.Parameters.Add(new SqlParameter("@UserName", user.UserName));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateFullName::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        public User GetUserHashCode(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            User User = new User();
            string strGetRecord = @"SELECT [TaxCenterId],[UserHash]  FROM [dbo].[aspnet_users] WHERE [UserName]=@UserName";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@UserName", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    User.TaxCenterId = Convert.ToInt32(dTable.Rows[0]["TaxCenterId"]);
                    User.UserHash = dTable.Rows[0]["UserHash"]==null? string.Empty: dTable.Rows[0]["UserHash"].ToString();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("UserHash::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return User;
        }
    }
}