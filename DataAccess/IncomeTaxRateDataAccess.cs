﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;

namespace CUSTOR.DataAccess
{
    public class IncomeTaxRateDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[IncomeFrom],[IncomeTo],[Rate],[DateFrom],[DateTo],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[IncomeTaxRate]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("IncomeTaxRate");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public IncomeTaxRate GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            IncomeTaxRate objIncomeTaxRate = new IncomeTaxRate();
            string strGetRecord = @"SELECT [Id],[IncomeFrom],[IncomeTo],[Rate],[DateFrom],[DateTo],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[IncomeTaxRate] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("IncomeTaxRate");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objIncomeTaxRate.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["IncomeFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeFrom = 0;
                    else
                        objIncomeTaxRate.IncomeFrom = (double)dTable.Rows[0]["IncomeFrom"];
                    if (dTable.Rows[0]["IncomeTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeTo = 0;
                    else
                        objIncomeTaxRate.IncomeTo = (double)dTable.Rows[0]["IncomeTo"];
                    if (dTable.Rows[0]["Rate"].Equals(DBNull.Value))
                        objIncomeTaxRate.Rate = 0;
                    else
                        objIncomeTaxRate.Rate = (double)dTable.Rows[0]["Rate"];
                    if (dTable.Rows[0]["DateFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateFrom = DateTime.MinValue;
                    else
                        objIncomeTaxRate.DateFrom = (DateTime)dTable.Rows[0]["DateFrom"];
                    if (dTable.Rows[0]["DateTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateTo = DateTime.MinValue;
                    else
                        objIncomeTaxRate.DateTo = (DateTime)dTable.Rows[0]["DateTo"];
                    objIncomeTaxRate.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    objIncomeTaxRate.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedDate = DateTime.MinValue;
                    else
                        objIncomeTaxRate.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedBy = string.Empty;
                    else
                        objIncomeTaxRate.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objIncomeTaxRate;
        }

        public bool Insert(IncomeTaxRate objIncomeTaxRate)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[IncomeTaxRate]
                                            ([IncomeFrom],[IncomeTo],[Rate],[DateFrom],[DateTo],[CreatedDate],[CreatedBy],[BudgetYear])
                                     VALUES    (@IncomeFrom,@IncomeTo,@Rate,@DateFrom,@DateTo,@CreatedDate,@CreatedBy,@BudgetYear)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@IncomeFrom", objIncomeTaxRate.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", objIncomeTaxRate.IncomeTo));
                command.Parameters.Add(new SqlParameter("@Rate", objIncomeTaxRate.Rate));
                command.Parameters.Add(new SqlParameter("@DateFrom", objIncomeTaxRate.DateFrom));
                if(objIncomeTaxRate.DateTo != DateTime.MinValue)
                command.Parameters.Add(new SqlParameter("@DateTo", objIncomeTaxRate.DateTo));
                else
                    command.Parameters.Add(new SqlParameter("@DateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@CreatedDate", objIncomeTaxRate.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objIncomeTaxRate.CreatedBy));
                command.Parameters.Add(new SqlParameter("@BudgetYear", objIncomeTaxRate.BudgetYear));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(IncomeTaxRate objIncomeTaxRate)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[IncomeTaxRate] SET     [IncomeFrom]=@IncomeFrom,    [IncomeTo]=@IncomeTo,    [Rate]=@Rate,    [DateFrom]=@DateFrom,    [DateTo]=@DateTo,    [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objIncomeTaxRate.Id));
                command.Parameters.Add(new SqlParameter("@IncomeFrom", objIncomeTaxRate.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", objIncomeTaxRate.IncomeTo));
                command.Parameters.Add(new SqlParameter("@Rate", objIncomeTaxRate.Rate));
                command.Parameters.Add(new SqlParameter("@DateFrom", objIncomeTaxRate.DateFrom));

                if (objIncomeTaxRate.DateTo != DateTime.MinValue)
                command.Parameters.Add(new SqlParameter("@DateTo", objIncomeTaxRate.DateTo));
                else
                command.Parameters.Add(new SqlParameter("@DateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@UpdatedDate", objIncomeTaxRate.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objIncomeTaxRate.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@BudgetYear", objIncomeTaxRate.BudgetYear));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[IncomeTaxRate] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<IncomeTaxRate> GetList()
        {
            List<IncomeTaxRate> RecordsList = new List<IncomeTaxRate>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[IncomeFrom],[IncomeTo],[Rate],[DateFrom],[DateTo],[CreatedDate],[CreatedBy],[UpdatedDate],
                                      [UpdatedBy] FROM [dbo].[IncomeTaxRate]   ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    IncomeTaxRate objIncomeTaxRate = new IncomeTaxRate();
                    if (dr["Id"].Equals(DBNull.Value))
                        objIncomeTaxRate.Id = 0;
                    else
                        objIncomeTaxRate.Id = (int)dr["Id"];
                    if (dr["IncomeFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeFrom = 0;
                    else
                        objIncomeTaxRate.IncomeFrom = (double)dr["IncomeFrom"];
                    if (dr["IncomeTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeTo = 0;
                    else
                        objIncomeTaxRate.IncomeTo = (double)dr["IncomeTo"];
                    if (dr["Rate"].Equals(DBNull.Value))
                        objIncomeTaxRate.Rate = 0;
                    else
                        objIncomeTaxRate.Rate = (double)dr["Rate"];
                    if (dr["DateFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateFrom = DateTime.MinValue;
                    else
                        objIncomeTaxRate.DateFrom = (DateTime)dr["DateFrom"];
                    if (dr["DateTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateTo = DateTime.MinValue;
                    else
                        objIncomeTaxRate.DateTo = (DateTime)dr["DateTo"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objIncomeTaxRate.CreatedDate = DateTime.MinValue;
                    else
                        objIncomeTaxRate.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objIncomeTaxRate.CreatedBy = string.Empty;
                    else
                        objIncomeTaxRate.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedDate = DateTime.MinValue;
                    else
                        objIncomeTaxRate.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedBy = string.Empty;
                    else
                        objIncomeTaxRate.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objIncomeTaxRate);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<IncomeTaxRate> GetList(int budgetYear)
        {
            List<IncomeTaxRate> RecordsList = new List<IncomeTaxRate>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"set dateformat ymd SELECT [Id],[IncomeFrom],[IncomeTo],[Rate],[DateFrom],[DateTo],[CreatedDate],[CreatedBy],[UpdatedDate],
                                      [UpdatedBy],[BudgetYear] FROM [dbo].[IncomeTaxRate] where BudgetYear = @BudgetYear  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@BudgetYear", budgetYear);
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    IncomeTaxRate objIncomeTaxRate = new IncomeTaxRate();
                    if (dr["Id"].Equals(DBNull.Value))
                        objIncomeTaxRate.Id = 0;
                    else
                        objIncomeTaxRate.Id = (int)dr["Id"];
                    if (dr["IncomeFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeFrom = 0;
                    else
                        objIncomeTaxRate.IncomeFrom = (double)dr["IncomeFrom"];
                    if (dr["IncomeTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.IncomeTo = 0;
                    else
                        objIncomeTaxRate.IncomeTo = (double)dr["IncomeTo"];
                    if (dr["Rate"].Equals(DBNull.Value))
                        objIncomeTaxRate.Rate = 0;
                    else
                        objIncomeTaxRate.Rate = (double)dr["Rate"];
                    if (dr["DateFrom"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateFrom = DateTime.MinValue;

                    else
                        objIncomeTaxRate.DateFrom = (DateTime)dr["DateFrom"];
                    objIncomeTaxRate.DateFromText = objIncomeTaxRate.DateFrom== DateTime.MinValue?string.Empty:EthiopicDateTime.GetEthiopianDate(objIncomeTaxRate.DateFrom);
                    if (dr["DateTo"].Equals(DBNull.Value))
                        objIncomeTaxRate.DateTo = DateTime.MinValue;
                    else
                        objIncomeTaxRate.DateTo = (DateTime)dr["DateTo"];
                    objIncomeTaxRate.DateToText = objIncomeTaxRate.DateTo == DateTime.MinValue ? string.Empty : EthiopicDateTime.GetEthiopianDate(objIncomeTaxRate.DateTo);

                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objIncomeTaxRate.CreatedDate = DateTime.MinValue;
                    else
                        objIncomeTaxRate.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objIncomeTaxRate.CreatedBy = string.Empty;
                    else
                        objIncomeTaxRate.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedDate = DateTime.MinValue;
                    else
                        objIncomeTaxRate.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objIncomeTaxRate.UpdatedBy = string.Empty;
                    else
                        objIncomeTaxRate.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["BudgetYear"].Equals(DBNull.Value))
                        objIncomeTaxRate.BudgetYear = 0;
                    else
                        objIncomeTaxRate.BudgetYear = (int)dr["BudgetYear"];
                    RecordsList.Add(objIncomeTaxRate);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(IncomeTaxRate rate)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[IncomeTaxRate]   WHERE DateFrom = @DateFrom  and (IncomeFrom >= @IncomeFrom and IncomeTo <= @IncomeTo) and BudgetYear = @BudgetYear";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@DateFrom", rate.DateFrom));
                command.Parameters.Add(new SqlParameter("@IncomeFrom", rate.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", rate.IncomeTo));
                command.Parameters.Add(new SqlParameter("@BudgetYear", rate.BudgetYear));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateExists(IncomeTaxRate rate)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[IncomeTaxRate]   WHERE DateFrom >= @DateFrom  and (IncomeFrom >= @IncomeFrom and IncomeTo <= @IncomeTo) and Id <> @Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", rate.Id));
                command.Parameters.Add(new SqlParameter("@DateFrom", rate.DateFrom));
                command.Parameters.Add(new SqlParameter("@IncomeFrom", rate.IncomeFrom));
                command.Parameters.Add(new SqlParameter("@IncomeTo", rate.IncomeTo));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("IncomeTaxRate::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}