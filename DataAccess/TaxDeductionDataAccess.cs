﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;

namespace CUSTOR.DataAccess
{
    public class RawTaxReductionDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[RawTaxReductionPercent],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[CreatedUserId],[UpdatedUserId] FROM [dbo].[RawTaxReduction] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("RawTaxReduction");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

         

        public RawTaxReductionDomain GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            RawTaxReductionDomain objRawTaxReduction = new RawTaxReductionDomain();
            string strGetRecord = @"SELECT [Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[RawTaxReductionPercent],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[CreatedUserId],[UpdatedUserId],[Description] FROM [dbo].[RawTaxReduction] where IsNull(IsDeleted,0) = 0 ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("RawTaxReduction");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objRawTaxReduction.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["TaxType"].Equals(DBNull.Value))
                        objRawTaxReduction.TaxType = 0;
                    else
                        objRawTaxReduction.TaxType = (int)dTable.Rows[0]["TaxType"];
                    objRawTaxReduction.AppliedDateFrom = (DateTime)dTable.Rows[0]["AppliedDateFrom"];
                    if (dTable.Rows[0]["AppliedDateTo"].Equals(DBNull.Value))
                        objRawTaxReduction.AppliedDateTo = DateTime.MinValue;
                    else
                        objRawTaxReduction.AppliedDateTo = (DateTime)dTable.Rows[0]["AppliedDateTo"];
                   
                    if (dTable.Rows[0]["RawTaxReductionPercent"].Equals(DBNull.Value))
                        objRawTaxReduction.RawTaxReductionPercent = 0;
                    else
                        objRawTaxReduction.RawTaxReductionPercent = (double)dTable.Rows[0]["RawTaxReductionPercent"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objRawTaxReduction.IsActive = false;
                    else
                        objRawTaxReduction.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objRawTaxReduction.IsDeleted = false;
                    else
                        objRawTaxReduction.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CenterId"].Equals(DBNull.Value))
                        objRawTaxReduction.CenterId = 0;
                    else
                        objRawTaxReduction.CenterId = (int)dTable.Rows[0]["CenterId"];
                    if (dTable.Rows[0]["CenterName"].Equals(DBNull.Value))
                        objRawTaxReduction.CenterName = string.Empty;
                    else
                        objRawTaxReduction.CenterName = (string)dTable.Rows[0]["CenterName"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedDate = DateTime.MinValue;
                    else
                        objRawTaxReduction.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedBy = string.Empty;
                    else
                        objRawTaxReduction.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedDate = DateTime.MinValue;
                    else
                        objRawTaxReduction.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedBy = string.Empty;
                    else
                        objRawTaxReduction.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedUserId = Guid.Empty;
                    else
                        objRawTaxReduction.CreatedUserId = (Guid)dTable.Rows[0]["CreatedUserId"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedUserId = Guid.Empty;
                    else
                        objRawTaxReduction.UpdatedUserId = (Guid)dTable.Rows[0]["UpdatedUserId"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objRawTaxReduction.Description = string.Empty;
                    else
                        objRawTaxReduction.Description = (string)dTable.Rows[0]["Description"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objRawTaxReduction;
        }
        public List<RawTaxReductionTaxpayer> GetReducedTaxPayersList(int rawTaxReductionId)
        {
            List<RawTaxReductionTaxpayer> RecordsList = new List<RawTaxReductionTaxpayer>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[TaxpayerGrade],[RawTaxReductionId],[IsDeleted] FROM [dbo].[RawTaxReductionTaxpayer] where RawTaxReductionId = @RawTaxReductionId and IsNull(IsDeleted,0)=0 ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@RawTaxReductionId", rawTaxReductionId);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    RawTaxReductionTaxpayer objRawTaxReductionTaxpayer = new RawTaxReductionTaxpayer();
                    if (dr["Id"].Equals(DBNull.Value))
                        objRawTaxReductionTaxpayer.Id = 0;
                    else
                        objRawTaxReductionTaxpayer.Id = (int)dr["Id"];
                    if (dr["TaxpayerGrade"].Equals(DBNull.Value))
                        objRawTaxReductionTaxpayer.TaxpayerGrade = 0;
                    else
                        objRawTaxReductionTaxpayer.TaxpayerGrade = (int)dr["TaxpayerGrade"];
                    if (dr["RawTaxReductionId"].Equals(DBNull.Value))
                        objRawTaxReductionTaxpayer.RawTaxReductionId = 0;
                    else
                        objRawTaxReductionTaxpayer.RawTaxReductionId = (int)dr["RawTaxReductionId"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objRawTaxReductionTaxpayer.IsDeleted = false;
                    else
                        objRawTaxReductionTaxpayer.IsDeleted = (bool)dr["IsDeleted"];
                    RecordsList.Add(objRawTaxReductionTaxpayer);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReductionTaxpayer::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Insert(RawTaxReductionDomain objRawTaxReduction, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"DECLARE @T AS TABLE(ID INT);INSERT INTO [dbo].[RawTaxReduction]
                                            ([TaxType],[AppliedDateFrom],[AppliedDateTo],[RawTaxReductionPercent],[IsActive],[IsDeleted],[CenterId],[CenterName],[CreatedDate],[CreatedBy],[CreatedUserId],[Description]) output Inserted.Id into @T
                                     VALUES    (@TaxType,@AppliedDateFrom,@AppliedDateTo,@RawTaxReductionPercent,@IsActive,@IsDeleted,@CenterId,@CenterName,@CreatedDate,@CreatedBy,@CreatedUserId, @Description)  select * from @T";

            command.CommandText = strInsert;
            try
            {
                command.Parameters.Clear();
                command.Parameters.Add(new SqlParameter("@TaxType", objRawTaxReduction.TaxType));
                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objRawTaxReduction.AppliedDateFrom));

                if (objRawTaxReduction.AppliedDateTo != DateTime.MinValue)
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", objRawTaxReduction.AppliedDateTo));
                else
                    command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));

                command.Parameters.Add(new SqlParameter("@RawTaxReductionPercent", objRawTaxReduction.RawTaxReductionPercent));
                command.Parameters.Add(new SqlParameter("@IsActive", objRawTaxReduction.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objRawTaxReduction.IsDeleted));
                command.Parameters.Add(new SqlParameter("@CenterId", objRawTaxReduction.CenterId));
                command.Parameters.Add(new SqlParameter("@CenterName", objRawTaxReduction.CenterName));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objRawTaxReduction.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objRawTaxReduction.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objRawTaxReduction.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@Description", objRawTaxReduction.Description));
                var obj = command.ExecuteScalar();
                if(obj != null)
                objRawTaxReduction.Id = (int)obj;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::Insert::Error!" + ex.Message, ex);
            }
            
        }
        public bool InsertTaxpayersDeduction(RawTaxReductionDomain objRawTaxReduction, SqlCommand command)
        {

            string strInsert = @"INSERT INTO [dbo].[RawTaxReductionTaxpayer]
                                            ([RawTaxReductionId],[TaxpayerGrade])
                                     VALUES    (@RawTaxReductionId,@TaxpayerGrade)";

            command.Parameters.Clear();
            command.CommandText = strInsert;

            try
            {
                command.Parameters.Clear();
                command.Parameters.Add(new SqlParameter("@RawTaxReductionId", objRawTaxReduction.Id));
                command.Parameters.Add(new SqlParameter("@TaxpayerGrade", objRawTaxReduction.TaxPayerGrade));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InsertTaxpayersDeduction::Insert::Error!" + ex.Message, ex);
            }

        }
        public bool Update(RawTaxReductionDomain objRawTaxReduction,SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[RawTaxReduction] SET     [TaxType]=@TaxType,    [AppliedDateFrom]=@AppliedDateFrom,    [AppliedDateTo]=@AppliedDateTo,     [RawTaxReductionPercent]=@RawTaxReductionPercent,    [IsActive]=@IsActive,    [IsDeleted]=@IsDeleted,     [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy,   [UpdatedUserId]=@UpdatedUserId,[Description] = @Description where Id = @Id ";

            command.CommandText = strUpdate;
            command.Parameters.Clear();
            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objRawTaxReduction.Id));
                command.Parameters.Add(new SqlParameter("@TaxType", objRawTaxReduction.TaxType));
                command.Parameters.Add(new SqlParameter("@AppliedDateFrom", objRawTaxReduction.AppliedDateFrom));
                if(objRawTaxReduction.AppliedDateTo!= DateTime.MinValue)
                command.Parameters.Add(new SqlParameter("@AppliedDateTo", objRawTaxReduction.AppliedDateTo));
                else
                command.Parameters.Add(new SqlParameter("@AppliedDateTo", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@RawTaxReductionPercent", objRawTaxReduction.RawTaxReductionPercent));
                command.Parameters.Add(new SqlParameter("@IsActive", objRawTaxReduction.IsActive));
                command.Parameters.Add(new SqlParameter("@IsDeleted", objRawTaxReduction.IsDeleted));
               
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objRawTaxReduction.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objRawTaxReduction.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objRawTaxReduction.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@Description", objRawTaxReduction.Description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID,SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"update [dbo].[RawTaxReduction] set isdeleted = 1 where Id = @Id";
            command.CommandText = strDelete;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@Id", ID);
            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();

                strDelete = @"Update [dbo].[RawTaxReductionTaxpayer] set IsDeleted = 1 where RawTaxReductionId = @Id";
                command.CommandText = strDelete;
                _rowsAffected = command.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::Delete::Error!" + ex.Message, ex);
            }
            
        }
        public bool DeleteReducedTaxpayerGrades(int ID, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"Update [dbo].[RawTaxReductionTaxpayer] set IsDeleted = 1 where RawTaxReductionId = @Id";
            command.CommandText = strDelete;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@Id", ID);
            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::Delete::Error!" + ex.Message, ex);
            }

        }


        public List<RawTaxReductionDomain> GetList()
        {
            List<RawTaxReductionDomain> RecordsList = new List<RawTaxReductionDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT TaxType.TaxTypeName, [dbo].[RawTaxReductionTaxpayerGrades]([dbo].[RawTaxReduction].Id) TaxpayerGradeText, [dbo].[RawTaxReduction].[Id],[TaxType],[AppliedDateFrom],[AppliedDateTo],[RawTaxReductionPercent],[dbo].[RawTaxReduction].[CenterId],[dbo].[RawTaxReduction].[CenterName],
                                        [dbo].[RawTaxReduction].[CreatedDate],[dbo].[RawTaxReduction].[CreatedBy],[dbo].[RawTaxReduction].[UpdatedDate],[dbo].[RawTaxReduction].[UpdatedBy],[dbo].[RawTaxReduction].[CreatedUserId],[dbo].[RawTaxReduction].[UpdatedUserId] 
                                        FROM [dbo].[RawTaxReduction] INNER JOIN TaxType on [dbo].[RawTaxReduction].TaxType = TaxType.Id 
                                        where IsNull( [dbo].[RawTaxReduction].[IsDeleted],0)=0 order by [dbo].[RawTaxReduction].[Id] desc";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    RawTaxReductionDomain objRawTaxReduction = new RawTaxReductionDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objRawTaxReduction.Id = 0;
                    else
                        objRawTaxReduction.Id = (int)dr["Id"];
                    if (dr["TaxTypeName"].Equals(DBNull.Value))
                        objRawTaxReduction.TaxTypeName = string.Empty;
                    else
                        objRawTaxReduction.TaxTypeName = (string)dr["TaxTypeName"];
                    if (dr["TaxpayerGradeText"].Equals(DBNull.Value))
                        objRawTaxReduction.TaxpayerGradText = string.Empty;
                    else
                        objRawTaxReduction.TaxpayerGradText = (string)dr["TaxpayerGradeText"];
                    if (dr["AppliedDateFrom"].Equals(DBNull.Value))
                        objRawTaxReduction.AppliedDateFrom = DateTime.MinValue;
                    else
                    {
                        objRawTaxReduction.AppliedDateFrom = (DateTime)dr["AppliedDateFrom"];
                        objRawTaxReduction.AppliedDateFromText = EthiopicDateTime.GetEthiopicDate(objRawTaxReduction.AppliedDateFrom);

                    }
                    if (dr["AppliedDateTo"].Equals(DBNull.Value))
                        objRawTaxReduction.AppliedDateTo = DateTime.MinValue;
                    else
                    {
                        objRawTaxReduction.AppliedDateTo = (DateTime)dr["AppliedDateTo"];
                        objRawTaxReduction.AppliedDateToText = EthiopicDateTime.GetEthiopicDate(objRawTaxReduction.AppliedDateTo);
                    }
                    
                    if (dr["RawTaxReductionPercent"].Equals(DBNull.Value))
                        objRawTaxReduction.RawTaxReductionPercent = 0;
                    else
                        objRawTaxReduction.RawTaxReductionPercent = (double)dr["RawTaxReductionPercent"];
                   
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedDate = DateTime.MinValue;
                    else
                        objRawTaxReduction.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedBy = string.Empty;
                    else
                        objRawTaxReduction.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedDate = DateTime.MinValue;
                    else
                        objRawTaxReduction.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedBy = string.Empty;
                    else
                        objRawTaxReduction.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objRawTaxReduction.CreatedUserId = Guid.Empty;
                    else
                        objRawTaxReduction.CreatedUserId = (Guid)dr["CreatedUserId"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objRawTaxReduction.UpdatedUserId = Guid.Empty;
                    else
                        objRawTaxReduction.UpdatedUserId = (Guid)dr["UpdatedUserId"];
                    RecordsList.Add(objRawTaxReduction);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[RawTaxReduction] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("RawTaxReduction::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}