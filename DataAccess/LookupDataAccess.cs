﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class LookUpDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[Description],[Amdescription],[Parent],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookUp]  ORDER BY  [Id] DESC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("LookUp");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public LookUp GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            LookUp objLookUp = new LookUp();
            string strGetRecord = @"SELECT [Id],[Description],[Amdescription],[Parent],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookUp] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("LookUp");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objLookUp.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objLookUp.Description = string.Empty;
                    else
                        objLookUp.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["Amdescription"].Equals(DBNull.Value))
                        objLookUp.Amdescription = string.Empty;
                    else
                        objLookUp.Amdescription = (string)dTable.Rows[0]["Amdescription"];
                    objLookUp.Parent = (int)dTable.Rows[0]["Parent"];
                    if (dTable.Rows[0]["SortValue"].Equals(DBNull.Value))
                        objLookUp.SortValue = string.Empty;
                    else
                        objLookUp.SortValue = (string)dTable.Rows[0]["SortValue"];
                    if (dTable.Rows[0]["Soundx"].Equals(DBNull.Value))
                        objLookUp.Soundx = string.Empty;
                    else
                        objLookUp.Soundx = (string)dTable.Rows[0]["Soundx"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objLookUp.IsActive = false;
                    else
                        objLookUp.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objLookUp.CreatedDate = DateTime.MinValue;
                    else
                        objLookUp.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objLookUp.CreatedBy = string.Empty;
                    else
                        objLookUp.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objLookUp.UpdatedDate = DateTime.MinValue;
                    else
                        objLookUp.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objLookUp.UpdatedBy = string.Empty;
                    else
                        objLookUp.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objLookUp;
        }

        public bool Insert(LookUp objLookUp)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[LookUp]
                                            ([Description],[Amdescription],[Parent],[SortValue],[Soundx],[IsActive],[CreatedDate],[CreatedBy])
                                     VALUES    (@Description,@Amdescription,@Parent,@SortValue,@Soundx,@IsActive,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Description", objLookUp.Description));
                command.Parameters.Add(new SqlParameter("@Amdescription", objLookUp.Amdescription));
                command.Parameters.Add(new SqlParameter("@Parent", objLookUp.Parent));
                command.Parameters.Add(new SqlParameter("@SortValue", objLookUp.SortValue));
                command.Parameters.Add(new SqlParameter("@Soundx", objLookUp.Soundx));
                command.Parameters.Add(new SqlParameter("@IsActive", objLookUp.IsActive));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objLookUp.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objLookUp.CreatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(LookUp objLookUp)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[LookUp] SET     [Description]=@Description,    [Amdescription]=@Amdescription,    [Parent]=@Parent,    [SortValue]=@SortValue,    [Soundx]=@Soundx,    [IsActive]=@IsActive,   [UpdatedDate]=@UpdatedDate,    [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objLookUp.Id));
                command.Parameters.Add(new SqlParameter("@Description", objLookUp.Description));
                command.Parameters.Add(new SqlParameter("@Amdescription", objLookUp.Amdescription));
                command.Parameters.Add(new SqlParameter("@Parent", objLookUp.Parent));
                command.Parameters.Add(new SqlParameter("@SortValue", objLookUp.SortValue));
                command.Parameters.Add(new SqlParameter("@Soundx", objLookUp.Soundx));
                command.Parameters.Add(new SqlParameter("@IsActive", objLookUp.IsActive));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objLookUp.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objLookUp.UpdatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[LookUp] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<LookUp> GetList(int Parent)
        {
            List<LookUp> RecordsList = new List<LookUp>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Description],[Amdescription],[Parent],[SortValue],[Soundx],[IsActive],[CreatedDate],
                                      [CreatedBy],[UpdatedDate],[UpdatedBy] FROM [dbo].[LookUp] where Parent = @Parent ORDER BY  [Id] DESC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.AddWithValue("@Parent", Parent);
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    LookUp objLookUp = new LookUp();
                    if (dr["Id"].Equals(DBNull.Value))
                        objLookUp.Id = 0;
                    else
                        objLookUp.Id = (int)dr["Id"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objLookUp.Description = string.Empty;
                    else
                        objLookUp.Description = (string)dr["Description"];
                    if (dr["Amdescription"].Equals(DBNull.Value))
                        objLookUp.Amdescription = string.Empty;
                    else
                        objLookUp.Amdescription = (string)dr["Amdescription"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objLookUp.Parent = 0;
                    else
                        objLookUp.Parent = (int)dr["Parent"];
                    if (dr["SortValue"].Equals(DBNull.Value))
                        objLookUp.SortValue = string.Empty;
                    else
                        objLookUp.SortValue = (string)dr["SortValue"];
                    if (dr["Soundx"].Equals(DBNull.Value))
                        objLookUp.Soundx = string.Empty;
                    else
                        objLookUp.Soundx = (string)dr["Soundx"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objLookUp.IsActive = false;
                    else
                        objLookUp.IsActive = (bool)dr["IsActive"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objLookUp.CreatedDate = DateTime.MinValue;
                    else
                        objLookUp.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objLookUp.CreatedBy = string.Empty;
                    else
                        objLookUp.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objLookUp.UpdatedDate = DateTime.MinValue;
                    else
                        objLookUp.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objLookUp.UpdatedBy = string.Empty;
                    else
                        objLookUp.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objLookUp);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LookUp::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool UpdateExists(LookUp lookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Lookup] WHERE (Description = @Description  and  [Id] <> @Id and Parent = @Parent)";

            if (lookup.SortValue != string.Empty)
            {
                strExists += " or (SortValue = @SortValue and  [Id] <> @Id and Parent = @Parent)";
            }
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", lookup.Id));
                command.Parameters.Add(new SqlParameter("@Parent", lookup.Parent));
                command.Parameters.Add(new SqlParameter("@Description", lookup.Description));
                command.Parameters.Add(new SqlParameter("@SortValue", lookup.SortValue));

                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(LookUp lookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Lookup] WHERE (Description = @Description and Parent = @Parent)";
            if (lookup.SortValue != string.Empty)
            {
                strExists += " or SortValue = @SortValue";
            }
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", lookup.Id));
                command.Parameters.Add(new SqlParameter("@Parent", lookup.Parent));
                command.Parameters.Add(new SqlParameter("@Description", lookup.Description));
                command.Parameters.Add(new SqlParameter("@SortValue", lookup.SortValue));

                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("LookupType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



    }
}