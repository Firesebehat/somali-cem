﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class aspnet_RolesDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description] FROM [dbo].[aspnet_Roles] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("aspnet_Roles");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public aspnet_Roles GetRecord(string roleName)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            aspnet_Roles objaspnet_Roles = new aspnet_Roles();
            string strGetRecord = @"SELECT [ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description] FROM [dbo].[aspnet_Roles] where roleName = @roleName ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("aspnet_Roles");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            command.Parameters.AddWithValue("@roleName", roleName);
            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objaspnet_Roles.ApplicationId = Convert.ToString(dTable.Rows[0]["ApplicationId"]);
                    objaspnet_Roles.RoleId = Convert.ToString(dTable.Rows[0]["RoleId"]);
                    objaspnet_Roles.RoleName = Convert.ToString(dTable.Rows[0]["RoleName"]);
                    objaspnet_Roles.LoweredRoleName = Convert.ToString(dTable.Rows[0]["LoweredRoleName"]);
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objaspnet_Roles.Description = string.Empty;
                    else
                        objaspnet_Roles.Description = (string)dTable.Rows[0]["Description"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objaspnet_Roles;
        }

        public bool Insert(aspnet_Roles objaspnet_Roles)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[aspnet_Roles]
                                            ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description])
                                     VALUES    (@ApplicationId, ISNULL(@RoleId, (newid())),@RoleName,@LoweredRoleName,@Description)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicationId", objaspnet_Roles.ApplicationId));
                command.Parameters.Add(new SqlParameter("@RoleId", objaspnet_Roles.RoleId));
                command.Parameters.Add(new SqlParameter("@RoleName", objaspnet_Roles.RoleName));
                command.Parameters.Add(new SqlParameter("@LoweredRoleName", objaspnet_Roles.LoweredRoleName));
                command.Parameters.Add(new SqlParameter("@Description", objaspnet_Roles.Description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(aspnet_Roles objaspnet_Roles)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[aspnet_Roles] SET     [ApplicationId]=@ApplicationId,    [RoleId]=@RoleId,    [RoleName]=@RoleName,    [LoweredRoleName]=@LoweredRoleName,    [Description]=@Description ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicationId", objaspnet_Roles.ApplicationId));
                command.Parameters.Add(new SqlParameter("@RoleId", objaspnet_Roles.RoleId));
                command.Parameters.Add(new SqlParameter("@RoleName", objaspnet_Roles.RoleName));
                command.Parameters.Add(new SqlParameter("@LoweredRoleName", objaspnet_Roles.LoweredRoleName));
                command.Parameters.Add(new SqlParameter("@Description", objaspnet_Roles.Description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool UpdateRoleDescription(string roleName,string description)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[aspnet_Roles] SET    [Description]=@Description Where roleName =@roleName ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RoleName", roleName));
                command.Parameters.Add(new SqlParameter("@Description", description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[aspnet_Roles] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<aspnet_Roles> GetList()
        {
            List<aspnet_Roles> RecordsList = new List<aspnet_Roles>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description] FROM [dbo].[aspnet_Roles] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    aspnet_Roles objaspnet_Roles = new aspnet_Roles();
                    if (dr["ApplicationId"].Equals(DBNull.Value))
                        objaspnet_Roles.ApplicationId = null;
                    else
                        objaspnet_Roles.ApplicationId = (string)dr["ApplicationId"];
                    if (dr["RoleId"].Equals(DBNull.Value))
                        objaspnet_Roles.RoleId = null;
                    else
                        objaspnet_Roles.RoleId = (string)dr["RoleId"];
                    if (dr["RoleName"].Equals(DBNull.Value))
                        objaspnet_Roles.RoleName = string.Empty;
                    else
                        objaspnet_Roles.RoleName = (string)dr["RoleName"];
                    if (dr["LoweredRoleName"].Equals(DBNull.Value))
                        objaspnet_Roles.LoweredRoleName = string.Empty;
                    else
                        objaspnet_Roles.LoweredRoleName = (string)dr["LoweredRoleName"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objaspnet_Roles.Description = string.Empty;
                    else
                        objaspnet_Roles.Description = (string)dr["Description"];
                    RecordsList.Add(objaspnet_Roles);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(bool ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[aspnet_Roles] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception("aspnet_Roles::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}