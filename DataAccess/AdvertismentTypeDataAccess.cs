﻿using Domain.Taxpayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Taxpayer
{
    public class AdvertismentTypeDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[Code],[AdveritsmentType],[AmAdveritsmentType],[Unit],[ModePayment],[Amount],[AmountAllPages] FROM [dbo].[AdvertismentType] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AdvertismentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public AdvertismentTypeDomain GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            AdvertismentTypeDomain objAdvertismentType = new AdvertismentTypeDomain();
            string strGetRecord = @"SELECT [Id],[Code],[AdveritsmentType],[AmAdveritsmentType],[Unit],[ModePayment],[Amount],[AmountAllPages] FROM [dbo].[AdvertismentType] where ID=@ID ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("AdvertismentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAdvertismentType.Id = (int)dTable.Rows[0]["Id"];
                    if (dTable.Rows[0]["Code"].Equals(DBNull.Value))
                        objAdvertismentType.Code = 0;
                    else
                        objAdvertismentType.Code = (int)dTable.Rows[0]["Code"];
                    if (dTable.Rows[0]["AdveritsmentType"].Equals(DBNull.Value))
                        objAdvertismentType.AdveritsmentType = string.Empty;
                    else
                        objAdvertismentType.AdveritsmentType = (string)dTable.Rows[0]["AdveritsmentType"];
                    if (dTable.Rows[0]["AmAdveritsmentType"].Equals(DBNull.Value))
                        objAdvertismentType.AmAdveritsmentType = string.Empty;
                    else
                        objAdvertismentType.AmAdveritsmentType = (string)dTable.Rows[0]["AmAdveritsmentType"];
                    if (dTable.Rows[0]["Unit"].Equals(DBNull.Value))
                        objAdvertismentType.Unit = 0;
                    else
                        objAdvertismentType.Unit = (int)dTable.Rows[0]["Unit"];
                    if (dTable.Rows[0]["ModePayment"].Equals(DBNull.Value))
                        objAdvertismentType.ModePayment = 0;
                    else
                        objAdvertismentType.ModePayment = (int)dTable.Rows[0]["ModePayment"];
                    if (dTable.Rows[0]["Amount"].Equals(DBNull.Value))
                        objAdvertismentType.Amount = 0;
                    else
                        objAdvertismentType.Amount = (double)dTable.Rows[0]["Amount"];
                    if (dTable.Rows[0]["AmountAllPages"].Equals(DBNull.Value))
                        objAdvertismentType.AmountAllPages = 0;
                    else
                        objAdvertismentType.AmountAllPages = (double)dTable.Rows[0]["AmountAllPages"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAdvertismentType;
        }





        public List<AdvertismentTypeDomain> GetList()
        {
            List<AdvertismentTypeDomain> RecordsList = new List<AdvertismentTypeDomain>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[Code],[AdveritsmentType],[AmAdveritsmentType],[Unit],[ModePayment],[Amount],[AmountAllPages] FROM [dbo].[AdvertismentType] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    AdvertismentTypeDomain objAdvertismentType = new AdvertismentTypeDomain();
                    if (dr["Id"].Equals(DBNull.Value))
                        objAdvertismentType.Id = 0;
                    else
                        objAdvertismentType.Id = (int)dr["Id"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objAdvertismentType.Code = 0;
                    else
                        objAdvertismentType.Code = (int)dr["Code"];
                    if (dr["AdveritsmentType"].Equals(DBNull.Value))
                        objAdvertismentType.AdveritsmentType = string.Empty;
                    else
                        objAdvertismentType.AdveritsmentType = (string)dr["AdveritsmentType"];
                    if (dr["AmAdveritsmentType"].Equals(DBNull.Value))
                        objAdvertismentType.AmAdveritsmentType = string.Empty;
                    else
                        objAdvertismentType.AmAdveritsmentType = (string)dr["AmAdveritsmentType"];
                    if (dr["Unit"].Equals(DBNull.Value))
                        objAdvertismentType.Unit = 0;
                    else
                        objAdvertismentType.Unit = (int)dr["Unit"];
                    if (dr["ModePayment"].Equals(DBNull.Value))
                        objAdvertismentType.ModePayment = 0;
                    else
                        objAdvertismentType.ModePayment = (int)dr["ModePayment"];
                    if (dr["Amount"].Equals(DBNull.Value))
                        objAdvertismentType.Amount = 0;
                    else
                        objAdvertismentType.Amount = (double)dr["Amount"];
                    if (dr["AmountAllPages"].Equals(DBNull.Value))
                        objAdvertismentType.AmountAllPages = 0;
                    else
                        objAdvertismentType.AmountAllPages = (double)dr["AmountAllPages"];
                    RecordsList.Add(objAdvertismentType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Insert(AdvertismentTypeDomain objAdvertismentType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[AdvertismentType]
                                            ([Code],[AdveritsmentType],[AmAdveritsmentType],[Unit],[ModePayment],[Amount],[AmountAllPages])
                                     VALUES    (@Code,@AdveritsmentType,@AmAdveritsmentType,@Unit,@ModePayment,@Amount,@AmountAllPages)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Code", objAdvertismentType.Code));
                command.Parameters.Add(new SqlParameter("@AdveritsmentType", objAdvertismentType.AdveritsmentType));
                command.Parameters.Add(new SqlParameter("@AmAdveritsmentType", objAdvertismentType.AmAdveritsmentType));
                command.Parameters.Add(new SqlParameter("@Unit", objAdvertismentType.Unit));
                command.Parameters.Add(new SqlParameter("@ModePayment", objAdvertismentType.ModePayment));
                command.Parameters.Add(new SqlParameter("@Amount", objAdvertismentType.Amount));
                command.Parameters.Add(new SqlParameter("@AmountAllPages", objAdvertismentType.AmountAllPages));
                command.Parameters.Add(new SqlParameter("@Id", objAdvertismentType.Id));
                command.Parameters["@Id"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                //objAdvertismentType.Id = (int)command.Parameters["@Id"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(AdvertismentTypeDomain objAdvertismentType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[AdvertismentType] SET     [Code]=@Code,    [AdveritsmentType]=@AdveritsmentType,    [AmAdveritsmentType]=@AmAdveritsmentType,    [Unit]=@Unit,    [ModePayment]=@ModePayment,    [Amount]=@Amount,    [AmountAllPages]=@AmountAllPages WHERE Id=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objAdvertismentType.Id));
                command.Parameters.Add(new SqlParameter("@Code", objAdvertismentType.Code));
                command.Parameters.Add(new SqlParameter("@AdveritsmentType", objAdvertismentType.AdveritsmentType));
                command.Parameters.Add(new SqlParameter("@AmAdveritsmentType", objAdvertismentType.AmAdveritsmentType));
                command.Parameters.Add(new SqlParameter("@Unit", objAdvertismentType.Unit));
                command.Parameters.Add(new SqlParameter("@ModePayment", objAdvertismentType.ModePayment));
                command.Parameters.Add(new SqlParameter("@Amount", objAdvertismentType.Amount));
                command.Parameters.Add(new SqlParameter("@AmountAllPages", objAdvertismentType.AmountAllPages));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[AdvertismentType] WHERE Id=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("AdvertismentType::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Id FROM [dbo].[AdvertismentType] WHERE [Code]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                int Exist = (int)command.ExecuteScalar();
                if (Exist > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxExemption::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return false;
        }



    }
}