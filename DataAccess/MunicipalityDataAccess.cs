﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class MunicipalityDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[CategoryId],[Rank],[AnnualFee],[CreatedBy],[CreatedUserId],[UpdatedBy],[UpdatedUserId],[CreatedDate],[UpdatedDate] FROM [dbo].[Municipality]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Municipality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public Municipality GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Municipality objMunicipality = new Municipality();
            string strGetRecord = @"SELECT [Id],[CategoryId],[Rank],[AnnualFee],[CreatedBy],[CreatedUserId],[UpdatedBy],[UpdatedUserId],[CreatedDate],[UpdatedDate] FROM [dbo].[Municipality] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Municipality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objMunicipality.Id = (int)dTable.Rows[0]["Id"];
                    objMunicipality.CategoryId = (int)dTable.Rows[0]["CategoryId"];
                    objMunicipality.Rank = (int)dTable.Rows[0]["Rank"];
                    objMunicipality.AnnualFee = (decimal)dTable.Rows[0]["AnnualFee"];
                    objMunicipality.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    objMunicipality.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objMunicipality.UpdatedBy = string.Empty;
                    else
                        objMunicipality.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objMunicipality.UpdatedUserId = string.Empty;
                    else
                        objMunicipality.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    objMunicipality.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objMunicipality.UpdatedDate = DateTime.MinValue;
                    else
                        objMunicipality.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objMunicipality;
        }

        public List<Municipality> GetMunicipalitys(int categoryId)
        {
            List<Municipality> RecordsList = new List<Municipality>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CategoryId],[Rank],[AnnualFee]  FROM [dbo].[Municipality] where CategoryId = @CategoryId   ORDER BY  [Rank] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@CategoryId", categoryId);
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Municipality objMunicipality = new Municipality();
                    if (dr["Id"].Equals(DBNull.Value))
                        objMunicipality.Id = 0;
                    else
                        objMunicipality.Id = (int)dr["Id"];
                    if (dr["CategoryId"].Equals(DBNull.Value))
                        objMunicipality.CategoryId = 0;
                    else
                        objMunicipality.CategoryId = (int)dr["CategoryId"];
                    if (dr["Rank"].Equals(DBNull.Value))
                        objMunicipality.Rank = 0;
                    else
                        objMunicipality.Rank = (int)dr["Rank"];
                    if (dr["AnnualFee"].Equals(DBNull.Value))
                        objMunicipality.AnnualFee = 0;
                    else
                        objMunicipality.AnnualFee = (decimal)dr["AnnualFee"];
                   
                    RecordsList.Add(objMunicipality);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Insert(Municipality objMunicipality)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Municipality]
                                            ([CategoryId],[Rank],[AnnualFee],[CreatedBy],[CreatedUserId],[CreatedDate])
                                     VALUES    (@CategoryId, ISNULL(@Rank, ((0))),@AnnualFee,@CreatedBy,@CreatedUserId,@CreatedDate)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CategoryId", objMunicipality.CategoryId));
                command.Parameters.Add(new SqlParameter("@Rank", objMunicipality.Rank));
                command.Parameters.Add(new SqlParameter("@AnnualFee", objMunicipality.AnnualFee));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objMunicipality.CreatedBy));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objMunicipality.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objMunicipality.CreatedDate));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Municipality objMunicipality)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Municipality] SET     [CategoryId]=@CategoryId,    [Rank]=@Rank,    [AnnualFee]=@AnnualFee,   [UpdatedBy]=@UpdatedBy,    [UpdatedUserId]=@UpdatedUserId, [UpdatedDate]=@UpdatedDate WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objMunicipality.Id));
                command.Parameters.Add(new SqlParameter("@CategoryId", objMunicipality.CategoryId));
                command.Parameters.Add(new SqlParameter("@Rank", objMunicipality.Rank));
                command.Parameters.Add(new SqlParameter("@AnnualFee", objMunicipality.AnnualFee));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objMunicipality.UpdatedBy));
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objMunicipality.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objMunicipality.UpdatedDate));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Municipality] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<Municipality> GetList()
        {
            List<Municipality> RecordsList = new List<Municipality>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CategoryId],[Rank],[AnnualFee],[CreatedBy],[CreatedUserId],[UpdatedBy],[UpdatedUserId],[CreatedDate],[UpdatedDate] FROM [dbo].[Municipality]  ORDER BY  [Id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Municipality objMunicipality = new Municipality();
                    if (dr["Id"].Equals(DBNull.Value))
                        objMunicipality.Id = 0;
                    else
                        objMunicipality.Id = (int)dr["Id"];
                    if (dr["CategoryId"].Equals(DBNull.Value))
                        objMunicipality.CategoryId = 0;
                    else
                        objMunicipality.CategoryId = (int)dr["CategoryId"];
                    if (dr["Rank"].Equals(DBNull.Value))
                        objMunicipality.Rank = 0;
                    else
                        objMunicipality.Rank = (int)dr["Rank"];
                    if (dr["AnnualFee"].Equals(DBNull.Value))
                        objMunicipality.AnnualFee = 0;
                    else
                        objMunicipality.AnnualFee = (decimal)dr["AnnualFee"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objMunicipality.CreatedBy = string.Empty;
                    else
                        objMunicipality.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objMunicipality.CreatedUserId = string.Empty;
                    else
                        objMunicipality.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objMunicipality.UpdatedBy = string.Empty;
                    else
                        objMunicipality.UpdatedBy = (string)dr["UpdatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objMunicipality.UpdatedUserId = string.Empty;
                    else
                        objMunicipality.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objMunicipality.CreatedDate = DateTime.MinValue;
                    else
                        objMunicipality.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objMunicipality.UpdatedDate = DateTime.MinValue;
                    else
                        objMunicipality.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    RecordsList.Add(objMunicipality);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int categoryId, int rank)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Municipality] WHERE [CategoryId]=@CategoryId and rank = @Rank";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CategoryId", categoryId));
                command.Parameters.Add(new SqlParameter("@Rank", rank));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Exists(int id, int categoryId, int rank)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[Municipality] WHERE [CategoryId]=@CategoryId and rank = @Rank and Id <> @Id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CategoryId", categoryId));
                command.Parameters.Add(new SqlParameter("@Rank", rank));
                command.Parameters.Add(new SqlParameter("@Id", id));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Municipality::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}