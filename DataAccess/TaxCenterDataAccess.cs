﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class TaxCenterDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Id],[CenterName],[CenterNameEnglish],[CenterNameSort],[CenterNameSound],[OrgType],[Parent],[Language],
                                    [Region],[Zone],[Woreda],[Kebele],[Telephone],[Email],[POBox],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],
                                    [CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[TaxCenter] where IsDeleted <> 1 ORDER BY  [CenterNameEnglish] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("TaxCenter");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public TaxCenter GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            TaxCenter objTaxCenter = new TaxCenter();
            string strGetRecord = @"SELECT [Id],[CenterName],[CenterNameEnglish],[CenterNameSort],[CenterNameSound],[OrgType],[SiteLevel],
                                [Parent],[Language],[Region],[Zone],[Woreda],[Kebele],[Telephone],[Email],[POBox],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[TaxCenter] WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("TaxCenter");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objTaxCenter.Id = (int)dTable.Rows[0]["Id"];
                    objTaxCenter.CenterName = (string)dTable.Rows[0]["CenterName"];
                    objTaxCenter.CenterNameEnglish = (string)dTable.Rows[0]["CenterNameEnglish"];
                    if (dTable.Rows[0]["CenterNameSort"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSort = string.Empty;
                    else
                        objTaxCenter.CenterNameSort = (string)dTable.Rows[0]["CenterNameSort"];
                    if (dTable.Rows[0]["CenterNameSound"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSound = string.Empty;
                    else
                        objTaxCenter.CenterNameSound = (string)dTable.Rows[0]["CenterNameSound"];
                    if (dTable.Rows[0]["OrgType"].Equals(DBNull.Value))
                        objTaxCenter.OrgType = 0;
                    else
                        objTaxCenter.OrgType =  Int16.Parse(dTable.Rows[0]["OrgType"].ToString());
                    if (dTable.Rows[0]["Parent"].Equals(DBNull.Value))
                        objTaxCenter.Parent = 0;
                    else
                        objTaxCenter.Parent = (int)dTable.Rows[0]["Parent"];
                    if (dTable.Rows[0]["SiteLevel"].Equals(DBNull.Value))
                        objTaxCenter.SiteLevel = 0;
                    else
                        objTaxCenter.SiteLevel = Int16.Parse( dTable.Rows[0]["SiteLevel"].ToString());

                    if (dTable.Rows[0]["Language"].Equals(DBNull.Value))
                        objTaxCenter.Language = 0;
                    else
                        objTaxCenter.Language = (int)dTable.Rows[0]["Language"];
                    if (dTable.Rows[0]["Region"].Equals(DBNull.Value))
                        objTaxCenter.Region = 0;
                    else
                        objTaxCenter.Region = (int)dTable.Rows[0]["Region"];
                    if (dTable.Rows[0]["Zone"].Equals(DBNull.Value))
                        objTaxCenter.Zone = 0;
                    else
                        objTaxCenter.Zone = (int)dTable.Rows[0]["Zone"];
                    if (dTable.Rows[0]["Woreda"].Equals(DBNull.Value))
                        objTaxCenter.Woreda = 0;
                    else
                        objTaxCenter.Woreda = (int)dTable.Rows[0]["Woreda"];
                    if (dTable.Rows[0]["Kebele"].Equals(DBNull.Value))
                        objTaxCenter.Kebele = 0;
                    else
                        objTaxCenter.Kebele = (int)dTable.Rows[0]["Kebele"];
                    if (dTable.Rows[0]["Telephone"].Equals(DBNull.Value))
                        objTaxCenter.Telephone = string.Empty;
                    else
                        objTaxCenter.Telephone = (string)dTable.Rows[0]["Telephone"];
                    if (dTable.Rows[0]["Email"].Equals(DBNull.Value))
                        objTaxCenter.Email = string.Empty;
                    else
                        objTaxCenter.Email = (string)dTable.Rows[0]["Email"];
                    if (dTable.Rows[0]["POBox"].Equals(DBNull.Value))
                        objTaxCenter.POBox = string.Empty;
                    else
                        objTaxCenter.POBox = (string)dTable.Rows[0]["POBox"];
                    if (dTable.Rows[0]["IsActive"].Equals(DBNull.Value))
                        objTaxCenter.IsActive = false;
                    else
                        objTaxCenter.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    if (dTable.Rows[0]["IsDeleted"].Equals(DBNull.Value))
                        objTaxCenter.IsDeleted = false;
                    else
                        objTaxCenter.IsDeleted = (bool)dTable.Rows[0]["IsDeleted"];
                    if (dTable.Rows[0]["CreatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.CreatedUserId = string.Empty;
                    else
                        objTaxCenter.CreatedUserId = dTable.Rows[0]["CreatedUserId"].ToString();
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objTaxCenter.CreatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objTaxCenter.CreatedBy = string.Empty;
                    else
                        objTaxCenter.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["UpdatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedUserId = string.Empty;
                    else
                        objTaxCenter.UpdatedUserId = dTable.Rows[0]["UpdatedUserId"].ToString();
                    if (dTable.Rows[0]["UpdatedDate"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.UpdatedDate = (DateTime)dTable.Rows[0]["UpdatedDate"];
                    if (dTable.Rows[0]["UpdatedBy"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedBy = string.Empty;
                    else
                        objTaxCenter.UpdatedBy = (string)dTable.Rows[0]["UpdatedBy"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objTaxCenter;
        }

        public List<TaxCenter> SearchCenter(string text,int OrgType, bool IsActivSitesOnly)
        {
            CVGeez geez = new CVGeez();
            string sortValue = geez.GetSortValueU(text);
            string parameter_ = sortValue == string.Empty ? text : sortValue; 
            List<TaxCenter> RecordsList = new List<TaxCenter>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CenterName],[CenterNameEnglish],[CenterNameSort],[CenterNameSound],[OrgType],[Parent],[Language],[Region],[Zone],[Woreda],[Kebele],[Telephone],[Email],[POBox],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],
                                    [CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[TaxCenter]  where IsDeleted <> 1 and ([CenterNameEnglish] Like @CenterName or [CenterNameSort] LIKE @CenterName)";


            SqlCommand command = new SqlCommand(); 

            command.Parameters.AddWithValue("@CenterName", "%" + parameter_ + "%");

            if(OrgType != -1)
            {
                strGetAllRecords += " and OrgType = @OrgType";
                command.Parameters.AddWithValue("@OrgType", OrgType);
            }
            if(IsActivSitesOnly)
            {
                strGetAllRecords += " and IsActive = 1";

            }
            strGetAllRecords += " ORDER BY[CenterNameEnglish] asc";
            command.CommandText = strGetAllRecords;
            command.CommandType = CommandType.Text;
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxCenter objTaxCenter = new TaxCenter();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxCenter.Id = 0;
                    else
                        objTaxCenter.Id = (int)dr["Id"];
                    if (dr["CenterName"].Equals(DBNull.Value))
                        objTaxCenter.CenterName = string.Empty;
                    else
                        objTaxCenter.CenterName = (string)dr["CenterName"];
                    if (dr["CenterNameEnglish"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameEnglish = string.Empty;
                    else
                        objTaxCenter.CenterNameEnglish = (string)dr["CenterNameEnglish"];
                    if (dr["CenterNameSort"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSort = string.Empty;
                    else
                        objTaxCenter.CenterNameSort = (string)dr["CenterNameSort"];
                    if (dr["CenterNameSound"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSound = string.Empty;
                    else
                        objTaxCenter.CenterNameSound = (string)dr["CenterNameSound"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objTaxCenter.OrgType = 0;
                    else
                        objTaxCenter.OrgType = (int)dr["OrgType"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objTaxCenter.Parent = 0;
                    else
                        objTaxCenter.Parent = (int)dr["Parent"];
                    if (dr["Language"].Equals(DBNull.Value))
                        objTaxCenter.Language = 0;
                    else
                        objTaxCenter.Language = (int)dr["Language"];
                    if (dr["Region"].Equals(DBNull.Value))
                        objTaxCenter.Region = 0;
                    else
                        objTaxCenter.Region = (int)dr["Region"];
                    if (dr["Zone"].Equals(DBNull.Value))
                        objTaxCenter.Zone = 0;
                    else
                        objTaxCenter.Zone = (int)dr["Zone"];
                    if (dr["Woreda"].Equals(DBNull.Value))
                        objTaxCenter.Woreda = 0;
                    else
                        objTaxCenter.Woreda = (int)dr["Woreda"];
                    if (dr["Kebele"].Equals(DBNull.Value))
                        objTaxCenter.Kebele = 0;
                    else
                        objTaxCenter.Kebele = (int)dr["Kebele"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objTaxCenter.Telephone = string.Empty;
                    else
                        objTaxCenter.Telephone = (string)dr["Telephone"];
                    if (dr["Email"].Equals(DBNull.Value))
                        objTaxCenter.Email = string.Empty;
                    else
                        objTaxCenter.Email = (string)dr["Email"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objTaxCenter.POBox = string.Empty;
                    else
                        objTaxCenter.POBox = (string)dr["POBox"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objTaxCenter.IsActive = false;
                    else
                        objTaxCenter.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objTaxCenter.IsDeleted = false;
                    else
                        objTaxCenter.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.CreatedUserId = string.Empty;
                    else
                        objTaxCenter.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objTaxCenter.CreatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objTaxCenter.CreatedBy = string.Empty;
                    else
                        objTaxCenter.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedUserId = string.Empty;
                    else
                        objTaxCenter.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedBy = string.Empty;
                    else
                        objTaxCenter.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objTaxCenter);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;

        }

        public bool Insert(TaxCenter objTaxCenter)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[TaxCenter]
                                            ([CenterName],[CenterNameEnglish],[CenterNameSort],[CenterNameSound],[OrgType],[Parent],[SiteLevel],[Language],[Region],[Zone],[Woreda],[Kebele],[Telephone],[Email],[POBox],[IsActive],[CreatedUserId],[CreatedDate],[CreatedBy])
                                     VALUES    (@CenterName,@CenterNameEnglish,@CenterNameSort,@CenterNameSound,@OrgType,@Parent,@SiteLevel,@Language,@Region,@Zone,@Woreda,@Kebele,@Telephone,@Email,@POBox,@IsActive,@CreatedUserId,@CreatedDate,@CreatedBy)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CenterName", objTaxCenter.CenterName));
                command.Parameters.Add(new SqlParameter("@CenterNameEnglish", objTaxCenter.CenterNameEnglish));
                command.Parameters.Add(new SqlParameter("@CenterNameSort", objTaxCenter.CenterNameSort));
                command.Parameters.Add(new SqlParameter("@CenterNameSound", objTaxCenter.CenterNameSound));
                command.Parameters.Add(new SqlParameter("@OrgType", objTaxCenter.OrgType));
                command.Parameters.Add(new SqlParameter("@SiteLevel", objTaxCenter.SiteLevel));
                command.Parameters.Add(new SqlParameter("@Parent", objTaxCenter.Parent));
                command.Parameters.Add(new SqlParameter("@Language", objTaxCenter.Language));
                command.Parameters.Add(new SqlParameter("@Region", objTaxCenter.Region.ToString()));
                command.Parameters.Add(new SqlParameter("@Zone", objTaxCenter.Zone.ToString()));
                command.Parameters.Add(new SqlParameter("@Woreda", objTaxCenter.Woreda.ToString()));
                command.Parameters.Add(new SqlParameter("@Kebele", objTaxCenter.Kebele.ToString()));
                command.Parameters.Add(new SqlParameter("@Telephone", objTaxCenter.Telephone));
                if (objTaxCenter.Email != null)
                    command.Parameters.Add(new SqlParameter("@Email", objTaxCenter.Email));
                else
                    command.Parameters.Add(new SqlParameter("@Email", DBNull.Value));
                if (objTaxCenter.POBox != null)
                    command.Parameters.Add(new SqlParameter("@POBox", objTaxCenter.POBox));
                else
                    command.Parameters.Add(new SqlParameter("@POBox", DBNull.Value));

                if (objTaxCenter.IsActive)
                    command.Parameters.Add(new SqlParameter("@IsActive", objTaxCenter.IsActive));
                else
                    command.Parameters.Add(new SqlParameter("@IsActive", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@CreatedUserId", objTaxCenter.CreatedUserId));
                command.Parameters.Add(new SqlParameter("@CreatedDate", objTaxCenter.CreatedDate));
                command.Parameters.Add(new SqlParameter("@CreatedBy", objTaxCenter.CreatedBy));
                


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(TaxCenter objTaxCenter)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[TaxCenter] SET     [CenterName]=@CenterName,    [CenterNameEnglish]=@CenterNameEnglish,   
                            [CenterNameSort]=@CenterNameSort,    [CenterNameSound]=@CenterNameSound,    [OrgType]=@OrgType,    [Parent]=@Parent, [SiteLevel] = @SiteLevel,   
                            [Language]=@Language,    [Region]=@Region,    [Zone]=@Zone,    [Woreda]=@Woreda,    [Kebele]=@Kebele,    [Telephone]=@Telephone,   
                            [Email]=@Email,    [POBox]=@POBox,    [IsActive]=@IsActive,   [UpdatedUserId]=@UpdatedUserId,    [UpdatedDate]=@UpdatedDate,   
                            [UpdatedBy]=@UpdatedBy WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", objTaxCenter.Id));
                command.Parameters.Add(new SqlParameter("@CenterName", objTaxCenter.CenterName));
                command.Parameters.Add(new SqlParameter("@CenterNameEnglish", objTaxCenter.CenterNameEnglish));
                command.Parameters.Add(new SqlParameter("@CenterNameSort", objTaxCenter.CenterNameSort));
                command.Parameters.Add(new SqlParameter("@CenterNameSound", objTaxCenter.CenterNameSound));
                command.Parameters.Add(new SqlParameter("@OrgType", objTaxCenter.OrgType));
                command.Parameters.Add(new SqlParameter("@Parent", objTaxCenter.Parent));
                command.Parameters.Add(new SqlParameter("@SiteLevel", objTaxCenter.SiteLevel));
                command.Parameters.Add(new SqlParameter("@Language", objTaxCenter.Language));
                command.Parameters.Add(new SqlParameter("@Region", objTaxCenter.Region.ToString()));
                command.Parameters.Add(new SqlParameter("@Zone", objTaxCenter.Zone.ToString()));
                command.Parameters.Add(new SqlParameter("@Woreda", objTaxCenter.Woreda.ToString()));
                command.Parameters.Add(new SqlParameter("@Kebele", objTaxCenter.Kebele.ToString()));
                command.Parameters.Add(new SqlParameter("@Telephone", objTaxCenter.Telephone));

                if(objTaxCenter.Email!=null)
                command.Parameters.Add(new SqlParameter("@Email", objTaxCenter.Email));
                else
                command.Parameters.Add(new SqlParameter("@Email", DBNull.Value));
                if(objTaxCenter.POBox != null)
                    command.Parameters.Add(new SqlParameter("@POBox", objTaxCenter.POBox));
                else
                    command.Parameters.Add(new SqlParameter("@POBox", DBNull.Value));

                if(objTaxCenter.IsActive)
                command.Parameters.Add(new SqlParameter("@IsActive", objTaxCenter.IsActive));
                else
                    command.Parameters.Add(new SqlParameter("@IsActive",DBNull.Value));

                
                command.Parameters.Add(new SqlParameter("@UpdatedUserId", objTaxCenter.UpdatedUserId));
                command.Parameters.Add(new SqlParameter("@UpdatedDate", objTaxCenter.UpdatedDate));
                command.Parameters.Add(new SqlParameter("@UpdatedBy", objTaxCenter.UpdatedBy));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"Update [dbo].[TaxCenter] set IsDeleted = 1 WHERE [Id]=@Id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<TaxCenter> GetList()
        {
            List<TaxCenter> RecordsList = new List<TaxCenter>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CenterName],[CenterNameEnglish],[CenterNameSort],[CenterNameSound],[OrgType],[Parent],[Language],[Region],[Zone],[Woreda],[Kebele],[Telephone],[Email],[POBox],[IsActive],[IsDeleted],[CreatedUserId],[CreatedDate],[CreatedBy],[UpdatedUserId],[UpdatedDate],[UpdatedBy] FROM [dbo].[TaxCenter] where IsDeleted <> 1 ORDER BY  [CenterNameEnglish] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxCenter objTaxCenter = new TaxCenter();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxCenter.Id = 0;
                    else
                        objTaxCenter.Id = (int)dr["Id"];
                    if (dr["CenterName"].Equals(DBNull.Value))
                        objTaxCenter.CenterName = string.Empty;
                    else
                        objTaxCenter.CenterName = (string)dr["CenterName"];
                    if (dr["CenterNameEnglish"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameEnglish = string.Empty;
                    else
                        objTaxCenter.CenterNameEnglish = (string)dr["CenterNameEnglish"];
                    if (dr["CenterNameSort"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSort = string.Empty;
                    else
                        objTaxCenter.CenterNameSort = (string)dr["CenterNameSort"];
                    if (dr["CenterNameSound"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameSound = string.Empty;
                    else
                        objTaxCenter.CenterNameSound = (string)dr["CenterNameSound"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objTaxCenter.OrgType = 0;
                    else
                        objTaxCenter.OrgType = (int)dr["OrgType"];
                    if (dr["Parent"].Equals(DBNull.Value))
                        objTaxCenter.Parent = 0;
                    else
                        objTaxCenter.Parent = (int)dr["Parent"];
                    if (dr["Language"].Equals(DBNull.Value))
                        objTaxCenter.Language = 0;
                    else
                        objTaxCenter.Language = (int)dr["Language"];
                    if (dr["Region"].Equals(DBNull.Value))
                        objTaxCenter.Region = 0;
                    else
                        objTaxCenter.Region = (int)dr["Region"];
                    if (dr["Zone"].Equals(DBNull.Value))
                        objTaxCenter.Zone = 0;
                    else
                        objTaxCenter.Zone = (int)dr["Zone"];
                    if (dr["Woreda"].Equals(DBNull.Value))
                        objTaxCenter.Woreda = 0;
                    else
                        objTaxCenter.Woreda = (int)dr["Woreda"];
                    if (dr["Kebele"].Equals(DBNull.Value))
                        objTaxCenter.Kebele = 0;
                    else
                        objTaxCenter.Kebele = (int)dr["Kebele"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objTaxCenter.Telephone = string.Empty;
                    else
                        objTaxCenter.Telephone = (string)dr["Telephone"];
                    if (dr["Email"].Equals(DBNull.Value))
                        objTaxCenter.Email = string.Empty;
                    else
                        objTaxCenter.Email = (string)dr["Email"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objTaxCenter.POBox = string.Empty;
                    else
                        objTaxCenter.POBox = (string)dr["POBox"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objTaxCenter.IsActive = false;
                    else
                        objTaxCenter.IsActive = (bool)dr["IsActive"];
                    if (dr["IsDeleted"].Equals(DBNull.Value))
                        objTaxCenter.IsDeleted = false;
                    else
                        objTaxCenter.IsDeleted = (bool)dr["IsDeleted"];
                    if (dr["CreatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.CreatedUserId = string.Empty;
                    else
                        objTaxCenter.CreatedUserId = dr["CreatedUserId"].ToString();
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objTaxCenter.CreatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objTaxCenter.CreatedBy = string.Empty;
                    else
                        objTaxCenter.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["UpdatedUserId"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedUserId = string.Empty;
                    else
                        objTaxCenter.UpdatedUserId = dr["UpdatedUserId"].ToString();
                    if (dr["UpdatedDate"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedDate = DateTime.MinValue;
                    else
                        objTaxCenter.UpdatedDate = (DateTime)dr["UpdatedDate"];
                    if (dr["UpdatedBy"].Equals(DBNull.Value))
                        objTaxCenter.UpdatedBy = string.Empty;
                    else
                        objTaxCenter.UpdatedBy = (string)dr["UpdatedBy"];
                    RecordsList.Add(objTaxCenter);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string NameEng, string NameAmhSort)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[TaxCenter] WHERE [CenterNameEnglish]=@NameEng or [CenterNameSort] = @CenterNameSort";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@NameEng", NameEng));
                command.Parameters.Add(new SqlParameter("@CenterNameSort", NameAmhSort));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool UpdateExists(string NameEng, string NameAmhSort, int CenterId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Count(Id) FROM [dbo].[TaxCenter] WHERE Id <> @Id and ( [CenterNameEnglish]=@NameEng or [CenterNameSort] = @CenterNameSort)";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Id", CenterId));
                command.Parameters.Add(new SqlParameter("@NameEng", NameEng));
                command.Parameters.Add(new SqlParameter("@CenterNameSort", NameAmhSort));
                connection.Open();
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public List<TaxCenter> GetParents(string OrgType)
        {
            List<TaxCenter> RecordsList = new List<TaxCenter>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Id],[CenterNameEnglish]    FROM [dbo].[TaxCenter] where OrgType < @OrgType and orgType >=  @OrgType - 1  ORDER BY  [CenterNameEnglish] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.AddWithValue("@OrgType", Int32.Parse(OrgType));
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    TaxCenter objTaxCenter = new TaxCenter();
                    if (dr["Id"].Equals(DBNull.Value))
                        objTaxCenter.Id = 0;
                    else
                        objTaxCenter.Id = (int)dr["Id"];

                    if (dr["CenterNameEnglish"].Equals(DBNull.Value))
                        objTaxCenter.CenterNameEnglish = string.Empty;
                    else
                        objTaxCenter.CenterNameEnglish = (string)dr["CenterNameEnglish"];

                    RecordsList.Add(objTaxCenter);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("TaxCenter::GetParents::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


    }
}