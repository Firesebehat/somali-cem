﻿using System;

namespace CUSTOR.Domain
{
    public class CategoryDomain
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public int Parent { get; set; }

        public double AnnualSales { get; set; }

        public double NetProfit { get; set; }

        public int WorkingDays { get; set; }

        public int OtherServices { get; set; }

        public double GrossProfitRrate { get; set; }

        public double ProfitRate { get; set; }

        public double SalesRate { get; set; }

        public double TOTRate { get; set; }

        public double ExciseRate { get; set; }

        public double DriverSalary { get; set; }

        public double AssistantSalary { get; set; }

        public int MinCapacity { get; set; }

        public int MaxCapacity { get; set; }

        public int Above15Years { get; set; }

        public int AppliedTo { get; set; }

        public string ExciseCode { get; set; }

        public string TOTCode { get; set; }

        public double LicensePayment { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public int UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
        public string DescriptionEng { get; set; }
        public string SoundxValue { get; set; }
        public string SortValue { get; set; }
        public string DescriptionShort { get; set; }
    }
}