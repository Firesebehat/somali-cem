﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace CUSTOR.Domain
{
    public class TaxExemption
    {



        public int Id { get; set; }

        public int TaxType { get; set; }

        public DateTime AppliedDateFrom { get; set; }

        public DateTime AppliedDateTo { get; set; }

        public double TaxAmount { get; set; }

        public double TaxPercent { get; set; }

        public decimal IncomeFrom { get; set; }

        public decimal IncomeTo { get; set; }

        public int TaxPayerGrade { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int CenterId { get; set; }

        public string CenterName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public Guid CreatedUserId { get; set; }

        public Guid UpdatedUserId { get; set; }

        public int OverdueDaysFrom { get; set; }

        public int OverdueDaysTo { get; set; }
        public List<CategoryDomain> Categories { get; set; }
        public List<TaxType> Taxes { get; set; }
        public List<int> TaxpyerGrades { get; set; }
        public int CategoryId { get; set; }
        public int ExemptionId { get; set; }
        public string TaxTypeName { get; set; }
        public string CategoryText { get; set; }
        public string TaxPayerGradeText { get; set; }
        public string AppliedDateFromText { get; set; }
        public string AppliedDatToText { get; set; }
        public string Description { get; set; }
    }

}