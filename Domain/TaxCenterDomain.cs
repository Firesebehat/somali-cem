﻿using System;

namespace CUSTOR.Domain
{
    public class TaxCenter
    {
        public int Id { get; set; }

        public string CenterName { get; set; }

        public string CenterNameEnglish { get; set; }

        public string CenterNameSort { get; set; }

        public string CenterNameSound { get; set; }

        public int OrgType { get; set; }

        public int Parent { get; set; }

        public int Language { get; set; }

        public int Region { get; set; }

        public int Zone { get; set; }

        public int Woreda { get; set; }

        public int Kebele { get; set; }

        public string Telephone { get; set; }

        public string Email { get; set; }

        public string POBox { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
        public short SiteLevel { get; set; }
    }
}