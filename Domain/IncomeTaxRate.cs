﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class IncomeTaxRate
    {



        public int Id { get; set; }

        public double IncomeFrom { get; set; }

        public double IncomeTo { get; set; }

        public double Rate { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
        public string DateFromText { get; set; }
        public string DateToText { get; set; }
        public int BudgetYear { get; set; }
    }
}