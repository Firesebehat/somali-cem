﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class UserSiteTransferHistor
    {
    
        public Guid UserId { get; set; }

        public int SiteIdOld { get; set; }

        public int SiteIdNew { get; set; }

        public Guid TransferedBy { get; set; }

        public string CreatedUserName { get; set; }

        public DateTime TransferedDate { get; set; }

        public string Remark { get; set; }
        public string UserName { get; set; }
        public string SiteOldName { get; set; }
        public string SiteNewName { get; set; }
        public string TransferedByName { get; set; }
    }
}