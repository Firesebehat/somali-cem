﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class PaymentAccounts
    {
        public int Id { get; set; }

        public int TaxType { get; set; }

        public int PaymentType { get; set; }

        public int AccountCode { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
        public string AccountCodeName { get; set; }
        public string TaxTypeName { get; set; }
    }
}
