﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class ChartOfAccountsDomain
    {



        public int Id { get; set; }

        public string Code { get; set; }

        public int Parent { get; set; }

        public int Hirachy { get; set; }

        public int AccountType { get; set; }

        public string Name { get; set; }

        public string NameEng { get; set; }

        public string NameSortValue { get; set; }

        public string NameSoundx { get; set; }

        public bool IsMain { get; set; }

        public bool IsBudgetAccount { get; set; }

        public bool IsCapitalBudgetAccount { get; set; }

        public string FatherCode { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
        public bool  IsDeleted { get; set; }
    }
}