﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class ExemptedTaxpayerGrade
    {
        public int Id { get; set; }

        public int ExemptionId { get; set; }

        public int TaxpayerGrade { get; set; }

        public bool IsDeleted { get; set; }

    }
}