﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{

    public class AdminSettingHistory
    {
        public int Id { get; set; }

        public int SettingId { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string Remark { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedByName { get; set; }

    }
}