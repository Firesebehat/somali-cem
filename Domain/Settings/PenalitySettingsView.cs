﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class PenalitySettingsView
    {
        public string Id { get; set; }

        public string OneMonthDelayedDeclaration { get; set; }

        public string MoreThanOneMonthDelayedDeclaration { get; set; }

        public string NotDeclaringYearlyIncome { get; set; }

        public string OneMonthUnderStatement { get; set; }

        public string TwoMonthUnderStatement { get; set; }

        public string ThreeMonthUnderStatement { get; set; }

        public string NoFinancialRecord { get; set; }

        public string BankInterestRate { get; set; }

        public string RevenueInterestRate { get; set; }

        public string PenalityDateFrom { get; set; }

        public string PenalityDateFromEthiopic { get; set; }

        public string PenalityDateTo { get; set; }
    }
}
