﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class PenaltyLiftingSettingView
    { 
        public int Id { get; set; }

        public string Description { get; set; }

        public int Parent { get; set; }

        public bool IsParent { get; set; }

        public int DelayPenaltyPercentage { get; set; }

        public int UnderStatementPercentage { get; set; }

        public int LackOfStatementPenaltyPercentage { get; set; }

        public int HidingIncomePenaltyPercentage { get; set; }

        public string Date { get; set; }
    }
}
