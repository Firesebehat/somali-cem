﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class SalarySettingsView
    {
        public string  TransportAllowancePercent { get; set; }

        public string TransportAllowanceInBirr { get; set; }

        public string DelegationAllowance { get; set; }

        public string ProvidentFund { get; set; }

        public string MonthlyCostSharing { get; set; }

        public string  SalarySettingDateFrom { get; set; }

        public string SalarySettingDateFromEthiopic { get; set; }

        public string SalarySettingDateTo { get; set; }
    }
}
