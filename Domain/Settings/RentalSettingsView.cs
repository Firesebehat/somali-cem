﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class RentalSettingsView
    {
        public string RentalTaxFromIncomePercent { get; set; }

        public string Grade { get; set; }

        public string DateFrom { get; set; }

        public string DateFromEthiopic { get; set; }

        public string DateTo { get; set; }
    }
}
