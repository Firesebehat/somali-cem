﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class LandUseSettingsView
    {
        public string NonCooperativeAssociation { get; set; }

        public string CooperativeAssociation { get; set; }

        public string MonthlyDelayedDeclaration { get; set; }

        public string LandUseDateFrom { get; set; }

        public string LandUseDateFromEthiopic { get; set; }

        public string LandUseDateTo { get; set; }
    }
}
