﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public  class AgriculutreSettingsView
    {
        public int Id { get; set; }

        public string AgricultureCategoryCode { get; set;}

        public string AgricultureTaxDelayFine { get; set;}

        public string AgricultureTaxArchiveFine { get; set;}

        public string AgricultureDateFrom { get; set; }

        public string AgricultureDateFromEthiopic { get; set; }

        public string AgricultureDateTo { get; set; }

    }
}
