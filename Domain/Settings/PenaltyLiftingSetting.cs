﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class PenaltyLiftingSetting
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string DescriptionSortV { get; set; }

        public string DescriptionSoundX { get; set; }

        public int Parent { get; set; }

        public bool IsParent { get; set; }

        public int DelayPenaltyPercentage { get; set; }

        public int UnderStatementPercentage { get; set; }

        public int LackOfStatementPenaltyPercentage { get; set; }

        public int HidingIncomePenaltyPercentage { get; set; }

        public DateTime Date { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int CenterId { get; set; }

        public string CenterName { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
    }
}
