﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{ 
    public class Setting
    {
        public int Id { get; set; }

        public string AppId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public int Category { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public byte[] Picture { get; set; }

        public int CenterId { get; set; }

        public string CenterName { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

    }
}
