﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{


    public class aspnet_Roles
    {
        public string ApplicationId { get; set; }

        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string LoweredRoleName { get; set; }

        public string Description { get; set; }

    }
}