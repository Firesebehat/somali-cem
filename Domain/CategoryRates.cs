﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class CategoryRates
    {



        public int Id { get; set; }

        public int CategoryId { get; set; }

        public decimal GrossProfit { get; set; }

        public double GrossProfitRate { get; set; }

        public decimal AnnualSales { get; set; }

        public decimal NetProfit { get; set; }

        public double NetProfitRate { get; set; }

        public decimal DriverSalary { get; set; }

        public decimal AssistantSalary { get; set; }

        public double SalesRate { get; set; }

        public string TOTCode { get; set; }

        public double TOTRate { get; set; }

        public string ExciseCode { get; set; }

        public double ExciseRate { get; set; }

        public double MinCapacity { get; set; }

        public double MaxCapacity { get; set; }

        public DateTime AppliedDateFrom { get; set; }

        public DateTime AppliedDateTo { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public int MinAge { get; set; }

        public int MaxAge { get; set; }

        public int WorkingDays { get; set; }
        public int Unit { get; set; }
        public string CreatedDateText { get; set; }
        public string AppliedDateToText { get; set; }
        public string AppliedDateFromText { get; set; }
        public int BudgetYear { get; set; }
        public int Above15Years { get; set; }
    }
}