﻿namespace CUSTOR.Domain
{
    public class Region
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public string AmDescription { get; set; }

        public string Parent { get; set; }

        public int Id { get; set; }
        public string AmDescriptionSort { get; set; }
    }
}