﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace CUSTOR.Domain
{





    public class Address
    {
        public int Id { get; set; }

        public bool IsMainOffice { get; set; }

        public string BranchName { get; set; }

        public int AddressType { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public string Zone { get; set; }

        public string Wereda { get; set; }

        public string Kebele { get; set; }

        public string HouseNo { get; set; }

        public string TeleNo { get; set; }

        public string POBox { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string ResidenceCountry { get; set; }

        public string Username { get; set; }

        public DateTime EventDateTime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

        public int OwnerId { get; set; }
        public string OtherAddress { get; set; }
        public string Remark { get; set; }
    }
}