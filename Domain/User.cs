﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace CUSTOR.Domain
{
    public class User
    {

        public Guid ApplicationId { get; set; }

        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string LoweredUserName { get; set; }

        public string MobileAlias { get; set; }

        public bool IsAnonymous { get; set; }

        public DateTime LastActivityDate { get; set; }

        public int TaxCenterId { get; set; }

        public int UserGroupId { get; set; }
        public string FullName { get; set; }

        public string UserHash { get; set; }
        public string TaxCenterName { get; set; }
        public string MobileNo { get; set; }
    }

    public class Role
    {
        public string RoleName { get; set; }
        public int UserCount { get; set; }
        public string UsersOfRole { get; set; }
        public string Description { get; set; }
    }
}