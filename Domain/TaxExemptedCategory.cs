﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class TaxExemptedCategory
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public int ExemptionId { get; set; }

        public bool IsDeleted { get; set; }
        public string CategoryName { get; set; }
        public string CategoryCode { get; set; }
    }
}