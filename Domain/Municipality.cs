﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{

    public class Municipality
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public int Rank { get; set; }

        public decimal AnnualFee { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedUserId { get; set; }

        public string UpdatedBy { get; set; }

        public string UpdatedUserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

    }
}