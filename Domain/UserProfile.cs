﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Profile;
using System.Web.Security;

namespace CUSTOR
{
    public class UserProfile
    {

        public int RowNumber { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string CreatedBy { get; set; }

        public string ApprovedBy { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public string LastLoggingIp { get; internal set; }
    }
    public class CUser

    {
        string _ConnectionString = string.Empty;

        public CUser()
        {
            _ConnectionString = ConnectionString;
        }
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public string GetUser(string user)
        {

            string strSQL = @"SELECT 	UserName FROM aspnet_Membership 
						        INNER   JOIN aspnet_Users 
						        ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						        WHERE	UserName = @UserName";

            SqlConnection mConn = null;
            try
            {
                mConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = strSQL;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserName", user);
                cmd.Connection = mConn;
                mConn.Open();
                string strUser = (string)cmd.ExecuteScalar();
                return strUser;
            }
            catch
            {
                return string.Empty; ;
            }
            finally
            {
                mConn.Close();
            }
        }
        public DataTable GetUsers(string strName, int intStartRowIndex, int intMaxRows)
        {
            #region Query
            string strSQL = @"SELECT 
						RowNumber, 
						UserName, 
						Email, 
						IsApproved, 
						IsLockedOut, 
						CreateDate, 
						LastLoginDate, 
						LastActivityDate,
						CreatedBy, 
						ApprovedBy
						
				FROM 
					(
						SELECT 	ROW_NUMBER() OVER (ORDER BY aspnet_Users.UserName DESC) AS RowNumber, 
								aspnet_Users.UserName, 
								aspnet_Membership.Email, 
								aspnet_Membership.IsApproved, 
								aspnet_Membership.IsLockedOut, 
								aspnet_Membership.CreateDate, 
								aspnet_Membership.LastLoginDate, 
								aspnet_Users.LastActivityDate,
								aspnet_Users.CreatedBy, 
								aspnet_Users.ApprovedBy
						FROM	aspnet_Membership 
						INNER   JOIN aspnet_Users 
						ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						WHERE	UserName LIKE @UserName
					 ) as u
				WHERE	u.RowNumber > @startRowIndex 
				AND		u.RowNumber <= (@startRowIndex + @maximumRows) order by UserName";
            #endregion
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = strSQL;
            command.CommandType = CommandType.Text;

            DataTable dTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.AddWithValue("@UserName", strName);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
                connection.Open();
                adapter.Fill(dTable);

                return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CUser::GetUsersByName::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }

        }
        private bool IsAdmin(string strUser)
        {
            try
            {

                //return Roles.IsUserInRole(strUser, "Administrator");
                return Roles.IsUserInRole(strUser, "Administrator"); //HR Data Entry to be removed
            }
            catch (Exception)
            {

                return false;
            }
            //MembershipUser userName = Membership.GetUser(strUser);

        }
        private bool IsSuperAdmin(string strUser)
        {
            try
            {

                //return Roles.IsUserInRole(strUser, "Administrator");
                return Roles.IsUserInRole(strUser, "SuperAdministrator"); //HR Data Entry to be removed
            }
            catch (Exception)
            {

                return false;
            }
            //MembershipUser userName = Membership.GetUser(strUser);

        }
        private bool IsRegionalAdmin(string strUser)
        {
            try
            {

                //return Roles.IsUserInRole(strUser, "Administrator");
                return Roles.IsUserInRole(strUser, "RegionalAdministrator"); //HR Data Entry to be removed
            }
            catch (Exception)
            {

                return false;
            }
            //MembershipUser userName = Membership.GetUser(strUser);

        }
        private bool IsDataEntry(string strUser)
        {
            try
            {

                //return Roles.IsUserInRole(strUser, "Administrator");
                return Roles.IsUserInRole(strUser, "HR Data Entry"); //HR Data Entry to be removed
            }
            catch (Exception)
            {

                return false;
            }
            //MembershipUser userName = Membership.GetUser(strUser);

        }
        private bool HasSameOrg(string strUser, string strOrgId)
        {
            var p = ProfileBase.Create(strUser);
            var org = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));
            if (p == null || org == null) return false;

            return (strOrgId == (string)org.GetPropertyValue("Code"));

            //ProfileBase pp = ProfileBase.Create(strUser);
            //if (pp == null) return false;
            //string strID = pp.GetPropertyValue("Organization.ParentCode").ToString();
            //return (strID==strOrgID);
        }
        public List<UserProfile> GetDataEntryUsers(string strName, int intStartRowIndex, int intMaxRows)
        {
            List<UserProfile> RecordsList = new List<UserProfile>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            #region Query
            string strSQL = @"SELECT 
                            distinct 
						ROW_NUMBER() OVER (ORDER BY UserName DESC) AS RowNumber , 
						UserName, 
						Email, 
						IsApproved, 
						IsLockedOut, 
						CreateDate, 
						LastLoginDate, 
						LastActivityDate,
						CreatedBy, 
						ApprovedBy,
                        LastLoggingIp
						
				FROM 
					(
						SELECT distinct	
								aspnet_Users.UserName, 
								aspnet_Membership.Email, 
								aspnet_Membership.IsApproved, 
								aspnet_Membership.IsLockedOut, 
								(select top(1) CreateDate  from aspnet_Membership where aspnet_Membership.UserId = aspnet_Users.UserId order by CreateDate desc) CreateDate, 
								(select top(1) LastLoginDate  from aspnet_Membership where aspnet_Membership.UserId = aspnet_Users.UserId order by LastLoginDate desc) LastLoginDate, 

								aspnet_Users.LastActivityDate,
								aspnet_Users.CreatedBy, 
								aspnet_Users.ApprovedBy,
                                aspnet_Users.TaxCenterId,
                                LastLoggingIp = (select top(1) IPAddress from Logged_User_History lu where lu.UserName =aspnet_Users.UserName order by LoggingDate desc )
						FROM	aspnet_Membership 
						INNER   JOIN aspnet_Users 
						ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						
						WHERE	UserName LIKE @UserName 
					 ) as u
				WHERE 1=1 ";
            #endregion

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.AddWithValue("@UserName", strName);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserProfile objUser = new UserProfile();
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];

                    if (dr["RowNumber"].Equals(DBNull.Value))
                        objUser.RowNumber = 0;
                    else
                        objUser.RowNumber = Convert.ToInt32(dr["RowNumber"]);

                    if (dr["Email"].Equals(DBNull.Value))
                        objUser.Email = string.Empty;
                    else
                        objUser.Email = (string)dr["Email"];
                    if (dr["IsApproved"].Equals(DBNull.Value))
                        objUser.IsApproved = false;
                    else
                        objUser.IsApproved = (bool)dr["IsApproved"];
                    if (dr["IsLockedOut"].Equals(DBNull.Value))
                        objUser.IsLockedOut = false;
                    else
                        objUser.IsLockedOut = (bool)dr["IsLockedOut"];

                    if (dr["CreateDate"].Equals(DBNull.Value))
                        objUser.CreateDate = DateTime.Now;
                    else
                        objUser.CreateDate = (DateTime)dr["CreateDate"];
                    if (dr["LastLoginDate"].Equals(DBNull.Value))
                        objUser.LastLoginDate = DateTime.Now;
                    else
                        objUser.LastLoginDate = (DateTime)dr["LastLoginDate"];

                    if (dr["LastActivityDate"].Equals(DBNull.Value))
                        objUser.LastActivityDate = DateTime.Now;
                    else
                        objUser.LastActivityDate = (DateTime)dr["LastActivityDate"];

                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objUser.CreatedBy = string.Empty;
                    else
                        objUser.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["ApprovedBy"].Equals(DBNull.Value))
                        objUser.ApprovedBy = string.Empty;
                    else
                        objUser.ApprovedBy = (string)dr["ApprovedBy"];

                    if (IsDataEntry(objUser.UserName))
                        RecordsList.Add(objUser);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("User::GetAdminUsers::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<UserProfile> GetAdminUsers(string strName, int activeUser, string OrgCode, int intStartRowIndex, int intMaxRows)
        {
            List<UserProfile> RecordsList = new List<UserProfile>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            #region Query
            string strSQL = @"SELECT 
                            distinct 
						ROW_NUMBER() OVER (ORDER BY UserName DESC) AS RowNumber , 
						UserName, 
						Email, 
						IsApproved, 
						IsLockedOut, 
						CreateDate, 
						LastLoginDate, 
						LastActivityDate,
						CreatedBy, 
						ApprovedBy,
                        LastLoggingIp
						
				FROM 
					(
						SELECT distinct	
								aspnet_Users.UserName, 
								aspnet_Membership.Email, 
								aspnet_Membership.IsApproved, 
								aspnet_Membership.IsLockedOut, 
								(select top(1) CreateDate  from aspnet_Membership where aspnet_Membership.UserId = aspnet_Users.UserId order by CreateDate desc) CreateDate, 
								(select top(1) LastLoginDate  from aspnet_Membership where aspnet_Membership.UserId = aspnet_Users.UserId order by LastLoginDate desc) LastLoginDate, 

								aspnet_Users.LastActivityDate,
								aspnet_Users.CreatedBy, 
								aspnet_Users.ApprovedBy,
                                aspnet_Users.TaxCenterId,
                                LastLoggingIp = (select top(1) IPAddress from Logged_User_History lu where lu.UserName =aspnet_Users.UserName order by LoggingDate desc )
						FROM	aspnet_Membership 
						INNER   JOIN aspnet_Users 
						ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						
						WHERE	UserName LIKE @UserName 
					 ) as u
				WHERE 1=1 ";
            if (activeUser == 1)
                strSQL += " and isApproved = 1";
            else if (activeUser == 0)
                strSQL += " and isApproved = 0";


            if (OrgCode != string.Empty && OrgCode != "0")
            {
                strSQL += " AND u.TaxCenterId = @OrgCode";
            }
            strSQL += " order by UserName";
            #endregion

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.AddWithValue("@UserName", strName);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
                command.Parameters.AddWithValue("@IsApproved", activeUser);
                command.Parameters.AddWithValue("@OrgCode", OrgCode);

                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserProfile objUser = new UserProfile();
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];

                    if (dr["RowNumber"].Equals(DBNull.Value))
                        objUser.RowNumber = 0;
                    else
                        objUser.RowNumber = Convert.ToInt32(dr["RowNumber"]);

                    if (dr["Email"].Equals(DBNull.Value))
                        objUser.Email = string.Empty;
                    else
                        objUser.Email = (string)dr["Email"];
                    if (dr["IsApproved"].Equals(DBNull.Value))
                        objUser.IsApproved = false;
                    else
                        objUser.IsApproved = (bool)dr["IsApproved"];
                    if (dr["IsLockedOut"].Equals(DBNull.Value))
                        objUser.IsLockedOut = false;
                    else
                        objUser.IsLockedOut = (bool)dr["IsLockedOut"];

                    if (dr["CreateDate"].Equals(DBNull.Value))
                        objUser.CreateDate = DateTime.Now;
                    else
                        objUser.CreateDate = (DateTime)dr["CreateDate"];
                    objUser.CreateDate = DateTime.SpecifyKind(DateTime.Parse(objUser.CreateDate.ToString()), DateTimeKind.Utc).ToLocalTime();
                    if (dr["LastLoginDate"].Equals(DBNull.Value))
                        objUser.LastLoginDate = DateTime.Now;
                    else
                        objUser.LastLoginDate = (DateTime)dr["LastLoginDate"];
                    objUser.LastLoginDate = DateTime.SpecifyKind(DateTime.Parse(objUser.LastLoginDate.ToString()), DateTimeKind.Utc).ToLocalTime();


                    if (dr["LastActivityDate"].Equals(DBNull.Value))
                        objUser.LastActivityDate = DateTime.Now;
                    else
                        objUser.LastActivityDate = (DateTime)dr["LastActivityDate"];
                    objUser.LastActivityDate = DateTime.SpecifyKind(DateTime.Parse(objUser.LastActivityDate.ToString()), DateTimeKind.Utc).ToLocalTime();


                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objUser.CreatedBy = string.Empty;
                    else
                        objUser.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["ApprovedBy"].Equals(DBNull.Value))
                        objUser.ApprovedBy = string.Empty;
                    else
                        objUser.ApprovedBy = (string)dr["ApprovedBy"];
                    if (dr["LastLoggingIp"].Equals(DBNull.Value))
                        objUser.LastLoggingIp = string.Empty;
                    else
                        objUser.LastLoggingIp = (string)dr["LastLoggingIp"];
                    // if(IsAdmin(objUser.UserName) || IsSuperAdmin(objUser.UserName) || IsRegionalAdmin(objUser.UserName))


                    RecordsList.Add(objUser);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("User::GetAdminUsers::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<UserProfile> GetUsersByOrg(string CurrentUserName, int CurrentUserSite, int OrganizationType, 
            string SearchedName,  string SearchedOrgSite, int activeUser, int intStartRowIndex, int intMaxRows)
        {
            List<UserProfile> RecordsList = new List<UserProfile>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            #region Query
            
            string strSQL = @"SELECT 
						RowNumber, 
						UserName, 
						Email, 
						IsApproved, 
						IsLockedOut, 
						CreateDate, 
						LastLoginDate, 
						LastActivityDate,
						CreatedBy, 
						ApprovedBy,
                        LastLoggingIp
						
				FROM 
					(
						SELECT DISTINCT 	ROW_NUMBER() OVER (ORDER BY aspnet_Users.UserName DESC) AS RowNumber, 
								aspnet_Users.UserName, 
								aspnet_Membership.Email, 
								aspnet_Membership.IsApproved, 
								aspnet_Membership.IsLockedOut, 
								aspnet_Membership.CreateDate, 
								aspnet_Membership.LastLoginDate, 
								aspnet_Users.LastActivityDate,
								aspnet_Users.CreatedBy, 
								aspnet_Users.ApprovedBy, aspnet_Users.TaxCenterId,
                                LastLoggingIp = (select top(1) IPAddress from Logged_User_History lu where lu.UserName =aspnet_Users.UserName order by LoggingDate desc )
						FROM	aspnet_Membership 
						INNER   JOIN aspnet_Users 
						ON		aspnet_Membership.UserId = aspnet_Users.UserId 
						WHERE	UserName <> @CurrentUserName and UserName  LIKE @UserName and aspnet_Users.UserId NOT IN  (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName in     ('super admin','admin')))
					 ) as u
				WHERE	u.RowNumber > @startRowIndex 
				AND		u.RowNumber <= (@startRowIndex + @maximumRows) ";
            if (activeUser == 1)
                strSQL += " and isApproved = 1";
            else if (activeUser == 0)
                strSQL += " and isApproved = 0";

            if (SearchedOrgSite != string.Empty && SearchedOrgSite != "0")
            {
                strSQL += " AND u.TaxCenterId = @OrgCode";
            }
            if (CurrentUserSite != 0)
            {
                strSQL += " AND u.TaxCenterId = @CurrentSite";
                if (OrganizationType == (int)CUSTORCommon.Enumerations.OrganizationalType.OrgType.Regional)
                {
                    strSQL += " or u.TaxCenterId in (select id from TaxCenter where Parent = @CurrentSite)";
                }
            }
            strSQL += " order by UserName";
            #endregion

            SqlCommand command = new SqlCommand() { CommandText = strSQL, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.AddWithValue("@CurrentSite", CurrentUserSite);
                command.Parameters.AddWithValue("@CurrentUserName", CurrentUserName);
                command.Parameters.AddWithValue("@UserName", SearchedName);
                command.Parameters.AddWithValue("@OrgCode", SearchedOrgSite);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
        
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserProfile objUser = new UserProfile();
                    if (dr["UserName"].Equals(DBNull.Value))
                        objUser.UserName = string.Empty;
                    else
                        objUser.UserName = (string)dr["UserName"];

                    if (dr["RowNumber"].Equals(DBNull.Value))
                        objUser.RowNumber = 0;
                    else
                        objUser.RowNumber = Convert.ToInt32(dr["RowNumber"]);

                    if (dr["Email"].Equals(DBNull.Value))
                        objUser.Email = string.Empty;
                    else
                        objUser.Email = (string)dr["Email"];
                    if (dr["IsApproved"].Equals(DBNull.Value))
                        objUser.IsApproved = false;
                    else
                        objUser.IsApproved = (bool)dr["IsApproved"];
                    if (dr["IsLockedOut"].Equals(DBNull.Value))
                        objUser.IsLockedOut = false;
                    else
                        objUser.IsLockedOut = (bool)dr["IsLockedOut"];

                    if (dr["CreateDate"].Equals(DBNull.Value))
                        objUser.CreateDate = DateTime.Now;
                    else
                        objUser.CreateDate = (DateTime)dr["CreateDate"];
                    objUser.CreateDate = DateTime.SpecifyKind(DateTime.Parse(objUser.CreateDate.ToString()), DateTimeKind.Utc).ToLocalTime();
                    if (dr["LastLoginDate"].Equals(DBNull.Value))
                        objUser.LastLoginDate = DateTime.Now;
                    else
                        objUser.LastLoginDate = (DateTime)dr["LastLoginDate"];
                    objUser.LastLoginDate = DateTime.SpecifyKind(DateTime.Parse(objUser.LastLoginDate.ToString()), DateTimeKind.Utc).ToLocalTime();


                    if (dr["LastActivityDate"].Equals(DBNull.Value))
                        objUser.LastActivityDate = DateTime.Now;
                    else
                        objUser.LastActivityDate = (DateTime)dr["LastActivityDate"];
                    objUser.LastActivityDate = DateTime.SpecifyKind(DateTime.Parse(objUser.LastActivityDate.ToString()), DateTimeKind.Utc).ToLocalTime();


                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objUser.CreatedBy = string.Empty;
                    else
                        objUser.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["ApprovedBy"].Equals(DBNull.Value))
                        objUser.ApprovedBy = string.Empty;
                    else
                        objUser.ApprovedBy = (string)dr["ApprovedBy"];
                    if (dr["LastLoggingIp"].Equals(DBNull.Value))
                        objUser.LastLoggingIp = string.Empty;
                    else
                        objUser.LastLoggingIp = (string)dr["LastLoggingIp"];
                    //if (//HasSameOrg(objUser.UserName, strOrgID) &&
                    //    !Roles.IsUserInRole(objUser.UserName, "Administrator")
                    //    && !Roles.IsUserInRole(objUser.UserName, "Super Admin"))
                    RecordsList.Add(objUser);
                }


            }
            catch (Exception ex)
            {
                throw new Exception("User::GetAdminUsers::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
    
        public DataTable GetUsersByName(string strName, int intStartRowIndex, int intMaxRows)
        {

            string strSQl = "[sp_wsat_GetUsersByName]";

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = strSQl;
            command.CommandType = CommandType.StoredProcedure;

            DataTable dTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.AddWithValue("@UserName", strName);
                command.Parameters.AddWithValue("@startRowIndex", intStartRowIndex);
                command.Parameters.AddWithValue("@maximumRows", intMaxRows);
                connection.Open();
                adapter.Fill(dTable);

                return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CUser::GetUsersByName::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }
    }
}