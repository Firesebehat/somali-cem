﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class MiscellaneousPaymentAccount
    {
        public int Id { get; set; }

        public string Payment_reason { get; set; }

        public string Payment_reason_eng { get; set; }

        public string Account_name { get; set; }

        public string Account_code { get; set; }

        public string Code { get; set; }
    }
}
