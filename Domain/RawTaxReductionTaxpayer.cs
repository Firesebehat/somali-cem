﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class RawTaxReductionTaxpayer
    {
        public int Id { get; set; }

        public int TaxpayerGrade { get; set; }

        public int RawTaxReductionId { get; set; }

        public bool IsDeleted { get; set; }

    }
}