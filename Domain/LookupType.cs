﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class LookupType
    {



        public int Id { get; set; }

        public string Description { get; set; }

        public string Amdescription { get; set; }

        public string SortValue { get; set; }

        public string Soundx { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

    }
}