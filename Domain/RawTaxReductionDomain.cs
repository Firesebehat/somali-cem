﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class RawTaxReductionDomain
    {
        public int Id { get; set; }

        public int TaxType { get; set; }

        public DateTime AppliedDateFrom { get; set; }

        public DateTime AppliedDateTo { get; set; }

        public double DeductionAmount { get; set; }

        public double RawTaxReductionPercent { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int CenterId { get; set; }

        public string CenterName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public Guid CreatedUserId { get; set; }

        public Guid UpdatedUserId { get; set; }
        public string TaxTypeName { get; set; }
        public string AppliedDateFromText { get; set; }
        public string AppliedDateToText { get; set; }
        public int TaxPayerGrade { get; set; }
        public string TaxpayerGradText { get; set; }
        public string Description { get; set; }
    }
}