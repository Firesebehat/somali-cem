﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace CUSTOR.Taxpayer
{
    public class  DBHelper
    {
        public static string strConn;
        private SqlCommand dbCmd;
        private SqlConnection dbConn;
        private SqlDataReader dbReader;
        private DataTable dbTable;
        public DBHelper()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["ConnectionString"];
            if (settings != null)
            {
                strConn = settings.ConnectionString;
            }
        }

        public bool Delete(string strSQL)
        {
            SqlConnection connection = new SqlConnection(strConn);
            SqlCommand command = new SqlCommand();
            command.CommandText = strSQL;
            command.CommandType = CommandType.Text;
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CDataAccess::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


         public DataTable GetRecord(string pStroedProcedureName, SqlParameterCollection pColl)
        {
            try
            {
                dbTable = new DataTable();
                dbConn = new SqlConnection(strConn);
                dbCmd = dbConn.CreateCommand();
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.CommandText = pStroedProcedureName;
                if (pColl != null)
                {
                    for (int i = 0; i < pColl.Count; i++)
                    {
                        dbCmd.Parameters.Add(pColl[i]);
                    }
                }
                dbConn.Open();
                dbReader = dbCmd.ExecuteReader();
                if (dbReader != null && dbReader.HasRows)
                {
                    dbTable.Load(dbReader);
                }
            }
            finally
            {
                dbConn.Close();
            }
            return dbTable;
        }

         //public bool Insert(string strSQL)
         //{
         //    SqlConnection connection = new SqlConnection(strConn);
         //    SqlCommand command = new SqlCommand();
         //    command.CommandText = strSQL;
         //    command.CommandType = CommandType.Text;
         //    command.Connection = connection;

         //    try
         //    {
         //        connection.Open();
         //        int _rowsAffected = command.ExecuteNonQuery();
         //        return true;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw new Exception("CDataAccess::Insert::Error!" + ex.Message, ex);
         //    }
         //    finally
         //    {
         //        connection.Close();
         //        command.Dispose();
         //    }
         //}

         public int Insert(string strSQL)
         {
             SqlConnection connection = new SqlConnection(strConn);
             SqlCommand command = new SqlCommand();
             command.CommandText = strSQL;
             command.CommandType = CommandType.Text;
             command.Connection = connection;

             try
             {
                 connection.Open();
                 int _rowId = command.ExecuteNonQuery();
                 return _rowId;
             }
             catch (Exception ex)
             {
                 throw new Exception("CDataAccess::Insert::Error!" + ex.Message, ex);
             }
             finally
             {
                 connection.Close();
                 command.Dispose();
             }
         }

        public  DataTable GetRecord(string strSQL)
        {
            try
            {
                dbTable = new DataTable();
                dbConn = new SqlConnection(strConn);
                dbCmd = dbConn.CreateCommand();
                dbCmd.CommandText = strSQL;
                dbConn.Open();
                dbReader = dbCmd.ExecuteReader();
                if (dbReader != null && dbReader.HasRows)
                {
                    dbTable.Load(dbReader);
                }
            }
            finally
            {
                dbConn.Close();
            }
            return dbTable;
        }
        public string GetValueString(string strSQL)//kinfe
        {
            try
            {
                dbConn = new SqlConnection(strConn);
                dbConn.Open();
                SqlCommand cmd = new SqlCommand(strSQL, dbConn);
                string result = cmd.ExecuteScalar().ToString();

                return result;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public bool UpdateRecord(string strSQL)
        {
            try
            {
                int _RowId = 0;
                dbTable = new DataTable();
                dbConn = new SqlConnection(strConn);
                dbCmd = dbConn.CreateCommand();
                dbCmd.CommandText = strSQL;
                dbConn.Open();
                _RowId = (int)dbCmd.ExecuteScalar();
                if (_RowId == 0)
                {
                    return false;
                }

            }
            finally
            {
                dbConn.Close();
            }
            return true;
        }
        public string GetConnectionString()
        {
            return strConn;
        }

    }

}