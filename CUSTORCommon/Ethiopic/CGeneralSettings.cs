﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTORCommon.Ethiopic

{
    public class CGeneralSettings
    {


        string _ConnectionString = string.Empty;
        public CGeneralSettings()
        {
            _ConnectionString = ConnectionString;
        }
        public CGeneralSettings(double RenewalDuration, double TempRecognitionDuration, int RenewalDurationUnit, int TempRecognitionDurationUnit, DateTime TradeStartTime, DateTime TradeEndTime, int MonthWithoutTrade)
        {
            _renewalDuration = RenewalDuration;
            _tempRecognitionDuration = TempRecognitionDuration;
            _renewalDurationUnit = RenewalDurationUnit;
            _tempRecognitionDurationUnit = TempRecognitionDurationUnit;
            _tradeStartTime = TradeStartTime;
            _tradeEndTime = TradeEndTime;
            _monthWithoutTrade = MonthWithoutTrade;
        }
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }


        #region Class Property Declarations
        double _renewalDuration;
        public double RenewalDuration
        {
            get { return _renewalDuration; }
            set { _renewalDuration = value; }
        }

        double _tempRecognitionDuration;
        public double TempRecognitionDuration
        {
            get { return _tempRecognitionDuration; }
            set { _tempRecognitionDuration = value; }
        }

        int _renewalDurationUnit;
        public int RenewalDurationUnit
        {
            get { return _renewalDurationUnit; }
            set { _renewalDurationUnit = value; }
        }

        int _tempRecognitionDurationUnit;
        public int TempRecognitionDurationUnit
        {
            get { return _tempRecognitionDurationUnit; }
            set { _tempRecognitionDurationUnit = value; }
        }

        DateTime _tradeStartTime;
        public DateTime TradeStartTime
        {
            get { return _tradeStartTime; }
            set { _tradeStartTime = value; }
        }

        DateTime _tradeEndTime;
        public DateTime TradeEndTime
        {
            get { return _tradeEndTime; }
            set { _tradeEndTime = value; }
        }

        int _monthWithoutTrade;
        public int MonthWithoutTrade
        {
            get { return _monthWithoutTrade; }
            set { _monthWithoutTrade = value; }
        }

        int _budgetYear;
        public int BudgetYear
        {
            get { return _budgetYear; }
            set { _budgetYear = value; }
        }


        bool _includeCriteria;
        public bool IncludeCriteria
        {
            get { return _includeCriteria; }
            set { _includeCriteria = value; }
        }

        bool _includePayment;
        public bool IncludePayment
        {
            get { return _includePayment; }
            set { _includePayment = value; }
        }

        int _numberOfMangerAllowed;
        public int NumberOfMangerAllowed
        {
            get { return _numberOfMangerAllowed; }
            set { _numberOfMangerAllowed = value; }
        }
        #endregion


        public DataTable GetRecord()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_GetRecord]";
            command.CommandType = CommandType.StoredProcedure;
            DataTable dTable = new DataTable("tblGeneralSetting");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["RenewalDuration"].Equals(DBNull.Value))
                        _renewalDuration = 0;
                    else
                        _renewalDuration = (double)dTable.Rows[0]["RenewalDuration"];
                    if (dTable.Rows[0]["TempRecognitionDuration"].Equals(DBNull.Value))
                        _tempRecognitionDuration = 0;
                    else
                        _tempRecognitionDuration = (double)dTable.Rows[0]["TempRecognitionDuration"];
                    if (dTable.Rows[0]["RenewalDurationUnit"].Equals(DBNull.Value))
                        _renewalDurationUnit = 0;
                    else
                        _renewalDurationUnit = (int)dTable.Rows[0]["RenewalDurationUnit"];
                    if (dTable.Rows[0]["TempRecognitionDurationUnit"].Equals(DBNull.Value))
                        _tempRecognitionDurationUnit = 0;
                    else
                        _tempRecognitionDurationUnit = (int)dTable.Rows[0]["TempRecognitionDurationUnit"];
                    if (dTable.Rows[0]["TradeStartTime"].Equals(DBNull.Value))
                        _tradeStartTime = DateTime.MinValue;
                    else
                        _tradeStartTime = (DateTime)dTable.Rows[0]["TradeStartTime"];
                    if (dTable.Rows[0]["TradeEndTime"].Equals(DBNull.Value))
                        _tradeEndTime = DateTime.MinValue;
                    else
                        _tradeEndTime = (DateTime)dTable.Rows[0]["TradeEndTime"];
                    if (dTable.Rows[0]["MonthWithoutTrade"].Equals(DBNull.Value))
                        _monthWithoutTrade = 0;
                    else
                        _monthWithoutTrade = (int)dTable.Rows[0]["MonthWithoutTrade"];
                    if (dTable.Rows[0]["BudgetYear"].Equals(DBNull.Value))
                        _budgetYear = 0;
                    else
                        _budgetYear = (int)dTable.Rows[0]["BudgetYear"];
                    if (dTable.Rows[0]["IncludeCriteria"].Equals(DBNull.Value))
                        _includeCriteria = false;
                    else
                        _includeCriteria = (bool)dTable.Rows[0]["IncludeCriteria"];
                    if (dTable.Rows[0]["IncludePayment"].Equals(DBNull.Value))
                        _includePayment = false;
                    else
                        _includePayment = (bool)dTable.Rows[0]["IncludePayment"];

                   
                    if (dTable.Rows[0]["NumberOfMangerAllowed"].Equals(DBNull.Value))
                        _numberOfMangerAllowed = 0;
                    else
                        _numberOfMangerAllowed = (int)dTable.Rows[0]["NumberOfMangerAllowed"];


                }

                return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }


        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_GetRecords]";
            command.CommandType = CommandType.StoredProcedure;
            DataTable dTable = new DataTable("tblGeneralSetting");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }
        public bool Insert()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_Insert]";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RenewalDuration", _renewalDuration));
                command.Parameters.Add(new SqlParameter("@TempRecognitionDuration", _tempRecognitionDuration));
                command.Parameters.Add(new SqlParameter("@RenewalDurationUnit", _renewalDurationUnit));
                command.Parameters.Add(new SqlParameter("@TempRecognitionDurationUnit", _tempRecognitionDurationUnit));
                command.Parameters.Add(new SqlParameter("@TradeStartTime", _tradeStartTime));
                command.Parameters.Add(new SqlParameter("@TradeEndTime", _tradeEndTime));
                command.Parameters.Add(new SqlParameter("@MonthWithoutTrade", _monthWithoutTrade));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Update()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_Update]";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RenewalDuration", _renewalDuration));
                command.Parameters.Add(new SqlParameter("@TempRecognitionDuration", _tempRecognitionDuration));
                command.Parameters.Add(new SqlParameter("@RenewalDurationUnit", _renewalDurationUnit));
                command.Parameters.Add(new SqlParameter("@TempRecognitionDurationUnit", _tempRecognitionDurationUnit));
                command.Parameters.Add(new SqlParameter("@TradeStartTime", _tradeStartTime));
                command.Parameters.Add(new SqlParameter("@TradeEndTime", _tradeEndTime));
                command.Parameters.Add(new SqlParameter("@MonthWithoutTrade", _monthWithoutTrade));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Delete()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_Delete]";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<CGeneralSettings> GetList()
        {
            List<CGeneralSettings> RecordsList = new List<CGeneralSettings>();
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlDataReader dr = null;
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_GetRecords]";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CGeneralSettings objCtblGeneralSetting = new CGeneralSettings();
                    if (dr["RenewalDuration"].Equals(DBNull.Value))
                        objCtblGeneralSetting.RenewalDuration = 0;
                    else
                        objCtblGeneralSetting.RenewalDuration = (double)dr["RenewalDuration"];
                    if (dr["TempRecognitionDuration"].Equals(DBNull.Value))
                        objCtblGeneralSetting.TempRecognitionDuration = 0;
                    else
                        objCtblGeneralSetting.TempRecognitionDuration = (double)dr["TempRecognitionDuration"];
                    if (dr["RenewalDurationUnit"].Equals(DBNull.Value))
                        objCtblGeneralSetting.RenewalDurationUnit = 0;
                    else
                        objCtblGeneralSetting.RenewalDurationUnit = (int)dr["RenewalDurationUnit"];
                    if (dr["TempRecognitionDurationUnit"].Equals(DBNull.Value))
                        objCtblGeneralSetting.TempRecognitionDurationUnit = 0;
                    else
                        objCtblGeneralSetting.TempRecognitionDurationUnit = (int)dr["TempRecognitionDurationUnit"];
                    if (dr["TradeStartTime"].Equals(DBNull.Value))
                        objCtblGeneralSetting.TradeStartTime = DateTime.MinValue;
                    else
                        objCtblGeneralSetting.TradeStartTime = (DateTime)dr["TradeStartTime"];
                    if (dr["TradeEndTime"].Equals(DBNull.Value))
                        objCtblGeneralSetting.TradeEndTime = DateTime.MinValue;
                    else
                        objCtblGeneralSetting.TradeEndTime = (DateTime)dr["TradeEndTime"];
                    if (dr["MonthWithoutTrade"].Equals(DBNull.Value))
                        objCtblGeneralSetting.MonthWithoutTrade = 0;
                    else
                        objCtblGeneralSetting.MonthWithoutTrade = (int)dr["MonthWithoutTrade"];
                    RecordsList.Add(objCtblGeneralSetting);
                }

                return RecordsList;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
        }
        public bool UpdateRenewalSetting()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblGeneralSetting_UpdateRenewalSettings]";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RenewalDuration", _renewalDuration));
                command.Parameters.Add(new SqlParameter("@BudgetYear", _budgetYear));
                command.Parameters.Add(new SqlParameter("@IncludeCriteria", _includeCriteria));
                command.Parameters.Add(new SqlParameter("@IncludePayment", _includePayment));
                command.Parameters.Add(new SqlParameter("@NumberOfMangerAllowed", _numberOfMangerAllowed));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblGeneralSetting::UpdateRenewalSetting::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

    }
}