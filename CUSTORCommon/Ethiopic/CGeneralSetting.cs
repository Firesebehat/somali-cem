﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CUSTOR;
using CUSTOR.Taxpayer;

/// <summary>
/// Summary description for CGeneralSetting
/// </summary>
namespace CUSTORCommon.Ethiopic    
{
    public static class CGeneralSetting
    {
        public static double RenewalDuration = 0;
        public static double TempRecognitionDuration = 0;
        public static int RenewalDurationUnit = 0;
        public static int TempRecognitionDurationUnit = 0;
        public static int MonthWithoutTrade = 0;
        public static DateTime TradeStartTime = DateTime.Parse("1900-01-01 2:30:00.000");
        public static DateTime TradeEndTime = DateTime.Parse("1900-01-01 11:30:00.000");
        public static int RenewalDayStartsFrom = 0;
        public static int RenewalMonthStartsFrom = 0;
        public static double RenewalMonthDuration = 0;
        public static int BudgetYear = 0;
        public static int RDayTo = 0;
        public static int RMonthTo = 0;
        public static bool IncludeCriteria = false;
        public static bool IncludePayment = false;



        public static void ReadSetting()
        {
            DBHelper dataAccess = new DBHelper();
            DataTable table = new DataTable();
            string strSQL = "";

            strSQL = "SELECT * FROM tblGeneralSetting";

            table = dataAccess.GetRecord(strSQL);

            if (table.Rows.Count > 0)
            {

                RenewalDuration = (double)table.Rows[0]["RenewalDuration"];
                TempRecognitionDuration = (double)table.Rows[0]["TempRecognitionDuration"];
                RenewalDurationUnit = (int)table.Rows[0]["RenewalDurationUnit"];
                TempRecognitionDurationUnit = (int)table.Rows[0]["TempRecognitionDurationUnit"];
                TradeStartTime = (DateTime)table.Rows[0]["TradeStartTime"];//kinfe
                TradeEndTime = (DateTime)table.Rows[0]["TradeEndTime"];//kinfe
                MonthWithoutTrade = (int)table.Rows[0]["MonthWithoutTrade"];
                RenewalDayStartsFrom = (int)table.Rows[0]["RenewalDayFrom"];
                RenewalMonthStartsFrom = (int)table.Rows[0]["RenewalMonthFrom"];
                RenewalMonthDuration = (double)table.Rows[0]["RenewalMonthDuration"];
                BudgetYear = (int)table.Rows[0]["BudgetYear"];
                RDayTo = (int)table.Rows[0]["RenewalDayTo"];
                RMonthTo = (int)table.Rows[0]["RenewalMonthTo"];
                if (table.Rows[0]["IncludeCriteria"].Equals(DBNull.Value))
                    IncludeCriteria = true;
                else
                    IncludeCriteria = (bool)table.Rows[0]["IncludeCriteria"];
                if (table.Rows[0]["IncludePayment"].Equals(DBNull.Value))
                    IncludePayment = true;
                else
                    IncludePayment = (bool)table.Rows[0]["IncludePayment"];
            }
        }

        public static void SaveSetting(double paramRenewalDuration, double paramTempRecognitionDuration, int paramRenewalDurationUnit, double paramTempRecognitionDurationUnit, double paramMonthWithoutTrade)
        {
            CGeneralSettings setting = new CGeneralSettings();

            setting.MonthWithoutTrade = int.Parse(paramMonthWithoutTrade.ToString());
            setting.RenewalDuration = paramRenewalDuration;
            setting.RenewalDurationUnit = paramRenewalDurationUnit;
            setting.TempRecognitionDuration = paramTempRecognitionDuration;
            setting.TempRecognitionDurationUnit = int.Parse(paramTempRecognitionDurationUnit.ToString());
            setting.TradeStartTime = TradeStartTime;
            setting.TradeEndTime = TradeEndTime;

            setting.Update();
        }
    }
}
