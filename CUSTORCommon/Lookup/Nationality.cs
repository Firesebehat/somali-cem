﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTORCommon.Enumerations;

namespace CUSTORCommon.Address
{
    public class Nationality 
    {
        public string NationalityCode { get; set; }

        public string NationalityName { get; set; } 


        private static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString; }
        }

        public List<Nationality> GetNationalities(int lang)   
        {
            string strSQL = "SELECT code,description, amDescription from Nationality";
            List<Nationality> Nationlities = new List<Nationality>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Nationality objNationality;
                            if (dataReader != null)
                            {
                                while (dataReader.Read())
                                {
                                    objNationality = new Nationality();
                                    objNationality.NationalityCode = Convert.ToString(dataReader["code"]);

                                    if (lang == Convert.ToInt32(Language.eLanguage.eEnglish))
                                    {
                                        objNationality.NationalityName = Convert.ToString(dataReader["description"]);//Tigrigna
                                    }
                                    else if(lang == Convert.ToInt32(Language.eLanguage.eAmharic))
                                    {
                                        objNationality.NationalityName = Convert.ToString(dataReader["amDescription"]);
                                    }
                                    Nationlities.Add(objNationality);
                                }
                            }
                        }
                    }
                }
                return Nationlities; 
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetNationality: " + ex.Message);

            }
        }


    }
}


