﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTORCommon.Enumerations;
using System.Configuration;


namespace CUSTORCommon.Lookup
{
    public class Lookup
    {
        public string value { get; set; }

        public string Description { get; set; }

        public string Parent { get; set; }

    }


    public class LookupDataAccess
    {
        private static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString; }
        }

        private string GetDescriptionField(Language.eLanguage eLang)
        {
            switch (eLang)
            {
                case Language.eLanguage.eAmharic:
                    return "amdescription";
                case Language.eLanguage.eEnglish:
                    return "description";
                //case Language.eLanguage.eTigrigna:
                //    return "Tigrigna";
                //case Language.eLanguage.eAfanOromo:
                //    return "AfanOromo";
                //case Language.eLanguage.eAfar:
                //    return "Afar";
                //case Language.eLanguage.eSomali:
                //    return "Somali";
                default:
                    return "amdescription";
            }

        }

        public List<Lookup> GetLookups(int intLang, string Parent)
        {
            string strDesc = GetDescriptionField((Language.eLanguage)intLang);
            string strSQL = String.Format("SELECT Code,amdescription,description From Lookup WHERE Parent=@Parent ", strDesc);
            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        command.Parameters.AddWithValue("@Parent", Parent);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {

                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.value = Convert.ToString(dataReader["Code"]);
                                    Lookup.Description = Convert.ToString(dataReader[strDesc]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }

        public List<Lookup> GetLookups(int intLang)
        {
            string strDesc = GetDescriptionField((Language.eLanguage)intLang);
            string strSQL = String.Format("SELECT {0}, Code From Lookup ", strDesc);


            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        //command.Parameters.AddWithValue("@LookupName", LookupName);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {

                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.value = Convert.ToString(dataReader["Code"]);
                                    Lookup.Description = Convert.ToString(dataReader[strDesc]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }

        public List<Lookup> GetLookups(string SearchPhrase)
        {
            if (SearchPhrase.Trim() == string.Empty) return null;
            string strSQL = @"SELECT AmDescription, Code From Lookup WHERE Parent='89' AND AmDescriptionS LIKE N'" +
                            SearchPhrase + "%'";

            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            command.Parameters.AddWithValue("@SearchPhrase", SearchPhrase);
                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.Description = Convert.ToString(dataReader["Code"]);
                                    Lookup.value = Convert.ToString(dataReader["AmDescription"]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                //return null;
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }
      

        public List<Lookup> GetLookups()
        {
            string strSQL = "SELECT AmDescription, Code From Lookup WHERE Parent='89' ";
            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.Description = Convert.ToString(dataReader["Code"]);
                                    Lookup.value = Convert.ToString(dataReader["AmDescription"]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                //return null;
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }
        
        public List<Lookup> GetCancllationTypes()   
        {
            string strSQL = "SELECT [parent], Code, description, amDescription,Tigrigna,AfanOromo,Afar,Somali,Arabic FROM Lookup WHERE Parent='0009' ORDER BY Code desc";
            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.Description = Convert.ToString(dataReader["Code"]);
                                    Lookup.value = Convert.ToString(dataReader["amDescription"]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                //return null;
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }

        public List<Lookup> GetCancllationResons()  
        {
            string strSQL = "SELECT [parent], Code, description, amDescription,Tigrigna,AfanOromo,Afar,Somali,Arabic FROM Lookup where parent = '95'ORDER BY Code";
            List<Lookup> Lookups = new List<Lookup>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (
                        SqlCommand command = new SqlCommand()
                        {
                            Connection = connection,
                            CommandText = strSQL,
                            CommandType = CommandType.Text
                        })
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Lookup Lookup;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Lookup = new Lookup();
                                    Lookup.Description = Convert.ToString(dataReader["Code"]);
                                    Lookup.value = Convert.ToString(dataReader["amDescription"]);
                                    Lookups.Add(Lookup);
                                }
                        }
                    }
                }
                return Lookups;
            }
            catch (Exception ex)
            {
                //return null;
                throw new Exception("Error in GetLookups: " + ex.Message);

            }
        }

    }
}
