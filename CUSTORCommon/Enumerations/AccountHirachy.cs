﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
    public static class AccountHirachy
    {
        public enum Hirachy
        {
            [Description("Main|ዋና|Main|Main|Main|Main")]
            main = 1,
            [Description("Sub|ንዑስ|Sub|Sub|Sub|Sub")]
            sub = 2,
            [Description("Sub of Sub|የንዑስ ንዑስ|Sub of Sub|Sub of Sub|Sub of Sub|Sub of Sub")]
            subsub = 3,
        }
    }
}
