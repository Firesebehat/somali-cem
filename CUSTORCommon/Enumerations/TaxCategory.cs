﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
   public enum TaxCategory
    {
        [Description("Corporate Taxes|ኮርፖሬት ግብር")]
        eCorporateTax = 1,
        [Description("Municipality Taxes|የማዘጋጃ ግብር")]
        eMunicipality = 2,
        [Description("Other Taxes|ሌሎች የግብር ዓይነቶች")]
        eOtherTaxes = 3,
    }
}
