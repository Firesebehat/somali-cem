﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
    public static class PaymentType
    {
        public enum ePaymentType
        {
            [Description("Tax|ፍሬ ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eTax = 0,
            [Description("Fine-Disclose Archive|ቅጣት-መዝገብ ባለመያዝ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eFineDiscloseArchive = 1,

            [Description("Fine-Delay Payment|ቅጣት-በግዜ ባለመክፈል|Kan Motuuma|እንግሊዝኛ|English|English")]
            eFineDelayPayment = 2,

            [Description("Fine-Disclose Revenue|ቅጣት-ገቢን ባለማሳወቅ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eFineDiscloseRevenue = 3,

            [Description("Fine-Under Statement|ቅጣት-አሳንሶ በማስታወቅ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eFineUnderStatement = 4,

            [Description("Interest|ወለድ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eInterest = 5,



        }
    }
}