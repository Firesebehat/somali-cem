﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
  public static class  Language
    {
        public enum eLanguage
        {
            [Description("English|እንግሊዝኛ|English|እንግሊዝኛ|English|English")]
            eEnglish,
            [Description("Amharic|አማርኛ|Amharic|ኣምሐርኛ|Amharic|Amharic")]
            eAmharic,
            //[Description("Afan Oromo|ኦሮምኛ|Afan Oromo|ኦሮምኛ|Afan Oromo|Afan Oromo")]
            //eAfanOromo,
            //[Description("Tigrigna|ትግርኛ|Tigrigna|ትግርኛ|Tigrigna|Tigrigna")]
            //eTigrigna,
            //[Description("Afarf|አፋርኛ|Afarf|አፋርኛ|Afarf|Afarf")]
            //eAfar,
            //[Description("Somali|ሶማሊ|Somali|ሶማሊ|Somali|Somali")]
            //eSomali
        }
    }
}
