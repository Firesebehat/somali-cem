﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
 
namespace CUSTORCommon.Enumerations
{
   public  class EnumHelper
    {
        public static object GetEnumDescriptions(Type enumType)
        {
            var list = new List<KeyValuePair<int, string>>();
            foreach (Enum value in Enum.GetValues(enumType))
            {
                string v = string.Empty;
                string description = value.ToString();
                FieldInfo fieldInfo = value.GetType().GetField(description);

                var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).First();
                if (attribute != null)
                {
                    description = (attribute as DescriptionAttribute).Description;
                    //v = fieldInfo.GetValue(value).ToString();
                }
                string s = description.Split('|')[1];
                list.Add(new KeyValuePair<int, string>(Convert.ToInt32(value), s));

            }
            return list;
        }

        public static object GetEnumDescriptions(Type enumType, Language.eLanguage Lang)
        {
            var list = new List<KeyValuePair<int, string>>();
            foreach (Enum value in Enum.GetValues(enumType))
            {
                string v = string.Empty;
                string description = value.ToString();
                FieldInfo fieldInfo = value.GetType().GetField(description);

                var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).First();
                if (attribute != null)
                {
                    description = (attribute as DescriptionAttribute).Description;

                }
                string s = description.Split('|')[(int)Lang];
                list.Add(new KeyValuePair<int, string>(Convert.ToInt32(value), s));

            }
            return list;
        }

        public static string GetEnumDescription(Type enumType, Language.eLanguage Lang, int intValue)
        {
            string v = string.Empty;
            foreach (Enum value in Enum.GetValues(enumType))
            {

                if (intValue == Convert.ToInt32(value))
                {
                    string description = value.ToString();
                    FieldInfo fieldInfo = value.GetType().GetField(description);

                    var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).First();
                    if (attribute != null)
                    {
                        description = (attribute as DescriptionAttribute).Description;

                    }
                    string s = description.Split('|')[(int)Lang];
                    return s;
                }
            }
            return string.Empty;
        }
        public static Hashtable GetEnumForBind(Type enumeration)
        {
            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);
            Hashtable ht = new Hashtable();
            for (int i = 0; i < names.Length; i++)
            {
                string temp = names[i];

                ht.Add(Convert.ToInt32((int)values.GetValue(i)).ToString(), temp);
            }
            return ht;
        }
    }
}
