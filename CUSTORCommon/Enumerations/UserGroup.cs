﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
    public static class UserGroup
    {
        public enum UserGroupTypes
        {
            [Description("Work Process|የሥራ ሂደት|Work Process|Work Process|Work Process")]
            WorkProcess = 1,
            [Description("Revenue Follow up|ገቢ ክትትል|Revenue Follow up|Revenue Follow up|Revenue Follow up")]
            RevenueFollowup = 2,
            [Description("Customer Service|የደነበኛ አገልግሎት|Customer Service|Customer Service|Customer Service")]
            CustomerService = 3,
            [Description("Unknown|አይታወቅም|Unknown|Unknown|Unknown")]
            Unknown = 0
        }
    }
}
