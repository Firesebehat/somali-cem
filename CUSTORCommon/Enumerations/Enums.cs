﻿using System.ComponentModel;

/// <summary>
/// Summary description for CustomerType
/// </summary>
public class Enums
{
    public enum eCustomerType
    {
        [Description("Individual|ግለሰብ|IndividualOromo|ግለሰብTig|IndividualAfar|IndividualSomali")]
        Individual = 0,
        [Description("Organization|የንግድ ማህበር|OrganizationOromo|OrganizationTig|OrganizationAfar|OrganizationSomali")]
        Organization = 1
    }

    public enum eAssessedUsing
    {
        [Description("Daily Revenue|የቀን ገቢ|DailyRevenueOromo|የቀን ገቢTig|DailyRevenueAfar|DailyRevenueSomali")]
        DailyRevenue,
        [Description("Income Statement|የሂሳብ መዘገብ|Income StatementOromo|Income StatementTig|Income StatementAfar|Income StatementSomali")]
        IncomeStatement,
        [Description("Data|በመረጃ|DataOromo|DataTig|DataAfar|DataSomali")]
        Data
    }
    public enum eLegalStatus
    {
        [Description("Individual|ግለሰብ ነጋዴ")]
        Individual,
        [Description("Private Limited Company|ሃላፊነቱ የተወሰነ የግል ማህበር")]
        PrivateLimitedCompany,
        [Description("Share Company|የአክስዮን ማህበር")]
        SahreCompany,
        [Description("Public EnterPrise|መንግስታዊ የልማት ድርጅት")]
        PublicEnterPrise,
        [Description("Paretnership|የህብረት ሽርክና ማህበር ")]
        Paretnership,
        [Description("Cooperatives Association|የህብረት ስራ ማህበር")]
        CooperativesAssociation,
        [Description("Individual Enterprise|የግል ኢንተርፕራይዝ")]
        IndividualEnterprise,
        [Description("Other|ሌላ")]
        Other
    }
    public enum eGrade
    {
        [Description("A|ሀ|AOromo|ሀ|AAfar|ASomali")]
        A = 0,
        [Description("B|ለ|BOromo|ለ|BAfar|BSomali")]
        B = 1,
        [Description("C|ሐ|COromo|ሐ|CAfar|CSomali")]
        C = 2,
        [Description("NA|አይታወቅም|NAOromo|አይታወቅም|NAAfar|NASomali")]
        NA = 3
    }

    public enum eTaxPayerStatus
    {
        [Description("Active|በስራ ላይ")]
        Active = 0,
        [Description("Cancelled|ተሰርዟል")]
        Cancelled = 1,
        [Description("Injuncted|ታግዷል")]
        Injuncted = 2,
        [Description("Closed|ተዘግቷል")]
        Closed = 3,
        [Description("Transfer|ተዛውሯል")]
        Transfer = 4,
        [Description("Not Active|የማይንቀሳቀስ")]
        Trabsfer = 5

    }

    public enum eWorkingDays
    {
        [Description("210|210")]
        Twohandredten = 210,
        [Description("300|300")]
        Threehandred = 300,
        [Description("365|365")]
        ThreeHandredSixtyFive = 365
    }
    public enum eTaAccountStatus
    {
        [Description("Active|በስራ ላይ")]
        Active = 0,
        [Description("Cancelled|ተሰርዟል")]
        Cancelled = 1,
        [Description("Injuncted|ታግዷል")]
        Injuncted = 2,
        [Description("Closed|ተዘግቷል")]
        Closed = 3,
        [Description("Not Active|የማይንቀሳቀስ")]
        Trabsfer = 4
    }

    public enum eAddressType
    {
        [Description("TaxPayer|ግብር ከፋይ")]
        Customer = 0,
        [Description("Advertisement|ማስታወቅያ")]
        Advertisement = 1,
        [Description("Business|ድርጅት")]
        Business = 2
    }
    public enum eAdminSettingTypes
    {
        EnableChangingAssessmentDate=1,
    }
    public enum eSiteLevel
    {
        [Description("Head Office|ዋና መ/ቤት")]
        large = 1
        //    ,
        //[Description("Medium|መካከለኛ")]
        //Medium = 2,
        //[Description("Small|አነስተኛ")]
        //Small = 3,
        //[Description("Micro|ጥቃቅን")]
        //Micro = 4
    }
}
