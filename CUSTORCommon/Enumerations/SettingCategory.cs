﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
    public class SettingCategory
    {
        public enum eSettingCategory
        {
            Agriculture=1,
            Rental,
            Penality,
            Salary,
            LandUse
        }
    }
}
