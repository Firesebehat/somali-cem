﻿using System.ComponentModel;

namespace CUSTORCommon.Enumerations
{
    public static class OrganizationalType
    {
        public  enum OrgType
        {
            //[Description("Federal Revenue Authority|የፌደራል ገቢዎች  ባለስልጣን")]
            //Federal = 0,
            [Description("Somali Region Investment and Dispora Beruea|የሶማሌ ክልላዊ መንግስት ኢንቨስትመንት እና ዲያስፖራ ቢሮ")]
            Regional = 0
            //[Description("Sub-cities, LTOs and Medium Tax Centers| የክፍለ ከተማ :የከፍተኛና መካከለኛ ግብር መሰብሰቢያ ማዕከል")]
            //KifleKetema = 1,
            //[Description("Woredas Micro Tax Centers|የወረዳዎች ጥቃቅን ግብር መሰብሰቢያ ማዕከል")]
            //Woreda=2
        }
    }
}
