﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTORCommon.Enumerations
{
    public static class TaxType
    {
        public enum eTaxType
        {
            [Description("Personal Income Tax|የደመወዝ ገቢ ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            ePersonalIncomeTax = 0,
            [Description("Excise Tax|ኤክሳይዝ ታክስ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eExciseTax = 1,

            [Description("Profit Tax|የንግድ ስራ ገቢ ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eProfitTax = 2,

            [Description("Turn Over Tax|የተርን ኦቨር/ሽያጭ ታክስ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eTurnOverTax = 3,

            [Description("Licence Registration Tax|የንግድ ፈቃድ/አገልግሎት ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eLicenceRegistrationTax = 4,

            [Description("Rental Tax|የኪራይ ገቢ ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eRentalTax = 5,

            [Description("Capital Gain Tax|ካፒታል ጌይን ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eCapitalGainTax = 6,

            [Description("Advertisement Tax|የውጪ ማስተዋወቅ ክፍያ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eAdvertisementTax = 7,

            [Description("Agriculture Tax|የእርሻ ስራ ገቢ ግብር|Kan Motuuma|እንግሊዝኛ|English|English")]
            eAgricultureTax = 8,

            [Description("Stamp Duty|የቴምብር ቀረጥ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eStampDuty = 9,
            [Description("Attorney Book Payment|የደብተር ክፍያ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eAttorneyBookPayment = 10,
            [Description("Sanitation Tax|የፅዳት አገልግሎት ግብር |Kan Motuuma|እንግሊዝኛ|English|English")]
            eSanitationTax = 11,
            [Description("Municipality|የፍቃድና ፅዳት አገልግሎት ክፍያ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eMunicipality = 12,
            [Description("Withholding|ዊዝሆልዲንግ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eWithholding = 13,
            [Description("Graduate Tax|የወቺ መጋራት ክፍያ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eGraduateTax = 14,
            [Description("VAT|የተጨማሪ አሴት ታክስ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eVAT = 15,
            [Description("Judicary Service|የዳኝነት|Kan Motuuma|እንግሊዝኛ|English|English")]
            eJudicaryService = 16,
            [Description("Rural Land Use|የገጠር መሬት መጠቀሚያ ክፍያ|Kan Motuuma|እንግሊዝኛ|English|English")]
            eRuralLandUse = 17,
            [Description("Loading and Unloading Goods|እቃ ማራገፍና መጫን|Kan Motuuma|እንግሊዝኛ|English|English")]
            eLoadingandUnloadingGoods = 18,

        }
    }
}