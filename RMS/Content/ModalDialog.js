﻿//customized 
$(document).ready(function () {
    var text_ = '<div class="modal fade" id="myModal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">' +
        '<h4 class="modal-title" style="color:#009688">ማረጋገጫ</h4><button type="button" class="close" data-dismiss="modal">&times;</button></div>' +
        '<div class="modal-body">እርግጠኛ ነዎት..</div><!-- Modal footer --><div class="modal-footer">' +
        '<button id="btnConfirmCancel" style="border-color:#009688;width:80px" type="button" class="btn btn-outline-secondary" data-dismiss="modal">ተው</button>' +
        ' <button id="btnConfirmOK" style="border-color:#009688;width:80px" type="button" class="btn btn-outline-danger" data-dismiss="modal">&nbsp;&nbsp;አዎ&nbsp;&nbsp;</button>' +
        '</div></div></div></div>';
    $('body').append(text_);
});
function ShowConfirmx(msg, onConfirm, culture, title, hideCancel) {
    $('#myModal').on('show.bs.modal',
        function () {
            $(this).find('.modal-body').text(msg);
            //if (typeof (onConfirm) !== 'undefined' && onConfirm !== null) {
            $('#btnConfirmOK').unbind().click(
                function () {
                    onConfirm(true);
                });
            $('#btnConfirmCancel').unbind().click(
                function () {
                    onConfirm(false);
                });
            //}
            if (typeof culture !== 'undefined' && culture !== null) {
                if (culture.toLowerCase() === 'en' ||
                    culture.toLowerCase() === 'english' ||
                    culture.toLowerCase() === 'en.us' ||
                    culture.toLowerCase() === 'en.uk') {
                    $('#btnConfirmOK').text(' OK ');
                    $('#btnConfirmCancel').text('Cancel');
                    if (typeof title === 'undefined' || title === null) {
                        $(this).find('.modal-title').text('Confirmation');
                    }
                }
            }
            if (hideCancel) {
                $('#btnConfirmCancel').hide();
            }
            if (typeof title !== 'undefined' && title !== null) {
                $(this).find('.modal-title').text(title);
            }
        });
    $('#myModal').modal('show');

}





