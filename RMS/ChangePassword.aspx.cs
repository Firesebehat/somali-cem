﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using CUSTOR.Common;
using DevExpress.Web;
using CUSTOR.DataAccess;
using CUSTOR.Business;
using CUSTOR.Domain;

namespace ENAPortal
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                if (Session["IsNewUser"] != null)
                {
                    bool IsNewUser = (bool)Session["IsNewUser"];
                    if (IsNewUser)
                        CurrentPasswordButtonEdit.Text = "Ch@ngeP@55w0rd";
                }
               
            }
        }

        protected void ChangePwd_ChangedPassword(object sender, EventArgs e)
        {
            Response.Redirect("~/Login");
        }
        protected void btnChange_Click(object sender, EventArgs e)
        {
            DoValidate();
        }

        private void DoValidate()
        {
            var strPassword = CurrentPasswordButtonEdit.Text;
            try
            {
                    if (CurrentPasswordButtonEdit.Text.Trim().Length == 0)
                    {
                        CMsgBar.ShowError("Current password is required", pnlMsg, lblError);
                        return;
                    }
                var strUserName = Context.User.Identity.Name;
                if (!Membership.ValidateUser(strUserName, strPassword))
                {
                    CMsgBar.ShowError("The password you entered is not correct", pnlMsg, lblError);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Response.Redirect("~/AccessDenied/AccessDenied",true);
                CMsgBar.ShowError(ex.Message, pnlMsg, lblError);
                return;
            }

            if (PasswordButtonEdit.Text.Trim().Length == 0)
            {
                CMsgBar.ShowError("Enter the new password", pnlMsg, lblError);
                return;
            }
            if (ConfirmPasswordButtonEdit.Text.Trim().Length == 0)
            {
                CMsgBar.ShowError("Please confirm the password", pnlMsg, lblError);
                return;
            }
            if (PasswordButtonEdit.Text.Trim() != ConfirmPasswordButtonEdit.Text.Trim())
            {
                CMsgBar.ShowError("The new password and confirmed password do not mutch.", pnlMsg, lblError);
                return;
            }

            PasswordSetting passwordSetting = CPasswordManager.GetPasswordSetting();
            StringBuilder sbPasswordRegx = new StringBuilder(string.Empty);

            //min and max
            sbPasswordRegx.Append(@"(?=^.{" + passwordSetting.MinLength + "," + passwordSetting.MaxLength + "}$)");

            //numbers length
            sbPasswordRegx.Append(@"(?=(?:.*?\d){" + passwordSetting.NumsLength + "})");

            //a-z characters
            sbPasswordRegx.Append(@"(?=.*[a-z])");

            //A-Z length
            sbPasswordRegx.Append(@"(?=(?:.*?[A-Z]){" + passwordSetting.UpperLength + "})");

            //special characters length
            sbPasswordRegx.Append(@"(?=(?:.*?[" + passwordSetting.SpecialChars + "]){" + passwordSetting.SpecialLength + "})");

            //(?!.*\s) - no spaces
            //[0-9a-zA-Z!@#$%*()_+^&] -- valid characters
            sbPasswordRegx.Append(@"(?!.*\s)[0-9a-zA-Z" + passwordSetting.SpecialChars + "]*$");

            if (!Regex.IsMatch(PasswordButtonEdit.Text, sbPasswordRegx.ToString()))
            {
                CMsgBar.ShowError("The password you entered do not match our password policy", pnlMsg, lblError);
                return;
            }

                string user = User.Identity.Name;
            MembershipUser usrInfo = Membership.GetUser(user);
            usrInfo.IsApproved = true;
            try
            {
                if (usrInfo.ChangePassword(strPassword, PasswordButtonEdit.Text.Trim()))
                {
                    Membership.UpdateUser(usrInfo);//

                    //Hash user information
                    UserDataAccess objUser = new UserDataAccess();
                    HashInfo objHash = new HashInfo();
                    User users = new User();
                    users = objUser.GetUserHashCode(user);
                    string UserPlainText = user + PasswordButtonEdit.Text.Trim() + users.TaxCenterId;
                    var roles = Roles.GetRolesForUser(user);
                    foreach (string role in roles)
                    {
                        UserPlainText = UserPlainText + role;
                    }
                    string CyperText = objHash.ComputeHash(UserPlainText);
                    users.UserHash = CyperText;
                    users.UserName = user;
                    objUser.UpdateHash(users);
                    // hash end 

                    CMsgBar.ShowSucess("Password is successfully changed.", pnlMsg, lblError);
                    pnlSuccess.Visible = true;
                    ASPxPanel1.Visible = false;
                }
                else
                {
                    CMsgBar.ShowError("Password changing task failed", pnlMsg, lblError);
                }
            }
            catch(Exception ex)
            {
                CMsgBar.ShowError(ex.Message, pnlMsg, lblError);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        

        protected void btnContinue_Click1(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectFromLoginPage(User.Identity.Name,false);
        }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectFromLoginPage(User.Identity.Name, false);

        }
    }
}