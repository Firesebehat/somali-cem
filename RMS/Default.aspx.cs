﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Administrator")
                 || HttpContext.Current.User.IsInRole("Super Admin")))
            {
                Response.Redirect("~/AccessDenied");
            }

            lblName.Text = string.Format("Welcome {0}", User.Identity.Name);
            lblOrg.Text = string.Format("Site: {0}", CurrentProfile.ActiveUserProfile.NameEng);
            lblFullname.Text = string.Format("Full Name: {0}", CurrentProfile.ActiveUserProfile.FullName);
            lblPasswordExpiry.Text = string.Format(@"Your password will expire after  <strong>{0}</strong> days", GetRemainingDays());
        }
        private DateTime GetLastPasswordChangeDate()
        {
            MembershipUser usr = Membership.GetUser(User.Identity.Name);
            return usr.LastPasswordChangedDate;
        }

        private int GetRemainingDays()
        {
            int intExpiryDays;
            intExpiryDays = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpiryDuration"]);

            DateTime dt = GetLastPasswordChangeDate();
            int dblDays = (DateTime.Today.Date - dt.Date).Days;
            int i = intExpiryDays - dblDays;
            return intExpiryDays - dblDays;
        }


    }
}