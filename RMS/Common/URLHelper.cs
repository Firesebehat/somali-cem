﻿using System;
using System.Configuration;
using System.Text;
using System.Web;
////using System.Linq;

namespace CUSTOR.Common
{
    public static class URLHelper
    {

        public static string GetUrl()
        {
            string strURL = ConfigurationManager.AppSettings["AppURL"];
            if (strURL.EndsWith("/") || strURL.EndsWith(@"\"))
                return strURL;
            return strURL + "/";
        }
        public static string GetUrl2(string url)
        {
            string strURL = ConfigurationManager.AppSettings["AppURL"];
            if (strURL.EndsWith("/") || strURL.EndsWith(@"\"))
                return strURL + "" + url;
            return strURL + "/" + url;
        }
        public static string GetUrl(string url)
        {


            string orginalUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (HttpContext.Current.Request.Url.Query.Length > 0)
                orginalUrl = orginalUrl.Replace(HttpContext.Current.Request.Url.Query, string.Empty);

            return orginalUrl.Replace(HttpContext.Current.Request.Url.AbsolutePath, string.Empty) +
              ((HttpContext.Current.Request.ApplicationPath == "/" ?
              "" : HttpContext.Current.Request.ApplicationPath)) + '/' + url;


        }
        public static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            sbUrl.Append('/');
                            foundSlash = true;
                            continue;
                        }
                        if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }
    }
}