﻿using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public static class CMsgBar
{

    public static void ShowSucess(string strText, HtmlGenericControl div,Label lblMsg)
    {
        if (strText.Length > 0)
        {
            div.Style.Remove("display");
            div.Style.Add("display", "block");
            lblMsg.Text = strText.Replace("\n", "<br />");
            div.Attributes["class"] = "alert alert-success alert - dismissable";
           // div.Visible = true;
        }


    }
    public static void ShowError(string strText, HtmlGenericControl div, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            div.Style.Remove("display");
            div.Style.Add("display", "block");
            lblMsg.Text = strText.Replace("\n", "<br />");
            div.Attributes["class"] = "alert alert-danger alert-dismissable";
            //div.Visible = true;
        }

    }
    public static void ShowWarning(string strText, HtmlGenericControl div, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            div.Style.Remove("display");
            div.Style.Add("display", "block");
            lblMsg.Text = strText.Replace("\n", "<br />");
            div.Visible = true;
            div.Attributes["class"] = "alert alert-warning alert-dismissable";
        }

    }
    public static void ShowInfo(string strText, HtmlGenericControl div, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            div.Style.Remove("display");
            div.Style.Add("display", "block");
            div.Visible = true;
            lblMsg.Text = strText.Replace("\n", "<br />");
            div.Attributes["class"] = "alert alert-info alert-dismissable";
        }
    }
    public static void ShowSucess(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-success";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;

    }
    public static void ShowError(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-danger";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }
    public static void ShowWarning(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-warnining";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }
    public static void ShowInfo(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-info";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }
}
