﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Lookup;
using DevExpress.Web;

namespace CUSTOR.Common
{
    public static class CDropdown
    {
        public static void FillComboFromLookUpDB(DropDownList cboName, string strSQL, string strTextField, string strValueField)
        {

            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(DropDownList cboName, string strSQL, string strTextField, string strValueField)
        {

            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillCheckBoxListFromDB(CheckBoxList chkblName, string strSQL, string strTextField, string strValueField)
        {
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        chkblName.Items.Clear();

                        chkblName.DataTextField = strTextField;
                        chkblName.DataValueField = strValueField;
                        chkblName.DataSource = dr;
                        chkblName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillCheckBoxListFromDB(DevExpress.Web.ASPxCheckBoxList chkblName, string strSQL, string strTextField, string strValueField)
        {
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        chkblName.Items.Clear();

                        chkblName.TextField = strTextField;
                        chkblName.ValueField = strValueField;
                        chkblName.DataSource = dr;
                        chkblName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void AddComboItem(DropDownList cboLang, string strText, int Lang)
        {
            ListItem li = new ListItem(strText, Convert.ToString(Lang));
            cboLang.Items.Add(li);
        }
        public static int GetComboIndex(DropDownList cbo, string paramStringValue, int paramIntValue)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (cbo.Items[i].Value.ToLower() == paramStringValue.ToLower())
                    {
                        return i;
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public static int GetComboIndex(DropDownList cbo, string paramStringValue, int paramIntValue, bool isGuid)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (isGuid)
                    {
                        if (cbo.Items[i].Value.ToLower() == paramStringValue.ToLower())
                        {
                            return i;
                        }
                    }
                    else
                    {
                        if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                        {
                            return i;
                        }
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public static int GetComboIndex(DropDownList cbo, string paramStringValue, double paramIntValue)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                    {
                        return i;
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static void FillComboBox(DropDownList cboName, string strSQL, string strTextField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;
                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();
                        cboName.DataTextField = strTextField;
                        //cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(DropDownList cboName, string strSQL, string strTextField, string strValueField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        public static void FillComboFromDB(ASPxComboBox cboName, string strSQL, string strTextField, string strValueField)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.TextField = strTextField;
                        cboName.ValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(ASPxComboBox cboName, string strSQL, string strTextField, string strValueField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.TextField = strTextField;
                        cboName.ValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }


        public static void FillComboFromLookup(ASPxComboBox cboName, string strParent, Language.eLanguage eLang)
        {
            LookupDataAccess objLookup = new LookupDataAccess();
            List<Lookup> lstLookup;
            string CacheKey = "Lookup_" + strParent + "_" + Convert.ToString(eLang);
            if (!CacheHelper.Get(CacheKey, out lstLookup))
            {
                lstLookup = objLookup.GetLookups((int)eLang, strParent);
                CacheHelper.Add(lstLookup, CacheKey);
            }
            cboName.DataSource = lstLookup;
            cboName.TextField = "Description";
            cboName.ValueField = "value";
            cboName.DataBind();
            if(cboName.Items.Count > 0)
            {
                cboName.Items.Insert(0, new ListEditItem("ይምረጡ", "-1"));
                cboName.SelectedIndex = 0;
            }

            //cboName.Items.Insert(0, new ListEditItem("-", "-1"));

        }

        public static void FillComboFromLookup(DropDownList cboName, string strParent, Language.eLanguage eLang)
        {
            LookupDataAccess objLookup = new LookupDataAccess();

            cboName.DataSource = objLookup.GetLookups((int)eLang, strParent);
            cboName.DataTextField = "Description";
            cboName.DataValueField = "value";
            cboName.DataBind();
            //cboName.Items.Insert(0, new ListItem("-", "-1"));
        }

        public  static void FillComboFromEnum(ASPxComboBox cboName, Language.eLanguage eLang, Type t)
        {

            cboName.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            cboName.TextField = "Value";
            cboName.ValueField = "Key";
            cboName.DataBind();
            //if (cboName.Items.Count > 0)
            //{
            //   cboName.Items.Insert(0, new ListEditItem("ይምረጡ", "-1"));
            //    cboName.SelectedIndex = 0;
            //}
        }

        public static void FillComboFromEnum(ASPxRadioButtonList cboName, Language.eLanguage eLang, Type t)
        {

            cboName.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            cboName.TextField = "Value";
            cboName.ValueField = "Key";
            cboName.DataBind();
        }


    }
}