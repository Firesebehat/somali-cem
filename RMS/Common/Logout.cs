﻿using System;
using System.Web;
using System.Web.Security;

namespace CUSTOR.Common
{
    public static   class CLogout
    {
        public static void DoLogout()
        {
            /* Create new session ticket that expires immediately */
            FormsAuthenticationTicket ticket =
                new FormsAuthenticationTicket(
                    1,
                    HttpContext.Current.User.Identity.Name,
                    DateTime.Now,
                    DateTime.Now,
                    false,
                    Guid.NewGuid().ToString());

            /* Encrypt the ticket */
            string encrypted_ticket = FormsAuthentication.Encrypt(ticket);

            /* Create cookie */
            HttpCookie cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                encrypted_ticket);

            /* Add cookie */
            HttpContext.Current.Response.Cookies.Add(cookie);

            /* Abandon session object to destroy all session variables */
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();

        }
    }

}