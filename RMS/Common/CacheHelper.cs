﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Caching;

namespace CUSTOR.Common
{
    public static class CacheHelper
    {

        public static void Add<T>(T obj, string key)
        {

            //Remeber to declare CacheExpirationTime in web.config
            int ExpTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpirationTime"]);

            HttpContext.Current.Cache.Insert(key, obj, null, DateTime.UtcNow.AddMinutes(ExpTime), Cache.NoSlidingExpiration);
        }


        public static void Clear(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }

        public static bool Get<T>(string key, out T value)
        {
            try
            {
                if (!Exists(key))
                {
                    value = default(T);
                    return false;
                }

                value = (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }
    }
}