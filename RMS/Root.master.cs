using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using RMS.Model;
using DevExpress.Web;
using System.IO;
using DevExpress.Web.Demos;
using System.Web.Profile;
using System.Web.Security;
using System.Web;
using CUSTOR.Common;
//using DevExpress.Utils.IoC;

namespace RMS
{
    public partial class Root : MasterPage
    {
        public bool EnableBackButton { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                BindTree();
            }

            if (!string.IsNullOrEmpty(Page.Header.Title))
                Page.Header.Title += " - ";
            Page.Header.Title = Page.Header.Title + "SIRM";

            Page.Header.DataBind();
            //UpdateUserMenuItemsVisible();
            HideUnusedContent();
            UpdateUserInfo();
        }
        private void BindTree()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
            {
                Response.Redirect("~/AccessDenied");
            }
            //if (Page.User.IsInRole("super admin"))
            //{
            //    if (TableOfContentsTreeView != null)
            //    {
            //        TableOfContentsTreeView.DataBind();
            //        TableOfContentsTreeView.ExpandAll();
            //        SelectCurrentNode();
            //    }
            //}
            //else 
            //if ((Page.User.IsInRole("super admin") || HttpContext.Current.User.IsInRole("Administrator")))
            //{
            //    TableOfContentsTreeView.DataSourceID = null;
            //    TableOfContentsTreeView.DataSource = null;
            //    TableOfContentsTreeView.DataBind();
            //    TreeViewNode node = new TreeViewNode("Users List", "Users");
            //    TableOfContentsTreeView.Nodes.Add(node);
            //    TableOfContentsTreeView.SelectedNode = node;

            //    node = new TreeViewNode("Admin Site Settings", "AdminSettings");
            //    TableOfContentsTreeView.Nodes.Add(node);
            //    TableOfContentsTreeView.SelectedNode = node;
            //}
            //else
            //{
            //    Response.Redirect("~/AccessDenied");
            //}
        }

        private void SelectCurrentNode()
        {
            TreeViewNode node = TableOfContentsTreeView.Nodes.FindByName("MoreSettings");
            if (Session["SelectedNode"] != null)
            {
                var toBeSelectedNode = TableOfContentsTreeView.Nodes.FindByText(Session["SelectedNode"].ToString());
                TableOfContentsTreeView.SelectedNode = toBeSelectedNode;
                if (toBeSelectedNode != null)
                    if (toBeSelectedNode.Parent.Name == "MoreSettings" || toBeSelectedNode.Name == "MoreSettings")
                        node.Expanded = true;
                    else
                        node.Expanded = false;
            }
            else
                node.Expanded = false;
        }

        protected void HideUnusedContent()
        {
            LeftAreaMenu1.Items[1].Visible = EnableBackButton;

            bool hasLeftPanelContent = true;// HasContent(LeftPanelContent);
            LeftAreaMenu1.Items.FindByName("ToggleLeftPanel").Visible = hasLeftPanelContent;
            LeftPanel.Visible = hasLeftPanelContent;

            bool hasRightPanelContent = HasContent(RightPanelContent);
            RightAreaMenu.Items.FindByName("ToggleRightPanel").Visible = hasRightPanelContent;
            RightPanel.Visible = hasRightPanelContent;

            bool hasPageToolbar = HasContent(PageToolbar);
            PageToolbarPanel.Visible = hasPageToolbar;
        }

        protected bool HasContent(Control contentPlaceHolder)
        {
            if (contentPlaceHolder == null) return false;

            ControlCollection childControls = contentPlaceHolder.Controls;
            if (childControls.Count == 0) return false;

            return true;
        }

        // SignIn/Register

        protected void UpdateUserMenuItemsVisible()
        {
            var isAuthenticated = AuthHelper.IsAuthenticated();
            //RightAreaMenu.Items.FindByName("SignInItem").Visible = !isAuthenticated;
            //RightAreaMenu.Items.FindByName("RegisterItem").Visible = !isAuthenticated;
            //RightAreaMenu.Items.FindByName("MyAccountItem").Visible = isAuthenticated;
            //RightAreaMenu.Items.FindByName("SignOutItem").Visible = isAuthenticated;
        }

        protected void UpdateUserInfo()
        {
            if (AuthHelper.IsAuthenticated())
            {
                var user = AuthHelper.GetLoggedInUserInfo();
                var myAccountItem = RightAreaMenu.Items.FindByName("MyAccountItem");
                var userName = (ASPxLabel)myAccountItem.FindControl("UserNameLabel");
                var email = (ASPxLabel)myAccountItem.FindControl("EmailLabel");
                var accountImage = (HtmlGenericControl)RightAreaMenu.Items[0].FindControl("AccountImage");
                userName.Text = string.Format("{0} ({1} {2})", user.UserName, user.FirstName, user.LastName);
                email.Text = user.Email;
                accountImage.Attributes["class"] = "account-image";

                if (string.IsNullOrEmpty(user.AvatarUrl))
                {
                    accountImage.InnerHtml = string.Format("{0}{1}", user.FirstName[0], user.LastName[0]).ToUpper();
                }
                else
                {
                    var avatarUrl = (HtmlImage)myAccountItem.FindControl("AvatarUrl");
                    avatarUrl.Attributes["src"] = ResolveUrl(user.AvatarUrl);
                    accountImage.Style["background-image"] = ResolveUrl(user.AvatarUrl);
                }
            }
        }

        protected void RightAreaMenu_ItemClick(object source, DevExpress.Web.MenuItemEventArgs e)
        {
            if (e.Item.Name == "SignOutItem")
            {
                CLogout.DoLogout();
                string loginUrl = FormsAuthentication.LoginUrl;
                Response.Redirect(loginUrl, true);
            }
        }

        protected void ApplicationMenu_ItemDataBound(object source, MenuItemEventArgs e)
        {
            /// e.Item.Image.Url = string.Format("Content/Images/{0}.svg", e.Item.Text);
            //e.Item.Image.UrlSelected = string.Format("Content/Images/{0}-white.svg", e.Item.Text);
        }

        protected void TableOfContentsTreeView_NodeClick(object source, TreeViewNodeEventArgs e)
        {
            string x = e.Node.Text;
            Session["SelectedNode"] = x;

            switch (x)
            {
                case "Tax Center Management":
                    Response.Redirect("~/Admin/TaxCenter");
                    break;
                case "User Management":
                case "Add User":
                    Response.Redirect("~/Admin/User");
                    break;
                case "Users List":
                    Response.Redirect("~/Admin/Users");
                    break;
                case "Manage Roles":
                    Response.Redirect("~/Admin/Roles");
                    break;
                case "Lookup Data":
                case "Lookup Type":
                    Response.Redirect("~/LookupType");
                    break;
                case "Lookup":
                    Response.Redirect("~/Lookup");
                    break;
                case "Address Administration":
                case "Region":
                    Response.Redirect("~/Address/Region");
                    break;
                case "City/Zone":
                    Response.Redirect("~/Address/Zone");
                    break;
                case "Subcity":
                    Response.Redirect("~/Address/Woreda");
                    break;
                case "Woreda":
                    Response.Redirect("~/Address/Kebele");
                    break;
                case "Settings":
                case "Category Rate":
                    Response.Redirect("~/Setting/CategoryRate");
                    break;
                case "Income Tax":
                    Response.Redirect("~/Setting/IncomeTaxRate");
                    break;
                case "Municipality":
                    Response.Redirect("~/Setting/Municipality");
                    break;

                case "Revenue Codes":
                    Response.Redirect("~/Setting/RevenueCodes");
                    break;

                case "Payement_Account_Codes":
                    Response.Redirect("~/Setting/PaymentAccount");
                    break;
                case "Penalty Rightoff":
                    Response.Redirect("~/Setting/PenaltyRightOff");
                    break;
                case "Agricultural Profit Tax":
                case "More Settings..":
                    Response.Redirect("~/Setting/Agriculture");
                    break;
                case "Rental Profit Tax":
                    Response.Redirect("~/Setting/Rental");
                    break;
                case "Penality":
                    Response.Redirect("~/Setting/Penality");
                    break;
                case "Salary":
                    Response.Redirect("~/Setting/Salary");
                    break;
                case "Land use":
                    Response.Redirect("~/Setting/LandUse");
                    break;
                case "Exemption":
                    Response.Redirect("~/Setting/Exemption");
                    break;
                case "TaxReduction":
                    Response.Redirect("~/Setting/TaxReduction");
                    break;
                case "User Site Transfer":
                    Response.Redirect("~/Admin/SiteTransfer");
                    break;
                case "Admin Site Settings":
                    Response.Redirect("~/Setting/AdminSettings");
                    break;
                case "Nationality":
                    Response.Redirect("~/Address/Nationality");
                    break;
                case "Miscellaneous Payment":
                    Response.Redirect("~/Pages/MiscellaneousPayment");
                    break;
                case "Advertisment Type":
                    Response.Redirect("~/Pages/AdvertismentType");
                    break;
            }

        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            SetProfile();

        }
        protected void SetProfile()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (CurrentProfile.ActiveUserProfile == null)
                {
                    string strCurrentUser = HttpContext.Current.User.Identity.Name;
                    var p = ProfileBase.Create(strCurrentUser);
                    var staff = ((ProfileGroupBase)(p.GetProfileGroup("Staff")));
                    var org = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));

                    var activeProfile = new CProfile
                    {

                        FullName = (string)staff.GetPropertyValue("FullName"),
                        Name = (string)org.GetPropertyValue("Name"),
                        NameEng = (string)org.GetPropertyValue("NameEng"),
                        IsActive = (bool)org.GetPropertyValue("IsActive"),
                        Category = (short)org.GetPropertyValue("Category"),
                        Parent = (int)org.GetPropertyValue("Parent"),
                        TaxCenterId = (int)org.GetPropertyValue("TaxCenterId"),
                        Region = (int)org.GetPropertyValue("Region"),
                        Zone = (int)org.GetPropertyValue("Zone"),
                        Wereda = (int)org.GetPropertyValue("Wereda"),
                        Kebele = (int)org.GetPropertyValue("Kebele"),
                        UserId = (string)staff.GetPropertyValue("UserId"),
                        UserGroupId = (short)staff.GetPropertyValue("UserGroupId"),
                        LanguageID = (short)org.GetPropertyValue("LanguageID"),
                    };
                    Session["UserProfile"] = activeProfile;
                }



            }
            else
            {
                string loginUrl = FormsAuthentication.LoginUrl;
                if (Page.IsCallback)
                    ASPxWebControl.RedirectOnCallback(loginUrl);
                else
                    Response.Redirect(loginUrl);
            }
        }
    }
}