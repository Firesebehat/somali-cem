﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Root.master"  CodeBehind="Login.aspx.cs" Inherits="RMS.Login" %>
<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <div class="row">
        <div style="margin: auto; vertical-align: middle; margin-top: 30%">
            <div class="col col-md-2"></div>
            <div class="col col-md-8">
                <asp:Panel runat="server" ID="pnlMsg">
                    <asp:Label runat="server" ID="lblError" EnableTheming="False"></asp:Label>
                    <br />
                    <br />
                    <asp:LoginView ID="loginBox" runat="server">
                        <LoggedInTemplate>
                        </LoggedInTemplate>
                        <AnonymousTemplate>
                            <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" OnLoggingIn="Login1_LoggingIn" OnLoginError="Login1_LoginError" VisibleWhenLoggedIn="False">
                                <LayoutTemplate>
                                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="LoginButton">
                                        <div style="margin-bottom: 25px; width: 200px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control" AutoCompleteType="None" MaxLength="50" TabIndex="1" ToolTip="enter your user name" Width="250px" placeholder="username"></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="UserName" EnableTheming="False" ErrorMessage="Username can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>

                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <asp:TextBox ID="Password" runat="server" CssClass="form-control" AutoCompleteType="None" MaxLength="50" TabIndex="2" TextMode="Password" ToolTip="enter your password" Width="250px" placeholder="password">></asp:TextBox>
                                        </div>

                                        <div style="margin-top: 10px" class="form-group">
                                            <!-- Button -->

                                            <div>
                                                <dx:ASPxButton ID="LoginButton" runat="server" CssClass="btn btn-success" Checked="true" EnableTheming="false" CommandName="Login" Text="Login" ValidationGroup="Login1" />
                                            </div>
                                        </div>



                                    </asp:Panel>
                                </LayoutTemplate>
                            </asp:Login>
                        </AnonymousTemplate>
                    </asp:LoginView>
                    <asp:LoginView ID="LoginView1" runat="server">
                        <LoggedInTemplate>
                        </LoggedInTemplate>
                        <AnonymousTemplate>
                            <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" OnLoggingIn="Login1_LoggingIn" OnLoginError="Login1_LoginError" VisibleWhenLoggedIn="False">
                                <LayoutTemplate>
                                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="LoginButton">
                                        <div style="margin-bottom: 25px; width: 200px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control" AutoCompleteType="None" MaxLength="50" TabIndex="1" ToolTip="enter your user name" Width="250px" placeholder="username"></asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="UserName" EnableTheming="False" ErrorMessage="Username can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>

                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <asp:TextBox ID="Password" runat="server" CssClass="form-control" AutoCompleteType="None" MaxLength="50" TabIndex="2" TextMode="Password" ToolTip="enter your password" Width="250px" placeholder="password">></asp:TextBox>
                                        </div>

                                        <div style="margin-top: 10px" class="form-group">
                                            <!-- Button -->

                                            <div>
                                                <dx:ASPxButton ID="LoginButton" runat="server" CssClass="btn btn-success" Checked="true" EnableTheming="false" CommandName="Login" Text="Login" ValidationGroup="Login1" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </LayoutTemplate>
                            </asp:Login>
                        </AnonymousTemplate>
                    </asp:LoginView>
                </asp:Panel>
            </div>
            <div class="col col-md-2"></div>
        </div>
        </div>
</asp:Content>