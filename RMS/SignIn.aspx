﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SignIn.Master" CodeBehind="SignIn.aspx.cs" Inherits="RMS.SignInModule" Title="Sign In" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
    <link rel="stylesheet" type="text/css" href='<%# ResolveUrl("~/Content/SignInRegister.css") %>' />
    <script type="text/javascript" src='<%# ResolveUrl("~/Content/SignInRegister.js") %>'></script>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="PageContent" runat="server">
    <style>
        
    </style>
    <div class="row text-center">
        <div class="col-md-8">
            <div class="text-center card w-50 card-body rounded " style="margin-left: 500px; margin-bottom:-50px; margin-top:25px; border: 10px">
                <h4 class="card-title font-weight-bold flex-grow-1;" style="background-color: #009688; color: white;text-align:left;padding:5px">Sign In</h4>
                
                <div style="margin-left: 40px;margin-left: 50px">
                    <div class="form-row" runat="server" id="pnlMsg">
                    <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
               </div>
                    <div class="form-row">
                        <dx:ASPxLabel ID="lblUserName" runat="server" Text="User Name"></dx:ASPxLabel>
                        <dx:ASPxLabel ID="lblUserNameStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                    </div>
                    <div class="form-row w-100">
                        <dx:ASPxTextBox ID="txtUserName" ClientInstanceName="txtUserName" runat="server" Width="70%">
                            <ValidationSettings Display="Dynamic" SetFocusOnError="true" ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText">
                                <RequiredField IsRequired="true" ErrorText="User name is required." />
                            </ValidationSettings>
                            <ClientSideEvents Init="function(s, e){ s.Focus(); }" />
                        </dx:ASPxTextBox>
                    </div>
                    <div class="form-row w-75">
                        <dx:ASPxLabel ID="lblPassword" runat="server" Text="Password"></dx:ASPxLabel>
                        <dx:ASPxLabel ID="lblPasswordStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                    </div>
                    <div class="form-row w-100">
                        <dx:ASPxButtonEdit ID="txtPassword" runat="server" Width="70%" Password="true" ClearButton-DisplayMode="Never">
                            <ButtonStyle Border-BorderWidth="0" Width="6" CssClass="eye-button" HoverStyle-BackColor="Transparent" PressedStyle-BackColor="Transparent">
                            </ButtonStyle>
                            <ButtonTemplate>
                                <div></div>
                            </ButtonTemplate>
                            <Buttons>
                                <dx:EditButton>
                                </dx:EditButton>
                            </Buttons>
                            <ValidationSettings Display="Dynamic" SetFocusOnError="true" ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText">
                                <RequiredField IsRequired="true" ErrorText="Password is required." />
                            </ValidationSettings>
                            <ClientSideEvents ButtonClick="onPasswordButtonEditButtonClick" />
                        </dx:ASPxButtonEdit>
                    </div>
                    <div class="form-row"style="margin-top:20px" >
                        <dx:ASPxCaptcha ID="Captcha" runat="server" TextBox-Position="Bottom">
                            <ValidationSettings SetFocusOnError="true" ErrorDisplayMode="Text" Display="Dynamic" />
                        </dx:ASPxCaptcha>
                    </div>
                    <div class="form-row">
                        <dx:ASPxCheckBox ID="chkRememberMe" runat="server" Text="Remember me" Checked="true"></dx:ASPxCheckBox>
                    </div>
                    <div class="form-row text-center">
                        <dx:ASPxButton ID="SignInButton" OnClick="SignInButton_Click" CausesValidation="false"  runat="server" Text="Login">
                        </dx:ASPxButton>
                    </div>
                </div>
                <br /><br />
            </div>
            <div class="col col-md-4"></div>
        </div>
    </div>
</asp:Content>
