﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error404.aspx.cs" Inherits="RMS.ErrorPages.Error404"  MasterPageFile="~/SignIn.Master"%>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
 <div style="min-height:650px;padding:5px">
    <div class="alert alert-danger" style="background-color:white;padding:5%">
       <asp:Label ID="Label1" style="font-size:25px;font-stretch:expanded;font-weight:bold;font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" runat="server" 
           Font-Bold="true" Font-Size="XX-Large" Text="Error has occured!"></asp:Label>
        <br/><br />
        <p class="jumbotron">
            <asp:Label ID="Label2" ForeColor="Black" style="font-size:14px" runat="server" Text="Error has occured during processing your request. The requested page was not found!"></asp:Label>
             <asp:LinkButton style="font-stretch:expanded;margin-left:20px;color:#8bc34a;font-size:14px;font-weight:bold"  ID="LinkButton1" runat="server"
            PostBackUrl="~/Default" Text="Return to home page"></asp:LinkButton>
        </p>
    </div>
     </div>
</asp:Content>
