﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Error.aspx.vb" Inherits="Admin_ErrorPages_Error" MasterPageFile="~/SignIn.Master"%>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
   <div style="min-height: 650px; padding: 5px">
        <div class="alert alert-danger" style="background-color: white; padding: 5%">
            <asp:Label ID="Label1" Style="font-size: 25px; font-stretch: expanded; font-weight: bold; font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" runat="server"
                Font-Bold="true" Font-Size="XX-Large" Text="Error has occured!"></asp:Label>
            <br />
            <br />
            <p class="jumbotron">
                <asp:Label ID="Label2" ForeColor="Black" Style="font-size: 14px" runat="server" Text="There has been an error during processing your request. The administrator will be informed."></asp:Label>
                <asp:LinkButton Style="font-stretch: expanded; margin-left: 20px; color: #8bc34a; font-size: 14px; font-weight: bold" ID="LinkButton1" runat="server"
                    PostBackUrl="../Default" Text="Return to home page"></asp:LinkButton>
            </p>
        </div>
    </div>
</asp:Content>

