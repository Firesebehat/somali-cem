﻿<%@ Page Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="Business.aspx.cs" Inherits="RMS.BusinessF" %>

<%@ Register TagPrefix="cdp" Namespace="CUSTORDatePicker" Assembly="CUSTORDatePicker" %>
<asp:Content runat="server" ContentPlaceHolderID="LeftPanelContent">
      <h3 class="section-caption contents-caption">Contents</h3>

    <dx:ASPxTreeView runat="server" ID="TableOfContentsTreeView" ClientInstanceName="tableOfContentsTreeView"
        EnableNodeTextWrapping="true" AllowSelectNode="true" Width="100%" SyncSelectionMode="None" DataSourceID="NodesDataSource">
        <Styles>
            <Elbow CssClass="tree-view-elbow" />
            <Node CssClass="tree-view-node" HoverStyle-CssClass="hovered"  />
        </Styles>
        <%--<ClientSideEvents NodeClick="function (s, e) { HideLeftPanelIfRequired(); }" />--%>
    </dx:ASPxTreeView>

   <asp:XmlDataSource ID="NodesDataSource" runat="server" DataFile="~/App_Data/OverviewContents.xml" XPath="//Nodes/*" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageToolbar">
    <style type="text/css">
        .SearchText {
            width: 100%;
            border-top: none;
            border-left: none;
            border-right: none;
            height: 40px;
        }
    </style>
    <script type="text/javascript">
        function DoNew() {
            btnSave.SetEnabled(true);
            clbOrganization.PerformCallback('New');
        }
        function DoSaveBusiness() {
            if (!ASPxClientEdit.ValidateGroup('GOrganization')) {
                ShowError('እባክዎ ያልተሟላ መረጃ አለ');
                 return;
             }
            clbOrganization.PerformCallback('Save');
        }
    </script>
    <div style="padding-left: 180px">
        <br />
        <h3 class="category" id="Business">የድርጅት ምዝገባ</h3>


        <%--<div class="row">
            <div class="col col-md-12">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <dx:ASPxTextBox runat="server" NullText="ግብር ከፋይ ይምረጡ" Width="100%" ID="txtCustomer" CssClass="SearchText"></dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxButton runat="server" CssClass="btn btn-default btn-sm" Style="max-height: 40px" Text="ይምረጡ..." ID="ASPxButton1">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>



        </div>--%>
        <br />
        <br />
        <dx:ASPxButton runat="server" Text="አዲስ" ID="btnNew" ClientInstanceName="btnNew" AutoPostBack="false" CausesValidation="false">
            <ClientSideEvents Click="DoNew" />
            <Image IconID="iconbuilder_actions_add_svg_32x32">
            </Image>
        </dx:ASPxButton>
        <dx:ASPxButton runat="server" Text="አስቀምጥ" ClientEnabled="true" ID="btnSave" ClientInstanceName="btnSave" AutoPostBack="false" CausesValidation="true">
            <Image IconID="save_save_svg_32x32">
            </Image>
            <ClientSideEvents Click="DoSaveBusiness" />

        </dx:ASPxButton>
        
    </div>
    <br />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <style type="text/css">
        .dxeTextBoxDefaultWidthSys {
            width: 100% !important
        }

        Label {
            border-color: silver;
        }

        td {
            margin: 1px;
            padding: 2px;
            vertical-align:top;
        }

        table {
            width: 100%;
        }
       #PageContent_clbOrganization_dpRegistrationDate_txt
       {
           border:1px solid rgba(0,0,0,0.22);
           height:30px;
       }
    </style>
    <script>
        function MenuItemClick(s, e) {
            if (e.item.name === "mnuNew") {
                DoAddOrganizationClick();
            }
            else if (e.item.name === "mnuList") {
                DoListOrganizationClick();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveOrganizationClick();
            }
        }


        function OnMnuAddtionalInfoClick(s, e) {
            if (e.item.name === "mnuPurpose") {
                popPurpose_Click();
            }
            else if (e.item.name === "mnuRepresentative") {
                PopRepresentative_Click();
            }
        }

        function PopRepresentative_Click(s, e) {

            clbPerson.PerformCallback('FindAllRepresentative');
            popRepresentative.Show();
        }
        function popPurpose_Click(s, e) {

            clbOrganization.PerformCallback('FillPurpose');
            popPurpose.Show();
        }
        function DoSaveOrganiztionCatagoryClick(s, e) {

            clbOrganization.PerformCallback('SaveOrganiztionCatagory');
        }

        function InitMenu() {

            //ToggleEnabled("mnuNew", false);

        }

        function DoNewOrganizationClick(s, e) {
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnumnuNew", true);
            clbOrganization.PerformCallback('New');
        }

        function DoAddOrganizationClick(s, e) {

            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuSave", true);
            clbOrganization.PerformCallback('New');
        }

        function DoSaveOrganizationClick() {
            clbOrganization.PerformCallback('Save');
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnumnuNew", true);
        }

        function DoListOrganizationClick(s, e) {

            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuList", false);
            ToggleEnabled("mnuNew", true);
            gvwOrganization.PerformCallback('List');
        }

        function OnclbOrganizationEndCallback(s, e) {
            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Delete") {
                    ShowSuccess(s.cpMessage);
                    gvwOrganization.PerformCallback('List');
                    return;
                }
                else if (s.cpAction == "Save") {
                    btnSave.SetEnabled(false);
                    ShowSuccess(s.cpMessage);
                }

            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s)
        {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }
       
        function gvwRepresentative_CustomButtonClick(s, e) {

            if (e.buttonID != 'DELETINGRep' && e.buttonID != 'EDITINGRep') return;
            if (e.buttonID == 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, "OrganizationGuid", ShowPopupRepresentative);
            }
            if (e.buttonID == 'EDITINGRep') {
                DoRepresentativeSelectedRow(oe);
                ToggleEnabled("mnuList", true);
                ToggleEnabled("mnuNew", true);
            }
        }

        function gvwOrganization_SelectionChanged(s, e) {
            if (e.visibleIndex == -1) {
                mnuAddtionalInfo.SetVisible(false);
                return;
            }
            mnuAddtionalInfo.SetVisible(true);
            s.GetRowValues(e.visibleIndex, "Tin;OrganizationName", ShowMenuItems);
        }


        function ShowMenuItems(values) {

            mnuAddtionalInfo.SetVisible(true);
            txtTinForOrgCatagory.SetText(values[0]);
            txtOrganizationNameForOrgCatagory.SetText(values[1]);
            hfOrganizationTin.SetText(values[0]);
            //clbOrganization.PerformCallback('AssighinHiddenFiled' + '|' + values[0] + '|' + values[1]);
            return;
        }




        function DoRepresentativeSelectedRow(e) {
            gvwRepresentative.GetRowValues(e.visibleIndex, "MainGuid", OnRepresentativeSelected);
        }

        function OnRepresentativeSelected(values) {

            btnSaveRepresentative.SetEnabled(true);
            clbOrganization.PerformCallback('EditRepresentative' + '|' + values);
        }


        

        function ShowPopupOwner() {
            popupControlRepresentative.Show();
            btnYesRepresentative.Focus();
        }

        function btnYes_Click(s, e) { ClosePopupRepresentative(true); }
        function btnNo_Click(s, e) { ClosePopupRepresentative(false); }

        function ClosePopupRepresentative(result) {
            popupControlOwner.Hide();
            if (result) {
                gvwRepresentative.GetRowValues(rowVisibleIndex, "MainGuid", OnRepresentaicveDeleted);
            }
        }
        function OnRepresentaicveDeleted(values) {
            clbOrganization.PerformCallback('DeleteRepresentative' + '|' + values);
        }





       

        


        function OnZoneChanged(cboZone) {
            cboWereda.ClearItems();
            cboKebele.ClearItems();
            cboWereda.PerformCallback(cboZone.GetValue().toString());
        }

        function OnWoredaChanged(cboWereda) {

            cboKebele.ClearItems();
            cboKebele.PerformCallback(cboWereda.GetValue().toString());
        }


        function OnRegionRepresentativeChanged(cboRegion) {
            cboRepresentativeZone.ClearItems();
            cboRepresentativeWereda.ClearItems();
            cboRepresentativeKebele.ClearItems();
            cboRepresentativeZone.PerformCallback(cboRegion.GetValue().toString());
        }

        function OnZoneRepresentativeChanged(cboZone) {
            cboRepresentativeWereda.ClearItems();
            cboRepresentativeKebele.ClearItems();
            cboRepresentativeWereda.PerformCallback(cboZone.GetValue().toString());
        }

        function OnWoredaRepresentativeChanged(cboWereda) {

            cboRepresentativeKebele.ClearItems();
            cboRepresentativeKebele.PerformCallback(cboWereda.GetValue().toString());
        }


        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);

        }
        function GetMenuItem(mnuName) {
            return mnuToolbar.GetItemByName(mnuName);
            //var m=mnuToolbar.GetSelectedItem();
        }
        function DoSearchOrganizatioinByNameClick(s, e) {

            if (txtOrganizationName.GetValue() == null) {

                ShowAlert("error", "እባክዎ የስልጠና ተቌሙ ስም ቢያንስ አንድ መፈለግያ ፊደል ያስገቡ!");
                return;
            }
            //pnlOrganizationData.SetVisible(false);
            //pnlOrganizationGrid.SetVisible(true);
            ToggleEnabled("mnuList", true);
            ToggleEnabled("mnuNew", true);
            gvwOrganization.PerformCallback('SearchByName');
        }


        function DoSearchOrganizatioinByTinClick(s, e) {

            if (txtTin.GetValue() == null) {

                ShowAlert("error", "እባክዎ ቲን ያስገቡ!");
                return;
            }
            //pnlOrganizationData.SetVisible(false);
            //pnlOrganizationGrid.SetVisible(true);
            //ToggleEnabled("mnuList", true);
            ToggleEnabled("mnuNew", true);
            gvwOrganization.PerformCallback('SearchByTin');
        }
        function chxIsMegaCheckedChanged() {
            if (chxIsMega.GetChecked())
                cbxGovProjectType.SetEnabled(true);
            else {
                cbxGovProjectType.SetEnabled(false);
                cbxGovProjectType.SetSelectedIndex(-1);
            }
        }
        function cboOrganizationTypeSelectionChanged(s) {
            // alert(s.GetValue());
            if (s.GetSelectedIndex() != 1) chxIsMega.SetChecked(false);
            chxIsMega.SetEnabled(s.GetSelectedIndex() == 1);
            chxIsMegaCheckedChanged();
        }
        function cboInit() {
            cbxGovProjectType.SetEnabled(false);
        }
        function chxInit() {
            chxIsMega.SetEnabled(false);
        }
    </script>
    <script type="text/javascript">
        function clbOrganizationInit() {

        }
       
        function OnRegionChanged(cboRegion) {
            cboZone.ClearItems();
            cboWereda.ClearItems();
            cboKebele.ClearItems();
            cboZone.PerformCallback(cboRegion.GetValue().toString());
        }
        function ShowCategory() {
            popContent.SetContentUrl('BusinessCategory.aspx');
            popContent.SetHeaderText('የንግድ ዘርፍ ምረጥ')
            popContent.Show();
        }
    </script>
    <div class="text-content" runat="server" id="TextContent">
        <dx:ASPxCallbackPanel ID="clbOrganization" OnCallback="clbOrganization_Callback" ClientInstanceName="clbOrganization" runat="server" Width="100%" Height="100%" meta:resourcekey="clbOrganizationResource1">
            <ClientSideEvents Init="clbOrganizationInit" EndCallback="OnclbOrganizationEndCallback" />
            <PanelCollection>
                <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="pnlMainResource1">
                    <div class="row">
                        <div class="col-md-12">
                            <dx:ASPxHiddenField runat="server" ID="hdBusiness" ClientInstanceName="hdBusiness"></dx:ASPxHiddenField>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <dx:ASPxLabel runat="server" Text="የድርጅቱ መጠሪያ በአማርኛ"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>
                                        <td colspan="2">

                                            <dx:ASPxLabel runat="server" Text="የድርጅቱ መጠሪያ በእንግሊዝኛ"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <dx:ASPxTextBox runat="server" ID="txtOrgNameAmh">
                                                 <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                  <RegularExpression ErrorText="ያአማርኛ ቃላቶችን ብቻ ይጠቀሙ" ValidationExpression="[ \u1200-\u137F \u0008 \/.\d]+$" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>

                                        </td>
                                        <td colspan="2">
                                            <dx:ASPxTextBox runat="server" ID="txtOrgNameEng">
                                                 <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="የንግድ ዘርፍ">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="የፈቃድ ቁጥር"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="ካፒታል">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel7" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="ፈቃድ የተሰጠበት ቀን"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel8" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxTextBox runat="server" ID="txtCaxtegory" NullText="ዘርፍ ለመምረጥ ይጫኑ" ClientInstanceName="txtCaxtegory" style="color:#bdbdbd !important;">
                                               <ClientSideEvents  GotFocus="ShowCategory" />
                                                 <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox runat="server" ID="txtLiceseNo">
                                                 <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>

                                        </td>
                                        <td>
                                            <dx:ASPxTextBox runat="server" ID="txtCapital">
                                               <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                 <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <cdp:CUSTORDatePicker ID="dpRegistrationDate" runat="server" SelectedDate="" TabIndex="1" 
                                                TextCssClass="" Width="100%" meta:resourcekey="dpFromResource1"  Height="30px" />
                                       </td>

                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col col-md-12">
                            <h4 class="category">አድራሻ</h4>

                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="lblRegion" runat="server" Text="ክልል" meta:resourcekey="lblRegionResource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="lblRegionRed" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblRegionRedResource1"></dx:ASPxLabel>

                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lblZone" runat="server" Text="ዞን" meta:resourcekey="lblZoneResource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="lblZoneRed" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblZoneRedResource1"></dx:ASPxLabel>

                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lblWereda" runat="server" Text="ክፍለ ከተማ" meta:resourcekey="lblWeredaResource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="lblWeredaRed" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblWeredaRedResource1"></dx:ASPxLabel>

                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lblKebele" runat="server" Text="ወረዳ" meta:resourcekey="lblKebeleResource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="lblKebeleRed" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblKebeleRedResource1"></dx:ASPxLabel>

                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="lblPhoneNo0" runat="server" Text="የቤት ቁጥር" meta:resourcekey="lblPhoneNo0Resource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="lblKebeleRed0" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblKebeleRed0Resource1"></dx:ASPxLabel>

                                        </td>
                                        <td>
                                            <dx:ASPxLabel ID="Label3" runat="server" Text="ስልክ ቁጥር" meta:resourcekey="Label3Resource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="Label4" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="Label4Resource1"></dx:ASPxLabel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion" IncrementalFilteringMode="StartsWith" Width="100%" meta:resourcekey="cboRegionResource1">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnRegionChanged(s); }" />
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cboZone" runat="server" ClientInstanceName="cboZone" EnableClientSideAPI="True" Width="100%" IncrementalFilteringMode="StartsWith" OnCallback="cboZone_Callback" meta:resourcekey="cboZoneResource1">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnZoneChanged(s); }" />
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cboWereda" runat="server" ClientInstanceName="cboWereda" EnableClientSideAPI="True" Width="100%" IncrementalFilteringMode="StartsWith" OnCallback="cboWoreda_Callback" meta:resourcekey="cboWeredaResource1">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnWoredaChanged(s); }" />
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cboKebele" runat="server" ClientInstanceName="cboKebele" Width="100%" EnableClientSideAPI="True" IncrementalFilteringMode="StartsWith" OnCallback="cboKebele_Callback" meta:resourcekey="cboKebeleResource1">
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtHouseNo" runat="server" ClientInstanceName="txtHouseNo" meta:resourcekey="txtHouseNoResource1">
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtTell" runat="server" ClientInstanceName="txtTell" meta:resourcekey="txtTellResource1">
                                                <MaskSettings Mask="(999)9999999" />
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                             <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="የቦታው ልዩ ቦታ መጠሪያ" meta:resourcekey="Label3Resource1"></dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="Label4Resource1"></dx:ASPxLabel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <dx:ASPxTextBox ID="txtOtherAddress" runat="server" ClientInstanceName="txtOtherAddress" meta:resourcekey="txtOtherAddressResource1">
                                                <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
        

      
     
    </div>
       <dx:ASPxPopupControl runat="server" ID="popContent" ClientInstanceName="popContent" PopupHorizontalAlign="WindowCenter" 
           PopupAnimationType="Fade" EnableCallbackAnimation="true"
            PopupVerticalAlign="WindowCenter" Width="550px" Height="500px">
        </dx:ASPxPopupControl>
</asp:Content>
