﻿using CUSTOR.Common;
using RMS.Model;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace CUSTOR
{
    public partial class AccessDenied : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Super Admin"))
                btnHome.Visible = true;
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            CLogout.DoLogout();
            string loginUrl = FormsAuthentication.LoginUrl;
            Response.Redirect(loginUrl, true);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectFromLoginPage(User.Identity.Name,false);
        }
    }
}