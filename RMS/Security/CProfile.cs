﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;

public class CProfile
{
    public string FullName { get; set; }
    public  short UserGroupId { get; set; }
    public int TaxCenterId { get; set; }
    public string NameEng { get; set; }

    public string Name { get; set; }
    public int Parent { get; set; }
    public string UserId { get; set; }
    public short Category { get; set; }
    public short LanguageID { get; set; }
    public int Region { get; set; }
    public int Zone { get; set; }
    public int Wereda { get; set; }
    public int Kebele { get; set; }
    public bool IsActive { get; set; }
     
}

