﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

 
    public static class CurrentProfile
    {
        public static CProfile ActiveUserProfile
    {
        get
        {
            //Session["UserProfile"] is on startup at master page
            //Profile propoerties can be accssed from any form as ActiveUserProfile.Name
             return (CProfile)HttpContext.Current.Session["UserProfile"]; 
        }
    }
    }
 