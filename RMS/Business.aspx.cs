﻿using CUSTOR.Business;
using CUSTOR.Domain;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;

namespace RMS
{
    public partial class BusinessF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(!Page.IsPostBack && !Page.IsCallback)
            {
                TableOfContentsTreeView.DataBind();
                TableOfContentsTreeView.ExpandAll();
                Session["IsNewBusiness"] = false;
                FillRegion();
                Session["Tin"] = 1;
            }
        }
        protected void clbOrganization_Callback(object sender, CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                strID = parameters[1];
            switch (strParam)
            {
                case "New":
                    DoClearForm();
                    Session["IsNewBusiness"] = true;
                    break;
                case "Save":
                    DoSave();
                    break;
            }
        }
        private bool DoSave()
        {
            Session["IsNewBusiness"] = true;//temp
            Session["CustomerId"] = 7;//temp

            Business business = new Business();
            CVGeez objGeez = new CVGeez();
            BusinessBs OrganizationBuss = new BusinessBs();
            Address address = new Address();
            DateTime eventDate = DateTime.Now;
            try
            {
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {
                    RestoreComboValues();
                    ShowError(GetErrorDisplay(errMessages));
                    return false;
                }
                bool isValid = ASPxEdit.ValidateEditorsInContainer(clbOrganization, "GOrganization");
                if (!isValid)
                {
                    if (errMessages.Count > 0)
                    {
                        ShowError(GetErrorDisplay(errMessages));
                    }

                    RestoreComboValues();
                    return false;
                }
                clbOrganization.JSProperties["cpAction"] = "Save";
                // var user =Membership.GetUser();
                // var userProfile = CurrentProfile.ActiveUserProfile;
                business.CustomerId = Int32.Parse(Session["CustomerId"].ToString());
                business.TradeNameAmh = txtOrgNameAmh.Text;
                business.TradeName = txtOrgNameEng.Text;
                business.OwnerTIN = Session["Tin"].ToString();
                business.TradeNameSort = objGeez.GetSortValueU(txtOrgNameAmh.Text);
                business.TradeNameSoundex = objGeez.GetSoundexValue(txtOrgNameAmh.Text);
                business.CategoryId = int.Parse(hdBusiness["CategoryId"].ToString());
                business.LicenseNumber = txtLiceseNo.Text;
                business.LicenseDate = dpRegistrationDate.SelectedGCDate;
                business.Status = 1;//set enum value
                business.CenterId = 1;// Convert.ToInt32(userProfile.OrgCode);
                business.CenterName = "Head Office";// userProfile.SiteName;
                

                address.Region = cboRegion.Value.ToString();
                address.Zone = cboZone.Value.ToString();
                address.Wereda = cboWereda.Value.ToString();
                address.Kebele = cboKebele.Value.ToString();
                address.HouseNo = txtHouseNo.Text;
                address.TeleNo = txtTell.Text;
                address.OtherAddress = txtOtherAddress.Text;
                address.AddressType = 2;//temp enum
                BusinessBs businessBs = new BusinessBs();
                if((bool)Session["IsNewBusiness"])
                {
                    business.CreatedUserId = 1; //user != null ? user.ProviderUserKey.ToString() : "0";
                    business.CreatedBy = "userone"; //User.Identity.Name;
                    business.CreatedDate = eventDate;
                    address.Username = business.CreatedBy;
                    address.EventDateTime = eventDate;
                    businessBs.InsertBusiness(business, address);

                    //address.
                }
                else
                {
                    business.UpdatedUserId = 1;//user != null ? user.ProviderUserKey.ToString() : "0";
                    business.UpdatedBy = User.Identity.Name;
                    business.UpdatedDate = eventDate;
                    address.UpdatedUsername = User.Identity.Name;
                    address.UpdatedEventDatetime = eventDate;
                    business.Id = Convert.ToInt32(Session["BusinessId"]);
                    address.Id = Convert.ToInt32(hdBusiness["AddressId"]);
                    businessBs.UpdateBusiness(business, address);
                }


                Session["IsNewOrganization"] = false;
                ShowSuccess("መረጃው በትክክል ተቀምጧል");
                DoClearForm();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }

        private void DoClearForm()
        {
            txtOrgNameAmh.Text = string.Empty;
            txtOrgNameEng.Text = string.Empty;
            txtCapital.Text = string.Empty;
            txtLiceseNo.Text = string.Empty;
            txtCaxtegory.Text = string.Empty;
            hdBusiness["CategoryId"] = null;
            cboRegion.SelectedIndex = -1;
            cboWereda.SelectedIndex = -1;
            cboZone.SelectedIndex = -1;
            cboKebele.SelectedIndex = -1;
            txtHouseNo.Text = string.Empty;
            txtTell.Text = string.Empty;
            txtHouseNo.Text = string.Empty;
            txtOtherAddress.Text = string.Empty;
            dpRegistrationDate.SelectedDate = string.Empty;
        }

        protected void cboZone_Callback(object source, CallbackEventArgsBase e)
        {

            FillZoneCombo(cboRegion.Value.ToString());
        }
        protected void cboWoreda_Callback(object source, CallbackEventArgsBase e)
        {
            FillWoredaCombo(cboZone.Value.ToString());

        }
        protected void cboKebele_Callback(object source, CallbackEventArgsBase e)
        {
            FillKebeleCombo(cboWereda.Value.ToString());

        }
        protected void gvwOrganization_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
        }
        protected void gvwOrganization_PageIndexChanged(Object sender, EventArgs e)
        {
            //BindGrid();
        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            ////Validate the ethiopic date
            if(dpRegistrationDate.SelectedDate == string.Empty)
            {
                errMessages.Add("እባክዎ የፈቃድ ቀን ያስፈልጋል!");
            }
            else if(!dpRegistrationDate.IsValidDate)
                {
                    errMessages.Add("ያስገቡት ቀን ትክክል ያልሆነ ነው");
                }
            else if (dpRegistrationDate.SelectedGCDate > DateTime.Now)
            {
                errMessages.Add("የምዝገባ ቀን ከ ዛሬ ቀን መብለጥ የለበትም");
            }
            return errMessages;

        }



        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }
        private void FillRegion()
        {
            //Fill combo from tblRegion
            RegionBusiness objRegion = new RegionBusiness();
            List<Region> objRegions = new List<Region>();
            int Lan = 1;// CurrentProfile.ActiveUserProfile.LanguageID;
            objRegions = objRegion.GetRegions(Lan);
            cboRegion.DataSource = objRegions;
            cboRegion.TextField = "Description";
            cboRegion.ValueField = "Code";
            cboRegion.DataBind();
        }

        protected void FillZoneCombo(string region)
        {
            if (string.IsNullOrEmpty(region)) return;
            const string strAddisAbabaRegionCode = "14";
            const string strAddisAbabaZoneCode = "80";
            int Lan = 1;// CurrentProfile.ActiveUserProfile.LanguageID;
            ZoneBusiness objAdd = new ZoneBusiness();
            cboZone.DataSource = objAdd.GetZones(region,Lan);
            cboZone.TextField = "Description";
            cboZone.ValueField = "Code";
            cboZone.DataBind();

            if (cboRegion.Value.ToString() == strAddisAbabaRegionCode)
            {
                cboZone.Value = strAddisAbabaZoneCode;
            }
        }

        protected void FillWoredaCombo(string zone)
        {
            if (string.IsNullOrEmpty(zone)) return;
            WoredaBusiness objAddda = new WoredaBusiness();
            int Lan = 1;// CurrentProfile.ActiveUserProfile.LanguageID;
            cboWereda.DataSource = objAddda.GetWoredas(zone, Lan);
            cboWereda.TextField = "Description";
            cboWereda.ValueField = "Code";
            cboWereda.DataBind();
        }

        protected void FillKebeleCombo(string woreda)
        {
            if (string.IsNullOrEmpty(woreda)) return;
            KebeleBusiness objAdd = new KebeleBusiness();
            int Lan = 1;// CurrentProfile.ActiveUserProfile.LanguageID;
            cboKebele.DataSource = objAdd.GetKebeles(woreda, Lan);
            cboKebele.TextField = "Description";
            cboKebele.ValueField = "Code";
            cboKebele.DataBind();
        }
        private void RestoreComboValues()
        {
            if (cboRegion.Value != null && cboRegion.Items.Count ==0)
                FillZoneCombo(cboRegion.Value.ToString());
            if (cboZone.Value != null  && cboZone.Items.Count ==0)
                FillWoredaCombo(cboZone.Value.ToString());
            if (cboWereda.Value != null & cboWereda.Items.Count == 0)
                FillKebeleCombo(cboWereda.Value.ToString());
        }
        private void ShowError(string strMsg)
        {
            clbOrganization.JSProperties["cpMessage"] = strMsg;
            clbOrganization.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbOrganization.JSProperties["cpMessage"] = strMsg;
            clbOrganization.JSProperties["cpStatus"] = "ERROR";
            clbOrganization.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbOrganization.JSProperties["cpMessage"] = strMsg;
            clbOrganization.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbOrganization.JSProperties["cpMessage"] = strMsg;
            clbOrganization.JSProperties["cpStatus"] = "SUCCESS";
        }

    }
}