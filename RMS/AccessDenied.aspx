﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SignIn.Master" AutoEventWireup="true" CodeBehind="AccessDenied.aspx.cs" Inherits="CUSTOR.AccessDenied" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <div  class="RoundPanelWhite" style="margin: 60px">
        <table class="nav-justified">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td rowspan="2">&nbsp;
                <img src="../Img/denied.png" style="padding:5px"/>
            </td>
            <td style="vertical-align:top"> 
                    <h1 style="color: orange">የተነፈገ ገፅ</h1>
                    <h2 style="color: orange">Access Denied</h2>
                    <hr/>
            </td>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
        </tr>
        <tr>
            <td><h5>ይቅርታ! ይህን ስራ ለማከናወን መብት አልተሰጥዎትም!</h5> 
                    <h5>Sorry! You are not allowed to access this resource!</h5>
                 
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <dx:ASPxButton ID="btnSignOut" runat="server" Text="SignOut"  OnClick="btnSignOut_Click"  ></dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="btnHome" runat="server" Text="Home"  PostBackUrl="Default" Visible="false"  ></dx:ASPxButton>

            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
   </div>
</asp:Content>
