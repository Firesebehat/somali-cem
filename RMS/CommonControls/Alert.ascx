﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Alert.ascx.cs" Inherits="RMS.CommonControls.Alert" %>
<style>
    .message{
        z-index:9;
        position:relative;
    }
</style>
<div id="divMessageBody">
                <a class="close-btn" onclick="HideMessage();" style="display:none">x</a>
                <div id="divMessage" class="message"></div>
            </div>
            <script type="text/javascript">
                var Timeout;
                var DisplayInterval = 8000;
                var mRight = -720;

                function ShowError(msg) {

                    ShowAlert(msg, "error");
                }
                function ShowMessage(msg) {

                    ShowAlert(msg, "info");
                }
                function ShowSuccess(msg) {

                    ShowAlert(msg, "success");
                }
                function ShowAlert(msg, type) {

                    clearInterval(Timeout);
                    $(".close-btn").show();

                    $("#divMessageBody").css("right", mRight);
                    var classAttr = "message-box " + type;
                    $("#divMessage").html(msg);
                    $("#divMessageBody").attr("class", classAttr);

                    $("#divMessageBody").stop().animate({
                        right: "6px"
                    }, 700, "easeInOutCirc");

                    Timeout = setTimeout(function () {
                        HideMessage();
                    }, DisplayInterval);
                }

                function HideMessage() {
                    $("#divMessageBody").stop().animate({
                        right: mRight
                    }, 600, "easeInOutCirc");
                    $("close-btn").hide();
                    clearInterval(Timeout);
                }
                $.extend($.easing, {
                    easeInOutCirc: function (x, t, b, c, d) {
                        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
                        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
                    }
                });


            </script>