using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Web.Configuration;
using System.Web.Security;
using CUSTOR.Business;
using CUSTOR.DataAccess;
using CUSTOR.Domain;
using DevExpress.Web;
using RMS.Model;

namespace RMS
{
    public partial class SignInModule : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SignInButton_Click(object sender, EventArgs e)
        {
            if (!Captcha.IsValid)
                return;

            Page.Validate();
            if (!Page.IsValid) return;
            UserDataAccess objUser = new UserDataAccess();
            HashInfo objHash = new HashInfo();
            User user = new User();
            if (Membership.ValidateUser(txtUserName.Text, txtPassword.Text))
            {
                //user = objUser.GetUserHashCode(txtUserName.Text);
                //var roles = Roles.GetRolesForUser(txtUserName.Text);
                //string UserLoginText = txtUserName.Text + txtPassword.Text + user.TaxCenterId;
                //foreach(string role in roles)
                //{
                //    UserLoginText = UserLoginText + role;
                //}
                //if (!objHash.VerifyHash(UserLoginText, user.UserHash))
                //{
                //    lblMsg.Text = "User Data Is Changed Directly From The DataBase</Br>Please Contact Your Administrator!";
                //    return;
                //}

                FormsAuthentication.SetAuthCookie(txtUserName.Text, true);
                if (IsValidAccount())
                {
                    string ipAddress = GetIp();
                    string mac = GetMacAddress();

                    InsertUseLogInHistory(txtUserName.Text, ipAddress, mac);
                    Response.Redirect(FormsAuthentication.DefaultUrl, true);
                }
            }
            else
            {
                lblMsg.Text = "Invalid username or password!";
            }
        }

        private bool InsertUseLogInHistory(string userName, string ipAddress, string mac)
        {
            CUSTOR.Bussiness.UserBussiness bussiness = new CUSTOR.Bussiness.UserBussiness();
            return bussiness.InsertUseLogInHistory(userName,ipAddress,mac);
        }

        private bool IsValidAccount()
        {
            MembershipUser usrInfo = Membership.GetUser(txtUserName.Text);
            if (usrInfo == null)
            {
                CMsgBar.ShowError("Unknown user", pnlMsg, lblMsg);
                return false;
            }

            int passwordAttemptLockoutDuration = Convert.ToInt32(WebConfigurationManager.AppSettings["PasswordAttemptLockoutDuration"]);
            if (usrInfo.IsLockedOut)
            {
                CMsgBar.ShowError("Your account has been locked out because of too many invalid login attempts. Please wait " + passwordAttemptLockoutDuration + " minutes and try again.", pnlMsg, lblMsg);
                return false;
            }
            if (!usrInfo.IsApproved)
            {
                CMsgBar.ShowError("Your account has not yet been approved.", pnlMsg, lblMsg);
                return false;
            }

            int passwordExpiryDuration = Convert.ToInt32(WebConfigurationManager.AppSettings["PasswordExpiryDuration"]);

            if (usrInfo.LastPasswordChangedDate.ToUniversalTime().AddDays(passwordExpiryDuration) <= DateTime.UtcNow)
            {

                Response.Redirect("~/ChangePassword");
            }
            if (txtPassword.Text.Trim() == "Ch@ngeP@55w0rd")
            {
                Session["IsNewUser"] = true;
                Response.Redirect("~/ChangePassword");
            }
            if (usrInfo.IsLockedOut && usrInfo.LastLockoutDate.ToUniversalTime().AddMinutes(passwordAttemptLockoutDuration) < DateTime.UtcNow)
            {
                usrInfo.UnlockUser();
            }
            return true;
        }

        private string GetMacAddress()
        {
            const int MIN_MAC_ADDR_LENGTH = 12;
            string macAddress = string.Empty;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                //log.Debug(
                //    "Found MAC Address: " + nic.GetPhysicalAddress() +
                //    " Type: " + nic.NetworkInterfaceType);

                string tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed &&
                    !string.IsNullOrEmpty(tempMac) &&
                    tempMac.Length >= MIN_MAC_ADDR_LENGTH)
                {
                    //log.Debug("New Max Speed = " + nic.Speed + ", MAC: " + tempMac);
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }

            return macAddress;
        }

        public  string GetMacAddress(string ipAddress)
        {
           return MAC.NativeMethods.GetMacAddress(ipAddress);
        }
        private string GetIp()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}