﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS
{
    public partial class Login : System.Web.UI.Page
    {
        private static void UpdateUserInfo(MembershipUser usrInfo)
        {
            Membership.UpdateUser(usrInfo);
        }
        private System.Web.UI.WebControls.Login GetLogin1()
        {
            System.Web.UI.WebControls.Login Login1 = loginBox.FindControl("Login1") as System.Web.UI.WebControls.Login;
            return Login1;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMsg.Visible = false;
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {

                    System.Web.UI.WebControls.Login Login1 = GetLogin1();
                    TextBox UserName = Login1.FindControl("UserName") as TextBox;
                    UserName.Focus();

                }

                //lblExpiryMsg.Text = string.Format("Your password must be changed at leaset every <strong>{0}</strong> days", GetExpiryDays());
            }
        }
        private int GetExpiryDays()
        {
            int intExpiryDays; try
            {
                intExpiryDays = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpiryDuration"]);
            }
            catch
            {
                intExpiryDays = 15;
            }
            return intExpiryDays;
        }
        private bool DoValidate()
        {
            System.Web.UI.WebControls.Login Login1 = GetLogin1();
            string loginUsername = Login1.UserName;
            string loginPassword = Login1.Password;
            if (loginUsername.Trim().Length == 0)
            {
                CMsgBar.ShowError("User name can not be empty!", pnlMsg, lblError);
                return false;
            }
            if (loginPassword.Trim().Length == 0)
            {
                CMsgBar.ShowError("Passoword can not be empty!", pnlMsg, lblError);
                return false;
            }
            return true;
        }
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {

            System.Web.UI.WebControls.Login Login1 = GetLogin1();
            string loginUsername = Login1.UserName;
            string loginPassword = Login1.Password;

            //Temp
            //MembershipCreateStatus createStatus;
            //MembershipUser newUser = Membership.CreateUser("Test234", "p@55w0rd", "ee@e.com", "A", "B", true, out createStatus);



            //MembershipUser usr = Membership.GetUser(loginUsername);
            //string strPWD = usr.GetPassword();
            //if (strPWD == "Cu5t0rP@55w0rd")
            //    Response.Redirect("Password.aspx?U=" + loginUsername);

            if (Membership.ValidateUser(loginUsername, loginPassword))// "59ce1189-d7c8-47d2-ab83-a55afce26bcd"))
            {
                e.Authenticated = true;
            }
            else
            {
                CMsgBar.ShowError("User Name and/or Password did not match!", pnlMsg, lblError);
                e.Authenticated = false;


            }
            //e.Authenticated = true;
            //}
        }
        protected void Login1_LoginError(object sender, EventArgs e)
        {
            if (!DoValidate()) return;
            System.Web.UI.WebControls.Login Login1 = GetLogin1();
            MembershipUser usrInfo = Membership.GetUser(Login1.UserName);
            if (usrInfo == null)
                return;
            int passwordAttemptLockoutDuration = Convert.ToInt32(WebConfigurationManager.AppSettings["PasswordResetDuration"]);
            // if user is locked out
            if (usrInfo.IsLockedOut)
            {
                CMsgBar.ShowError("Your account has been locked out because of too many invalid login attempts. Please wait " + passwordAttemptLockoutDuration + " minutes and try again.", pnlMsg, lblError);
            }

            int passwordResetDuration = Convert.ToInt32(WebConfigurationManager.AppSettings["PasswordResetDuration"]);
            // if password is not expired but the account is not approved
            if (usrInfo.IsApproved || usrInfo.LastPasswordChangedDate.ToUniversalTime().AddDays(passwordResetDuration) <= DateTime.UtcNow)
                return;
            CMsgBar.ShowError("Your account has not yet been approved.", pnlMsg, lblError);
        }

        protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            System.Web.UI.WebControls.Login Login1 = GetLogin1();
            MembershipUser usrInfo = Membership.GetUser(Login1.UserName, false);
            int passwordAttemptLockoutDuration = Convert.ToInt32(WebConfigurationManager.AppSettings["PasswordResetDuration"]);
            if (usrInfo != null && usrInfo.IsLockedOut && usrInfo.LastLockoutDate.ToUniversalTime().AddMinutes(passwordAttemptLockoutDuration) < DateTime.UtcNow)
            {
                usrInfo.UnlockUser();
            }
            return;
           
        }
    }
}