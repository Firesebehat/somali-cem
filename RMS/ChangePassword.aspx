﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Root.master"AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ENAPortal.ChangePassword" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
    <link rel="stylesheet" type="text/css" href='<%# ResolveUrl("~/Content/SignInRegister.css") %>' />
    <script type="text/javascript" src='<%# ResolveUrl("~/Content/SignInRegister.js") %>'></script>
     <script type="text/javascript" src='<%# ResolveUrl("~/PasswordPolicy/jquery.password-strength.js") %>'></script>
        <script type="text/javascript">
            $(document).ready(function () {

                var myPlugin = $("#NewPassword").password_strength();
                $("[id$='ChangePasswordPushButton']").click(function () {
                    return myPlugin.metReq();
                });
                $("[id$='passwordPolicy']").click(function (event) {
                    var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
                        top = (screen.height / 2) - (height / 2);
                    window.open("PasswordPolicy/PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
                    event.preventDefault();
                    return false;
                });
            });

        </script>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="PageContent" runat="server">
    <style>
        .formLayout-verticalAlign {
            display: block !important;
            margin-top: 50px;
        }
    </style>
   
    <div class="formLayout-verticalAlign">
        <div class="formLayout-container">
                    <div class="form-row" runat="server" id="pnlMsg">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
               </div>
            
              <dx:ASPxPanel runat="server" ID="ASPxPanel1">
                <PanelCollection>
                    <dx:PanelContent>
                        <h4 class="card-title font-weight-bold flex-grow-1;" style="background-color: #009688; color: white; text-align: left; padding: 5px; padding-top: 10px">Change Password</h4>
                        <div style="text-align: right;float:right;margin-right:10px;color:red">
                            <a id="passwordPolicy"href="#">
                                <asp:Label ID="Label2" runat="server" Text="Show Password Policy"  ForeColor="OrangeRed" Font-Size="Larger" Font-Underline="true"></asp:Label>
                            </a>
                        </div>
            <dx:ASPxFormLayout  runat="server" ID="FormLayout" ClientInstanceName="formLayout" CssClass="formLayout" UseDefaultPaddings="false" style="overflow:visible !important">
                <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" />
                <SettingsItemCaptions Location="Top" />
                <Styles LayoutGroup-Cell-Paddings-Padding="0" LayoutItem-Paddings-PaddingBottom="8" />
                <Items>
                    <dx:LayoutGroup ShowCaption="False" GroupBoxDecoration="None" Paddings-Padding="16">
                        <Items>
                             <dx:LayoutItem Caption="Current Password">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxButtonEdit ID="CurrentPasswordButtonEdit" runat="server" ClientInstanceName="CurrentPasswordButtonEdit" Width="100%" Password="true" ClearButton-DisplayMode="Never">
                                            <ButtonStyle Border-BorderWidth="0" Width="6" CssClass="eye-button" HoverStyle-BackColor="Transparent" PressedStyle-BackColor="Transparent">
                                            </ButtonStyle>
                                            <ButtonTemplate>
                                                <div></div>
                                            </ButtonTemplate>
                                            <Buttons>
                                                <dx:EditButton>
                                                </dx:EditButton>
                                            </Buttons>
                                            <ValidationSettings Display="Dynamic" SetFocusOnError="true" ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText">
                                                <RequiredField IsRequired="true" ErrorText="Current password is required" />
                                            </ValidationSettings>
                                            <ClientSideEvents ButtonClick="onPasswordButtonEditButtonClick" Validation="onPasswordValidation" />
                                        </dx:ASPxButtonEdit>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Password">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxButtonEdit ID="PasswordButtonEdit"  ClientInstanceName="passwordButtonEdit"  runat="server" ClientIDMode="Static"  Width="100%" Password="true" >
                                            <ButtonStyle Border-BorderWidth="0" Width="6" CssClass="eye-button" HoverStyle-BackColor="Transparent" PressedStyle-BackColor="Transparent">
                                            </ButtonStyle>
                                            <ButtonTemplate>
                                                <div></div>
                                                
                                            </ButtonTemplate>
                                            <Buttons>
                                                <dx:EditButton>
                                                </dx:EditButton>
                                            </Buttons>
                                             
                                            <ValidationSettings Display="Dynamic" SetFocusOnError="true" ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText">
                                                <RequiredField IsRequired="true" ErrorText="Password is required" />
                                                <RegularExpression ValidationExpression="" />
                                            </ValidationSettings>
                                            <%--<ClientSideEvents TextChanged="function(s){document.getElementById('#NewPassword').text = s.GetText();}" />--%>
                                            <%--<ClientSideEvents ButtonClick="onPasswordButtonEditButtonClick" Validation="onPasswordValidation" />--%>
                                        </dx:ASPxButtonEdit>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Confirm password">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxButtonEdit ID="ConfirmPasswordButtonEdit"  runat="server" ClientInstanceName="confirmPasswordButtonEdit" Width="100%" Password="true" ClearButton-DisplayMode="Never">
                                            <ButtonStyle Border-BorderWidth="0" Width="6" CssClass="eye-button" HoverStyle-BackColor="Transparent" PressedStyle-BackColor="Transparent">
                                            </ButtonStyle>
                                            <ButtonTemplate>
                                                <div></div>
                                            </ButtonTemplate>
                                            <Buttons>
                                                <dx:EditButton>
                                                </dx:EditButton>
                                            </Buttons>
                                            <ValidationSettings Display="Dynamic" SetFocusOnError="true" ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText">
                                                <RequiredField IsRequired="true" ErrorText="Confirm your password" />
                                            </ValidationSettings>
                                            <ClientSideEvents ButtonClick="onPasswordButtonEditButtonClick" Validation="onPasswordValidation" />
                                        </dx:ASPxButtonEdit>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                    <dx:LayoutGroup GroupBoxDecoration="HeadingLine" ShowCaption="False"   > 
                        <Paddings PaddingTop="13" PaddingBottom="13" />
                        <GroupBoxStyle CssClass="formLayout-groupBox" />
                        <Items>
                            <dx:LayoutItem ShowCaption="False" HorizontalAlign="Center" Paddings-Padding="0">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxButton ID="btnSubmit" runat="server" Width="100" Text="Change" OnClick="btnChange_Click" ></dx:ASPxButton>
                                        <dx:ASPxButton ID="btnCancelChange" runat="server" Width="100" Text="Cancel" OnClick="btnCancel_Click1" BackColor="Silver" CausesValidation="false" ></dx:ASPxButton>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                  
                </Items>
            </dx:ASPxFormLayout>

                        </dx:PanelContent>
                    </PanelCollection>
                  </dx:ASPxPanel>
            <div style="display:table;text-align:right">
            <dx:ASPxPanel runat="server" ID="pnlSuccess" Visible="false">
                <Paddings  Padding="10"/>
                <PanelCollection>
                    <dx:PanelContent>

                        <dx:ASPxButton ID="btnContinue" runat="server"  Text="Continue Login" OnClick="btnContinue_Click1" ></dx:ASPxButton>
                         <dx:ASPxButton ID="btnCancel" runat="server"  Text="Log out"  OnClick="btnCancel_Click" ></dx:ASPxButton>
                    </dx:PanelContent>
                </PanelCollection>

            </dx:ASPxPanel>
                </div>
        </div>
    </div>
      

    </asp:Content>