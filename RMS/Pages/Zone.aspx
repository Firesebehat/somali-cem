﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Zone.aspx.cs" Inherits="RMS.Pages.Zone" MasterPageFile="~/Root.master" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
            history.pushState(null, null, document.URL);
            window.addEventListener('popstate', function () {
                this.history.pushState(null, null, document.URL);
            });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
     <script type="text/javascript">
         function DoNewClientClick(s, e) {

             clbClient.PerformCallback('New');
             
         }

         function DoSaveClientClick() {
             if (!ASPxClientEdit.ValidateGroup('Registration')) {
                 return;
             }

             clbClient.PerformCallback('Save');
             //}
         }

         function DoSearchClientClick(s, e) {
             clbClient.PerformCallback('Search');
         }

         function DoCancelClient() {
            // clbClient.PerformCallback('Cancel');
             doListMode();
         }

         function DoAddClientClick(s, e) {
             
             DoNewClientClick();
         }
         function ClearForm()
         {
             txtAmharic.SetText("");
             txtEnglish.SetText("");
         }
         function DoListClientClick(s, e) {
             pnlData.SetVisible(false);
             pnlGrid.SetVisible(true);
             btnNewClient.SetEnabled(true);
             btnListClient.SetEnabled(false);
           //  cboRegionFind.SetValue("-1");
             gvZone.PerformCallback();
             //DoSearchClientClick();
         }
         function DoDeleteImageClick() {
             ShowPopup('deleteimage');
         }
         function Show_Click() {
            
             if (cboRegionFind.GetValue() === null || cboRegionFind.GetValue().toString() === '-1') {
                 ShowError("Please select Region");
                 return;
             }
             gvZone.PerformCallback();
         }
         var rowVisibleIndex;
         var strID;
         function gvZone_CustomButtonClick(s, e) {
             
             if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
             if (e.buttonID == 'DELETING') {
                 rowVisibleIndex = e.visibleIndex;
                 s.GetRowValues(e.visibleIndex, 'Code', DeleteZone);
                 //ShowError("test");
             }
             if (e.buttonID == 'EDITING') {

                 gvZone.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
                 btnNewClient.SetEnabled(false);
                 btnListClient.SetEnabled(true);
             }
         }
         function DeleteZone(values) {
             ShowConfirmx("Are you sure? Do you want to delete this record", function (result) {
                 if (result) {
                     btnSaveClient.SetEnabled(true);
                     clbClient.PerformCallback('Delete' + '|' + values);
                 }
             })
         }
         function DoDoubleClick(s, e) {
             gvZone.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
             btnNewClient.SetEnabled(false);
             btnListClient.SetEnabled(true);
         }
         function OnGetSelectedFieldValues(values) {

             pnlData.SetVisible(true);
             btnSaveClient.SetEnabled(true);
             pnlGrid.SetVisible(false);
             btnNewClient.SetEnabled(false);
             btnListClient.SetEnabled(true);
             //alert('e.buttonID');
             clbClient.PerformCallback('Edit' + '|' + values);


         }

         function ShowPopup(rowId) {
             doListMode();
             popupControl.Show();
             btnYes.Focus();
         }
         function doListMode()
         {
                     pnlData.SetVisible(false);
                     pnlGrid.SetVisible(true);
                     btnListClient.SetEnabled(false);
                     btnNewClient.SetEnabled(true);
         }
         function OnClientEndCallback(s, e) {

             if (s.cpStatus == "SUCCESS") {
                 if (s.cpAction == "Cancel") {
                     pnlGrid.SetVisible(false);
                     return;
                 }
                else if (s.cpAction == "Delete") {
                     ShowSuccess(s.cpMessage);
                     doListMode();
                     return;
                 }
                 else if (s.cpAction == "Find") {
                     pnlGrid.SetVisible(false);
                     pnlData.SetVisible(true);
                 }
                 else if (s.cpAction == "List") {
                     doListMode();
                 }
                else if (s.cpAction == "Save") {
                     ShowSuccess(s.cpMessage);
                     doListMode();
                 }
               else  if (s.cpAction == "new") {
                     pnlData.SetVisible(true);
                     pnlGrid.SetVisible(false);
                     btnNewClient.SetEnabled(false);
                     btnListClient.SetEnabled(true);
                     ClearForm();
                 }
                 return;
             }
             else if (s.cpStatus == "ERROR") {
                 pnlData.SetVisible(true);
                 pnlGrid.SetVisible(false);
                 ShowError(s.cpMessage);
                 if (s.cpAction == "Delete" || s.cpAction == "Find" || s.cpAction == "List") {
                     doListView();
                     return;
                 }

                 if (s.cpAction == "Save") {
                     doEditeView();
                 }

             }
            

         }
         function gvZonEndCallBack() {
             if (gvZone.cpStatus == "SUCCESS") {
                 if (gvZone.cpAction == "List") {
                     gvZone.SetVisible(true);
                 }
                 else gvZone.SetVisible(false);
             }

         }
         function btnYes_Click(s, e) {

             ClosePopup(true);
         }

         function btnNo_Click(s, e) {
             ClosePopup(false);
         }

         function ClosePopup(result) {
             popupControl.Hide();
             if (result) {
                 gvZone.GetRowValues(rowVisibleIndex, "Code", OnGetSelectedDeleteField);
             }

         }

         function OnGetSelectedDeleteField(values) {
             btnSaveClient.SetEnabled(true);
             clbClient.PerformCallback('Delete' + '|' + values);
         }

         function DeleteGridRow(visibleIndex) {
             var index = gvZone.GetFocusedRowIndex();

             gvZone.DeleteRow(index);
         }

         function doListMode() {
             pnlData.SetVisible(false);
             pnlGrid.SetVisible(true);
             gvZone.SetVisible(true);
             btnNewClient.SetEnabled(true);
         }
     </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            DoListClientClick();
        };
    </script>
    <style>
        td{
            vertical-align:top;
        }
        .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
    </style>
   
    <div class="contentmain">
        <h5>
            Zone Maintenance
        </h5>
        <div class="card card-body">
        <script src="../Content/ModalDialog.js"></script>
        <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
       
                <div style="padding-bottom:15px">
                                            <dx:ASPxButton ID="btnZoneNewClick" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient" Font-Size="Small"
                                                HorizontalAlign="Center" Image-Url="~/Images/Toolbar/Add.png" Text="New"  VerticalAlign="Middle" Font-Names="Visual Geez Unicode">
                                                <ClientSideEvents Click="DoAddClientClick" />
                                            </dx:ASPxButton>
                                       
                                             <dx:ASPxButton ID="btnList" runat="server" AutoPostBack="False" ClientInstanceName="btnListClient" Font-Size="Small"
                                                HorizontalAlign="Center" Image-Url="~/Images/Toolbar/table.png" Text="Show List"  VerticalAlign="Middle" Font-Names="Visual Geez Unicode">
                                                <ClientSideEvents Click="DoListClientClick" />
                                            </dx:ASPxButton>

                                        </div>

        
                 <dx:ASPxCallbackPanel ID="clbClient" OnCallback="ClbClient_Callback" ClientInstanceName="clbClient" runat="server" style="max-width:700px"  Width="100%">
                    <ClientSideEvents EndCallback="OnClientEndCallback" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlWoreda" runat="server"   SupportsDisabledAttribute="True">
                            <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" ShowHeader="false" Width="100%" ClientVisible="false">
                            <PanelCollection>
                                <dx:PanelContent>
                                   <%-- Data Entry Table--%>
                                  <table style="width:100%">
                                           
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:Label ID="lblZone" runat="server" Text="Parent Region:"></asp:Label>
                                                    <asp:Label ID="codeStar0" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion" Width="300px">
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Label ID="lblEnglish" runat="server" Text="Description (English):"></asp:Label>
                                                    <asp:Label ID="EnglishStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td >
                                                    <dx:ASPxTextBox ID="txtEnglish" runat="server" Width="300px" ClientInstanceName="txtEnglish">
                                                          <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter English name." IsRequired="True" />
                                                       <RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />
                                                            </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Label ID="lblAmharic" runat="server" Text="Description (Amharic):"></asp:Label>
                                                    <asp:Label ID="AmharicStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td class="auto-style6" >
                                                    <dx:ASPxTextBox ID="txtAmharic" runat="server" Width="300px" ClientInstanceName="txtAmharic">
                                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />
                                                            <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td class="auto-style6"> 
                                                <table  style="margin-left:280px;">
                                                   <tr>
                                                       <td>
                                                           <dx:ASPxButton ID="btnZoneSaveClick" runat="server" CausesValidation="true" AutoPostBack="False" ClientInstanceName="btnSaveClient" 
                                                                Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="Save"  VerticalAlign="Middle">
                                                               <ClientSideEvents Click="DoSaveClientClick" />
                                                           </dx:ASPxButton>
                                                       </td>
                                                       <td>
                                                           <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" ClientInstanceName="btnCancelClient" Text="Cancel">
                                                               <ClientSideEvents Click="DoCancelClient" />
                                                           </dx:ASPxButton>
                                                       </td>
                                                   </tr>
                                                </table>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                              
                                <  <%-- Data Entry Table--%>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel>

                            
                             <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="700px" ClientVisible="false" >
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <%--Gridview table--%>
                                           <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <div style="width:100%;"  class="RoundPanel">
                                                            <table style="width:70%;">
                                                                <tr>
                                                                     <td style="max-width:80px" >
                                                                         <asp:Label ID="lblLang" runat="server" Text="Region"></asp:Label>
                                                                     </td>
                                                                
                                                                
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboRegionFind" ClientInstanceName="cboRegionFind" runat="server" Width="100%"></dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnFind" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="Show" VerticalAlign="Middle">
                                                                            <ClientSideEvents Click="Show_Click" />
                                                                            <Image Url="~/Controls/ToolbarImages/search.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                  </tr>
                                              <tr>
                                                <td>
                                                    <dx:ASPxGridView ID="gvZone" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvZone" EnableTheming="True"  
                                                         KeyFieldName="Code"  Visible="true" Width="700" OnPageIndexChanged="gvZone_PageIndexChanged" OnCustomCallback="Grd_CustomCallback">
                                                        <ClientSideEvents RowDblClick="DoDoubleClick" />
                                                        <ClientSideEvents CustomButtonClick="gvZone_CustomButtonClick" EndCallback="gvZonEndCallBack"  />

<SettingsPopup>
<HeaderFilter MinHeight="140px"></HeaderFilter>
</SettingsPopup>
                                                        <SettingsSearchPanel Visible="True" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Visible="false"  VisibleIndex="1">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Amharic" FieldName="AmDescription" ShowInCustomizationForm="True" VisibleIndex="2">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="English" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="3">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="9" Width="100px" ShowClearFilterButton="True">
                                                                <CustomButtons>
                                                                    <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Select">
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                    <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                </CustomButtons>
                                                            </dx:GridViewCommandColumn>
                                                        </Columns>
                                                        <Settings ShowFilterRow="True" />
                                                        <SettingsBehavior AllowFocusedRow="True" />
                                                        <SettingsPager PageSize="20">
                                                        </SettingsPager>

<Styles AdaptiveDetailButtonWidth="22"></Styles>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--Gridview table--%>
                                    </dx:PanelContent>
                                </PanelCollection>
                             </dx:ASPxPanel>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
              
    </div>
        </div>
</asp:Content>

