﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Roles.aspx.cs" Inherits="RMS.Pages.Roles" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <style>
        td {
            vertical-align: top;
        }
    </style>
    <script type="text/javascript">
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", false);
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                clbClient.PerformCallback('New');

            }
            else if (e.item.name === "mnuSave") {
                clbClient.PerformCallback('Save');
            }
            else if (e.item.name === "mnuList") {
                doListMode();
                grdRole.PerformCallback('Bind');
            }

        }
        function GetMenuItem(mnuName) {
            return mnuToolbar.GetItemByName(mnuName);
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function onDeleteRole(s, e) {
            ShowConfirmx('Are you sure? Do you want to delete this role?', function (result) {
                if (result) {
                    s.GetRowValues(e.visibleIndex, "RoleName", DeleteRole);
                }
            }, 'en');
        }
        function onEditRole(s, e) {
            s.GetRowValues(e.visibleIndex, "RoleName", EditRole);
        }
        function EditRole(roleName) {
            clbClient.PerformCallback('Edit|' + roleName);
        }
        function DeleteRole(values) {
            grdRole.PerformCallback('Delete|' + values);
        }
        function onAddRole() {
            if (!ASPxClientEdit.ValidateGroup('VGRole')) {
                return;
            }
            clbClient.PerformCallback('Save');

        }
        function onEndCallback(s) {
            if (s.cpStatus === 'SUCCESS') {
                if (s.cpMessage !== '')
                    ShowSuccess(s.cpMessage);
                txtRole.SetText('');
            }
            else if (s.cpStatus === 'ERROR') {
                ShowError(s.cpMessage)
            }
            s.cpStatus = '';
        }
        function OnClientEndCallback(s, e) {

            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Save") {
                    ShowSuccess(s.cpMessage);
                    doListMode();
                    grdRole.PerformCallback('Bind');
                    s.cpMessage = "";
                    return;
                }
                else if (s.cpAction === "New") {
                    doEditMode();
                }
                else if (s.cpAction === "Edit") {
                    doEditMode();
                }
                else if (s.cpAction === "Delete") {
                    doListMode();
                    ShowSuccess(s.cpMessage);
                    grdRole.PerformCallback('Bind');
                    return;
                }
                s.cpAction = s.cpStatus = '';
            }

            else if (s.cpStatus === "ERROR") {
                doEditMode();
                if (s.cpMessage !== '')
                    ShowError(s.cpMessage);
            }
            else if (s.cpStatus === "NOTFOUND") {
                if (s.cpAction === "List") {
                    ShowError('No Record is found');
                    s.cpAction = "";
                }
            }
        }
        function doListMode() {
            $('#divForms').hide(100);
            $('#divData').show(100);
            ToggleEnabled("mnuSave", false);
        }
        function doEditMode() {
            $('#divForms').show(100);
            $('#divData').hide(100);
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuList", true);

        }
    </script>

    <div class="contentmain">
        <h5>Role Maintenance
        </h5>
        <div class="card card-body">
            <script src="../Content/ModalDialog.js"></script>
            <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />
            <div style="padding-bottom:10px">
                <dx:ASPxMenu ID="mnuToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                    ShowPopOutImages="True" EnableTheming="True" Height="35px"
                    ClientInstanceName="mnuToolbar" Font-Names="Visual Geez Unicode"
                    AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                    <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                    <Items>
                        <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                            <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                            <Image IconID="iconbuilder_actions_add_svg_16x16">
                            </Image>
                        </dx:MenuItem>
                        <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                            <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                            <Image IconID="save_save_svg_16x16">
                            </Image>
                        </dx:MenuItem>

                        <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                            <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                            <Image IconID="businessobjects_bo_appearance_svg_16x16">
                            </Image>
                        </dx:MenuItem>
                    </Items>
                </dx:ASPxMenu>
            </div>
            <div style="max-width: 800px">
                <dx:ASPxCallbackPanel runat="server" ID="clbClient" ClientInstanceName="clbClient" OnCallback="clbClient_Callback">
                    <ClientSideEvents EndCallback="OnClientEndCallback" />
                    <PanelCollection>
                        <dx:PanelContent>
                            <div id="divForms" style="display:none">
                                <table style="width: 100%; margin-bottom: 5px">
                                    <tr>
                                        <td>Role Name:</td>
                                        <td style="width: 80%">
                                            <dx:ASPxTextBox runat="server" ID="txtRole" ClientInstanceName="txtRole" Width="250px" MaxLength="50">
                                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="VGRole" ErrorTextPosition="Bottom" Display="Dynamic">
                                                    <RequiredField ErrorText="Please enter Role name." IsRequired="True" />
                                                    <RegularExpression ErrorText="Only Alphabetic Characters are Allowed" ValidationExpression="^[a-zA-Z ]*$" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>
                                            <dx:ASPxLabel runat="server" Text="Role Description">

                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxMemo ID="txtRoleDescription" runat="server" Height="60px" Width="100%">
                                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="VGRole" ErrorTextPosition="Bottom" Display="Dynamic">
                                                    <RequiredField ErrorText="Please enter Role name." IsRequired="false" />
                                                    <RegularExpression ErrorText="Only Alphabetic letters or numbers are Allowed" ValidationExpression="^[a-zA-Z0-9]*$" />
                                                </ValidationSettings>
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
                <div id="divData">
                    <dx:ASPxGridView ID="grdRole" ClientInstanceName="grdRole" runat="server" AutoGenerateColumns="False" Width="750px"
                        OnCustomCallback="grdRole_CustomCallback" OnPageIndexChanged="grdRole_PageIndexChanged">
                        <ClientSideEvents CustomButtonClick="onEditRole" EndCallback="onEndCallback" />
                        <SettingsPopup>
                            <HeaderFilter MinHeight="140px"></HeaderFilter>
                        </SettingsPopup>
                        <SettingsBehavior AllowSort="false" AllowAutoFilter="false" />
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Role Name" FieldName="RoleName" ShowInCustomizationForm="True" VisibleIndex="0">
                                <CellStyle Wrap="False"></CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="User Count" FieldName="UserCount" ShowInCustomizationForm="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Users" Width="35%" FieldName="UsersOfRole" ShowInCustomizationForm="True" VisibleIndex="1">
                                <CellStyle Wrap="True"></CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="35%" Caption="Description" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1">
                                <CellStyle Wrap="True"></CellStyle>
                            </dx:GridViewDataTextColumn>


                             <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="10">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Select">
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                        </Columns>
                        <SettingsPager PageSize="4"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

