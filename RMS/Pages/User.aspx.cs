﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.DataAccess;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages
{
    public partial class User : System.Web.UI.Page
    {
        public string strDefaultPassword = "Ch@ngeP@55w0rd";
        private string[] selectedRoles;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack && !IsCallback)
                {
                    if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        FormsAuthentication.RedirectToLoginPage();
                    }
                    if (!(HttpContext.Current.User.IsInRole("Administrator")
                         || HttpContext.Current.User.IsInRole("Super Admin")))
                    {
                        Response.Redirect("~/AccessDenied");
                    }
                    //btnAddUser.Visible = btnSave.Visible = HttpContext.Current.User.IsInRole("Super Admin");
                    Session["IsNew"] = true;
                    Session["NextUserNo"] = null;
                    string strCurrentUser = User.Identity.Name;
                    var p = ProfileBase.Create(strCurrentUser);
                    var site = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));
                    Session["CenterId"] = (int)site.GetPropertyValue("TaxCenterId");
                    cboOrg.ClientEnabled = false;
                    FillCombo(cboOrgType, Language.eLanguage.eEnglish, typeof(OrganizationalType.OrgType));
                    cboOrgType.Value = null;
                    //FillCombo(cboUserGroup, Language.eLanguage.eEnglish, typeof(UserGroup.UserGroupTypes));
                    DisplayForm();
                }

            }
            catch
            {

            }
        }
        private void FillRole()
        {
            string strSQL = "Select distinct RoleId,RoleName FROM aspnet_Roles ";
            lstUserRoles.Items.Clear();
            CDropdown.FillCheckBoxListFromDB(lstUserRoles, strSQL, "RoleName", "RoleId");
        }


        private void BindRolesNoAdmins()
        {

            try
            {
                string[] AllRoles = System.Web.Security.Roles.GetAllRoles();
                var list = from r in AllRoles
                           where !r.Contains("Super Admin")
                           && !r.Contains("Administrator")
                           && !r.Contains("Regional Administrator")
                           && !r.Contains("Subscriber Administrator")
                           && !r.Contains("Subscriber User")
                           select r;
                foreach (string x in list)
                {
                    lstUserRoles.Items.Add(x);
                }
            }
            catch (Exception ex)
            {
                ShowError("Error! User has no valid profile!");
            }
        }

        private void ReBindRoles_forOrg(string orgName)
        {

            try
            {
                lstUserRoles.Items.Clear();
                int OrgType = cboOrgType.Value != null ? Convert.ToInt16(cboOrgType.Value) : -1;
                if (HttpContext.Current.User.IsInRole("Super Admin") && OrgType != 0)
                {
                    lstUserRoles.Enabled = true;
                    lstUserRoles.Items.Add("Administrator");
                    lstUserRoles.Items.Add("Super Admin");
                }
                if ((HttpContext.Current.User.IsInRole("Administrator")
                     || HttpContext.Current.User.IsInRole("Super Admin"))
                     && (cboOrgType.Value != null && cboOrgType.Value.ToString() == "0"))//sbscriber administrator
                {
                    lstUserRoles.Enabled = true;
                    lstUserRoles.Items.Add("Subscriber User");
                }
                else
                {
                    BindRolesNoAdmins();
                    List<object> result = lstUserRoles.Items.OfType<object>().ToList();
                    if (orgName.ToLower().Contains("web"))
                    {
                        result = result.Where(p => p.ToString().ToLower().Contains("web")).ToList();
                        result.Add("Digital Asset Manager");
                        result.Add("raw media asset manager");
                        result.Add("Manager");

                    }
                    else
                        result = result.Where(p => !(p.ToString().ToLower().Contains("web"))).ToList();
                    lstUserRoles.DataSource = result;
                    lstUserRoles.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        private void DoNew()
        {
            FillRole();
            string strGUID = Session["CenterId"].ToString();
            if (string.IsNullOrEmpty(strGUID) && !HttpContext.Current.User.IsInRole("Super Admin")) return;
            chkApproved.Checked = true;
            Session["IsNew"] = true;
            ClearForm();
            //lstUserRoles.Items.Clear();
            cboOrg.ClientEnabled = false;
            lblUserName.Visible = false;
            txtUserName.Visible = true;
            cbData.JSProperties["cpAction"] = "New";
            cbData.JSProperties["cpStatus"] = "SUCCESS";
        }

        private void ClearForm()
        {
            txtUserName.Text = string.Empty;
            txtComment.Text = string.Empty;
            txtEMail.Text = string.Empty;
            txtFullName.Text = string.Empty;
            txtLang.Text = string.Empty;
            cboOrg.NullText = "-";
            cboOrg.Value = null;
            cboOrgType.SelectedIndex = -1;
            lstUserRoles.SelectedIndex = -1;
            // cboUserGroup.SelectedIndex = -1;
            lblUserName.Text = string.Empty;
        }

        private void SetProfile(string strUser)
        {
            try
            {
                var p = ProfileBase.Create(strUser);
                if (p != null)
                {
                    var userP = ((ProfileGroupBase)(p.GetProfileGroup("Staff")));
                    var orgP = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));

                    txtFullName.Text = (string)userP.GetPropertyValue("FullName");
                    cboOrgType.Value = Int32.Parse(orgP.GetPropertyValue("Category").ToString());
                    //cboUserGroup.Value = Int32.Parse(userP.GetPropertyValue("UserGroupId").ToString());
                    FillOrg(cboOrgType.Value.ToString());
                    FillRole();
                    cboOrg.Value = Int32.Parse(orgP.GetPropertyValue("TaxCenterId").ToString());
                    cboOrg.ClientEnabled = true;
                }

            }
            catch (Exception ex)
            {
                string strError = ex.Message;
                ShowError(strError);
            }

        }
        private void DisplayForm()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["username"]))
                {
                    rdpData.ClientVisible = true;
                    btnAddUser.ClientEnabled = true;
                    btnSave.ClientEnabled = true;
                    Session["IsNew"] = false;
                    MembershipUser user = Membership.GetUser(Request.QueryString["username"]);
                    lblUserName.Text = user.UserName.ToUpper();
                    txtUserName.Text = user.UserName;
                    lblUserName.Visible = true;
                    txtUserName.Visible = false;
                    txtComment.Text = user.Comment;
                    txtEMail.Text = user.Email == user.UserName + "@a.com" ? string.Empty : user.Email;
                    chkApproved.Checked = user.IsApproved;
                    UserBussiness userBussiness = new UserBussiness();
                    txtMobileNo.Text = userBussiness.GetUserByName(user.UserName).MobileNo;
                    FillRole();
                    SetProfile(Request.QueryString["username"]);

                    //ReBindRoles_forOrg(cboOrg.Text);
                    string[] userRoles = System.Web.Security.Roles.GetRolesForUser(Request.QueryString["username"]);
                    foreach (string role in userRoles)
                    {
                        ListEditItem checkbox = lstUserRoles.Items.FindByText(role);
                        if (checkbox != null)
                            checkbox.Selected = true;
                    }

                }

            }
            catch (Exception ex)
            {
                ShowError("Error! User has no valid profile!");
            }
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append("Please correct the following");
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            if (txtUserName.Text == string.Empty)
                errMessages.Add("Enter the user name!");


            if (txtFullName.Text == string.Empty)
                errMessages.Add("Enter Staff name");
            //if (this.txtEMail.Text == string.Empty || !txtEMail.IsValid)
            //    errMessages.Add("Enter a vailid Email");
            if (txtUserName.Text.Length < 5)
                errMessages.Add("Username must be at least 5 characters long");



            return errMessages;
        }
        private bool HasSelection()
        {
            foreach (ListEditItem item in lstUserRoles.Items)
            {
                if (item.Selected)
                    return true;
            }
            return false;
        }

        protected bool DoSaveUser()
        {
            RestorValues();
            if (!Page.IsValid)
                return false;
            if (!ASPxEdit.ValidateEditorsInContainer(cbData))
                return false;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                ShowError(GetErrorDisplay(errMessages));
                return false;
            }
            if ((bool)Session["IsNew"] && !DoValidate())
            {
                ShowError("The password do not conform to the password policy");
                return false;
            }
            try
            {
                if (selectedRoles.Length == 0)
                {
                    ShowError("You must select at least one role");
                    return false;
                }
                if ((bool)Session["IsNew"])
                {
                    MembershipCreateStatus createStatus = AddUser();
                    if (createStatus != MembershipCreateStatus.Success)
                        return false;
                }
                else
                {
                    UpdateUser();

                }
                chkApproved.ClientVisible = true;
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        private void RestorValues()
        {
            cboOrg.ClientEnabled = true;
            if (cboOrgType.Items.Count == 0)
            {
                FillCombo(cboOrgType, Language.eLanguage.eEnglish, typeof(OrganizationalType.OrgType));
            }
            if (cboOrg.Items.Count == 0)
            {
                FillOrg();
            }
            if (lstUserRoles.Items.Count == 0)
            {
                ReBindRoles_forOrg(cboOrg.Text);
            }
            lblUserName.Visible = false;
            txtUserName.Visible = true;
            if (cboOrg.Value != null)
            {
                ReBindRoles_forOrg(cboOrg.Text);
            }
        }

        private void DoSave()
        {
            try
            {
                if (DoSaveUser())
                {
                    DoUserExtraUpdate();

                    cbData.JSProperties["cpAction"] = "Save";
                    DoNew();
                    Session["NextUserNo"] = null;
                    ShowSuccess("Data saved successfully");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                cbData.JSProperties["cpAction"] = "Save";
            }
        }

        private void UpdateHash()
        {
            //Hash user information
            string user = txtUserName.Text;
            MembershipUser usrInfo = Membership.GetUser(user);
            UserDataAccess objUser = new UserDataAccess();
            HashInfo objHash = new HashInfo();
            CUSTOR.Domain.User users = new CUSTOR.Domain.User();
            users = objUser.GetUserHashCode(user);
            string UserPlainText = user + usrInfo.GetPassword() + users.TaxCenterId;
            var roles = System.Web.Security.Roles.GetRolesForUser(user);
            foreach (string role in roles)
            {
                UserPlainText = UserPlainText + role;
            }
            string CyperText = objHash.ComputeHash(UserPlainText);
            users.UserHash = CyperText;
            users.UserName = user;
            objUser.UpdateHash(users);
        }

        private void DoUserExtraUpdate()
        {
            //update fullname,site,and usergroup info
            UserDataAccess objUserDataAccess = new UserDataAccess();
            CUSTOR.Domain.User user = new CUSTOR.Domain.User();
            //user.UserGroupId = Int16.Parse(cboUserGroup.Value.ToString());
            user.TaxCenterId = Int32.Parse(cboOrg.Value.ToString());
            user.FullName = txtFullName.Text;
            user.UserName = txtUserName.Text;
            user.MobileNo = txtMobileNo.Text;
            objUserDataAccess.UpdateUser(user);
            //update profile
            DoSaveProfile(user);
        }

        private void DoSaveProfile(CUSTOR.Domain.User user)
        {
            int taxCenterId = Convert.ToInt32(cboOrg.Value);
            TaxCenterBussiness TaxCenterBis = new TaxCenterBussiness();
            CUSTOR.Domain.TaxCenter objTaxCenter = TaxCenterBis.GetTaxCenter(taxCenterId);
            var p = ProfileBase.Create(user.UserName);
            var userP = ((ProfileGroupBase)(p.GetProfileGroup("Staff")));
            userP.SetPropertyValue("FullName", user.FullName);
            userP.SetPropertyValue("UserGroupId", Int16.Parse(user.UserGroupId.ToString()));
            userP.SetPropertyValue("UserId", Membership.GetUser(user.UserName).ProviderUserKey.ToString());
            p.Save();

            var OrgP = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));
            OrgP.SetPropertyValue("NameEng", objTaxCenter.CenterNameEnglish);
            OrgP.SetPropertyValue("Name", objTaxCenter.CenterName);
            OrgP.SetPropertyValue("TaxCenterId", objTaxCenter.Id);
            OrgP.SetPropertyValue("Category", Int16.Parse(objTaxCenter.OrgType.ToString()));
            OrgP.SetPropertyValue("LanguageID", Int16.Parse(objTaxCenter.Language.ToString()));
            OrgP.SetPropertyValue("Region", objTaxCenter.Region);
            OrgP.SetPropertyValue("Zone", objTaxCenter.Zone);
            OrgP.SetPropertyValue("Wereda", objTaxCenter.Woreda);
            OrgP.SetPropertyValue("Kebele", objTaxCenter.Kebele);
            OrgP.SetPropertyValue("IsActive", objTaxCenter.IsActive);
            p.Save();
        }

        private void UpdateRoles()
        {
            FillRole();
            foreach (ListEditItem itmList in lstUserRoles.Items)
            {
                if (selectedRoles.Contains(itmList.Text))
                {
                    if (!System.Web.Security.Roles.IsUserInRole(txtUserName.Text, itmList.Text))
                        System.Web.Security.Roles.AddUserToRole(txtUserName.Text, itmList.Text);
                }
                else
                    if (System.Web.Security.Roles.IsUserInRole(txtUserName.Text, itmList.Text))
                    System.Web.Security.Roles.RemoveUserFromRole(txtUserName.Text, itmList.Text);
            }
        }
        private void UpdateUser()
        {

            string strUserName = Request.QueryString["username"];
            if (strUserName != null || strUserName != string.Empty)
            {
                MembershipUser user = Membership.GetUser(strUserName);
                user.Comment = txtComment.Text;
                user.Email = txtEMail.Text == string.Empty ? strUserName + "@a.com" : txtEMail.Text;
                user.IsApproved = (chkApproved.Checked);

                Membership.UpdateUser(user);
                UpdateRoles();
                Guid currentUserId = (Guid)user.ProviderUserKey;

                ShowMessage("Update Successful.");
            }

        }
        private MembershipCreateStatus AddUser()
        {
            try
            {
                Page.Validate();
                if (!Page.IsValid) return MembershipCreateStatus.UserRejected;
                MembershipCreateStatus createStatus;
                //MembershipUser newUser = Membership.CreateUser(txtUserName.Text, txtPassword.Text, txtEMail.Text, "A", "B", true, out createStatus);
                string email = txtEMail.Text == string.Empty ? txtUserName.Text + "@a.com" : txtEMail.Text;
                MembershipUser newUser = Membership.CreateUser(txtUserName.Text, strDefaultPassword, email, "A", "B", true, out createStatus);
                if (createStatus == MembershipCreateStatus.Success)
                {

                    newUser.Comment = txtComment.Text;
                    Membership.UpdateUser(newUser);
                    Guid currentUserId = (Guid)newUser.ProviderUserKey;
                    //InsertUserSite(currentUserId, cboSite.SelectedItem.Text);
                    AddRoles();
                }
                switch (createStatus)
                {
                    case MembershipCreateStatus.Success:
                        ShowSuccess("The user account was successfully created!");
                        break;
                    case MembershipCreateStatus.DuplicateUserName:
                        ShowError("There already exists a user with this username.");
                        break;
                    case MembershipCreateStatus.DuplicateEmail:
                        ShowError("There already exists a user with this email address.");
                        break;
                    case MembershipCreateStatus.InvalidAnswer:
                        ShowError("There security answer was invalid.");
                        break;
                    default:
                        ShowError("There was an unknown error; the user account was NOT created.");
                        break;
                }

                return createStatus;
            }
            catch (Exception ex)
            {
                return MembershipCreateStatus.ProviderError;
            }
        }

        private void AddRoles()
        {

            for (int i = 0; i < selectedRoles.Length; i++)
            {
                System.Web.Security.Roles.AddUserToRole(txtUserName.Text, selectedRoles[i]);
            }
        }
        private bool DoValidate()
        {
            return true;
        }

        private void ShowSuccess(string strMsg)
        {
            cbData.JSProperties["cpMessage"] = strMsg;
            cbData.JSProperties["cpStatus"] = "SUCCESS";
        }
        private void ShowError(string strMsg)
        {
            cbData.JSProperties["cpMessage"] = strMsg;
            cbData.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowMessage(string strMsg)
        {
            cbData.JSProperties["cpMessage"] = strMsg;
            cbData.JSProperties["cpStatus"] = "INFO";
        }



        protected void CallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains("|"))
                strID = parameters[1].ToString();

            switch (strParam)
            {
                case "New":
                    DoNew();
                    Session["IsNew"] = true;
                    break;
                case "Save":
                    selectedRoles = strID.Split(',');
                    DoSave();
                    break;
                case "FillOrg":
                    if (cboOrgType.Value != null)
                        FillOrg(cboOrgType.Value.ToString());
                    // FillRole();
                    break;

                default:
                    break;
            }

        }

        private void FillCombo(ASPxComboBox cboName, Language.eLanguage eLang, Type t)
        {
            cboName.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            cboName.TextField = "Value";
            cboName.ValueField = "Key";
            cboName.DataBind();
            cboName.SelectedIndex = -1;
        }

        protected void cboOrg_Callback(object sender, CallbackEventArgsBase e)
        {
            FillOrg(e.Parameter);
        }

        protected void clbRoleList_Callback(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter == "CLEAR")
            {
                lstUserRoles.UnselectAll();
            }
        }

        protected void cboUnit_Performcallback(object sender, CallbackEventArgsBase e)
        {

        }
        private void FillOrg(string strOrgType)
        {

            string strSQL = "Select Id,CenterNameEnglish FROM TaxCenter where IsDeleted <> 1 and IsActive=1";
            if (strOrgType != string.Empty)
                strSQL += " and OrgType='" + strOrgType + "'";
            CDropdown.FillComboFromDB(cboOrg, strSQL, "CenterNameEnglish", "Id");
            cboOrg.Value = null;
        }

        private void FillOrg()
        {
            string strSQL = string.Empty;

            strSQL = "Select Id,CenterNameEnglish FROM TaxCenter where IsDeleted <> 1 and IsActive=1 and (Parent='0' OR OrgType=1) order by CenterNameEnglish ";
            CDropdown.FillComboFromDB(cboOrg, strSQL, "CenterNameEnglish", "Id");
        }

        protected void lstUserRoles_Init(object sender, EventArgs e)
        {
            if (IsCallback)
                FillRole();
        }
        private int GetNextNo()
        {
            if (Session["NextUserNo"] == null)
            {
                UserBussiness bussiness = new UserBussiness();
                int nextNo = bussiness.GetNextUserNo();
                Session["NextUserNo"] = nextNo;
                return nextNo;
            }
            else
            {
                return Convert.ToInt32(Session["NextUserNo"]);
            }
        }
        public void SuggestName(string fullName)
        {
            if ((bool)Session["IsNew"])
            {
                if (txtFullName.Text == string.Empty) return;
                string[] names = fullName.Split(' ');
                string text = "";
                for (int i = 0; i < 2; i++)
                {
                    string subText = names[i];
                    int length = subText.Length;
                    if (length >= 3)
                        text += names[i].Substring(0, 3);
                    else if (length >= 2)
                        text += names[i].Substring(0, 2);
                    else text += names[i];

                    if (names.Length == 1) break;
                }

                text += GetNextNo().ToString("000");
                txtUserName.Text = text;
                txtUserName.Visible = true;
                txtUserName.ClientVisible = true;
                lblUserName.ClientVisible = false;
            }
            else
            {
                txtUserName.ClientVisible = false;
                lblUserName.ClientVisible = true;
            }
        }

        protected void cblUserName_Callback(object sender, CallbackEventArgsBase e)
        {
            SuggestName(e.Parameter);
        }
    }
}