﻿using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.Domain;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class Lookup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                LoadLookUpTypes();
                Session["Print"] = false;
                pnlData.ClientVisible = false;
                cboLookupTypes.Value = null;
                gvwLookups.ClientVisible = false;
            }

            DoShow();
        }

        protected void LoadLookUpTypes()
        {
            LookupTypeBussiness LookUpType = new LookupTypeBussiness();
            cboLookupTypes.DataSource = LookUpType.GetLookupTypes();
            cboLookupTypes.TextField = "Description";
            cboLookupTypes.ValueField = "Id";
            cboLookupTypes.DataBind(); if (cboLookupTypes.Items.Count > 1)
            {
                //cboLookupTypes.Items.Insert(0, new ListEditItem("Select", "-1"));
                cboLookupTypes.Enabled = true;
                // cboLookupTypes.SelectedIndex = -1;
            }
        }

       


        protected void LoadLookUpInfo(int LookUpId)
        {
            DoNew();
            chIsActive.Checked = false;


            LookUpBussiness objLookUpBussiness = new LookUpBussiness();
            CUSTOR.Domain.LookUp LookUp = objLookUpBussiness.GetLookUp(LookUpId);
            if (LookUp != null)
            {

                txtDescription.Text = LookUp.Description;
                txtAmdescription.Text = LookUp.Amdescription;
                cboLookupTypes.Value = LookUp.Parent.ToString();
                chIsActive.Checked = LookUp.IsActive;
                clbLookupsID.JSProperties["cpAction"] = "Loaded";
                Session["LookUpId"] = LookUp.Id;
                ShowSuccess("");
            }
        }

        protected void DoNew()
        {
           // ASPxEdit.ClearEditorsInContainer(clbLookupsID);
            txtDescription.IsValid = true;
            cboLookupTypes.IsValid = true;
            chIsActive.Checked = true;
            txtAmdescription.Text = string.Empty;
            Session["PageMode"] = "NEW";
            chIsActive.Checked = true;
            txtAmdescription.IsValid = true;
            txtDescription.Text = string.Empty;
            //clbLookupsID.JSProperties["cpMessage"] = string.Empty;
        }
        protected void ClearForm()
        {

            cboLookupTypes.SelectedIndex = -1;
            txtDescription.Text = "";
        }


        protected void Delete(int LookUpId)
        {
            LookUpBussiness objLookUpBussiness = new LookUpBussiness();
            if (objLookUpBussiness.Delete(LookUpId))
            {
                //LoadAllLookups();
                DoShow();
                clbLookupsID.JSProperties["cpStatus"] = "INFO";
                ShowMessage("Selected LookUp deleted successfully!!!");
            }
        }
        protected void DoShow()
        {
            clbLookupsID.JSProperties["cpAction"] = "Show";
            ASPxEdit.ClearEditorsInContainer(pnlData);
            cboLookupTypes.IsValid = true;
            txtDescription.IsValid = true;
            chIsActive.Checked = true;
            int Parent = cboLookupTypes.Value == null ? -1 : Convert.ToInt16(cboLookupTypes.Value);
            LookUpBussiness objLookUpBussiness = new LookUpBussiness();
            try
            {
                List<LookUp> list = objLookUpBussiness.GetLookUps(Parent);
                if (list.Count > 0)
                {
                    gvwLookups.DataSource = list;
                    gvwLookups.DataBind();
                    gvwLookups.ClientVisible = true;
                    ShowSuccess("");
                }
                else
                {
                    gvwLookups.ClientVisible = false;
                    ShowMessage("No data was found");
                    gvwLookups.JSProperties["cpData"] = "NA";
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

            //gvwLookups.Visible = true;
        }


        protected void SaveEdit()
        {
            clbLookupsID.JSProperties["cpAction"] = "Save";
            int LookUpId = Convert.ToInt32(Session["LookUpId"]);
            LookUpBussiness objLookUpBussiness = new LookUpBussiness();
            CUSTOR.Domain.LookUp objLookUpList = objLookUpBussiness.GetLookUp(LookUpId);
            #region Build LookUp Information
            objLookUpList.Parent = Convert.ToInt16(cboLookupTypes.Value);
            objLookUpList.Description = txtDescription.Text;
            objLookUpList.Amdescription = txtAmdescription.Text;
            objLookUpList.IsActive = chIsActive.Checked;
            CVGeez geez = new CVGeez();
            objLookUpList.SortValue = geez.GetSortValueU(txtAmdescription.Text);
            objLookUpList.Soundx = geez.GetSoundexValue(txtAmdescription.Text);
            objLookUpList.IsActive = chIsActive.Checked;
            objLookUpList.UpdatedBy = "user";
            objLookUpList.UpdatedDate = DateTime.Now;

            #endregion
            #region Save LookUp Informatin

            //Check existance
            if(objLookUpBussiness.UpdateExists(objLookUpList))
            {
                ShowError("Lookup name already exists!");
                return;
            }
            if (objLookUpBussiness.UpdateLookUp(objLookUpList))
            {
                //LoadSavedLookUp(objLookUpList.ID);
                DoShow();
                clbLookupsID.JSProperties["cpStatus"] = "SUCCESS";
                clbLookupsID.JSProperties["cpAction"] = "Save";
                ShowSuccess("LookUp saved successfully");
            }

            else
            {
                //display message
                clbLookupsID.JSProperties["cpStatus"] = "ERROR";
                ShowError("There exist a LookUp with the same LookUp type and code !!!");
            }
            #endregion



        }

        protected void SaveNew()
        {
            #region Build LookUpTypes Information
            clbLookupsID.JSProperties["cpAction"] = "Save";
            CUSTOR.Domain.LookUp objLookUp = new CUSTOR.Domain.LookUp();
            CVGeez objGeez = new CVGeez();
            objLookUp.Parent = Convert.ToInt32(cboLookupTypes.Value);
            objLookUp.Description = txtDescription.Text;
            objLookUp.Amdescription = txtAmdescription.Text;
            objLookUp.IsActive = chIsActive.Checked;
            CVGeez geez = new CVGeez();
            objLookUp.SortValue = geez.GetSortValueU(txtAmdescription.Text);
            objLookUp.Soundx = geez.GetSoundexValue(txtAmdescription.Text);
            objLookUp.IsActive = chIsActive.Checked;
            objLookUp.CreatedBy = "user";
            objLookUp.CreatedDate = DateTime.Now;
            #endregion
            #region Save LookUpTypes Information

            LookUpBussiness objLookUpBussiness = new LookUpBussiness();
            //Check existance
            if (objLookUpBussiness.Exists(objLookUp))
            {
                ShowError("Lookup name already exists!");
                return;
            }
            bool registeredLookUp = objLookUpBussiness.InsertLookUp(objLookUp);
            if (registeredLookUp)
            {
                // DoNew();

                //LoadSavedLookUp(registeredLookUp);
                DoShow();
                clbLookupsID.JSProperties["cpStatus"] = "SUCCESS";
                clbLookupsID.JSProperties["cpAction"] = "Save";
                ShowSuccess("LookUp saved successfully");
            }

            else
            {
                //display message
                clbLookupsID.JSProperties["cpStatus"] = "ERROR";
                ShowError("There exist a LookUp with the same LookUp type and code !!!");
            }

            #endregion
        }

        private void ShowError(string strMsg)
        {
            clbLookupsID.JSProperties["cpMessage"] = strMsg;
            clbLookupsID.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbLookupsID.JSProperties["cpMessage"] = strMsg;
            clbLookupsID.JSProperties["cpStatus"] = "INFO";
        }

        private void ShowSuccess(string strMsg)
        {
            clbLookupsID.JSProperties["cpMessage"] = strMsg;
            clbLookupsID.JSProperties["cpStatus"] = "SUCCESS";
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            if (txtDescription.Text == string.Empty)
            {
                errMessages.Add("Please, enter Description!");
            }

            

            if (cboLookupTypes.SelectedIndex == -1)
            {

                errMessages.Add("Please, select LookUp type!");
            }
            if (txtAmdescription.Text == string.Empty)
            {

                errMessages.Add("Please enter english description name");
            }
            return errMessages;
        }

        protected void Saved()
        {
            // Page.Validate();
            var n = ASPxEdit.ValidateEditorsInContainer(clbLookupsID);
            if (!ASPxEdit.ValidateEditorsInContainer(clbLookupsID))
            {
                pnlData.ClientVisible = true;
                gvwLookups.ClientVisible = false;
                return;
            }
            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                clbLookupsID.JSProperties["cpStatus"] = "ERROR";
                ShowError(GetErrorDisplay(errMessages));
                pnlData.ClientVisible = true;
                gvwLookups.ClientVisible = false;
                return;
            }
            else
            {
                switch (Session["PageMode"].ToString())
                {
                    case "NEW":
                        SaveNew();
                        break;
                    case "EDIT":
                        SaveEdit();
                        break;
                }
            }
        }

        protected void clbLookups_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            clbLookupsID.JSProperties["cpStatus"] = "";
            clbLookupsID.JSProperties["cpAction"] = "";
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    DoNew();
                    pnlData.ClientVisible = true;
                    gvwLookups.ClientVisible = false;
                    break;
                case "Save":
                    Saved();
                    break;

                case "EDITING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        LoadLookUpInfo(Convert.ToInt32(strID));
                        Session["PageMode"] = "EDIT";
                        gvwLookups.ClientVisible = false;
                        pnlData.ClientVisible = true;
                    }
                    break;
                case "DELETING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        Session["ID"] = strID;
                        Delete(Convert.ToInt32(Session["ID"]));
                    }
                    break;
                case "Report":
                    Session["Print"] = true;
                    //GenerateLookUpReport();
                    break;
                case "Show":
                    DoShow();
                    break;
                default:
                    break;
            }
        }
        protected void Grid_PageIndexChanged(object sender, EventArgs e)
        {
            DoShow();
        }
        private void FillLanguage(ASPxComboBox combo)
        {
            try
            {
                string qtext = "select * from NewsLanguage where IsActive = '1' and LangId = 0";
                CDropdown.FillComboFromDB(combo, qtext, "Description", "Id");
            }
            catch
            {
            }
        }

        protected void cboLookupTypes_callback(object sender, CallbackEventArgsBase e)
        {
        }

        
    }
}