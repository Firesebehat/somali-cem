﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="RMS.Pages.User" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ContentPlaceHolderID="PageContent" runat="Server">

    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">
        function chClientChanged(s) {
            var y = lstUserRoles.GetSelectedItems();
            var length = lstUserRoles.GetSelectedIndices().length;
            for (var i = 0; i < length; i++) {
                if (y[i].text === 'Editor') {

                }
            }
            return;
        }
        function OrgTypeSelectedChanged(s) {
            Org.SetSelectedIndex(-1);
            Org.PerformCallback(s.GetValue());
            Org.SetEnabled(true);
        }
        function OnEndCallback(s, e) {
            if (s.cpStatus == "SUCCESS") {
                chkApproved.SetEnabled(true);
                chkApproved.SetChecked(true);

                if (s.cpAction == "SHOW") {
                }
                if (s.cpMessage == '') {
                    return;
                }
                if (s.cpAction == "Save" || s.cpAction == "New") {
                    clbRoleList.PerformCallback('CLEAR');
                }
                if (s.cpMessage != '' && s.cpMessage != null)
                    ShowSuccess(s.cpMessage);
            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);
            }
        }
        function DoNewClick(s, e) {
            //pnlData.SetEnabled(true);
            btnAddUserClient.SetEnabled(false);
            rdpData.SetVisible(true);
            btnSaveClient.SetEnabled(true);
            hfOrg.Clear();
            CallbackPanel.PerformCallback('New');

        }
        function OnOrgIndexChanged(s, e) {

            hfOrg.Clear();
            CallbackPanel.PerformCallback('SetProfile' + "|" + Org.GetValue());

        }
        function OrgEndCallback() {
            clbRoleList.PerformCallback('ORGTYPECHANGED|' + cboOrgType.GetText());
            Org.SetEnabled(true);

        }
        function DoSaveClick() {
            if (!ASPxClientEdit.ValidateGroup('reg'))
                return;
            var length = lstUserRoles.GetSelectedIndices().length;
            if (length == 0) {
                ShowError('You must select atleast one role');
                return;

            }
            var s = '';
            var y = lstUserRoles.GetSelectedItems();
            for (var i = 0; i < length; i++) {
                s += y[i].text + ',';
            }
            s = s.substring(0, s.length - 1);
            CallbackPanel.PerformCallback('Save|' + s);
        }

        function OnOrgChanged() {
            // clbRoleList.PerformCallback('UNITCHANGED|' + Org.GetText());

        }
        function tempChanged(s) {
            clbDate.SetVisible(s.GetChecked());
            chkApproved.SetVisible(!s.GetChecked());
            chkApproved.SetEnabled(!s.GetChecked());
            clbDate.PerformCallback();
        }
        function SuggetName(s) {

            cblClientUserName.PerformCallback(s.GetText());
        }
    </script>


    <div class="contentmain">
        <h5>User Credentials
        </h5>
        <div class="card card-body" style="width: 100%">
            <script src="../Content/ModalDialog.js"></script>

            <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />

            <div style="padding-bottom: 15px">
                <dx:ASPxButton ID="btnAddUser" runat="server" AutoPostBack="False" ClientInstanceName="btnAddUserClient" Text="New User" CausesValidation="false" ClientEnabled="true">
                    <ClientSideEvents Click="DoNewClick" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSaveClient" Text="Save" CausesValidation="true" ClientEnabled="false">
                    <ClientSideEvents Click="function(s, e) {
	                                    DoSaveClick();
                                    }" />
                </dx:ASPxButton>

            </div>


            <dx:ASPxRoundPanel runat="server" ID="rdpData" ClientInstanceName="rdpData" ClientVisible="false" ShowHeader="false">
                <ContentPaddings Padding="15px" />
                <PanelCollection>
                    <dx:PanelContent>


                        <dx:ASPxCallbackPanel runat="server" ID="cbData" ClientInstanceName="CallbackPanel" OnCallback="CallbackPanel_Callback">
                            <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server" class="RoundPanelWhite">
                                    <dx:ASPxPanel runat="server" ID="ASPxPanel1" ClientInstanceName="pnlData" Width="100%">
                                        <PanelCollection>
                                            <dx:PanelContent>
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <table style="float: left">

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <asp:Label ID="lblEmpName" runat="server" Text="Staff Full  Name"></asp:Label>
                                                                    <asp:Label ID="Label5" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtFullName" runat="server" ClientInstanceName="txtFName" Width="350px">
                                                                        <ValidationSettings ValidationGroup="reg" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Top">
                                                                            <RequiredField ErrorText="Full name is required" IsRequired="True" />
                                                                            <RegularExpression ValidationExpression="^[A-Za-z\s]+$" ErrorText="Only letters  are allowed" />
                                                                        </ValidationSettings>
                                                                        <ClientSideEvents LostFocus="function(s){SuggetName(s);}" />
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <asp:Label ID="lblUserID" runat="server" Text="User Name"></asp:Label>
                                                                    <asp:Label ID="Label1" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxCallbackPanel runat="server" Width="100%" ID="cblUserName" ClientInstanceName="cblClientUserName" OnCallback="cblUserName_Callback">
                                                                        <PanelCollection>
                                                                            <dx:PanelContent>
                                                                                <dx:ASPxTextBox ID="txtUserName" ClientInstanceName="txtUserName" Font-Size="Medium" CssClass="text-primary" runat="server" EnableTheming="True" ValidationGroup="user" Width="350px" Style="float: left">
                                                                                    <ValidationSettings ErrorDisplayMode="Text" SetFocusOnError="True" ValidationGroup="reg" ErrorText="User name is required!" ErrorFrameStyle-Font-Size="Small" Display="Dynamic" ErrorTextPosition="Top">
                                                                                        <RequiredField ErrorText="User name is required" IsRequired="True" />
                                                                                        <RegularExpression ValidationExpression="^[0-9A-Za-z]+$" ErrorText="Only letters or numbers are allowed" />
                                                                                    </ValidationSettings>
                                                                                </dx:ASPxTextBox>
                                                                                <dx:ASPxLabel ID="lblUserName" Font-Size="Medium" runat="server" Text="" Height="30px" Style="margin-left: 10px; padding: 7px 2px 7px 2px;" Visible="false"></dx:ASPxLabel>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxCallbackPanel>


                                                                </td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <asp:Label ID="Label9" runat="server" Text="Organization Level"></asp:Label>
                                                                    <asp:Label ID="Label10" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboOrgType" runat="server" ClientInstanceName="cboOrgType" Width="350px" NullText="Select"
                                                                        ValidationSettings-CausesValidation="false" ValueType="System.Int32">
                                                                        <ValidationSettings ErrorTextPosition="Top" SetFocusOnError="True" ValidationGroup="reg" Display="Dynamic" ErrorDisplayMode="Text">
                                                                            <RequiredField ErrorText="Org. Level can not be empty." IsRequired="true" />
                                                                        </ValidationSettings>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){OrgTypeSelectedChanged(s);}" />
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel ID="lblOrg" runat="server" Font-Size="Small" Text="Site">
                                                                    </dx:ASPxLabel>
                                                                    <asp:Label ID="Label8" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboOrg" runat="server" ClientInstanceName="Org" EnableClientSideAPI="True" Font-Size="Small" ValueType="System.Int32" NullText="Select"
                                                                        Width="350px" OnCallback="cboOrg_Callback" ValidationGroup="reg">
                                                                        <ClientSideEvents SelectedIndexChanged="OnOrgChanged" EndCallback="OrgEndCallback" />
                                                                        <ValidationSettings ErrorText="Organization can not be empty" ErrorDisplayMode="Text" Display="Dynamic" ValidationGroup="reg" ErrorTextPosition="Top">
                                                                            <RequiredField ErrorText="Site name is required" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <%--             <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Size="Small" Text="User Group">
                                                                    </dx:ASPxLabel>
                                                                    <asp:Label ID="Label2" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboUserGroup" runat="server" ClientInstanceName="cboUserGroup" EnableClientSideAPI="True" Font-Size="Small" ValueType="System.Int32"
                                                                        Width="350px" OnCallback="cboOrg_Callback" ValidationGroup="reg" NullText="Select">
                                                                        <ClientSideEvents SelectedIndexChanged="OnOrgChanged" EndCallback="OrgEndCallback" />
                                                                        <ValidationSettings ErrorText="User group can not be empty" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Top">
                                                                            <RequiredField ErrorText="User group is required" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>--%>


                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <dx:ASPxHiddenField ID="hfOrg" runat="server" ClientInstanceName="hfOrg">
                                                                    </dx:ASPxHiddenField>
                                                                </td>

                                                            </tr>
                                                        </table>

                                                    </div>
                                                    <div class="col col-md-5">


                                                        <table>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td class="auto-style19">
                                                                    <asp:Label ID="lblUserID3" runat="server" Text="EMail"></asp:Label>
                                                                    <%--<asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*" EnableTheming="False"></asp:Label>--%>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtEMail" runat="server" ValidationGroup="user" Width="300px" NullText="example.yahoo.com">
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidationGroup="reg" Display="Dynamic" ErrorTextPosition="Top">
                                                                            <RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                            <RequiredField IsRequired="False" ErrorText="Email is required!" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <asp:Label ID="lblTele" runat="server" Text="Telephone"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtMobileNo" runat="server" Width="100%" ClientInstanceName="txtMobileNo">
                                                                        <MaskSettings Mask="+251 (999) 000000" IncludeLiterals="None" ErrorText="የሞባይል ቁጥር የተሟላ አይደለም" />
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ErrorText="የሞባይል ቁጥር ያስፈልጋል" Display="Dynamic" ValidationGroup="pnlTaxPayerAddress" ErrorTextPosition="Bottom">
                                                                            <RequiredField ErrorText="የሞባይል ስልክ ቁጥር ባዶ መሆን አይችልም።" IsRequired="true" />
                                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <asp:Label ID="lblUserID0" runat="server" Text="Description"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxMemo ID="txtComment" runat="server" Height="40px" TextMode="MultiLine" ValidationGroup="user" Width="300px">
                                                                    </dx:ASPxMemo>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <dx:ASPxCheckBox ID="chkApproved" ClientInstanceName="chkApproved" runat="server" Checked="true" Text="Approved User" Width="150px">
                                                                    </dx:ASPxCheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                            </dx:PanelContent>
                                        </PanelCollection>
                                    </dx:ASPxPanel>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>


                        <h6>Roles:</h6>
                        <dx:ASPxCallbackPanel ID="clbRoleList" runat="server" ClientInstanceName="clbRoleList" OnCallback="clbRoleList_Callback" Width="100%">
                            <PanelCollection>
                                <dx:PanelContent>


                                    <dx:ASPxCheckBoxList ID="lstUserRoles" runat="server" RepeatColumns="4" ValidationGroup="user" Width="100%" ClientInstanceName="lstUserRoles" Theme="DevEx" OnInit="lstUserRoles_Init">
                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidationGroup="reg" ErrorTextPosition="Top" Display="Dynamic">
                                            <RequiredField IsRequired="True" ErrorText="Please select atleast one role." />
                                        </ValidationSettings>
                                    </dx:ASPxCheckBoxList>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>


                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxRoundPanel>
        </div>






        <div class="RoundPanelWhite" style="visibility: hidden">

            <dx:ASPxTextBox runat="server" Width="250px" ClientInstanceName="txtLang" ClientEnabled="False" ID="txtLang" Visible="False"></dx:ASPxTextBox>
            <dx:ASPxLabel runat="server" Text="Default Language" ID="lblLang" Visible="False"></dx:ASPxLabel>

        </div>
</asp:Content>
