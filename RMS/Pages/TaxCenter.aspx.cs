﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class TaxCenter : System.Web.UI.Page
    {
        const int ZoneCode = 112134;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack && !Page.IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                hdfValue["IsNewRecord"] = true;
                FillCombo();
                chbIsActive.Checked = true;
                cboParent.ClientEnabled = false;
                
            }
            trlSite.ExpandAll();

        }
        private void FillCombo()
        {
            CDropdown.FillComboFromEnum(cboOrgType, Language.eLanguage.eEnglish, typeof(OrganizationalType.OrgType));
            CDropdown.FillComboFromEnum(cboOrgTypeSearch, Language.eLanguage.eEnglish, typeof(OrganizationalType.OrgType));
            CDropdown.FillComboFromEnum(cboLanguage, Language.eLanguage.eEnglish, typeof(Language.eLanguage));
            CDropdown.FillComboFromEnum(cboSiteLeve, Language.eLanguage.eEnglish, typeof(Enums.eSiteLevel));
            cboOrgTypeSearch.Items.Add(new DevExpress.Web.ListEditItem("All", "-1"));
            cboOrgTypeSearch.Value = "-1";
            string RegionCode = "05";
            FillRegion();
            cboRegion.Value = RegionCode;
            FillZoneCombo(RegionCode);
            cboZone.Value = ZoneCode;
            FillWoredaCombo(ZoneCode.ToString());
        }
        protected void cbData_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {

        }

        protected void clbCenter_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                strID = parameters[1];

            switch (strParam)
            {
                case "New":
                    DoNew();
                    RestoreComboValues();
                    break;
                case "Save":
                    DoSave();
                    RestoreComboValues();
                    break;
                case "Edit":
                    DoFind(strID);
                    break;
                case "Delete":
                    Delete();
                    break;
            }
        }

        private void DoFind(string strID)
        {
            clbCenter.JSProperties["cpAction"] = "Edit";
            TaxCenterBussiness bussiness = new TaxCenterBussiness();
            try
            {
                int CenterId = Convert.ToInt32(strID);
                CUSTOR.Domain.TaxCenter objCenter = bussiness.GetTaxCenter(CenterId);

                txtCenterNameAmh.Text = objCenter.CenterName;
                txtCenterNameEng.Text = objCenter.CenterNameEnglish;
                txtTell.Text = objCenter.Telephone;
                txtEmail.Text = objCenter.Email;
                txtPobox.Text = objCenter.POBox;
                cboOrgType.Value = objCenter.OrgType;

                if (objCenter.SiteLevel != 0)
                    cboSiteLeve.Value = objCenter.SiteLevel;

                FillParentOrganization(objCenter.OrgType.ToString());
                cboParent.Value = objCenter.Parent;
                cboLanguage.Value = objCenter.Language;
                if (cboRegion.Items.Count == 0) FillRegion();
                cboRegion.Value = objCenter.Region.ToString();
                //if (cboZone.Items.Count == 0) FillZoneCombo(cboRegion.Value.ToString());
                //cboZone.Value = objCenter.Zone;
                if (cboWereda.Items.Count == 0) FillWoredaCombo(ZoneCode.ToString());
                cboWereda.Value = objCenter.Woreda.ToString();
                if (cboKebele.Items.Count == 0) FillKebeleCombo(objCenter.Woreda.ToString());
                cboKebele.Value = objCenter.Kebele.ToString();
                hdfValue["IsNewRecord"] = false;
                Session["CenterId"] = strID;
                cboParent.ClientEnabled = true;
                chbIsActive.Checked = objCenter.IsActive;
                ShowSuccess("");

            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void cboZone_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            FillZoneCombo(cboRegion.Value.ToString());
        }

        protected void cboWereda_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            FillWoredaCombo(cboZone.Value.ToString());
        }

        protected void cboKebele_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            FillKebeleCombo(cboWereda.Value.ToString());
        }

        protected void gvwCenter_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {

        }
        private void FillRegion()
        {
            //Fill combo from tblRegion
            RegionBusiness objRegion = new RegionBusiness();
            List<CUSTOR.Domain. Region> objRegions = new List<CUSTOR.Domain.Region>();
            int Lan = 0;
            objRegions = objRegion.GetRegions(Lan);
            cboRegion.DataSource = objRegions;
            cboRegion.TextField = "Description";
            cboRegion.ValueField = "Code";
            cboRegion.DataBind();
        }
        protected void FillWoredaCombo(string zone)
        {
            if (string.IsNullOrEmpty(zone)) return;
            WoredaBusiness objAddda = new WoredaBusiness();
            int Lan = 0;
            cboWereda.DataSource = objAddda.GetWoredas(zone, Lan);
            cboWereda.TextField = "Description";
            cboWereda.ValueField = "Code";
            cboWereda.DataBind();
        }

        protected void FillKebeleCombo(string woreda)
        {
            if (string.IsNullOrEmpty(woreda)) return;
            KebeleBusiness objAdd = new KebeleBusiness();
            int Lan = 0;
            cboKebele.DataSource = objAdd.GetKebeles(woreda, Lan);
            cboKebele.TextField = "Description";
            cboKebele.ValueField = "Code";
            cboKebele.DataBind();
        }
        private void RestoreComboValues()
        {
            if (cboRegion.Value != null && cboRegion.Items.Count == 0)
                FillZoneCombo(cboRegion.Value.ToString());
            if (cboWereda.Items.Count == 0)
                FillWoredaCombo(ZoneCode.ToString());
            if (cboWereda.Value != null & cboKebele.Items.Count == 0)
                FillKebeleCombo(cboWereda.Value.ToString());

            cboRegion.Value = "05";
        }
        protected void FillZoneCombo(string region)
        {
            if (string.IsNullOrEmpty(region)) return;
            int Lan = 0;
            ZoneBusiness objAdd = new ZoneBusiness();
            cboZone.DataSource = objAdd.GetZones(region, Lan);
            cboZone.TextField = "Description";
            cboZone.ValueField = "Code";
            cboZone.DataBind();
        }
        private void ShowError(string strMsg)
        {
            clbCenter.JSProperties["cpMessage"] = strMsg;
            clbCenter.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            clbCenter.JSProperties["cpMessage"] = strMsg;
            clbCenter.JSProperties["cpStatus"] = "SUCCESS";
        }
        private void FillParentOrganization(string OrgType)
        {
            TaxCenterBussiness objBusiness = new TaxCenterBussiness();
            List<CUSTOR.Domain.TaxCenter> parents = objBusiness.Parents(OrgType);
            cboParent.TextField = "CenterNameEnglish";
            cboParent.ValueField = "Id";
            cboParent.DataSource = parents;
            cboParent.DataBind();
        }

        protected void cboParent_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string OrgType = e.Parameter;
            FillParentOrganization(OrgType);
        }
        private void DoNew()
        {
            clbCenter.JSProperties["cpAction"] = "New";
            ClearForm();
            SetValidTrue();
            chbIsActive.Checked = true;
            cboParent.ClientEnabled = false;
            hdfValue["IsNewRecord"] = true;
        }
        private void ClearForm()
        {
            txtCenterNameAmh.Text = string.Empty;
            txtCenterNameEng.Text = string.Empty;
            txtTell.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPobox.Text = string.Empty;
            cboOrgType.Value = null;
            cboParent.Value = null;
            cboLanguage.Value = null;
            cboRegion.Value = null;
            //cboZone.Value = null;
            cboWereda.Value = null;
            cboKebele.Value = null;
        }
        private void SetValidTrue()
        {
            txtCenterNameAmh.IsValid = true;
            txtCenterNameEng.IsValid = true;
            txtTell.IsValid = true;
            txtEmail.IsValid = true;
            txtPobox.IsValid = true;
            cboOrgType.IsValid = true;
            cboParent.IsValid = true;
            cboLanguage.IsValid = true;
            cboRegion.IsValid = true;
            //cboZone.IsValid = true;
            cboWereda.IsValid = true;
            cboKebele.IsValid = true;
        }
        private void DoSave()
        {
           clbCenter.JSProperties["cpAction"] = "Save";
            DateTime EventDate = DateTime.Now;
            CUSTOR.Domain.TaxCenter objCenter = new CUSTOR.Domain.TaxCenter();
            TaxCenterBussiness bussiness = new TaxCenterBussiness();
            objCenter.CenterName = txtCenterNameAmh.Text;
            objCenter.CenterNameEnglish = txtCenterNameEng.Text;
            objCenter.OrgType = Convert.ToInt16(cboOrgType.Value);
            objCenter.Parent = Convert.ToInt32(cboParent.Value);
            objCenter.SiteLevel = Convert.ToInt16(cboSiteLeve.Value);
            objCenter.Language = Convert.ToInt16(cboLanguage.Value);
            objCenter.IsActive = chbIsActive.Checked;
            objCenter.Region = Convert.ToInt16(cboRegion.Value);
            objCenter.Zone = ZoneCode;
            objCenter.Woreda = Convert.ToInt32(cboWereda.Value);
            objCenter.Kebele = Convert.ToInt32(cboKebele.Value);
            objCenter.Telephone = txtTell.Text;
            objCenter.Email = txtEmail.Text;
            objCenter.POBox = txtPobox.Text;
            CVGeez geez = new CVGeez();
            objCenter.CenterNameSort = geez.GetSortValueU(objCenter.CenterName);
            objCenter.CenterNameSound = geez.GetSoundexValue(objCenter.CenterName);
            try
            {
                if ((bool)hdfValue["IsNewRecord"])
                {
                    if(bussiness.Exists(objCenter.CenterName,objCenter.CenterNameSort))
                    {
                        ShowError("Center Name already exists.");
                        return;
                    }
                    objCenter.CreatedBy = "user";
                    objCenter.CreatedUserId = Guid.NewGuid().ToString();
                    objCenter.CreatedDate = EventDate;

                    bussiness.InsertTaxCenter(objCenter);
                        
                }
                else
                {
                    int Id = Convert.ToInt32(Session["CenterId"]);
                    if (bussiness.UpdateExists(objCenter.CenterName, objCenter.CenterNameSort, Id)) 
                    {
                        ShowError("Center Name already exists.");
                        return;
                    }
                    objCenter.Id = Id;
                    objCenter.UpdatedBy = "user";
                    objCenter.UpdatedDate = EventDate;
                    objCenter.UpdatedUserId = Guid.NewGuid().ToString();

                    bussiness.UpdateTaxCenter(objCenter);
                }
                ShowSuccess("The record is saved successfully.");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }

        }
        private void Delete()
        {
            clbCenter.JSProperties["cpAction"] = "Delete";
            TaxCenterBussiness bussiness = new TaxCenterBussiness();
            try
            {
                int CenterId = Convert.ToInt32(Session["CenterId"]);
                bussiness.Delete(CenterId);
                ShowSuccess("The record is deleted successfully");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        private void Search()
        {
            clbCenter.JSProperties["cpAction"] = "Search";
            TaxCenterBussiness bussiness = new TaxCenterBussiness();
            try
            {
                string text = txtSearch.Text;
                int OrgType = cboOrgTypeSearch.Value == null ? -1 : Int16.Parse(cboOrgTypeSearch.Value.ToString());
                bool isActivOnly = chActiveSites.Checked;

                List<CUSTOR.Domain.TaxCenter> taxCenters =  bussiness.SearchCenter(text,OrgType,isActivOnly);
                if (taxCenters.Count > 0)
                {
                    trlSite.DataSource = taxCenters;
                }
                else
                {
                    trlSite.DataSource = null;
                    trlSite.JSProperties["cpMessage"] = "NA";
                }
                trlSite.DataBind();

            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void trlSite_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
        {
                Search();
        }

        private void BindList()
        {
            clbCenter.JSProperties["cpAction"] = "Bind";
            TaxCenterBussiness bussiness = new TaxCenterBussiness();
            try
            {

                List<CUSTOR.Domain.TaxCenter> taxCenters = bussiness.GetTaxCenters();
                if(taxCenters.Count > 0)
                {
                    trlSite.DataSource = taxCenters;
                }
                else
                {
                    trlSite.DataSource = null;
                    trlSite.JSProperties["cpMessage"] = "NA";
                }
                trlSite.DataBind();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void trlSite_NodeExpanding(object sender, DevExpress.Web.ASPxTreeList.TreeListNodeCancelEventArgs e)
        {
            //BindList();
        }

        protected void trlSite_Init(object sender, EventArgs e)
        {
            if(Page.IsCallback)
            Search();
           trlSite.ExpandAll();
        }
    }
}