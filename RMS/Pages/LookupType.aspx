﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LookupType.aspx.cs" Inherits="RMS.Pages.LookupType" MasterPageFile="~/Root.master" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <style>
        td{
            vertical-align:top;
        }
        .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
    </style>
    <script type="text/javascript">

        function DoNewLookTypeClick(s, e) {

            clbLookupTypes.PerformCallback('New');

        }
        function DoSaveLookTypeClick(s, e) {
            var validated = ASPxClientEdit.ValidateGroup('LookupTypeRegistration');
            if (!validated) {
                return;
            }
            clbLookupTypes.PerformCallback('Save');
        }

        function DoReportLookTypeClick(s, e) {
            clbLookupTypes.PerformCallback('Report');
        }

        function btnSearchLookupTypeViaAmharic(s, e) {
            clbLookupTypes.PerformCallback('SearchAmharic');
        }

        function btnSearchLookupTypeViaEnglish(s, e) {
            clbLookupTypes.PerformCallback('SearchEnglish');
        }

        function DoSearchLookupTypeClickAfanOromo(s, e) {
            clbLookupTypes.PerformCallback('SearchAfanOromo');
        }

        function DoSearchLookupTypeClickAfar(s, e) {
            clbLookupTypes.PerformCallback('SearchAfar');
        }

        function DoSearchLookupTypeClickSomali(s, e) {
            clbLookupTypes.PerformCallback('SearchSomali');
        }

        function DoSearchLookupTypeClickTigrigna(s, e) {
            clbLookupTypes.PerformCallback('SearchTigrigna');
        }
        function DoCancelClient() {
            btnSaveLookType.SetEnabled(false);
            pnlData.SetVisible(false);
            btnNewLookType.SetEnabled(true);
            clbLookupTypes.PerformCallback('Find');


        }
        function DoFindClick() {
            pnlData.SetVisible(false);
            btnSaveLookType.SetEnabled(false);
            clbLookupTypes.PerformCallback('Find');
        }
        var rowVisibleIndex;
        var strID;

        function gvwLookupTypes_CustomButtonClick(s, e) {
            if (e.buttonID !== 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID === 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                gvLookupTypes.GetRowValues(e.visibleIndex, 'Id', ShowPopup);
            }
            if (e.buttonID === 'EDITING') {
                btnSaveLookType.SetEnabled(true);
                gvLookupTypes.GetRowValues(e.visibleIndex, 'Id', OnGetSelectedFieldValues);

                //DoToggle();
            }
        }

        function OnGetSelectedFieldValues(values) {
            clbLookupTypes.PerformCallback('EDITING' + '|' + values);
            btnDetail.SetEnabled(true);
        }

        function ShowPopup(values) {

            ShowConfirmx('Are you sure? Do you want to delete this record?', function (result) {
                if (result) {
                    Delete(values);
                }
            }, 'en');
        }

        function OnLookupTypesEndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Cancel") {
                    doListView();
                    return;
                }
                else if (s.cpAction === "Save") {
                    doListView();
                    ShowSuccess(s.cpMessage);
                }
                else if (s.cpAction === "delete") {
                    doListView();
                    ShowSuccess(s.cpMessage);
                }
                else if (s.cpAction === "New") {
                    doEditeView();
                } else if (s.cpAction === "Loaded") {
                    doEditeView();
                }
                else if (s.cpAction === "Show") {
                    btnNewLookType.SetEnabled(true);
                    btnDetail.SetEnabled(false);
                }
            }
            else if (s.cpStatus === "ERROR") {
                if (s.cpAction === "Save") {
                    doEditeView();
                }
                if (s.cpAction === "delete") {
                    doListView();
                }
                ShowError(s.cpMessage);
            }
            else if (s.cpStatus === "INFO") {
                ShowMessage(s.cpMessage);
            }
        }

        

        function Delete(values) {

            clbLookupTypes.PerformCallback('DELETING' + '|' + values);
            
        }

       
        function DeleteGridRow(visibleIndex) {
            var index = gvLookupTypes.GetFocusedRowIndex();
            gvLookupTypes.DeleteRow(index);
        }
        function doEditeView() {
            gvLookupTypes.SetVisible(false);
            pnlData.SetVisible(true);
            btnDetail.SetEnabled(true);
            btnNewLookType.SetEnabled(false);
            btnSaveLookType.SetEnabled(true);
        }
        function doListView() {
            gvLookupTypes.SetVisible(true);
            pnlData.SetVisible(false);
            btnDetail.SetEnabled(false);
            btnNewLookType.SetEnabled(true);
            btnSaveLookType.SetEnabled(false);
        }

    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            btnSaveLookType.SetEnabled(false);
        }
    </script>
    
    <div class="contentmain">
        <h5>
            Lookup-types Maintenance
        </h5>
        <div class="card card-body">
        <script src="../Content/ModalDialog.js"></script>
          <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
      
        <div style="padding-bottom:10px">
            <dx:ASPxButton ID="btnReportClient" runat="server" AutoPostBack="False" ClientInstanceName="btnReportLookType" Text="ሪፓርት አትም" Theme="Office2010Silver" VerticalAlign="Middle" Visible="False" Width="150px">
                <ClientSideEvents Click="function(s, e) {
	                                                                    DoReportLookTypeClick();
                                                                    }" />

            </dx:ASPxButton>

            <dx:ASPxButton ID="btnNewClient" runat="server" AutoPostBack="False" ClientInstanceName="btnNewLookType" Text="New">
                <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewLookTypeClick();
                                                                    }" />

            </dx:ASPxButton>


            <dx:ASPxButton ID="btnDetail" runat="server" ClientInstanceName="btnDetail" Text="Show List"
                AutoPostBack="False" CausesValidation="false" ClientEnabled="true">
                <ClientSideEvents Click="function(s, e) {
	                                                                    DoCancelClient();
                                                                    }" />

            </dx:ASPxButton>


        </div>
            <dx:ASPxCallbackPanel ID="clbLookupTypesID" OnCallback="ClbLookupTypes_Callback" ClientInstanceName="clbLookupTypes" runat="server" Height="100%" Width="700px">
                <ClientSideEvents EndCallback="OnLookupTypesEndCallback" />
                <PanelCollection>

                    <dx:PanelContent ID="pnlContent1" runat="server" SupportsDisabledAttribute="True">
                        <table>
                            <tr>

                                <td></td>

                            </tr>

                        </table>

                        <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" ShowHeader="false" Width="100%">
                            <PanelCollection>
                                <dx:PanelContent>
                                    <table style="width:100%">
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Amharic Description:"></asp:Label>
                                           <span class="star">*</span>
                                                </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtAmhDescription" runat="server" Width="400px">
                                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="LookupTypeRegistration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                        <RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />
                                                        <RegularExpression ErrorText="Only amharic letters are allowed"  ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$"  />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDescription" runat="server" Text="English Description:"></asp:Label>
                                                 <span class="star">*</span>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtDescription" runat="server" Width="400px">
                                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="LookupTypeRegistration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                        <RequiredField ErrorText="Please enter English name." IsRequired="True" />
                                                        <RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Is Active:"></asp:Label>
                                                <span class="star">*</span>
                                            </td>
                                            <td>
                                                <dx:ASPxCheckBox ID="chIsActive" runat="server" Checked="true" Style="margin: 0px"></dx:ASPxCheckBox>
                                            </td>

                                        </tr>


                                        <tr>
                                            <td></td>
                                            <td>
                                                <div style="float: right">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" ClientInstanceName="btnCancelClient" BackColor="Silver" Text="Cancel" CausesValidation="false">
                                                                    <ClientSideEvents Click="DoCancelClient" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxButton ID="btnSaveClient" runat="server" ClientInstanceName="btnSaveLookType"
                                                                    Text="Save" ValidationGroup="LookupTypeRegistration" CausesValidation="true" AutoPostBack="False" Style="float: right">
                                                                    <ClientSideEvents Click="function(s, e) { DoSaveLookTypeClick();}" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>

                                </dx:PanelContent>
                            </PanelCollection>

                        </dx:ASPxRoundPanel>
                       
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="gvwLookupTypes" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvLookupTypes" EnableTheming="True" Width="500px" ClientVisible="false"
                                        Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id" OnPageIndexChanged="gvwLookupTypes_PageIndexChanged" OnInit="gvwLookupTypes_Init">
                                        <ClientSideEvents CustomButtonClick="function(s,e){ gvwLookupTypes_CustomButtonClick(s,e);}" />
                                        <Settings ShowFilterRow="True" />
                                        <SettingsPopup>
                                            <HeaderFilter MinHeight="140px">
                                            </HeaderFilter>
                                        </SettingsPopup>
                                        <SettingsSearchPanel Visible="True" />
                                        <Columns>

                                            <dx:GridViewDataTextColumn Caption="Amharic Name" FieldName="Amdescription" VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewDataTextColumn Caption="English Name" FieldName="Description" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>

                                            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="2" Width="100px" ShowClearFilterButton="True">
                                                <CustomButtons>
                                                    <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Edit">
                                                    </dx:GridViewCommandColumnCustomButton>
                                                    <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                        <Styles Style-ForeColor="Red">
                                                            <Style ForeColor="Red">
                                                            </Style>
                                                        </Styles>
                                                    </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dx:GridViewCommandColumn>
                                        </Columns>
                                          <SettingsPager PageSize="6"></SettingsPager>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </table>


                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>


    </div>
</div>
</asp:Content>
