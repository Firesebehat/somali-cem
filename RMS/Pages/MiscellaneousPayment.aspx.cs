﻿
using CUSTOR.Common;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using CUSTOR.Domain;
using CUSTOR.Business;

namespace RMS.Pages
{
    public partial class MiscellaneousPayment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                pnlData.ClientVisible = false;
            }
            DoFind();
        }
        protected void DoNew()
        {
            Session["PageMode"] = "NEW";
            txtDescription.Text = string.Empty;
            txtAccountCode.Text = string.Empty;
            txtAmhDescription.Text = string.Empty;
            txtDescription.IsValid = true;
            txtAmhDescription.IsValid = true;
        }

        protected void Delete(int MiscellaneousPaymentTypeId)
        {
            MiscellaneousPaymentAccountBusiness objMiscellaneousPaymentAccountBusiness = new MiscellaneousPaymentAccountBusiness();
            if (objMiscellaneousPaymentAccountBusiness.Delete(MiscellaneousPaymentTypeId))
            {
                DoFind();
                clbMiscellanenous.JSProperties["cpStatus"] = "INFO";
                pnlData.ClientVisible = false;
                ShowMessage("Selected MiscellaneousPayment type deleted successfully!!!");
            }
        }


        protected void LoadAllMiscellaneousPaymentAccounts()
        {
            MiscellaneousPaymentAccountBusiness objMiscellaneousPaymentAccountBusiness = new MiscellaneousPaymentAccountBusiness();
            List<MiscellaneousPaymentAccount> MiscellaneousPaymentList = objMiscellaneousPaymentAccountBusiness.GetMiscellaneousPaymentAccounts();
            if (MiscellaneousPaymentList != null)
            {
                gvwMiscellanenous.DataSource = MiscellaneousPaymentList;
                gvwMiscellanenous.DataBind();
                gvwMiscellanenous.ClientVisible = true;
            }
            else
            {
                gvwMiscellanenous.ClientVisible = false;
            }
        }

        protected void LoadMiscellaneousPaymentAccountInfo(int MiscellaneousPaymentTypeId)
        {
            clbMiscellanenous.JSProperties["cpAction"] = "Loaded";
            ASPxEdit.ClearEditorsInContainer(pnlData);
            MiscellaneousPaymentAccountBusiness objMiscellaneousPaymentAccountBusiness = new MiscellaneousPaymentAccountBusiness();
            MiscellaneousPaymentAccount MiscellaneousPaymentType = objMiscellaneousPaymentAccountBusiness.GetMiscellaneousPaymentAccount(MiscellaneousPaymentTypeId);
            if (MiscellaneousPaymentType != null)
            {
                ValidControlTrue();
                Session["MiscellaneousPaymentAccountId"] = MiscellaneousPaymentType.Id;
                txtDescription.Text = MiscellaneousPaymentType.Payment_reason_eng;
                txtAccountCode.Text = MiscellaneousPaymentType.Account_code;
                txtAmhDescription.Text = MiscellaneousPaymentType.Payment_reason;
                clbMiscellanenous.JSProperties["cpStatus"] = "SUCCESS";
            }
        }

        private void ValidControlTrue()
        {
            txtAmhDescription.IsValid = true;
            txtDescription.IsValid = true;
            txtAccountCode.IsValid = true;
        }

        protected void SaveEdit()
        {
            clbMiscellanenous.JSProperties["cpAction"] = "Save";

            int MiscellaneousPaymentAccountId = Convert.ToInt32(Session["MiscellaneousPaymentAccountId"]);
            MiscellaneousPaymentAccountBusiness objMiscellaneousPaymentAccountBusiness = new MiscellaneousPaymentAccountBusiness();

            MiscellaneousPaymentAccount objMiscellaneousPaymentAccount = new MiscellaneousPaymentAccount();
            objMiscellaneousPaymentAccount.Id = MiscellaneousPaymentAccountId;
            objMiscellaneousPaymentAccount.Payment_reason_eng = txtDescription.Text;
            objMiscellaneousPaymentAccount.Payment_reason = txtAmhDescription.Text;
            objMiscellaneousPaymentAccount.Account_name = txtAmhDescription.Text;
            objMiscellaneousPaymentAccount.Account_code = txtAccountCode.Text;
            objMiscellaneousPaymentAccount.Code = string.Empty;
            //if (objMiscellaneousPaymentAccountBusiness.Exists(objMiscellaneousPaymentAccount))
            //{
            //    ShowError("Lookup name already exist");
            //    return;
            //}

            if (objMiscellaneousPaymentAccountBusiness.UpdateMiscellaneousPaymentAccount(objMiscellaneousPaymentAccount))
            {
                //LoadSavedMiscellaneousPaymentAccount(Convert.ToInt32(objMiscellaneousPaymentAccount.ID));
                LoadAllMiscellaneousPaymentAccounts();
                clbMiscellanenous.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }
        }

        protected void SaveNew()
        {

            MiscellaneousPaymentAccountBusiness objMiscellaneousPaymentAccountBusiness = new MiscellaneousPaymentAccountBusiness();
            CUSTOR.Domain.MiscellaneousPaymentAccount objMiscellaneousPaymentAccount = new CUSTOR.Domain.MiscellaneousPaymentAccount();
            objMiscellaneousPaymentAccount.Payment_reason_eng = txtDescription.Text;
            objMiscellaneousPaymentAccount.Payment_reason = txtAmhDescription.Text;
            objMiscellaneousPaymentAccount.Account_code = txtAccountCode.Text;
            objMiscellaneousPaymentAccount.Account_name = txtAmhDescription.Text;
            objMiscellaneousPaymentAccount.Code = string.Empty;

            //if (objMiscellaneousPaymentAccountBusiness.Exists(objMiscellaneousPaymentAccount))
            //{
            //    ShowError("Lookup name already exist");
            //    return;
            //}
            bool registeredLookup = objMiscellaneousPaymentAccountBusiness.InsertMiscellaneousPaymentAccount(objMiscellaneousPaymentAccount);
            if (registeredLookup)
            {
                // DoNew();
                //LoadSavedMiscellaneousPaymentAccount(registeredLookup);
                LoadAllMiscellaneousPaymentAccounts();
                clbMiscellanenous.JSProperties["cpStatus"] = "SUCCESS";
                clbMiscellanenous.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }
        }

        private void ShowError(string strMsg)
        {
            clbMiscellanenous.JSProperties["cpMessage"] = strMsg;
            clbMiscellanenous.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbMiscellanenous.JSProperties["cpMessage"] = strMsg;
            clbMiscellanenous.JSProperties["cpStatus"] = "INFO";
        }

        private void ShowSuccess(string strMsg)
        {
            clbMiscellanenous.JSProperties["cpMessage"] = strMsg;
            clbMiscellanenous.JSProperties["cpStatus"] = "SUCCESS";
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        protected void Saved()
        {
            Page.Validate();
            if (!ASPxEdit.ValidateEditorsInContainer(clbMiscellanenous))
            {
                gvwMiscellanenous.ClientVisible = false;
                pnlData.ClientVisible = true;
                return;
            }
            clbMiscellanenous.JSProperties["cpAction"] = "Save";


            switch (Session["PageMode"].ToString())
            {
                case "NEW":
                    SaveNew();
                    break;
                case "EDIT":
                    SaveEdit();
                    break;
            }
        }

        protected void clbMiscellanenous_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            clbMiscellanenous.JSProperties["cpStatus"] = "";
            clbMiscellanenous.JSProperties["cpAction"] = "";
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    clbMiscellanenous.JSProperties["cpAction"] = "New";
                    DoNew();
                    clbMiscellanenous.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Save":
                    Saved();
                    break;
                case "EDITING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        LoadMiscellaneousPaymentAccountInfo(Convert.ToInt32(strID));
                        Session["PageMode"] = "EDIT";
                        //gvwMiscellanenous.Visible = true;
                    }
                    break;
                case "DELETING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        Session["ID"] = strID;
                        Delete(Convert.ToInt32(Session["ID"]));
                    }
                    break;
                case "Report":
                    Session["PrintReport"] = false;
                    //GenerateReport();
                    break;
                case "Cancel":
                    clbMiscellanenous.JSProperties["cpAction"] = "Cancel";
                    clbMiscellanenous.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Find":
                    DoFind();
                    pnlData.ClientVisible = false;
                    break;
                default:
                    break;
            }
        }

        private void DoFind()
        {
            clbMiscellanenous.JSProperties["cpAction"] = "Show";
            MiscellaneousPaymentAccountBusiness objLookupBus = new MiscellaneousPaymentAccountBusiness();
            try
            {
                List<CUSTOR.Domain.MiscellaneousPaymentAccount> list = objLookupBus.GetMiscellaneousPaymentAccounts();
                if (list.Count > 0)
                {
                    gvwMiscellanenous.DataSource = list;
                    gvwMiscellanenous.DataBind();
                    gvwMiscellanenous.ClientVisible = true;
                    ShowSuccess("");
                }
                else
                {
                    gvwMiscellanenous.ClientVisible = false;
                    ShowMessage("No data found!");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void gvwMiscellanenous_PageIndexChanged(object sender, EventArgs e)
        {
            LoadAllMiscellaneousPaymentAccounts();
        }
        private void FillLanguage(ASPxComboBox combo)
        {
            try
            {
                string qtext = "select * from NewsLanguage where IsActive = '1' AND LangId = 0";
                CDropdown.FillComboFromDB(combo, qtext, "Description", "Id");
            }
            catch
            {
            }
        }
        protected void gvwMiscellanenous_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
                LoadAllMiscellaneousPaymentAccounts();
        }
    }
}