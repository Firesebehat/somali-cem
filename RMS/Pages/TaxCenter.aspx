﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxCenter.aspx.cs" Inherits="RMS.Pages.TaxCenter" MasterPageFile="~/Root.master" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <script>
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoAddClientClick();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveClientClick();
            }
            else if (e.item.name === "mnuList") {
                DoListMode();
            }
            else if (e.item.name === "mnuDelete") {
                DoDeleteClick();
            }
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }
        function DoEditMode() {
            $('#pnlData').show();
            $('#pnlGrid').hide();
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuList", true);
            rdpData.SetVisible(true);
        }
        function DoListMode() {
            $('#pnlData').hide();
            $('#pnlGrid').show();
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuList", false);
            trlSite.PerformCallback();
            rdpData.SetVisible(true);
        }
        function DoAddClientClick() {
            
            DoEditMode();
            ToggleEnabled("mnuDelete", false);
            clbCenter.PerformCallback('New');
        }
        function DoDeleteClick() {
          
            ShowConfirmx('Are you sure you want to delete this record?', function (result) {
                if (result) {
                    clbCenter.PerformCallback('Delete');
                }
            },'en');
        }
        function DoSaveClientClick() {
            if (!ASPxClientEdit.ValidateGroup('VGSite')) {
                ShowError('One or more required data are not provided.');
                return;
            }
            clbCenter.PerformCallback('Save');
        }
        function DoSearchClick() {
            $('#pnlData').hide();
            $('#pnlGrid').show();
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuList", false);
            trlSite.PerformCallback('Search');
            rdpData.SetVisible(true);
        }
        function ActiveSiteChanged() {

        }
        function onclbCenterEndCallback(s, e) {
            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Delete") {
                    ShowSuccess(s.cpMessage);
                    DoListMode();
                    return;
                }
                else if (s.cpAction == "Save") {
                    ShowSuccess(s.cpMessage);
                    DoListMode();
                    
                }
                else if (s.cpAction == "Edit") {
                    DoEditMode();
                }
            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s) {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }

        function OnZoneChanged(cboZone) {
            cboWereda.ClearItems();
            cboKebele.ClearItems();
            cboWereda.PerformCallback(cboZone.GetValue().toString());
        }
        function OnWoredaChanged(cboWereda) {

            cboKebele.ClearItems();
            cboKebele.PerformCallback(cboWereda.GetValue().toString());
        }
        function onOrgTypeChanged(s) {
            var isParent = (s.GetSelectedIndex() == 0)
            cboParent.SetEnabled(!isParent);
            if (!isParent) {
                cboParent.PerformCallback(s.GetValue());
            }
        }
        function DoNodeClick(s, e) {
            DoFind(e.nodeKey);
        }
        function DoFind(values) {
            DoEditMode();
            clbCenter.PerformCallback('Edit|' + values);
        }
    </script>
  <style>
      .dxtlFocusedNode_Metropolis,.dxpLite_Metropolis .dxp-current 
        {
            background-color:#009688;
        }
  </style>
    
    <div class="contentmain" >
        <h5>
            Tax Center Management
        </h5>
        <div class="card card-body">

            <div style="max-width:800px">
        <script src="../Content/ModalDialog.js"></script>
       
        <div class="row">


            <div class="col-md-7" style="margin: 0px; padding: 0px;">
                <%--<dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" ClientInstanceName="btnNew" Text="New Site">
                    <ClientSideEvents Click="DoAddClientClick" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSave" Text="Save">
                    <ClientSideEvents Click="DoSaveClientClick" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnList" runat="server" AutoPostBack="False" ClientInstanceName="btnList" Text="Show List" ClientEnabled="true">
                    <ClientSideEvents Click="function(s, e) { DoListMode();}" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDelete" runat="server" AutoPostBack="False" ClientInstanceName="btnDelete" ClientVisible="false" Font-Size="Small" CausesValidation="false" ClientEnabled="false"
                    Text="Delete">
                    <ClientSideEvents Click="DoDeleteClick" />
                </dx:ASPxButton>--%>

                     <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                   <dx:MenuItem Name="mnuDelete" Text="Delete" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="diagramicons_del_svg_16x16"></Image>
                            </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>

            </div>
            <div class="col-md-5"style="margin:0px;padding:0px;" >
                <table style="width: 100%">
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" TabIndex="1" Width="100%" NullText="Search">
                                <ClientSideEvents KeyUp="function(s, e) {DoSearchClick();}" />
                            </dx:ASPxTextBox>
                        </td>
                        <td style="width: 55px">
                            <dx:ASPxButton ID="btnFind" runat="server" AutoPostBack="False" UseSubmitBehavior="false" CausesValidation="false" ClientInstanceName="btnFindClient" Width="50px">
                                <ClientSideEvents Click="function(s, e) {
	                                                                         DoSearchClick();
                                                                         }" />
                                <Image IconID="zoom_zoom_16x16">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
       <div style="padding-top:15px"></div>
        <dx:ASPxRoundPanel runat="server" ShowHeader="false" Width="100%" ClientVisible="false" ID="rdpData" ClientInstanceName="rdpData">
            <ContentPaddings Padding="0"/>
                <PanelCollection>
                    <dx:PanelContent>
                        <div style="padding:15px;width:100%">
                         <div class="row" style="width:100%">
            <div class="col col-md-12">


           
      <div id="pnlData" style="width:100%;">

            <dx:ASPxCallbackPanel ID="clbCenter" OnCallback="clbCenter_Callback" ClientInstanceName="clbCenter" runat="server" meta:resourcekey="clbCenterResource1">
                <ClientSideEvents EndCallback="onclbCenterEndCallback" />
                <PanelCollection>
                    <dx:PanelContent runat="server" SupportsDisabledAttribute="True" meta:resourcekey="pnlMainResource1">
                        <dx:ASPxHiddenField ID="hdfBusinessStatus" ClientInstanceName="hdfStatus" runat="server"></dx:ASPxHiddenField>
                        <dx:ASPxHiddenField runat="server" ID="hdfValue" ClientInstanceName="hdfValue"></dx:ASPxHiddenField>
                        <table style="margin: 0 auto; width: 100%">
                            <tbody>
                                <tr>
                                    <td colspan="2" style="vertical-align: top">
                                        <dx:ASPxLabel runat="server" Text="Site Name Amharic"></dx:ASPxLabel>
                                        <span class="star">*</span>
                                    </td>
                                    <td colspan="2" style="vertical-align: top">

                                        <dx:ASPxLabel runat="server" Text="Site Name English"></dx:ASPxLabel>
                                        <span class="star">*</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="vertical-align: top">
                                        <dx:ASPxTextBox runat="server" ID="txtCenterNameAmh" Width="100%">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Site Amharic is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                                <RegularExpression ErrorText="Use only amharic words" ValidationExpression="[ \u1200-\u137F \u0008 \/.\d]+$" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>

                                    </td>
                                    <td colspan="2" style="vertical-align: top">
                                        <dx:ASPxTextBox runat="server" ID="txtCenterNameEng" Width="100%">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Center Name in English is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                                <RegularExpression ValidationExpression="[ a-zA-Z \u0008 \/d \.]+$" ErrorText="Only english letters are allowed" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>

                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Organizational Hirachy"></dx:ASPxLabel>
                                        <span class="star">*</span>

                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Parent Oranization"></dx:ASPxLabel>
                                        <span class="star">*</span>
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Site Level"></dx:ASPxLabel>
                                        <span class="star">*</span>
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Site Language"></dx:ASPxLabel>
                                        <span class="star">*</span>
                                    </td>
                                    
                                </tr>
                                <tr>

                                    <td>
                                        <dx:ASPxComboBox ID="cboOrgType" runat="server" ClientInstanceName="chbOrgType" IncrementalFilteringMode="StartsWith" Width="100%" meta:resourcekey="cboRegionResource1" ValueType="System.Int32">
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { onOrgTypeChanged(s); }" />
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Organization type is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboParent" runat="server" ClientInstanceName="cboParent" OnCallback="cboParent_Callback" IncrementalFilteringMode="StartsWith" Width="100%" ValueType="System.Int32">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Parent organization is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboSiteLeve" runat="server" ClientInstanceName="cboSiteLeve" IncrementalFilteringMode="StartsWith" Width="100%" meta:resourcekey="cboRegionResource1" ValueType="System.Int16">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Site leve is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboLanguage" runat="server" ClientInstanceName="cboLanguage" IncrementalFilteringMode="StartsWith" Width="100%" ValueType="System.Int32">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Parent organization is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    
                                </tr>
                                <%--<tr>
                                        <td colspan="4">
                                            <hr />
                                        </td>
                                    </tr>--%>
                                <tr>
                                    <td>
                                        <dx:ASPxLabel ID="lblRegion" runat="server" Text="Region"  meta:resourcekey="lblRegionResource1"></dx:ASPxLabel>
                                        <span class="star">*</span>

                                    </td>
                                     <td>
                                            <dx:ASPxLabel ID="lblZone" runat="server" Text="City/Zone"  meta:resourcekey="lblZoneResource1"></dx:ASPxLabel>
                                            <span class="star">*</span>

                                        </td>
                                    <td>
                                        <dx:ASPxLabel ID="lblSubcity" runat="server" Text="Subcity" meta:resourcekey="lblWeredaResource1"></dx:ASPxLabel>
                                        <span class="star">*</span>

                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="lblKebele" runat="server" Text="Woreda" meta:resourcekey="lblKebeleResource1"></dx:ASPxLabel>
                                        <span class="star">*</span>

                                    </td>

                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion" IncrementalFilteringMode="StartsWith" Width="100%" ValueType="System.String">
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { cboZone.PerformCallback(s.GetValue()); }" />
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Region is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                            <dx:ASPxComboBox ID="cboZone" runat="server" ClientInstanceName="cboZone" EnableClientSideAPI="True" Width="100%" IncrementalFilteringMode="StartsWith" OnCallback="cboZone_Callback" ValueType="System.Int32">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnZoneChanged(s); }" />
                                                <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Zone is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboWereda" runat="server" ClientInstanceName="cboWereda" EnableClientSideAPI="True" Width="100%" IncrementalFilteringMode="StartsWith" OnCallback="cboWereda_Callback" ValueType="System.String">
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnWoredaChanged(s); }" />
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Woreda is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="cboKebele" runat="server" ClientInstanceName="cboKebele" Width="100%" EnableClientSideAPI="True" IncrementalFilteringMode="StartsWith" OnCallback="cboKebele_Callback" ValueType="System.String">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Kebele is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td></td>

                                </tr>
                                <tr>

                                    <td>
                                        <dx:ASPxLabel ID="Label3" runat="server" Text="Telephone"></dx:ASPxLabel>
                                        <span class="star">*</span>

                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="E-mail" meta:resourcekey="Label3Resource1"></dx:ASPxLabel>


                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="POBox" meta:resourcekey="Label3Resource1"></dx:ASPxLabel>


                                    </td>
                                    <td></td>
                                </tr>
                                <tr>

                                    <td style="vertical-align:top">
                                        <dx:ASPxTextBox ID="txtTell" runat="server" ClientInstanceName="txtTell" meta:resourcekey="txtTellResource1">
                                            <MaskSettings Mask="(+251) (000) 000000"  IncludeLiterals="None" ErrorText="Invalid character"/>
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Tel no. is required" ErrorDisplayMode="Text" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td style="vertical-align:top">
                                        <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="txtEmail" meta:resourcekey="txtOtherAddressResource1" Width="100%">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="Email is required" ErrorDisplayMode="Text" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                  <RegularExpression ErrorText="Invalid Email!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                <RequiredField IsRequired="false" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td style="vertical-align:top">
                                        <dx:ASPxTextBox ID="txtPobox" runat="server" ClientInstanceName="txtPobox" meta:resourcekey="txtOtherAddressResource1" Width="100%">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="PO Box is required" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VGSite" Display="Dynamic">
                                                <RequiredField IsRequired="false" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxCheckBox Style="float: right" runat="server" ID="chbIsActive" TextAlign="Right" Text="Is Active Now"></dx:ASPxCheckBox>

                                    </td>
                                   

                                </tr>
                              
                            </tbody>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>

                 
      
                   
        </div>
 </div>
        </div>
        <div id="pnlGrid" style="display:none">
            <table>
                <tr>
                    <td>
                        <dx:ASPxCheckBox Text="Active Sites Only" runat="server" ID="chActiveSites" ClientInstanceName="chActiveSites" Checked="true">
                            <ClientSideEvents CheckedChanged="ActiveSiteChanged" />
                        </dx:ASPxCheckBox>
                    </td>
                    <td style="width: 15px">&nbsp;
                    </td>

                    <td>

                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="By site type:" Style="color: rgb(0,0,0)"></dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cboOrgTypeSearch" ClientInstanceName="cboUnitType" runat="server" NullText="Select Site Type" ValueType="System.String">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                         DoSearchClick();
                                                                         }" />
                        </dx:ASPxComboBox>
                    </td>
                </tr>
            </table>

            <div style="padding-top: 10px"></div>
            <dx:ASPxTreeList ID="trlSite" runat="server" AutoGenerateColumns="False" Theme="Metropolis" OnInit="trlSite_Init"
                ClientInstanceName="trlSite" EnableTheming="True" Font-Names="Visual Geez Unicode"
                KeyFieldName="Id" ParentFieldName="Parent" OnCustomCallback="trlSite_CustomCallback" OnNodeExpanding="trlSite_NodeExpanding">
                <Columns>
                    <%--<dx:TreeListTextColumn Caption="Id" FieldName="Id" ShowInCustomizationForm="True" VisibleIndex="1" Visible="false">
                    </dx:TreeListTextColumn>--%>
                    <dx:TreeListTextColumn Caption="Site" FieldName="CenterNameEnglish" VisibleIndex="0">
                    </dx:TreeListTextColumn>
                </Columns>
                <SettingsPager Mode="ShowPager" PageSize="15">
                    <PageSizeItemSettings Items="20, 30, 50" Visible="False">
                    </PageSizeItemSettings>
                </SettingsPager>
                <%--  <Settings GridLines="None" ShowFooter="True" />
                <SettingsBehavior AllowFocusedNode="True" />
                

                <SettingsCustomizationWindow PopupHorizontalAlign="RightSides" PopupVerticalAlign="BottomSides"></SettingsCustomizationWindow>

                <SettingsLoadingPanel ImagePosition="Top" />

                <SettingsPopupEditForm VerticalOffset="-1"></SettingsPopupEditForm>

                <SettingsPopup>
                    <EditForm VerticalOffset="-1"></EditForm>
                </SettingsPopup>--%>

<SettingsPopup>
<HeaderFilter MinHeight="140px"></HeaderFilter>
</SettingsPopup>

                <ClientSideEvents NodeDblClick="function(s,e){
                                                                DoNodeClick(s,e);
                                                            }" />
            </dx:ASPxTreeList>
            </div>
                            </div>
           </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxRoundPanel>
    </div>
    </div>
        <uc1:Alert runat="server" ID="Alert" /> 

    </div>
</asp:Content>
