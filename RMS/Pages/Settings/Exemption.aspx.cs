﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class Exemption : System.Web.UI.Page
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack && !Page.IsCallback)
            {
                FillTaxTypes();
                SetDefaltValue();
            }
        }
        public void SetDefaltValue()
        {
            Session["IsNewExemptionRecord"] = true;
            txtTaxPercent.Value = 100;
        }
       
        private void FillTaxTypes()
        {
            TaxTypeBusiness business = new TaxTypeBusiness();
            List<CUSTOR.Domain.TaxType> taxTypes = new List<CUSTOR.Domain.TaxType>();
            taxTypes = business.GetServiceTaxTypes();//business.GetTaxAccountList(Convert.ToInt32(Session["TaxpayerId"]);
            cboTaxType.DataSource = taxTypes;
            cboTaxType.TextField = "TaxTypeName";
            cboTaxType.ValueField = "Id";
            cboTaxType.DataBind();
        }

        protected void cbpClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string id = "0";
            string[] parameters = e.Parameter.Split('|');
            if (parameters.Length > 1)
                id = parameters[1];
            switch(parameters[0])
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Edit":
                    DoFind(id);
                    break;
                case "Delete":
                    DeleteExemption();
                    break;
            }
        }
        private void DoFind(string id)
        {
            clbClient.JSProperties["cpAction"] = "Edit";
            Session["ExemptionId"] = id;
            int exemptionId = Int32.Parse(id.ToString());
            TaxExemptionBussiness objBusiness = new TaxExemptionBussiness();
            TaxExemption objExemption = objBusiness.GetTaxExemption(exemptionId);

            txtTaxPercent.Text  = objExemption.TaxPercent == 0 ? string.Empty : objExemption.TaxPercent.ToString();
            txtAmount.Text = objExemption.TaxAmount == 0 ? string.Empty : objExemption.TaxAmount.ToString();
            txtMinIncome.Text = objExemption.IncomeFrom == 0 ? string.Empty : objExemption.IncomeFrom.ToString();
            txtMaxIncome.Text = objExemption.IncomeTo == 0 ? string.Empty : objExemption.IncomeTo.ToString();
            dpAppliedDateFrom.SelectedDate = objExemption.AppliedDateFrom == DateTime.MinValue ? string.Empty:
            CUSTORCommon.Ethiopic.EthiopicDateTime.GetEthiopianDate(objExemption.AppliedDateFrom);
            dpAppliedDateTo.SelectedDate = objExemption.AppliedDateTo == DateTime.MinValue ? string.Empty :
            CUSTORCommon.Ethiopic.EthiopicDateTime.GetEthiopianDate(objExemption.AppliedDateTo);
            cboTaxType.Value = objExemption.TaxType.ToString();

            //select grid lookup
            List<TaxExemptedCategory> exemptedCategories = objBusiness.GetExemptedCategoryList(exemptionId.ToString());
            List<string> selectionIdList = new List<string>();
            string selectionViewText = string.Empty;

            foreach (var item in exemptedCategories)
            {
                selectionIdList.Add(item.CategoryId.ToString());
                selectionViewText += item.CategoryCode + ",";
            }
            txtCaxtegory.Text = selectionViewText.TrimEnd(',');
            if(selectionIdList.Count > 0)
            BusinessCategoryControl.SelectTree(selectionIdList);

            //select taxpayer grades


            BindGrades();
            grlTaxPayerGrade.GridView.Selection.UnselectAll();
            List<string> selectionId = new List<string>();
            List<ExemptedTaxpayerGrade> taxpayers = objBusiness.GetExemptedGradeList(Int32.Parse(id));
            foreach (var item in taxpayers)
            {
                //grlTaxPayerGrade.GridView.Selection.SelectRowByKey(item.Id.ToString());
                selectionId.Add(item.TaxpayerGrade.ToString());


            }
            grlTaxPayerGrade.Value = (object)selectionId;

            Session["ExemptionId"] = id;
            Session["IsNewExemptionRecord"] = false;
            ShowSuccess("");
        }

        private void DoNew()
        {
            clbClient.JSProperties["cpAction"] = "New";
            txtCaxtegory.Text = string.Empty;
            txtTaxPercent.Text = string.Empty;
            txtMinIncome.Text = string.Empty;
            txtMaxIncome.Text = string.Empty;
            dpAppliedDateFrom.SelectedDate = string.Empty;
            dpAppliedDateTo.SelectedDate = string.Empty;
            txtAmount.Text = string.Empty;
            grlTaxPayerGrade.GridView.Selection.UnselectAll();
            cboTaxType.SelectedIndex = -1;
            BusinessCategoryControl.ResetTree();
            SetDefaltValue();
            ShowSuccess("");
        }
        private void DoSave()
        {
            clbClient.JSProperties["cpAction"] = "Save";
            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                ShowError(GetErrorDisplay(errMessages));
                return;
            }
            TaxExemptionBussiness objBusiness = new TaxExemptionBussiness();
            TaxExemption objExemption = new TaxExemption();
            objExemption.TaxPercent = txtTaxPercent.Text==string.Empty ? 0: Convert.ToDouble(txtTaxPercent.Text);
            objExemption.TaxAmount = txtAmount.Text==string.Empty ? 0: Convert.ToDouble(txtAmount.Text);
            objExemption.IncomeFrom = txtMinIncome.Text == string.Empty ? 0 : decimal.Parse(txtMinIncome.Text);
            objExemption.IncomeTo = txtMaxIncome.Text == string.Empty ? 0 : decimal.Parse(txtMaxIncome.Text);
            objExemption.AppliedDateFrom = dpAppliedDateFrom.SelectedDate == string.Empty ? DateTime.MinValue : dpAppliedDateFrom.SelectedGCDate;
            objExemption.AppliedDateTo = dpAppliedDateTo.SelectedDate == string.Empty ? DateTime.MinValue : dpAppliedDateTo.SelectedGCDate;
            objExemption.Categories = GetSelectedCategories();
            objExemption.TaxType = Convert.ToInt32(cboTaxType.Value);
            objExemption.TaxpyerGrades = GetSelectedGrades();

            DateTime eventDate = DateTime.Now;
            CProfile userProfile = CurrentProfile.ActiveUserProfile;
            
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            connection.Open();
            SqlTransaction tran = connection.BeginTransaction();
            command.Transaction = tran;

            try
            {
                if (!(bool)Session["IsNewExemptionRecord"])
                {
                    //first delete existing record
                    int exemptionId = Int32.Parse(Session["ExemptionId"].ToString());
                    objExemption.Id = exemptionId;
                    objExemption.UpdatedBy = this.Page.User.Identity.Name;
                    objExemption.UpdatedDate = eventDate;
                    objExemption.UpdatedUserId = new Guid(userProfile.UserId);

                    objBusiness.DeleteExemptionChildTables(exemptionId,command);
                    objBusiness.UpdateTaxExemption(objExemption, command);
                }
                else
                {
                    objExemption.CreatedUserId = new Guid(userProfile.UserId);
                    objExemption.CenterId = userProfile.TaxCenterId;
                    objExemption.CreatedBy = this.Page.User.Identity.Name;
                    objExemption.CreatedDate = eventDate;
                    objExemption.CenterName = userProfile.Name;
                    objBusiness.InsertTaxExemption(objExemption, command);
                }

                //Insert grades
                List<object> selectedGrades = grlTaxPayerGrade.GridView.GetSelectedFieldValues("Id");
                foreach (object item in selectedGrades)
                {
                    objExemption.TaxPayerGrade = (int)item;
                    objBusiness.InsertExemptedTaxpayers(objExemption, command);
                }
                //Insert categories
                string[] selectedCategories = txtCaxtegory.Text.Split(',');
                foreach (object item in selectedCategories)
                {
                    string categorCode = Convert.ToString(item);
                    objExemption.CategoryId = GetCategoryId(categorCode);
                    objBusiness.InsertExemptedCategories(objExemption, command);
                }
                tran.Commit();
                ShowSuccess("The record is successfully saved!");
                
            }
            catch(Exception ex)
            {
                try
                {
                    tran.Rollback();
                    ShowError(ex.Message);
                }
                catch
                {

                }
            }

            
            
        }
        private int GetCategoryId(string code)
        {
            CategoryBusiness business = new CategoryBusiness();
            return (business.GetCategoryByCode(code)).Id;
        }

        private List<int> GetSelectedGrades()
        {
           
            List<object> selectedTaxes = grlTaxPayerGrade.GridView.GetSelectedFieldValues("Id");
            List<int> keys = new List<int>();
            foreach (object item in selectedTaxes)
            {
                keys.Add((int)item);
            }
            return keys;
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();
            if (cboTaxType.Value == null)
            {
                errMessages.Add("Please select the tax type");
            }
            var objSelection = grlTaxPayerGrade.GridView.Selection;
            if (objSelection == null || objSelection.Count == 0)
            {
                errMessages.Add("Please select the taxpayer grade");
            }
            if (txtTaxPercent.Text == string.Empty && txtAmount.Text == string.Empty)
            {
                errMessages.Add("Please enter the exemption amount in percent or amount.");
                
            }
            if(txtTaxPercent.Text != string.Empty)
            {
                if(txtAmount.Text !=string.Empty )
                {
                    if(Convert.ToDouble(txtAmount.Text) == 0 &&  Convert.ToDouble(txtTaxPercent.Text)==0)
                    {
                        errMessages.Add("Please enter either amount of exemption, amount or percent.");

                    }
                }
            }
            else if (txtAmount.Text != string.Empty)
            {
                if (txtTaxPercent.Text != string.Empty)
                {
                    if (Convert.ToDouble(txtAmount.Text) == 0 && Convert.ToDouble(txtTaxPercent.Text) == 0)
                    {
                        errMessages.Add("Please enter either amount of exemption, amount or percent.");

                    }
                }
            }
            if (dpAppliedDateFrom.SelectedDate == string.Empty)
            {
                errMessages.Add("Please enter 'Applied Date From' field");
            }
            if (dpAppliedDateTo.SelectedDate != string.Empty)
            {
                try
                {
                    if (!dpAppliedDateTo.IsValidDate)
                    {
                        errMessages.Add("Please enter a valid ending date");
                    }
                }
                catch
                {
                    errMessages.Add("Please enter a valid ending date");
                }
            }
            if (dpAppliedDateFrom.SelectedDate != string.Empty)
            {
                try
                {
                    if (!dpAppliedDateFrom.IsValidDate)
                    {
                        errMessages.Add("Please enter a valid min date");
                    }
                }
                catch
                {
                    errMessages.Add("Please enter a valid min date");
                }
            }
                return errMessages;
        }
        private List<CategoryDomain> GetSelectedCategories()
        {
            string[] categoryCodes = txtCaxtegory.Text.Split(',');
            List<CategoryDomain> selectedCategories = new List<CategoryDomain>();
            for (int i=0; i<categoryCodes.Length;i++)
            {
                CategoryDomain objCat = new CategoryDomain();
                objCat.Code = categoryCodes[i];
                selectedCategories.Add(objCat);
            }
            return selectedCategories;
        }
       
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
      
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        protected void grlTaxPayerGrade_Init(object sender, EventArgs e)
        {
            BindGrades();
        }
        private void BindGrades()
        {
            grlTaxPayerGrade.DataSource = GetGradeList();
            grlTaxPayerGrade.DataBind();
        }
        private List<Grade> GetGradeList()
        {

            List<Grade> grades = new List<Grade>();
            Grade objgrad = new Grade()
            {
                Name = "Grade A",
                Id = (int)Enums.eGrade.A
            };
            grades.Add(objgrad);
            objgrad = new Grade()
            {
                Name = "Grade B",
                Id = (int)Enums.eGrade.B
            };
            grades.Add(objgrad);
            objgrad = new Grade()
            {
                Name = "Grade C",
                Id = (int)Enums.eGrade.C
            };
            grades.Add(objgrad);

            return grades;
        }

        protected void grdList_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            BindGrid();
        }

        private void DeleteExemption()
        {
            int exemptionId = Int32.Parse(Session["ExemptionId"].ToString());
            clbClient.JSProperties["cpAction"] = "Delete";
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            connection.Open();
            SqlTransaction tran = connection.BeginTransaction();
            TaxExemptionBussiness bussiness = new TaxExemptionBussiness();
            try
            {
                command.Transaction = tran;
                bussiness.Delete(exemptionId, command);
                tran.Commit();
                ShowSuccess("The record is deleted successfully");
            }
            catch (Exception ex)
            {
                if(tran !=null)
                tran.Rollback();
                ShowError(ex.Message);
            }

        }

        protected void grdCategory_DetailRowExpandedChanged(object sender, DevExpress.Web.ASPxGridViewDetailRowEventArgs e)
        {

        }
        protected void BindGrid()
        {
            TaxExemptionBussiness bussiness = new TaxExemptionBussiness();
            List<TaxExemption> taxExemptions = bussiness.GetExemptionDetails();
            grdList.DataSource = taxExemptions;
            grdList.DataBind();

        }
    }
    class Grade
    {
        public string Name { get; set; }
        public  int Id { get; set; }
    }
}
