﻿using CUSTOR.Bussiness;
using CUSTORCommon.Ethiopic;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class IncomeTaxRate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback && !Page.IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                DateTime date = DateTime.Now;
               txtBudgetYear.Text = EthiopicDateTime.GetEthiopicYear(date.Day,date.Month,date.Year).ToString();
               BindGrid();
               if (grdTaxPayer.VisibleRowCount > 0)
                    grdTaxPayer.ClientVisible = true;
            }
        }

        protected void grdTaxPayer_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtBudgetYear.Text == string.Empty)
            {
                grdTaxPayer.JSProperties["cpMessage"] = "Budget year is required";
                return;
            }
            string[] params_ = e.Parameters.Split('|');
            if (params_[0]=="Delete")
            {
                int id = Convert.ToInt32(params_[1]);
                Delete(id);
            }
            BindGrid();

        }
        private void BindGrid()
        {
            IncomeTaxRateBussiness bussiness = new IncomeTaxRateBussiness();
            try
            {
                int year = Convert.ToInt32(txtBudgetYear.Text);
                List<CUSTOR.Domain.IncomeTaxRate> list = new List<CUSTOR.Domain.IncomeTaxRate>();
                list = bussiness.GetIncomeTaxRates(year);
                if (list.Count > 0)
                {
                    grdTaxPayer.DataSource = list;
                }
                else
                {
                    grdTaxPayer.JSProperties["cpMessage"] = "NA";
                    grdTaxPayer.DataSource = null;
                }
                grdTaxPayer.DataBind();
                grdTaxPayer.JSProperties["cpStatus"] = "SUCCESS";

            }
            catch (Exception ex)
            {
                grdTaxPayer.JSProperties["cpMessage"] = ex.Message;
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";

            }
        }

       
      

        protected void grdTaxPayer_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            grdTaxPayer.JSProperties["cpAction"] = "Save";
            var IncomeFrom = e.NewValues["IncomeFrom"];
            var IncomeTo = e.NewValues["IncomeTo"];
            var DateFromText = e.NewValues["DateFromText"];
            string errText = string.Empty;
            /* Validate data*/
            #region Validation
            if (IncomeFrom == null)
                errText = "Enter the minimum salary amount.";
            else if (IncomeFrom.ToString().Length > Math.Pow(10d, 6d))
                errText = "Very larg minimum salary amount";
            if (IncomeTo == null || Convert.ToDouble(IncomeTo) == 0.00d)
                errText = "Enter the maximum salary amount";
            else if (IncomeTo.ToString().Length > Math.Pow(10d, 6d))
                errText = "Very large maximum salary amount";
            else if (DateFromText == null)
                errText = "Enter the Date From item";
            if (errText != string.Empty)
            {
                grdTaxPayer.JSProperties["cpMessage"] = errText;
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                e.Cancel = true;
                return;
            }
            DateTime dateFrom;
            try
            {
                string[] str = DateFromText.ToString().Split('/');
                dateFrom = EthiopicDateTime.GetGregorianDate(Convert.ToInt32(str[0]), Convert.ToInt32(str[1]), Convert.ToInt32(str[2]));

            }
            catch
            {
                errText += "Invalid Date";
                grdTaxPayer.JSProperties["cpMessage"] = errText;
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                e.Cancel = true;
                return;
            }

            #endregion

            try
            {
                DateTime evenDate = DateTime.Now;
               CUSTOR.Domain. IncomeTaxRate rate = new CUSTOR.Domain.IncomeTaxRate();
                rate.IncomeFrom = Convert.ToDouble(IncomeFrom);
                rate.IncomeTo = Convert.ToDouble(IncomeTo);
                rate.DateFrom = dateFrom;
                rate.DateTo = DateTime.MinValue;
                rate.UpdatedBy = User.Identity.Name;
                rate.UpdatedDate = evenDate;
                rate.Id = Convert.ToInt32(e.Keys[0]);
                IncomeTaxRateBussiness bussiness = new IncomeTaxRateBussiness();
                if (bussiness.UpdateExists(rate))
                {
                    grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                    grdTaxPayer.JSProperties["cpMessage"] = "ያስገቡት መረጃ ከዚህ በፊት የገባ ነው";
                    e.Cancel = true;
                    return;
                }


                bussiness.UpdateIncomeTaxRate(rate);
                e.Cancel = true;
                grdTaxPayer.JSProperties["cpAction"] = "Save";
                grdTaxPayer.JSProperties["cpStatus"] = "SUCCESS";
                grdTaxPayer.JSProperties["cpMessage"] = "The record is successfully saved";
                grdTaxPayer.CancelEdit();
            }
            catch (Exception ex)
            {
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                grdTaxPayer.JSProperties["cpMessage"] = ex.Message;
                e.Cancel = true;
                return;
            }
        }

        protected void grdTaxPayer_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            grdTaxPayer.JSProperties["cpAction"] = "Save";
            var IncomeFrom = e.NewValues["IncomeFrom"];
            var IncomeTo = e.NewValues["IncomeTo"];
            var DateFromText = e.NewValues["DateFromText"];
            string errText = string.Empty;

            /* Validate data*/
            #region Validation
            if (IncomeFrom == null )
                errText = "Enter the minimum salary amount.";
            else if (IncomeFrom.ToString().Length > Math.Pow(10d, 6d))
                errText = "Very larg minimum salary amount";
            if (IncomeTo == null || Convert.ToDouble(IncomeTo) == 0.00d)
                errText = "Enter the maximum salary amount";
            else if (IncomeTo.ToString().Length > Math.Pow(10d, 6d))
                errText = "Very large maximum salary amount";
            else if (DateFromText == null)
                errText = "Enter the Date From item";
            if (errText != string.Empty)
            {
                grdTaxPayer.JSProperties["cpMessage"] = errText;
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                e.Cancel = true;
                return;
            }
            DateTime dateFrom;
            try
            {
                string[] str = DateFromText.ToString().Split('/');
                dateFrom = EthiopicDateTime.GetGregorianDate(Convert.ToInt32(str[0]), Convert.ToInt32(str[1]), Convert.ToInt32(str[2]));

            }
            catch
            {
                errText += "Invalid Date";
                grdTaxPayer.JSProperties["cpMessage"] = errText;
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                e.Cancel = true;
                return;
            }
            
            #endregion

            try
            {
                DateTime evenDate = DateTime.Now;
                CUSTOR.Domain.IncomeTaxRate rate = new CUSTOR.Domain.IncomeTaxRate();
                rate.IncomeFrom = Convert.ToDouble(IncomeFrom);
                rate.IncomeTo = Convert.ToDouble(IncomeTo);
                rate.DateFrom = dateFrom;
                rate.DateTo = DateTime.MinValue;
                rate.CreatedBy = User.Identity.Name;
                rate.CreatedDate = evenDate;
                rate.BudgetYear = Convert.ToInt32(txtBudgetYear.Text);
                IncomeTaxRateBussiness bussiness = new IncomeTaxRateBussiness();
                //Check existense of some values in the same date range
                if (bussiness.Exists(rate))
                {
                    grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                    grdTaxPayer.JSProperties["cpMessage"] = "Record exst.";
                    e.Cancel = true;
                    return;
                }
                bussiness.InsertIncomeTaxRate(rate);
                e.Cancel = true;
                grdTaxPayer.JSProperties["cpAction"] = "Save";
                grdTaxPayer.JSProperties["cpStatus"] = "SUCCESS";
                grdTaxPayer.JSProperties["cpMessage"] = "Record is successfully saved";
                grdTaxPayer.CancelEdit();
            }
            catch (Exception ex)
            {
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                grdTaxPayer.JSProperties["cpMessage"] = ex.Message;
                e.Cancel = true;
                return;
            }
        }

        private void Delete(int Id)
        {
            try
            {
                grdTaxPayer.JSProperties["cpAction"] = "Delete";
                IncomeTaxRateBussiness bussiness = new IncomeTaxRateBussiness();
                bussiness.Delete(Id);
                grdTaxPayer.JSProperties["cpMessage"] = "Record is successfully deleted";
                grdTaxPayer.JSProperties["cpStatus"] = "SUCCESS";
            }
            catch (Exception ex)
            {
                grdTaxPayer.JSProperties["cpStatus"] = "ERROR";
                grdTaxPayer.JSProperties["cpMessage"] = ex.Message;
            }
        }
    }
}