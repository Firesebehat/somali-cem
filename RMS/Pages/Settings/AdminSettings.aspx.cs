﻿using CUSTOR.Business;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages.Settings
{
    public partial class AdminSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Administrator")
                     || HttpContext.Current.User.IsInRole("Super Admin")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                PopulateSettings();
            }
        }

        private void PopulateSettings()
        {
            int siteId = CurrentProfile.ActiveUserProfile.TaxCenterId;
            AdminSettingsBusiness adminSettingsBusiness = new AdminSettingsBusiness();
            List<AdminSettingss> settingss = new List<AdminSettingss>();
            settingss = adminSettingsBusiness.GetAdminSettingss(siteId);

            //for enable assessment date
            var key = settingss.FirstOrDefault(x => x.Key == Enums.eAdminSettingTypes.EnableChangingAssessmentDate.ToString());
            cboEnableAsstDate.Value = key != null ? key.Value : "0";
        }

        protected void clbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strKey = string.Empty;
            string strValue = "";
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
            {
                strKey = parameters[1];
                strValue = parameters[2];
            }
            switch (strParam)
            {
                case "Save":
                    DoSave(strKey, strValue);
                    break;
            }
        }

        private void DoSave(string strKey, string val)
        {
            clbClient.JSProperties["cpAction"] = "Save";
            string key = strKey;
            AdminSettingss adminSettings = new AdminSettingss();
            adminSettings.Key = key;
            adminSettings.Value = val;
            adminSettings.SiteId = CurrentProfile.ActiveUserProfile.TaxCenterId;
            AdminSettingsBusiness adminSettingBusiness = new AdminSettingsBusiness();
            AdminSettingss oldSetting = adminSettingBusiness.GetAdminSettings(key,adminSettings.SiteId);

            AdminSettingHistory settingHistory = new AdminSettingHistory();
            settingHistory.OldValue = oldSetting.Value;
            settingHistory.NewValue = val;
            settingHistory.CreatedBy = Membership.GetUser().ProviderUserKey.ToString();
            settingHistory.CreatedByName = Page.User.Identity.Name;
            settingHistory.CreatedDate = DateTime.Now;
            settingHistory.SettingId = CurrentProfile.ActiveUserProfile.TaxCenterId;
            try
            {
                adminSettingBusiness.UpdateAdminSettings(adminSettings, settingHistory);
                ShowSuccess("Setting saved successfully");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }

        protected void gvHstory_PageIndexChanged(object sender, EventArgs e)
        {
            BindHistory();
        }

        protected void gvHstory_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            BindHistory();
        }

        private void BindHistory()
        {
            int siteId = 0;
            if(!User.IsInRole("Super Admin"))
            siteId= CurrentProfile.ActiveUserProfile.TaxCenterId;
            try
            {
                AdminSettingHistoryBusiness settingHistoryBusiness = new AdminSettingHistoryBusiness();
                List<AdminSettingHistory> list = settingHistoryBusiness.GetAdminSettingHistorys(siteId);
                if (list.Count > 0)
                    gvHstory.DataSource = list;
                else
                    gvHstory.DataSource = null;
                gvHstory.DataBind();
            }
            catch
            {

            }
        }
    }
}