﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;

namespace RMS.Pages.Settings
{
    public partial class TaxReduction : System.Web.UI.Page
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                FillTaxTypes();
                SetDefaltValue();
            }
        }
        public void SetDefaltValue()
        {
            Session["IsNewRecord"] = true;
        }

        private void FillTaxTypes()
        {
            TaxTypeBusiness business = new TaxTypeBusiness();
            List<CUSTOR.Domain.TaxType> taxTypes = new List<CUSTOR.Domain.TaxType>();
            taxTypes = business.GetTaxTypes(TaxCategory.eCorporateTax, 0);//business.GetTaxAccountList(Convert.ToInt32(Session["TaxpayerId"]);
            cboTaxType.DataSource = taxTypes;
            cboTaxType.TextField = "TaxTypeName";
            cboTaxType.ValueField = "Id";
            cboTaxType.DataBind();
        }

        protected void cbpClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            int Id = 0;
            string[] parameters = e.Parameter.Split('|');
            if(parameters.Length > 1)
                 Id = Int32.Parse(parameters[1]);
            switch (parameters[0])
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Delete":
                    DeleteReduction();
                    break;
                case "Edit":
                    DoFind(Id);
                    break;
            }
        }

        private void DoFind(int id)
        {
            clbClient.JSProperties["cpAction"] = "Edit";
            Session["TaxRedcutionId"] = id;
            Session["NewReductionRecord"] = false;
            RawTaxReductionBussiness objBusiness = new RawTaxReductionBussiness();
            RawTaxReductionDomain rawTaxReductions = objBusiness.GetRawTaxReduction(id);
            txtTaxPercent.Text = rawTaxReductions.RawTaxReductionPercent==0?string.Empty: rawTaxReductions.RawTaxReductionPercent.ToString();
            dpAppliedDateFrom.SelectedDate = rawTaxReductions.AppliedDateFrom == DateTime.MinValue ?
                string.Empty : CUSTORCommon.Ethiopic.EthiopicDateTime.GetEthiopianDate(rawTaxReductions.AppliedDateFrom);
            dpAppliedDateTo.SelectedDate = rawTaxReductions.AppliedDateTo == DateTime.MinValue ?
                string.Empty : CUSTORCommon.Ethiopic.EthiopicDateTime.GetEthiopianDate(rawTaxReductions.AppliedDateTo);
            cboTaxType.Value = rawTaxReductions.TaxType.ToString();

            BindGrades();
            grlTaxPayerGrade.GridView.Selection.UnselectAll();
            List<string> selectionId = new List<string>();
            List<RawTaxReductionTaxpayer> taxpayers = objBusiness.GetReducedTaxPayersList(id);
            foreach(var item in taxpayers)
            {
                //grlTaxPayerGrade.GridView.Selection.SelectRowByKey(item.Id.ToString());
                selectionId.Add(item.TaxpayerGrade.ToString());

            }
            grlTaxPayerGrade.Value = (object)selectionId;
            Session["TaxRedcutionId"] = id;
            Session["NewReductionRecord"] = false; 
            ShowSuccess("");
        }
        private void DoSave()
        {
            clbClient.JSProperties["cpAction"] = "Save";
            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                ShowError(GetErrorDisplay(errMessages));
                return;
            }
            RawTaxReductionBussiness objBusiness = new RawTaxReductionBussiness();
            RawTaxReductionDomain objReduction = new RawTaxReductionDomain();
            objReduction.RawTaxReductionPercent = txtTaxPercent.Text == string.Empty ? 0 : Convert.ToDouble(txtTaxPercent.Text);
            objReduction.AppliedDateFrom = dpAppliedDateFrom.SelectedDate == string.Empty ? DateTime.MinValue : dpAppliedDateFrom.SelectedGCDate;
            objReduction.AppliedDateTo = dpAppliedDateTo.SelectedDate == string.Empty ? DateTime.MinValue : dpAppliedDateTo.SelectedGCDate;
            objReduction.TaxType = Convert.ToInt32(cboTaxType.Value);

            DateTime eventDate = DateTime.Now;
            CProfile userProfile = CurrentProfile.ActiveUserProfile;
            objReduction.CreatedUserId = new Guid(userProfile.UserId);
            objReduction.CenterId = userProfile.TaxCenterId;

            objReduction.CreatedBy = this.Page.User.Identity.Name;
            objReduction.CreatedDate = eventDate;
            objReduction.CenterName = userProfile.Name;
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            connection.Open();
            SqlTransaction tran = connection.BeginTransaction();
            command.Transaction = tran;
            List<object> selectedGrades = grlTaxPayerGrade.GridView.GetSelectedFieldValues("Id");

            try
            {
                if (!(bool)Session["NewReductionRecord"])
                {
                    //first delete existing record
                    int reductionId = (int)Session["TaxRedcutionId"];
                    objReduction.Id = reductionId;
                    objReduction.UpdatedBy = this.Page.User.Identity.Name;
                    objReduction.UpdatedUserId = new Guid(userProfile.UserId);
                    objReduction.UpdatedDate = DateTime.Now;
                    objBusiness.UpdateRawTaxReduction(objReduction, command);

                    //Insert grades by first delting
                    objBusiness.DeleteReducedTaxpayerGrades(reductionId,command);
                    foreach (object item in selectedGrades)
                    {
                        objReduction.TaxPayerGrade = (int)item;
                        objBusiness.InsertTaxpayersDeduction(objReduction, command);
                    }
                }
                else
                {
                    objBusiness.InsertRawTaxReduction(objReduction, command);
                    //Insert grades
                    foreach (object item in selectedGrades)
                    {
                        objReduction.TaxPayerGrade = (int)item;
                        objBusiness.InsertTaxpayersDeduction(objReduction, command);
                    }
                }

                tran.Commit();
                ShowSuccess("The record is saved successfully saved!");
            }
            catch (Exception ex)
            {
                try
                {
                    if(tran != null)
                    tran.Rollback();
                    ShowError(ex.Message);
                }
                catch
                {

                }
            }



        }
        private void DoNew()
        {
            clbClient.JSProperties["cpAction"] = "New";
            txtTaxPercent.Text = string.Empty;
            dpAppliedDateFrom.SelectedDate = string.Empty;
            dpAppliedDateTo.SelectedDate = string.Empty;
            grlTaxPayerGrade.GridView.Selection.UnselectAll();
            cboTaxType.SelectedIndex = -1;
            Session["NewReductionRecord"] = true;
            SetDefaltValue();
            ShowSuccess("");
        }
        private int GetCategoryId(string code)
        {
            CategoryBusiness business = new CategoryBusiness();
            return (business.GetCategoryByCode(code)).Id;
        }


        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();
            if(cboTaxType.Value == null)
            {
                errMessages.Add("Please select the tax type");
            }
            var objSelection = grlTaxPayerGrade.GridView.Selection;
            if (objSelection == null || objSelection.Count == 0)
            {
                errMessages.Add("Please select the taxpayer grade");
            }
            
            if (txtTaxPercent.Text == string.Empty )
            {
                errMessages.Add("Please enter the deduction rate amount.");
            }
            
            if (dpAppliedDateFrom.SelectedDate == string.Empty)
            {
                errMessages.Add("Please enter 'Applied Date From' field");
            }
            if (dpAppliedDateTo.SelectedDate != string.Empty)
            {
                try
                {
                    if (!dpAppliedDateTo.IsValidDate)
                    {
                        errMessages.Add("Please enter a valid ending date");
                    }
                }
                catch
                {
                    errMessages.Add("Please enter a valid ending date");
                }
            }
            if (dpAppliedDateFrom.SelectedDate != string.Empty)
            {
                try
                {
                    if (!dpAppliedDateFrom.IsValidDate)
                    {
                        errMessages.Add("Please enter a valid min date");
                    }
                }
                catch
                {
                    errMessages.Add("Please enter a valid min date");
                }
            }
            return errMessages;
        }

        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }


        protected void grdList_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            
            BindGrid();
        }

        private void DeleteReduction()
        {
            clbClient.JSProperties["cpAction"] = "Delete";
            int id = Int32.Parse(Session["TaxRedcutionId"].ToString());
            RawTaxReductionBussiness bussiness = new RawTaxReductionBussiness();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            connection.Open();
            SqlTransaction tran = connection.BeginTransaction();
            try
            {
                command.Transaction = tran;
                bussiness.Delete(id, command);
                tran.Commit();
                ShowSuccess("The record is deleted successfully");

            }
            catch (Exception ex)
            {
                if (tran != null)
                    tran.Rollback();
                ShowError(ex.Message);
            }

        }

        protected void grlTaxPayerGrade_Init(object sender, EventArgs e)
        {
            BindGrades();
        }


        protected void BindGrid()
        {
            RawTaxReductionBussiness bussiness = new RawTaxReductionBussiness();
            List<RawTaxReductionDomain> RawTaxReductions = bussiness.GetRawTaxReductions();
            grdList.DataSource = RawTaxReductions;
            grdList.DataBind();

        }
        private void BindGrades()
        {
            grlTaxPayerGrade.DataSource = GetGradeList();
            grlTaxPayerGrade.DataBind();
        }
        private List<Grade> GetGradeList()
        {

            List<Grade> grades = new List<Grade>();
            Grade objgrad = new Grade()
            {
                Name = "Grade A",
                Id = (int)Enums.eGrade.A
            };
            grades.Add(objgrad);
            objgrad = new Grade()
            {
                Name = "Grade B",
                Id = (int)Enums.eGrade.B
            };
            grades.Add(objgrad);
            objgrad = new Grade()
            {
                Name = "Grade C",
                Id = (int)Enums.eGrade.C
            };
            grades.Add(objgrad);

            return grades;
        }
        class Grade
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }
    }
    
}
