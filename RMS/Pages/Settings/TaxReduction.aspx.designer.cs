﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace RMS.Pages.Settings {
    
    
    public partial class TaxReduction {
        
        /// <summary>
        /// mnuSettingToolbar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxMenu mnuSettingToolbar;
        
        /// <summary>
        /// alert control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::RMS.CommonControls.Alert alert;
        
        /// <summary>
        /// clbClient control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxCallbackPanel clbClient;
        
        /// <summary>
        /// cboTaxType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cboTaxType;
        
        /// <summary>
        /// grlTaxPayerGrade control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridLookup grlTaxPayerGrade;
        
        /// <summary>
        /// txtTaxPercent control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtTaxPercent;
        
        /// <summary>
        /// dpAppliedDateFrom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CUSTORDatePicker.CUSTORDatePicker dpAppliedDateFrom;
        
        /// <summary>
        /// dpAppliedDateTo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CUSTORDatePicker.CUSTORDatePicker dpAppliedDateTo;
        
        /// <summary>
        /// grdList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView grdList;
        
        /// <summary>
        /// Edit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.GridViewCommandColumnCustomButton Edit;
        
        /// <summary>
        /// popCategory control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl popCategory;
        
        /// <summary>
        /// BusinessCategoryControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::RMS.Controls.BusinessCategoryControl BusinessCategoryControl;
    }
}
