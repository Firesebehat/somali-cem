﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="PenaltyRightOff.aspx.cs" Inherits="RMS.Pages.Settings.PenaltyRightOff" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            margin-left: 20px !important;
            margin-right: 20px !important;
        }

        .erroStyle {
            font-size: smaller;
        }

        .erroStyleSmaller {
            font-size: xx-small;
        }

        .menu {
            /*background-color: #F7F7F7;*/
            padding: 15px;
        }

        h5 {
            margin-left: 20px;
            font-weight: bold;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        .background-gradiant {
            height: 100%;
            width: 100%;
            background-color: darkgrey;
            margin-top: 0px;
            background: linear-gradient(top,white, rgb(218, 216, 216));
        }

        .formHeader-content-title {
            font-size: xx-large;
            font-weight: 600;
            padding-top: 1.5rem;
            height: 4rem;
            color: #009688;
            margin-top: -30px;
        }

        .subHeader-content-title {
            font-size: 1rem;
            padding-top: 1.5rem;
            height: 4rem;
            color: #009688;
            margin-top: -30px;
            font-weight: 100;
        }

        div.hidden {
            display: none;
        }

        fullWidth {
            max-width: 100% !important;
        }
    </style>
    <script type="text/javascript">
        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }

        function IsParentChecked(s, e) {
            if (chkIsParent.GetCheckState() == "Checked") {
                $("#ParentPenaltyrightoffCaption").hide();
                $("#ParentPenaltyrightoffControl").hide();
                txtLatePayment.SetEnabled(false);
                txtNoFinancialRecord.SetEnabled(false);
                txtUnderStatement.SetEnabled(false);
                txtIncomeHiding.SetEnabled(false);
            }
            else {
                $("#ParentPenaltyrightoffCaption").show();
                $("#ParentPenaltyrightoffControl").show();
                txtLatePayment.SetEnabled(true);
                txtNoFinancialRecord.SetEnabled(true);
                txtUnderStatement.SetEnabled(true);
                txtIncomeHiding.SetEnabled(true);
            }
        }

        function OnClientEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == "updated") {
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == "PenaltyRightOffSettingLoaded") {
                    if (s.cpIsParent === "Yes") {
                        $("#ParentPenaltyrightoffCaption").hide();
                        $("#ParentPenaltyrightoffControl").hide();
                    }
                    else if (s.cpIsParent === "No") {
                        $("#ParentPenaltyrightoffCaption").show();
                        $("#ParentPenaltyrightoffControl").show();
                        cboParentPenaltyrightoff.SetVisible(true);
                    }
                    ToggleEnabled("mnuSave", true);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    ShowError(s.cpMessage);
                }
                if (s.cpAction == "penaltyRightOffExist") {
                    ShowError(s.cpMessage);
                }
            }
        }

        function DoNewPenaltyRightoff() {
            cboParentPenaltyrightoff.SetValue(null);
            $("#ParentPenaltyrightoffCaption").show();
            $("#ParentPenaltyrightoffControl").show();
            txtLatePayment.SetValue(null);
            txtNoFinancialRecord.SetValue(null);
            txtUnderStatement.SetValue(null);
            txtIncomeHiding.SetValue(null);
            txtPenaltyRightOffDescription.SetValue(null);
            chkIsParent.SetCheckState("Unchecked");
            ToggleEnabled("mnuSave", true);
            hfPenaltyLiftingSettingFormStatus.Set("isNewPenaltyRightOff", true);
        }

        function DoSavePenaltyRightoff() {
            if (chkIsParent.GetCheckState() == "Checked") {
                if (ASPxClientEdit.ValidateGroup('penalityLiftingDescription')) {
                    clbPenaltyRightOff.PerformCallback('Save');
                }
            }
            else {
                if (ASPxClientEdit.ValidateGroup('penalityLiftingDescription') && ASPxClientEdit.ValidateGroup('penalityLiftingValue')) {
                    clbPenaltyRightOff.PerformCallback('Save');
                }
            }
        }

        function DoListPenaltyRightoff() {
            popPenaltyRightOffList.Show();
            gvwPenaltyRightOffSettings.PerformCallback('List');
        }

        function gvwPenaltyRightOffSettingsEndCallBack(s, e) {

        }

        function onMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewPenaltyRightoff();
            }
            else if (e.item.name === "mnuSave") {
                DoSavePenaltyRightoff();
            }
            else if (e.item.name === "mnuFind") {
                DoListPenaltyRightoff();
            }
        }

        function GetMenuItem(mnuName) {
            return mnuToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuFind", true);
        }

        function DeletePenaltyRightoff(value) {
            ShowConfirmx('Are you sure you want to delete penalty rightoff?',
                function (result) {
                    if (result) {
                        gvwPenaltyRightOffSettingsDetail.PerformCallback('DeletePenalityRightOffSetting' + '|' + value);
                    }
                }, 'en');
        }

        function OnGetPenalityRightOffSettingSelectedFieldValues(values) {
            clbPenaltyRightOff.PerformCallback('LoadPenalityRightOffSettingsInfo' + '|' + values);
        }

        function gvwPenaltyRightOffSettingsDetail_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID != "Delete") return;
            if (e.buttonID == 'Edit') {
                popPenaltyRightOffList.Hide();
                gvwPenaltyRightOffSettingsDetail.GetRowValues(e.visibleIndex, "Id", OnGetPenalityRightOffSettingSelectedFieldValues);
            }
            if (e.buttonID == 'Delete') {
                gvwPenaltyRightOffSettingsDetail.GetRowValues(e.visibleIndex, 'Id', DeletePenaltyRightoff);
            }
        }

        function gvwPenaltyRightOffSettings_CustomeClick(s, e) {
            if (e.buttonID != 'EditParent' && e.buttonID != "DeleteParent") return;
            if (e.buttonID == 'EditParent') {
                popPenaltyRightOffList.Hide();
                gvwPenaltyRightOffSettings.GetRowValues(e.visibleIndex, "Id", OnGetPenalityRightOffSettingSelectedFieldValues);
            }
            if (e.buttonID == 'DeleteParent') {
                gvwPenaltyRightOffSettings.GetRowValues(e.visibleIndex, 'Id', DeletePenaltyRightoff);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <div class="category">
        <div class="row">
            <div class="col col-md-12">
                <div class="row">
                    <h5 style="margin-left: 0px">Administrative Penalty Rightoff</h5>
                </div>
                <div class="card card-body" style="width: 500px; margin-top: 5px; margin-left: 5px;">
                    <dx:ASPxMenu ID="mnuToolbar" AllowSelectItem="True" runat="server"
                        ShowPopOutImages="True" EnableTheming="True" Height="35px"
                        ClientInstanceName="mnuToolbar" Font-Names="Visual Geez Unicode" meta:resourcekey="mnuToolbarResource1">
                        <ClientSideEvents Init="onMenuInit" ItemClick="onMenuItemClicked"></ClientSideEvents>
                        <Items>
                            <dx:MenuItem Name="mnuNew" Text="Add" Enabled="true" meta:resourcekey="MenuNewItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="iconbuilder_actions_add_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuSaveItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="save_save_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuFind" Text="List" Selected="true" Enabled="true" meta:resourcekey="MenuFindItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="iconbuilder_actions_zoom_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                    <dx:ASPxCallbackPanel ID="clbPenaltyRightOff" ClientInstanceName="clbPenaltyRightOff" Theme="Material" runat="server"
                        OnCallback="clbPenaltyRightOff_Callback" Width="1000px" meta:resourcekey="clbTaxPayerDetailsResource1">
                        <ClientSideEvents EndCallback="OnClientEndCallback" />
                        <PanelCollection>
                            <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="pnlMainResource1">
                                <uc1:Alert runat="server" ID="Alert" />
                                <div class="row" style="margin-left: -40px">
                                    <div class="col col-md-12">
                                        <br />

                                        <div class="form-row">
                                            <div class="col-md-4" id="ParentPenaltyrightoffCaption">
                                                <dx:ASPxLabel ID="lblParentPenaltyrightoff" runat="server" ClientInstanceName="lblParentPenaltyrightoff" Text="Parent penalty rightoff" meta:resourcekey="lblParentPenaltyrightoffResource1"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblParentPenaltyrightoffStar" runat="server" ClientInstanceName="lblParentPenaltyrightoffStar" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblParentPenaltyrightoffStarResource1"></dx:ASPxLabel>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-2">
                                                <dx:ASPxLabel ID="lblRegitrationDate" runat="server" ClientInstanceName="lblRegitrationDate" Text="Registration Date" meta:resourcekey="lblRegitrationDateResource1"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblRegitrationDateStar" runat="server" ClientInstanceName="lblRegitrationDateStar" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblRegitrationDateStarResource1"></dx:ASPxLabel>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-md-4" id="ParentPenaltyrightoffControl">
                                                <dx:ASPxComboBox ID="cboParentPenaltyrightoff" runat="server" ClientInstanceName="cboParentPenaltyrightoff"
                                                    IncrementalFilteringMode="StartsWith" Width="100%" NullText="Select" meta:resourcekey="cboParentPenaltyrightoffResource">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingValue">
                                                        <RequiredField ErrorText="Please the parent penalty rightoff" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                            <div class="col-md-2" style="margin-left: 15px">
                                                <dx:ASPxCheckBox runat="server" ID="chkIsParent" AutoPostBack="false" ClientEnabled="true" ClientInstanceName="chkIsParent" Checked="false" Text="Is Parent?">
                                                    <ClientSideEvents CheckedChanged="IsParentChecked" />
                                                </dx:ASPxCheckBox>
                                            </div>
                                            <div class="col-md-2" style="margin-left: -15px">
                                                <cdp:CUSTORDatePicker ID="cdpRegistrationDate" runat="server" Width="120px" SelectedDate="" TextCssClass="" meta:resourcekey="cdpBirthDateResource1" />
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <dx:ASPxLabel ID="lblLatePayment" runat="server" Text="Late payment" meta:resourcekey="lblLatePaymentResource1"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblLatePaymentStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblLiftingLevelStarResource1"></dx:ASPxLabel>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxLabel ID="lblNoFinancialRecord" runat="server" Text="No financial record" meta:resourcekey="lblNoFinancialRecordResource1"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblNoFinancialRecordStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblNoFinancialRecordStarResource1"></dx:ASPxLabel>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxLabel ID="lblUnderStatement" runat="server" Text="Under statement" meta:resourcekey="lblUnderStatementResource"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblUnderStatementStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblUnderStatementStarResource1"></dx:ASPxLabel>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxLabel ID="lblIncomeHiding" runat="server" Text="Hiding income" meta:resourcekey="lblUnderStatementResource"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblIncomeHidingStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblUnderStatementStarResource1"></dx:ASPxLabel>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <dx:ASPxTextBox ID="txtLatePayment" runat="server" ClientInstanceName="txtLatePayment"
                                                    Width="100%" onkeypress="return AcceptNumericOnly(event,true)" MaxLength="3">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingValue">
                                                        <RequiredField ErrorText="Late payment can not be empty" IsRequired="True" />
                                                    </ValidationSettings>
                                                    <ClientSideEvents Validation="ValidateSettingValue" />
                                                </dx:ASPxTextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxTextBox ID="txtNoFinancialRecord" runat="server" ClientInstanceName="txtNoFinancialRecord"
                                                    Width="100%" onkeypress="return AcceptNumericOnly(event,true)" MaxLength="3">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingValue">
                                                        <RequiredField ErrorText="No financial record can not be empty" IsRequired="True" />
                                                    </ValidationSettings>
                                                    <ClientSideEvents Validation="ValidateSettingValue" />
                                                </dx:ASPxTextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxTextBox ID="txtUnderStatement" runat="server" ClientInstanceName="txtUnderStatement"
                                                    Width="100%" onkeypress="return AcceptNumericOnly(event,true)" MaxLength="3">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingValue">
                                                        <RequiredField ErrorText="Understatement can not be empty" IsRequired="True" />
                                                    </ValidationSettings>
                                                    <ClientSideEvents Validation="ValidateSettingValue" />
                                                </dx:ASPxTextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <dx:ASPxTextBox ID="txtIncomeHiding" runat="server" ClientInstanceName="txtIncomeHiding"
                                                    Width="100%" onkeypress="return AcceptNumericOnly(event,true)" MaxLength="3">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingValue">
                                                        <RequiredField ErrorText="Income hiding can not be empty" IsRequired="True" />
                                                    </ValidationSettings>
                                                    <ClientSideEvents Validation="ValidateSettingValue" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <dx:ASPxLabel ID="lblPenaltyRightOffDescription" runat="server" Text="Description" meta:resourcekey="lblLiftingLevelResource1"></dx:ASPxLabel>
                                                <dx:ASPxLabel ID="lblPenaltyRightOffDescriptionStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*" meta:resourcekey="lblPenaltyRightOffDescriptionStarResource1"></dx:ASPxLabel>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <dx:ASPxMemo ID="txtPenaltyRightOffDescription" runat="server" ClientInstanceName="txtPenaltyRightOffDescription"
                                                    Width="100%" CssClass="fullWidth" Rows="5">
                                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penalityLiftingDescription">
                                                        <RequiredField ErrorText="Income hiding can not be empty" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <dx:ASPxPopupControl ID="popPenaltyRightOffList" ClientInstanceName="popPenaltyRightOffList" runat="server" Width="1000px" AllowDragging="True"
                                    PopupAnimationType="Fade" PopupHorizontalAlign="WindowCenter" ShowHeader="true" PopupVerticalAlign="TopSides" EnableCallbackAnimation="True"
                                    HeaderText="Penalty RightOff List" Modal="true" meta:resourcekey="popPenaltyLiftingResource1" Top="100">
                                    <SettingsAdaptivity MaxHeight="95%" />
                                    <HeaderStyle BackColor="#009688" ForeColor="White" />
                                    <ContentCollection>
                                        <dx:PopupControlContentControl runat="server" meta:resourcekey="PopupControlContentControlResource1">
                                            <dx:ASPxGridView ID="gvwPenaltyRightOffSettings" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwPenaltyRightOffSettings" OnCustomCallback="gvwPenaltyRightOffSettings_CustomCallback"
                                                OnPageIndexChanged="gvwPenaltyRightOffSettings_PageIndexChanged" ClientVisible="true" KeyFieldName="Id" EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" Width="100%">
                                                <ClientSideEvents CustomButtonClick="gvwPenaltyRightOffSettings_CustomeClick" />
                                                <SettingsPager PageSize="5">
                                                </SettingsPager>
                                                <Styles AdaptiveDetailButtonWidth="22"></Styles>
                                                <ClientSideEvents EndCallback="gvwPenaltyRightOffSettingsEndCallBack" />
                                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                                <Templates>
                                                    <DetailRow>
                                                        <dx:ASPxGridView ID="gvwPenaltyRightOffSettingsDetail" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwPenaltyRightOffSettingsDetail"
                                                            KeyFieldName="Id" OnDataBinding="gvwPenaltyRightOffSettingsDetail_DataBinding" OnBeforePerformDataSelect="gvwPenaltyRightOffSettingsDetail_BeforePerformDataSelect"
                                                            Font-Size="Small" Width="100%">
                                                            <ClientSideEvents CustomButtonClick="gvwPenaltyRightOffSettingsDetail_CustomButtonClick" />
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Visible="false" FieldName="Id" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource6">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource5" />

                                                                <dx:GridViewDataTextColumn Caption="Late Payment" FieldName="DelayPenaltyPercentage" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                    <HeaderStyle Wrap="True" HorizontalAlign="Left"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="No Financial Record" FieldName="LackOfStatementPenaltyPercentage" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                    <HeaderStyle Wrap="True" HorizontalAlign="Left"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="UnderStatement" FieldName="UnderStatementPercentage" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                    <HeaderStyle Wrap="True" HorizontalAlign="Left"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Income Hiding" FieldName="HidingIncomePenaltyPercentage" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                    <HeaderStyle Wrap="True" HorizontalAlign="Left"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="12">
                                                                    <HeaderTemplate>
                                                                    </HeaderTemplate>
                                                                    <CustomButtons>
                                                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                                                        <%--<dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                                                            <Styles Style-ForeColor="Red">
                                                                                <Style ForeColor="Red"></Style>
                                                                            </Styles>
                                                                        </dx:GridViewCommandColumnCustomButton>--%>
                                                                    </CustomButtons>
                                                                </dx:GridViewCommandColumn>
                                                            </Columns>
                                                            <Settings ShowFooter="true"  />
                                                        </dx:ASPxGridView>
                                                    </DetailRow>
                                                </Templates>
                                                <SettingsPager PageSize="5" AlwaysShowPager="True">
                                                    <FirstPageButton Visible="True">
                                                    </FirstPageButton>
                                                    <LastPageButton Visible="True">
                                                    </LastPageButton>
                                                    <NextPageButton Text="Next">
                                                        <Image IconID="arrows_next_16x16">
                                                        </Image>
                                                    </NextPageButton>
                                                    <PrevPageButton Text="Previous">
                                                        <Image IconID="arrows_prev_16x16">
                                                        </Image>
                                                    </PrevPageButton>
                                                </SettingsPager>
                                                <SettingsBehavior AllowSelectSingleRowOnly="true" AllowFocusedRow="True" />
                                                <SettingsPopup>
                                                    <HeaderFilter MinHeight="140px"></HeaderFilter>
                                                </SettingsPopup>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="True" Visible="false" VisibleIndex="1" />
                                                    <dx:GridViewDataTextColumn Caption="Date" FieldName="Date" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource5" />
                                                    <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource6" />
                                                    <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="12">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <CustomButtons>
                                                            <dx:GridViewCommandColumnCustomButton ID="EditParent" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                                            <%--<dx:GridViewCommandColumnCustomButton ID="DeleteParent" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                                                <Styles Style-ForeColor="Red">
                                                                    <Style ForeColor="Red"></Style>
                                                                </Styles>
                                                            </dx:GridViewCommandColumnCustomButton>--%>
                                                        </CustomButtons>
                                                    </dx:GridViewCommandColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                                <dx:ASPxHiddenField runat="server" ID="hfPenaltyLiftingSettingFormStatus" ClientInstanceName="hfPenaltyLiftingSettingFormStatus">
                                </dx:ASPxHiddenField>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
