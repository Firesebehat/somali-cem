﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncomeTaxRate.aspx.cs" Inherits="RMS.Pages.IncomeTaxRate" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<%@ Register TagPrefix="cdp" Namespace="CUSTORDatePicker" Assembly="CUSTORDatePicker" %>

<asp:Content runat="server" ContentPlaceHolderID="PageContent">
<script>
    function grdView_CustomButtonClick(s, e) {
        if (e.buttonID != 'DELETE') return;
        if (e.buttonID == 'DELETE') {
            s.GetRowValues(e.visibleIndex, "Id", onDeleteInComeTax);
        }
    }
    function onDeleteInComeTax(values) {
        ShowConfirmx('Are you sure? Do you want to delete this record?', function (result) {
            if (result)
                grdTaxPayer.PerformCallback('Delete|' + values);
        }, 'en');
    }
    function ShowRateDetail() {
        grdTaxPayer.PerformCallback();
    }
    function grdTaxPayerEndCallback(s) {
        if (s.cpStatus === 'SUCCESS') {
            if (s.cpAction === 'Save') {
                ShowSuccess(s.cpMessage);
                grdTaxPayer.PerformCallback();
            }
            else if (s.cpAction === 'Delete') {
                ShowSuccess(s.cpMessage);
                grdTaxPayer.PerformCallback();
            }
            

        }
        else if (s.cpStatus === 'ERROR') {
            ShowError(s.cpMessage);
            grdTaxPayer.PerformCallback();
        }
        if (s.cpMessage === 'NA') {
            grdTaxPayer.SetVisible(false);
            ShowMessage('Record was not found');
        }
        else
            grdTaxPayer.SetVisible(true);
        s.cpStatus = s.cpAction = s.cpMessage = '';
    }
    
</script>
<style>
    h5 {
            font-weight: bold;
            margin-top: 10px;
            text-align: left;
            color: #009688;
        }
</style>
     
 <div class="contentmain">
     <h5>
         Income Tax Rate
     </h5>
     <div class="card card" style="padding:15px !important">
     <script src="../../Content/ModalDialog.js"></script>
   
         <uc1:Alert ID="alert" runat="server" />
          
            <table>
                <tr style="height:20px">
                    <td>
                        <dx:ASPxLabel runat="server"  Text="Budget Year:"></dx:ASPxLabel>

                    </td>
                    <td>
                        <dx:ASPxSpinEdit runat="server" ID="txtBudgetYear" ClientInstanceName="txtBudgetYear" MinValue="2010" MaxValue="2099" Style="max-width: 100px" NullText="Budget Year">
                            <ValidationSettings Display="Dynamic" ErrorText="Budget year is required" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" RequiredField-IsRequired="true"></ValidationSettings>
                        </dx:ASPxSpinEdit>
                    </td>
                       <td style="vertical-align:bottom;max-width:100px !important;">
                         <a onclick="ShowRateDetail()" style="color:#009688;width:100%;" class="btn btn-link btn-sm" >
                         <span style="font-weight:bold"> &nbsp; Show &nbsp;</span>
                       </a>
                     </td>
                    <td rowspan="3">
                        <dx:ASPxGridView ID="grdTaxPayer" ClientInstanceName="grdTaxPayer" runat="server" AutoGenerateColumns="False" KeyFieldName="Id"  ClientVisible="False"   
                            OnCustomCallback="grdTaxPayer_CustomCallback" OnRowUpdating="grdTaxPayer_RowUpdating" OnRowInserting="grdTaxPayer_RowInserting">
                            <ClientSideEvents EndCallback="function(s){grdTaxPayerEndCallback(s);}"  CustomButtonClick="function(s,e){grdView_CustomButtonClick(s,e);}"/>
                            <Styles InlineEditCell-Font-Underline="true">
<InlineEditCell Font-Underline="True"></InlineEditCell>
                            </Styles>
                            <SettingsEditing Mode="Inline">
                            </SettingsEditing>
                            <SettingsBehavior ConfirmDelete="true"  AllowFocusedRow="true" EnableRowHotTrack="true" />
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Salary From" FieldName="IncomeFrom" VisibleIndex="0" Width="120px">
                                    <PropertiesTextEdit >
                                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" IncludeLiterals="DecimalSymbol" />
                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Salary To" FieldName="IncomeTo" VisibleIndex="1" Width="120px">
                                    <PropertiesTextEdit>
                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" IncludeLiterals="DecimalSymbol" />
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                               
                                <dx:GridViewDataTextColumn Caption="%" FieldName="Rate" VisibleIndex="2" Width="80px">
                                    <PropertiesTextEdit>
                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                        <MaskSettings Mask="&lt;0..99g&gt;.&lt;00..99&gt;" IncludeLiterals="DecimalSymbol" />
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Starting From" FieldName="DateFromText" VisibleIndex="3" Width="80px">
                                    <PropertiesTextEdit MaxLength="10" NullDisplayText="DD/MM/YYYY">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                 <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="4" ShowNewButton="True">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewCommandColumn VisibleIndex="5">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="DELETE" Text="Delete">
                                            <Styles Style-ForeColor="Red"></Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                            </Columns>



<SettingsPopup>
<HeaderFilter MinHeight="140px"></HeaderFilter>
</SettingsPopup>

                            <SettingsText CommandEdit="Edit" CommandDelete="Delete"  CommandNew="New" 
                                ConfirmDelete="Are you sure do you want to delete this record?" CommandUpdate="Save" CommandCancel="Cancel"  />
                        </dx:ASPxGridView>

                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top">
                        
                     <td style="vertical-align:top;max-width:100px !important">
                         
                     </td>
                     <td style="vertical-align:top"></td>
                </tr>
                <tr>
                    <td></td>
                     <td></td>
                     <td></td>
                </tr>
            </table>
         </div>

        </div>
      <uc1:Alert runat="server" ID="Alert1"  style="z-index:1000000 !important"/> 
    </asp:Content>