﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTORCommon.Ethiopic;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class CategoryRate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                hdfValue["IsNewRate"] = true;
                hdfValue["CategoryId"] = 0;
                Session["OtherService"] = null;
                SetDefaultDate();
            }
            //BindTree(false);
        }

        private void SetDefaultDate()
        {

            DateTime date = DateTime.Now;
            string yearText = EthiopicDateTime.GetEthiopicYear(date.Day, date.Month, date.Year).ToString();
            txtBudgetYear.Text = yearText;
            dpFrom.SelectedDate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
        }

        protected void clbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {

        }
        //private void BindCategoryRateCode()
        //{
        //    try
        //    {
        //        CategoryRateCategoryRate CategoryRateCategoryRate = new CategoryRateCategoryRate();
        //        List<CategoryRate> categories = CategoryRateCategoryRate.GetParentCategoryRates();
        //        //cboCategoryRateCode.TextField = "Code";
        //        //cboCategoryRateCode.ValueField = "Code";
        //        //cboCategoryRateCode.DataSource = categories;
        //        //cboCategoryRateCode.DataBind();
        //    }
        //    catch
        //    {

        //    }
        //}

        private void BindTree(bool otherService)
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            //List<CUSTOR.Domain.CategoryDomain> categories;
            //if (txtSearch.Text != string.Empty)
            //{
                DoSearchCategory(otherService);
                return;
            //}
            //try
            //{
            //    categories = categoryBusiness.GetCategorys(otherService);

            //    trlCategorys.DataSource = categories;
            //    trlCategorys.DataBind();
            //}
            //catch
            //{
                
            //}
        }

        private void DoSearchCategory(bool otherService)
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            List<CUSTOR.Domain.CategoryDomain> categories;
            
            try
            {
                CVGeez obj = new CVGeez();
                string text = obj.GetSortValueU(txtSearch.Text);
                text = text == string.Empty ? txtSearch.Text : text.Replace("w1",".");
                categories = categoryBusiness.SearchCategory(text, otherService);
                if (categories.Count > 0)
                {
                    trlCategorys.DataSource = categories;
                    trlCategorys.DataBind();
                }
                else
                {
                    trlCategorys.DataSource = null;
                    trlCategorys.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void trlCategoryRates_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
        {
            BindTree(chbotherServiceList.Checked);
        }

        protected void cblMainClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains("|"))
                strID = parameters[1];
            switch (strParam)
            {
                case "Save":
                    DoSave();
                    break;
                case "Bind":
                    BindGrid();
                    hdfValue["IsNewRate"] = false;
                    break;
                case "Get":
                    DoGet(strID, parameters[2]);
                    hdfValue["IsNewRate"] = false;
                    break;
                case "GetLatesRate":
                    DoGet(strID, parameters[2]);
                    hdfValue["IsNewRate"] = false;
                    break;
                case "New":
                    DoNew();
                    hdfValue["IsNewRate"] = true;
                    break;
            }
        }

        private void DoNew()
        {
            hdfValue["IsNewRate"] = true;
            cblMainClient.JSProperties["cpAction"] = "New";
            txtAnnualSales.Text = string.Empty;
            txtSalesRate.Text = string.Empty;
            txtGrossProfit.Text = string.Empty;
            txtGrossProfitRate.Text = string.Empty;
            txtNetProfit.Text = string.Empty;
            txtNetProfitRate.Text = string.Empty;
            txtExciseCode.Text = string.Empty;
            txtExciseRate.Text = string.Empty;
            txtTOTCode.Text = string.Empty;
            txtTOTRate.Text = string.Empty;
            txtWorkingHour.Text = string.Empty;
           // txtMinAge.Text = string.Empty;
            //txtMaxAge.Text = string.Empty;
            txtDriverSalary.Text = string.Empty;
            txtAssistantSalary.Text = string.Empty;
            cboUnit.Text = string.Empty;
            txtMinCapacity.Text = string.Empty;
            txtMaxCapacity.Text = string.Empty;
            txtBudgetYear.Text = string.Empty;
            SetDefaultDate();
            ShowSuccess("");
        }

        private void DoGet(string strID, string categoryId)
        {
            int id = Convert.ToInt32(strID);
            hdfValue["RateId"] = Int32.Parse(categoryId);
            cblMainClient.JSProperties["cpAction"] = "Get";
            CategoryRatesBussiness business = new CategoryRatesBussiness();
            try
            {
                //int catId = Convert.ToInt32(categoryId);
                //hdfValue["CategoryId"] = catId;
                CUSTOR.Domain.CategoryRates rate;
                if (id > 0) 
                    rate = business.GetCategoryRates(id);
                else//get latest rate
                    rate = business.GetLatestCategoryRate(Int32.Parse(categoryId));
                hdfValue["CategoryId"] = rate.CategoryId;
                txtAnnualSales.Text = rate.AnnualSales.ToString();
                txtSalesRate.Text = rate.SalesRate.ToString();
                txtGrossProfit.Text = rate.GrossProfit.ToString();
                txtGrossProfitRate.Text = rate.GrossProfitRate.ToString();
                txtNetProfit.Text = rate.NetProfit.ToString();
                txtNetProfitRate.Text = rate.NetProfitRate.ToString();
                txtExciseCode.Text = rate.ExciseCode;
                txtExciseRate.Text = rate.ExciseRate.ToString();
                txtTOTCode.Text = rate.TOTCode;
                txtTOTRate.Text = rate.TOTRate.ToString();
                txtWorkingHour.Text = rate.WorkingDays.ToString();
                //txtMinAge.Text = rate.MinAge.ToString();
                //txtMaxAge.Text = rate.MaxAge.ToString();
                txtDriverSalary.Text = rate.DriverSalary.ToString();
                txtAssistantSalary.Text = rate.AssistantSalary.ToString();
                cboUnit.Text = rate.Unit.ToString();
                txtMinCapacity.Text = rate.MinCapacity.ToString();
                txtMaxCapacity.Text = rate.MaxCapacity.ToString();
                //txtMaxAge.Text = rate.MaxCapacity.ToString();
                txtBudgetYear.Text = rate.BudgetYear.ToString();
                dpFrom.SelectedDate = rate.AppliedDateFrom == DateTime.MinValue?string.Empty: rate.AppliedDateFromText;
                dpTo.SelectedDate = rate.AppliedDateTo == DateTime.MinValue ? string.Empty : rate.AppliedDateToText;
                chbIsAbove15.Checked = rate.Above15Years ==1?true:false;
                ShowSuccess("");
                lblDescription.Text = trlCategorys.FindNodeByKeyValue(categoryId).GetValue("Description").ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void BindGrid()
        {
            
            grdView.JSProperties["cpAction"] = "Bind";
            CategoryRatesBussiness bussiness = new CategoryRatesBussiness();
            int selectedCategory = Convert.ToInt32(hdfValue["CategoryId"]);
            hdfValue["CategoryId"] = selectedCategory;
            try
            {
                List<CUSTOR.Domain.CategoryRates> rates = bussiness.GetCategoryRatess(selectedCategory);
                if (rates.Count > 0)
                {
                    grdView.DataSource = rates;
                    grdView.DataBind();
                    grdView.JSProperties["cpMessage"] = "";
                }
                else
                {
                    grdView.DataSource = null;
                    grdView.DataBind();
                    grdView.JSProperties["cpMessage"] = "NA";
                }
                grdView.JSProperties["cpStatus"] = "SUCCESS";
                
            }
            catch
            {

            }
        }

        private void DoSave()
        {
            cblMainClient.JSProperties["cpAction"] = "Save";
            CProfile userProfile = CurrentProfile.ActiveUserProfile;
            CUSTOR.Domain.CategoryRates rate = new CUSTOR.Domain.CategoryRates();
            CategoryRatesBussiness rateBusiness = new CategoryRatesBussiness();
            DateTime eventDate = DateTime.Now;
            try
            {
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {
                    ShowError(GetErrorDisplay(errMessages));
                    return; ;
                }
                rate.CategoryId = Convert.ToInt32(hdfValue["CategoryId"]);
                rate.AnnualSales = txtAnnualSales.Text == string.Empty ? 0:  Convert.ToDecimal(txtAnnualSales.Text);
                rate.SalesRate = txtSalesRate.Text==string.Empty?0:  Convert.ToDouble( txtSalesRate.Text);
                rate.GrossProfit = txtGrossProfit.Text==string.Empty?0: Convert.ToDecimal( txtGrossProfit.Text);
                rate.GrossProfitRate = Convert.ToDouble( txtGrossProfitRate.Text);
                rate.NetProfit = txtNetProfit.Text==string.Empty?0: Convert.ToDecimal(txtNetProfit.Text);
                rate.NetProfitRate = txtNetProfitRate.Text == string.Empty ? 0: Convert.ToDouble(txtNetProfitRate.Text);
                rate.ExciseCode = txtExciseCode.Text;
                rate.ExciseRate = txtExciseRate.Text==string.Empty?0: Convert.ToDouble( txtExciseRate.Text);
                
                rate.TOTCode =  txtTOTCode.Text;
                rate.TOTRate = txtTOTRate.Text==string.Empty?0:  Convert.ToDouble(txtTOTRate.Text);
                rate.WorkingDays = txtWorkingHour.Text==string.Empty?0: Convert.ToInt32(txtWorkingHour.Text);
               // rate.MinAge = txtMinAge.Text==string.Empty?0: Convert.ToInt32(txtMinAge.Text);
                //rate.MaxAge = txtMaxAge.Text==string.Empty?0: Convert.ToInt32(txtMaxAge.Text);
                rate.DriverSalary = txtDriverSalary.Text==string.Empty?0: Convert.ToDecimal(txtDriverSalary.Text);
                rate.AssistantSalary = txtAssistantSalary.Text==string.Empty?0: Convert.ToDecimal(txtAssistantSalary.Text);
                rate.Unit = cboUnit.Value == null?0: Convert.ToInt32(cboUnit.Value);
                rate.MinAge = txtMinCapacity.Text==string.Empty?0:  Convert.ToInt32(txtMinCapacity.Text);
                rate.MaxCapacity = txtMaxCapacity.Text==string.Empty?0: Convert.ToDouble(txtMaxCapacity.Text);
                rate.AppliedDateFrom = dpFrom.SelectedGCDate;
                rate.AppliedDateTo = dpTo.SelectedDate == string.Empty? DateTime.MinValue: dpTo.SelectedGCDate;
                rate.BudgetYear = Convert.ToInt32(txtBudgetYear.Text);
                if ((bool)hdfValue["IsNewRate"])
                {
                    rate.CreatedUserId = userProfile.UserId;
                    rate.CreatedBy = User.Identity.Name;
                    rate.CreatedDate = eventDate;
                    //if (rateBusiness.Exists(rate))
                    //{
                    //    //ShowError("ያስገቡት ንግድ በፊት ከተመዘገበ የንግድ ስም ወይም ፈቃድ ቁጥር አንድ ነው:: እባክዎ አጣርተው እንደገና ይሞክሩ");
                    //    return;
                    //}
                    rateBusiness.InsertCategoryRates(rate);
                }
                else
                {
                    rate.UpdatedUserId = userProfile.UserId;
                    rate.UpdatedBy = User.Identity.Name;
                    rate.UpdatedDate = eventDate;
                    rate.Id = Convert.ToInt32(hdfValue["RateId"]);

                    //if (rateBusiness.UpdateExists(rate))
                    //{
                    //    ShowError("ያስገቡት ንግድ በፊት ከተመዘገበ የንግድ ስም ወይም ፈቃድ ቁጥር አንድ ነው:: እባክዎ አጣርተው እንደገና ይሞክሩ");
                    //    return false;
                    //}
                    rateBusiness.UpdateCategoryRates(rate);
                }
                hdfValue["IsNewRate"] = false;
                DoNew();
                ShowSuccess("The record is successfully saved");
                cblMainClient.JSProperties["cpAction"] = "Save";
                return;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return; ;
            }
        }

        protected void grdView_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] param = e.Parameters.Split('|');
            string command = param[0];
            switch (command)
            {
                case "Delete":
                    DoDelete(param[1]);
                    break;
                default:
                    BindGrid();
                    break;
            }
        }

        private void DoDelete(string id)
        {
            grdView.JSProperties["cpAction"] = "Delete";
            CategoryRatesBussiness business = new CategoryRatesBussiness();
            try
            {
                int Id = Convert.ToInt32(id);
                business.Delete(Id);
                grdView.JSProperties["cpStatus"] = "SUCCESS";
                grdView.JSProperties["cpMessage"] = "Record is successfully deleted";
            }
            catch (Exception ex)
            {
                grdView.JSProperties["cpStatus"] = "ERROR";
                grdView.JSProperties["cpMessage"] = ex.Message;
            }
        }

        protected void grdView_PageIndexChanged(object sender, EventArgs e)
        {

        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            ////Validate the ethiopic date
            if (dpFrom.SelectedDate == string.Empty)
            {
                errMessages.Add("Please select the applied date");
            }
            else if (!dpFrom.IsValidDate)
            {
                errMessages.Add("Invalid date range");
            }
            
            return errMessages;

        }
        private void ShowError(string strMsg)
        {
            cblMainClient.JSProperties["cpMessage"] = strMsg;
            cblMainClient.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            cblMainClient.JSProperties["cpMessage"] = strMsg;
            cblMainClient.JSProperties["cpStatus"] = "SUCCESS";
        }



        private string GetErrorDisplay(List<string> strMessages)
        {
            
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");
            return sb.ToString();

        }
        protected void trlCategorys_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
        {
            BindTree(chbotherServiceList.Checked);
        }

        protected void grdView_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
               BindGrid();
                //BindTree(false);
        }

        protected void trlCategorys_Init(object sender, EventArgs e)
        {
            bool otherService = Session["OtherService"] == null ? false : (bool)Session["OtherService"];
            BindTree(otherService);
        }

        protected void cblTree_Callback(object sender, CallbackEventArgsBase e)
        {
            string [] strPar = e.Parameter.Split('|');
            if(strPar[0] == "Search")
            {
                txtSearch.Text = strPar[1];
            }

            
            BindTree(chbotherServiceList.Checked);
            Session["OtherService"] = chbotherServiceList.Checked;

        }
    }
}