﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exemption.aspx.cs" Inherits="RMS.Pages.Exemption" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<%@ Register Src="~/Controls/BusinessCategoryControl.ascx" TagPrefix="uc1" TagName="BusinessCategoryControl" %>


<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <script>
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function ShowCategoryBusiness() {
            // popCategory.SetContentUrl('Pages/BusinessCategory');
            popCategory.SetHeaderText('Select Categories')
            popCategory.Show();

        }
        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                clbClient.PerformCallback('New');
            }
            else if (e.item.name === "mnuSave") {
                clbClient.PerformCallback('Save');
            }
            else if (e.item.name === "mnuList") {
                grdList.PerformCallback('Bind');
            }
            else if (e.item.name === "mnuDelete") {
                DoDelteExemption();
            }
        }
        function DoSaveClientClick() {
        }
        function popClosed() {
            trlCategorys.GetSelectedNodeValues("Code", PopulateText);
        }
        function PopulateText(values) {
            txtCaxtegory.SetText(values);
            popCategory.Hide();
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }
        function grdCustomButtonClick(s, e) {
            if (e.buttonID == 'Edit') {
                s.GetRowValues(e.visibleIndex, "Id", DoEdit);
            }
        }
        function DoEdit(id) {
            clbClient.PerformCallback('Edit|' + id);
        }
        function DoDelteExemption() {
            ShowConfirmx("Are you sure? Do you want to delete this record?", function (result) {
                if (result) {
                    clbClient.PerformCallback("Delete");

                }
            }, 'en');
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function OnClientEndCallback(s, e) {

            if (s.cpStatus === "SUCCESS") {
                 if (s.cpAction === "Save") {
                    ShowSuccess(s.cpMessage);
                     doListMode();
                     grdList.PerformCallback('Bind');
                    s.cpMessage = "";
                    return;
                }
                 else if (s.cpAction === "New") {
                     doEditMode();
                 }
                 else if (s.cpAction === "Edit") {
                     doEditMode();
                 }
                else if (s.cpAction === "Delete") {
                     doListMode();
                     ShowSuccess(s.cpMessage);
                     grdList.PerformCallback('Bind');
                    return;
                }
                s.cpAction = s.cpStatus = '';
            }
            

            else if (s.cpStatus === "ERROR") {
                if(s.cpMessage !== '')
                ShowError(s.cpMessage);
            }
            else if (s.cpStatus === "NOTFOUND") {
                if (s.cpAction === "List") {
                    ShowError('No Record is found');
                    s.cpAction = "";
                }
            }

        }
        function grdListEndCallback(s) {
            doListMode();
            if (s.cpAction === 'Delete') {
                if (s.cpStatus === 'ERROR') {
                    ShowError(s.cpMessage);
                }
                else if (s.cpStatus === 'SUCCESS')
                {
                    ShowSuccess('The record is deleted successfully');
                }
            }
            s.cpStatus = s.cpAction = '';
        }
        function doListMode() {
            $('#divForms').hide(100);
            $('#divData').show(100);
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
        }
        function doEditMode() {
            $('#divForms').show(100);
            $('#divData').hide(100);
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuDelete", true);
        }
    </script>
    <style>
      
       
        h5 {
            font-weight: bold;
            margin-top: 10px;
            text-align: left;
            color: #009688;
        }
        h6{
            color: #009688;
        }
        .card{
            overflow:visible;
        }
    </style>
   

    <div class="contentmain">
        <h5>Tax Exemption
        </h5>
        <div style="padding-left:15px;margin-bottom:8px;"> 
             <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                   <dx:MenuItem Name="mnuDelete" Text="Delete" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="diagramicons_del_svg_16x16"></Image>
                            </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
            
            </div>

        <div class="card" style="padding: 15px;">
            <script src="../../Content/ModalDialog.js"></script>
            <uc1:Alert ID="alert" runat="server" />

            <div  id="divForms">
                <dx:ASPxCallbackPanel runat="server" ID="clbClient" ClientInstanceName="clbClient" OnCallback="cbpClient_Callback">
                    <ClientSideEvents EndCallback="OnClientEndCallback" />
                    <PanelCollection>
                        <dx:PanelContent>
                            <table style="max-width: 800px; margin: 0 auto">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Category Codes"></asp:Label>

                                    </td>
                                    <td colspan="3">
                                        <dx:ASPxTextBox runat="server" ID="txtCaxtegory" NullText="Click to select" ClientInstanceName="txtCaxtegory" Style="color: #bdbdbd !important;" Width="100%" meta:resourcekey="txtCaxtegoryResource1">
                                            <ClientSideEvents GotFocus="ShowCategoryBusiness" />
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የንግድ ዘርፍ ያስፈልጋል" ErrorDisplayMode="Text" SetFocusOnError="True" ValidationGroup="GOrganization" Display="Dynamic">
                                                <RequiredField IsRequired="True" ErrorText="የንግድ ዘርፍ ያስፈልጋል" />
                                                <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="max-width: 60px !important;">
                                        <asp:Label runat="server" Text="Tax Types"></asp:Label>
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox runat="server" ID="cboTaxType" ClientInstanceName="cboTaxType" Width="100%"></dx:ASPxComboBox>

                                    </td>
                                    <td style="text-align: right;">
                                        <asp:Label runat="server" Text="Grade"></asp:Label>
                                    </td>
                                    <td>

                                        <dx:ASPxGridLookup ID="grlTaxPayerGrade" runat="server" SelectionMode="Multiple" ClientInstanceName="grlTaxPayerGrade"
                                            KeyFieldName="Id" TextFormatString="{1}" MultiTextSeparator="| " OnInit="grlTaxPayerGrade_Init" Width="100%">
                                            <GridViewProperties>

                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>
                                                <SettingsPopup>
                                                    <HeaderFilter MinHeight="140px"></HeaderFilter>
                                                </SettingsPopup>
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="AllPages" VisibleIndex="0" />
                                                <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="2" Caption="መለያ" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="1" Caption="Select">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </td>

                                </tr>



                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Income Amount(Min)"></asp:Label>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtMinIncome" runat="server" Text="0" Width="100%">
                                            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                                        </dx:ASPxTextBox>

                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label runat="server" Text="Income Amount(Max)"></asp:Label>

                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtMaxIncome" ClientInstanceName="txtMaxIncome" runat="server" Text="0" Width="100%">
                                            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                                        </dx:ASPxTextBox>

                                    </td>
                                </tr>




                                <%--    <tr>
                    <td>
                        <asp:Label runat="server" Text="Overdue From(days)"></asp:Label>
                    </td>
                    <td>
                                         <dx:ASPxSpinEdit ID="txtOverdueDaysFrom" ClientInstanceName="txtOverdueDaysFrom" runat="server"  Width="100%" 
                          MinValue="0"  MaxValue="90">
                            
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="text-align:right">
                        <asp:Label runat="server" Text="Overdue To(days)"></asp:Label>

                    </td>
                    <td>
                                            <dx:ASPxSpinEdit ID="txtOverdueDaysTo" ClientInstanceName="txtOverdueDaysTo" runat="server"  Width="100%"
                            MinValue="0" MaxValue="90">
                            
                        </dx:ASPxSpinEdit>
                    </td>
                </tr>--%>

                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Start Date"></asp:Label>

                                    </td>
                                    <td>
                                        <cdp:CUSTORDatePicker ID="dpAppliedDateFrom" runat="server" SelectedDate="" TabIndex="1"
                                            TextCssClass="" Width="100%" meta:resourcekey="dpStartDate" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label runat="server" Text="End Date"></asp:Label>

                                    </td>
                                    <td>
                                        <cdp:CUSTORDatePicker ID="dpAppliedDateTo" runat="server" SelectedDate="" TabIndex="1"
                                            TextCssClass="" Width="100%" meta:resourcekey="dpStartDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label runat="server" Text="Exemption " Style="overflow-wrap: break-word" CssClass="h5"></asp:Label><br />

                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="In Amount" Style="overflow-wrap: break-word"></asp:Label>

                                    </td>
                                    <td style="max-width: 70px">
                                        <dx:ASPxTextBox ID="txtAmount" ClientInstanceName="txtAmount" runat="server" Width="100%">
                                            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text="In Percent Of Raw Tax" Style="overflow-wrap: break-word"></asp:Label><br />

                                    </td>
                                    <td style="max-width: 70px">
                                        <dx:ASPxSpinEdit ID="txtTaxPercent" ClientInstanceName="txtTaxPercent" runat="server" Width="100%" ClientEnabled="false"
                                            MinValue="0" MaxValue="100">
                                        </dx:ASPxSpinEdit>
                                    </td>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="btnChangePercentValue" ClientInstanceName="btnChangePercentValue" Text="Change" Theme="DevEx" AutoPostBack="false" CausesValidation="false" Style="width: 50px; padding: 2px">
                                            <ClientSideEvents Click="function(){txtTaxPercent.SetEnabled(true);}" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td></td>
                                </tr>


                            </table>
                            <dx:ASPxPopupControl runat="server" ID="popCategory" ClientInstanceName="popCategory" CloseAction="CloseButton"
                                PopupHorizontalAlign="WindowCenter" PopupAnimationType="Fade" EnableCallbackAnimation="True" CloseOnEscape="True"
                                PopupVerticalAlign="Above" meta:resourcekey="popCategoryResource1">
                                <SettingsAdaptivity Mode="Always" VerticalAlign="WindowCenter" MinWidth="30%" MaxWidth="50%" MinHeight="80%" MaxHeight="90%" />
                                <ContentCollection>
                                    <dx:PopupControlContentControl runat="server" meta:resourcekey="PopupControlContentControlResource2">
                                        <uc1:BusinessCategoryControl runat="server" ID="BusinessCategoryControl" />
                                        <div style="float: right; padding: 4px">
                                            <dx:ASPxButton runat="server" Text="Select" CssClass="btn btn-sm" AutoPostBack="false" CausesValidation="false">
                                                <ClientSideEvents Click="popClosed" />
                                            </dx:ASPxButton>
                                        </div>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
           </div>
           
                 <div id="divData" style="display:none;width:100%;overflow-x:scroll;padding:2px">
           <dx:ASPxGridView ID="grdList" KeyFieldName="Id" ClientInstanceName="grdList" runat="server" AutoGenerateColumns="False"
                            Width="100%" OnCustomCallback="grdList_CustomCallback" Style="min-width: 600px">
               <ClientSideEvents EndCallback="grdListEndCallback" CustomButtonClick="grdCustomButtonClick" />

                            <SettingsBehavior AllowFocusedRow="false" />
                            <SettingsPopup>
                                <HeaderFilter MinHeight="140px"></HeaderFilter>
                            </SettingsPopup>
                            <Columns>
                               
                                <dx:GridViewDataTextColumn Caption="Tax Type" FieldName="TaxTypeName" ShowInCustomizationForm="True" VisibleIndex="0">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Left" />
                                </dx:GridViewDataTextColumn>
                                 <dx:GridViewDataTextColumn Caption="Taxpayer grade" FieldName="TaxPayerGradeText" ShowInCustomizationForm="True" VisibleIndex="1">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Category Codes" FieldName="CategoryText" ShowInCustomizationForm="True" VisibleIndex="1">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Exemption In Amount" FieldName="TaxAmount" ShowInCustomizationForm="True" VisibleIndex="2">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Exemption In %" FieldName="TaxPercent" ShowInCustomizationForm="True" VisibleIndex="3">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Income Amount(Min)" FieldName="IncomeFrom" ShowInCustomizationForm="True" VisibleIndex="4">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Income Amount(Max)" FieldName="IncomeTo" ShowInCustomizationForm="True" VisibleIndex="5">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                 <dx:GridViewDataTextColumn Caption="Applied Date From" FieldName="AppliedDateFromText" ShowInCustomizationForm="True" VisibleIndex="8">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Applied Date To" FieldName="AppliedDatToText" ShowInCustomizationForm="True" VisibleIndex="9">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>


                          
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="10">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Select">
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>


                            </Columns>
                           
                            <Settings GridLines="Horizontal" ShowFilterRow="false" />
                        </dx:ASPxGridView>
            </div>
           
        </div>
        
   
    </div>

</asp:Content>
