﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Municipality.aspx.cs" MasterPageFile="~/Root.Master" Inherits="RMS.Pages.Municipality" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content runat="server" ContentPlaceHolderID="PageContent">
 
     <style>
        ASPxTextBox {
            width: 100% !important;
        }
        .dxtlFocusedNode_Metropolis,.dxpLite_Metropolis .dxp-current 
        {
            background-color:#009688;
        }
    </style>
   
    <script type="text/javascript">
       
        function grdView_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING') return;
            if (e.buttonID == 'DELETING') {
                s.GetRowValues(e.visibleIndex, "Id", onDeleteMunicipal);
            }
        }
        function onDeleteMunicipal(values) {
            ShowConfirmx('Are you sure? Do you want to delete this record?', function (result) {
                if (result)
                    grdView.PerformCallback('Delete|' + values);
            }, 'en');
        }
        function DoNodeClick(s, e) {
            s.GetNodeValues(e.nodeKey, "Id;Description;Parent", ReturnSelectedPickCategoryRow);
        }
        
        function DoDeleteMunicipality(values) {
            grdView.PerformCallback('Delete|' + values);
        }
        function DoSearchCodeClick(s, e) {

            clbPickCategory.PerformCallback('SearchCode');
        }
        //Disable Parent NodeClick
        function OnClientNodeClicked1(sender, args) {
            if (args.get_node()._hasChildren() == true)
                args.get_node().set_selected(false)
        }

        function ReturnSelectedPickCategoryRow(values) {
            parent = values[2];
            cid = values[0];
            if (parent == -1) {//Parent node
                lblDescription.SetText("Select Category");
                grdView.SetVisible(false);
               // $('#divLeft').hide();
                hdfValue.Set('CategoryId', null);
                return;
            }
           // $('#divLeft').show();
            lblDescription.SetText(values[1]);
            hdfValue.Set('CategoryId',cid)
            grdView.PerformCallback('Bind|' + cid);
            grdView.SetVisible(true);
        }


        function DoNew() {
            hdfValue.Set('IsNewMunicipality', true);
            DoEditMode();
            ClearForm();
            txtRank.SetEnabled(true);
            txtRank.Focus();
            // clbClient.PerformCallback('New');
        }
        function DisableOthers(s) {
            if (s.GetText() === '' || s.GetText() =='0.00') {
                for (i = 1; i <= 8; i++) {
                    id = 'txtCat' + i;
                    window[id].SetEnabled(true);
                }
            }
            else {
                for (i = 1; i <= 8; i++) {
                    id = 'txtCat' + i;
                    if (s != window[id])
                        window[id].SetEnabled(false);
                }
            }
        }
        function DoSaveMunicipal() {
            if (hdfValue.Get('CategoryId') === 'undefined' || typeof hdfValue.Get('CategoryId') === 'undefined' ) {
                ShowError('Please select Category');
                return;
            }
           else if (txtAnnualFee.GetText() === "0.00") {
                ShowError('Please enter the Annual Fee');
                return;
            }
            clbClient.PerformCallback('Save');
        }
        function DoEditMode() {
            btnSave.SetEnabled(true);
            btnDetail.SetEnabled(true);
        }
        function DoDetail() {
            grdView.SetVisible(true);
            btnSave.SetEnabled(false);
            btnDetail.SetEnabled(false);
        }

        function gridViewEndCallback(s) {
            if (s.cpStatus === 'SUCCESS') {
                if (s.cpAction === 'Save') {
                    ShowSuccess(s.cpMessage);
                    //s.PerformCallback();
                }
                else if (s.cpAction === 'Delete') {
                    ShowSuccess(s.cpMessage);
                    s.PerformCallback();
                }


            }
            else if (s.cpStatus === 'ERROR') {
                ShowError(s.cpMessage);
                s.PerformCallback();
            }
            if (s.cpMessage === 'NA') {
                ShowMessage('No record is found');
                s.SetVisible(true);
            }
            else
                s.SetVisible(true);
            s.cpStatus = s.cpAction = s.cpMessage = '';
        }
        function clbClient_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Delete") {
                    DoDetail();
                    ShowSuccess(s.cpMessage);
                    return;
                }
                else if (s.cpAction === "Save") {
                    DoDetail();
                    grdView.PerformCallback('Bind');
                    ShowSuccess(s.cpMessage);
                }
                else if (s.cpAction === "Get") {
                    DoEditMode();
                    txtRank.SetEnabled(false);
                    txtAnnualFee.Focus();
                }
                
            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s) {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }
        function ClearForm() {
            
                txtAnnualFee.SetText('0.00');
                txtRank.SetValue('1');
        }
       
        function SearchCategory() {
            trlCategorys.PerformCallback();
        }
    </script>

  
    <div class="contentmain">
        <h5>
            Municipality
        </h5>
        <div class="card card-body">
             <script src="../../Content/ModalDialog.js"></script>
    <div>
       <div class="row">
            <div class="col col-md-5">
                
                <div id="divLeft" class="card card-flat">
                   <div style="background-color:#F7F7F7;padding:15px;width:100%">
                        
                    <table style="min-width:80%">
                        <tbody>
                            <tr>
                                <td>
                                    <dx:ASPxTextBox runat="server" ID="txtSearch" ClientInstanceName="txtSearch" NullText="Search by name or code" Style="float: left; width: 100%"></dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxButton runat="server" ID="btnSearch" ClientInstanceName="btnSearch" CausesValidation="false" AutoPostBack="false" ToolTip="ፈልግ" style="padding:3px;width:50px"
                                            Theme="Material">
                                            <ClientSideEvents Click="SearchCategory" />
                                            <Image IconID="iconbuilder_actions_zoom_svg_white_16x16">
                                            </Image>
                                        </dx:ASPxButton>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                       </div>
                          <div style="padding-top:13px"></div>      
                    <dx:ASPxTreeList ID="trlCategorys" runat="server" AutoGenerateColumns="False" 
                        ClientInstanceName="trlCategorys"  KeyFieldName="Id" ParentFieldName="Parent" Theme="Metropolis">
                        <ClientSideEvents NodeClick="function(s,e){DoNodeClick(s,e);}" />
                        <Columns>
                           <%-- <dx:TreeListTextColumn Caption="ኮድ" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="1" Width="15%">
                            </dx:TreeListTextColumn>--%>
                            <dx:TreeListTextColumn Caption="Business Category" FieldName="DescriptionShort" ShowInCustomizationForm="True" VisibleIndex="0">
                            </dx:TreeListTextColumn>
                            <dx:TreeListTextColumn FieldName="Id" Visible="false">
                            </dx:TreeListTextColumn>
                             <dx:TreeListTextColumn FieldName="Parent" Visible="false">
                            </dx:TreeListTextColumn>
                             <dx:TreeListTextColumn Caption="Business Category" FieldName="Description" Visible="false">
                            </dx:TreeListTextColumn>
                        </Columns>
                        <%--<Settings GridLines="None" ShowFooter="true" VerticalScrollBarMode="Auto"  />--%>
                        <SettingsBehavior AllowFocusedNode="True" />
                        <SettingsPager Mode="ShowPager" PageSize="12" ShowNumericButtons="false">
                            <PageSizeItemSettings Items="20, 30, 50" Visible="False">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <%--<SettingsLoadingPanel ImagePosition="Top" />--%>

                    </dx:ASPxTreeList>
                        </div>
                </div>


            <div class="col col-md-7" style="padding-left:15px !important">
                
                <div style="padding-bottom:10px;text-align:left">
                    <dx:ASPxLabel runat="server" ID="lblDescription" ClientInstanceName="lblDescription" Font-Size="Larger" CssClass="text text-muted"  Text="Select Category"></dx:ASPxLabel>
              
                </div>
                 
       
              
                          
                         <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdView" Style="margin: 0 auto; margin-top: 10px" ClientVisible="False"
                                EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id" OnRowUpdating="ASPxGridView1_RowUpdating" 
                                OnCustomCallback="ASPxGridView1_CustomCallback" OnPageIndexChanged="ASPxGridView1_PageIndexChanged" OnRowInserting="ASPxGridView1_RowInserting"
                                Width="100%" meta:resourcekey="grdViewResource1">
                                <ClientSideEvents   EndCallback="function(s,e){gridViewEndCallback(s,e);}"  CustomButtonClick="function(s,e){grdView_CustomButtonClick(s,e);}" />
                                <SettingsBehavior AllowFocusedRow="true"  />
                                 <SettingsEditing Mode="Inline">
                            </SettingsEditing>
                                <SettingsPopup>
                                    <HeaderFilter MinHeight="140px"></HeaderFilter>
                                </SettingsPopup>
                                <Columns>
                                    <dx:GridViewDataSpinEditColumn Caption="License Level" FieldName="Rank" ShowInCustomizationForm="True" VisibleIndex="0">
                                    <PropertiesSpinEdit DisplayFormatString="g" MinValue="1" MaxValue="12"></PropertiesSpinEdit>
                                    </dx:GridViewDataSpinEditColumn>
                                    <dx:GridViewDataTextColumn Caption="Amount in Birr" FieldName="AnnualFee" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource3">
                                     <PropertiesTextEdit >
                                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" IncludeLiterals="DecimalSymbol" />
                                        <ValidationSettings Display="Dynamic" >
                                            <RequiredField  IsRequired="true" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CategoryId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowInCustomizationForm="True" ShowNewButton="True" VisibleIndex="3">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewCommandColumn VisibleIndex="4">
                                        <CustomButtons>
                                            <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                <Styles Style-ForeColor="Red"></Styles>
                                            </dx:GridViewCommandColumnCustomButton>
                                        </CustomButtons>
                                    </dx:GridViewCommandColumn>
                                </Columns>
                                <Styles AdaptiveDetailButtonWidth="22">
                                    <SelectedRow BackColor="Orange" ForeColor="White">
                                    </SelectedRow>
                                </Styles>
                             <SettingsText CommandEdit="Edit" CommandDelete="Delete"  CommandNew="New" 
                                ConfirmDelete="Are you sure do you want to delete this record?" CommandUpdate="Save" CommandCancel="Cancel"  />
                            </dx:ASPxGridView>
              
                    <dx:ASPxHiddenField runat="server" ID="hdfValue" ClientInstanceName="hdfValue"></dx:ASPxHiddenField>

                
                 

                     

                
            
                   
               
                    </div>
           
        </div>
   </div>
    <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
       </div>
    </div>
</asp:Content>
