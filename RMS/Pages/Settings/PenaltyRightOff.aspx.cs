﻿using System;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTOR.Common;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;
using System.Collections.Specialized;
using System.Web.Security;
using DevExpress.Web;

namespace RMS.Pages.Settings
{
    public partial class PenaltyRightOff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (!Page.IsPostBack)
                {
                    cdpRegistrationDate.SelectedDate = CUSTORCommon.Ethiopic.EthiopicDateTime.GetEthiopianDate(DateTime.Today);
                    LoadPenaltyRightOffParents();
                }
            }
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        private void StorePreErrorValues()
        {
            try
            {
                //Cascaded combos lose their items in this case
                string strParentPenaltyRightOff = string.Empty;

                if (cboParentPenaltyrightoff.Value != null)
                {
                    strParentPenaltyRightOff = cboParentPenaltyrightoff.Value.ToString();
                }

                Session["SelectedParentPenaltyRightOff"] = strParentPenaltyRightOff;
            }
            catch
            {
                //do nothing
            }
        }

        private bool DoValidate()
        {
            bool isValid = true;
            List<string> errMessages = GetErrorMessage();
            StorePreErrorValues();
            if (errMessages.Count > 0)
            {
                isValid = false;
                RestoreComboValues();
                clbPenaltyRightOff.JSProperties["cpAction"] = "registration";
                clbPenaltyRightOff.JSProperties["cpStatus"] = "error";
                clbPenaltyRightOff.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }

        protected void RestoreComboValues()
        {

        }

        protected void LoadPenaltyRightOffParents()
        {
            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            List<PenaltyLiftingSettingView> objPenaltyLiftingSettingList = objPenaltyLiftingSettingBusiness.GetPenaltyRightOffParentList();
            cboParentPenaltyrightoff.ValueField = "Id";
            cboParentPenaltyrightoff.TextField = "Description";
            cboParentPenaltyrightoff.DataSource = objPenaltyLiftingSettingList;
            cboParentPenaltyrightoff.DataBind();
        }

        protected PenaltyLiftingSetting AssignPenaltyLiftingSettingControlsToObject()
        {
            PenaltyLiftingSetting objPenaltyLiftingSetting = new PenaltyLiftingSetting();
            objPenaltyLiftingSetting.Description = txtPenaltyRightOffDescription.Text;
            CVGeez objGeez = new CVGeez();
            objPenaltyLiftingSetting.DescriptionSortV = objGeez.GetSortValueU(txtPenaltyRightOffDescription.Text);
            objPenaltyLiftingSetting.DescriptionSoundX = objGeez.GetSoundexValue(txtPenaltyRightOffDescription.Text);
            if (chkIsParent.Checked)
            {
                objPenaltyLiftingSetting.Parent = -1;
                objPenaltyLiftingSetting.IsParent = true;
                objPenaltyLiftingSetting.DelayPenaltyPercentage = 0;
                objPenaltyLiftingSetting.HidingIncomePenaltyPercentage = 0;
                objPenaltyLiftingSetting.LackOfStatementPenaltyPercentage = 0;
                objPenaltyLiftingSetting.UnderStatementPercentage = 0;
            }
            else
            {
                objPenaltyLiftingSetting.Parent = Convert.ToInt32(cboParentPenaltyrightoff.Value.ToString());
                objPenaltyLiftingSetting.IsParent = false;
                objPenaltyLiftingSetting.DelayPenaltyPercentage = Convert.ToInt32(txtLatePayment.Text);
                objPenaltyLiftingSetting.HidingIncomePenaltyPercentage = Convert.ToInt32(txtIncomeHiding.Text);
                objPenaltyLiftingSetting.LackOfStatementPenaltyPercentage = Convert.ToInt32(txtNoFinancialRecord.Text);
                objPenaltyLiftingSetting.UnderStatementPercentage = Convert.ToInt32(txtUnderStatement.Text);
            }
            
            if (!(bool)hfPenaltyLiftingSettingFormStatus["isNewPenaltyRightOff"])
            {
                objPenaltyLiftingSetting.UpdatedDate = DateTime.Today;
                objPenaltyLiftingSetting.UpdatedBy = HttpContext.Current.User.Identity.Name;
                objPenaltyLiftingSetting.UpdatedUserId = CurrentProfile.ActiveUserProfile.UserId;
            }
            else
            {
                objPenaltyLiftingSetting.CreatedDate = DateTime.Today;
                objPenaltyLiftingSetting.CreatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                objPenaltyLiftingSetting.CreatedBy = HttpContext.Current.User.Identity.Name;
            }
            objPenaltyLiftingSetting.Date = Convert.ToDateTime(cdpRegistrationDate.SelectedGCDate);
            objPenaltyLiftingSetting.IsDeleted = false;
            objPenaltyLiftingSetting.IsActive = true;
            objPenaltyLiftingSetting.CenterId = CurrentProfile.ActiveUserProfile.TaxCenterId;
            objPenaltyLiftingSetting.CenterName = CurrentProfile.ActiveUserProfile.Name;
            return objPenaltyLiftingSetting;
        }

        protected bool SavePenaltyLiftingSetting()
        {
            bool saved = false;

            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            PenaltyLiftingSetting objPenaltyLiftingSetting = AssignPenaltyLiftingSettingControlsToObject();
            if (objPenaltyLiftingSetting != null)
            {
                if (!objPenaltyLiftingSettingBusiness.GetPenaltyRightOffExistance(objPenaltyLiftingSetting.DescriptionSoundX))
                {
                    if (objPenaltyLiftingSettingBusiness.InsertPenalityLifting(objPenaltyLiftingSetting))
                    {
                        saved = true;
                    }
                }
                else
                {
                    clbPenaltyRightOff.JSProperties["cpAction"] = "penaltyRightOffExist";
                    clbPenaltyRightOff.JSProperties["cpStatus"] = "error";
                    clbPenaltyRightOff.JSProperties["cpMessage"] = "The penalty rightoff setting description you try to registered previously";
                }
            }
            if (saved)
            {
                clbPenaltyRightOff.JSProperties["cpAction"] = "registration";
                clbPenaltyRightOff.JSProperties["cpStatus"] = "success";
                clbPenaltyRightOff.JSProperties["cpMessage"] = "penalty rightoff registered successfully";
            }

            return saved;
        }

        protected bool SaveEditedPenaltyLiftingSetting()
        {
            bool saved = false;

            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            PenaltyLiftingSetting objPenaltyLiftingSetting = AssignPenaltyLiftingSettingControlsToObject();
            if (objPenaltyLiftingSetting != null)
            {
                objPenaltyLiftingSetting.Id = Convert.ToInt32(Session["PenaltyRightOffId"].ToString());
                if (objPenaltyLiftingSettingBusiness.UpdatePenalityLifting(objPenaltyLiftingSetting))
                {
                    saved = true;
                    clbPenaltyRightOff.JSProperties["cpAction"] = "updated";
                    clbPenaltyRightOff.JSProperties["cpStatus"] = "success";
                    clbPenaltyRightOff.JSProperties["cpMessage"] = "penalty rightoff updated successfully";
                }
            }
            
            return saved;
        }

        protected bool DoSave()
        {
            bool saved = false;

            if (DoValidate())
            {
                if ((bool)hfPenaltyLiftingSettingFormStatus["isNewPenaltyRightOff"])
                {
                    RestoreComboValues();
                    saved = SavePenaltyLiftingSetting();
                }
                else
                {
                    RestoreComboValues();
                    saved = SaveEditedPenaltyLiftingSetting();
                }
            }

            return saved;
        }

        protected void LoadPenalityRightOffSettingsInfo(int penaltyRightOffId)
        {
            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            PenaltyLiftingSetting objPenaltyLiftingSetting = objPenaltyLiftingSettingBusiness.GetPenalityLifting(penaltyRightOffId);
            if (objPenaltyLiftingSetting != null)
            {
                LoadPenaltyRightOffParents();
                Session["PenaltyRightOffId"] = objPenaltyLiftingSetting.Id;
                chkIsParent.Checked = objPenaltyLiftingSetting.IsParent;
                txtIncomeHiding.Text = objPenaltyLiftingSetting.HidingIncomePenaltyPercentage.ToString();
                txtLatePayment.Text = objPenaltyLiftingSetting.DelayPenaltyPercentage.ToString();
                txtNoFinancialRecord.Text = objPenaltyLiftingSetting.LackOfStatementPenaltyPercentage.ToString();
                txtUnderStatement.Text = objPenaltyLiftingSetting.UnderStatementPercentage.ToString();
                txtPenaltyRightOffDescription.Text = objPenaltyLiftingSetting.Description;
                cdpRegistrationDate.SelectedDate = EthiopicDateTime.GetEthiopianDate(objPenaltyLiftingSetting.Date);
                hfPenaltyLiftingSettingFormStatus.Set("isNewPenaltyRightOff", false);
                if (objPenaltyLiftingSetting.Parent != -1)
                {
                    clbPenaltyRightOff.JSProperties["cpIsParent"] = "No";
                    cboParentPenaltyrightoff.Value = objPenaltyLiftingSetting.Parent.ToString();
                }
                else
                {
                    clbPenaltyRightOff.JSProperties["cpIsParent"] = "Yes";
                }
                clbPenaltyRightOff.JSProperties["cpAction"] = "PenaltyRightOffSettingLoaded";
                clbPenaltyRightOff.JSProperties["cpStatus"] = "success";
            }
        }

        protected void DoDelete()
        {

        }

        protected void clbPenaltyRightOff_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                strID = parameters[1];
            switch (strParam)
            {
                case "Save":
                case "Edit":
                    DoSave();
                    break;
                case "Delete":
                    DoDelete();
                    break;
                case "LoadPenalityRightOffSettingsInfo":
                    LoadPenalityRightOffSettingsInfo(Convert.ToInt32(strID));
                    break;
                default:
                    break;
            }
        }

        protected void LoadPenaltyRightOffList()
        {
            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            List<PenaltyLiftingSettingView> objPenaltyLiftingSettingList = objPenaltyLiftingSettingBusiness.GetPenaltyRightOffParentList();
            gvwPenaltyRightOffSettings.DataSource = objPenaltyLiftingSettingList;
            gvwPenaltyRightOffSettings.DataBind();
            gvwPenaltyRightOffSettings.DetailRows.ExpandRow(0);
        }

        protected void gvwPenaltyRightOffSettings_PageIndexChanged(object sender, EventArgs e)
        {
            LoadPenaltyRightOffList();
        }

        protected void gvwPenaltyRightOffSettings_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            string parameters = e.Parameters;
            switch (parameters)
            {
                case "List":
                    LoadPenaltyRightOffList();
                    break;
            }
        }

        protected void gvwPenaltyRightOffSettingsDetail_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["PenaltyRightOffParentId"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gvwPenaltyRightOffSettingsDetail_DataBinding(object sender, EventArgs e)
        {
            DevExpress.Web.ASPxGridView grd = (DevExpress.Web.ASPxGridView)sender;
            int penaltyRightOffParentId = Int32.Parse(Session["PenaltyRightOffParentId"].ToString());
            PenaltyLiftingSettingBusiness objPenaltyLiftingSettingBusiness = new PenaltyLiftingSettingBusiness();
            List<PenaltyLiftingSetting> objPenaltyLiftingSettingChildList = objPenaltyLiftingSettingBusiness.GetPenaltyRightOffParentListByParent(penaltyRightOffParentId);
            grd.DataSource = objPenaltyLiftingSettingChildList;
        }
    }
}