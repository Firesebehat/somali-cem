﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages.AccountCodes
{
    public partial class PaymentAccount : System.Web.UI.Page
    {
        public int Lan { get { return 1; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                BindGrid();
                CDropdown.FillComboFromEnum(cboTaxType, Language.eLanguage.eAmharic, typeof(CUSTORCommon.Enumerations.TaxType.eTaxType));
                CDropdown.FillComboFromEnum(cboPaymentType, Language.eLanguage.eAmharic, typeof(PaymentType.ePaymentType));

            }
        }

        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void BindGrid()
        {

            PaymentAccountsBusiness business = new PaymentAccountsBusiness();
            try
            {
                List<PaymentAccounts> list = business.GetPaymentAccountss(Lan);
                if (list.Count > 0)
                {
                    grdView.DataSource = list;
                }
                else
                {
                    grdView.DataSource = null;
                }
                grdView.DataBind();
            }
            catch
            {
                
            }
           
        }


        protected void ClearForm()
        {
            txtCode.Text = string.Empty;
            cboPaymentType.SelectedIndex = -1;
            cboTaxType.SelectedIndex = -1;
        }


        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }


        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        private void DoDelete()
        {
            clbClient.JSProperties["cpAction"] = "Delete";
            int Id = Int32.Parse( Session["PaymentAccountId"].ToString());
            PaymentAccountsBusiness objPaymentAccountsBusiness = new PaymentAccountsBusiness();
            try
            {
                objPaymentAccountsBusiness.Delete(Id);
                ShowSuccess("The record is deleted successfully");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }


        }


        protected void DoFind(int id)
        {
            clbClient.JSProperties["cpAction"] = "Get";
            PaymentAccountsBusiness objPaymentAccountsBusiness = new PaymentAccountsBusiness();
            Session["PaymentAccountId"] = id;

            try
            {
                PaymentAccounts objAct   = objPaymentAccountsBusiness.GetPaymentAccounts(id);
                ClearForm();
                hdfStatus["IsNewPaymentAccount"] = false;
                hdfCode["AccountId"] = objAct.AccountCode;
                txtCode.Text = GetAccountCodeName( objAct.AccountCode);
                cboTaxType.Value = objAct.TaxType;
                cboPaymentType.Value = objAct.PaymentType;
                ValidateTrue();
                Session["PaymentAccountId"] = id;
                clbClient.JSProperties["cpStatus"] = "SUCCESS";
                return;
            }
            catch (Exception ex)
            {

                ShowError(ex.Message);
            }
        }
        private string GetAccountCodeName(int id)
        {
            ChartOfAccountsBussiness business = new ChartOfAccountsBussiness();
            try
            {
                ChartOfAccountsDomain objAccount = business.GetChartOfAccounts(id);
                return objAccount.Code + "-" + objAccount.Name;
            }
            catch
            {
                return string.Empty;
            }
        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            if (txtCode.Text.Trim().Length == 0)
            {
                errMessages.Add("የሂሳብ መደብ ባዶ መሆን አይችልም!");
            }
            if (cboTaxType.Value == null)
            {
                errMessages.Add("Tax type is required.");
            }
            if (cboPaymentType.Value == null)
            {
                errMessages.Add("Payment type is required!");
            }

            return errMessages;
        }
        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }
       
        protected void DoSave()
        {
            clbClient.JSProperties["cpAction"] = "Save";
            DateTime eventDate = DateTime.Now;
            CVGeez objGeez = new CVGeez();
            PaymentAccounts objPaymentAccounts = new PaymentAccounts();
            try
            {
                
                List<string> errMessages = GetErrorMessage();
                bool isValid = ASPxEdit.ValidateEditorsInContainer(clbClient);

                if (errMessages.Count > 0)
                {
                    clbClient.JSProperties["cpAction"] = "ERROR";
                    ShowError(GetErrorDisplay(errMessages));
                    return;
                }
                if (!isValid)
                {
                    return;
                }

                objPaymentAccounts.AccountCode =Int32.Parse(hdfCode["AccountId"].ToString());
                objPaymentAccounts.TaxType = int.Parse(cboTaxType.Value.ToString());
                objPaymentAccounts.PaymentType = int.Parse(cboPaymentType.Value.ToString());
                PaymentAccountsBusiness objPaymentAccountsBusiness = new PaymentAccountsBusiness();
                if ((bool)hdfStatus["IsNewPaymentAccount"])
                {
                    if (objPaymentAccountsBusiness.Exists(objPaymentAccounts))
                    {
                        ShowError("Record exist.");
                        return;
                    }
                    objPaymentAccounts.CreatedBy = User.Identity.Name;
                    objPaymentAccounts.CreatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    objPaymentAccounts.CreatedDate = eventDate;
                    objPaymentAccountsBusiness.InsertPaymentAccounts(objPaymentAccounts);
                }
                else
                {
                    objPaymentAccounts.Id = int.Parse(Session["PaymentAccountId"].ToString());
                    if (objPaymentAccountsBusiness.UpdateExists(objPaymentAccounts))
                    {
                        ShowError("Record exist");
                        return;
                    }
                    objPaymentAccounts.UpdatedBy = User.Identity.Name;
                    objPaymentAccounts.UpdatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    objPaymentAccounts.UpdatedDate = eventDate;
                    objPaymentAccountsBusiness.UpdatePaymentAccounts(objPaymentAccounts);
                    ShowSuccess("The record is successfully saved.");
                }
                hdfStatus["IsNewPaymentAccount"] = true;
                ClearForm();
                ShowSuccess("The record is successfully saved.");
                return;
            }
            catch (Exception ex)
            {
               ShowError(ex.Message);
            }
        }


    


        public bool DoNew()
        {
            try
            {
                hdfStatus["IsNewPaymentAccount"] = true;
                ClearForm();
                Session["PaymentAccountId"] = ID;
                clbClient.JSProperties["cpAction"] = "New";
                ValidateTrue();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        private void ValidateTrue()
        {
            cboTaxType.IsValid = true;
            cboPaymentType.IsValid = true;
            txtCode.IsValid = true;
        }

        protected void ClbClient_Callback(object sender, CallbackEventArgsBase e)
        {
            clbClient.JSProperties["cpStatus"] = "";
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Bind":
                    BindGrid();
                    break;
                case "Get":
                    DoFind(Convert.ToInt16(strID));
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Delete":
                    DoDelete();
                    break;

                default:
                    break;
            }
        }

        protected void grdView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] param = e.Parameters.Split('|');
           
            BindGrid();

        }

        protected void grdView_PageIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}