﻿using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Accounts
{
    public partial class ChartOfAccount : System.Web.UI.Page
    {
        public int Lan { get { return 1; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack && !Page.IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                hdfStatus["IsNewCAccount"] = false;
                FillParentcode();
                FillHirachy();
            }
           
        }

       
        protected void clbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains("|"))
                strID = parameters[1];
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Delete":
                    DoDelete();
                    break;
                
                case "Get":
                    DoGet(Convert.ToInt32(strID));
                    Session["IsNewAccount"] = false;
                    break;
            }
        }

        private void DoDelete()
        {
            clbClient.JSProperties["cpAction"] = "Delete";
            ChartOfAccountsBussiness business = new ChartOfAccountsBussiness();
            try
            {
                int Id = Int32.Parse(Session["CAccountId"].ToString());
                business.Delete(Id);
                ShowSuccess("The record is deleted succcessfully");
                return;
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoGet(int id)
        {
            clbClient.JSProperties["cpAction"] = "Get";
            DoNew();
            ChartOfAccountsBussiness business = new ChartOfAccountsBussiness();
            try
            {
                ChartOfAccountsDomain accoutCode = business.GetChartOfAccounts(id);
                txtCode.Text = accoutCode.Code;
                txtFatherCode.Text = accoutCode.FatherCode;
                cboHiracchy.Value = accoutCode.Hirachy.ToString();
                txtName.Text = accoutCode.Name;
                txtNameEng.Text = accoutCode.NameEng;
                chbIsActive.Checked = accoutCode.IsActive;
                chbIsBudgetAccount.Checked = accoutCode.IsBudgetAccount;
                if (Int32.Parse(accoutCode.Parent.ToString()) > 0)
                    cboParentCode.Value = Int32.Parse(accoutCode.Parent.ToString());
                chbIsMainAccount.Checked = accoutCode.IsMain;
                hdfStatus["IsNewCAccount"] = false;
                Session["CAccountId"] = id;
                clbClient.JSProperties["cpStatus"] = "SUCCESS";


            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoSave()
        {
            ChartOfAccountsDomain account = new ChartOfAccountsDomain();
            CVGeez objGeez = new CVGeez();
            DateTime eventDate = DateTime.Now;
            try
            {
               //validation is on client side
                clbClient.JSProperties["cpAction"] = "Save";
                // var user =Membership.GetUser();
                // var userProfile = CurrentProfile.ActiveUserProfile;
                account.FatherCode =  txtFatherCode.Text;
                account.Hirachy = Convert.ToInt16(cboHiracchy.Value);
                account.Code = txtCode.Text;
                account.Name = txtName.Text;
                account.NameEng = txtNameEng.Text;
                account.NameSortValue = objGeez.GetSortValueU(txtName.Text);
                account.NameSoundx = objGeez.GetSortValueU(txtName.Text);
                account.IsBudgetAccount = chbIsBudgetAccount.Checked;
                account.IsMain = chbIsMainAccount.Checked;
                account.IsActive = chbIsActive.Checked;
                account.Parent = cboParentCode.Value == null ? -1 : Convert.ToInt16(cboParentCode.Value);
                
                
                ChartOfAccountsBussiness actBusiness = new ChartOfAccountsBussiness();
                if ((bool)hdfStatus["IsNewCAccount"])
                {
                    account.CreatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    account.CreatedBy = User.Identity.Name;
                    account.CreatedDate = eventDate;
                    if (actBusiness.Exists(account))
                    {
                        ShowError("Record exists.");
                        return;
                    }
                    actBusiness.InsertChartOfAccounts(account);
                }
                else
                {
                    account.UpdatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    account.UpdatedBy = User.Identity.Name;
                    account.UpdatedDate = eventDate;
                    account.Id = Convert.ToInt32(Session["CAccountId"]);
                    if (actBusiness.UpdateExists(account))
                    {
                        ShowError("Record exists");
                        return;
                    }
                    actBusiness.UpdateChartOfAccounts(account);
                }
                ShowSuccess("The record is successfully saved");
                return ;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return;
            }
        }

        private void DoNew()
        {
            txtFatherCode.Text = string.Empty;
            txtCode.Text = string.Empty;
            cboHiracchy.SelectedIndex = -1;
            txtName.Text = string.Empty;
            txtNameEng.Text = string.Empty;
            chbIsActive.Checked = true;
            chbIsMainAccount.Checked = false;
            chbIsBudgetAccount.Checked = false;
            cboParentCode.SelectedIndex = -1;
            hdfStatus["IsNewCAccount"] = true;
            ValidControls();
            txtFatherCode.Focus();
        }
        private void ValidControls()
        {
            txtFatherCode.IsValid = true;
            txtCode.IsValid = true;
            cboHiracchy.IsValid = true;
            txtName.IsValid = true;
            txtNameEng.IsValid = true;
            cboParentCode.IsValid = true;
        }

            private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }

        
        private void FillParentcode()
        {
           
            string strParent = "Select case "+ Lan + " when 1 then Name else NameEng end as Name, Id  FROM ChartOfAccounts WHERE (Parent = 0 or parent is null) Order by Name";
            string strCategory = "Select case "+ Lan + " when 1 then Name else NameEng end as Name, Id  FROM ChartOfAccounts WHERE [Hirachy] = 0 and (Parent = 0 or parent is null) Order by Name";
            CDropdown.FillComboFromDB(cboParentCode, strParent, "Name", "Id");

        }
        private void FillHirachy()
        {
            CDropdown.FillComboFromEnum(cboHiracchy,Language.eLanguage.eAmharic, typeof(AccountHirachy.Hirachy));
        }
      
    }
}