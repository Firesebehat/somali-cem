﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentAccount.aspx.cs" Inherits="RMS.Pages.AccountCodes.PaymentAccount" MasterPageFile="~/Root.Master" %>

<%@ Register Src="~/Controls/ChartOfAccountControl.ascx" TagPrefix="uc1" TagName="ChartOfAccountControl" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <script type="text/javascript">
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNew();
            }
            else if (e.item.name === "mnuSave") {
                DoSave();
            }
            else if (e.item.name === "mnuList") {
                DoDetail();
            }
            else if (e.item.name === "mnuDelete") {
                onDeleteAccountCode();
            }
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }


        function grdView_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Bind") {
                    if (s.cpMessage === "NA") {
                        ClearForm();
                        DoEditMode();
                        txtRank.SetEnabled(true);
                    }
                    else {
                        DoDetail();
                    }
                }
                else if (s.cpAction === "Delete") {
                    DoDetail();
                    ShowSuccess(s.cpMessage);
                    //grdView.PerformCallback('Bind');
                    return;
                }
            }
            else if (s.cpStatus === "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            ClearJsProperty(s);
        }
        function grdView_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING' && e.buttonID != 'ADDING') return;
             if (e.buttonID == 'EDITING') {
                s.GetRowValues(e.visibleIndex, "Id", onEditAccountCode);

            }
            else if (e.buttonID == 'ADDING')
                OnAddBusinessSelected();
        }
        function onEditAccountCode(values) {
            clbClient.PerformCallback('Get|' + values);
        }
        function DoNodeClick(s, e) {
            popActCode.Hide();
            hdfCode.Set('AccountId', e.nodeKey);
            s.GetNodeValues(e.nodeKey, 'Name;Code', function (values) { txtCode.SetText(values[1] + '-' + values[0]); txtCode.SetValue(values[1] + '-' + values[0]); });
        }
        
        function onDeleteAccountCode() {
            ShowConfirmx("Are you sure do you want to delete this record", function (result) {
                if (result)
                    DoDeletPaymentAccount();
            },'en');
        }
        function DoDeletPaymentAccount(values) {
            clbClient.PerformCallback('Delete');
        }
        function DoSearchCodeClick(s, e) {

            clbClient.PerformCallback('SearchCode');
        }
        function DoSave() {
            if (!ASPxClientEdit.ValidateGroup('VAccount')) {
                ShowError('Some entries are required');
                return;
            }
            clbClient.PerformCallback('Save');
        }
        function DoNew() {
            DoEditMode();
            ToggleEnabled("mnuDelete", false);
            clbClient.PerformCallback('New');
        }
        function DoEditMode() {
            $('#pnlGrid').hide();
            $('#pnlData').show();
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuList", true);
            ToggleEnabled("mnuDelete", true);
        }
        function DoDetail() {
            $('#pnlGrid').show();
            $('#pnlData').hide();
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuList", false);
            ToggleEnabled("mnuDelete", false);
            grdView.PerformCallback();
        }
        function clbClient_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Delete") {
                    DoDetail();
                    ShowSuccess(s.cpMessage);
                    return;
                }
                else if (s.cpAction === "Save") {
                    DoDetail();
                   // grdView.PerformCallback();
                    ShowSuccess(s.cpMessage);
                }
                else if (s.cpAction === "Get") {
                    DoEditMode();
                }

            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s) {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }
        
       
        
    </script>
       <style type="text/css">
           
         .text-content
         {
             max-width:800px !important;
             margin:auto;
             text-align:center;
         }
         .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
       

         .dxeTextBoxDefaultWidthSys {
            width: 100% !important
        }

        Label {
            border-color: silver;
        }
        

        table {
            width: 100%;
        }
       
         h5 {
            font-weight: bold;
            margin-top: 10px;
            text-align: left;
            color: #009688;
        }
          .menu {
            text-align: center;
            background-color: #F7F7F7;
            padding: 5px 0px 10px 0px;
            margin-top:4px;
        }
    </style>
    
    <div class="contentmain">
        <h5>
            Payment Account
        </h5>
        <div class="card card-body">
        <script src="../../../Content/ModalDialog.js"></script>
        <div style="max-width: 600px;">
            <div class="col col-md-12">
                  <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                   <dx:MenuItem Name="mnuDelete" Text="Delete" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="diagramicons_del_svg_16x16"></Image>
                            </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>

            </div>
            <div style="padding-top:15px">

            </div>
          
                            <div class="row">
                                <div class="col col-md-12">
                                    <div id="pnlGrid" style="display:none">
                                        <dx:ASPxGridView ID="grdView" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdView" Style="margin: 0 auto"
                                            EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id"
                                            OnCustomCallback="grdView_CustomCallback" OnPageIndexChanged="grdView_PageIndexChanged"
                                            Width="100%" meta:resourcekey="grdViewResource1">
                                            <ClientSideEvents CustomButtonClick="grdView_CustomButtonClick" EndCallback="function(s,e){grdView_EndCallback(s,e);}" />
                                            <SettingsBehavior AllowSort="false" AllowHeaderFilter="false" AllowFocusedRow="true" ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                                            <Columns>

                                                <dx:GridViewDataTextColumn Caption="Chart of account" FieldName="AccountCodeName" ShowInCustomizationForm="True" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Tax type" FieldName="TaxTypeName" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Payment type" FieldName="PaymentTypeName" Visible="false" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource3">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewCommandColumnResource1">
                                                    <CustomButtons>

                                                        <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Edit" meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                                        </dx:GridViewCommandColumnCustomButton>
                                                       <%-- <dx:GridViewCommandColumnCustomButton ID="SPACER" Text=" " meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                                        </dx:GridViewCommandColumnCustomButton>--%>
                                                    <%--    <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                                            <Styles Style-ForeColor="Red">
                                                                <Style ForeColor="Red"></Style>
                                                            </Styles>
                                                        </dx:GridViewCommandColumnCustomButton>--%>
                                                    </CustomButtons>

                                                </dx:GridViewCommandColumn>
                                            </Columns>
                                            <Styles AdaptiveDetailButtonWidth="22">
                                                <SelectedRow BackColor="Orange" ForeColor="White">
                                                </SelectedRow>
                                            </Styles>
                                        </dx:ASPxGridView>
                                    </div>
                                </div>

                                <div class="col col-md-12">
                                    <div id="pnlData" style="display: none">
                                        <dx:ASPxRoundPanel runat="server" ShowHeader="false">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                        <dx:ASPxCallbackPanel runat="server" ID="clbClient" ClientInstanceName="clbClient" OnCallback="ClbClient_Callback">
                                            <ClientSideEvents EndCallback="clbClient_EndCallback" />
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxHiddenField ID="hdfStatus" ClientInstanceName="hdfStatus" runat="server"></dx:ASPxHiddenField>
                                                    <table>



                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel runat="server" Text="የታክስ አይነት"></dx:ASPxLabel>
                                                                <span class="star">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox runat="server" ID="cboTaxType" Width="100%" ValueType="System.Int32" >
                                                                    <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የግብር አይነት ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel runat="server" Text="የክፍያ አይነት"></dx:ASPxLabel>
                                                                <span class="star">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox runat="server" ID="cboPaymentType" ClientInstanceName="cboPaymentType" Width="100%" ValueType="System.Int32">
                                                                    <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የክፍያ አይነት ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel runat="server" Text="የአካውንቱ ኮድ"></dx:ASPxLabel>
                                                                <span class="star">*</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtCode" ClientInstanceName="txtCode" MaxLength="200" Width="100%">
                                                                    <ClientSideEvents GotFocus="function(){   popActCode.Show();}" />
                                                                    <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የአካውንቱ ኮድ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxHiddenField ID="hdfCode" runat="server" ClientInstanceName="hdfCode"></dx:ASPxHiddenField>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                                    </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />
                        

        <dx:ASPxPopupControl runat="server" ID="popActCode" ClientInstanceName="popActCode" CloseAction="CloseButton"
            PopupHorizontalAlign="WindowCenter" PopupAnimationType="Fade" EnableCallbackAnimation="true" CloseOnEscape="true"
            PopupVerticalAlign="TopSides" ShowHeader="true" HeaderText="Select Account">
            <HeaderStyle  BackColor="#009688" ForeColor="White"/>
            <SettingsAdaptivity Mode="Always" VerticalAlign="WindowCenter" MinWidth="40%" MaxWidth="50%" MinHeight="90%" MaxHeight="95%" />
            <ContentCollection>

                <dx:PopupControlContentControl>
                    <uc1:ChartOfAccountControl runat="server" ID="ChartOfAccountControl" />

                    <%-- <div style="margin-top:4px;float:right">
                    <dx:ASPxButton runat="server" ID="btnClose" ClientInstanceName="btnClose" AutoPostBack="false" CausesValidation="false" CssClass="btn btn-sm" Text="ምረጥ">
                        <ClientSideEvents Click="function(){popActCode.Hide();}" />
                    </dx:ASPxButton>
                </div>--%>
                </dx:PopupControlContentControl>

            </ContentCollection>
        </dx:ASPxPopupControl>
    </div>
        </div>
</asp:Content>