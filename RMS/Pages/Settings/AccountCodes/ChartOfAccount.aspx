﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChartOfAccount.aspx.cs" MasterPageFile="~/Root.Master" Inherits="RMS.Accounts.ChartOfAccount" %>

<%@ Register Src="~/Controls/ChartOfAccountControl.ascx" TagPrefix="uc1" TagName="ChartOfAccountControl" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>


<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <script type="text/javascript">
        function onMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNew();
            }
            else if (e.item.name === "mnuSave") {
                DoSave();
            }
            else if (e.item.name === "mnuList") {
                DoDetail();
            }
            else if (e.item.name === "mnuDelete") {
                DoDelete();
            }
        }
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }
        function grdView_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING' && e.buttonID != 'ADDING') return;
            if (e.buttonID == 'DELETING') {
                s.GetRowValues(e.visibleIndex, "Id", onDeleteAccountCode);
            }
            else if (e.buttonID == 'EDITING') {
                s.GetRowValues(e.visibleIndex, "Id;CategoryId", onEditAccountCode);

            }
            else if (e.buttonID == 'ADDING')
                OnAddBusinessSelected();
        }
        function onEditAccountCode(values) {
            clbClient.PerformCallback('Get|' + values[0] + '|' + values[1]);
        }
        function DoNodeClick(s, e) {
            clbClient.PerformCallback('Get|' + e.nodeKey);
        }

        function onDeleteAccountCode(values) {
            ShowConfirmx("Are you sure? Do you want to delete this record", function (result) {
                if (result)
                    DoDeleteAccountCode(values);
            },'en');
        }
        function DoDeleteAccountCode(values) {
            grdView.PerformCallback('Delete|' + values);
        }
        function DoSearchCodeClick(s, e) {

            clbClient.PerformCallback('SearchCode');
        }
        function DoSave() {
            if (!ASPxClientEdit.ValidateGroup('VAccount')) {
                ShowError('Some entries are required.');
                return;
            }
            clbClient.PerformCallback('Save');
        }
        function DoDelete() {
            ShowConfirmx("Are you sure, do you want to delete this record?", function (result) {
                if (result)
                    clbClient.PerformCallback('Delete');
            },'en');
            
        }


        function DoNew() {
            DoEditMode();
            clbClient.PerformCallback('New');
            IsMainChecked(false);
        }


        function DoEditMode() {
            $('#pnlGrid').hide();
            $('#pnlData').show();
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuDelete", true);
            ToggleEnabled("mnuList", true);
            IsMainChecked(chbIsMainAccount.GetChecked());
        }
        function DoDetail() {
            $('#pnlGrid').show();
            $('#pnlData').hide();
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuList", false);
            txtCodeSearch.SetText('');
            cboParentSearch.SetSelectedIndex(-1);
            txtNameSearch.SetText('');
            trlChartAccount.PerformCallback();
        }


        function clbClient_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Delete") {
                    DoDetail();
                    ShowSuccess(s.cpMessage);
                    return;
                }
                else if (s.cpAction === "Save") {
                    DoDetail();
                    trlChartAccount.PerformCallback();
                    ShowSuccess(s.cpMessage);
                }
                else if (s.cpAction === "Get") {
                    DoEditMode();
                }

            }
            else if (s.cpStatus == "ERROR") {
                IsMainChecked(chbIsMainAccount.GetChecked());
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s) {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }
        function IsMainChecked(s) {
            if (s) {
                $('#mainText').hide();
                $('#mainControl').hide();
            }
            else {
                $('#mainText').show();
                $('#mainControl').show();
            }
        }
        $(document).ready(function () {
            $('#pnlData').hide();
            $('#pnlGrid').show();

        });

    </script>
    <style type="text/css">
       

        .star {
            color: red;
            margin: 1px;
            font-size: large;
        }


      
        .dxtlFocusedNode_Metropolis,.dxpLite_Metropolis .dxp-current 
        {
            background-color:#009688;
        }

    </style>

   
     <div class="contentmain">
         <h5>Chart of accounts</h5>
         <div class="card card-body">
    <div style="max-width: 600px;">
        
        <div class="col col-md-12">
            

            <div>

                 <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                        ShowPopOutImages="True" EnableTheming="True" Height="35px"
                        ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                        AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                        <ClientSideEvents Init="onMenuInit" ItemClick="onMenuItemClicked"></ClientSideEvents>
                        <Items>
                            <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="iconbuilder_actions_add_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="save_save_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                             <dx:MenuItem Name="mnuDelete" Text="Delete" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="diagramicons_del_svg_16x16"></Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
            </div>
        </div>
        
        <div style="padding-top:15px"></div>
          <div class="col col-md-12">
        

                   
                    <div class="row">

                        <div id="pnlGrid">

                            <uc1:ChartOfAccountControl runat="server" ID="ChartOfAccountControl" />

                        </div>


                        <div class="col col-md-12">
                            <div id="pnlData" style="display:none">
                                <dx:ASPxCallbackPanel runat="server" ID="clbClient" ClientInstanceName="clbClient" OnCallback="clbClient_Callback">
                                    <ClientSideEvents EndCallback="clbClient_EndCallback" />
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxHiddenField ID="hdfStatus" ClientInstanceName="hdfStatus" runat="server"></dx:ASPxHiddenField>
                                            <table>
                                                <tr>
                                                    <td style="width: 25%">
                                                        <dx:ASPxLabel runat="server" Text="አባት ኮድ"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                    <td style="width: 25%">
                                                        <dx:ASPxLabel runat="server" Text="እርከን"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                    <td style="width: 25%">
                                                        <dx:ASPxLabel runat="server" Text="ኮድ"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox runat="server" ID="txtFatherCode" Width="100%">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የአካውንቱ ኮድ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox runat="server" ID="cboHiracchy" ClientInstanceName="cboHiracchy" Width="100%">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="አባት ኮድ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxTextBox runat="server" ID="txtCode" Width="100%">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="የአካውንቱ ኮድ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        <%--<dx:ASPxButton runat="server" ID="btnSearch" ClientInstanceName="btnSearch" CausesValidation="false" AutoPostBack="false" Text="">
                                            <Image IconID="iconbuilder_actions_zoom_svg_white_16x16">
                                            </Image>
                                        </dx:ASPxButton>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <dx:ASPxLabel runat="server" Text="የአካውንቱ አማርኛ ስም"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <dx:ASPxTextBox runat="server" ID="txtName" Width="100%">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="ስም በአማርኛ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4">
                                                        <dx:ASPxLabel runat="server" Text="የአካውንቱ ስም እንግሊዝኛ ስም"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <dx:ASPxTextBox runat="server" ID="txtNameEng" Width="100%">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="ስም በእንግሊዝኛ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <dx:ASPxCheckBox runat="server" ID="chbIsMainAccount" Text="ማጠቃለያ ነው?" TextAlign="Left" ClientInstanceName="chbIsMainAccount" style="margin:0;padding:0">
                                                            <ClientSideEvents CheckedChanged="function(s){IsMainChecked(s.GetChecked()); }" />
                                                        </dx:ASPxCheckBox>
                                                    </td>
                                                    <td colspan="2" style="text-align: right">
                                                        <dx:ASPxCheckBox runat="server" ID="chbIsBudgetAccount" Text="በጀት አካውንት ነው?" TextAlign="Left" ClientInstanceName="chbIsBudgetAccount"></dx:ASPxCheckBox>

                                                    </td>
                                                </tr>

                                                <tr id="mainText">
                                                    <td colspan="4">
                                                        <dx:ASPxLabel runat="server" Text="ዋና መደብ"></dx:ASPxLabel>
                                                        <span class="star">*</span>
                                                    </td>
                                                </tr>
                                                <tr id="mainControl">
                                                    <td colspan="4">
                                                        <dx:ASPxComboBox runat="server" ID="cboParentCode" ClientInstanceName="cboParentCode" Width="100%" ValueType="System.Int32">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorText="ዋና መደብ ያስፈልጋል" ErrorDisplayMode="None" SetFocusOnError="True" ValidationGroup="VAccount" Display="Dynamic">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <dx:ASPxCheckBox runat="server" Text="አሁን በሥራ ላይ ነው" Checked="true" ID="chbIsActive"></dx:ASPxCheckBox>
                                                    </td>
                                                </tr>
                                            </table>



                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>
                            </div>
                        </div>

                    </div>
           
              </div>
    </div>
          <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
         </div>
         </div>
</asp:Content>
