﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="CategoryRate.aspx.cs" Inherits="RMS.Pages.CategoryRate" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Src="~/Controls/Category.ascx" TagPrefix="uc1" TagName="Category" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>



<asp:Content runat="server" ContentPlaceHolderID="PageContent">
    <style type="text/css">
        .cRate {
            width: 70px !important;
        }

        .menu {
            text-align: center;
        }

        .dxtlFocusedNode_Metropolis, .dxpLite_Metropolis .dxp-current {
            background-color: #009688;
        }

        #PageContent_cblMainClient_pnlData_dpFrom_txt, #PageContent_cblMainClient_pnlData_dpTo_txt {
            border: 1px solid rgba(0,0,0,0.22);
            height: 32px;
        }

        #PageContent_cblMainClient_pnlData_dpFrom, #PageContent_cblMainClient_pnlData_dpTo {
            width: 100%;
        }
    </style>
    <style>
        ASPxTextBox {
            width: 100% !important;
        }
    </style>
    <script type="text/javascript">
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            //ToggleEnabled("mnuDelete", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                var text = lblDescription.GetText();
                DoNewRate();
                lblDescription.SetText(text);
            }
            else if (e.item.name === "mnuSave") {
                DoSaveRate();
            }
            else if (e.item.name === "mnuList") {
                DoDetailRate();
            }
            else if (e.item.name === "mnuDelete") {
            }
        }
        function DoNodeClick(s, e) {
            hdfValue.Set('RateId', e.nodeKey);
            cblMainClient.PerformCallback('Get|' + '0|' + e.nodeKey);
            btnEdit.SetEnabled(true);
            // s.GetNodeValues(e.nodeKey, "Id;Description;Parent", ReturnSelectedPickCategoryRow);
        }
        function DoDeleteCategory() {
            ShowConfirmx("Are you sure do you want to delete this record?", function (result) {
                if (result)
                    cblCategory.PerformCallback('Delete');
            }, 'en');
        }
        function DoSearchCodeClick(s, e) {

            clbPickCategory.PerformCallback('SearchCode');
        }
        //Disable Parent NodeClick
        function OnClientNodeClicked1(sender, args) {
            if (args.get_node()._hasChildren() == true)
                args.get_node().set_selected(false)
        }

        function ReturnSelectedPickCategoryRow(values) {
            grdView.SetVisible(false);

            parent = values[2];

            cid = values[0];
            if (cid == null || cid == typeof 'undefined')
                return;
            btnDelete.SetEnabled(true);
            btnEdit.SetEnabled(true);
            hdfValue.Set('CategoryId', cid)
            ////if (parent == -1) {//Parent node
            //    lblDescription.SetText("Select Category");
            //    pnlData.SetVisible(false);
            //    hdfValue.Set('CategoryId', null);
            //    return;
            //}



            lblDescription.SetText(values[1]);
            btnEdit.SetEnabled(true);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
            ToggleEnabled("mnuSave", false);
            pnlData.SetVisible(false);
            //grdView.PerformCallback('Bind|' + cid);
        }


        function DoNew() {
            cboParent.PerformCallback();
            popCategory.Show();
        }
        function DisableOthers(s) {
            if (s.GetText() === '' || s.GetText() == '0.00') {
                for (i = 1; i <= 8; i++) {
                    id = 'txtCat' + i;
                    window[id].SetEnabled(true);
                }
            }
            else {
                for (i = 1; i <= 8; i++) {
                    id = 'txtCat' + i;
                    if (s != window[id])
                        window[id].SetEnabled(false);
                }
            }
        }

        function DoFindCategory() {
            cblCategory.PerformCallback('Get|' + hdfValue.Get('CategoryId'));
            popCategory.Show();
            btnSave.SetEnabled(true);
        }
        function DoEditMode() {
            pnlData.SetVisible(true);
            btnSave.SetEnabled(true);
            ToggleEnabled("mnuList", true);

        }
        function DoDetail() {
            pnlData.SetVisible(false);
            btnSave.SetEnabled(false);
            ToggleEnabled("mnuList", false);
        }


        function cblMainClient_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                OtherServiceChecked();
                if (s.cpAction === "Delete") {
                    DoDetail();
                    ShowSuccess(s.cpMessage);
                    return;
                }
                else if (s.cpAction === "Save") {
                    ShowSuccess(s.cpMessage);
                    DoDetailRate();

                }
                else if (s.cpAction === "Get") {
                    DoEditModeRate();
                }
                else if (s.cpAction === "New") {
                    pnlData.SetVisible(true);

                }

            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpAction === "Save") {
                    pnlData.SetVisible(true);
                }
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);

            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearJsProperty(s) {
            s.cpStatus = s.cpMessage = s.cpAction = '';
        }

        function grdView_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING' && e.buttonID != 'ADDING') return;
            if (e.buttonID == 'DELETING') {
                s.GetRowValues(e.visibleIndex, "Id", onDeleteRate);
            }
            else if (e.buttonID == 'EDITING') {
                s.GetRowValues(e.visibleIndex, "Id;CategoryId", onEditRate);

            }

        }
        function onEditRate(values) {
            cblMainClient.PerformCallback('Get|' + values[0] + '|' + values[1]);
        }

        function onDeleteRate(values) {
            ShowConfirmx("Are you sure do you want to delete this record?", function (result) {
                if (result)
                    DoDeleteRate(values);
            }, 'en');
        }
        function DoDeleteRate(values) {
            grdView.PerformCallback('Delete|' + values);
        }

        function DoNewRate() {
            if (hdfValue.Get('CategoryId') == null || typeof hdfValue.Get('CategoryId') == "undefined") {
                ShowError('Please select category first');
                return;
            }
            hdfValue.Set('IsNewRate', true);
            ClearFormRate();
            DoEditModeRate();
        }
        function DoSaveRate() {
            if (hdfValue.Get('CategoryId') === 'undefined' || typeof hdfValue.Get('CategoryId') === 'undefined') {
                ShowError('Please select category first');
                return;
            }
            else if (txtBudgetYear.GetText() === '') {
                ShowError('Budget Year is required');
                txtBudgetYear.Focus();
                return;
            }
            cblMainClient.PerformCallback('Save');
        }
        function DoEditModeRate() {
            grdView.SetVisible(false);
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuList", true);
            pnlData.SetVisible(true);
        }
        function DoDetailRate() {
            if (hdfValue.Get('CategoryId') === 'undefined' || typeof hdfValue.Get('CategoryId') === 'undefined') {
                ShowError('እባክዎ ዘርፍ አልመረጡም');
                return;
            }
            grdView.PerformCallback();
        }
        function grdView_EndCallback(s, e) {
            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Bind") {
                    if (s.cpMessage === "NA") {
                        ShowMessage('No data is available');
                        grdView.SetVisible(false);
                    }
                    else {
                        pnlData.SetVisible(false);
                        grdView.SetVisible(true);
                    }
                }
                else if (s.cpAction === "Delete") {
                    DoDetailRate();
                    ShowSuccess(s.cpMessage);
                    grdView.PerformCallback('Bind');
                    return;
                }
            }
            else if (s.cpStatus === "ERROR") {
                if (s.cpMessage == '') { return; }
                ShowError(s.cpMessage);
            }
            ClearJsProperty(s);
        }
        function ClearFormRate() {
            cblMainClient.PerformCallback('New');
        }
        function SearchCategory() {
            cblTree.PerformCallback('Search|' + txtSearch.GetText());
            // trlCategorys.PerformCallback();
        }
        function OtherServiceChecked() {

            if (chbOtherServiceList.GetChecked()) {
                $('#divVehicle').slideDown(200);
            }
            else {
                $('#divVehicle').slideUp(200);
            }
            cblTree.PerformCallback('Other Service')
        }
    </script>
    <div class="contentmain">
        <h5>Category Rate
        </h5>
        <div class="card card-body">
            <script src="../../Content/ModalDialog.js"></script>

            <div class="row">
                <div class="col col-md-4">
                    <div class="card card-flat">
                        <div>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <dx:ASPxButton runat="server" ID="btnNew" ClientInstanceName="btnNew" AutoPostBack="false" ToolTip="Add" Width="50px"
                                                CausesValidation="false" Theme="Material">
                                                <ClientSideEvents Click="DoNew" />
                                                <Image IconID="iconbuilder_actions_addcircled_svg_white_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                            <dx:ASPxButton runat="server" ID="btnEdit" ClientInstanceName="btnEdit" AutoPostBack="false" ToolTip="Edit" Width="50px" ClientEnabled="false"
                                                CausesValidation="false" Theme="Material">
                                                <ClientSideEvents Click="DoFindCategory" />
                                                <Image IconID="iconbuilder_actions_edit_svg_white_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                            <dx:ASPxCheckBox runat="server" ID="chbotherServiceList" ClientInstanceName="chbOtherServiceList" AutoPostBack="false" Text="Other Categories?" Style="display: inline-block !important">
                                                <ClientSideEvents CheckedChanged="OtherServiceChecked" />
                                            </dx:ASPxCheckBox>
                                            <dx:ASPxButton runat="server" ID="btnDelete" ClientInstanceName="btnDelete" AutoPostBack="false" ToolTip="Delete" Width="50px" ClientEnabled="false" ClientVisible="false"
                                                CausesValidation="false" Theme="Material">
                                                <ClientSideEvents Click="DoDeleteCategory" />
                                                <Image IconID="diagramicons_del_svg_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <dx:ASPxTextBox runat="server" ID="txtSearch" ClientInstanceName="txtSearch" NullText="Search by name or code" Style="float: left; width: 100%"></dx:ASPxTextBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton runat="server" ID="btnSearch" ClientInstanceName="btnSearch" CausesValidation="false" AutoPostBack="false" ToolTip="ፈልግ" Width="50px"
                                                Theme="Material">
                                                <ClientSideEvents Click="SearchCategory" />
                                                <Image IconID="iconbuilder_actions_zoom_svg_white_16x16">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div style="padding-top: 12px; margin-bottom: 10px; width: 100%; overflow-x: auto">
                            <dx:ASPxCallbackPanel runat="server" ID="cblTree" ClientInstanceName="cblTree" OnCallback="cblTree_Callback">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxTreeList ID="trlCategorys" runat="server" AutoGenerateColumns="False"
                                            ClientInstanceName="trlCategorys" Theme="Metropolis" KeyFieldName="Id" ParentFieldName="Parent" OnInit="trlCategorys_Init">
                                            <ClientSideEvents NodeClick="function(s,e){DoNodeClick(s,e);}" />
                                            <Columns>
                                                <dx:TreeListTextColumn Caption="Category Name" FieldName="DescriptionShort" ShowInCustomizationForm="True" VisibleIndex="0">
                                                </dx:TreeListTextColumn>
                                                <dx:TreeListTextColumn FieldName="Id" Visible="false">
                                                </dx:TreeListTextColumn>
                                                <dx:TreeListTextColumn FieldName="Parent" Visible="false">
                                                </dx:TreeListTextColumn>

                                                <dx:TreeListTextColumn Caption="የንግድ ዘርፍ" FieldName="Description" Visible="false">
                                                </dx:TreeListTextColumn>
                                                <dx:TreeListTextColumn FieldName="OtherService" Visible="false">
                                                </dx:TreeListTextColumn>
                                            </Columns>
                                            <%--<Settings GridLines="None" ShowFooter="true" VerticalScrollBarMode="Auto"  />--%>
                                            <SettingsBehavior AllowFocusedNode="True" />
                                            <SettingsPager Mode="ShowPager" PageSize="15" ShowNumericButtons="false" FirstPageButton-ShowDefaultText="true" LastPageButton-ShowDefaultText="true">
                                                <PageSizeItemSettings Items="20, 30, 50" Visible="False">
                                                </PageSizeItemSettings>
                                            </SettingsPager>
                                            <%--<SettingsLoadingPanel ImagePosition="Top" />--%>
                                        </dx:ASPxTreeList>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </div>
                    </div>
                </div>
                <div class="col col-md-8">
                    <div style="padding-left: 15px !important">
                        <div style="padding-bottom: 20px">
                            <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                                ShowPopOutImages="True" EnableTheming="True" Height="35px"
                                ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                                AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                                <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                                <Items>
                                    <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                        <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                        <Image IconID="iconbuilder_actions_add_svg_16x16">
                                        </Image>
                                    </dx:MenuItem>
                                    <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                        <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                        <Image IconID="save_save_svg_16x16">
                                        </Image>
                                    </dx:MenuItem>
                                    <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                        <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                        <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                        </Image>
                                    </dx:MenuItem>
                                </Items>
                            </dx:ASPxMenu>
                        </div>

                        <dx:ASPxCallbackPanel runat="server" ID="cblMainClient" ClientInstanceName="cblMainClient" OnCallback="cblMainClient_Callback">
                            <ClientSideEvents EndCallback="cblMainClient_EndCallback" />
                            <PanelCollection>
                                <dx:PanelContent>
                                    <div style="padding-bottom: 15px; text-align: left;">
                                        <dx:ASPxLabel runat="server" ID="lblDescription" ClientInstanceName="lblDescription" Font-Size="Larger" CssClass="text-muted" Text="Select Category"></dx:ASPxLabel>
                                    </div>
                                    <dx:ASPxHiddenField runat="server" ID="hdfValue" ClientInstanceName="hdfValue"></dx:ASPxHiddenField>
                                    <div class="row" style="width: 100%">
                                        <dx:ASPxRoundPanel runat="server" Width="100%" ID="pnlData" ShowHeader="false" ClientInstanceName="pnlData" ClientVisible="false">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel4" Text="Annual Sale"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel17" Text="Rate%"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="Profit"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Crate">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel18" Text="Rate%"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel2" Text="Net Profit"></dx:ASPxLabel>

                                                                </td>
                                                                <td class="Crate">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel19" Text="Rate%"></dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtAnnualSales" ClientInstanceName="txtAnnualSales" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtSalesRate" ClientInstanceName="txtSalesRate" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtGrossProfit" ClientInstanceName="txtGrossProfit" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtGrossProfitRate" ClientInstanceName="txtGrossProfitRate" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtNetProfit" ClientInstanceName="txtNetProfit" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxTextBox runat="server" ID="txtNetProfitRate" ClientInstanceName="txtNetProfit" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel3" Text="EXCISE Code"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel5" Text="EXC. Rate %"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel6" Text="TOT Code"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel20" Text="TOT Rate %"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel21" Text="Working Hrs."></dx:ASPxLabel>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtTOTCode" ClientInstanceName="txtTOTCode" Width="100%">
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxTextBox runat="server" ID="txtTOTRate" ClientInstanceName="txtTOTRate" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>

                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtExciseCode" ClientInstanceName="txtExciseCode" Width="100%">
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td class="cRate">
                                                                    <dx:ASPxTextBox runat="server" ID="txtExciseRate" ClientInstanceName="txtExciseRate" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td class="Csep">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxSpinEdit runat="server" ID="txtWorkingHour" Width="100%"></dx:ASPxSpinEdit>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>


                                                    <div id="divVehicle">
                                                        <table style="max-width: 550px">
                                                            <tr>
                                                                <td colspan="4"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="Csep">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel13" Text="Driver Salary"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel14" Text="Assistant Salary"></dx:ASPxLabel>

                                                                </td>
                                                                <td class="Csep">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel8" Text="Capacity From"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel9" Text="Capacity To"></dx:ASPxLabel>
                                                                </td>
                                                                <td class="Csep"></td>
                                                                <td class="Csep">
                                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel10" Text="Measuring Unit" Visible="false"></dx:ASPxLabel>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtDriverSalary" ClientInstanceName="txtDriverSalary" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox runat="server" ID="txtAssistantSalary" ClientInstanceName="txtAssistantSalary" Width="100%">
                                                                        <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                                                                        <ValidationSettings Display="Dynamic"></ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxSpinEdit runat="server" ID="txtMinCapacity" ClientInstanceName="txtMinCapacity" MaxValue="1000000" MinValue="1" Width="100%" Height="30px">
                                                                    </dx:ASPxSpinEdit>

                                                                </td>
                                                                <td>
                                                                    <dx:ASPxSpinEdit runat="server" ID="txtMaxCapacity" ClientInstanceName="txtMaxCapacity" MaxValue="1000000" MinValue="1" Width="100%" Height="30px"></dx:ASPxSpinEdit>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxCheckBox runat="server" ID="chbIsAbove15" Text="Above 15 Years"></dx:ASPxCheckBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox runat="server" Width="100%" ID="cboUnit" Visible="false">
                                                                        <Items>
                                                                            <dx:ListEditItem Text="Person" Value="1" />
                                                                            <dx:ListEditItem Text="Killogram" Value="2" />
                                                                            <dx:ListEditItem Text="Liter" Value="3" />
                                                                        </Items>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="divYear">
                                                        <table style="max-width: 350px">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="Csep">
                                                                        <dx:ASPxLabel runat="server" ID="ASPxLabel7" Text="Budget Year"></dx:ASPxLabel>
                                                                        <span class="star">*</span>
                                                                    </td>
                                                                    <td class="Csep">
                                                                        <dx:ASPxLabel runat="server" ID="ASPxLabel15" Text="Applied Date From"></dx:ASPxLabel>
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <dx:ASPxLabel runat="server" ID="ASPxLabel12" Text="Applied Date To"></dx:ASPxLabel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxSpinEdit runat="server" ID="txtBudgetYear" ClientInstanceName="txtBudgetYear" MinValue="2010" MaxValue="2099" Style="max-width: 120px">
                                                                            <ValidationSettings Display="Dynamic" ErrorText="Budget year is required" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" RequiredField-IsRequired="true"></ValidationSettings>
                                                                        </dx:ASPxSpinEdit>

                                                                    </td>
                                                                    <td style="vertical-align: top">
                                                                        <cdp:CUSTORDatePicker ID="dpFrom" runat="server" SelectedDate="" TabIndex="1"
                                                                            TextCssClass="" Style="max-width: 150px" Width="100%" meta:resourcekey="dpFromResource1" Height="30px" />
                                                                    </td>
                                                                    <td class="Csep" style="vertical-align: top" colspan="2">
                                                                        <cdp:CUSTORDatePicker ID="dpTo" runat="server" SelectedDate="" TabIndex="1"
                                                                            TextCssClass="" Style="max-width: 150px" Width="100%" Height="30px" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                        <dx:ASPxGridView ID="grdView" runat="server" AutoGenerateColumns="False" ClientInstanceName="grdView" Style="margin: 0 auto" OnInit="grdView_Init"
                            EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id" ClientVisible="False"
                            OnCustomCallback="grdView_CustomCallback" OnPageIndexChanged="grdView_PageIndexChanged"
                            Width="100%" meta:resourcekey="grdViewResource1">
                            <ClientSideEvents CustomButtonClick="grdView_CustomButtonClick" EndCallback="function(s,e){grdView_EndCallback(s,e);}" />
                            <SettingsBehavior AllowFocusedRow="true" ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                            <SettingsPopup>
                                <HeaderFilter MinHeight="140px"></HeaderFilter>
                            </SettingsPopup>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Budget Year" FieldName="BudgetYear" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource3">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Applied Date From" FieldName="AppliedDateFromText" ShowInCustomizationForm="True" VisibleIndex="1">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Applied Date To" FieldName="AppliedDateToText" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource3">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataCheckColumn FieldName="CategoryId" Visible="false" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource3">
                                </dx:GridViewDataCheckColumn>
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewCommandColumnResource1">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Edit" meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="SPACER" Text=" " meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                        </dx:GridViewCommandColumnCustomButton>
                                        <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            <Styles Style-ForeColor="Red">
                                                <Style ForeColor="Red"></Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>

                                </dx:GridViewCommandColumn>
                            </Columns>
                            <Styles AdaptiveDetailButtonWidth="22">
                                <SelectedRow BackColor="Orange" ForeColor="White">
                                </SelectedRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </div>
                </div>
            </div>
        </div>
        <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />
    </div>
    <dx:ASPxPopupControl runat="server" ID="popCategory" ClientInstanceName="popCategory" Width="500px" HeaderText="Edit Category"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseAction="CloseButton" EnableCallbackAnimation="true">
        <HeaderStyle BackColor="#009688" ForeColor="White" />
        <ContentCollection>
            <dx:PopupControlContentControl>
                <uc1:Category runat="server" ID="Category" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>

