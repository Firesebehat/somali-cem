﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminSettings.aspx.cs" MasterPageFile="~/Root.master" Inherits="RMS.Pages.Settings.AdminSettings" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
          <script>

              function OnClientEndCallback(s, e) {
                  if (s.cpStatus === 'SUCCESS') {
                      if (s.cpAction === 'Save') {
                          ShowSuccess(s.cpMessage);
                          btn1.SetEnabled(false);
                      }
                  }
                  else if(s.cpStatus === 'ERROR')
                  {
                      if (s.cpMessage !== '') {
                          ShowError(s.cpMessage);
                      }
                  }
                  s.cpStatus = s.cpAction = '';
              }
          </script>   

    <dx:ASPxCallbackPanel ID="clbClient" OnCallback="clbClient_Callback" ClientInstanceName="clbClient" runat="server" Width="100%" >

                    <ClientSideEvents EndCallback="function(s,e){OnClientEndCallback(s,e);}" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlCategory" runat="server"  SupportsDisabledAttribute="True">
                           <h5>Site Level Admin Settings</h5>
                            <div class="card card-body">
                                <table style="margin:0 auto;max-width:90%" class="table table-hover table-primary">
                                    <tr>
                                        <td style="width:60%">
                                           <h6> Allow users to change assessment date</h6>
                                        </td>
                                        <td style="width:20%">
                                            <dx:ASPxComboBox runat="server" ID="cboEnableAsstDate" ClientInstanceName="cboEnableAsstDate"  Theme="iOS">
                                                <ClientSideEvents ValueChanged="function(){btn1.SetEnabled(true);}" />
                                                <Items>
                                                    <dx:ListEditItem Value="1" Text="Yes" />
                                                    <dx:ListEditItem Value="0" Text="No" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ClientInstanceName="btn1"  runat="server" Text="Save" AutoPostBack="false" CausesValidation="false" ClientEnabled="false" Theme="iOS">
                                                <ClientSideEvents Click="function(){clbClient.PerformCallback('Save|EnableChangingAssessmentDate|' + cboEnableAsstDate.GetValue());} " />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                             <dx:ASPxButton   runat="server" Text="History" AutoPostBack="false" CausesValidation="false" Theme="iOS">
                                                <ClientSideEvents Click="function(){popup.Show();gvHstory.PerformCallback('EnableChangingAssessmentDate');}" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                                 <uc1:Alert ID="alert" runat="server" />

                             </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
    <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" AllowDragging="true" Width="700px" EnableCallbackAnimation="true" PopupAnimationType="Fade"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" HeaderText="Change History" PopupHorizontalOffset="200">
        <LoadingPanelStyle Paddings-Padding="0"></LoadingPanelStyle>
        <HeaderStyle BackColor="#009688" ForeColor="White" />
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxGridView ID="gvHstory" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvHstory" EnableTheming="True"
                    KeyFieldName="Id" Visible="true" Width="100%" OnPageIndexChanged="gvHstory_PageIndexChanged"  OnCustomCallback="gvHstory_CustomCallback">
                    <%--<ClientSideEvents CustomButtonClick="function(s,e){OnRemarkClick(s,e);}" />--%>
                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                    <SettingsPopup>
                        <HeaderFilter MinHeight="140px"></HeaderFilter>
                    </SettingsPopup>
                    <SettingsBehavior AllowSort="false" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="From" FieldName="OldValue" ShowInCustomizationForm="True" VisibleIndex="1" Width="30%">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="To" FieldName="NewValue" ShowInCustomizationForm="True" VisibleIndex="2" Width="30%">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Changed By" FieldName="CreatedByName" ShowInCustomizationForm="True" VisibleIndex="3" Width="15%">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="CreatedDate" ShowInCustomizationForm="True" VisibleIndex="4" Width="25%">
                        </dx:GridViewDataTextColumn>

                        <%--<dx:GridViewDataTextColumn FieldName="Remark" VisibleIndex="6" Visible="false">
                        </dx:GridViewDataTextColumn>--%>
                    </Columns>
                    <SettingsPager PageSize="7"></SettingsPager>
                </dx:ASPxGridView>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <SettingsAdaptivity Mode="OnWindowInnerWidth" SwitchAtWindowInnerWidth="800" />
    </dx:ASPxPopupControl>
    </asp:Content>
                        
                                     