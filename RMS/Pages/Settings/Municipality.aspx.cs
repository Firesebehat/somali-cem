﻿using CUSTOR.Business;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class Municipality : System.Web.UI.Page
    {
        public int Lan
        {
            get
            {
                return 1;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                hdfValue["IsNewMunicipality"] = true;
                hdfValue["CategoryId"] = 0;
                Bind();
            }
            BindTree();
        }

        private void BindTree()
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            List<CUSTOR.Domain.CategoryDomain> categories;
            if (txtSearch.Text != string.Empty)
            {
                DoSearchCategory();
                return;
            }
            try
            {
                categories = categoryBusiness.GetCategorys();

                trlCategorys.DataSource = categories;
                trlCategorys.DataBind();
            }
            catch
            {

            }
        }

        private void DoSearchCategory()
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            List<CUSTOR.Domain.CategoryDomain> categories;

            try
            {
                CVGeez obj = new CVGeez();
                string text = obj.GetSortValueU(txtSearch.Text);
                text = text == string.Empty ? txtSearch.Text : text.Replace("w1", ".");
                categories = categoryBusiness.SearchCategory(text,false);
                if (categories.Count > 0)
                {
                    trlCategorys.DataSource = categories;
                    trlCategorys.DataBind();
                    
                }
                else
                {
                    trlCategorys.DataSource = null;
                    trlCategorys.DataBind();
                   

                }
            }
            catch
            {

            }
        }

        protected void ASPxGridView1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] param = e.Parameters.Split('|');
            string command = param[0];
            switch (command)
            {
                case "Delete":
                    int id = Convert.ToInt32(param[1]);
                    Delete(id);
                    break;
                default:
                    Bind();
                    break;
            }
        }

        private void Delete(int id)
        {
            ASPxGridView1.JSProperties["cpAction"] = "Delete";
            try
            {
                
                DoDelete(id);
                ShowSuccess("Record is successfully deleted");
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoDelete(int id)
        {
            ASPxGridView1.JSProperties["cpAction"] = "Delete";
            MunicipalityBusiness business = new MunicipalityBusiness();
                business.Delete(id);
           
        }

        protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
        {
            Bind();
        }


      

        private void Bind()
        {
            ASPxGridView1.JSProperties["cpAction"] = "Bind";
            MunicipalityBusiness business = new MunicipalityBusiness();
            try
            {
                int CategoryId = Convert.ToInt32(hdfValue["CategoryId"]);
                List<CUSTOR.Domain.Municipality> list = business.GetMunicipalitys(CategoryId);
                ASPxGridView1.DataSource = list;
                ASPxGridView1.DataBind();
                hdfValue["IsNewMunicipality"] = true;
            }
            catch (Exception ex)
            {
                ASPxGridView1.JSProperties["cpStatus"] = "ERROR";
                ASPxGridView1.JSProperties["cpMessage"] = ex.Message;
            }
        }
    
        private void ShowError(string strMsg)
        {
            ASPxGridView1.JSProperties["cpMessage"] = strMsg;
            ASPxGridView1.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            ASPxGridView1.JSProperties["cpMessage"] = strMsg;
            ASPxGridView1.JSProperties["cpStatus"] = "SUCCESS";
        }

    

        protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            ASPxGridView1.JSProperties["cpAction"] = "Save";
            #region validate
            string errText = string.Empty;
            var rank = e.NewValues["Rank"];
            var annualFee = e.NewValues["AnnualFee"];
            if (rank == null || Convert.ToInt16(rank) < 1)
                errText += "Please enter Level.";
            else if (annualFee != null && Convert.ToDouble(annualFee) == 0.00d)
                errText += "Please enter Anual Fee.";
            if (errText != string.Empty)
            {
                e.Cancel = true;
                ShowError(errText);
                return;
            }
            #endregion
            MunicipalityBusiness business = new MunicipalityBusiness();
            CUSTOR.Domain.Municipality objMunicipality = new CUSTOR.Domain.Municipality();
            DateTime eventDate = DateTime.Now;
            try
            {
                objMunicipality.CategoryId = Convert.ToInt32(hdfValue["CategoryId"]);
                objMunicipality.Rank = Convert.ToInt16(e.NewValues["Rank"]);
                objMunicipality.AnnualFee = Convert.ToDecimal(e.NewValues["AnnualFee"]);

               
                    objMunicipality.CreatedBy = User.Identity.Name;
                    objMunicipality.CreatedDate = eventDate;
                    objMunicipality.CreatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    if (business.Exists(objMunicipality.CategoryId, objMunicipality.Rank))
                    {
                        e.Cancel = true;
                        ShowError("Level should be distict. Record exist.");
                        return;
                    }
                    business.InsertMunicipality(objMunicipality);
                
                Bind();
                ShowSuccess("The record is successfully saved");
                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                ASPxGridView1.JSProperties["cpAction"] = "Save";

            }
            catch (Exception ex)
            {
                e.Cancel = true;
                ShowError(ex.Message);

            }
        }

        protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.Cancel = true;
            ASPxGridView1.JSProperties["cpAction"] = "Save";
            #region validate
            string errText = string.Empty;
            var rank = e.NewValues["Rank"];
            var annualFee = e.NewValues["AnnualFee"];
            if (rank == null || Convert.ToInt16(rank) < 1)
                errText += "Please enter Level.";
            else if (annualFee != null && Convert.ToDouble(annualFee) == 0.00d)
                errText += "Please enter Anual Fee.";
            if(errText != string.Empty)
            {
                ShowError(errText);
                return;
            }
            #endregion

            MunicipalityBusiness business = new MunicipalityBusiness();
            CUSTOR.Domain.Municipality objMunicipality = new CUSTOR.Domain.Municipality();
            DateTime eventDate = DateTime.Now;
            try
            {
                objMunicipality.CategoryId = Convert.ToInt32(hdfValue["CategoryId"]);
                objMunicipality.Rank = Convert.ToInt16(rank);
                objMunicipality.AnnualFee = Convert.ToDecimal(annualFee);

                
                    objMunicipality.Id = Convert.ToInt32(e.Keys[0]);
                if(business.Exists(objMunicipality.Id, objMunicipality.CategoryId, objMunicipality.Rank))
                {
                    ShowError("Level must be distinct. Record Exist.");
                    return;
                }
                    objMunicipality.UpdatedBy = User.Identity.Name;
                    objMunicipality.UpdatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    objMunicipality.UpdatedDate = eventDate;
                    business.UpdateMunicipality(objMunicipality);
                    Bind();
                ShowSuccess("Record is successfully saved");
                ASPxGridView1.CancelEdit();
                ASPxGridView1.JSProperties["cpAction"] = "Save";

            }
            catch (Exception ex)
            {
                ShowError(ex.Message);

            }
        }

        
    }
}