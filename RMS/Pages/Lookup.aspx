﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Lookup.aspx.cs" Inherits="RMS.Pages.Lookup" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
     <style>
        td{
            vertical-align:top;
        }
        .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
    </style>
      <script type="text/javascript">
            history.pushState(null, null, document.URL);
            window.addEventListener('popstate', function () {
                this.history.pushState(null, null, document.URL);
            });
        </script>
    <script type="text/javascript">

        function DoNewLookupClick(s, e) {
            pnlData.SetVisible(true);
            btnFind.SetEnabled(true);
           
            gvwLookups.SetVisible(false);
            clbLookups.PerformCallback('New');
            btnNewLookups.SetEnabled(false);
        }
        function DoSaveLookupClick(s, e) {
            var validated = ASPxClientEdit.ValidateGroup('LookupRegistration');
            if (!validated) {
                return;
            }

      
            clbLookups.PerformCallback('Save');
        }

        

        
        function DoCancelClient() {
            DoListView();
        }
        function DoListView() {

            pnlData.SetVisible(false);
            btnNewLookups.SetEnabled(true);
            btnNewLookups.SetEnabled(true);
            btnFind.SetEnabled(true);
        }
        function DoShowClick() {
            
            if (cboLookupTypes.GetValue() == null) {
                ShowError("First select lookup type");
                return;
            }
            
            clbLookups.PerformCallback('Show');
            pnlData.SetVisible(false);
            btnNewLookups.SetEnabled(true);
        }
        var rowVisibleIndex;
        var strID;
        function gvwLookups_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID == 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Id', ShowPopup);

            }
            if (e.buttonID == 'EDITING') {
                gvwLookups.GetRowValues(e.visibleIndex, "Id", OnGetSelectedFieldValues);
                
                //DoToggle();
            }
        }

        function OnGetSelectedFieldValues(values) {

            clbLookups.PerformCallback('EDITING' + '|' + values);
            gvwLookups.SetVisible(false);
            pnlData.SetVisible(true);
        }

        function ShowPopup(values) {

            ShowConfirmx('Are you sure? Do you want to delete this record?', function (result) {
                if (result) {
                    Delete(values);
                }
            }, 'en');
        }

        function OnLookupTypesEndCallback(s, e) {
            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Cancel") {
                    pnlData.SetVisible(false);
                    return;
                }
               
                else if (s.cpAction == "Save") {
                    //pnldata.SetVisible(false);
                    ShowSuccess(s.cpMessage);
                    btnNewLookups.SetEnabled(true);
                    btnFind.SetEnabled(false);
                }
                else if (s.cpAction == "delete") {
                    ShowSuccess(s.cpMessage);
                    btnFind.SetEnabled(false);
                    btnNewLookups.SetEnabled(true);
                    btnFind.SetEnabled(false);
                }
                else if (s.cpAction == "Loaded") {
                    btnFind.SetEnabled(true);
                    btnNewLookups.SetEnabled(true);
                }
                else if (s.cpAction == "Show") {
                    btnFind.SetEnabled(false);
                    btnNewLookups.SetEnabled(true);
                }
            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);
                pnlData.SetVisible(true);
               
                //DoToggle();
            }
            else if (s.cpStatus == "INFO") {
                ShowMessage(s.cpMessage);
                btnNewLookups.SetEnabled(true);
            }
        }

        function btnYes_Click(s, e) {

            ClosePopup(true);
        }

        function btnNo_Click(s, e) {
            ClosePopup(false);
        }

        function Delete(values) {
            
            clbLookups.PerformCallback('DELETING' + '|' + values);
        }

        
        function DeleteGridRow(visibleIndex) {
            var index = gvwLookups.GetFocusedRowIndex();
            gvwLookups.DeleteRow(index);
        }

    </script>
   
     
        <div  class="contentmain">
            <h5>
                Lookup Maintenance
            </h5>
            <div class="card card-body">
                 <script src="../Content/ModalDialog.js"></script>
              <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
      
        <div class="panel panel-body">
             <dx:ASPxCallbackPanel ID="clbLookupsID" OnCallback="clbLookups_Callback" ClientInstanceName="clbLookups" runat="server" Height="100%" Width="700px">
                    <ClientSideEvents EndCallback="OnLookupTypesEndCallback" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlContent1" runat="server" SupportsDisabledAttribute="True">
                             <div style="padding-top:10px;padding-bottom:10px">
                              <table>
                                            
                                            <tr>
                                                
                                                 <td style="text-align: left;">
                                                    <asp:Label ID="lblLookupTypes" runat="server" Text="Lookup Type: "   style="margin-left:8px"></asp:Label>
                                                </td>
                                                <td style="text-align: left;">
                                                    <dx:ASPxComboBox ID="cboLookupTypes" runat="server" ClientInstanceName="cboLookupTypes" TextField="Description" NullText="Select Lookup Type"
                                                        ValueField="Id" Width="250px" OnCallback="cboLookupTypes_callback">
                                                        <ClientSideEvents SelectedIndexChanged="function(s){btnNewLookups.SetEnabled(s.GetValue()!=null);btnFind.SetEnabled(s.GetValue()!=null);}"></ClientSideEvents>
                                                   <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="LookupRegistration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                        <RequiredField ErrorText="Please enter the Lookup Type" IsRequired="True" />
                                                    </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </td>
                                             
                                                <td>
                                                    <dx:ASPxButton ID="btnFind" runat="server"   ClientInstanceName="btnFind" ClientEnabled="false"
                                                        Text="Show List"   Font-Size="Small" AutoPostBack="False" CausesValidation="false">
                                                        <ClientSideEvents Click="function(s, e) {
	                                                                    DoShowClick();
                                                                    }" />
                                                    </dx:ASPxButton>

                                                       <dx:ASPxButton ID="btnNewLookups" runat="server" AutoPostBack="False" ClientInstanceName="btnNewLookups" ClientEnabled="false"  CausesValidation="false" Text="Add New" >
                                        <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewLookupClick();
                                                                    }" />
                                       
                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                           </div>
                            <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData"  ShowHeader="false" Width="700px">
                                <PanelCollection>
                                    <dx:PanelContent>
                                          <table style="width:100%">
                                        <tr>
                                            <td >
                                                <asp:Label ID="lblDescription" runat="server" Text="Amharic Description:"></asp:Label>
                                                <span class="star">*</span>
                                            </td>
                                            <td >
                                                <dx:ASPxTextBox ID="txtAmdescription" runat="server" style="width:400px" MaxLength="40">
                                                    <ValidationSettings ErrorDisplayMode="Text"  ErrorTextPosition="Bottom" Display="Dynamic">
                                                        <%--<RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />--%>
                                                            <%--<RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$" />--%>
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                          
                                        </tr>
                                                 <tr>
                                            <td >
                                                <asp:Label ID="Label1" runat="server" Text="English Description:"></asp:Label>
                                                 <span class="star">*</span>
                                            </td>
                                            <td >
                                                <dx:ASPxTextBox ID="txtDescription" runat="server" style="width:400px" MaxLength="40">
                                                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom" Display="Dynamic">
                                                        <%--<RequiredField ErrorText="Please enter English nam.e" IsRequired="True" />--%>
                                                    <%--<RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />--%>
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                          
                                        </tr>
                                               <tr>
                                            <td >
                                                <asp:Label ID="Label5" runat="server" Text="Is Active:"></asp:Label>
                                                <span class="star">*</span>
                                            </td>
                                            <td>
                                                <dx:ASPxCheckBox ID="chIsActive" runat="server" Checked="true" style="margin:0px" ></dx:ASPxCheckBox>
                                            </td>
                                           </tr>
                                      <tr>
                                          <td></td>
                                          <td>
                                              <div style="float:right">
                                                  <table>
                                                      <tr>
                                                          <td>
                                                              <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" ClientInstanceName="btnCancelClient" BackColor="Silver" Text="Cancel" CausesValidation="false">
                                                                  <ClientSideEvents Click="DoCancelClient" />
                                                              </dx:ASPxButton>
                                                          </td>
                                                          <td>
                                                              <dx:ASPxButton ID="btnSaveLookups" runat="server" ClientInstanceName="btnSaveLookups" Text="Save" AutoPostBack="False" CausesValidation="true" Style="float: right">
                                                                  <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveLookupClick();
                                                                    }" />

                                                              </dx:ASPxButton>
                                                          </td>
                                                      </tr>
                                                  </table>


                                              </div>
                                          </td>
                                      </tr>
                                       
                                    </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                          </dx:ASPxRoundPanel>
                           
                                                    <dx:ASPxGridView ID="gvwLookups" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwLookups" EnableTheming="True" EnableCallBacks="false"  Width="600px" 
                                                         KeyFieldName="Id"  OnPageIndexChanged="Grid_PageIndexChanged" >
                                                        <ClientSideEvents CustomButtonClick="gvwLookups_CustomButtonClick" />
                                                        <Settings ShowFilterRow="True" />

<SettingsPopup>
<HeaderFilter MinHeight="140px"></HeaderFilter>
</SettingsPopup>
                                                        <SettingsSearchPanel Visible="True" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Amharic Name" FieldName="Amdescription" ShowInCustomizationForm="True" VisibleIndex="0">
                                                            </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="English Name" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1">
                                                            </dx:GridViewDataTextColumn>
                                                          
                                                            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="5" Width="100px" ShowClearFilterButton="True">
                                                                <CustomButtons>
                                                                    <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Edit">
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                    <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                                        <Styles Style-ForeColor="Red">
                                                                            <Style ForeColor="Red">
                                                                            </Style>
                                                                        </Styles>
                                                                    </dx:GridViewCommandColumnCustomButton>
                                                                </CustomButtons>
                                                            </dx:GridViewCommandColumn>
                                                        </Columns>
                                                        <SettingsPager PageSize="6"></SettingsPager>
                                                        <Styles>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>

            
    
              
        </div>
                </div>
  </div>
               
    
            
</asp:Content>

