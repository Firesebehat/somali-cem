﻿using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.Domain;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class LookupType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                pnlData.ClientVisible = false;
            }
            DoFind();
        }
        protected void DoNew()
        {
            Session["PageMode"] = "NEW";
            chIsActive.Checked = true;
            txtDescription.Text = string.Empty;
            txtAmhDescription.Text = string.Empty;
            txtDescription.IsValid = true;
            txtAmhDescription.IsValid = true;
        }

        protected void Delete(int lookupTypeId)
        {
            LookupTypeBussiness objLookupTypeBussiness = new LookupTypeBussiness();
            if (objLookupTypeBussiness.Delete(lookupTypeId))
            {
                DoFind();
                clbLookupTypesID.JSProperties["cpStatus"] = "INFO";
                pnlData.ClientVisible = false;
                ShowMessage("Selected lookup type deleted successfully!!!");
            }
        }


        protected void LoadAllLookupTypes()
        {
            LookupTypeBussiness objLookupTypeBussiness = new LookupTypeBussiness();
            List<CUSTOR.Domain.LookupType> lookupList = objLookupTypeBussiness.GetLookupTypes();
            if (lookupList != null)
            {
                gvwLookupTypes.DataSource = lookupList;
                gvwLookupTypes.DataBind();
                gvwLookupTypes.ClientVisible = true;
            }
            else
            {
                gvwLookupTypes.ClientVisible = false;
            }
        }

        protected void LoadLookupTypeInfo(int lookupTypeId)
        {
            clbLookupTypesID.JSProperties["cpAction"] = "Loaded";
            ASPxEdit.ClearEditorsInContainer(pnlData);
            LookupTypeBussiness objLookupTypeBussiness = new LookupTypeBussiness();
            CUSTOR.Domain.LookupType lookupType = objLookupTypeBussiness.GetLookupType(lookupTypeId);
            if (lookupType != null)
            {
                ValidControlTrue();
                Session["LookupTypeId"] = lookupType.Id;
                txtDescription.Text = lookupType.Description;
                chIsActive.Checked = lookupType.IsActive;
                txtDescription.Text = lookupType.Description;
                txtAmhDescription.Text = lookupType.Amdescription;
                clbLookupTypesID.JSProperties["cpStatus"] = "SUCCESS";
            }
        }

        private void ValidControlTrue()
        {
            txtAmhDescription.IsValid = true;
            txtDescription.IsValid = true;
        }

        protected void SaveEdit()
        {
            clbLookupTypesID.JSProperties["cpAction"] = "Save";

            int LookupTypeId = Convert.ToInt32(Session["LookupTypeId"]);
            LookupTypeBussiness objLookupTypeBussiness = new LookupTypeBussiness();



            #region Build LookupType Information

            CUSTOR.Domain.LookupType objLookupType = new CUSTOR.Domain.LookupType();
            objLookupType.Id = LookupTypeId;
            objLookupType.Description = txtDescription.Text;
            objLookupType.Amdescription = txtAmhDescription.Text;
            CVGeez geez = new CVGeez();
            objLookupType.SortValue = geez.GetSortValueU(txtDescription.Text);
            objLookupType.Soundx = geez.GetSoundexValue(txtDescription.Text);
            objLookupType.IsActive = chIsActive.Checked;
            objLookupType.UpdatedBy = "user";
            objLookupType.UpdatedDate = DateTime.Now;
            #endregion
            #region Save LookupType Informatin
            if(objLookupTypeBussiness.UpdateExists(objLookupType))
            {
                ShowError("Lookup name already exist");
                return;
            }

            if (objLookupTypeBussiness.UpdateLookupType(objLookupType))
            {
                //LoadSavedLookupType(Convert.ToInt32(objLookupType.ID));
                LoadAllLookupTypes();
                clbLookupTypesID.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }

            #endregion



        }

        protected void SaveNew()
        {

            #region Save LookupType Information
            LookupTypeBussiness objLookupTypeBussiness = new LookupTypeBussiness();
            CUSTOR.Domain.LookupType objLookupType = new CUSTOR.Domain.LookupType();
            objLookupType.Description = txtDescription.Text;
            objLookupType.IsActive = chIsActive.Checked;
            objLookupType.Amdescription = txtAmhDescription.Text;
            CVGeez geez = new CVGeez();
            objLookupType.SortValue = geez.GetSortValueU(txtDescription.Text);
            objLookupType.Soundx = geez.GetSoundexValue(txtDescription.Text);
            objLookupType.IsActive = chIsActive.Checked;
            objLookupType.CreatedBy = "user";
            objLookupType.CreatedDate = DateTime.Now;
            if (objLookupTypeBussiness.Exists(objLookupType))
            {
                ShowError("Lookup name already exist");
                return;
            }
            bool registeredLookup = objLookupTypeBussiness.InsertLookupType(objLookupType);
            if (registeredLookup)
            {
                // DoNew();
                //LoadSavedLookupType(registeredLookup);
                LoadAllLookupTypes();
                clbLookupTypesID.JSProperties["cpStatus"] = "SUCCESS";
                clbLookupTypesID.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }


            #endregion
        }







        private void ShowError(string strMsg)
        {
            clbLookupTypesID.JSProperties["cpMessage"] = strMsg;
            clbLookupTypesID.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbLookupTypesID.JSProperties["cpMessage"] = strMsg;
            clbLookupTypesID.JSProperties["cpStatus"] = "INFO";
        }

        private void ShowSuccess(string strMsg)
        {
            clbLookupTypesID.JSProperties["cpMessage"] = strMsg;
            clbLookupTypesID.JSProperties["cpStatus"] = "SUCCESS";
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }



        protected void Saved()
        {
            Page.Validate();
            if (!ASPxEdit.ValidateEditorsInContainer(clbLookupTypesID))
            {
                gvwLookupTypes.ClientVisible = false;
                pnlData.ClientVisible = true;
                return;
            }
            clbLookupTypesID.JSProperties["cpAction"] = "Save";


            switch (Session["PageMode"].ToString())
            {
                case "NEW":
                    SaveNew();
                    break;
                case "EDIT":
                    SaveEdit();
                    break;
            }
        }

        protected void ClbLookupTypes_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            clbLookupTypesID.JSProperties["cpStatus"] = "";
            clbLookupTypesID.JSProperties["cpAction"] = "";
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    clbLookupTypesID.JSProperties["cpAction"] = "New";
                    DoNew();
                    clbLookupTypesID.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Save":
                    Saved();
                    break;


                case "EDITING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        LoadLookupTypeInfo(Convert.ToInt32(strID));
                        Session["PageMode"] = "EDIT";
                        //gvwLookupTypes.Visible = true;
                    }
                    break;
                case "DELETING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        Session["ID"] = strID;
                        Delete(Convert.ToInt32(Session["ID"]));
                    }
                    break;
                case "Report":
                    Session["PrintReport"] = false;
                    //GenerateReport();
                    break;
                case "Cancel":
                    clbLookupTypesID.JSProperties["cpAction"] = "Cancel";
                    clbLookupTypesID.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Find":
                    DoFind();
                    pnlData.ClientVisible = false;
                    break;
                default:
                    break;
            }
        }

        private void DoFind()
        {
            clbLookupTypesID.JSProperties["cpAction"] = "Show";
            LookupTypeBussiness objLookupBus = new LookupTypeBussiness();
            try
            {
                List<CUSTOR.Domain.LookupType> list = objLookupBus.GetLookupTypes();
                if (list.Count > 0)
                {
                    gvwLookupTypes.DataSource = list;
                    gvwLookupTypes.DataBind();
                    gvwLookupTypes.ClientVisible = true;
                    ShowSuccess("");
                }
                else
                {
                    gvwLookupTypes.ClientVisible = false;
                    ShowMessage("No data found!");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void gvwLookupTypes_PageIndexChanged(object sender, EventArgs e)
        {
            LoadAllLookupTypes();
        }
        private void FillLanguage(ASPxComboBox combo)
        {
            try
            {
                string qtext = "select * from NewsLanguage where IsActive = '1' AND LangId = 0";
                CDropdown.FillComboFromDB(combo, qtext, "Description", "Id");
            }
            catch
            {
            }
        }

        protected void gvwLookupTypes_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
                LoadAllLookupTypes();
        }
    }
}