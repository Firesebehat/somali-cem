﻿using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.DataAccess;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages
{
    public partial class SiteTransfer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                BindUsers();
                FillCombo(cboUnitNew, Language.eLanguage.eEnglish, typeof(OrganizationalType.OrgType));
                BindTransferHistory();
            }
        }

        protected void gvSiteTransfer_PageIndexChanged(object sender, EventArgs e)
        {
            BindTransferHistory();
        }

        protected void gvSiteTransfer_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            BindTransferHistory();
        }

        protected void gvSiteTransfer_Init(object sender, EventArgs e)
        {

        }

        protected void gvSiteTransfer_CustomCallback1(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {

        }

        protected void clbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                default:
                    break;
            }
        }

        private void DoNew()
        {
            clbClient.JSProperties["cpAction"] = "New";
            cboUnitNew.Value = null;
            cboSiteNew.Value = null;
            txtRemark.Text = string.Empty;
            txtUser.Text = string.Empty;
            grdUsers.Selection.UnselectAll();
            ShowSuccess("");
        }

        protected void grdUsers_Init(object sender, EventArgs e)
        {

        }
        private void FillCombo(ASPxComboBox cboName, Language.eLanguage eLang, Type t)
        {
            cboName.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            cboName.TextField = "Value";
            cboName.ValueField = "Key";
            cboName.DataBind();
            cboName.SelectedIndex = -1;
        }

       

        protected void grdUsers_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            BindUsers();
        }
        private void BindUsers()
        {
            UserBussiness objUser = new UserBussiness();
            List<CUSTOR.Domain.User> lst = new List<CUSTOR.Domain.User>();
            if (HttpContext.Current.User.IsInRole("Super Admin"))
            {
                lst = objUser.GetUsers();
            }
            if (lst.Count > 0)
            {
                grdUsers.DataSource = lst;
                grdUsers.DataBind();
            }
        }
        private void DoSave()
        {
            try
            {
                //update fullname,site,and usergroup info
                clbClient.JSProperties["cpAction"] = "Save";
                DateTime date = DateTime.Now;
                UserSiteTransferHistorBussiness objBusiness = new UserSiteTransferHistorBussiness();
                UserSiteTransferHistor objTransferDomain = new UserSiteTransferHistor()
                {
                    SiteIdNew = Int32.Parse(cboSiteNew.Value.ToString()),
                    Remark = txtRemark.Text,
                    TransferedBy = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    CreatedUserName = User.Identity.Name,
                    TransferedDate = date,
                   
                };

                List<object> userList = grdUsers.GetSelectedFieldValues("UserName", "TaxCenterId");
                foreach (var item in userList)
                {
                    object[] row = (object[])item;
                    string userName = row[0].ToString();
                    objTransferDomain.SiteIdOld = Int32.Parse(row[1].ToString());
                    UserBussiness userBussiness = new UserBussiness();
                    objTransferDomain.UserId = userBussiness.GetUserByName(userName).UserId;
                    objBusiness.UpdateSiteTransfer(objTransferDomain);
                    
                    //update profile
                    DoSaveProfile(userName);
                    ShowSuccess("The record is saved successfully.");
                }
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        private void DoSaveProfile(string userName)
        {
            //int taxCenterId = Convert.ToInt32(cboSite.Value);
           // TaxCenterBussiness TaxCenterBis = new TaxCenterBussiness();

            var p = ProfileBase.Create(userName);
            

            var OrgP = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));
            
            OrgP.SetPropertyValue("TaxCenterId",Convert.ToInt32(cboSiteNew.Value));
            p.Save();
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }

        protected void grdUsers_PageIndexChanged(object sender, EventArgs e)
        {
            BindUsers();
        }

        protected void grdUsers_DataBinding(object sender, EventArgs e)
        {
            UserBussiness objUser = new UserBussiness();
            List<CUSTOR.Domain.User> lst = new List<CUSTOR.Domain.User>();
            if (HttpContext.Current.User.IsInRole("Super Admin"))
            {
                lst = objUser.GetUsers();
            }
            if (lst.Count > 0)
            {
                grdUsers.DataSource = lst;
            }
        }

        protected void BindTransferHistory()
        {
            gvSiteTransfer.DataSource = null;
            UserSiteTransferHistorBussiness business = new UserSiteTransferHistorBussiness();
            try
            {
                List<UserSiteTransferHistor> list = business.GetSiteTransferDetails();
                if (list.Count > 0)
                {
                    gvSiteTransfer.DataSource = list;
                }
                gvSiteTransfer.DataBind();
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        
        private void FillSite(ASPxComboBox cbo, string orgType)
        {

            string strSQL = "Select Id,CenterNameEnglish FROM TaxCenter where IsNull(IsDeleted,0) = 0 and IsNULL(IsActive,0)=1 ";
            if (orgType != string.Empty)
                strSQL += " and OrgType='" + orgType + "'";
            CDropdown.FillComboFromDB(cbo, strSQL, "CenterNameEnglish", "Id");
            cbo.Value = null;
        }

        protected void cboSiteNew_Callback(object sender, CallbackEventArgsBase e)
        {
            FillSite(cboSiteNew, e.Parameter);
        }
    }
}