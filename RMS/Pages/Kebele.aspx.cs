﻿using CUSTOR.Business;
using CUSTOR.Common;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class Kebele : System.Web.UI.Page
    {
        const string RegionCode = "14";
        const string ZoneCode = "80";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !Page.IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                Session["NewKebele"] = true;
                FillRegion(cboRegion);
                FillRegion(cboRegionFind);
                cboRegion.Value = cboRegionFind.Value = RegionCode;
                FillZone(cboRegion.Value.ToString(), cboZone);
                FillZone(cboRegion.Value.ToString(), cboZoneFind);
                cboZone.Value = cboZoneFind.Value = ZoneCode;

                cboZone.Value = cboZoneFind.Value = ZoneCode;
                FillWoreda(ZoneCode,cboWoreda);
                FillWoreda(cboZoneFind.Value.ToString(),cboWoredaFind);
                pnlData.ClientVisible = false;
                gvKebele.ClientVisible = false;
            }
            BindGridByWoreda();
        }
        private void FillCombo(DevExpress.Web.ASPxComboBox ddl, Language.eLanguage eLang, Type t)
        {
            ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            ddl.ValueField = "Key";
            ddl.TextField = "Value";
            ddl.DataBind();
        }

        protected void FillRegion(DevExpress.Web.ASPxComboBox cboRegion)
        {
            CDropdown.FillComboFromDB(cboRegion, "SELECT Code,Description FROM Region", "Description", "Code");
        }
        protected void FillZone(string region, ASPxComboBox cboZone)
        {
            string strSQL = String.Format("SELECT Code,Description FROM Zone WHERE Parent='{0}'", region);
            CDropdown.FillComboFromDB(cboZone, strSQL, "Description", "Code");
        }
        protected void FillWoreda(string zone, ASPxComboBox cbo)
        {
            string strSQL = String.Format("SELECT Code,Description FROM Woreda WHERE Parent='{0}'", zone);
            CDropdown.FillComboFromDB(cbo, strSQL, "Description", "Code");
            //CDropdown.FillComboFromDB(cboWoredaFind, strSQL, "Description", "Code");
        }
        protected void cboZone_Callback(object source, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "-1")
                FillZone(e.Parameter, cboZone);
        }
        protected void cboZoneFind_Callback(object source, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "-1")
                FillZone(e.Parameter, cboZoneFind);
        }
        protected void cboWoreda_Callback(object source, CallbackEventArgsBase e)
        {
            //if (e.Parameter.ToString() != "-1")
            //    FillWoreda(e.Parameter);
        }
        protected void cboWoredaFind_Callback(object source, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "-1")
                FillWoreda(e.Parameter,cboWoredaFind);
        }


        protected void ClbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            RestorValues();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Search":
                    BindGridByWoreda();
                    clbClient.JSProperties["cpAction"] = "List";
                    ShowSuccess(string.Empty);
                    break;
                case "Edit":
                    DoFind(strID);
                    break;
                case "Delete":
                    DoDelete(strID);
                    break;
                case "Cancel":
                    DoNew();
                    clbClient.JSProperties["cpAction"] = "Cancel";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Clear":
                    FillRegion(cboRegionFind);
                    clbClient.JSProperties["cpAction"] = "Clear";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                default:
                    break;
            }
        }

        protected bool DoFind(string ID)
        {
            CUSTOR.Business.KebeleBusiness objKebeleBusiness = new CUSTOR.Business.KebeleBusiness();

            CUSTOR.Domain.Kebele objKebele = new CUSTOR.Domain.Kebele();

            try
            {
                objKebele = objKebeleBusiness.GetKebele(ID);
                if (objKebele == null)
                {
                    ShowMessage("No Record is found.");
                    return false;
                }
                Session["NewKebele"] = false;
                ClearForm();
                txtAmharic.Text = objKebele.AmDescription;
                txtEnglish.Text = objKebele.Description;
                //cboRegion.Value = RegionCode;
                //cboRegion.Text = "Addis Ababa";
                objKebeleBusiness.GetRegionCode(objKebele.Parent);
                FillZone(cboRegionFind.Value.ToString(), cboZone);
                cboZone.Value = objKebeleBusiness.GetZoneCode(objKebele.Parent);
                cboZone.Value = cboZoneFind.Value;
                FillWoreda(cboZoneFind.Value.ToString(),cboWoreda);
                cboWoreda.Value = cboWoredaFind.Value;


                Session["ID"] = ID;
                clbClient.JSProperties["cpAction"] = "Find";
                ShowSuccess(string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Find");
                return false;
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
            clbClient.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        protected bool DoSave()
        {
            CUSTOR.Domain.Kebele objKebele = new CUSTOR.Domain.Kebele();

            try
            {
                clbClient.JSProperties["cpAction"] = "Save";
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {

                    ShowError(GetErrorDisplay(errMessages), "Save");
                    return false;
                }

                CVGeez objGeez = new CVGeez();

                objKebele.AmDescription = txtAmharic.Text;
                objKebele.AmDescriptionSort = objGeez.GetSortValueU(txtAmharic.Text);
                objKebele.Description = txtEnglish.Text;
                objKebele.Parent = cboWoreda.Value.ToString();

                KebeleBusiness objKebeleBusiness = new KebeleBusiness();

                bool bNew = ((bool)Session["NewKebele"]);

                if (bNew)
                {
                    if (!objKebeleBusiness.Exists(objKebele.Description, objKebele.AmDescriptionSort, objKebele.Parent))
                    {
                        objKebele.Code = objKebeleBusiness.GetNextCode();
                        objKebeleBusiness.InsertKebele(objKebele);
                    }
                    else
                    {
                        ShowError("Kebele name exists. Try another name");
                        return false;
                    }
                }
                else
                {

                    objKebele.Code = Session["ID"].ToString();
                    if (!objKebeleBusiness.Exists(objKebele.Description, objKebele.AmDescriptionSort, objKebele.Code, objKebele.Parent))
                    {

                        objKebeleBusiness.UpdateKebele(objKebele);
                    }
                    else
                    {
                        ShowError("Kebele name exists. Try another name");
                        return false;
                    }
                }
                Session["NewKebele"] = false;

                //BindGrid();
                ShowSuccess("Record is saved successfully.");
                ClearForm();
                BindGridByWoreda();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Save");
                return false;
            }
        }
        protected void ClearForm()
        {

            txtAmharic.Text = "";
            txtEnglish.Text = "";
            txtAmharic.IsValid = true;
            txtEnglish.IsValid = true;
            cboRegion.IsValid = true;
            cboZone.IsValid = true;
            cboWoreda.IsValid = true;
        }
        protected bool DoNew()
        {
            try
            {
                Session["NewKebele"] = true;
                ClearForm();
                clbClient.JSProperties["cpAction"] = "New";
                ShowSuccess(string.Empty);
                cboRegion.Value = cboRegionFind.Value;

                if (cboRegion.Value!=null)
                 FillZone(cboRegion.Value.ToString(),cboZone);

                cboZone.Value = cboZoneFind.Value;
                if (cboZone.Value != null)
                    FillWoreda(cboZone.Value.ToString(),cboWoreda);
                cboWoreda.Value = cboWoredaFind.Value;
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        protected bool DoDelete(string intID)
        {
            Kebele objKebele = new Kebele();
            KebeleBusiness objKebeleBusiness = new KebeleBusiness();
            try
            {
                objKebeleBusiness.Delete(intID);
                Session["NewKebele"] = false;

                //BindGrid();
                BindGridByWoreda();
                clbClient.JSProperties["cpAction"] = "Delete";
                ShowSuccess("Record is deleted successfully.");
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Delete");
                return false;
            }
        }
        private void BindGridByWoreda()
        {
            try
            {
                KebeleBusiness objKebeleBusiness = new KebeleBusiness();
                List<CUSTOR.Domain.Kebele> l = new List<CUSTOR.Domain.Kebele>();
                if (cboWoredaFind.Value != null)
                {
                    l = objKebeleBusiness.GetKebeles(cboWoredaFind.Value.ToString());
                    if (l != null && l.Count > 0)
                    {
                        gvKebele.DataSource = l;
                        gvKebele.DataBind();
                        gvKebele.JSProperties["cpStatus"] = "SUCCESS";
                        gvKebele.ClientVisible = true;

                    }
                    else
                    {
                        gvKebele.JSProperties["cpStatus"] = "NOTFOUND";
                        gvKebele.ClientVisible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                gvKebele.DataSource = null;
                gvKebele.DataBind();
                gvKebele.ClientVisible = false;
                gvKebele.JSProperties["cpStatus"] = "ERROR";

            }
        }
        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();


            if (txtEnglish.Text == string.Empty &&
               txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter Kebele description!");
            }
            if (txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter Kebele description in Amharic!");
            }
            if (txtEnglish.Text == string.Empty)
            {
                errMessages.Add("Enter Kebele description in English");
            }
            if (cboRegion.Value == null)
            {
                errMessages.Add("Select the Parent Region");
            }
            if (cboZone.Value==null)
            {
                errMessages.Add("Select the Parent Zone");
            }
            if (cboWoreda.Value==null)
            {
                errMessages.Add("Select the Parent Woreda");
            }
            KebeleBusiness objKebele = new KebeleBusiness();
            bool bNew = (bool)Session["NewKebele"];

            return errMessages;
        }
        protected void gvKebele_PageIndexChanged(object sender, EventArgs e)
        {
            BindGridByWoreda();
        }
        protected void Grd_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            gvKebele.JSProperties["cpAction"] = "List";
            BindGridByWoreda();
        }
        protected void RestorValues()
        {
            if (cboZone.Items.Count == 0 && cboRegion.Value != null)
            {
                FillZone(cboRegion.Value.ToString(), cboZone);
            }
            if (cboWoreda.Items.Count == 0 && cboZone.Value != null)
            {
                FillWoreda(cboZone.Value.ToString(),cboWoreda);
            }

            if (cboZoneFind.Items.Count == 0 && cboRegionFind.Value != null)
            {
                FillZone(cboRegionFind.Value.ToString(), cboZoneFind);
            }
            if (cboWoredaFind.Items.Count == 0 && cboZoneFind.Value != null)
            {
                FillWoreda(cboZoneFind.Value.ToString(),cboWoredaFind);
            }
        }
    }
}