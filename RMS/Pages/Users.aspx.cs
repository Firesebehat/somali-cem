﻿using CUSTOR;
using CUSTOR.Common;
using RMS.Controls;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //UserGvUtil.AssignDefaultUserNameLetter(categoryID, gvwUsers, IsPostBack);

            Msg.MsgBoxAnswered += MessageResponse;

            if (!Page.IsPostBack)
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now);//check the user
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Administrator")
                          || HttpContext.Current.User.IsInRole("Super Admin")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                FillOrg();
                Button1.Focus();
                string strName = "%";
                BindGrid(strName);

            }



        }
        private int GetOrgID()
        {
            try
            {
               return CurrentProfile.ActiveUserProfile.TaxCenterId;
                //var p = ProfileBase.Create(HttpContext.Current.User.Identity.Name);
                //var site = ((ProfileGroupBase)(p.GetProfileGroup("Organization")));
                //return (int)site.GetPropertyValue("Id");

            }
            catch
            {
                return 0;
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //BindGrid("%");
        }
        private void ResetPassword(string strName)
        {
            MembershipUser usr = Membership.GetUser(strName);
            string resetPwd = usr.ResetPassword();
            usr.ChangePassword(resetPwd, "NEW");
        }
        private void BindGridByName(string strName)
        {

            CUSTOR.CUser objUser = new CUSTOR.CUser();
            int userType = Int16.Parse(rdbIsActive.Value.ToString());
            string orgCode = cboSite.Value != null && cboSite.Value.ToString() != "-1" ? Convert.ToString(cboSite.Value) : string.Empty;
            List<CUSTOR.UserProfile> lst = new List<CUSTOR.UserProfile>();
            //if (HttpContext.Current.User.IsInRole("Super Admin"))
            //{
                lst = objUser.GetAdminUsers(strName, userType, orgCode, 0, 2000);
            //}
            //else if (HttpContext.Current.User.IsInRole("Administrator"))
            //{
            //    lst = objUser.GetUsersByOrg(strName, isActiveUser, orgCode, 0, 2000, GetOrgID());
            //}
            //else
            //    return;

            //int n=dt.Rows.Count;
            if (lst.Count > 0)
            {
                gvwUsers.DataSource = lst;
                gvwUsers.DataBind();
                lblRecords.Text = lst.Count.ToString() + " users";
            }
            else
            {
                gvwUsers.DataSource = null;
                gvwUsers.DataBind();
            }

        }
        private void BindGrid(string searchedUser)
        {
            CUSTOR.CUser objUser = new CUSTOR.CUser();
            //DataTable dt = new DataTable();
            List<CUSTOR.UserProfile> lst = new List<CUSTOR.UserProfile>();
            string userFirstLetter = HttpContext.Current.Request.QueryString["az"];
            if (string.IsNullOrEmpty(userFirstLetter))
            {
               // searchedUser = searchedUser;
               // Session["ActiveUserOnly"] = null;
            }
            else
                searchedUser = userFirstLetter + "%";
            int activUser = Int16.Parse(rdbIsActive.Value.ToString());
            
            string SearchedSite = cboSite.Value != null && cboSite.Value.ToString() != "-1" ?
                Convert.ToString(cboSite.Value.ToString()) : "0";
            
            //temp now user is superadmini

            //lst = objUser.GetAdminUsers(strName, isActiveOnly, siteCode, 0, 2000);

            if (HttpContext.Current.User.IsInRole("Super Admin"))
            {
                lst = objUser.GetAdminUsers(searchedUser, activUser, SearchedSite, 0, 2000);
            }
            else 
            {
                int currentUserSite = GetOrgID();
                int OrgType = CurrentProfile.ActiveUserProfile.Category;
                lst = objUser.GetUsersByOrg(User.Identity.Name,currentUserSite,OrgType, searchedUser,  SearchedSite, activUser, 0, 2000);
            }
            if (lst.Count > 0)
            {
                gvwUsers.DataSource = lst;
                gvwUsers.DataBind();
                lblRecords.Text = lst.Count.ToString() + " users";
            }
            else
            {
                gvwUsers.DataSource = null;
                gvwUsers.DataBind();
            }

        }
        protected void Grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwUsers.PageIndex = e.NewPageIndex;

            BindGrid(categoryID.Text);
        }
        protected void gvwUsers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void mnuMain_MenuItemClick(object sender, MenuEventArgs e)
        {
            try
            {
                if (e.Item.Value.ToLower() == "delete")
                {
                    Msg.ShowMessage("Are you sure?", Msg.enmMessageType.Attention, false, true, string.Empty);
                    string strMsg = "Selected users were DELETED successfully";
                    CMsgBar.ShowSucess(strMsg, pnlInfo, lblInfo);
                }
                else if (e.Item.Value.ToLower() == "approve")
                {
                    if (UserGvUtil.ApproveSelectedUsers(gvwUsers, hlMsg))
                    {
                        BindGrid(categoryID.Text);

                    }
                    else
                    { return; }
                    string strMsg = "Selected users were APPROVED successfully";
                    CMsgBar.ShowSucess(strMsg, pnlInfo, lblInfo);
                }
                else if (e.Item.Value.ToLower() == "unlock")
                {
                    UserGvUtil.UnlockSelectedUsers(gvwUsers, hlMsg);
                    BindGrid(categoryID.Text);
                    string strMsg = "Selected users were UNLOCKED successfully";
                    CMsgBar.ShowSucess(strMsg, pnlInfo, lblInfo);
                }
                else if (e.Item.Value.ToLower() == "unapprove")
                {
                    UserGvUtil.UnapproveSelectedUsers(gvwUsers, hlMsg);
                    BindGrid(categoryID.Text);
                }
                else if (e.Item.Value.ToLower() == "reset")
                {
                    UserGvUtil.ResetSelectedUsers(gvwUsers, hlMsg);
                    BindGrid(categoryID.Text);
                    string strMsg = "Selected users were RESETED successfully";
                    CMsgBar.ShowSucess(strMsg, pnlInfo, lblInfo);
                }
                //lblMsg.Text = "Operation Successful";
            }
            catch (Exception ex)
            {
                lblMsg.Text = ex.Message;
            }
        }

        public void MessageResponse(object sender, Msg.MsgBoxEventArgs e)
        {


            if (e.Answer == Msg.enmAnswer.OK)
            {
                UserGvUtil.DeleteSelectedUsers(gvwUsers, hlMsg);
                BindGrid(categoryID.Text);
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            BindGrid(txtFindUser.Text + "%");
        }
        protected void gvwUsers_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void txtFindUser_TextChanged(object sender, EventArgs e)
        {

            BindGrid(txtFindUser.Text + "%");

        }

        protected void chActiveUserOnly_CheckedChanged(object sender, EventArgs e)
        {
           // Session["ActiveUserOnly"] = chActiveUserOnly.Checked;
            BindGrid(txtFindUser.Text + "%");
        }
        private void FillOrg()
        {

            string strSQL = "Select Id,CenterNameEnglish FROM TaxCenter where IsActive=1";
            CDropdown.FillComboFromDB(cboSite, strSQL, "CenterNameEnglish", "Id");
            cboSite.Items.Add("All", "-1");
            cboSite.SelectedIndex = -1;
            cboSite.ClientEnabled = true;

            if (!Page.User.IsInRole("Super Admin"))
            {
                cboSite.Value = CurrentProfile.ActiveUserProfile.TaxCenterId.ToString();
                cboSite.Enabled = false;
            }
                    
        }

    }
}