﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Nationality.aspx.cs" MasterPageFile="~/Root.master" Inherits="RMS.Pages.Nationality" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">
        function onMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                clbClient.PerformCallback('New');
            }
            else if (e.item.name === "mnuSave") {
                if (!ASPxClientEdit.ValidateGroup('Registration')) {
                    return;
                }
                clbClient.PerformCallback('Save');
            }
            else if (e.item.name === "mnuList") {
                DoListView();
                gridNationality.PerformCallback('Search');
            }
        }

        function DoCancelClient() {
            pnlData.SetVisible(false);
        }

        function DoAddClientClick(s, e) {
            pnlData.SetVisible(true);
            pnlGrid.SetVisible(false);

            DoNewClientClick();
        }


        var rowVisibleIndex;
        var strID;
        function gridNationality_CustomButtonClick(s, e) {

            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID == 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Id', ShowPopup);
            }
            if (e.buttonID == 'EDITING') {

                gridNationality.GetRowValues(e.visibleIndex, "Id", OnGetSelectedFieldValues);
            }
        }

        function OnGetSelectedFieldValues(values) {
            clbClient.PerformCallback('Edit' + '|' + values);
        }

        function ShowPopup(values) {

            ShowConfirmx('Are you sure do you want to delete this record?', function (result) {
                if (result)
                    Delete(values);
            }, 'en');
        }
        function Delete(values) {
            clbClient.PerformCallback('Delete' + '|' + values);
        }
        function OnClientEndCallback(s, e) {

            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Cancel") {
                    DoListView();
                    return;
                }
                else if (s.cpAction == "Delete") {
                    ShowSuccess(s.cpMessage);
                    DoListView();
                    return;
                }
                else if (s.cpAction == "Find") {
                    DoEditView();
                }
                else if (s.cpAction == "List") {
                    DoListView();
                }
                else if (s.cpAction == "New") {
                    DoEditView();
                    return;
                }
                else if (s.cpMessage == '') {
                    return;
                }
                if (s.cpAction == "Save") {

                    ShowSuccess(s.cpMessage);
                    DoListView();
                }

            }
            else if (s.cpStatus === "ERROR") {
                ShowError(s.cpMessage);
                if (s.cpAction == "Delete" || s.cpAction == "Find" || s.cpAction == "List") {
                    return;
                }

                if (s.cpAction == "Save") {
                    DoEditView();
                }

            }
            else
                pnlData.SetVisible(false);
            s.cpStatus = '';
            s.cpMessage = '';
        }
        function DeleteGridRow(visibleIndex) {
            var index = gridNationality.GetFocusedRowIndex();

            gridNationality.DeleteRow(index);
        }
        function DoListView() {
            pnlGrid.SetVisible(true);
            pnlData.SetVisible(false);
            ToggleEnabled("mnuSave", false);
        }
        function DoEditView() {
            pnlData.SetVisible(true);
            pnlGrid.SetVisible(false);
            ToggleEnabled("mnuSave", true);
        }
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            //DoListClientClick();
        };
    </script>
    <style>
        td {
            vertical-align: top;
        }

        .star {
            color: red;
            margin: 1px;
            font-size: large;
        }
    </style>

    <div class="contentmain">
        <h5>Nationality Maintenance
        </h5>
        <div class="card card-body">
            <script src="../Content/ModalDialog.js"></script>
            <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />

            <div style="padding-bottom: 15px">



                <div>
                    <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                        ShowPopOutImages="True" EnableTheming="True" Height="35px"
                        ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                        AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                        <ClientSideEvents Init="onMenuInit" ItemClick="onMenuItemClicked"></ClientSideEvents>
                        <Items>
                            <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="iconbuilder_actions_add_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="save_save_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                            <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                </Image>
                            </dx:MenuItem>
                        </Items>
                    </dx:ASPxMenu>
                </div>
            </div>

            <div>

                <dx:ASPxCallbackPanel ID="clbClient" OnCallback="ClbClient_Callback" ClientInstanceName="clbClient" runat="server" Style="max-width: 700px">

                    <ClientSideEvents EndCallback="function(s,e){OnClientEndCallback(s,e);}" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlCategory" runat="server" SupportsDisabledAttribute="True">
                            <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" Width="100%" ShowHeader="false">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblAmharic" runat="server" Text="Country Name (Amharic):"></asp:Label>
                                                    <asp:Label ID="AmharicStar" runat="server" Text="*" EnableTheming="false" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtAmharic" runat="server" Width="250px">
                                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />
                                                            <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEnglish" runat="server" Text="Country Name (English):"></asp:Label>
                                                    <asp:Label ID="EnglishStar" runat="server" Text="*" EnableTheming="false" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtEnglish" runat="server" Width="250px">
                                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter English name." IsRequired="True" />
                                                            <RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </table>


                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>


                            <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="100%" ClientVisible="false">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxGridView ID="gridNationality" runat="server" AutoGenerateColumns="False" ClientInstanceName="gridNationality" EnableTheming="True" OnInit="gvNationality_Init"
                                            KeyFieldName="Id" Visible="true" Width="100%" OnPageIndexChanged="gvNationality_PageIndexChanged" OnCustomCallback="gvNationality_CustomCallback">
                                            <ClientSideEvents CustomButtonClick="gridNationality_CustomButtonClick" />

                                            <SettingsPopup>
                                                <HeaderFilter MinHeight="140px"></HeaderFilter>
                                            </SettingsPopup>
                                            <SettingsSearchPanel Visible="True" />
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="Code" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <%--<dx:GridViewDataTextColumn Caption="Code" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="1" Visible="false">
                                                </dx:GridViewDataTextColumn>--%>
                                                <dx:GridViewDataTextColumn Caption="Country Name English" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Country Name Amharic" FieldName="Amdescription" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="9" Width="100px">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Select">
                                                        </dx:GridViewCommandColumnCustomButton>
                                                        <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                            <Styles Style-ForeColor="Red">
                                                                <Style ForeColor="Red"></Style>
                                                            </Styles>
                                                        </dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                </dx:GridViewCommandColumn>
                                            </Columns>
                                            <SettingsPager PageSize="6"></SettingsPager>
                                        </dx:ASPxGridView>

                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>


            </div>
        </div>
    </div>
</asp:Content>

