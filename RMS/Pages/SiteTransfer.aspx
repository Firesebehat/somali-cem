﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SiteTransfer.aspx.cs" Inherits="RMS.Pages.SiteTransfer" MasterPageFile="~/Root.master" %>

<%--<%@ Register Assembly="DevExpress.Web.v19.1, Version=19.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>--%>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">
        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", true);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }
        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoAddClientClick();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveClientClick();
            }
            else if (e.item.name === "mnuList") {
                DoListClientClick();
            }
            else if (e.item.name === "mnuDelete") {
                DoDeleteClick();
            }
        }
        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function DoNewClientClick(s, e) {
            clbClient.PerformCallback('New');
        }

        function DoSaveClientClick() {
            if (!ASPxClientEdit.ValidateGroup('Registration')) {
                return;
            }
            if (txtUser.GetText() === '') {
                ShowError('Please select users');
                return;
            }
            clbClient.PerformCallback('Save');
        }

            function DoSearchClientClick(s, e) {
                gvSiteTransfer.PerformCallback('Search');

        }

        function DoCancelClient() {
            //DoListView();
            ToggleEnabled("mnuList", true);
            ToggleEnabled("mnuNew", true);
            pnlData.SetVisible(false);
        }

        function DoAddClientClick(s, e) {
            DoNewClientClick();
        }
        function DoListClientClick(s, e) {
            pnlData.SetVisible(false);
            pnlGrid.SetVisible(true);
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", false);
            DoSearchClientClick();


        }
        function DoDeleteImageClick() {
            ShowPopup('deleteimage');
        }
        var rowVisibleIndex;
        var strID;
      
        function OnClientEndCallback(s, e) {

            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Cancel") {
                    DoListView();
                    return;
                }
                else if (s.cpAction == "New") {
                    DoEditView();
                    return;
                }
                else if (s.cpAction == "Save") {
                    ShowSuccess(s.cpMessage);
                    DoListView();
                    gvSiteTransfer.PerformCallback();
                }
            }
            else if (s.cpStatus === "ERROR") {
                ShowError(s.cpMessage);
                if (s.cpAction == "Delete" || s.cpAction == "Find" || s.cpAction == "List") {
                    return;
                }

                if (s.cpAction == "Save") {
                    DoEditView();
                }

            }
            else
                pnlData.SetVisible(false);
            s.cpStatus = '';
            s.cpMessage = '';
        }

        function DeleteGridRow(visibleIndex) {
            var index = gvSiteTransfer.GetFocusedRowIndex();

            gvSiteTransfer.DeleteRow(index);
        }

        function DoListView() {
            pnlGrid.SetVisible(true);
            pnlData.SetVisible(false);
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", false);
            //alert('e.buttonID'); 
        }
        function DoEditView() {
            pnlData.SetVisible(true);
            ToggleEnabled("mnuSave", true);
            pnlGrid.SetVisible(false);
            ToggleEnabled("mnuNew", false);
            ToggleEnabled("mnuList", true);
        }
        
        function SiteSelected(s) {
            if (s.GetValue() != null) {
                grdUsers.PerformCallback();
                popUsers.Show();
            }
        }
        function UnitSelected(s) {
            cboSite.PerformCallback();
        }
        function NewUnitSelected(s) {
            cboSiteNew.PerformCallback(s.GetValue());
        }
        function popUserCloseup() {
            var selectedCount = grdUsers.GetSelectedRowCount();
            if (selectedCount > 0) {
                grdUsers.GetSelectedFieldValues('UserName', function (data) {
                    var text = '';
                    var x = data;
                    for (i = 0; i < x.length; i++) {
                        text += x[i] + ', ';
                    }
                     text = text.slice(0, text.length - 2);

                    txtUser.SetText(text);
                    popUsers.Hide();
                    if (selectedCount > 1)
                        lblSelectedNo.SetText(selectedCount + ' Users are selected.');
                    else
                        lblSelectedNo.SetText(selectedCount + ' User is selected.');


                });
            }
        }
        function OnRemarkClick(s, e) {

            if (e.buttonID == 'REMARK') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Remark', function (value) {
                    ShowRemark(s, value);
                });
                //ShowError("test");
            }
        }
        function ShowRemark(elt,value) {
            lblRemark.SetText(value);
            popup.Show();
           // popup.ShowAtElement(gvSiteTransfer);
            //keyValue = key;
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            //DoListClientClick();
        };
    </script>
    <style>
        td{
            vertical-align:top;
        }
        .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
    </style>
   
    <div class="contentmain">
        <h5>
            User Site Transfer
        </h5>
        <div class="card card-body">
        <script src="../Content/ModalDialog.js"></script>
        <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
       
                <div style="padding-bottom:15px">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                </div>
      
            <div>

                <dx:ASPxCallbackPanel ID="clbClient" OnCallback="clbClient_Callback" ClientInstanceName="clbClient" runat="server" Width="100%" >

                    <ClientSideEvents EndCallback="function(s,e){OnClientEndCallback(s,e);}" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlCategory" runat="server"  SupportsDisabledAttribute="True">
                            
                            <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" Width="100%" ShowHeader="false">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <table style="width:100%;max-width:700px">
                                            <tr>
                                                
                                                 <td colspan="3">Selected users list</td>
                                                <td></td>

                                            </tr>
                                            <tr>
                                           
                                                <td  colspan="4">
                                                  <dx:ASPxTextBox runat="server" ID="txtUser" ClientInstanceName="txtUser" Width="100%" ClientEnabled="false" NullText="Click the button to select">
                                                      <ValidationSettings ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom" ErrorDisplayMode="Text">
                                                          <RequiredField ErrorText="Users is required" IsRequired="true" />
                                                      </ValidationSettings>
                                                  </dx:ASPxTextBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton runat="server" AutoPostBack="false" CausesValidation="false" ID="btnSearch" Text="Select..">
                                                        <ClientSideEvents Click="function(){popUsers.Show();}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <dx:ASPxLabel runat="server" ID="lblSelectedNo" ClientInstanceName="lblSelectedNo" ForeColor="Orange" ></dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>
                                            </tr>
                                            <tr>
                                                <td>Organization Level To</td>
                                                <td></td>
                                                <td>Taxcenter To</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="width:25%">
                                                     <dx:ASPxComboBox runat="server" ID="cboUnitNew" ClientInstanceName="cboUnitNew" Width="100%" NullText="Select">
                                                       <ClientSideEvents SelectedIndexChanged="NewUnitSelected" />
                                                         <ValidationSettings Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="Registration" ErrorDisplayMode="Text">
                                                            <RequiredField ErrorText="New Unit is Required" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td></td>
                                                <td style="width:25%">
                                                    <dx:ASPxComboBox runat="server" ID="cboSiteNew" ClientInstanceName="cboSiteNew" Width="100%" NullText="Select" OnCallback="cboSiteNew_Callback">
                                                        <ValidationSettings Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="Registration" ErrorDisplayMode="Text">
                                                            <RequiredField ErrorText="New Taxcenter is Required" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="width:50%"></td>
                                                <td></td>
                                            </tr>
                                             <tr>
                                                <td colspan="5">Remark</td>
                                                 </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <dx:ASPxMemo runat="server" ID="txtRemark" ClientInstanceName="txtRemark" Width="100%" NullText="Enter Remark"></dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                     


                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>


                            <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="100%" ClientVisible="false">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxGridView ID="gvSiteTransfer" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvSiteTransfer" EnableTheming="True" OnInit="gvSiteTransfer_Init"
                                            KeyFieldName="Code" Visible="true" Width="100%" OnPageIndexChanged="gvSiteTransfer_PageIndexChanged" OnCustomCallback="gvSiteTransfer_CustomCallback">
                                            <ClientSideEvents CustomButtonClick="function(s,e){OnRemarkClick(s,e);}" />
                                            <SettingsPopup>
                                                <HeaderFilter MinHeight="140px"></HeaderFilter>
                                            </SettingsPopup>
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="UserName" ShowInCustomizationForm="True" Caption="UserName" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Old Site Name" FieldName="SiteOldName" ShowInCustomizationForm="True" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="New Site Name" FieldName="SiteNewName" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Transfered By" FieldName="TransferedByName" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Transfered Date" FieldName="TransferedDate" ShowInCustomizationForm="True" VisibleIndex="4">
                                                </dx:GridViewDataTextColumn>
                                                

                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="5" Width="100px">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton ID="REMARK" Text="Remark..">
                                                        </dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                    
                                                </dx:GridViewCommandColumn>
                                                
                                                <dx:GridViewDataTextColumn  FieldName="Remark" VisibleIndex="6" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <SettingsPager PageSize="7"></SettingsPager>
                                        </dx:ASPxGridView>
                                        <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" AllowDragging="true"
                                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="Remark" PopupHorizontalOffset="500">
                                            <HeaderStyle BackColor="#009688" ForeColor="White" />
                                            <ContentCollection>
                                                <dx:PopupControlContentControl runat="server">
                                                    <dx:ASPxLabel runat="server" ID="lblRemark" Font-Underline="true" Font-Size="Larger" Font-Italic="true" ClientInstanceName="lblRemark"></dx:ASPxLabel>
                                                </dx:PopupControlContentControl>
                                            </ContentCollection>
                                            <SettingsAdaptivity Mode="OnWindowInnerWidth" SwitchAtWindowInnerWidth="800" />
                                        </dx:ASPxPopupControl>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
               <dx:ASPxPopupControl runat="server" ID="popUsers" ClientInstanceName="popUsers" Width="700px" Height="500px" CloseAction="CloseButton" PopupVerticalAlign="TopSides" PopupHorizontalAlign="WindowCenter"
                   HeaderText="Select Users" PopupAnimationType="Fade" EnableCallbackAnimation="true">
                   <HeaderStyle BackColor="#009688" ForeColor="White" />
                  <ClientSideEvents CloseUp="popUserCloseup" />
                   <ContentCollection>
                       <dx:PopupControlContentControl>
                           <div style="width:100%;padding:5px;max-height:450px; overflow-y:auto">
                             <dx:ASPxGridView ID="grdUsers" KeyFieldName="UserName" ClientInstanceName="grdUsers" OnDataBinding="grdUsers_DataBinding" OnPageIndexChanged="grdUsers_PageIndexChanged"  runat="server"
                                                        AutoGenerateColumns="False" OnCustomCallback="grdUsers_CustomCallback" Width="100%">
                                                        <Settings ShowFilterRow="True" />
                                                        <SettingsPager PageSize="5"></SettingsPager>
                                                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                                        <SettingsPopup>
                                                            <HeaderFilter MinHeight="140px"></HeaderFilter>
                                                        </SettingsPopup>
                                                        <Columns>

                                                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0">
                                                            </dx:GridViewCommandColumn>
                                                            <dx:GridViewDataTextColumn Caption="User Name" FieldName="UserName" ShowInCustomizationForm="True" VisibleIndex="1">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Full Name" FieldName="FullName" ShowInCustomizationForm="True" VisibleIndex="2">
                                                            </dx:GridViewDataTextColumn>
                                                             <dx:GridViewDataTextColumn Caption="Current Tax Center" FieldName="TaxCenterName" ShowInCustomizationForm="True" VisibleIndex="2">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn  Visible="false" FieldName="TaxCenterId" ShowInCustomizationForm="True" VisibleIndex="4">
                                                             </dx:GridViewDataTextColumn>

                                                        </Columns>
                                                    </dx:ASPxGridView>
                            </div>
                           <dx:ASPxButton runat="server" Text="Select" AutoPostBack="false" CausesValidation="false" style="float:right;margin-right:10px">
                               <ClientSideEvents Click="popUserCloseup" />
                           </dx:ASPxButton>
                       </dx:PopupControlContentControl>
                   </ContentCollection>
               </dx:ASPxPopupControl>



              

            </div>
        </div>
    </div>
</asp:Content>

