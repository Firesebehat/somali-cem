﻿using CUSTOR.Common;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using CUSTOR.Domain;
using CUSTOR.Business;
using Business.Taxpayer;
using Domain.Taxpayer;

namespace RMS.Pages
{
    public partial class AdvertismentType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                pnlData.ClientVisible = false;
            }
            DoFind();
        }
        protected void DoNew()
        {
            Session["PageMode"] = "NEW";
            txtAdveritsmentType.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtAmountAllPages.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtAmhAdveritsmentType.Text = string.Empty;
            txtAdveritsmentType.IsValid = true;
            txtAmhAdveritsmentType.IsValid = true;
        }

        protected void Delete(int AdvertismentTypeid)
        {
            AdvertismentTypeBussiness objAdvertismentTypeBussiness = new AdvertismentTypeBussiness();
            if (objAdvertismentTypeBussiness.Delete(AdvertismentTypeid))
            {
                DoFind();
                clbAdvertismentType.JSProperties["cpStatus"] = "INFO";
                pnlData.ClientVisible = false;
                ShowMessage("Selected Advertisment type deleted successfully!!!");
            }
        }


        protected void LoadAllAdvertismentTypeDomains()
        {
            AdvertismentTypeBussiness objAdvertismentTypeBussiness = new AdvertismentTypeBussiness();
            List<AdvertismentTypeDomain> AdvertismentTypeDomaintList = objAdvertismentTypeBussiness.GetAdvertismentTypes();
            if (AdvertismentTypeDomaintList != null)
            {
                gvwAdvertismentType.DataSource = AdvertismentTypeDomaintList;
                gvwAdvertismentType.DataBind();
                gvwAdvertismentType.ClientVisible = true;
            }
            else
            {
                gvwAdvertismentType.ClientVisible = false;
            }
        }

        protected void LoadAdvertismentTypeDomainInfo(int AdvertismentTypeid)
        {
            clbAdvertismentType.JSProperties["cpAction"] = "Loaded";
            ASPxEdit.ClearEditorsInContainer(pnlData);
            AdvertismentTypeBussiness objAdvertismentTypeBussiness = new AdvertismentTypeBussiness();
            AdvertismentTypeDomain objAdvertismentTypeDomain = objAdvertismentTypeBussiness.GetAdvertismentType(AdvertismentTypeid);
            if (objAdvertismentTypeDomain != null)
            {
                ValidControlTrue();
                Session["AdvertismentTypeDomainId"] = objAdvertismentTypeDomain.Id;
                txtAdveritsmentType.Text = objAdvertismentTypeDomain.AdveritsmentType;
                txtAmount.Text = objAdvertismentTypeDomain.Amount.ToString();
                txtAmountAllPages.Text = objAdvertismentTypeDomain.AmountAllPages.ToString();
                txtAmhAdveritsmentType.Text = objAdvertismentTypeDomain.AmAdveritsmentType;
                txtCode.Text = objAdvertismentTypeDomain.Code.ToString();
                clbAdvertismentType.JSProperties["cpStatus"] = "SUCCESS";
            }
        }

        private void ValidControlTrue()
        {
            txtAmhAdveritsmentType.IsValid = true;
            txtAdveritsmentType.IsValid = true;
            txtAmount.IsValid = true;
            txtAmountAllPages.IsValid = true;
            txtCode.IsValid = true;
        }

        protected void SaveEdit()
        {
            clbAdvertismentType.JSProperties["cpAction"] = "Save";

            int AdvertismentTypeDomainId = Convert.ToInt32(Session["AdvertismentTypeDomainId"]);
            AdvertismentTypeBussiness objAdvertismentTypeBussiness = new AdvertismentTypeBussiness();

            AdvertismentTypeDomain objAdvertismentTypeDomain = new AdvertismentTypeDomain();
            objAdvertismentTypeDomain.Id = AdvertismentTypeDomainId;
            objAdvertismentTypeDomain.AdveritsmentType = txtAdveritsmentType.Text;
            objAdvertismentTypeDomain.AmAdveritsmentType = txtAmhAdveritsmentType.Text;
            objAdvertismentTypeDomain.Amount = Convert.ToDouble(txtAmount.Text);
            objAdvertismentTypeDomain.AmountAllPages = Convert.ToDouble(txtAmountAllPages.Text);
            objAdvertismentTypeDomain.Code = Convert.ToInt16(txtCode.Text);
            //if (objAdvertismentTypeBussiness.Exists(objAdvertismentTypeDomain.Id))
            //{
            //    ShowError("Lookup name already exist");
            //    return;
            //}

            if (objAdvertismentTypeBussiness.UpdateAdvertismentType(objAdvertismentTypeDomain))
            {                
                LoadAllAdvertismentTypeDomains();
                clbAdvertismentType.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }
        }

        protected void SaveNew()
        {

            AdvertismentTypeBussiness objAdvertismentTypeBussiness = new AdvertismentTypeBussiness();
            AdvertismentTypeDomain objAdvertismentTypeDomain = new AdvertismentTypeDomain();
            objAdvertismentTypeDomain.AdveritsmentType = txtAdveritsmentType.Text;
            objAdvertismentTypeDomain.AmAdveritsmentType = txtAmhAdveritsmentType.Text;
            objAdvertismentTypeDomain.Amount = Convert.ToDouble(txtAmount.Text);
            objAdvertismentTypeDomain.AmountAllPages = Convert.ToDouble(txtAmountAllPages.Text);
            objAdvertismentTypeDomain.Code = Convert.ToInt16(txtCode.Text);
            //if (objAdvertismentTypeBussiness.Exists(objAdvertismentTypeDomain.Id))
            //{
            //    ShowError("Lookup name already exist");
            //    return;
            //}
            bool registeredLookup = objAdvertismentTypeBussiness.InsertAdvertismentType(objAdvertismentTypeDomain);
            if (registeredLookup)
            {
                // DoNew();
                //LoadSavedAdvertismentTypeDomain(registeredLookup);
                LoadAllAdvertismentTypeDomains();
                clbAdvertismentType.JSProperties["cpStatus"] = "SUCCESS";
                clbAdvertismentType.JSProperties["cpAction"] = "Save";
                ShowSuccess("Lookup type saved successfully");
            }
        }

        private void ShowError(string strMsg)
        {
            clbAdvertismentType.JSProperties["cpMessage"] = strMsg;
            clbAdvertismentType.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowMessage(string strMsg)
        {
            clbAdvertismentType.JSProperties["cpMessage"] = strMsg;
            clbAdvertismentType.JSProperties["cpStatus"] = "INFO";
        }

        private void ShowSuccess(string strMsg)
        {
            clbAdvertismentType.JSProperties["cpMessage"] = strMsg;
            clbAdvertismentType.JSProperties["cpStatus"] = "SUCCESS";
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        protected void Saved()
        {
            Page.Validate();
            if (!ASPxEdit.ValidateEditorsInContainer(clbAdvertismentType))
            {
                gvwAdvertismentType.ClientVisible = false;
                pnlData.ClientVisible = true;
                return;
            }
            clbAdvertismentType.JSProperties["cpAction"] = "Save";


            switch (Session["PageMode"].ToString())
            {
                case "NEW":
                    SaveNew();
                    break;
                case "EDIT":
                    SaveEdit();
                    break;
            }
        }

        protected void clbAdvertismentType_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            clbAdvertismentType.JSProperties["cpStatus"] = "";
            clbAdvertismentType.JSProperties["cpAction"] = "";
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    clbAdvertismentType.JSProperties["cpAction"] = "New";
                    DoNew();
                    clbAdvertismentType.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Save":
                    Saved();
                    break;
                case "EDITING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        LoadAdvertismentTypeDomainInfo(Convert.ToInt32(strID));
                        Session["PageMode"] = "EDIT";
                        //gvwAdvertismentType.Visible = true;
                    }
                    break;
                case "DELETING":
                    //ReportToolbar1.Visible = ReportViewer1.Visible = false;
                    if (int.Parse(strID) > 0)
                    {
                        Session["ID"] = strID;
                        Delete(Convert.ToInt32(Session["ID"]));
                    }
                    break;
                case "Report":
                    Session["PrintReport"] = false;
                    //GenerateReport();
                    break;
                case "Cancel":
                    clbAdvertismentType.JSProperties["cpAction"] = "Cancel";
                    clbAdvertismentType.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                case "Find":
                    DoFind();
                    pnlData.ClientVisible = false;
                    break;
                default:
                    break;
            }
        }

        private void DoFind()
        {
            clbAdvertismentType.JSProperties["cpAction"] = "Show";
            AdvertismentTypeBussiness objLookupBus = new AdvertismentTypeBussiness();
            try
            {
                List<AdvertismentTypeDomain> list = objLookupBus.GetAdvertismentTypes();
                if (list.Count > 0)
                {
                    gvwAdvertismentType.DataSource = list;
                    gvwAdvertismentType.DataBind();
                    gvwAdvertismentType.ClientVisible = true;
                    ShowSuccess("");
                }
                else
                {
                    gvwAdvertismentType.ClientVisible = false;
                    ShowMessage("No data found!");
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        protected void gvwAdvertismentType_PageIndexChanged(object sender, EventArgs e)
        {
            LoadAllAdvertismentTypeDomains();
        }
        private void FillLanguage(ASPxComboBox combo)
        {
            try
            {
                string qtext = "select * from NewsLanguage where IsActive = '1' AND LangId = 0";
                CDropdown.FillComboFromDB(combo, qtext, "Description", "Id");
            }
            catch
            {
            }
        }
        protected void gvwAdvertismentType_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
                LoadAllAdvertismentTypeDomains();
        }
    }
}