﻿using CUSTOR.Business;
using CUSTOR.Common;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class Woreda : System.Web.UI.Page
    {
        const string ZoneCode = "80";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !Page.IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                FillRegion(cboRegion);
                FillRegion(cboRegionFind);
                Session["NewWoreda"] = true;
                cboRegion.Value = cboRegionFind.Value = "14";
                FillZone(cboRegion.Value.ToString(), cboZone);
                FillZone(cboRegion.Value.ToString(), cboZoneFind);
                cboZone.Value =  cboZoneFind.Value = ZoneCode;
                pnlData.ClientVisible = false;

            }
            BindGrid();
        }
        private void FillCombo(DevExpress.Web.ASPxComboBox ddl, Language.eLanguage eLang, Type t)
        {
            ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            ddl.ValueField = "Key";
            ddl.TextField = "Value";
            ddl.DataBind();
        }

        protected void FillRegion(DevExpress.Web.ASPxComboBox cboRegion)
        {
            CDropdown.FillComboFromDB(cboRegion, "SELECT Code,Description FROM Region", "Description", "Code");
        }
        protected void FillZone(string region, ASPxComboBox cboZone)
        {
            CDropdown.FillComboFromDB(cboZone, String.Format("SELECT Code,Description FROM Zone WHERE Parent='{0}'", region), "Description", "Code");
        }
        protected void cboZone_Callback(object source, CallbackEventArgsBase e)
        {
            //if (e.Parameter.ToString() != "-1")
                FillZone(e.Parameter, cboZone);
        }
        protected void cboZoneFind_Callback(object source, CallbackEventArgsBase e)
        {
            //if (e.Parameter.ToString() != "-1")
                FillZone(e.Parameter, cboZoneFind);
        }

        protected void ClbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            RestorValues();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Search":
                    BindGrid();
                    clbClient.JSProperties["cpAction"] = "List";
                    ShowSuccess(string.Empty);
                    break;
                case "Edit":
                    DoFind(strID);
                    break;
                case "Delete":
                    DoDelete(strID);
                    break;
                case "Cancel":
                    DoNew();
                    clbClient.JSProperties["cpAction"] = "Cancel";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                default:
                    break;
            }
        }

        protected bool DoFind(string ID)
        {
            CUSTOR.Business.WoredaBusiness objWoredaBusiness = new CUSTOR.Business.WoredaBusiness();

            CUSTOR.Domain.Woreda objWoreda = new CUSTOR.Domain.Woreda();
            try
            {
                objWoreda = objWoredaBusiness.GetWoreda(ID);
                if (objWoreda == null)
                {
                    ShowMessage("No Record is found.");
                    return false;
                }
                //Now Record is found;
                Session["NewWoreda"] = false;
                ClearForm();
                txtAmharic.Text = objWoreda.AmDescription;
                txtEnglish.Text = objWoreda.Description;
                Session["ID"] = ID;
                clbClient.JSProperties["cpAction"] = "Find";
                ShowSuccess(string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Find");
                return false;
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
            clbClient.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        protected bool DoSave()
        {
            CUSTOR.Domain.Woreda objWoreda = new CUSTOR.Domain.Woreda();
            clbClient.JSProperties["cpAction"] = "Save";
            try
            {

                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {
                    ShowError(GetErrorDisplay(errMessages), "Save");
                    return false;
                }

                CVGeez objGeez = new CVGeez();

                objWoreda.AmDescription = txtAmharic.Text;
                objWoreda.AmDescriptionSort = objGeez.GetSortValueU(txtAmharic.Text);
                objWoreda.Description = txtEnglish.Text;
                objWoreda.Parent = cboZone.Value.ToString();

                WoredaBusiness objWoredaBusiness = new WoredaBusiness();
                bool bNew = ((bool)Session["NewWoreda"]);

                if (bNew)
                {
                    if (!objWoredaBusiness.Exists(objWoreda.Description, objWoreda.AmDescriptionSort, objWoreda.Parent))
                    {
                        objWoreda.Code = objWoredaBusiness.GetNextCode();
                        objWoredaBusiness.InsertWoreda(objWoreda);
                    }
                    else
                    {
                        ShowError("Woreda name exists. Try another name");
                        return false;
                    }
                }
                else
                {
                    objWoreda.Code = Session["ID"].ToString();
                    if (!objWoredaBusiness.Exists(objWoreda.Description, objWoreda.AmDescriptionSort, objWoreda.Code, objWoreda.Parent))
                    {

                        objWoredaBusiness.UpdateWoreda(objWoreda);
                    }
                    else
                    {
                        ShowError("Woreda name exists. Try another name");
                        return false;
                    }

                }

                Session["NewWoreda"] = true;
                ShowSuccess("Record is saved successfully.");
                ClearForm();
                BindGrid();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Save");
                return false;
            }
        }
        protected void ClearForm()
        {

            txtAmharic.Text = "";
            txtEnglish.Text = "";
            txtAmharic.IsValid = true;
            txtEnglish.IsValid = true;
            cboRegion.IsValid = true;
            cboZone.IsValid = true;

        }
        protected bool DoNew()
        {
            try
            {
                Session["NewWoreda"] = true;
                ClearForm();
                clbClient.JSProperties["cpAction"] = "New";
                cboRegion.Value = cboRegionFind.Value;
                if (cboRegion.Value != null)
                    FillZone(cboRegion.Value.ToString(), cboZone);
                cboZone.Value = cboZoneFind.Value;
                ShowSuccess(string.Empty);

                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        protected bool DoDelete(string intID)
        {
            Woreda objWoreda = new Woreda();
            WoredaBusiness objWoredaBusiness = new WoredaBusiness();
            try
            {
                objWoredaBusiness.Delete(intID);
                BindGrid();
                clbClient.JSProperties["cpAction"] = "Delete";
                ShowSuccess("Record is deleted successfully.");
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Delete");
                return false;
            }
        }


        private void BindGrid()
        {
            try
            {
                if (cboZoneFind.Value == null) return;
                WoredaBusiness objWoredaBusiness = new WoredaBusiness();
                List<CUSTOR.Domain.Woreda> l = new List<CUSTOR.Domain.Woreda>();
                string zoneCode = cboZoneFind.Value.ToString();
                l = objWoredaBusiness.GetWoredas(zoneCode);
                if (l != null && l.Count > 0)
                {
                    gvWoreda.DataSource = l;
                    gvWoreda.DataBind();
                    gvWoreda.JSProperties["cpStatus"] = "SUCCESS";
                }
                else
                {
                    gvWoreda.DataSource = null;
                    gvWoreda.DataBind();
                    gvWoreda.JSProperties["cpStatus"] = "NoRecord";


                }
            }
            catch (Exception ex)
            {
                gvWoreda.DataSource = null;
                gvWoreda.DataBind();
                ShowError(ex.Message, "List");
            }
        }
        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();
            if (txtEnglish.Text == string.Empty &&
               txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter Woreda description!");
            }
            if (txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter Woreda description in Amharic!");
            }
            if (txtEnglish.Text == string.Empty)
            {
                errMessages.Add("Enter Woreda description in English");
            }
            if (cboRegion.Text == string.Empty)
            {
                errMessages.Add("Select the Parent Region");
            }
            if (cboZone.Value.ToString() == "-1")
            {
                errMessages.Add("Select the Parent Zone");
            }
            WoredaBusiness objWoreda = new WoredaBusiness();
            bool bNew = (bool)Session["NewWoreda"];
            return errMessages;
        }

        protected void gvWoreda_PageIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void Grd_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            gvWoreda.JSProperties["cpAction"] = "List";
            BindGrid();
        }

        protected void gvWoreda_Init(object sender, EventArgs e)
        {
            if(Page.IsCallback)
            {
                BindGrid();
            }
            
        }
        protected void RestorValues()
        {
            if (cboZone.Items.Count == 0 && cboRegion.Value != null)
            {
                FillZone(cboRegion.Value.ToString(), cboZone);
            }

            if (cboZoneFind.Items.Count == 0 && cboRegionFind.Value != null)
            {
                FillZone(cboRegionFind.Value.ToString(), cboZoneFind);
            }
            
        }
    }
}