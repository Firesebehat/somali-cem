﻿using CUSTOR.Business;
using CUSTOR.Common;
using CUSTORCommon.Enumerations;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Pages
{
    public partial class Zone : System.Web.UI.Page
    {
        const string RegionCode = "14";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                Session["NewZone"] = true;
                FillRegion(cboRegion);
                FillRegion(cboRegionFind);
                //cboRegion.Value = cboRegionFind.Value = RegionCode;
                gvZone.ClientVisible = false;
                pnlData.ClientVisible = false;
            }
            BindGridByRegion();
        }
        protected void FillRegion(DevExpress.Web.ASPxComboBox cboRegion)
        {
            CDropdown.FillComboFromDB(cboRegion, "SELECT Code,Description FROM Region", "Description", "Code");
            cboRegion.Items.Insert(0, new ListEditItem("-", "-1"));
            cboRegion.SelectedIndex = 0;
        }

        protected void ClbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Search":
                    BindGridByRegion();
                    clbClient.JSProperties["cpAction"] = "List";
                    ShowSuccess(string.Empty);
                    break;
                case "Edit":
                    DoFind(strID);
                    break;
                case "Delete":
                    DoDelete(strID);
                    break;
                case "Cancel":
                    DoNew();
                    clbClient.JSProperties["cpAction"] = "Cancel";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                default:
                    break;
            }
        }

        protected bool DoFind(string ID)
        {
            CUSTOR.Business.ZoneBusiness objZoneBusiness = new CUSTOR.Business.ZoneBusiness();
            CUSTOR.Domain.Zone objZone = new CUSTOR.Domain.Zone();

            try
            {
                objZone = objZoneBusiness.GetZone(ID);
                if (objZone == null)
                {
                    ShowMessage("No record was found.");
                    return false;
                }
                Session["ID"] = ID;
                Session["NewZone"] = false;
                ClearForm();

                txtAmharic.Text = objZone.AmDescription;
                txtEnglish.Text = objZone.Description;
                cboRegion.Value = objZone.Parent;
                clbClient.JSProperties["cpAction"] = "Find";
                ShowSuccess(string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Find");
                return false;
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
            clbClient.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        protected bool DoSave()
        {
            CUSTOR.Domain.Zone objZone = new CUSTOR.Domain.Zone();

            try
            {
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {

                    ShowError(GetErrorDisplay(errMessages), "Save");
                    return false;
                }

                CVGeez objGeez = new CVGeez();

                // objZone.Code = txtCode.Text;
                objZone.AmDescription = txtAmharic.Text;
                objZone.AmDescriptionSort = objGeez.GetSortValueU(txtAmharic.Text);
                objZone.Description = txtEnglish.Text;

                objZone.Parent = cboRegion.Value.ToString();



                ZoneBusiness objZoneBusiness = new ZoneBusiness();

                bool bNew = ((bool)Session["NewZone"]);

                if (bNew)
                {
                    if (!objZoneBusiness.Exists(objZone.Description, objZone.AmDescriptionSort, objZone.Parent))
                    {
                        objZone.Code = objZoneBusiness.GetNextCode(); 
                        objZoneBusiness.InsertZone(objZone);
                    }
                    else
                    {
                        ShowError("Zone name exists. Try another name");
                        return false;
                    }
                }
                else
                {

                    objZone.Code = Session["ID"].ToString();
                    if (!objZoneBusiness.Exists(objZone.Description, objZone.AmDescriptionSort, objZone.Code, objZone.Parent))
                    {
                        objZoneBusiness.UpdateZone(objZone);
                    }
                    else
                    {
                        ShowError("Zone name exists. Try another name");
                        return false;
                    }
                }
                Session["NewZone"] = true;

                //BindGrid();
                clbClient.JSProperties["cpAction"] = "Save";
                ShowSuccess("Record was saved successfully.");
                ClearForm();
                BindGridByRegion();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Save");
                return false;
            }
        }
        protected void ClearForm()
        {
            if (cboRegionFind.Value != null)
            {
                cboRegion.Value = cboRegionFind.Value;
            }
            txtAmharic.Text = "";
            txtEnglish.Text = "";
            txtAmharic.IsValid = true;
            txtEnglish.IsValid = true;
            cboRegion.IsValid = true;

        }
        protected bool DoNew()
        {
            try
            {
                Session["NewZone"] = true;
                ClearForm();
                clbClient.JSProperties["cpAction"] = "new";
                ShowSuccess(string.Empty);

                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        protected bool DoDelete(string intID)
        {
            CUSTOR.Domain.Zone objZone = new CUSTOR.Domain.Zone();
            ZoneBusiness objZoneBusiness = new ZoneBusiness();
            try
            {
                objZoneBusiness.Delete(intID);
                Session["NewZone"] = false;

                BindGridByRegion();
                clbClient.JSProperties["cpAction"] = "Delete";
                ShowSuccess("Record was deleted successfully.");
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Delete");
                return false;
            }
        }

      
        private void BindGridByRegion()
        {
            try
            {

                ZoneBusiness objZoneBusiness = new ZoneBusiness();
                List<CUSTOR.Domain.Zone> l = new List<CUSTOR.Domain.Zone>();
                l = objZoneBusiness.GetZonesByRegion(cboRegionFind.Value.ToString());
                if (l != null && l.Count > 0)
                {
                    gvZone.DataSource = l;
                    gvZone.DataBind();
                    gvZone.JSProperties["cpStatus"] = "SUCCESS";
                }
                else
                {

                    gvZone.DataSource = null;
                    gvZone.DataBind();
                    gvZone.JSProperties["cpStatus"] = "NO DATA";

                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "List");
            }
        }
        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");
            return sb.ToString();
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();


            if (txtEnglish.Text == string.Empty &&
               txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter CUSTOR.Domain. Zone description!");
            }
            if (txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter CUSTOR.Domain. Zone description in Amharic!");
            }
            if (txtEnglish.Text == string.Empty)
            {
                errMessages.Add("Enter CUSTOR.Domain. Zone description in English");
            }
            if (cboRegion.Text == string.Empty)
            {
                errMessages.Add("Select the Parent Region");
            }
            return errMessages;
        }

        protected void gvZone_PageIndexChanged(object sender, EventArgs e)
        {
            BindGridByRegion();
        }
        protected void Grd_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            gvZone.JSProperties["cpAction"] = "List";
            BindGridByRegion();
        }
    }
}