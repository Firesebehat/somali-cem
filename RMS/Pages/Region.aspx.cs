﻿using CUSTOR.Business;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class Region : System.Web.UI.Page
    {
        public int Lan { get { return 0; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                Session["NewRegion"] = true;
                pnlData.ClientVisible = false;

            }
        }
        private void FillCombo(DevExpress.Web.ASPxComboBox ddl, Language.eLanguage eLang, Type t)
        {
            ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            ddl.ValueField = "Key";
            ddl.TextField = "Value";
            ddl.DataBind();
        }

        protected void ClbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Search":
                    BindGrid();
                    clbClient.JSProperties["cpAction"] = "List";
                    ShowSuccess(string.Empty);
                    break;
                case "Edit":
                    DoFind(Convert.ToInt32(strID));
                    break;
                case "Delete":
                    DoDelete(Convert.ToInt32(strID));
                    break;
                case "Cancel":
                    DoNew();
                    clbClient.JSProperties["cpAction"] = "Cancel";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                default:
                    break;
            }
        }

        protected bool DoFind(int ID)
        {
            RegionBusiness objRegionBusiness = new RegionBusiness();

            CUSTOR.Domain.Region objRegion = new CUSTOR.Domain.Region();

            try
            {
                objRegion = objRegionBusiness.GetRegion(ID);
                if (objRegion == null)
                {
                    ShowMessage("No Record is found.");
                    return false;
                }
                //Now Record is found;
                Session["NewRegion"] = false;
                ClearForm();
                pnlData.ClientVisible = true;
                pnlGrid.ClientVisible = false;
                txtAmharic.Text = objRegion.AmDescription;
                txtEnglish.Text = objRegion.Description;


                Session["ID"] = ID;
                clbClient.JSProperties["cpAction"] = "Find";
                ShowSuccess(string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Find");
                return false;
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
            clbClient.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        protected bool DoSave()
        {
            CUSTOR.Domain.Region objRegion = new CUSTOR.Domain.Region();

            try
            {
                //Page.Validate();
                if ((!Page.IsValid)) return false;

                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {

                    ShowError(GetErrorDisplay(errMessages), "Save");
                    return false;
                }
                clbClient.JSProperties["cpAction"] = "Save";
                CVGeez objGeez = new CVGeez();

                objRegion.AmDescription = txtAmharic.Text;
                objRegion.Description = txtEnglish.Text;
                objRegion.AmDescriptionSort = objGeez.GetSortValueU(txtAmharic.Text);
                RegionBusiness objRegionBusiness = new RegionBusiness();
                bool bNew = ((bool)Session["NewRegion"]);

                if (bNew)
                {
                    objRegion.Code = objRegionBusiness.GeNextCode();
                    if (!objRegionBusiness.Exists(txtEnglish.Text, objGeez.GetSortValueU(txtAmharic.Text)))
                    {
                        objRegionBusiness.InsertRegion(objRegion);
                    }
                    else
                    {
                        ShowError("Record exists with the same name. Try another name!", "Save");
                        return false;
                    }
                }
                else
                {
                    objRegion.Code = Session["ID"].ToString();
                    if (!objRegionBusiness.Exists(txtEnglish.Text, objGeez.GetSortValueU(txtAmharic.Text), objRegion.Code))
                    {
                        objRegionBusiness.UpdateRegion(objRegion);
                    }
                    else
                    {
                        ShowError("Record exists with the same name. Try another name!", "Save");
                        return false;
                    }
                }
                Session["NewRegion"] = false;
                clbClient.JSProperties["cpAction"] = "Save";
                ShowSuccess("Record is saved successfully.");
                BindGrid();
                ClearForm();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Save");
                return false;
            }
        }
        protected void ClearForm()
        {

            txtAmharic.Text = "";
            txtEnglish.Text = "";
            txtAmharic.IsValid = true;
            txtEnglish.IsValid = true;
        }
        protected bool DoNew()
        {
            try
            {
                Session["NewRegion"] = true;
                pnlData.ClientVisible = true;
                pnlGrid.ClientVisible = false;
                ClearForm();
                clbClient.JSProperties["cpAction"] = "New";
                ShowSuccess(string.Empty);

                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        protected bool DoDelete(int intID)
        {
            Region objRegion = new Region();
            RegionBusiness objRegionBusiness = new RegionBusiness();
            try
            {
                objRegionBusiness.Delete(intID);
                Session["NewRegion"] = false;

                BindGrid();
                clbClient.JSProperties["cpAction"] = "Delete";
                ShowSuccess("Record is deleted successfully.");
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Delete");
                return false;
            }
        }

        private void BindGrid()
        {
            try
            {

                RegionBusiness objRegionBusiness = new RegionBusiness();
                List<CUSTOR.Domain.Region> l = new List<CUSTOR.Domain.Region>();
                l = objRegionBusiness.GetRegions();
                if (l != null && l.Count > 0)
                {
                    gvRegion.DataSource = l;
                    gvRegion.DataBind();
                   
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "List");
            }
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();


            if (txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter region description in Amharic!");
            }
            if (txtEnglish.Text == string.Empty)
            {
                errMessages.Add("Enter region description in English");
            }
            RegionBusiness objReg = new RegionBusiness();
            bool bNew = (bool)Session["NewRegion"];


            return errMessages;
        }

        protected void gvRegion_PageIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void gvRegion_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            //BindGrid();
        }

        protected void gvRegion_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
                BindGrid();
        }
    }
}
    