﻿using System;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using CUSTOR.Common;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;
using System.Web.Security;

namespace RMS.Pages.SettingsList
{
    public partial class Rental : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
            {
                Response.Redirect("~/AccessDenied");
            }
            CDropdown.FillComboFromEnum(cboGrade, (Language.eLanguage.eEnglish), typeof(Enums.eGrade));
        }
        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date
            if (cdpRentalSettingFrom.SelectedDate.Trim().Length == 0)
            {
                errMessages.Add("Please enter rental profit settings starting date!");
            }
            else
            {
                if (!cdpRentalSettingFrom.IsValidDate)
                    errMessages.Add("Please enter a valid rental profit settings starting date!");
                if (cdpRentalSettingFrom.IsFutureDate)
                    errMessages.Add("Starting date of rental profit settings should not upcoming date. Please try again!");
            }

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        protected bool DoValidate()
        {
            bool isValid = true;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                isValid = false;
                clbRentalSettings.JSProperties["cpAction"] = "registration";
                clbRentalSettings.JSProperties["cpStatus"] = "error";
                clbRentalSettings.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }


        protected List<Setting> AssignRentalSettingsToObject()
        {
            Dictionary<string, string> rentalSettingsList = new Dictionary<string, string>();
            if(cboGrade.Text == EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, (int)Enums.eGrade.A))
            {
                rentalSettingsList.Add("RentalTaxFromIncomePercentA", txtRentalTaxRate.Text);
            }
            if (cboGrade.Text == EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, (int)Enums.eGrade.B))
            {
                rentalSettingsList.Add("RentalTaxFromIncomePercentB", txtRentalTaxRate.Text);
            }
            if (cboGrade.Text == EnumHelper.GetEnumDescription(typeof(Enums.eGrade), Language.eLanguage.eEnglish, (int)Enums.eGrade.C))
            {
                rentalSettingsList.Add("RentalTaxFromIncomePercentC", txtRentalTaxRate.Text);
            }
            List<Setting> objSettingsList = new List<Setting>();
            foreach (KeyValuePair<string, string> rentalSetting in rentalSettingsList)
            {
                Setting objSettings = new Setting();
                objSettings.AppId = "SIRIM-AddsiAbeba";
                objSettings.Key = rentalSetting.Key;
                objSettings.Value = rentalSetting.Value;
                objSettings.DateFrom = cdpRentalSettingFrom.SelectedGCDate;
                if (cdpRentalSettingTo.SelectedDate != "")
                {
                    objSettings.DateTo = cdpRentalSettingTo.SelectedGCDate;
                }
                else
                {
                    objSettings.DateTo = Convert.ToDateTime("1/1/1753");
                }
                objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.Rental);
                objSettings.CenterId = 0;
                objSettings.CenterName = "";
                objSettingsList.Add(objSettings);
            }

            return objSettingsList;
        }

        protected void DoSaveRentalSetting()
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            Setting objSetting = new Setting();
            List<Setting> objRentalSettings = AssignRentalSettingsToObject();
            int savedRentalSettingsCount = 0;
            string message = "";
            if (!(bool)hfRentalSettingStatus["isNewRentalSetting"])
            {
                List<int> rentalSettingsIdCollection = (List<int>)Session["RentalSettingIdCollection"];
                for (int index = 0; index < objRentalSettings.Count; index++)
                {
                    objRentalSettings[index].UpdatedBy = "";
                    objRentalSettings[index].UpdatedDate = DateTime.Now;
                    objRentalSettings[index].UpdatedUserId = "";
                    objRentalSettings[index].Id = rentalSettingsIdCollection[index];
                    if (objSettingsBusiness.UpdateByKey(objRentalSettings[index]))
                    {
                        savedRentalSettingsCount++;
                    }
                }
                message = "Rental profit settings updated successfully";
            }
            else
            {
                //number of keys that are under rental settings are 3
                List<int> objLatestRentalSettingsId = objSettingsBusiness.GetLatestSettingsId(Convert.ToInt32(SettingCategory.eSettingCategory.Rental), 3);
                for (int index = 0; index < objRentalSettings.Count; index++)
                {
                    objRentalSettings[index].CreatedBy = "";
                    objRentalSettings[index].CreatedDate = DateTime.Now;
                    objRentalSettings[index].CreatedUserId = "";
                    if (objSettingsBusiness.InsertSettings(objRentalSettings[index]))
                    {
                        savedRentalSettingsCount++;
                    }
                }
                if (objLatestRentalSettingsId != null)
                {
                    for (int index = 0; index < objLatestRentalSettingsId.Count; index++)
                    {
                        objSetting.Id = objLatestRentalSettingsId[index];
                        objSetting.DateTo = cdpRentalSettingFrom.SelectedGCDate;
                        objSettingsBusiness.UpdateEndingDateofKey(objSetting);
                    }
                }
                message = "Rental profit settings registered successfully";
            }
            if (savedRentalSettingsCount == objRentalSettings.Count)
            {
                clbRentalSettings.JSProperties["cpAction"] = "registration";
                clbRentalSettings.JSProperties["cpStatus"] = "success";
                clbRentalSettings.JSProperties["cpMessage"] = message;
            }
        }

        protected void DoNewRentalSetting()
        {
            txtRentalTaxRate.Text = string.Empty;
            cdpRentalSettingFrom.SelectedDate = string.Empty;
            cdpRentalSettingTo.SelectedDate = string.Empty;
            hfRentalSettingStatus["isNewRentalSetting"] = true;
            clbRentalSettings.JSProperties["cpAction"] = "newRentalSetting";
            clbRentalSettings.JSProperties["cpStatus"] = "success";
        }

        protected void DoLoadRentalSettingsInfo(DateTime rentalStartingDate, int category)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            RentalSettingsView objRentalSettings = objSettingsBusiness.GetRentalSettingsInfo(rentalStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Rental));
            if (objRentalSettings != null)
            {
                if (objRentalSettings.RentalTaxFromIncomePercent.Equals(""))
                {
                    txtRentalTaxRate.Text = objRentalSettings.RentalTaxFromIncomePercent;
                    cboGrade.Text = objRentalSettings.Grade;
                }
                cdpRentalSettingFrom.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objRentalSettings.DateFrom));
                if (objRentalSettings.DateTo != "01/01/1753")
                {
                    cdpRentalSettingTo.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objRentalSettings.DateTo.ToString()));
                    clbRentalSettings.JSProperties["cphasEndDate"] = "true";
                }
                clbRentalSettings.JSProperties["cpAction"] = "LoadingRentalSettingInfo";
                clbRentalSettings.JSProperties["cpStatus"] = "success";
                List<int> objSettingId = objSettingsBusiness.GetSettingsByCategoryandDateFrom(Convert.ToInt32(SettingCategory.eSettingCategory.Rental), Convert.ToDateTime(objRentalSettings.DateFrom));
                Session["RentalSettingIdCollection"] = objSettingId;
            }
        }

        protected void clbRentalSettings_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            DateTime rentalSettingDate = new DateTime();
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                rentalSettingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "SaveRental":
                    if (DoValidate())
                    {
                        DoSaveRentalSetting();
                    }
                    break;
                case "NewRentalSetting":
                    DoNewRentalSetting();
                    break;
                case "LoadRentalSettingsInfo":
                    DoLoadRentalSettingsInfo(rentalSettingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Rental));
                    break;
                default:
                    break;
            }
        }

        protected void LoadRentalSettings()
        {
            SettingsBusiness objRentalSettingsBusiness = new SettingsBusiness();
            List<RentalSettingsView> objRentalSettingsList = objRentalSettingsBusiness.GetSettingsByRentalCategory(Convert.ToInt32(SettingCategory.eSettingCategory.Rental));
            if (objRentalSettingsList == null)
            {
                gvwRental.DataSource = null;
                gvwRental.DataBind();
                gvwRental.JSProperties["cpStatus"] = "error";
                gvwRental.JSProperties["cpAction"] = "LoadingRentalSettingList";
                gvwRental.JSProperties["cpMessage"] = "There are no rental settings registered";
            }
            else
            {
                gvwRental.DataSource = objRentalSettingsList;
                gvwRental.DataBind();
                gvwRental.JSProperties["cpAction"] = "LoadingRentalSettingList";
                gvwRental.JSProperties["cpStatus"] = "success";
            }
        }

        protected void DeleteRentalSetting(DateTime rentalSettingStartingDate)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            if (objSettingsBusiness.DeleteSettings(rentalSettingStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Rental)))
            {
                gvwRental.JSProperties["cpAction"] = "DeletRentalSetting";
                gvwRental.JSProperties["cpStatus"] = "success";
                gvwRental.JSProperties["cpMessage"] = "Rental settings deleted successfully";
            }
            else
            {
                gvwRental.JSProperties["cpAction"] = "DeletRentalSetting";
                gvwRental.JSProperties["cpStatus"] = "error";
            }
        }

        protected void gvwRental_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            DateTime rentalSettingStartingDate = new DateTime();
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains("|"))
                rentalSettingStartingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "LoadRentalSettings":
                    LoadRentalSettings();
                    break;
                case "DeleteRentalSetting":
                    DeleteRentalSetting(rentalSettingStartingDate);
                    break;
            }
        }

        protected void gvwRental_PageIndexChanged(object sender, EventArgs e)
        {
            LoadRentalSettings();
        }
    }
}