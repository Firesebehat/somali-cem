﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="Rental.aspx.cs" Inherits="RMS.Pages.SettingsList.Rental" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            min-height: 600px;
            color: #F87C1D !important;
            margin-left: 20px !important;
            margin: auto;
            max-width: 900px;
        }

        .menu {
            padding: 15px;
        }

        h5 {
            margin-left: 20px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        div.hidden {
            display: none
        }
    </style>
    <script type="text/javascript">
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function DoNewRentalUse(s, e) {
            clbRentalSettings.PerformCallback('NewRentalSetting');
            hfRentalSettingStatus.Set("isNewRentalSetting", true);
        }

        function DoSaveRentalSettings(s, e) {
            isValid = ASPxClientEdit.ValidateGroup('Rental');
            if (isValid) {
                clbRentalSettings.PerformCallback('SaveRental');
            }
        }

        function DoDisplayRentalSettings(s, e) {
            gvwRental.PerformCallback('LoadRentalSettings');
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewRentalUse();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveRentalSettings();
            }
            else if (e.item.name === "mnuList") {
                DoDisplayRentalSettings();
            }
        }
        function OnclbRentalSettingsEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingRentalSettingInfo') {
                    ToggleEnabled("mnuSave", true);
                    hfRentalSettingStatus.Set("isNewRentalSetting", false);
                    $("#RentalDetail").show();
                    if (s.cphasEndDate === "true") {
                        lblRentalDurationTo.SetVisible(true);
                        $("#rentalSettingEndingDate").show();
                    }
                }
                else if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                    ToggleEnabled("mnuList", true);
                    gvwRental.PerformCallback('LoadRentalSettings');
                }
                else if (s.cpAction == "newRentalSetting") {
                    $("#RentalDetail").show();
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    $("#RentalDetail").show();
                    ShowError(s.cpMessage);
                }
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
            s.cphasEndDate = "";
        }

        function OnGetRentalSettingSelectedFieldValues(values) {
            clbRentalSettings.PerformCallback('LoadRentalSettingsInfo' + '|' + values);
        }

        function DoSelectedRentalSettingsRow(e) {
            gvwRental.GetRowValues(e.visibleIndex, "DateFrom", OnGetRentalSettingSelectedFieldValues);
        }

        function DeleteRentalSetting(value) {
            gvwRental.PerformCallback('DeleteRentalSetting' + '|' + value);
            return;
        }

        function OnGetDeletedRentalRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete rental settings?',
                function (result) {
                    if (result) DeleteRentalSetting(Value);
                }, 'en');
        }

        function DeleteRentalSetting(value) {
            gvwRental.PerformCallback('DeleteRentalSetting' + '|' + value);
            return;
        }

        function OnGetDeletedRentalRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete rental settings?',
                function (result) {
                    if (result) DeleteRentalSetting(Value);
                }, 'en');
        }


        function gvwRental_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID != 'Delete') return;
            if (e.buttonID == 'Edit') {
                $("#rentalSettingEndingDate").hide();
                lblRentalDurationTo.SetVisible(true);
                DoSelectedRentalSettingsRow(e);
            }
            if (e.buttonID == 'Delete') {
                rowVisibleIndex = e.visibleIndex;
                gvwRental.GetRowValues(e.visibleIndex, 'DateFrom', OnGetDeletedRentalRowValues);
            }
        }

        function gvwRentalEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingRentalSettingList') {
                    gvwRental.SetVisible(true);
                    $("#RentalDetail").hide();
                    ToggleEnabled("mnuSave", false);
                }
                if (s.cpAction == 'DeletRentalSetting') {
                    gvwRental.PerformCallback('LoadRentalSettings');
                    ShowSuccess(s.cpMessage);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == 'LoadingRentalSettingList') {
                    ShowError(s.cpMessage);
                }
            }
        }

        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }
        //$(document).ready(function () {
        //    gvwRental.PerformCallback('LoadRentalSettings');
        //})
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <dx:ASPxCallbackPanel ID="clbRentalSettings" ClientInstanceName="clbRentalSettings" Theme="Material" runat="server"
        Width="1000px" OnCallback="clbRentalSettings_Callback" Height="100%" meta:resourcekey="clbRentalSettingsDetailsResource1">
        <ClientSideEvents EndCallback="OnclbRentalSettingsEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True">

                <h5 style="margin-top: 20px">Rental Profit Tax</h5>
                <div class="card card-body" style="margin-left: 20px">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                    <div id="RentalDetail" class="hidden" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col col-md-12" style="margin-top: 30px">
                                <div class="form-row">
                                    <div class="col-md-2" style="margin-left: 10px">
                                        <dx:ASPxLabel ID="lblGrade" runat="server" Text="Grade" meta:resourcekey="lblGradeResource1"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblRentalTax" runat="server" Text="Taxable Income(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblRentalTaxStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                   <%-- <div class="col-md-4">
                                        <dx:ASPxLabel ID="lblFederalTaxOnRentalProfit" runat="server" Text="Net Income(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblFederalTaxOnRentalProfitStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>--%>

                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblRentalDurationFrom" runat="server" Text="Starting date"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblRentalDurationFromStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblRentalDurationTo" ClientInstanceName="lblRentalDurationTo" ClientVisible="false" runat="server" Text="Ending date"></dx:ASPxLabel>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-2" style="margin-left: 10px">
                                        <dx:ASPxComboBox ID="cboGrade" runat="server" NullText="Select" ClientInstanceName="cboGrade" IncrementalFilteringMode="StartsWith" meta:resourceKey="cboEducationLevelResource1" Width="100%">
                                            <ValidationSettings ErrorTextPosition="Bottom" SetFocusOnError="True" Display="Dynamic" ErrorDisplayMode="Text" ValidationGroup="IndividualAndCompanyTaxPayeCommon">
                                                <RequiredField ErrorText="Please Select Grade" IsRequired="True" />
                                                <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtRentalTaxRate" runat="server" ClientInstanceName="txtRentalTaxRate"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Rental">
                                                <RequiredField  ErrorText="Taxable income can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                             <ClientSideEvents Validation="ValidateSettingValue"/> 
                                        </dx:ASPxTextBox>
                                    </div>
                                    <%--<div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtFederalTaxOnRentalProfit" runat="server" ClientInstanceName="txtFederalTaxOnRentalProfit"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" ToolTip="Net Income(%)" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="None" ErrorTextPosition="Bottom" ValidationGroup="Rental">
                                                <RequiredField ErrorText="Net income can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>--%>
                                    <div class="col-md-2">
                                        <cdp:CUSTORDatePicker ID="cdpRentalSettingFrom" runat="server" Width="100%" SelectedDate="" TextCssClass="" />
                                    </div>
                                    <div id="rentalSettingEndingDate" class="col-md-2 hidden">
                                        <cdp:CUSTORDatePicker ID="cdpRentalSettingTo" runat="server" Width="100%" SelectedDate="" TextCssClass="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="RentalSettingList" style="margin-top: 20px;">
                        <dx:ASPxGridView ID="gvwRental" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwRental" ClientVisible="false"
                            EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id"
                            OnCustomCallback="gvwRental_CustomCallback" OnPageIndexChanged="gvwRental_PageIndexChanged"
                            Width="100%" meta:resourcekey="gvwRentalResource">
                            <ClientSideEvents CustomButtonClick="gvwRental_CustomButtonClick" EndCallback="gvwRentalEndCallback" />
                            <SettingsBehavior ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                                <dx:GridViewDataTextColumn FieldName="RentalTaxFromIncomePercent" Caption="Taxable Income(%)" VisibleIndex="1">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Grade" Caption="Grade" VisibleIndex="2">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DateFrom" Visible="false" Caption="Starting date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DateFromEthiopic" Visible="true" Caption="Starting date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="5">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                        <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            <Styles Style-ForeColor="Red">
                                                <Style ForeColor="Red"></Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                            </Columns>
                            <Styles AdaptiveDetailButtonWidth="22">
                                <SelectedRow BackColor="Orange" ForeColor="White">
                                </SelectedRow>
                            </Styles>
                        </dx:ASPxGridView>

                    </div>
                </div>
                <uc1:Alert runat="server" ID="Alert" />
                <dx:ASPxHiddenField runat="server" ClientInstanceName="hfRentalSettingStatus" ID="hfRentalSettingStatus">
                </dx:ASPxHiddenField>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
