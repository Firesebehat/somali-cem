﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="Penality.aspx.cs" Inherits="RMS.Pages.SettingsList.Penality" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            min-height: 600px;
            color: #F87C1D !important;
            margin-left: 20px !important;
            margin: auto;
            max-width: 900px;
        }

        .menu {
            padding: 15px;
        }

        h5 {
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        div.hidden {
            display: none
        }
    </style>
    <script type="text/javascript">
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function DoNewPenality(s, e) {
            clbPenalitySettings.PerformCallback('NewPenalitySetting');
            hfPenalitySettingStatus.Set("isNewPenalitySetting", true);
        }

        function DoSavePenalitySettings(s, e) {
            var isValid = ASPxClientEdit.ValidateGroup('penality');
            if (isValid) {
                clbPenalitySettings.PerformCallback('SavePenalitySetting');
            }
        }

        function DoDisplayPenalitySettings(s, e) {
            gvwPenality.PerformCallback('LoadPenalitySettings');
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewPenality();
            }
            else if (e.item.name === "mnuSave") {
                DoSavePenalitySettings();
            }
            else if (e.item.name === "mnuList") {
                DoDisplayPenalitySettings();
            }
        }

        function OnclbPenalitySettingsEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingPenalitySettingInfo') {
                    ToggleEnabled("mnuSave", true);
                    hfPenalitySettingStatus.Set("isNewPenalitySetting", false);
                    $("#PenalityDetail").show();
                    if (s.cphasEndDate === "true") {
                        lblPenalityDurationTo.SetVisible(true);
                        $("#penalitySettingEndingDate").show();
                    }
                }
                else if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                    ToggleEnabled("mnuList", true);
                    gvwPenality.PerformCallback('LoadPenalitySettings');
                }
                else if (s.cpAction == "newPenalitySetting") {
                    $("#PenalityDetail").show();
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    ShowError(s.cpMessage);
                }
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
            s.cphasEndDate = "";
        }

        function gvwPenalityEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingPenalitySettingList') {
                    gvwPenality.SetVisible(true);
                    $("#PenalityDetail").hide();
                    $("#PenalitySettingList").show();
                    ToggleEnabled("mnuSave", false);
                }
                if (s.cpAction == 'DeletPenalitySetting') {
                    gvwPenality.PerformCallback('LoadPenalitySettings');
                    ShowSuccess(s.cpMessage);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == 'LoadingPenalitySettingList') {
                    ShowError(s.cpMessage);
                }
            }
        }

        function OnGetPenalitySettingSelectedFieldValues(values) {
            clbPenalitySettings.PerformCallback('LoadPenalitySettingsInfo' + '|' + values);
        }

        function DoSelectedPenalitySettingsRow(e) {
            gvwPenality.GetRowValues(e.visibleIndex, "PenalityDateFrom", OnGetPenalitySettingSelectedFieldValues);
        }

        function DeletePenalitySetting(value) {
            gvwPenality.PerformCallback('DeletePenalitySetting' + '|' + value);
            return;
        }

        function OnGetDeletedPenalityRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete penality settings?',
                function (result) {
                    if (result) DeletePenalitySetting(Value);
                }, 'en');
        }

        function gvwPenality_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID != "Delete") return;
            if (e.buttonID == 'Edit') {
                lblPenalityDurationTo.SetVisible(true);
                $("#penalitySettingEndingDate").show();
                DoSelectedPenalitySettingsRow(e);
            }
            if (e.buttonID == 'Delete') {
                rowVisibleIndex = e.visibleIndex;
                gvwPenality.GetRowValues(e.visibleIndex, 'PenalityDateFrom', OnGetDeletedPenalityRowValues);
            }
        }

        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }
        //$(document).ready(function () {
        //    gvwPenality.PerformCallback('LoadPenalitySettings');
        //});
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <dx:ASPxCallbackPanel ID="clbPenalitySettings" ClientInstanceName="clbPenalitySettings" Theme="Material" runat="server"
        Width="1070px" OnCallback="clbPenalitySettings_Callback" Height="100%" meta:resourcekey="clbRentalSettingsDetailsResource1">
        <ClientSideEvents EndCallback="OnclbPenalitySettingsEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True">
                <h5>Penality</h5>
                <div class="card card-body" style="margin-left: 10px">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                    <div id="PenalityDetail" class="hidden" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col col-md-12" style="padding-left: 30px">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblOneMonthDelayed" runat="server" Text="One month late declaration(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblOneMonthDelayedStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblMoreThanOneMonthDelayed" runat="server" Text="More than a month late declaration(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblMoreThanOneMonthDelayedStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>

                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblNotDeclaringYearlyIncome" runat="server" Text="Not declaring Profit per Annum(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblNotDeclaringYearlyIncomeStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtOneMonthDelayed" runat="server" ClientInstanceName="txtOneMonthDelayed"
                                            Width="100%" Font-Names="Nyala" MaxLength="6" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="One month late declaration can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtMoreThanOneMonthDelayed" runat="server" ClientInstanceName="MoreThanOneMonthDelayed"
                                            Width="100%" Font-Names="Nyala" MaxLength="6" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="More than one month late declaration can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtNoDeclaringYearlyIncome" runat="server" ClientInstanceName="txtNoFinancialRecord"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="Not declaring Profit per annum can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblOneMonthUnderStatement" runat="server" Text="One month understatement(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblOneMonthUnderStatementStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblTwoMonthUnderStatement" runat="server" Text="Two month understatement(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblTwoMonthUnderStatementStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblThreeMonthUnderStatement" runat="server" Text="Three month understatement(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblThreeMonthUnderStatementStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtOneMonthUnderStatement" runat="server" ClientInstanceName="txtOneMonthUnderStatement"
                                            Width="100%" Font-Names="Nyala" MaxLength="6" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="One month understatement can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtTwoMonthUnderStatement" runat="server" ClientInstanceName="txtTwoMonthUnderStatement"
                                            Width="100%" Font-Names="Nyala" MaxLength="2" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="Two month understatement can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtThreeMonthUnderStatement" runat="server" ClientInstanceName="txtThreeMonthUnderStatement"
                                            Width="100%" Font-Names="Nyala" MaxLength="2" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="Three month understatement can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblNoFinancialRecord" runat="server" Text="No financial record(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblNoFinancialRecordStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblBankInterestRate" runat="server" Text="Bank Interest Rate(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblBankInterestRateStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblRevenueInterestRate" runat="server" Text="Revenue Interest Rate(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblRevenueInterestRateStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtNoFinancialRecord" runat="server" ClientInstanceName="txtNoFinancialRecord"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="No financial record can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtBankInterestRate" runat="server" ClientInstanceName="txtInterestRate"
                                            Width="100%" Font-Names="Nyala" MaxLength="7" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="No bank interest rate record can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtRevenueInterestRate" runat="server" ClientInstanceName="txtRevenueInterestRate"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="penality">
                                                <RequiredField ErrorText="No interest rate record can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="form-row" style="margin-top:-20px">
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblPenalityDurationFrom" runat="server" Text="Starting date"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblPenalityDurationFromStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2" style="margin-left:-30px">
                                        <dx:ASPxLabel ID="lblPenalityDurationTo" ClientVisible="false" ClientInstanceName="lblPenalityDurationTo" runat="server" Text="Ending date"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 w-25">
                                        <cdp:CUSTORDatePicker ID="cdpPenalityDateFrom" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                    <div id="penalitySettingEndingDate" class="col-md-1 text-left hidden" style="margin-left: -30px">
                                        <cdp:CUSTORDatePicker ID="cdpPenalityDateTo" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="PenalitySettingList" class="hidden" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col col-md-10">
                                <dx:ASPxGridView ID="gvwPenality" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwPenality" ClientVisible="false"
                                    EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small"  KeyFieldName="Id" Width="300px"
                                    OnCustomCallback="gvwPenality_CustomCallback" OnPageIndexChanged="gvwPenality_PageIndexChanged" meta:resourcekey="gvwPenalityResource">
                                    <ClientSideEvents CustomButtonClick="gvwPenality_CustomButtonClick" EndCallback="gvwPenalityEndCallback" />
                                    <SettingsBehavior ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="OneMonthDelayedDeclaration" Caption="One month late declaration(%)" VisibleIndex="1">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Font-Size="Small" Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="MoreThanOneMonthDelayedDeclaration" Width="10" Caption="More than a month late declaration(%)" VisibleIndex="2">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left"></HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NotDeclaringYearlyIncome"  Width="5" Caption="Not declaring Profit per Annum(%)" VisibleIndex="3">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="OneMonthUnderStatement" Width="5" Caption="One month understatement(%)" VisibleIndex="4">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TwoMonthUnderStatement" Width="5" Caption="Two month understatement(%)" VisibleIndex="5">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ThreeMonthUnderStatement" Width="5" Caption="Three month understatement(%)" VisibleIndex="6">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NoFinancialRecord" Width="5" Caption="No financial record(%)" VisibleIndex="7">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="BankInterestRate" Width="5" Caption="Bank Interest Rate" VisibleIndex="8">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="RevenueInterestRate" Width="5" Caption="Revenue Interest Rate" VisibleIndex="9">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PenalityDateFromEthiopic" Caption="Starting date" VisibleIndex="10">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PenalityDateFrom" Visible="false" Caption="Starting date" VisibleIndex="11">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="12">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                                <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                                    <Styles Style-ForeColor="Red">
                                                        <Style ForeColor="Red"></Style>
                                                    </Styles>
                                                </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                        </dx:GridViewCommandColumn>
                                    </Columns>
                                    <Styles AdaptiveDetailButtonWidth="22">
                                        <SelectedRow BackColor="Orange" ForeColor="White">
                                        </SelectedRow>
                                    </Styles>
                                </dx:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </div>
                <dx:ASPxHiddenField runat="server" ClientInstanceName="hfPenalitySettingStatus" ID="hfPenalitySettingStatus">
                </dx:ASPxHiddenField>
                <uc1:Alert runat="server" ID="Alert" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
