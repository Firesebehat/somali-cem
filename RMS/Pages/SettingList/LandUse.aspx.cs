﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;
using System.Web.Security;

namespace RMS.Pages.SettingList
{
    public partial class LandUse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
            {
                Response.Redirect("~/AccessDenied");
            }
        }

        protected List<Setting> AssignLandUseSettingsToObject()
        {
            Dictionary<string, string> landUseSettingsList = new Dictionary<string, string>();
            landUseSettingsList.Add("NonCooperativeAssociation", txtNonCooperativesAssociation.Text);
            landUseSettingsList.Add("CooperativeAssociation", txtCooperativesAssociation.Text);
            landUseSettingsList.Add("MonthlyDelayedDeclaration", txtMonthlyDelayedDeclaration.Text);
            List<Setting> objSettingsList = new List<Setting>();
            foreach (KeyValuePair<string, string> rentalSetting in landUseSettingsList)
            {
                Setting objSettings = new Setting();
                objSettings.AppId = "SIRIM-AddsiAbeba";
                objSettings.Key = rentalSetting.Key;
                objSettings.Value = rentalSetting.Value;
                objSettings.DateFrom = cdpLandUseDateFrom.SelectedGCDate;
                if (cdpLandUseDateTo.SelectedDate != "")
                {
                    objSettings.DateTo = cdpLandUseDateTo.SelectedGCDate;
                }
                else
                {
                    objSettings.DateTo = Convert.ToDateTime("1/1/1753");
                }
                objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.LandUse);
                objSettings.CenterId = 0;
                objSettings.CenterName = "";
                objSettingsList.Add(objSettings);
            }

            return objSettingsList;
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date
            if (cdpLandUseDateFrom.SelectedDate.Trim().Length == 0)
            {
                errMessages.Add("Please enter land use settings starting date!");
            }
            else
            {
                if (!cdpLandUseDateFrom.IsValidDate)
                    errMessages.Add("Please enter a valid land use settings starting date!");
                if (cdpLandUseDateFrom.IsFutureDate)
                    errMessages.Add("Starting date of land use settings should not be upcoming date. Please try again!");
            }

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        protected bool DoValidate()
        {
            bool isValid = true;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                isValid = false;
                clbLandUseSettings.JSProperties["cpAction"] = "registration";
                clbLandUseSettings.JSProperties["cpStatus"] = "error";
                clbLandUseSettings.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }

        protected void DoSaveLandUseSetting()
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            Setting objSetting = new Setting();
            List<Setting> objLandUseSettings = AssignLandUseSettingsToObject();
            int saveLandUseSettingsCount = 0;
            string message = "";
            if (!(bool)hfLandUseSettingStatus["isNewLandUseSetting"])
            {
                List<int> landUseSettingsIdCollection = (List<int>)Session["LandUseSettingIdCollection"];
                for (int index = 0; index < objLandUseSettings.Count; index++)
                {
                    objLandUseSettings[index].UpdatedBy = "";
                    objLandUseSettings[index].UpdatedDate = DateTime.Now;
                    objLandUseSettings[index].UpdatedUserId = "";
                    objLandUseSettings[index].Id = landUseSettingsIdCollection[index];
                    if (objSettingsBusiness.UpdateByKey(objLandUseSettings[index]))
                    {
                        saveLandUseSettingsCount++;
                    }
                }
                message = "Land use payment settings updated successfully";
            }
            else
            {
                //number of keys that are under land use settings are 3
                List<int> objLatestLandUseSettingsId = objSettingsBusiness.GetLatestSettingsId(Convert.ToInt32(SettingCategory.eSettingCategory.LandUse), 3);
                for (int index = 0; index < objLandUseSettings.Count; index++)
                {
                    objLandUseSettings[index].CreatedBy = "";
                    objLandUseSettings[index].CreatedDate = DateTime.Now;
                    objLandUseSettings[index].CreatedUserId = "";
                    if (objSettingsBusiness.InsertSettings(objLandUseSettings[index]))
                    {
                        saveLandUseSettingsCount++;
                    }
                }
                if (objLatestLandUseSettingsId != null)
                {
                    for (int index = 0; index < objLatestLandUseSettingsId.Count; index++)
                    {
                        objSetting.Id = objLatestLandUseSettingsId[index];
                        objSetting.DateTo = cdpLandUseDateFrom.SelectedGCDate;
                        objSettingsBusiness.UpdateEndingDateofKey(objSetting);
                    }
                }
                message = "Land use payment settings registered successfully";
            }
            if (saveLandUseSettingsCount == objLandUseSettings.Count)
            {
                clbLandUseSettings.JSProperties["cpAction"] = "registration";
                clbLandUseSettings.JSProperties["cpStatus"] = "success";
                clbLandUseSettings.JSProperties["cpMessage"] = message;
            }
        }

        protected void DoNewLandUseSetting()
        {
            txtNonCooperativesAssociation.Text = string.Empty;
            txtCooperativesAssociation.Text = string.Empty;
            txtMonthlyDelayedDeclaration.Text = string.Empty;
            cdpLandUseDateFrom.SelectedDate = "";
            cdpLandUseDateTo.SelectedDate = "";
            hfLandUseSettingStatus["isNewLandUseSetting"] = true;
            clbLandUseSettings.JSProperties["cpAction"] = "newLandUseSetting";
            clbLandUseSettings.JSProperties["cpStatus"] = "success";
        }

        protected void LoadLandUseSettingsInfo(DateTime salarySettingStartingDate, int category)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            LandUseSettingsView objLandUseSettings = objSettingsBusiness.GetLandUseSettingsInfo(salarySettingStartingDate, category);
            if (objLandUseSettings != null)
            {
                txtCooperativesAssociation.Text = objLandUseSettings.CooperativeAssociation;
                txtNonCooperativesAssociation.Text = objLandUseSettings.NonCooperativeAssociation;
                txtMonthlyDelayedDeclaration.Text = objLandUseSettings.MonthlyDelayedDeclaration;
                cdpLandUseDateFrom.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objLandUseSettings.LandUseDateFrom));
                if (objLandUseSettings.LandUseDateTo != "01/01/1753")
                {
                    cdpLandUseDateTo.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objLandUseSettings.LandUseDateTo.ToString()));
                    clbLandUseSettings.JSProperties["cphasEndDate"] = "true";

                }
                clbLandUseSettings.JSProperties["cpAction"] = "LoadingLandUseSettingInfo";
                clbLandUseSettings.JSProperties["cpStatus"] = "success";
                List<int> objSettingId = objSettingsBusiness.GetSettingsByCategoryandDateFrom(Convert.ToInt32(SettingCategory.eSettingCategory.LandUse), Convert.ToDateTime(objLandUseSettings.LandUseDateFrom));
                Session["LandUseSettingIdCollection"] = objSettingId;
            }
        }

        protected void clbLandUseSettings_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            DateTime landUseSettingSatrtingDate = new DateTime();
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                landUseSettingSatrtingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "SaveLandUseSettings":
                    if (DoValidate())
                    {
                        DoSaveLandUseSetting();
                    }
                    break;
                case "LoadLandUseSettingsInfo":
                    LoadLandUseSettingsInfo(landUseSettingSatrtingDate, Convert.ToInt32(SettingCategory.eSettingCategory.LandUse));
                    break;
                case "NewLandUseSetting":
                    DoNewLandUseSetting();
                    break;
                default:
                    break;
            }
        }

        protected void LoadLandUseSettings()
        {
            SettingsBusiness objLandUseSettingsBusiness = new SettingsBusiness();
            List<LandUseSettingsView> objLandUseSettingsList = objLandUseSettingsBusiness.GetSettingsByLandUseCategory(Convert.ToInt32(SettingCategory.eSettingCategory.LandUse));
            if (objLandUseSettingsList == null)
            {
                gvwLandUse.DataSource = null;
                gvwLandUse.DataBind();
                gvwLandUse.JSProperties["cpStatus"] = "error";
                gvwLandUse.JSProperties["cpAction"] = "LoadingLandUseSettingList";
                gvwLandUse.JSProperties["cpMessage"] = "There are no land use settings registered";
            }
            else
            {
                gvwLandUse.DataSource = objLandUseSettingsList;
                gvwLandUse.DataBind();
                gvwLandUse.JSProperties["cpAction"] = "LoadingLandUseSettingList";
                gvwLandUse.JSProperties["cpStatus"] = "success";
            }
        }

        protected void DeleteLandUseSetting(DateTime landUseSettingStartingDate)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            if (objSettingsBusiness.DeleteSettings(landUseSettingStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.LandUse)))
            {
                gvwLandUse.JSProperties["cpAction"] = "DeleteLandUseSetting";
                gvwLandUse.JSProperties["cpStatus"] = "success";
                gvwLandUse.JSProperties["cpMessage"] = "Land use settings deleted successfully";
            }
            else
            {
                gvwLandUse.JSProperties["cpAction"] = "DeleteLandUseSetting";
                gvwLandUse.JSProperties["cpStatus"] = "error";
            }
        }

        protected void gvwLandUse_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            DateTime landUseStartingDate = new DateTime();
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains("|"))
                landUseStartingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "LoadLandUseSettings":
                    LoadLandUseSettings();
                    break;
                case "DeleteLandUseSetting":
                    DeleteLandUseSetting(landUseStartingDate);
                    break;
            }
        }

        protected void gvwLandUse_PageIndexChanged(object sender, EventArgs e)
        {

        }
    }
}