﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="Salary.aspx.cs" Inherits="RMS.Pages.SettingsList.Salary" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            min-height: 600px;
            color: #F87C1D !important;
            margin-left: 20px !important;
            margin: auto;
            max-width: 900px;
        }

        .menu {
            padding: 15px;
        }

        h5 {
            margin-left: 20px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        div.hidden {
            display: none
        }
    </style>
    <script type="text/javascript">
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function DoNewSalary(s, e) {
            clbSalarySettings.PerformCallback('NewSalarySetting');
            hfSalarySettingStatus.Set("isNewSalarySetting", true);
        }

        function DoSaveSalarySettings(s, e) {
            var isValid = ASPxClientEdit.ValidateGroup('Salary');
            if (isValid) {
                clbSalarySettings.PerformCallback('SaveSalarySettings');
            }
        }

        function DoDisplaySalarySettings(s, e) {
            gvwSalary.PerformCallback('LoadSalarySettings');
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewSalary();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveSalarySettings();
            }
            else if (e.item.name === "mnuList") {
                DoDisplaySalarySettings();
            }
        }
        function OnclbSalarySettingsEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingSalarySettingInfo') {
                    hfSalarySettingStatus.Set("isNewSalarySetting", false);
                    $("#SalaryDetail").show();
                    if (s.cphasEndDate === "true") {
                        lblDurationTo.SetVisible(true);
                        $("#salarySettingEndingDate").show();
                    }
                    ToggleEnabled("mnuSave", true);
                }
                else if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                    ToggleEnabled("mnuList", true);
                    gvwSalary.PerformCallback('LoadSalarySettings');
                }
                else if (s.cpAction == "newSalarySetting") {
                    $("#SalaryDetail").show();
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    $("#SalaryDetail").show();
                    ShowError(s.cpMessage);
                }
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
            s.hasEndDate = "";
        }

        function OnGetSalarySettingSelectedFieldValues(values) {
            clbSalarySettings.PerformCallback('LoadSalarySettingsInfo' + '|' + values);
        }

        function DoSelectedSalarySettingsRow(e) {
            gvwSalary.GetRowValues(e.visibleIndex, "SalarySettingDateFrom", OnGetSalarySettingSelectedFieldValues);
        }

        function DeleteSalarySetting(value) {
            gvwSalary.PerformCallback('DeleteSalarySetting' + '|' + value);
            return;
        }

        function OnGetDeletedSalaryRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete salary settings?',
                function (result) {
                    if (result) DeleteSalarySetting(Value);
                }, 'en');
        }

        function gvwSalary_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID != 'Delete') return;
            if (e.buttonID == 'Edit') {
                lblDurationTo.SetVisible(false);
                $("#salarySettingEndingDate").hide();
                DoSelectedSalarySettingsRow(e);
            }
            if (e.buttonID == 'Delete') {
                rowVisibleIndex = e.visibleIndex;
                gvwSalary.GetRowValues(e.visibleIndex, 'SalarySettingDateFrom', OnGetDeletedSalaryRowValues);
            }
        }

        function gvwSalaryEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingSalarySettingList') {
                    gvwSalary.SetVisible(true);
                    $("#SalaryDetail").hide();
                    $("#SalarySettingList").show();
                    ToggleEnabled("mnuSave", false);
                }
                if (s.cpAction == 'DeletSalarySetting') {
                    ShowSuccess(s.cpMessage);
                    gvwSalary.PerformCallback('LoadSalarySettings');
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == 'LoadingSalarySettingList') {
                    ShowError(s.cpMessage);
                }
            }
        }

        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }

        //$(document).ready(function () {
        //    gvwSalary.PerformCallback('LoadSalarySettings');
        //});
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <dx:ASPxCallbackPanel ID="clbSalarySettings" ClientInstanceName="clbSalarySettings" Theme="Material" runat="server"
        Width="1000px" OnCallback="clbSalarySettings_Callback" Height="100%" meta:resourcekey="clbSalarySettingsDetailsResource1">
        <ClientSideEvents EndCallback="OnclbSalarySettingsEndCallback" />
        <PanelCollection>
            <dx:PanelContent>

                <h5>Salary</h5>
                <div class="card card-body" style="margin-left: 20px;">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px">
                                        <Paddings PaddingTop="7px" PaddingBottom="5px"></Paddings>
                                    </ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px">
                                        <Paddings PaddingTop="7px" PaddingBottom="5px"></Paddings>
                                    </ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px">
                                        <Paddings PaddingTop="7px" PaddingBottom="5px"></Paddings>
                                    </ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                    <div id="SalaryDetail" class="hidden" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col col-md-12">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblTransportAllowancePercent" runat="server" Text="Transport allowance(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblTransportAllowancePercentStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblTransportAllowanceInBirr" runat="server" Text="Transport allowance(Birr)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblTransportAllowanceInBirrdStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>

                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblDelegationAllowance" runat="server" Text="Delegation allowance"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblDelegationAllowanceStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtTransportAllowancePercent" runat="server" ClientInstanceName="txtTransportAllowancePercent"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Salary">
                                                <RequiredField ErrorText="Transport allowance(%) can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtTransportAllowanceInBirr" runat="server" ClientInstanceName="txtTransportAllowancePercent"
                                            Width="100%" Font-Names="Nyala" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="None" ErrorTextPosition="Bottom" ValidationGroup="Salary">
                                                <RequiredField ErrorText="Transport allowance(Birr) can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                           
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtDelegationAllowance" runat="server" ClientInstanceName="txtTransportAllowancePercent"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Salary">
                                                <RequiredField ErrorText="Delegation allowance(%) can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                              <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblOrganizationProvidentFund" runat="server" Text="Providunt fund allowance(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblOrganizationProvidentFundStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblCostSharing" runat="server" Text="Cost sharing(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblCostSharingStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblDurationFrom" runat="server" Text="Starting date"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblDurationFromStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3" style="margin-left: -30px">
                                        <dx:ASPxLabel ID="lblDurationTo" runat="server" ClientVisible="false" ClientInstanceName="lblDurationTo" Text="Ending date"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtProvidentFund" runat="server" ClientInstanceName="txtProvidentFund"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Salary">
                                                <RequiredField ErrorText="Providunt fund allowance(%) can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                              <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtCostSharing" runat="server" ClientInstanceName="txtCostSharing"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Salary">
                                                <RequiredField ErrorText="Cost sharing(%) can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                              <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-2 w-25">
                                        <cdp:CUSTORDatePicker ID="cdpSalaryDateFrom" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                    <div id="salarySettingEndingDate" class="col-md-1 text-left hidden" style="margin-left: -30px">
                                        <cdp:CUSTORDatePicker ID="cdpSalaryDateTo" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SalarySettingList" style="margin-top: 20px;">
                        <dx:ASPxGridView ID="gvwSalary" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwSalary" ClientVisible="false"
                            EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id"
                            OnCustomCallback="gvwSalary_CustomCallback" OnPageIndexChanged="gvwSalary_PageIndexChanged"
                            Width="100%" meta:resourcekey="gvwSalaryResource">
                            <ClientSideEvents CustomButtonClick="gvwSalary_CustomButtonClick" EndCallback="gvwSalaryEndCallback" />
                            <SettingsBehavior ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                            <SettingsPopup>
                                <HeaderFilter MinHeight="140px"></HeaderFilter>
                            </SettingsPopup>
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                                <dx:GridViewDataTextColumn FieldName="TransportAllowancePercent" Caption="Transport allowance(%)" VisibleIndex="1">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="TransportAllowanceInBirr" Caption="Transport allowance(Birr)" VisibleIndex="2">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DelegationAllowance" Caption="Delegation allowance" VisibleIndex="3">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ProvidentFund" Caption="Providunt fund allowance(%)" VisibleIndex="3">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="MonthlyCostSharing" Caption="Cost sharing(%)" VisibleIndex="3">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="SalarySettingDateFrom" Visible="false" Caption="Staring date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="SalarySettingDateFromEthiopic" Caption="Staring date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="4">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                        <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            <Styles Style-ForeColor="Red">
                                                <Style ForeColor="Red"></Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                            </Columns>
                            <Styles AdaptiveDetailButtonWidth="22">
                                <SelectedRow BackColor="Orange" ForeColor="White">
                                </SelectedRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </div>
                </div>
                <uc1:Alert runat="server" ID="Alert" />
                <dx:ASPxHiddenField runat="server" ClientInstanceName="hfSalarySettingStatus" ID="hfSalarySettingStatus">
                </dx:ASPxHiddenField>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
