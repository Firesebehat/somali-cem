﻿using System;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;
using System.Collections.Specialized;
using System.Web.Security;

namespace RMS.Pages.SettingList
{
    public partial class Agriculture : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
            {
                Response.Redirect("~/AccessDenied");
            }
            var id = Request.QueryString["Id"];
            if (id != null)
            {
                
            }
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date
            if (cdpDateFrom.SelectedDate.Trim().Length == 0)
            {
                errMessages.Add("Please enter agricultural settings starting date!");
            }
            else
            {
                if (!cdpDateFrom.IsValidDate)
                    errMessages.Add("Please enter a valid agriculural settings starting date!");
                if (cdpDateFrom.IsFutureDate)
                    errMessages.Add("Starting date of agricultural settings should not be upcoming date. Please try again!");
            }

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        protected bool DoValidate()
        {
            bool isValid = true;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                isValid = false;
                clbAgricultureSettings.JSProperties["cpAction"] = "registration";
                clbAgricultureSettings.JSProperties["cpStatus"] = "error";
                clbAgricultureSettings.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }

        protected List<Setting> AssignAgriculturSettingsToObject()
        {
            Dictionary<string, string> agriculturSettingsList = new Dictionary<string, string>();
            agriculturSettingsList.Add("AgricultureCategoryCode", txtAgriculturCategoryRate.Text);
            agriculturSettingsList.Add("AgricultureTaxDelayFine", txtAgricultureTaxLateDeclaration.Text);
            agriculturSettingsList.Add("AgricultureTaxArchiveFine", txtAgricultureNoArchiveRecord.Text);
            List<Setting> objSettingsList = new List<Setting>();
            foreach (KeyValuePair<string, string> agricuturSetting in agriculturSettingsList)
            {
                Setting objSettings = new Setting();
                objSettings.AppId = "SIRIM-AddsiAbeba";
                objSettings.Key = agricuturSetting.Key;
                objSettings.Value = agricuturSetting.Value;
                objSettings.DateFrom = cdpDateFrom.SelectedGCDate;
                if (cdpDateTo.SelectedDate != "")
                {
                    objSettings.DateTo = cdpDateTo.SelectedGCDate;
                }
                else
                {
                    objSettings.DateTo = Convert.ToDateTime("1/1/1753");
                }
                objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture);
                objSettings.CenterId = 0;
                objSettings.CenterName = "";
                //objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture);
                //objSettings.CenterId = CurrentProfile.ActiveUserProfile.TaxCenterId;
                //objSettings.CenterName = CurrentProfile.ActiveUserProfile.Name;
                objSettingsList.Add(objSettings);
            }

            return objSettingsList;
        }

        protected void DoSaveAgriculture()
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            Setting objSetting = new Setting();
            int savedAgriculturSettingsCount = 0;
            string message = "";
            List<Setting> objAgriculuturrSettings = AssignAgriculturSettingsToObject();
            if (!(bool)hfAgricultureSettingStatus["isNewAgricultureSetting"])
            {
                List<int> agricultureSettingsIdCollection = (List<int>)Session["AgricultureSettingIdCollection"];
                for (int index = 0; index < objAgriculuturrSettings.Count; index++)
                {
                    objAgriculuturrSettings[index].Id = agricultureSettingsIdCollection[index];
                    //objAgriculuturrSettings[index].UpdatedBy = HttpContext.Current.User.Identity.Name;
                    //objAgriculuturrSettings[index].UpdatedDate = DateTime.Now;
                    //objAgriculuturrSettings[index].UpdatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    objAgriculuturrSettings[index].UpdatedBy = "";
                    objAgriculuturrSettings[index].UpdatedDate = DateTime.Now;
                    objAgriculuturrSettings[index].UpdatedUserId = "0";
                    if (objSettingsBusiness.UpdateByKey(objAgriculuturrSettings[index]))
                    {
                        savedAgriculturSettingsCount++;
                    }
                }
                message = "Agricultural tax settings updated successfully";
            }
            else
            {
                //number of keys that are under agriculture setting are 3
                List<int> objLatestAgricultureSettingsId = objSettingsBusiness.GetLatestSettingsId(Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture), 3);
                for (int index = 0; index < objAgriculuturrSettings.Count; index++)
                {
                    //objAgriculuturrSettings[index].CreatedBy = HttpContext.Current.User.Identity.Name;
                    //objAgriculuturrSettings[index].CreatedDate = DateTime.Now;
                    //objAgriculuturrSettings[index].CreatedUserId = CurrentProfile.ActiveUserProfile.UserId;
                    objAgriculuturrSettings[index].CreatedBy = "";
                    objAgriculuturrSettings[index].CreatedDate = DateTime.Now;
                    objAgriculuturrSettings[index].CreatedUserId = "";
                    if (objSettingsBusiness.InsertSettings(objAgriculuturrSettings[index]))
                    {
                        savedAgriculturSettingsCount++;
                    }
                }
                if (objLatestAgricultureSettingsId != null)
                {
                    for (int index = 0; index < objLatestAgricultureSettingsId.Count; index++)
                    {
                        objSetting.Id = objLatestAgricultureSettingsId[index];
                        objSetting.DateTo = cdpDateFrom.SelectedGCDate;
                        objSettingsBusiness.UpdateEndingDateofKey(objSetting);
                    }
                }
                message = "Agricultural tax settings registered successfully";
            }

            if (savedAgriculturSettingsCount == objAgriculuturrSettings.Count)
            {
                clbAgricultureSettings.JSProperties["cpAction"] = "registration";
                clbAgricultureSettings.JSProperties["cpStatus"] = "success";
                clbAgricultureSettings.JSProperties["cpMessage"] = message;
            }
        }

        protected void DoNewAgricultureSetting()
        {
            txtAgriculturCategoryRate.Text = string.Empty;
            txtAgricultureTaxLateDeclaration.Text = string.Empty;
            txtAgricultureNoArchiveRecord.Text = string.Empty;
            cdpDateFrom.SelectedDate = string.Empty;
            cdpDateTo.SelectedDate = string.Empty;
            clbAgricultureSettings.JSProperties["cpAction"] = "newAgricultureSetting";
            clbAgricultureSettings.JSProperties["cpStatus"] = "success";
        }

        protected void LoadAgricultureSettings(DateTime agriculturSettingDate, int category)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            AgriculutreSettingsView objAgricultureSettings = objSettingsBusiness.GetAgricultureSettingsInfo(agriculturSettingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture));
            if (objAgricultureSettings != null)
            {
                txtAgriculturCategoryRate.Text = objAgricultureSettings.AgricultureCategoryCode;
                txtAgricultureTaxLateDeclaration.Text = objAgricultureSettings.AgricultureTaxDelayFine;
                txtAgricultureNoArchiveRecord.Text = objAgricultureSettings.AgricultureTaxArchiveFine;
                cdpDateFrom.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objAgricultureSettings.AgricultureDateFrom));
                if (objAgricultureSettings.AgricultureDateTo != "01/01/1753")
                {
                    cdpDateTo.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objAgricultureSettings.AgricultureDateTo.ToString()));
                    clbAgricultureSettings.JSProperties["cphasEndDate"] = "true";
                }
                clbAgricultureSettings.JSProperties["cpAction"] = "LoadingAgriculturSettingInfo";
                clbAgricultureSettings.JSProperties["cpStatus"] = "success";
                List<int> objSettingId = objSettingsBusiness.GetSettingsByCategoryandDateFrom(Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture), Convert.ToDateTime(objAgricultureSettings.AgricultureDateFrom));
                Session["AgricultureSettingIdCollection"] = objSettingId;
            }
        }

        protected void clbAgricultureSettings_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            DateTime agriculturSettingDate = new DateTime();
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                agriculturSettingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "SaveAgriculture":
                    if (DoValidate())
                    {
                        DoSaveAgriculture();
                    }
                    break;
                case "NewAgricultureSetting":
                    DoNewAgricultureSetting();
                    break;
                case "LoadAgricultureSetting":
                    LoadAgricultureSettings(agriculturSettingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture));
                    break;
                default:
                    break;
            }
        }

        protected void LoadAgriculturalSettings()
        {
            SettingsBusiness objAgricultureSettingsBusiness = new SettingsBusiness();
            List<AgriculutreSettingsView> objAgricultureSettingsList = objAgricultureSettingsBusiness.GetSettingsByAgricultureCategory(Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture));
            if (objAgricultureSettingsList == null)
            {
                gvwAgriculture.DataSource = null;
                gvwAgriculture.DataBind();
                gvwAgriculture.JSProperties["cpAction"] = "LoadingAgriculturSettingList";
                gvwAgriculture.JSProperties["cpStatus"] = "error";
                gvwAgriculture.JSProperties["cpMessage"] = "There are no agricultural settings registered";
            }
            else
            {
                gvwAgriculture.DataSource = objAgricultureSettingsList;
                gvwAgriculture.DataBind();
                gvwAgriculture.JSProperties["cpAction"] = "LoadingAgriculturSettingList";
                gvwAgriculture.JSProperties["cpStatus"] = "success";
            }
        }

        protected void DeleteAgriculturalSetting(DateTime agriculturalStartingDate)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            if (objSettingsBusiness.DeleteSettings(agriculturalStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Agriculture)))
            {
                gvwAgriculture.JSProperties["cpAction"] = "DeletAgriculturalSetting";
                gvwAgriculture.JSProperties["cpStatus"] = "success";
                gvwAgriculture.JSProperties["cpMessage"] = "Agricultural settings deleted successfully";
            }
            else
            {
                gvwAgriculture.JSProperties["cpAction"] = "DeletAgriculturalSetting";
                gvwAgriculture.JSProperties["cpStatus"] = "error";
            }
        }

        protected void gvwAgriculture_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            DateTime agriculturalStartingDate = new DateTime();
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains("|"))
                agriculturalStartingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "LoadAgriculturalSettings":
                    LoadAgriculturalSettings();
                    break;
                case "DeleteAgriculturalSetting":
                    DeleteAgriculturalSetting(agriculturalStartingDate);
                    break;
            }
        }

        protected void gvwAgriculture_PageIndexChanged(object sender, EventArgs e)
        {

        }
    }
}