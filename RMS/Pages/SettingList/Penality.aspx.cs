﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;
using System.Web.Security;

namespace RMS.Pages.SettingsList
{
    public partial class Penality : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
            {
                Response.Redirect("~/AccessDenied");
            }
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date
            if (cdpPenalityDateFrom.SelectedDate.Trim().Length == 0)
            {
                errMessages.Add("Please enter penality settings starting date!");
            }
            else
            {
                if (!cdpPenalityDateFrom.IsValidDate)
                    errMessages.Add("Please enter a valid penality settings starting date!");
                if (cdpPenalityDateFrom.IsFutureDate)
                    errMessages.Add("Starting date of penality settings should not upcoming date. Please try again!");
            }

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        protected bool DoValidate()
        {
            bool isValid = true;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                isValid = false;
                clbPenalitySettings.JSProperties["cpAction"] = "registration";
                clbPenalitySettings.JSProperties["cpStatus"] = "error";
                clbPenalitySettings.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }

        protected List<Setting> AssignPenalitySettingsToObject()
        {
            Dictionary<string, string> penalitySettingsList = new Dictionary<string, string>();
            penalitySettingsList.Add("OneMonthDelayedDeclaration", txtOneMonthDelayed.Text);
            penalitySettingsList.Add("BankInterestRate", txtBankInterestRate.Text);
            penalitySettingsList.Add("RevenueInterestRate", txtRevenueInterestRate.Text);
            penalitySettingsList.Add("MoreThanOneMonthDelayedDeclaration", txtMoreThanOneMonthDelayed.Text);
            penalitySettingsList.Add("NotDeclaringYearlyIncome", txtNoDeclaringYearlyIncome.Text);
            penalitySettingsList.Add("OneMonthUnderStatement", txtOneMonthUnderStatement.Text);
            penalitySettingsList.Add("TwoMonthUnderStatement", txtTwoMonthUnderStatement.Text);
            penalitySettingsList.Add("ThreeMonthUnderStatement", txtThreeMonthUnderStatement.Text);
            penalitySettingsList.Add("NoFinancialRecord", txtNoFinancialRecord.Text);
            List<Setting> objSettingsList = new List<Setting>();
            foreach (KeyValuePair<string, string> rentalSetting in penalitySettingsList)
            {
                Setting objSettings = new Setting();
                objSettings.AppId = "SIRIM-AddsiAbeba";
                objSettings.Key = rentalSetting.Key;
                objSettings.Value = rentalSetting.Value;
                objSettings.DateFrom = cdpPenalityDateFrom.SelectedGCDate;
                if (cdpPenalityDateTo.SelectedDate != "")
                {
                    objSettings.DateTo = cdpPenalityDateTo.SelectedGCDate;
                }
                else
                {
                    objSettings.DateTo = DateTime.MinValue;
                }
                objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.Penality);
                objSettings.CenterId = 0;
                objSettings.CenterName = "";
                objSettingsList.Add(objSettings);
            }

            return objSettingsList;
        }

        protected void DoSavePenalitySetting()
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            Setting objSetting = new Setting();
            List<Setting> objPenalitySettings = AssignPenalitySettingsToObject();
            int savedPenalitySettingsCount = 0;
            string message = "";
            if (!(bool)hfPenalitySettingStatus["isNewPenalitySetting"])
            {
                List<int> penalitySettingsIdCollection = (List<int>)Session["PenalitySettingIdCollection"];
                for (int index = 0; index < objPenalitySettings.Count; index++)
                {
                    objPenalitySettings[index].UpdatedBy = "";
                    objPenalitySettings[index].UpdatedDate = DateTime.Now;
                    objPenalitySettings[index].UpdatedUserId = "";
                    objPenalitySettings[index].Id = penalitySettingsIdCollection[index];
                    if (objSettingsBusiness.UpdateByKey(objPenalitySettings[index]))
                    {
                        savedPenalitySettingsCount++;
                    }
                }
                message = "Penality settings updated successfully";
            }
            else
            {
                //number of keys that are under penality settings are 3
                List<int> objLatestPenalitySettingsId = objSettingsBusiness.GetLatestSettingsId(Convert.ToInt32(SettingCategory.eSettingCategory.Penality), 7);
                for (int index = 0; index < objPenalitySettings.Count; index++)
                {
                    objPenalitySettings[index].CreatedBy = "";
                    objPenalitySettings[index].CreatedDate = DateTime.Now;
                    objPenalitySettings[index].CreatedUserId = "";
                    if (objSettingsBusiness.InsertSettings(objPenalitySettings[index]))
                    {
                        savedPenalitySettingsCount++;
                    }
                }
                if (objLatestPenalitySettingsId != null)
                {
                    for (int index = 0; index < objLatestPenalitySettingsId.Count; index++)
                    {
                        objSetting.Id = objLatestPenalitySettingsId[index];
                        objSetting.DateTo = cdpPenalityDateFrom.SelectedGCDate;
                        objSettingsBusiness.UpdateEndingDateofKey(objSetting);
                    }
                }
                message = "Penality settings registered successfully";

            }
            if (savedPenalitySettingsCount == objPenalitySettings.Count)
            {
                clbPenalitySettings.JSProperties["cpAction"] = "registration";
                clbPenalitySettings.JSProperties["cpStatus"] = "success";
                clbPenalitySettings.JSProperties["cpMessage"] = message;
            }
        }

        protected void DoNewPenalitySetting()
        {
            //txtOneMonthDelayed.Text = string.Empty;
            //txtMoreThanOneMonthDelayed.Text = string.Empty;
            //txtNoDeclaringYearlyIncome.Text = string.Empty;
            //txtNoFinancialRecord.Text = string.Empty;
            //txtOneMonthUnderStatement.Text = string.Empty;
            //txtTwoMonthUnderStatement.Text = string.Empty;
            //txtThreeMonthUnderStatement.Text = string.Empty;
            //txtBankInterestRate.Text = string.Empty;
            //txtRevenueInterestRate.Text = string.Empty;
            //cdpPenalityDateFrom.SelectedDate = string.Empty;
            //cdpPenalityDateTo.SelectedDate = string.Empty;
            hfPenalitySettingStatus["isNewPenalitySetting"] = true;
            clbPenalitySettings.JSProperties["cpAction"] = "newPenalitySetting";
            clbPenalitySettings.JSProperties["cpStatus"] = "success";
        }

        protected void LoadPenalityeSettings(DateTime penalityStartingDate, int category)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            PenalitySettingsView objPenalitySettings = objSettingsBusiness.GetPenalitySettingsInfo(penalityStartingDate, category);
            if (objPenalitySettings != null)
            {
                txtOneMonthDelayed.Text = objPenalitySettings.OneMonthDelayedDeclaration;
                txtMoreThanOneMonthDelayed.Text = objPenalitySettings.MoreThanOneMonthDelayedDeclaration;
                txtNoDeclaringYearlyIncome.Text = objPenalitySettings.NotDeclaringYearlyIncome;
                txtOneMonthUnderStatement.Text = objPenalitySettings.OneMonthUnderStatement;
                txtTwoMonthUnderStatement.Text = objPenalitySettings.TwoMonthUnderStatement;
                txtThreeMonthUnderStatement.Text = objPenalitySettings.ThreeMonthUnderStatement;
                txtNoFinancialRecord.Text = objPenalitySettings.NoFinancialRecord;
                txtBankInterestRate.Text = objPenalitySettings.BankInterestRate;
                txtRevenueInterestRate.Text = objPenalitySettings.RevenueInterestRate;
                cdpPenalityDateFrom.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objPenalitySettings.PenalityDateFrom));
                if (objPenalitySettings.PenalityDateTo != "01/01/1753")
                {
                    cdpPenalityDateTo.SelectedDate = string.IsNullOrEmpty(objPenalitySettings.PenalityDateTo)?string.Empty: EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objPenalitySettings.PenalityDateTo.ToString()));
                    clbPenalitySettings.JSProperties["cphasEndDate"] = "true";
                }
                clbPenalitySettings.JSProperties["cpAction"] = "LoadingPenalitySettingInfo";
                clbPenalitySettings.JSProperties["cpStatus"] = "success";
                List<int> objSettingId = objSettingsBusiness.GetSettingsByCategoryandDateFrom(Convert.ToInt32(SettingCategory.eSettingCategory.Penality), Convert.ToDateTime(objPenalitySettings.PenalityDateFrom));
                Session["PenalitySettingIdCollection"] = objSettingId;
            }
        }

        protected void clbPenalitySettings_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            DateTime penalitySettingDate = new DateTime();
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                penalitySettingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "SavePenalitySetting":
                    if (DoValidate())
                    {
                        DoSavePenalitySetting();
                    }
                    break;
                case "LoadPenalitySettingsInfo":
                    LoadPenalityeSettings(penalitySettingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Penality));
                    break;
                case "NewPenalitySetting":
                    DoNewPenalitySetting();
                    break;
                default:
                    break;
            }
        }

        protected void gvwPenality_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void LoadPenalitySettings()
        {
            SettingsBusiness objRentalSettingsBusiness = new SettingsBusiness();
            List<PenalitySettingsView> objPenalitySettingsList = objRentalSettingsBusiness.GetSettingsByPenalityCategory(Convert.ToInt32(SettingCategory.eSettingCategory.Penality));
            if (objPenalitySettingsList == null)
            {
                gvwPenality.DataSource = null;
                gvwPenality.DataBind();
                gvwPenality.JSProperties["cpAction"] = "LoadingPenalitySettingList";
                gvwPenality.JSProperties["cpStatus"] = "error";
                gvwPenality.JSProperties["cpMessage"] = "There are no penality settings registered";
            }
            else
            {
                gvwPenality.DataSource = objPenalitySettingsList;
                gvwPenality.DataBind();
                gvwPenality.JSProperties["cpAction"] = "LoadingPenalitySettingList";
                gvwPenality.JSProperties["cpStatus"] = "success";
            }
        }

        protected void DeletePenalitySetting(DateTime penalitySettingStartingDate)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            if (objSettingsBusiness.DeleteSettings(penalitySettingStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Penality)))
            {
                gvwPenality.JSProperties["cpAction"] = "DeletPenalitySetting";
                gvwPenality.JSProperties["cpStatus"] = "success";
                gvwPenality.JSProperties["cpMessage"] = "Penality settings deleted successfully";
            }
            else
            {
                gvwPenality.JSProperties["cpAction"] = "DeletPenalitySetting";
                gvwPenality.JSProperties["cpStatus"] = "error";
            }
        }

        protected void gvwPenality_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            DateTime penalitySettingStartingDate = new DateTime();
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains("|"))
                penalitySettingStartingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "LoadPenalitySettings":
                    LoadPenalitySettings();
                    break;
                case "DeletePenalitySetting":
                    DeletePenalitySetting(penalitySettingStartingDate);
                    break;
            }

        }
    }
}