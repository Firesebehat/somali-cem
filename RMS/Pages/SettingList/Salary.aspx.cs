﻿using System;
using System.Text;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using CUSTOR.Business;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using CUSTORCommon.Ethiopic;

namespace RMS.Pages.SettingsList
{
    public partial class Salary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();

            //Validate the ethiopic date
            if (cdpSalaryDateFrom.SelectedDate.Trim().Length == 0)
            {
                errMessages.Add("Please enter salary settings starting date!");
            }
            else
            {
                if (!cdpSalaryDateFrom.IsValidDate)
                    errMessages.Add("Please enter a valid salary settings starting date!");
                if (cdpSalaryDateFrom.IsFutureDate)
                    errMessages.Add("Starting date of salary settings should not upcoming date. Please try again!");
            }

            return errMessages;
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            //sb.Append(Resources.Message.ERR_GENERAL);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        protected bool DoValidate()
        {
            bool isValid = true;

            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                isValid = false;
                clbSalarySettings.JSProperties["cpAction"] = "registration";
                clbSalarySettings.JSProperties["cpStatus"] = "error";
                clbSalarySettings.JSProperties["cpMessage"] = GetErrorDisplay(errMessages);
            }

            return isValid;
        }

        protected List<Setting> AssignSalarySettingsToObject()
        {
            Dictionary<string, string> salarySettingsList = new Dictionary<string, string>();
            salarySettingsList.Add("TransportAllowancePercent", txtTransportAllowancePercent.Text);
            salarySettingsList.Add("TransportAllowanceInBirr", txtTransportAllowanceInBirr.Text);
            salarySettingsList.Add("ProvidentFund", txtProvidentFund.Text);
            salarySettingsList.Add("MonthlyCostSharing", txtCostSharing.Text);
            salarySettingsList.Add("DelegationAllowance", txtDelegationAllowance.Text);
            List<Setting> objSettingsList = new List<Setting>();
            foreach (KeyValuePair<string, string> agricuturSetting in salarySettingsList)
            {
                Setting objSettings = new Setting();
                objSettings.AppId = "SIRIM-AddsiAbeba";
                objSettings.Key = agricuturSetting.Key;
                objSettings.Value = agricuturSetting.Value;
                objSettings.DateFrom = cdpSalaryDateFrom.SelectedGCDate;
                if (cdpSalaryDateTo.SelectedDate != "")
                {
                    objSettings.DateTo = cdpSalaryDateTo.SelectedGCDate;
                }
                else
                {
                    objSettings.DateTo = Convert.ToDateTime("1/1/1753");
                }
                objSettings.Category = Convert.ToInt32(SettingCategory.eSettingCategory.Salary);
                objSettings.CenterId = 0;
                objSettings.CenterName = "";
                objSettingsList.Add(objSettings);
            }

            return objSettingsList;
        }

        protected void DoSaveSalary()
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            Setting objSetting = new Setting();
            List<Setting> objSalarySettings = AssignSalarySettingsToObject();
            int savedSalarySettings = 0;
            string message = "";
            if (!(bool)hfSalarySettingStatus["isNewSalarySetting"])
            {
                List<int> salarySettingsIdCollection = (List<int>)Session["SalarySettingIdCollection"];
                for (int index = 0; index < objSalarySettings.Count; index++)
                {
                    objSalarySettings[index].UpdatedBy ="";
                    objSalarySettings[index].UpdatedDate = DateTime.Now;
                    objSalarySettings[index].UpdatedUserId ="";
                    objSalarySettings[index].Id = salarySettingsIdCollection[index];
                    if (objSettingsBusiness.UpdateByKey(objSalarySettings[index]))
                    {
                        savedSalarySettings++;
                    }
                }
                message = "Salary settings registered successfully";
            }
            else
            {
                //number of keys that are under rental settings are 3
                List<int> objLatestSalarySettingsId = objSettingsBusiness.GetLatestSettingsId(Convert.ToInt32(SettingCategory.eSettingCategory.Salary), 5);
                for (int index = 0; index < objSalarySettings.Count; index++)
                {
                    objSalarySettings[index].CreatedBy = "";
                    objSalarySettings[index].CreatedDate = DateTime.Now;
                    objSalarySettings[index].CreatedUserId = "";
                    if (objSettingsBusiness.InsertSettings(objSalarySettings[index]))
                    {
                        savedSalarySettings++;
                    }
                }
                if (objLatestSalarySettingsId != null)
                {
                    for (int index = 0; index < objLatestSalarySettingsId.Count; index++)
                    {
                        objSetting.Id = objLatestSalarySettingsId[index];
                        objSetting.DateTo = cdpSalaryDateFrom.SelectedGCDate;
                        objSettingsBusiness.UpdateEndingDateofKey(objSetting);
                    }
                }
                message = "Salary settings updated successfully";
            }
            if (savedSalarySettings == objSalarySettings.Count)
            {
                clbSalarySettings.JSProperties["cpAction"] = "registration";
                clbSalarySettings.JSProperties["cpStatus"] = "success";
                clbSalarySettings.JSProperties["cpMessage"] = message;
                //DoNewSalarySetting();
            }
        }

        protected void DoNewSalarySetting()
        {
            txtTransportAllowancePercent.Text = string.Empty;
            txtTransportAllowanceInBirr.Text = string.Empty;
            txtProvidentFund.Text = string.Empty;
            txtDelegationAllowance.Text = string.Empty;
            txtCostSharing.Text = string.Empty;
            cdpSalaryDateFrom.SelectedDate = string.Empty;
            cdpSalaryDateTo.SelectedDate = string.Empty;
            hfSalarySettingStatus["isNewSalarySetting"] = true;
            clbSalarySettings.JSProperties["cpAction"] = "newSalarySetting";
            clbSalarySettings.JSProperties["cpStatus"] = "success";
        }

        protected void DoLoadSalarySettingsInfo(DateTime salarySettingDate, int category)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            SalarySettingsView objSalarySettings = objSettingsBusiness.GetSalarySettingsInfo(salarySettingDate, category);
            if (objSalarySettings != null)
            {
                txtTransportAllowancePercent.Text = objSalarySettings.TransportAllowancePercent;
                txtTransportAllowanceInBirr.Text = objSalarySettings.TransportAllowanceInBirr;
                txtProvidentFund.Text = objSalarySettings.ProvidentFund;
                txtDelegationAllowance.Text = objSalarySettings.DelegationAllowance;
                txtCostSharing.Text = objSalarySettings.MonthlyCostSharing;
                cdpSalaryDateFrom.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objSalarySettings.SalarySettingDateFrom));
                if (objSalarySettings.SalarySettingDateTo != "01/01/1753")
                {
                    cdpSalaryDateTo.SelectedDate = EthiopicDateTime.GetEthiopianDate(Convert.ToDateTime(objSalarySettings.SalarySettingDateTo.ToString()));
                    clbSalarySettings.JSProperties["cphasEndDate"] = "true";
                }
                clbSalarySettings.JSProperties["cpAction"] = "LoadingSalarySettingInfo";
                clbSalarySettings.JSProperties["cpStatus"] = "success";
                List<int> objSettingId = objSettingsBusiness.GetSettingsByCategoryandDateFrom(Convert.ToInt32(SettingCategory.eSettingCategory.Salary), Convert.ToDateTime(objSalarySettings.SalarySettingDateFrom));
                Session["SalarySettingIdCollection"] = objSettingId;
            }
        }

        protected void clbSalarySettings_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            DateTime salarySettingDate = new DateTime();
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                salarySettingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "SaveSalarySettings":
                    if (DoValidate())
                    {
                        DoSaveSalary();
                    }
                    break;
                case "LoadSalarySettingsInfo":
                    DoLoadSalarySettingsInfo(salarySettingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Salary));
                    break;
                case "NewSalarySetting":
                    DoNewSalarySetting();
                    break;
                default:
                    break;
            }
        }

        protected void DeleteSalarySetting(DateTime salarySettingStartingDate)
        {
            SettingsBusiness objSettingsBusiness = new SettingsBusiness();
            if(objSettingsBusiness.DeleteSettings(salarySettingStartingDate, Convert.ToInt32(SettingCategory.eSettingCategory.Salary)))
            {
                gvwSalary.JSProperties["cpAction"] = "DeletSalarySetting";
                gvwSalary.JSProperties["cpStatus"] = "success";
                gvwSalary.JSProperties["cpMessage"] = "Salary settings deleted successfully";
            }
            else
            {
                gvwSalary.JSProperties["cpAction"] = "DeletSalarySetting";
                gvwSalary.JSProperties["cpStatus"] = "error";
            }
        }

        protected void LoadSalarySettings()
        {
            SettingsBusiness objSalarySettingsBusiness = new SettingsBusiness();
            List<SalarySettingsView> objSalarySettingsList = objSalarySettingsBusiness.GetSettingsBySalaryCategory(Convert.ToInt32(SettingCategory.eSettingCategory.Salary));
            if (objSalarySettingsList == null)
            {
                gvwSalary.DataSource = null;
                gvwSalary.DataBind();
                gvwSalary.JSProperties["cpStatus"] = "error";
                gvwSalary.JSProperties["cpAction"] = "LoadingSalarySettingList";
                gvwSalary.JSProperties["cpMessage"] = "There are no salary settings registered";
            }
            else
            {
                gvwSalary.DataSource = objSalarySettingsList;
                gvwSalary.DataBind();
                gvwSalary.JSProperties["cpAction"] = "LoadingSalarySettingList";
                gvwSalary.JSProperties["cpStatus"] = "success";
            }
        }
        protected void gvwSalary_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            DateTime salaryStartingDate = new DateTime();
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains("|"))
                salaryStartingDate = Convert.ToDateTime(parameters[1]);
            switch (strParam)
            {
                case "LoadSalarySettings":
                    LoadSalarySettings();
                    break;
                case "DeleteSalarySetting":
                    DeleteSalarySetting(salaryStartingDate);
                    break;
            }
        }
        protected void gvwSalary_PageIndexChanged(object sender, EventArgs e)
        {

        }
    }
}