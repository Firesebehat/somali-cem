﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="LandUse.aspx.cs" Inherits="RMS.Pages.SettingList.LandUse" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            min-height: 600px;
            color: #F87C1D !important;
            margin-left: 20px !important;
            margin: auto;
            max-width: 900px;
        }

        .menu {
            padding: 15px;
        }

        h5 {
            margin-left: 20px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        div.hidden {
            display: none
        }
    </style>
    <script type="text/javascript">
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function DoNewLandUse(s, e) {
            clbLandUseSettings.PerformCallback('NewLandUseSetting');
            hfLandUseSettingStatus.Set("isNewLandUseSetting", true);
            $("#LandUseDetail").show();
            $("#LandUseSettingList").hide();
        }

        function DoSaveLandUseSettings(s, e) {
            var isValid = ASPxClientEdit.ValidateGroup('LandUse');
            if (isValid) {
                clbLandUseSettings.PerformCallback('SaveLandUseSettings');
            }
        }

        function DoDisplayLandUseSettings(s, e) {
            gvwLandUse.PerformCallback('LoadLandUseSettings');
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewLandUse();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveLandUseSettings();
            }
            else if (e.item.name === "mnuList") {
                DoDisplayLandUseSettings();
            }
        }


        function OnclbLandUseSettingsEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingLandUseSettingInfo') {
                    hfLandUseSettingStatus.Set("isNewLandUseSetting", false);
                    $("#LandUseDetail").show();
                    if (s.cphasEndDate === "true") {
                        lblLandUseDurationTo.SetVisible(true);
                        $("#landUseSettingEndingSetting").show();
                    }
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true);
                }
                else if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                    ToggleEnabled("mnuSave", false);
                    ToggleEnabled("mnuList", true);
                    gvwLandUse.PerformCallback('LoadLandUseSettings');
                }
                else if (s.cpAction == "newLandUseSetting") {
                    $("#LandUseDetail").show();
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true)
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    $("#LandUseDetail").show();
                    ShowError(s.cpMessage);
                }
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
            s.hasEndDate = "";
        }


        function OnGetLandUserSettingSelectedFieldValues(values) {
            clbLandUseSettings.PerformCallback('LoadLandUseSettingsInfo' + '|' + values);
        }

        function DoSelectedLandUserSettingsRow(e) {
            gvwLandUse.GetRowValues(e.visibleIndex, "LandUseDateFrom", OnGetLandUserSettingSelectedFieldValues);
        }

        function DeleteLandUseSetting(value) {
            gvwLandUse.PerformCallback('DeleteLandUseSetting' + '|' + value);
            return;
        }

        function OnGetDeletedLandUseRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete land use settings?',
                function (result) {
                    if (result) DeleteLandUseSetting(Value);
                }, 'en');
        }

        function gvwLandUse_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID != 'Delete') return;
            if (e.buttonID == 'Edit') {
                lblLandUseDurationTo.SetVisible(false);
                $("#landUseSettingEndingSetting").hide();
                DoSelectedLandUserSettingsRow(e);
            }
            if (e.buttonID == 'Delete') {
                rowVisibleIndex = e.visibleIndex;
                gvwLandUse.GetRowValues(e.visibleIndex, 'LandUseDateFrom', OnGetDeletedLandUseRowValues);
            }
        }

        function gvwLandUseEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingLandUseSettingList') {
                    gvwLandUse.SetVisible(true);
                    $("#LandUseDetail").hide();
                    $("#LandUseSettingList").show();
                    ToggleEnabled("mnuSave", false);
                }
                if (s.cpAction == 'DeleteLandUseSetting') {
                    ShowSuccess(s.cpMessage);
                    gvwLandUse.PerformCallback('LoadLandUseSettings');
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == 'LoadingLandUseSettingList') {
                    ShowError(s.cpMessage);
                }
            }
        }

        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }

        //$(document).ready(function () {
        //    gvwLandUse.PerformCallback('LoadLandUseSettings');
        //});
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <dx:ASPxCallbackPanel ID="clbLandUseSettings" ClientInstanceName="clbLandUseSettings" Theme="Material" runat="server"
        Width="1000px" OnCallback="clbLandUseSettings_Callback" Height="100%">
        <ClientSideEvents EndCallback="OnclbLandUseSettingsEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True">
                <h5>Land use</h5>
                <div class="card card-body" style="margin-left: 20px">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                    <div id="LandUseDetail" class="hidden" style="margin-top: 20px">
                        <div class="row">
                            <div class="col col-md-12">
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblNonCooperativesAssociation" runat="server" Text="Non-Cooperatives association(Birr)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblNonCooperativesAssociationStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblCooperativesAssociation" runat="server" Text="Cooperatives association(Birr)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblCooperativesAssociationStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxLabel ID="lblMonthlyDelayedDeclaration" runat="server" Text="Delayed monthly declaration(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblMonthlyDelayedDeclarationStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtNonCooperativesAssociation" runat="server" ClientInstanceName="txtNonCooperativesAssociation"
                                            Width="100%" Font-Names="Nyala" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="LandUse">
                                                <RequiredField ErrorText="የህብረት ስራ ማህበር አባል ላልሆኑ ባዶ መሆን አይችልም።" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtCooperativesAssociation" runat="server" ClientInstanceName="txtCooperativesAssociation"
                                            Width="100%" Font-Names="Nyala" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="LandUse">
                                                <RequiredField ErrorText="የህብረት ስራ ማህበር አባል ለሆኑ ባዶ መሆን አይችልም።" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <dx:ASPxTextBox ID="txtMonthlyDelayedDeclaration" runat="server" ClientInstanceName="txtTransportAllowancePercent"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="LandUse">
                                                <RequiredField ErrorText="በመዝግየት በየወሩ ባዶ መሆን አይችልም።" IsRequired="True" />
                                            </ValidationSettings>
                                            <ClientSideEvents Validation="ValidateSettingValue" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblLandUseDurationFrom" runat="server" Text="Starting date"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblLandUseDurationFromStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2" style="margin-left: -30px">
                                        <dx:ASPxLabel ID="lblLandUseDurationTo" ClientInstanceName="lblLandUseDurationTo" ClientVisible="false" runat="server" Text="Ending date"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 w-25">
                                        <cdp:CUSTORDatePicker ID="cdpLandUseDateFrom" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                    <div id="landUseSettingEndingSetting" class="col-md-2 text-left hidden" style="margin-left: -30px">
                                        <cdp:CUSTORDatePicker ID="cdpLandUseDateTo" runat="server" Width="80px" SelectedDate="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="LandUseSettingList" class="hidden" style="margin-top: 20px">
                        <dx:ASPxGridView ID="gvwLandUse" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwLandUse" ClientVisible="false"
                            EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id"
                            OnCustomCallback="gvwLandUse_CustomCallback" OnPageIndexChanged="gvwLandUse_PageIndexChanged"
                            Width="100%" meta:resourcekey="gvwSalaryResource">
                            <ClientSideEvents CustomButtonClick="gvwLandUse_CustomButtonClick" EndCallback="gvwLandUseEndCallback" />
                            <SettingsBehavior ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                                <dx:GridViewDataTextColumn FieldName="NonCooperativeAssociation" Caption="Non-Cooperatives association(Birr)" VisibleIndex="1">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="CooperativeAssociation" Caption="Non-Cooperatives association(Birr)" VisibleIndex="2">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="MonthlyDelayedDeclaration" Caption="Delayed monthly declaration(%)" VisibleIndex="3">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="LandUseDateFromEthiopic" Caption="Starting date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="LandUseDateFrom" Visible="false" Caption="Starting date" VisibleIndex="4">
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="4">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                        <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            <Styles Style-ForeColor="Red">
                                                <Style ForeColor="Red"></Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                            </Columns>
                            <Styles AdaptiveDetailButtonWidth="22">
                                <SelectedRow BackColor="Orange" ForeColor="White">
                                </SelectedRow>
                            </Styles>
                        </dx:ASPxGridView>
                    </div>
                </div>
                <uc1:Alert runat="server" ID="Alert1" />
                <dx:ASPxHiddenField runat="server" ClientInstanceName="hfLandUseSettingStatus" ID="hfLandUseSettingStatus">
                </dx:ASPxHiddenField>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
