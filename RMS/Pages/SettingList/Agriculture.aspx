﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Root.master" AutoEventWireup="true" CodeBehind="Agriculture.aspx.cs" Inherits="RMS.Pages.SettingList.Agriculture" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .category {
            min-height: 600px;
            color: #F87C1D !important;
            margin-left: 20px !important;
            margin: auto;
            max-width: 900px;
        }

        .menu {
            padding: 15px;
        }

        h5 {
            margin-left: 20px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: left;
            color: #009688;
        }

        div.hidden {
            display: none
        }
    </style>
    <script type="text/javascript">
        function GetMenuItem(mnuName) {
            return mnuSettingToolbar.GetItemByName(mnuName);
        }

        function ToggleEnabled(mnuName, bEnable) {
            GetMenuItem(mnuName).SetEnabled(bEnable);
        }

        function onMenuInit(s, e) {
            ToggleEnabled("mnuSave", false);
            ToggleEnabled("mnuNew", true);
            ToggleEnabled("mnuList", true);
        }

        function DoNewAgriculture() {
            hfAgricultureSettingStatus.Set("isNewAgricultureSetting", true);
            clbAgricultureSettings.PerformCallback('NewAgricultureSetting');
        }

        function DoDisplaySettings() {
            gvwAgriculture.PerformCallback('LoadAgriculturalSettings');
        }

        function DoSaveSettings() {
            var isValid = ASPxClientEdit.ValidateGroup('Agriculture');
            if (isValid) {
                clbAgricultureSettings.PerformCallback('SaveAgriculture');
            }
        }

        function onSettingMenuItemClicked(s, e) {
            if (e.item.name === "mnuNew") {
                DoNewAgriculture();
            }
            else if (e.item.name === "mnuSave") {
                DoSaveSettings();
            }
            else if (e.item.name === "mnuList") {
                DoDisplaySettings();
            }
        }

        function OnAgricultureClientEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction == 'LoadingAgriculturSettingInfo') {
                    $("#AgricultureDetail").show();
                    ToggleEnabled("mnuSave", true);
                    hfAgricultureSettingStatus.Set("isNewAgricultureSetting", false);
                    if (s.cphasEndDate === "true") {
                        lblDrurationTo.SetVisible(true);
                        $("#endingDate").show();
                    }
                }
                else if (s.cpAction == "registration") {
                    ShowSuccess(s.cpMessage);
                    ToggleEnabled("mnuList", false);
                    gvwAgriculture.PerformCallback('LoadAgriculturalSettings');
                }
                else if (s.cpAction == "newAgricultureSetting") {
                    $("#AgricultureDetail").show();
                    ToggleEnabled("mnuSave", true);
                    ToggleEnabled("mnuList", true);
                }
            }
            else if (s.cpStatus == "error") {
                if (s.cpAction == "registration") {
                    ShowError(s.cpMessage);
                    $("#AgricultureDetail").show();
                }
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
            s.hasEndDate = "";
            s.cphasEndDate = "";
        }

        function OnGetAgricultureSettingSelectedFieldValues(values) {
            $("div#AgricultureDetail").removeClass("hidden");
            clbAgricultureSettings.PerformCallback('LoadAgricultureSetting' + '|' + values);
        }

        function DeleteAgriculturalSetting(value) {
            gvwAgriculture.PerformCallback('DeleteAgriculturalSetting' + '|' + value);
            return;
        }

        function OnGetDeletedAgriculturalRowValues(Value) {
            ShowConfirmx('Are you sure you want to delete agricultural settings?',
                function (result) {
                    if (result) DeleteAgriculturalSetting(Value);
                }, 'en');
        }

        function DoSelectedAgricultureSettingsRow(e) {
            gvwAgriculture.GetRowValues(e.visibleIndex, "AgricultureDateFrom", OnGetAgricultureSettingSelectedFieldValues);
        }

        function gvwAgriculture_CustomButtonClick(s, e) {
            if (e.buttonID != 'Edit' && e.buttonID!='Delete') return;
            if (e.buttonID == 'Edit') {
                $("#endingDate").show();
                lblDrurationTo.SetVisible(true);
                DoSelectedAgricultureSettingsRow(e);
            }
            if (e.buttonID == 'Delete') {
                rowVisibleIndex = e.visibleIndex;
                gvwAgriculture.GetRowValues(e.visibleIndex, 'AgricultureDateFrom', OnGetDeletedAgriculturalRowValues);       
            }
        }

        function gvwAgricultureEndCallback(s, e) {
            if (s.cpStatus == "success") {
                if (s.cpAction === 'LoadingAgriculturSettingList') {
                    gvwAgriculture.SetVisible(true);
                    $("#AgricultureDetail").hide();
                    $("#AgriculturSettingList").show();
                    ToggleEnabled("mnuSave", false);
                    ToggleEnabled("mnuList", true);
                }
                if (s.cpAction === 'DeletAgriculturalSetting') {
                    ShowSuccess(s.cpMessage);
                    gvwAgriculture.PerformCallback('LoadAgriculturalSettings');
                }
            }
            else if (s.cpStatus === "error") {
                if (s.cpAction === 'LoadingAgriculturSettingList') {
                    ShowError(s.cpMessage);                
                }
            }
        }

        function ValidateSettingValue(s, e) {
            var settingValue = e.value;
            if (!((settingValue => 1) && (settingValue <= 100))) {
                e.isValid = false;
                e.errorText = "The setting value should be between 1 and 100";
                return;
            }
        }

        //$(document).ready(function () {
        //    gvwAgriculture.PerformCallback('LoadAgriculturalSettings');
        //});
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageContent" runat="server">
    <dx:ASPxCallbackPanel ID="clbAgricultureSettings" ClientInstanceName="clbAgricultureSettings" Theme="Material" runat="server"
        Width="1000px" OnCallback="clbAgricultureSettings_Callback" Height="100%" meta:resourcekey="clbAgricultureSettingsDetailsResource1">
        <ClientSideEvents EndCallback="OnAgricultureClientEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="pnlMain" runat="server" SupportsDisabledAttribute="True">
                <h5>Agricultural Profit Tax</h5>
                <div class="card card-body" style="margin-left: 20px">
                    <div>
                        <dx:ASPxMenu ID="mnuSettingToolbar" AllowSelectItem="True" Orientation="Horizontal" runat="server"
                            ShowPopOutImages="True" EnableTheming="True" Height="35px"
                            ClientInstanceName="mnuSettingToolbar" Font-Names="Visual Geez Unicode"
                            AutoPostBack="false" meta:resourcekey="mnuToolbarResource1">
                            <ClientSideEvents Init="onMenuInit" ItemClick="onSettingMenuItemClicked"></ClientSideEvents>
                            <Items>
                                <dx:MenuItem Name="mnuNew" Text="New" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="iconbuilder_actions_add_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuSave" Text="Save" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image IconID="save_save_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                                <dx:MenuItem Name="mnuList" Text="Show List" Selected="true" Enabled="true" meta:resourcekey="MenuItemResource3">
                                    <ItemStyle Paddings-PaddingTop="7px" Paddings-PaddingBottom="5px" Height="35px"></ItemStyle>
                                    <Image  IconID="businessobjects_bo_appearance_svg_16x16">
                                    </Image>
                                </dx:MenuItem>
                            </Items>
                        </dx:ASPxMenu>
                    </div>
                    <div id="AgricultureDetail" class="hidden" style="margin-top: 20px">
                        <div class="row">
                            <div class="col col-md-12">
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblAgricultureCategoryCode" runat="server" Text="Category code"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblAgricultureCategoryCodeStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblAgricultureLateMonthlyTaxDeclaration" runat="server" Text="Monthly late declaration(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblAgricultureLateMonthlyTaxDeclarationStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblAgricultureNoArchiveRecord" runat="server" Text="No monthly financial record(%)"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblAgricultureNoArchiveRecordStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblDurationFrom" runat="server" Text="Starting date"></dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblDurationFromStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></dx:ASPxLabel>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblDrurationTo" ClientInstanceName="lblDrurationTo" ClientVisible="false" runat="server" Text="Ending date"></dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <dx:ASPxTextBox ID="txtAgriculturCategoryRate" runat="server" ClientInstanceName="txtAgriculturCategoryRate"
                                            Width="100%" Font-Names="Nyala" MaxLength="6" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="None" ErrorTextPosition="Bottom" ValidationGroup="Agriculture">
                                                <RequiredField ErrorText="Category code can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxTextBox ID="txtAgricultureTaxLateDeclaration" runat="server" ClientInstanceName="txtAgricultureTaxLateDeclaration"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ClientSideEvents Validation="ValidateSettingValue"/> 
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Agriculture">
                                                <RequiredField ErrorText="Monthly late declaration can not be empty" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <dx:ASPxTextBox ID="txtAgricultureNoArchiveRecord" runat="server" ClientInstanceName="txtAgricultureNoArchiveRecord"
                                            Width="100%" Font-Names="Nyala" MaxLength="3" onkeypress="return AcceptNumericOnly(event,true)">
                                            <ClientSideEvents Validation="ValidateSettingValue"/> 
                                            <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Bottom" ValidationGroup="Agriculture">
                                                <RequiredField ErrorText="No monthly financial record can not be empty " IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="col-md-2">
                                        <cdp:CUSTORDatePicker ID="cdpDateFrom" runat="server" Width="100%" SelectedDate="" TextCssClass="" />
                                    </div>
                                    <div id="endingDate" class="col-md-2 hidden">
                                        <cdp:CUSTORDatePicker ID="cdpDateTo" runat="server" Width="100%" SelectedDate="" TextCssClass="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="AgriculturSettingList" class="hidden" style="margin-top: 20px">
                        <div class="row">
                            <div class="col col-md-12">
                                <dx:ASPxGridView ID="gvwAgriculture" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwAgriculture" ClientVisible="false"
                                    EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small" KeyFieldName="Id"
                                    OnCustomCallback="gvwAgriculture_CustomCallback" OnPageIndexChanged="gvwAgriculture_PageIndexChanged"
                                    Width="100%" meta:resourcekey="gvwAgricultureResource">
                                    <ClientSideEvents CustomButtonClick="gvwAgriculture_CustomButtonClick" EndCallback="gvwAgricultureEndCallback" />
                                    <SettingsBehavior ProcessFocusedRowChangedOnServer="false" ProcessSelectionChangedOnServer="false" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="0" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="AgricultureCategoryCode" Caption="Category code" VisibleIndex="1">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="AgricultureTaxDelayFine" Caption="Monthly late declaration(%)" VisibleIndex="2">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="AgricultureTaxArchiveFine" Caption="No monthly financial record" VisibleIndex="3">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="AgricultureDateFromEthiopic" Caption="Starting date" VisibleIndex="4">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Visible="false" FieldName="AgricultureDateFrom" Caption="Starting date" VisibleIndex="4">
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewCommandColumn meta:resourcekey="GridViewAdvertisementCommandColumnResource1" VisibleIndex="4">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit" meta:resourcekey="GridViewAdvertisementCommandColumnCustomButtonResource3" />
                                                <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            <Styles Style-ForeColor="Red">
                                                <Style ForeColor="Red"></Style>
                                            </Styles>
                                        </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                        </dx:GridViewCommandColumn>
                                    </Columns>
                                    <Styles AdaptiveDetailButtonWidth="22">
                                        <SelectedRow BackColor="Orange" ForeColor="White">
                                        </SelectedRow>
                                    </Styles>
                                </dx:ASPxGridView>
                            </div>
                        </div>
                    </div>
                    <uc1:Alert runat="server" ID="Alert1" />
                    <dx:ASPxHiddenField runat="server" ClientInstanceName="hfAgricultureSettingStatus" ID="hfAgricultureSettingStatus">
                    </dx:ASPxHiddenField>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
