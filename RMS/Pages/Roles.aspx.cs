﻿using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace RMS.Pages
{
    public partial class Roles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack && !IsCallback)
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                Session["IsNewRole"] = true;
            }
            BindRole();
        }

        private void BindRoleOld()
        {

            try
            {
                DataTable RoleList = new DataTable();
                RoleList.Columns.Add("Role Name");
                RoleList.Columns.Add("User Count");

                string[] allRoles = System.Web.Security.Roles.GetAllRoles();
                foreach (string roleName in allRoles)
                {
                    int numberOfUsersInRole = System.Web.Security.Roles.GetUsersInRole(roleName).Length;
                    string[] roleRow = { roleName, numberOfUsersInRole.ToString() };
                    RoleList.Rows.Add(roleRow);
                }
                grdRole.DataSource = RoleList;
                grdRole.DataBind();

            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void BindRole()
        {
            UserBussiness business = new UserBussiness();
            List<Role> roles = business.GetRoles();
            grdRole.DataSource = roles;
            grdRole.DataBind();
        }

        protected void grdRole_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0];
            if (e.Parameters.Contains('|'))
                strID = parameters[1];
            if(strParam == "Delete")
            {
                DoDeleteRole(strID);
            }
            else if (strParam == "Save")
            {
                DoSaveRole();
            }
            BindRole();
        }

        private void DoSaveRole()
        {
            clbClient.JSProperties["cpAction"] = "Save";
            if(txtRole.Text == string.Empty)
            {
                ShowError("Please enter role name");
                return;
            }
            
            bool isNewRole = (bool)Session["IsNewRole"];
            try
            {
               string strRole = txtRole.Text;
                if (isNewRole)
                    System.Web.Security.Roles.CreateRole(strRole);
                UpdateRoleDescription(strRole, txtRoleDescription.Text);
               ShowSuccess("Role is added successfully");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoDeleteRole(string RoleName)
        {
            grdRole.JSProperties["cpAction"] = "Delete";
            try
            {
                System.Web.Security.Roles.DeleteRole(RoleName);
                ShowSuccess("Role is deleted successfully");
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }

        protected void grdRole_PageIndexChanged(object sender, EventArgs e)
        {
            BindRole();
        }
        protected void UpdateRoleDescription(string roleName,string description)
        {
            aspnet_RolesBusiness business = new aspnet_RolesBusiness();
            business.UpdateRoleDescription(roleName,description);
        }

        protected void clbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string id = "0";
            string[] parameters = e.Parameter.Split('|');
            if (parameters.Length > 1)
                id = parameters[1];
            switch (parameters[0])
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSaveRole();
                    break;
                case "Edit":
                    DoFind(id);
                    break;
            }
        }

        private void DoFind(string roleName)
        {
            clbClient.JSProperties["cpAction"] = "Edit";
            aspnet_RolesBusiness business = new aspnet_RolesBusiness();
            try
            {
                aspnet_Roles role = business.Getaspnet_Roles(roleName);
                txtRole.Text = role.RoleName;
                txtRoleDescription.Text = role.Description;
                txtRole.ClientEnabled = false;
                Session["IsNewRole"] = false;
                ShowSuccess("");
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoNew()
        {
            clbClient.JSProperties["cpAction"] = "New";
            Session["IsNewRole"] = true;
            txtRole.Text = string.Empty;
            txtRoleDescription.Text = string.Empty;
            txtRole.ClientEnabled = true;
            txtRole.IsValid = true;
            txtRoleDescription.IsValid = true;
            ShowSuccess("");
        }
    }
}