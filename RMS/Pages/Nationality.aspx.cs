﻿using CUSTOR.Business;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace RMS.Pages
{
    public partial class Nationality : System.Web.UI.Page
    {
        public int Lan { get { return 0; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsCallback)
            {
                //UpdateNationlitySort();
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                if (!(HttpContext.Current.User.IsInRole("Super Admin") || HttpContext.Current.User.IsInRole("Administrator")))
                {
                    Response.Redirect("~/AccessDenied");
                }
                Session["NewNationality"] = true;
                pnlData.ClientVisible = false;
                UpdateNationlitySort();
            }
        }
       

        protected void ClbClient_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Search":
                    BindGrid();
                    clbClient.JSProperties["cpAction"] = "List";
                    ShowSuccess(string.Empty);
                    break;
                case "Edit":
                    DoFind(Convert.ToInt32(strID));
                    break;
                case "Delete":
                    DoDelete(Convert.ToInt32(strID));
                    break;
                case "Cancel":
                    DoNew();
                    clbClient.JSProperties["cpAction"] = "Cancel";
                    clbClient.JSProperties["cpStatus"] = "SUCCESS";
                    break;
                default:
                    break;
            }
        }

        protected bool DoFind(int ID)
        {
            NationalityBusiness objNationalityBusiness = new NationalityBusiness();

            CUSTOR.Domain.Nationality objNationality = new CUSTOR.Domain.Nationality();

            try
            {
                clbClient.JSProperties["cpAction"] = "Find";
                objNationality = objNationalityBusiness.GetNationality(ID);
                if (objNationality == null)
                {
                    ShowMessage("No Record is found.");
                    return false;
                }
                Session["NewNationality"] = false;
                ClearForm();
                pnlData.ClientVisible = true;
                pnlGrid.ClientVisible = false;
                txtAmharic.Text = objNationality.Amdescription;
                txtEnglish.Text = objNationality.Description;
                Session["ID"] = ID;
                ShowSuccess(string.Empty);
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Find");
                return false;
            }
        }
        private void ShowError(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
        }
        private void ShowError(string strMsg, string strAction)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "ERROR";
            clbClient.JSProperties["cpAction"] = strAction;
        }
        private void ShowMessage(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "INFO";
        }
        private void ShowSuccess(string strMsg)
        {
            clbClient.JSProperties["cpMessage"] = strMsg;
            clbClient.JSProperties["cpStatus"] = "SUCCESS";
        }
        protected bool DoSave()
        {
            CUSTOR.Domain.Nationality objNationality = new CUSTOR.Domain.Nationality();

            try
            {
                //Page.Validate();
                if ((!Page.IsValid)) return false;

                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {

                    ShowError(GetErrorDisplay(errMessages), "Save");
                    return false;
                }
                clbClient.JSProperties["cpAction"] = "Save";
                CVGeez objGeez = new CVGeez();

                objNationality.Amdescription = txtAmharic.Text;
                objNationality.Description = txtEnglish.Text;
                objNationality.AmDescriptionSortValue = objGeez.GetSortValueU(txtAmharic.Text);
                objNationality.AmDescriptionSoundxValue = objGeez.GetSoundexValue(txtAmharic.Text);

                NationalityBusiness objNationalityBusiness = new NationalityBusiness();
                bool bNew = ((bool)Session["NewNationality"]);

                if (bNew)
                {
                    objNationality.Code = objNationalityBusiness.GeNextCode();
                    if (!objNationalityBusiness.Exists(txtEnglish.Text, objGeez.GetSortValueU(txtAmharic.Text)))
                    {
                        objNationalityBusiness.InsertNationality(objNationality);
                    }
                    else
                    {
                        ShowError("Record exists with the same name. Try another name!", "Save");
                        return false;
                    }
                }
                else
                {
                    objNationality.Id = Int32.Parse(Session["ID"].ToString());
                    if (!objNationalityBusiness.Exists(txtEnglish.Text, objGeez.GetSortValueU(txtAmharic.Text), objNationality.Id))
                    {
                        objNationalityBusiness.UpdateNationality(objNationality);
                    }
                    else
                    {
                        ShowError("Record exists with the same name. Try another name!", "Save");
                        return false;
                    }
                }
                Session["NewNationality"] = false;
                clbClient.JSProperties["cpAction"] = "Save";
                ShowSuccess("Record is saved successfully.");
                BindGrid();
                ClearForm();
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Save");
                return false;
            }
        }
        protected void ClearForm()
        {

            txtAmharic.Text = "";
            txtEnglish.Text = "";
            txtAmharic.IsValid = true;
            txtEnglish.IsValid = true;
        }
        protected bool DoNew()
        {
            try
            {
                Session["NewNationality"] = true;
                pnlData.ClientVisible = true;
                pnlGrid.ClientVisible = false;
                ClearForm();
                clbClient.JSProperties["cpAction"] = "New";
                ShowSuccess(string.Empty);

                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }
        protected bool DoDelete(int Id)
        {
            Nationality objNationality = new Nationality();
            NationalityBusiness objNationalityBusiness = new NationalityBusiness();
            try
            {
                objNationalityBusiness.Delete(Id);
                Session["NewNationality"] = false;
                BindGrid();
                clbClient.JSProperties["cpAction"] = "Delete";
                ShowSuccess("Record is deleted successfully.");
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "Delete");
                return false;
            }
        }

        private void BindGrid()
        {
            try
            {
                gridNationality.DataSource = null;
                gridNationality.DataBind();
                NationalityBusiness objNationalityBusiness = new NationalityBusiness();
                List<CUSTOR.Domain.Nationality> l = new List<CUSTOR.Domain.Nationality>();
                l = objNationalityBusiness.GetNationalitys();
                if (l != null && l.Count > 0)
                {
                    gridNationality.DataSource = l;
                    gridNationality.DataBind();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message, "List");
            }
        }

        private string GetErrorDisplay(List<string> strMessages)
        {
            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(strMessages);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();


            if (txtAmharic.Text == string.Empty)
            {
                errMessages.Add("Enter Nationality description in Amharic!");
            }
            if (txtEnglish.Text == string.Empty)
            {
                errMessages.Add("Enter Nationality description in English");
            }
            NationalityBusiness objReg = new NationalityBusiness();
            bool bNew = (bool)Session["NewNationality"];


            return errMessages;
        }

        protected void gvNationality_PageIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void gvNationality_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            //BindGrid();
        }

        protected void gvNationality_Init(object sender, EventArgs e)
        {
            if (Page.IsCallback)
                BindGrid();
        }
        private void UpdateNationlitySort()
        {
            CVGeez objGeez = new CVGeez();
            NationalityBusiness business = new NationalityBusiness();
            List<CUSTOR.Domain.Nationality> nationalitys = business.GetNationalitys();

            foreach (var n in nationalitys)
            {
                n.AmDescriptionSortValue = objGeez.GetSortValueU(n.Amdescription);
                n.AmDescriptionSoundxValue = objGeez.GetSoundexValue(n.Amdescription);
                business.UpdateNationality(n);
            }
        }
    }
}
