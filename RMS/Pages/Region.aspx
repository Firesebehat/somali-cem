﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Region.aspx.cs" Inherits="RMS.Pages.Region" MasterPageFile="~/Root.master" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">


        function DoNewClientClick(s, e) {

            clbClient.PerformCallback('New');
        }

        function DoSaveClientClick() {
            if (!ASPxClientEdit.ValidateGroup('Registration')) {
                return;
            }
            clbClient.PerformCallback('Save');
        }

            function DoSearchClientClick(s, e) {
                gvRegion.PerformCallback('Search');

        }

        function DoCancelClient() {
            //DoListView();
            btnListClient.SetEnabled(true);
            btnNewClient.SetEnabled(true);
            pnlData.SetVisible(false);
        }

        function DoAddClientClick(s, e) {
            pnlData.SetVisible(true);
            pnlGrid.SetVisible(false);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);

            DoNewClientClick();
        }
        function DoListClientClick(s, e) {
            pnlData.SetVisible(false);
            pnlGrid.SetVisible(true);
            btnNewClient.SetEnabled(true);
            btnListClient.SetEnabled(false);
            DoSearchClientClick();


        }
        function DoDeleteImageClick() {
            ShowPopup('deleteimage');
        }
        var rowVisibleIndex;
        var strID;
        function gvRegion_CustomButtonClick(s, e) {

            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID == 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Code', ShowPopup);
                //ShowError("test");
            }
            if (e.buttonID == 'EDITING') {

                gvRegion.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
                btnNewClient.SetEnabled(false);
                btnListClient.SetEnabled(true);
            }
        }

        function OnGetSelectedFieldValues(values) {


            clbClient.PerformCallback('Edit' + '|' + values);


        }

        function ShowPopup(values) {

            ShowConfirmx('Are you sure do you want to delete this record?', function (result) {
                if (result)
                    Delete(values);
            },'en');
        }
        function Delete(values) {
            btnSaveClient.SetEnabled(true);
            clbClient.PerformCallback('Delete' + '|' + values);
        }
        function OnClientEndCallback(s, e) {

            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Cancel") {
                    DoListView();
                    return;
                }
                else if (s.cpAction == "Delete") {
                    ShowSuccess(s.cpMessage);
                    DoListView();
                    return;
                }
                else if (s.cpAction == "Find") {
                    pnlGrid.SetVisible(false);
                }
                else if (s.cpAction == "List") {
                    DoListView();
                }

                else if (s.cpAction == "New") {
                    DoEditView();
                    return;
                }
                else if (s.cpMessage == '') {
                    return;
                }
                if (s.cpAction == "Save") {

                    ShowSuccess(s.cpMessage);
                    DoListView();
                }

            }
            else if (s.cpStatus === "ERROR") {
                ShowError(s.cpMessage);
                if (s.cpAction == "Delete" || s.cpAction == "Find" || s.cpAction == "List") {
                    return;
                }

                if (s.cpAction == "Save") {
                    DoEditView();
                }

            }
            else
                pnlData.SetVisible(false);
            s.cpStatus = '';
            s.cpMessage = '';
        }

      

       

        

        function DeleteGridRow(visibleIndex) {
            var index = gvRegion.GetFocusedRowIndex();

            gvRegion.DeleteRow(index);
        }

        function DoListView() {
            pnlGrid.SetVisible(true);
            btnSaveClient.SetEnabled(false);
            btnNewClient.SetEnabled(true);
            btnListClient.SetEnabled(false);
            //alert('e.buttonID');
        }
        function DoEditView() {
            pnlData.SetVisible(true);
            btnSaveClient.SetEnabled(true);
            pnlGrid.SetVisible(false);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);
            //alert('e.buttonID');
        }

    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            //DoListClientClick();
        };
    </script>
    <style>
        td{
            vertical-align:top;
        }
        .star
         {
             color:red;
             margin:1px;
             font-size:large;
         }
    </style>
   
    <div class="contentmain">
        <h5>
            Region Maintenance
        </h5>
        <div class="card card-body">
        <script src="../Content/ModalDialog.js"></script>
        <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
       
                <div style="padding-bottom:15px">

                   
                        <dx:ASPxButton ID="btnCategoryNewClick" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient"
                            HorizontalAlign="Center" Text="New">
                            <ClientSideEvents Click="DoAddClientClick" />
                        </dx:ASPxButton>

                        <dx:ASPxButton ID="btnList" runat="server" AutoPostBack="False" ClientInstanceName="btnListClient" 
                            HorizontalAlign="Center" Text="Show List">
                            <ClientSideEvents Click="DoListClientClick" />
                        </dx:ASPxButton>
                </div>
      
            <div>

                <dx:ASPxCallbackPanel ID="clbClient" OnCallback="ClbClient_Callback" ClientInstanceName="clbClient" runat="server" style="max-width:700px">

                    <ClientSideEvents EndCallback="function(s,e){OnClientEndCallback(s,e);}" />
                    <PanelCollection>
                        <dx:PanelContent ID="pnlCategory" runat="server"  SupportsDisabledAttribute="True">
                            <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" Width="100%" ShowHeader="false">
                                <PanelCollection>
                                    <dx:PanelContent>

                                        <table style="width:100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEnglish" runat="server" Text="Description (English):"></asp:Label>
                                                    <asp:Label ID="EnglishStar" runat="server" Text="*" EnableTheming="false" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtEnglish" runat="server" Width="250px">
                                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter English name." IsRequired="True" />
                                                       <RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />
                                                            </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblAmharic" runat="server" Text="Description (Amharic):"></asp:Label>
                                                    <asp:Label ID="AmharicStar" runat="server" Text="*" EnableTheming="false" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtAmharic" runat="server" Width="250px">
                                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" ErrorTextPosition="Bottom" Display="Dynamic">
                                                            <RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />
                                                            <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>
                                                   
                                                           
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td><td></td>
                                                <td>
                                                   <div style="float:right">
                                                     
                                                                <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False"  ClientInstanceName="btnCancelClient" BackColor="Silver" Text="Cancel" CausesValidation="false">
                                                                    <ClientSideEvents Click="DoCancelClient" />
                                                                </dx:ASPxButton>&nbsp;&nbsp;
                                                                <dx:ASPxButton ID="btnCategorySaveClick" runat="server" CausesValidation="true" AutoPostBack="False"  Text="Save" ClientInstanceName="btnSaveClient" style="float:right">
                                                                    <ClientSideEvents Click="DoSaveClientClick" />
                                                                </dx:ASPxButton>
                                                                
                                                            </div>
                                                </td>
                                            </tr>
                                        </table>


                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>

                           
                            <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="100%" ClientVisible="false">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <dx:ASPxGridView ID="gvRegion" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvRegion" EnableTheming="True"   OnInit="gvRegion_Init"
                                            KeyFieldName="Code"  Visible="true"  Width="100%" OnPageIndexChanged="gvRegion_PageIndexChanged" OnCustomCallback="gvRegion_CustomCallback">
                                            <ClientSideEvents CustomButtonClick="gvRegion_CustomButtonClick"  />
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="ID" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="1" Visible="false">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="English" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Amharic" FieldName="AmDescription" ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="9" Width="100px">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Select">
                                                        </dx:GridViewCommandColumnCustomButton>
                                                        <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                             <Styles Style-ForeColor="Red"></Styles>
                                                        </dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                </dx:GridViewCommandColumn>
                                            </Columns>
                                            <SettingsPager PageSize="6"></SettingsPager>
                                        </dx:ASPxGridView>

                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
               

            </div>
        </div>
    </div>
</asp:Content>

