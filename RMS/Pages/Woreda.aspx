﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Woreda.aspx.cs" Inherits="RMS.Pages.Woreda" MasterPageFile="~/Root.master" %>

<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">

        function OnRegionChanged(cboRegion) {
            cboZone.ClearItems();
            cboZone.PerformCallback(cboRegion.GetValue().toString());
        }

        function OnRegionFindChanged(s) {
            cboZoneFind.PerformCallback(s.GetValue().toString());
        }

        function DoNewClientClick(s, e) {

            clbClient.PerformCallback('New');
        }

        function DoSaveClientClick() {
            if (!ASPxClientEdit.ValidateGroup('Registration')) {
                return;
            }
            clbClient.PerformCallback('Save');
        }

        function DoSearchClientClick(s, e) {
            clbClient.PerformCallback('Search');
        }

        function DoCancelClient() {
            pnlData.SetVisible(false);
            pnlGrid.SetVisible(false);
            btnNewClient.SetEnabled(true);
            btnListClient.SetEnabled(true);
        }

        function DoAddClientClick(s, e) {
            //pnlData.SetVisible(true);
            //pnlGrid.SetVisible(false);
            ////IsZoneSelected = (cboZoneFind.GetValue() != null);
            //btnNewClient.SetEnabled(true);
            //btnListClient.SetEnabled(true);
            //cboZone.SetValue(cboZoneFind.GetValue());
            //cboRegion.SetValue(cboRegionFind.GetValue());
            //clearForm();
            DoNewClientClick("New");
        }
        function DoListClientClick(s, e) {
            if (cboRegionFind.GetValue() == null) {
                ShowError('Select Region and Zone');
                gvWoreda.SetVisible(false);
                pnlData.SetVisible(false);
                pnlGrid.SetVisible(true);

                return;
            }
            else if (cboZoneFind.GetValue() == null) {
                ShowError('Select Zone');
                gvWoreda.SetVisible(false);
                pnlData.SetVisible(false);
                pnlGrid.SetVisible(true);
                return;
            }
            gvWoreda.SetVisible(true);
            gvWoreda.PerformCallback();
        }
        function DoDeleteImageClick() {
            ShowPopup('deleteimage');
        }
        function Show_Click() {
            if (cboZoneFind.GetValue() === null || cboZoneFind.GetValue().toString() === '-1') {
                ShowError("Please select Zone");
                return;
            }


            gvWoreda.PerformCallback();
        }
        var rowVisibleIndex;
        var strID;
        function gvWoreda_CustomButtonClick(s, e) {

            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID == 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Code', ShowPopup);
                //ShowError("test");
            }
            if (e.buttonID == 'EDITING') {

                gvWoreda.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
                btnNewClient.SetEnabled(false);
                btnListClient.SetEnabled(true);
            }
        }
        function DoDoubleClick(s, e) {
            gvWoreda.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);
        }
        function OnGetSelectedFieldValues(values) {

            pnlData.SetVisible(true);
            btnSaveClient.SetEnabled(true);
            pnlGrid.SetVisible(false);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);
            //alert('e.buttonID');
            clbClient.PerformCallback('Edit' + '|' + values);


        }

        function ShowPopup(values) {

            ShowConfirmx('Are you sure do you want to delete this record?', function (result) {
                if (result)
                    Delete(values);
            }, 'en');
        }
        function Delete(values) {
            btnSaveClient.SetEnabled(true);
            clbClient.PerformCallback('Delete' + '|' + values);
        }

        function OnClientEndCallback(s, e) {
            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == "Cancel") {
                    pnlGrid.SetVisible(false);
                    return;
                }
                else if (s.cpAction == "Delete") {
                    ShowSuccess(s.cpMessage);
                    doListMode();
                    return;
                }
                else if (s.cpAction == "Find") {
                    pnlData.SetVisible(true);
                    pnlGrid.SetVisible(false);
                }
                else if (s.cpAction == "List") {
                    doListMode();
                }
                else if (s.cpAction == "Save") {
                    ShowSuccess(s.cpMessage);
                    //DoNewClientClick();
                    doListMode();
                }

                else if (s.cpAction == "New") {
                    pnlData.SetVisible(true);
                    pnlGrid.SetVisible(false);
                    btnSaveClient.SetEnabled(true);
                }
                if (s.cpMessage == '') {
                    return;
                }

            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);
                if (s.cpAction == "Delete" || s.cpAction == "Find" || s.cpAction == "List") {
                    return;
                }

                else if (s.cpAction == "Save") {
                    pnlGrid.SetVisible(false);
                    pnlData.SetVisible(true);
                }
                else if (s.cpAction == "List") {
                    pnlGrid.SetVisible(true);
                    pnlData.SetVisible(false);
                }
            }
            else if (s.cpAction == "List") {
                if (s.cpStatus == 'NoRecord') {
                    ShowError('No record found');
                    pnlGrid.SetVisible(true);
                    pnlData.SetVisible(false);
                }
            }
            else
                pnlData.SetVisible(false);


        }



        function OnGetSelectedDeleteField(values) {
            btnSaveClient.SetEnabled(true);
            clbClient.PerformCallback('Delete' + '|' + values);
        }

        function DeleteGridRow(visibleIndex) {
            var index = gvWoreda.GetFocusedRowIndex();

            gvWoreda.DeleteRow(index);
        }
        function doListMode() {
            btnNewClient.SetEnabled(true);
            btnListClient.SetEnabled(false);
            pnlGrid.SetVisible(true);
            pnlData.SetVisible(false);
            gvWoreda.SetVisible(true);
        }
        function clearForm() {
            //cboZone.SetValue(null);
            txtEnglish.SetText('');
            txtAmharic.SetText('');
        }
        function gvWeredaEndCallback(s)
        {
            if (s.cpStatus === "NoRecord") {
                ShowError('No record was found');
            }
            doListMode();

        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            //btnSaveClient.SetEnabled(false);
            //pnlGrid.SetVisible(false);
            //btnNewClient.SetEnabled(false);
            //DoNewClick();

            // DoListClientClick();
        };

    </script>
    <style>
        td {
            vertical-align: top;
        }

        .star {
            color: red;
            margin: 1px;
            font-size: large;
        }
    </style>

    <div class="contentmain">
        <h5>Subcity Maintenance
        </h5>
        <div class="card card-body">
            <uc1:Alert runat="server" ID="Alert" style="z-index: 1000000 !important" />
            <script src="../Content/ModalDialog.js"></script>
            <div style="padding-bottom: 15px">

                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnWoredaNewClick" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient"
                                Text="New">
                                <ClientSideEvents Click="DoAddClientClick" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnList" runat="server" AutoPostBack="False" ClientInstanceName="btnListClient" ClientEnabled="true"
                                Text="Show List" Font-Names="Visual Geez Unicode">
                                <ClientSideEvents Click="DoListClientClick" />
                            </dx:ASPxButton>

                        </td>
                    </tr>
                </table>

            </div>

            <%-- Data Entry Table--%>
            <dx:ASPxCallbackPanel ID="clbClient" OnCallback="ClbClient_Callback" ClientInstanceName="clbClient" runat="server" Style="max-width: 700px" Width="100%">
                <ClientSideEvents EndCallback="OnClientEndCallback" />
                <PanelCollection>
                    <dx:PanelContent ID="pnlWoreda" runat="server" SupportsDisabledAttribute="True">
                        <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" ShowHeader="false" Width="100%" ClientVisible="false">
                            <PanelCollection>
                                <dx:PanelContent>
                                    <%-- Data Entry Table--%>
                                    <table style="width: 100%">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblWoreda" runat="server" Text="Parent Region:"></asp:Label>
                                                <asp:Label ID="codeStar0" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                            <td class="auto-style6">
                                                <dx:ASPxComboBox runat="server" ClientInstanceName="cboRegion" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" ID="cboRegion" Width="300px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnRegionChanged(s); }" />
                                                </dx:ASPxComboBox>

                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Zone/City:"></asp:Label>
                                                <asp:Label ID="Label2" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                            <td class="auto-style6">
                                                <dx:ASPxComboBox ID="cboZone" runat="server" ClientInstanceName="cboZone" EnableIncrementalFiltering="True"
                                                    EnableSynchronization="False" IncrementalFilteringMode="StartsWith" OnCallback="cboZone_Callback" Width="300px" />
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblEnglish" runat="server" Text="Description (English):"></asp:Label>
                                                <asp:Label ID="EnglishStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtEnglish" runat="server" Width="300px" ClientInstanceName="txtEnglish">
                                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                                        <RequiredField ErrorText="Please enter English name." IsRequired="True" />
                                                        <RegularExpression ValidationExpression="^[^-\s][a-zA-Z\s]+$" ErrorText="Only english letters are allowed" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblAmharic" runat="server" Text="Description (Amharic):"></asp:Label>
                                                <asp:Label ID="AmharicStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                            <td class="auto-style6">
                                                <dx:ASPxTextBox ID="txtAmharic" runat="server" Width="300px" ClientInstanceName="txtAmharic">
                                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                                        <RequiredField ErrorText="Please enter Amharic name." IsRequired="True" />
                                                        <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.]+$" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td colspan="2"></td>
                                            <td>

                                                <dx:ASPxButton ID="btnWoredaSaveClick" runat="server" CausesValidation="true" AutoPostBack="False" ClientInstanceName="btnSaveClient"
                                                    Text="Save" Style="float: right">
                                                    <ClientSideEvents Click="DoSaveClientClick" />
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" ClientInstanceName="btnCancelClient" Text="Cancel" BackColor="Silver" CausesValidation="false">
                                                    <ClientSideEvents Click="DoCancelClient" />
                                                </dx:ASPxButton>

                                            </td>

                                        </tr>
                                    </table>

                                    <%-- Data Entry Table--%>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxRoundPanel>


                        <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="700px" ClientVisible="true">
                            <PanelCollection>
                                <dx:PanelContent>
                                    <%--Gridview table--%>

                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Zone/City"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxComboBox ID="cboRegionFind" ClientInstanceName="cboRegionFind" runat="server" Width="180px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnRegionFindChanged(s); }" />
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cboZoneFind" runat="server" ClientInstanceName="cboZoneFind" EnableIncrementalFiltering="True" EnableSynchronization="False" IncrementalFilteringMode="StartsWith" OnCallback="cboZoneFind_Callback" Width="300px" />

                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnFind" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="Show" VerticalAlign="Middle">
                                                    <ClientSideEvents Click="Show_Click" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>


                                    <dx:ASPxGridView ID="gvWoreda" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvWoreda" EnableTheming="True" OnInit="gvWoreda_Init"
                                        KeyFieldName="Code" ClientVisible="false" Width="100%" OnPageIndexChanged="gvWoreda_PageIndexChanged" OnCustomCallback="Grd_CustomCallback">
                                        <ClientSideEvents CustomButtonClick="gvWoreda_CustomButtonClick" EndCallback="gvWeredaEndCallback" />
                                        <ClientSideEvents RowDblClick="DoDoubleClick" />
                                        <SettingsPager PageSize="6">
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" />
                                        <SettingsBehavior AllowFocusedRow="True" />

                                        <SettingsPopup>
                                            <HeaderFilter MinHeight="140px"></HeaderFilter>
                                        </SettingsPopup>
                                        <SettingsSearchPanel Visible="True" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="Id" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="3" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="English" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Amharic" FieldName="AmDescription" ShowInCustomizationForm="True" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="9" Width="100px" ShowClearFilterButton="True">
                                                <CustomButtons>
                                                    <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Select">
                                                    </dx:GridViewCommandColumnCustomButton>
                                                    <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                        <Styles Style-ForeColor="Red">
                                                            <Style ForeColor="Red">
                                                                        </Style>
                                                        </Styles>
                                                    </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dx:GridViewCommandColumn>
                                        </Columns>
                                    </dx:ASPxGridView>

                                    <%--Gridview table--%>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxPanel>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>

            <%--Gridview table--%>
        </div>
    </div>
</asp:Content>
