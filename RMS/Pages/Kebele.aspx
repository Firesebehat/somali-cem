﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Kebele.aspx.cs" Inherits="RMS.Pages.Kebele" MasterPageFile="~/Root.master" %>
<%@ Register Src="~/CommonControls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <script type="text/javascript">
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            this.history.pushState(null, null, document.URL);
        });
    </script>
    <script type="text/javascript">

        function OnRegionChanged(cboRegion) {
            cboZone.ClearItems();
            cboWoreda.ClearItems();
            cboZone.PerformCallback(cboRegion.GetValue().toString());
        }

        function OnRegionFindChanged(cboRegionFind) {
            cboZoneFind.ClearItems();
            cboWoredaFind.ClearItems();
            cboZoneFind.PerformCallback(cboRegionFind.GetValue().toString());
        }

        function OnZoneChanged(cboZone) {
            cboWoreda.ClearItems();
            cboWoreda.PerformCallback(cboZone.GetValue().toString());
        }

        function OnZoneFindChanged(cboZoneFind) {
            cboWoredaFind.ClearItems();
            cboWoredaFind.PerformCallback(cboZoneFind.GetValue().toString());
        }

        function DoNewClientClick(s, e) {

            clbClient.PerformCallback('New');
        }

        function DoSaveClientClick() {
            if (!ASPxClientEdit.ValidateGroup('Registration')) {
                return;
            }

            clbClient.PerformCallback('Save');
            //}
        }

        function DoSearchClientClick(s, e) {
            clbClient.PerformCallback('Search');
        }

        function DoCancelClient() {
           // clbClient.PerformCallback('Search');
            doListMode();
        }

        function DoAddClientClick1() {
            if (cboWoredaFind.GetValue() == null || cboWoredaFind.GetValue() === '-1') {
                ShowError("Please select Woreda");
                return;
            }
           
            clbClient.PerformCallback('New');
        }
        function DoListClientClick(s, e) {
            clbClient.PerformCallback('Search');
            doListMode();

        }
        function DoDeleteImageClick() {
            ShowPopup('deleteimage');
        }
        function Show_Click() {

            //Alert(cboWoredaFind.GetValue());
            // if (cboRegionFind.GetValue() === null || cboRegionFind.GetValue().toString() === '-1') {
            //    ShowError("Please select Region,Zone and Woreda");
            //    return;
            //}
            //else if (cboZoneFind.GetValue() === null || cboZoneFind.GetValue().toString() === '-1') {
            //    ShowError("Please select Zone and Woreda");
            //    return;
            //}
            if (cboWoredaFind.GetValue() === null || cboWoredaFind.GetValue().toString() === '-1') {
                ShowError("Please select Woreda");
                return;
            }
            gvKebele.PerformCallback();
        }
        var rowVisibleIndex;
        var strID;
        function gvKebele_CustomButtonClick(s, e) {

            if (e.buttonID !== 'DELETING' && e.buttonID !== 'EDITING') return;
            if (e.buttonID === 'DELETING') {
                rowVisibleIndex = e.visibleIndex;
                s.GetRowValues(e.visibleIndex, 'Code', ShowPopup);
                //ShowError("test");
            }
            if (e.buttonID == 'EDITING') {

                gvKebele.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
                btnNewClient.SetEnabled(false);
                btnListClient.SetEnabled(true);
            }
        }
        function DoDoubleClick(s, e) {
            gvKebele.GetRowValues(e.visibleIndex, "Code", OnGetSelectedFieldValues);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);
        }
        function OnGetSelectedFieldValues(values) {
            clbClient.PerformCallback('Edit' + '|' + values);
        }
        function doEditMode() {
            pnlData.SetVisible(true);
            btnSaveClient.SetEnabled(true);
            pnlGrid.SetVisible(false);
            btnNewClient.SetEnabled(false);
            btnListClient.SetEnabled(true);
        }
        function doListMode() {
            pnlData.SetVisible(false);
            btnSaveClient.SetEnabled(false);
            pnlGrid.SetVisible(true);
            IsWoredaSelected = (cboWoredaFind.GetValue() != null);
            btnNewClient.SetEnabled(IsWoredaSelected);
            btnListClient.SetEnabled(IsWoredaSelected);
        }
        function ShowPopup(values) {

            ShowConfirmx('Are you sure do you want to delete this record?', function (result) {
                if (result)
                    Delete(values);
            },'en');
        }
        function Delete(values) {
            btnSaveClient.SetEnabled(true);
            clbClient.PerformCallback('Delete' + '|' + values);
        }

        function OnClientEndCallback(s, e) {

            if (s.cpStatus === "SUCCESS") {
                if (s.cpAction === "Clear") {
                    doListMode();
                    return;
                }
                else if (s.cpAction === "Cancel") {
                    doListMode();
                    return;
                }
                else if (s.cpAction === "Delete") {
                    ShowSuccess(s.cpMessage);
                    doListMode();
                    s.cpMessage = "";
                    return;
                }
                else if (s.cpAction === "Find") {
                    doEditMode();
                    return;
                }
                else if (s.cpAction === "List") {
                    doListMode();
                    if (s.cpStatus === "SUCCESS")
                        s.SetVisible(true);
                    else {
                        s.SetVisible(false);
                    }
                    return;
                }


                else if (s.cpAction === "Save") {
                    ShowSuccess(s.cpMessage);
                    doListMode();
                    s.cpMessage = "";
                    return;
                }
                else if (s.cpAction === "New") {
                    doEditMode();
                }

            }
            else if (s.cpStatus === "ERROR") {
                ShowError(s.cpMessage);
                s.cpMessage = "";
                if (s.cpAction === "Delete" || s.cpAction === "Find" || s.cpAction === "List") {
                    doListMode();
                    return;
                }
                if (s.cpAction === "Save") {
                    doEditMode();

                }

            }
            else if (s.cpStatus === "NOTFOUND") {
                if (s.cpAction === "List") {
                    ShowError('No Record is found');
                    s.cpAction = "";
                    gvKebele.SetVisible(false);
                }
            }
            else pnlData.SetVisible(false);

        }
        function DeleteGridRow(visibleIndex) {
            var index = gvKebele.GetFocusedRowIndex();

            gvKebele.DeleteRow(index);
        }

        function clearForm() {
            txtAmharic.SetText('');
            txtEnglish.SetText('');
        }
        function cboWeredaSelected(s) {
            if (s.GetValue() != null) {
                btnNewClient.SetEnabled(true);
                btnListClient.SetEnabled(true);
            }
        }
    </script>
    <script type="text/javascript">
        //function pageLoad(sender, args) {
        //    doListMode();
        //    gvKebele.SetVisible(false);
        //};
    </script>
   
    <div class="contentmain">
        <h5>
            Woreda Maintenance
        </h5>
        <div class="card card-body">
          <uc1:Alert runat="server" ID="Alert"  style="z-index:1000000 !important"/> 
        <script src="../Content/ModalDialog.js"></script>
         
                    <dx:ASPxCallbackPanel ID="clbClient" OnCallback="ClbClient_Callback" ClientInstanceName="clbClient" runat="server" Height="100%" style="max-width:500px">
                        <ClientSideEvents EndCallback="OnClientEndCallback" />
                        <PanelCollection>
                            <dx:PanelContent ID="pnlKebele" runat="server" CssClass="RoundPanel" SupportsDisabledAttribute="True">
                                <dx:ASPxRoundPanel runat="server" ID="pnlData" ClientInstanceName="pnlData" Width="700px" ShowHeader="false">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%-- Data Entry Table--%>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lblKebele" runat="server" Text="Parent Region:"></asp:Label>
                                                        <asp:Label ID="codeStar0" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox runat="server" ClientInstanceName="cboRegion" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" ID="cboRegion" Width="300px">
                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnRegionChanged(s); }" />
                                                        </dx:ASPxComboBox>

                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text="Parent Zone/City:"></asp:Label>
                                                        <asp:Label ID="Label2" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxComboBox ID="cboZone" runat="server" ClientInstanceName="cboZone" EnableIncrementalFiltering="True" EnableSynchronization="False"
                                                            IncrementalFilteringMode="StartsWith" OnCallback="cboZone_Callback" Width="300px">
                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnZoneChanged(s); }" />
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server" Text="Sub city:"></asp:Label>
                                                        <asp:Label ID="Label6" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxComboBox ID="cboWoreda" runat="server" ClientInstanceName="cboWoreda" EnableIncrementalFiltering="True" NullText="Select Subcity"
                                                            EnableSynchronization="False" IncrementalFilteringMode="StartsWith" OnCallback="cboWoreda_Callback" Width="300px">
                                                            <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                                                <RequiredField ErrorText="Please select subcity." IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                               
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lblAmharic" runat="server" Text="Description (Amharic):"></asp:Label>
                                                        <asp:Label ID="AmharicStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtAmharic" runat="server" Width="300px" ClientInstanceName="txtAmharic">
                                                            <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                                                <RequiredField ErrorText="Please enter Amharic description." IsRequired="True" />
                                                                <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.\d]+$" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                 <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lblEnglish" runat="server" Text="Description (English):"></asp:Label>
                                                        <asp:Label ID="EnglishStar" runat="server" EnableTheming="False" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxTextBox ID="txtEnglish" runat="server" Width="300px" ClientInstanceName="txtEnglish">
                                                            <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                                                <RequiredField ErrorText="Please enter the English description." IsRequired="True" />
                                                           <RegularExpression ValidationExpression="[a-zA-Z0-9 ]+$" ErrorText="Only english letters are allowed" />
                                                                </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td>
                                                        <dx:ASPxButton ID="btnKebeleSaveClick" runat="server" AutoPostBack="False" ClientInstanceName="btnSaveClient" Text="Save" Style="float: right">
                                                            <ClientSideEvents Click="DoSaveClientClick" />
                                                        </dx:ASPxButton>

                                                        <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" ClientInstanceName="btnCancelClient" Text="Cancel" CausesValidation="False" BackColor="Silver">
                                                            <ClientSideEvents Click="DoCancelClient" />
                                                        </dx:ASPxButton>
                                                    </td>

                                                </tr>
                                            </table>

                                            <%-- Data Entry Table--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <%--<Border BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" />--%>
                                </dx:ASPxRoundPanel>
                               
                                <dx:ASPxPanel runat="server" ID="pnlGrid" ClientInstanceName="pnlGrid" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--Gridview table--%>
                                             
                                                       <table style="width:500px">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label3" runat="server" Text="Zone/City"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label7" runat="server" Text="Sub city"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboRegionFind" ClientInstanceName="cboRegionFind" runat="server" Width="150px">
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { OnRegionFindChanged(s); }" />
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboZoneFind" runat="server" ClientInstanceName="cboZoneFind" EnableIncrementalFiltering="True" EnableSynchronization="False"
                                                                        IncrementalFilteringMode="StartsWith" OnCallback="cboZoneFind_Callback" Width="150px">
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { OnZoneFindChanged(s); }" />
                                                                    </dx:ASPxComboBox>

                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboWoredaFind" runat="server" ClientInstanceName="cboWoredaFind" EnableIncrementalFiltering="True" EnableSynchronization="False"
                                                                        IncrementalFilteringMode="StartsWith" OnCallback="cboWoredaFind_Callback" Width="150px"  NullText="Select Subcity">
                                                                        <ClientSideEvents SelectedIndexChanged="cboWeredaSelected" />
                                                                    </dx:ASPxComboBox>

                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnKebeleNewClick" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient" Font-Size="Small" ClientEnabled="false"
                                                                        Text="New" CausesValidation="False">
                                                                        <ClientSideEvents Click="DoAddClientClick1" />

                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnFind" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="Show List" CausesValidation="False" ClientVisible="false">
                                                                        <ClientSideEvents Click="Show_Click" />

                                                                    </dx:ASPxButton>
                                                                </td>
                                                                
                                                                <td>
                                                                    <dx:ASPxButton ID="btnList" runat="server" AutoPostBack="False" ClientInstanceName="btnListClient" Font-Size="Small" ClientEnabled="false"
                                                                        Text="Show List" CausesValidation="False">
                                                                        <ClientSideEvents Click="DoListClientClick" />
                                                                    </dx:ASPxButton>

                                                                </td>
                                                            </tr>
                                                            
                                        
                                            <tr>
                                                <td colspan="6">
                                           
                                            <div style="margin-top:10px;margin-bottom:10px;width:100%">
                                                                   
                                                                     <dx:ASPxGridView ID="gvKebele" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvKebele" EnableTheming="True"
                                                            KeyFieldName="Code" Visible="true" Width="100%" OnPageIndexChanged="gvKebele_PageIndexChanged" OnCustomCallback="Grd_CustomCallback" Font-Names="Visual Geez Unicode" Font-Size="Small">
                                                            <ClientSideEvents RowDblClick="DoDoubleClick" EndCallback="OnClientEndCallback" />
                                                            <ClientSideEvents CustomButtonClick="gvKebele_CustomButtonClick" />
                             
<SettingsPopup>
<HeaderFilter MinHeight="140px"></HeaderFilter>
</SettingsPopup>
                                                                         <SettingsSearchPanel Visible="True" />
                             
                                                                         <Columns>
                                                                <dx:GridViewDataTextColumn FieldName="ID" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="1" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                               
                                                                <dx:GridViewDataTextColumn Caption="English" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="2">
                                                                </dx:GridViewDataTextColumn>
                                                                 <dx:GridViewDataTextColumn Caption="Amharic" FieldName="AmDescription" ShowInCustomizationForm="True" VisibleIndex="3">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="9" Width="100px" ShowClearFilterButton="True">
                                                                    <CustomButtons>
                                                                        <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="Select">
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                        <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="Delete">
                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                    </CustomButtons>
                                                                </dx:GridViewCommandColumn>
                                                            </Columns>
                                                                         <Settings ShowFilterRow="True" />
                                                            <SettingsBehavior AllowFocusedRow="True" />
                                                            <SettingsPager PageSize="6">
                                                            </SettingsPager>
                                                        </dx:ASPxGridView>
                                                </div>
                                                    </td>
                                                </tr>
                                                           </table>
                                            <%--Gridview table--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                   

            </div>
    </div>
</asp:Content>

