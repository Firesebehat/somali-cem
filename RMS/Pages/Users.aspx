﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="RMS.Pages.Users" MasterPageFile="~/Root.master" %>
<%@ Register Src="~/Controls/Msg.ascx" TagPrefix="uc2" TagName="Msg" %>
<%@ Register Src="~/Controls/a-z-menu.ascx" TagPrefix="uc2" TagName="azmenu" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
            history.pushState(null, null, document.URL);
            window.addEventListener('popstate', function () {
                this.history.pushState(null, null, document.URL);
            });
        </script>
</asp:Content>

<asp:Content  ContentPlaceHolderID="PageContent" runat="Server">
    <%--<style>
        a{
            color:#009688 !important;
        }
    </style>--%>
    <script type="text/javascript">

</script>
   
     <div class="contentmain">
         <h5>
             Users Management
         </h5>
         <div class="card card-body" style="width:100%">
    <div style="text-align: center">
        <uc2:Msg runat="server" id="Msg" />
        <asp:Panel class="alert alert-info" runat="server" ID="pnlInfo" Visible="False">
            <asp:Label runat="server" ID="lblInfo" EnableTheming="False"></asp:Label>
        </asp:Panel>

    </div>
         <div style="padding-top:5px">
                <asp:Label ID="lblMsg" runat="server" BackColor="#FFCC00" Font-Bold="True"
                    ForeColor="White"></asp:Label>
         </div>
    <table>
        <tr>
            <td style="text-align: center; height: 16px;">
                <asp:Panel ID="pnlMenu" runat="server" BackImageUrl="~/Images/gray_menu_bg.jpg"
                    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Height="25px"
                    Width="100%" style="max-width:950px">

                    <table style="width: 100%">
                        <tr>
                            <td class="style4">&nbsp;
                            </td>
                            <td style="text-align: center;">
                                <asp:Menu ID="mnuMain" runat="server" BorderStyle="None" ForeColor="#009688"
                                    DynamicHorizontalOffset="2" OnMenuItemClick="mnuMain_MenuItemClick"
                                    Orientation="Horizontal" StaticSubMenuIndent="2px"
                                    Style="text-align: center; height: 20px;">
                                    <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
                                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                                    <DynamicMenuStyle BackColor="#FFFBD6" />
                                    <DynamicSelectedStyle BackColor="#FFCC66" />
                                    <Items>

                                        <asp:MenuItem Text="Unlock" Value="Unlock">
                                            
                                        </asp:MenuItem>
                                        <asp:MenuItem Text="Approve" Value="Approve"></asp:MenuItem>


                                        <asp:MenuItem Text="Reset Password" Value="Reset"></asp:MenuItem>


                                    </Items>
                                    <StaticHoverStyle BackColor="White" ForeColor="#003300" />
                                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                                    <StaticSelectedStyle BackColor="#FFCC66" />
                                </asp:Menu>
                            </td>
                            <td style="text-align: right">&nbsp;
                                                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Size="Medium"
                                                        ForeColor="#336699" Text="All Users"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td style="height: 16px">&nbsp;</td>
            <td style="height: 16px">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; height: 16px;">
                <table>
                    <tr>
                        <td>
                            <%--<dx:ASPxCheckBox ID="chActiveUserOnly" Text="Approved Users Only" ClientInstanceName="chActive" AutoPostBack="true" runat="server"  Checked="true" OnCheckedChanged="chActiveUserOnly_CheckedChanged">
                            </dx:ASPxCheckBox>--%>
                            <dx:ASPxComboBox ID="rdbIsActive" runat="server">
                                <Items>
                                    <dx:ListEditItem Text="Approved Users" Value="1" Selected="true" />
                                    <dx:ListEditItem Text="Not Approved Users" Value="0" />
                                    <dx:ListEditItem Text="All" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>&nbsp;</td>
                         <td style="vertical-align:bottom">
                            <dx:ASPxComboBox ID="cboSite" ClientInstanceName="cboSite" NullText="Select Site" Width="100%" runat="server" ValueType="System.String"></dx:ASPxComboBox>
                        </td>
                        <td style="text-align: right;vertical-align:bottom">
                            <dx:ASPxTextBox ID="txtFindUser" runat="server" NullText="Enter name"  Width="100%" OnTextChanged="txtFindUser_TextChanged" AutoPostBack="true">
                            </dx:ASPxTextBox>
                        </td>
                       
                        <td style="vertical-align:bottom">
                            <dx:ASPxButton ID="Button1" runat="server" OnClick="Button1_Click" Text="Find" AllowFocus="true"  >
                                
                            </dx:ASPxButton>
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td style="height: 16px">&nbsp;</td>
            <td style="height: 16px">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; height: 16px;">
                <uc2:azmenu runat="server" id="azmenu" />
            </td>
            <td style="height: 16px"></td>
            <td style="height: 16px"></td>
        </tr>
        <tr>
            <td style="text-align: right; height: 16px;">
                <asp:CheckBox ID="chkUnapproved" runat="server" Text="Unapproved  Only"
                    Visible="False" />
                &nbsp;<asp:CheckBox ID="chkLocked" runat="server" Text="Locked Only" Visible="False" />
            </td>
            <td style="height: 16px">&nbsp;</td>
            <td style="height: 16px">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align:top" class="style2">
                <asp:GridView ID="gvwUsers" runat="server"
                    AutoGenerateColumns="False" BorderColor="#666666" BorderStyle="Solid"
                    BorderWidth="1px" GridLines="Horizontal"
                    OnSelectedIndexChanged="gvwUsers_SelectedIndexChanged"
                    OnPageIndexChanging="Grid_PageIndexChanging" AllowPaging="True" AllowSorting="true" OnSorting="gvwUsers_Sorting"
                    PageSize="20" Width="100%" DataKeyNames="userName" Font-Names="Times New Roman" Font-Size="Small">
                    <RowStyle CssClass="rowstyle" />
                    <SortedAscendingHeaderStyle CssClass="sortasc" />
                    <SortedDescendingHeaderStyle CssClass="sortdesc" />
                    <Columns>

                        <asp:BoundField DataField="RowNumber" HeaderText="No."
                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px">
                            <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Del">
                            <HeaderTemplate>
                                <input id="chkAll" onclick="SelectAllCheckboxes('chkRows', this.checked);" runat="server" type="checkbox" title="Check all checkboxes" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRows" runat="server" ToolTip="Select user in this row." />
                            </ItemTemplate>
                            <ItemStyle Width="25px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="USER NAME" SortExpression="UserName">
                            <ItemTemplate>
                                <span class="gvShortcut"><a href='User?username=<%# Eval("UserName") %>' style="color:#009688 !important;">
                                    <%# Eval("UserName") %></a></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMAIL" SortExpression="Email">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                &nbsp;<a style="color:#009688 !important;" href='Mailto:<%# Eval("Email") %>' title="click to email from your computer"><%#Eval("Email")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="createdate" HeaderText="ACCOUNT START" SortExpression="createdate" />
                        <asp:BoundField DataField="lastlogindate" HeaderText="LAST LOGIN DATE" SortExpression="lastlogindate" />
                        <asp:BoundField DataField="LastLoggingIp" HeaderText="LAST LOGIN IP" />
                        <asp:CheckBoxField DataField="IsApproved" HeaderText="APPROVED?" SortExpression="IsApproved">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CheckBoxField>
                        <asp:CheckBoxField DataField="IsLockedOut" HeaderText="LOCKED OUT?" SortExpression="IsLockedOut">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CheckBoxField>
                    </Columns>
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="headerstyle" />
                    <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>
                </asp:GridView>
            </td>
            <td class="style2"></td>
            <td class="style2"></td>
        </tr>
        <tr>
            <td>

                <div style="text-align: center">

                    <asp:HyperLink ID="hlMsg" runat="server" EnableViewState="False" style="color:#009688 !important;"
                        Visible="False">[hlMsg]</asp:HyperLink>

                    <uc2:Msg ID="Msg2" runat="server" />

                    <div style="text-align: center">

                        <uc2:Msg ID="Msg3" runat="server" />
                        <asp:Label ID="lblRecords" runat="server" Font-Bold="True" Font-Size="Small"></asp:Label>

                    </div>

                </div>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="categoryID" runat="server" Text="%" Visible="False"></asp:Literal>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
   </div>
         </div>
</asp:Content>
