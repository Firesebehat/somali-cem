﻿using System.Web.Routing;

namespace Routing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("SignIn", "SignIn", "~/SignIn.aspx", false);
            routes.MapPageRoute("AccessDenied", "AccessDenied", "~/AccessDenied.aspx");
            routes.MapPageRoute("ChangePassword", "ChangePassword", "~/ChangePassword.aspx");
            routes.MapPageRoute("Default", "Default", "~/Default.aspx");

            routes.MapPageRoute("TaxCenter", "Admin/TaxCenter/{app}", "~/Pages/TaxCenter.aspx", false, new RouteValueDictionary { { "app", "en" } },
             new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Kebele", "Address/Kebele/{app}", "~/Pages/Kebele.aspx", false, new RouteValueDictionary { { "app", "en" } },
             new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Region", "Address/Region/{app}", "~/Pages/Region.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Woreda", "Address/Woreda/{app}", "~/Pages/Woreda.aspx", false, new RouteValueDictionary { { "app", "en" } },
             new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Zone", "Address/Zone/{app}", "~/Pages/Zone.aspx", false, new RouteValueDictionary { { "app", "en" } },
             new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("Lookup", "Lookup/{app}", "~/Pages/Lookup.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("LookupType", "LookupType/{app}", "~/Pages/LookupType.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("User", "Admin/User/{app}", "~/Pages/User.aspx", false, new RouteValueDictionary { { "app", "en" } },
          new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Users", "Admin/Users/{app}", "~/Pages/Users.aspx", false, new RouteValueDictionary { { "app", "en" } },
          new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("Roles", "Admin/Roles/{app}", "~/Pages/Roles.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            //Settings List
            routes.MapPageRoute("Agriculture", "Setting/Agriculture/{app}", "~/Pages/SettingList/Agriculture.aspx", false, new RouteValueDictionary { { "app", "en" } },
           new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("LandUse", "Setting/LandUse/{app}", "~/Pages/SettingList/LandUse.aspx", false, new RouteValueDictionary { { "app", "en" } },
         new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Penality", "Setting/Penality/{app}", "~/Pages/SettingList/Penality.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Rental", "Setting/Rental/{app}", "~/Pages/SettingList/Rental.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Salary", "Setting/Salary/{app}", "~/Pages/SettingList/Salary.aspx", false, new RouteValueDictionary { { "app", "en" } },
            new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            //settings
            routes.MapPageRoute("RevenueCodes", "Setting/RevenueCodes/{app}", "~/Pages/Settings/AccountCodes/ChartOfAccount.aspx", false, new RouteValueDictionary { { "app", "en" } },
         new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("PaymentAccount", "Setting/PaymentAccount/{app}", "~/Pages/Settings/AccountCodes/PaymentAccount.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("PenaltyRightOff", "Setting/PenaltyRightOff/{app}", "~/Pages/Settings/PenaltyRightOff.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("CategoryRate", "Setting/CategoryRate/{app}", "~/Pages/Settings/CategoryRate.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("IncomeTaxRate", "Setting/IncomeTaxRate/{app}", "~/Pages/Settings/IncomeTaxRate.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("Municipality", "Setting/Municipality/{app}", "~/Pages/Settings/Municipality.aspx", false, new RouteValueDictionary { { "app", "en" } },
       new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });


            routes.MapPageRoute("Exemption", "Setting/Exemption/{app}", "~/Pages/Settings/Exemption.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("TaxReduction", "Setting/TaxReduction/{app}", "~/Pages/Settings/TaxReduction.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("SiteTransfer", "Admin/SiteTransfer/{app}", "~/Pages/SiteTransfer.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("AdminSettings", "Setting/AdminSettings/{app}", "~/Pages/Settings/AdminSettings.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

            routes.MapPageRoute("Nationality", "Address/Nationality/{app}", "~/Pages/Nationality.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("MiscellaneousPayment", "Pages/MiscellaneousPayment/{app}", "~/Pages/MiscellaneousPayment.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });
            routes.MapPageRoute("AdvertismentType", "Pages/AdvertismentType/{app}", "~/Pages/AdvertismentType.aspx", false, new RouteValueDictionary { { "app", "en" } },
        new RouteValueDictionary { { "app", "am|en|am|fr|ar" } });

        }
    }
}