﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessCategoryControl.ascx.cs" Inherits="RMS.Controls.BusinessCategoryControl" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>

<script type="text/javascript">
    function DoNodeClick(s, e) {
        s.GetNodeValues(e.nodeKey, "Id;Description;Parent", ReturnSelectedPickCategoryRow);
    }
    function PopPickCategoryShow_Click() {
        PopPickCategory.Show();
    }
    function PopPickCategoryHide_Click() {
        PopPickCategory.Hide();
    }
    function DoSearchCodeClick(s, e) {

        clbPickCategory.PerformCallback('SearchCode');
    }
    function OnClientNodeClicked1(sender, args) {
        if (args.get_node()._hasChildren() == true)
            args.get_node().set_selected(false)
    }
    function ReturnSelectedPickCategoryRow(values) {
        if (values[2] === "0" || values[2] === "-1") return;//Parent node
        txtCaxtegory.SetText(values[1]);
        hdBusiness.Set('CategoryId', values[0]);
        popBusiness.Hide();
    }
    function SearchCategory() {
        trlCategorys.PerformCallback();
    }
    </script>
    <style type="text/css">
        .box-content {
            padding: 2px;
            margin: auto;
            text-align: center;
            max-width: 500px;
            padding-top: 20px;
        }
        td {
            padding: 2px;
        }
    </style>

    <table>
        <tbody>
            <tr>
                <td colspan="2">
                    <dx:ASPxTextBox runat="server" ID="txtSearch" ClientInstanceName="txtSearch" NullText="በዘርፉ ስም ወይም ኮድ ፈልግ" Style="float: left; width: 100%" meta:resourcekey="txtSearchResource1"></dx:ASPxTextBox>
                </td>
                <td>
                    <dx:ASPxButton runat="server" ID="btnSearch" ClientInstanceName="btnSearch" CausesValidation="False" AutoPostBack="False" ToolTip="ፈልግ" Width="50px"
                        Theme="Material" meta:resourcekey="btnSearchResource1">
                        <ClientSideEvents Click="SearchCategory" />
                        <Image IconID="iconbuilder_actions_zoom_svg_white_16x16">
                        </Image>
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <dx:ASPxTreeList ID="trlCategorys" runat="server"  AutoGenerateColumns="False" Width="100%" OnInit="trlCategorys_Init"   OnCustomDataCallback="trlCategorys_CustomDataCallback"
                        ClientInstanceName="trlCategorys" EnableTheming="True" KeyFieldName="Id" ParentFieldName="Parent" meta:resourcekey="trlCategorysResource1">
                        <ClientSideEvents NodeDblClick="DoNodeClick" />
                        <Columns>
                            <dx:TreeListTextColumn Caption="ኮድ" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="0" Width="15%">
                            </dx:TreeListTextColumn>
                            <dx:TreeListTextColumn Caption="የንግድ ዘርፍ" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1" Width="85%" meta:resourcekey="TreeListTextColumnResource1">
                            </dx:TreeListTextColumn>
                            <dx:TreeListTextColumn FieldName="Parent" Visible="false" meta:resourcekey="TreeListTextColumnResource2">
                            </dx:TreeListTextColumn>
                        </Columns>
                        <Settings GridLines="None" ShowFooter="false" VerticalScrollBarMode="Auto" />
                        <SettingsSelection Enabled="True" />
                        <SettingsBehavior AllowFocusedNode="True" />
                        <SettingsPager Mode="ShowPager" PageSize="30">
                            <PageSizeItemSettings Items="20, 30, 50" Visible="False">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsLoadingPanel ImagePosition="Top" />
                    </dx:ASPxTreeList>
                </td>

            </tr>
        </tbody>
    </table>
