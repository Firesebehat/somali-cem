﻿using CUSTOR.Bussiness;
using CUSTOR.Common;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMS.Controls
{
    public partial class ChartOfAccountControl : System.Web.UI.UserControl
    {
        public int Lan { get { return 1; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack && !Page.IsCallback)
            {

                FillParentcode();
            }
                
        }
        private void BindTree()
        {
            ChartOfAccountsBussiness account = new ChartOfAccountsBussiness();
            CVGeez objGeez = new CVGeez();
            string nameValue = string.Empty;
            string nameEng = string.Empty;
            if (txtNameSearch.Text != string.Empty)
            {
                nameValue = objGeez.GetSortValueU(txtNameSearch.Text);
                if (nameValue == string.Empty)
                    nameEng = txtNameSearch.Text;

            }

            string codeValue = txtCodeSearch.Text;
            int parentValue = cboParentSearch.Value != null && Int16.Parse(cboParentSearch.Value.ToString()) > 0 ? Convert.ToInt16(cboParentSearch.Value) : 0;
            try
            {
                List<ChartOfAccountsDomain> list = account.SearchChartOfAccounts(nameValue, nameEng, codeValue, parentValue);
                trlChartAccount.DataSource = list;
                trlChartAccount.DataBind();

            }
            catch
            {

            }
        }
        protected void trlChartAccount_CustomCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomCallbackEventArgs e)
        {
            BindTree();
        }
        private void FillParentcode()
        {

            string strParent = "Select case " + Lan + " when 1 then Name else NameEng end as Name, Id  FROM ChartOfAccounts WHERE (Parent = 0 or parent is null) Order by Name";
            string strCategory = "Select case " + Lan + " when 1 then Name else NameEng end as Name, Id  FROM ChartOfAccounts WHERE [Hirachy] = 0 and (Parent = 0 or parent is null) Order by Name";
            CDropdown.FillComboFromDB(cboParentSearch, strCategory, "Name", "Id");

        }

        protected void trlChartAccount_Init(object sender, EventArgs e)
        {
            if(Page.IsCallback)
                BindTree();
        }
    }
}