﻿using CUSTOR.Business;
using DevExpress.Web.ASPxTreeList;
using System;
using System.Collections.Generic;


namespace RMS.Controls
{
    public partial class BusinessCategoryControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (txtSearch.Text != string.Empty)
                BindTree();
             trlCategorys.SettingsSelection.Recursive = true;
        }
        public void BindTree()
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            List<CUSTOR.Domain.CategoryDomain> categories;
            if (txtSearch.Text != string.Empty)
            {
                DoSearchCategory(false);
                return;
            }
            try
            {
                categories = categoryBusiness.GetCategorys();

                trlCategorys.DataSource = categories;
                trlCategorys.DataBind();
            }
            catch
            {

            }
        }

        private void DoSearchCategory(bool otherService)
        {
            CategoryBusiness categoryBusiness = new CategoryBusiness();

            List<CUSTOR.Domain.CategoryDomain> categories;

            try
            {
                CVGeez obj = new CVGeez();
                string text = obj.GetSortValueU(txtSearch.Text);
                text = text == string.Empty ? txtSearch.Text : text.Replace("w1", ".");
                categories = categoryBusiness.SearchCategory(text,otherService);
                if (categories.Count > 0)
                {
                    trlCategorys.DataSource = categories;
                    trlCategorys.DataBind();
                }
                else
                {
                    trlCategorys.DataSource = null;
                    trlCategorys.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void trlCategorys_Init(object sender, EventArgs e)
        {
            if (txtSearch.Text != string.Empty)
                return;
                BindTree();
        }

        protected void trlCategorys_CustomDataCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomDataCallbackEventArgs e)
        {
            e.Result = trlCategorys.SelectionCount.ToString();
        }
        public void SelectTree(List<string> selectionListId)
        {
            foreach (var item in selectionListId)
            {
              TreeListNode node =  trlCategorys.FindNodeByKeyValue(item);
                if (node != null)
                {
                    node.Selected = true;
                    TreeListNode nodeParent = node.ParentNode;
                    if (nodeParent != null)
                        nodeParent.Expanded = true;
                }
            }
        }
        public void ResetTree()
        {
            trlCategorys.UnselectAll();
            trlCategorys.CollapseAll();
        }
    }
}