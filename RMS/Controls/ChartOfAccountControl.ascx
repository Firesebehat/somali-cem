﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartOfAccountControl.ascx.cs" Inherits="RMS.Controls.ChartOfAccountControl" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<script>
    function trlChartEndCallback(s) {

        s.SetVisible(true);
    }
</script>
<style>
    .dxmLite_Office365 .dxm-horizontal .dxm-image-l .dxm-content {
            padding-left: 0;
        }
        .dxtvControl_Metropolis .dxtv-ndSel 
        {
          background-color:  #009688;
        }
        .dxpLite_Metropolis .dxp-current 
        {
          background-color:  #009688;

        }
        .dxtlFocusedNode_Metropolis
        {
          background-color:  #009688;

        }
</style>
   <div class="col col-md-12">
                 <div >
               <div style="padding:4px">
                  
               <table>
                 
                   <tr>
                       <td style="width:33%">
                           <dx:ASPxTextBox runat="server" ID="txtCodeSearch" NullText="Code" ClientInstanceName="txtCodeSearch"></dx:ASPxTextBox>
                       </td>
                       <td style="width:33%">
                           <dx:ASPxComboBox runat="server" ID="cboParentSearch" Width="100%" NullText="Account type" ClientInstanceName="cboParentSearch"></dx:ASPxComboBox>
                       </td>
                       <td></td>
                   </tr>
                  
                    <tr>
                       <td colspan="2">
                           <dx:ASPxTextBox ID="txtNameSearch" runat="server" NullText="Account name" ClientInstanceName="txtNameSearch" Width="100%"></dx:ASPxTextBox>
                       </td>
                        <td>
                            <dx:ASPxButton runat="server" ID="btnSearchMain" ClientInstanceName="btnSearchMain" CausesValidation="false" AutoPostBack="false" Width="70px" Text="Search">
                                             <ClientSideEvents Click="function(){trlChartAccount.PerformCallback(txtNameSearch.GetText() + '|' + txtCodeSearch.GetText() + '|' + cboParentSearch.GetValue());}" />
                                  
                                <Image IconID="iconbuilder_actions_zoom_svg_white_16x16">
                                            </Image>
                            </dx:ASPxButton>
                        </td>
                   </tr>
               </table>
                   </div>

                     <dx:ASPxTreeList ID="trlChartAccount"   runat="server" AutoGenerateColumns="False"  OnCustomCallback="trlChartAccount_CustomCallback" OnInit="trlChartAccount_Init" ClientVisible="false"
                         ClientInstanceName="trlChartAccount" Theme="Metropolis" KeyFieldName="Id" ParentFieldName="Parent" Width="100%">
                         <ClientSideEvents EndCallback="function(s){ trlChartEndCallback(s);}" />
                         <SettingsPopup>
                             <HeaderFilter MinHeight="140px"></HeaderFilter>
                         </SettingsPopup>

                         <ClientSideEvents NodeDblClick ="function(s,e){DoNodeClick(s,e);}"  />

                         <Columns>
                             <dx:TreeListTextColumn Caption="Code" FieldName="Code" ShowInCustomizationForm="True" VisibleIndex="1" Width="15%">
                             </dx:TreeListTextColumn>
                             <dx:TreeListTextColumn Caption="Account Name" FieldName="Name" ShowInCustomizationForm="True" VisibleIndex="0">
                             </dx:TreeListTextColumn>
                             <dx:TreeListTextColumn FieldName="Id" Visible="false">
                             </dx:TreeListTextColumn>
                             <dx:TreeListTextColumn FieldName="Parent" Visible="false">
                             </dx:TreeListTextColumn>
                         </Columns>
                         <%--<Settings GridLines="None" ShowFooter="true" VerticalScrollBarMode="Auto"  />--%>
                         <SettingsBehavior AllowFocusedNode="True" />
                         <SettingsPager Mode="ShowPager" PageSize="13">
                             <PageSizeItemSettings Items="20, 30, 50" Visible="False">
                             </PageSizeItemSettings>
                         </SettingsPager>
                         <%--<SettingsLoadingPanel ImagePosition="Top" />--%>
                     </dx:ASPxTreeList>
                 </div>
           </div>