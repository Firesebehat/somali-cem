﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Msg.ascx.cs" Inherits="RMS.Controls.Msg" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <link href="Msg.css" type="text/css" rel="stylesheet"/>
<asp:Button ID="btnD" runat="server" Text="" Style="display: none" Width="0" Height="0" />
        <asp:Button ID="btnD2" runat="server" Text="" Style="display: none" Width="0" Height="0" />
        <asp:Panel ID="pnlMsg" runat="server" CssClass="mp" Style="display: none" Width="400px">
            <asp:Panel ID="pnlMsgHD" runat="server" CssClass="mpHd">
                &nbsp;Message
            </asp:Panel>
            <br />
            <asp:GridView ID="grvMsg" runat="server" ShowHeader="false" GridLines="None" Width="100%" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Image ID="imgErr" runat="server" ImageUrl="Images/err.png"
                                            Visible=' <%# (((RMS.Controls.Msg. Message)Container.DataItem).MessageType == RMS.Controls.Msg. enmMessageType.Error) ? true : false %>' />
                                        <asp:Image ID="imgSuc" runat="server" ImageUrl="Images/suc.png"
                                            Visible=' <%# (((RMS.Controls.Msg.Message)Container.DataItem).MessageType == RMS.Controls.Msg. enmMessageType.Success) ? true : false %>' />
                                        <asp:Image ID="imgAtt" runat="server" ImageUrl="Images/att.png"
                                            Visible=' <%# (((RMS.Controls.Msg.Message)Container.DataItem).MessageType == RMS.Controls.Msg. enmMessageType.Attention) ? true : false %>' />
                                        <asp:Image ID="imgInf" runat="server" ImageUrl="Images/inf.png"
                                            Visible=' <%# (((RMS.Controls.Msg.Message)Container.DataItem).MessageType == RMS.Controls.Msg. enmMessageType.Info) ? true : false %>' />
                                    </td>
                                    <td>
                                        <%# Eval("MessageText")%>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <br />
            <div class="mpClose">
                <div class="lbNavigaion">
                    <asp:LinkButton ID="btnOK"   runat="server" Text="OK (እሺ)" CausesValidation="false" Width="60px" />
                </div>
                <div class="lbNavigaion">
                <asp:LinkButton ID="btnPostOK" runat="server" Text="OK (እሺ)" CausesValidation="false" OnClick="btnPostOK_Click"
                    Visible="false" Width="60px" />
                    </div>
                <div class="lbNavigaion">
                <asp:LinkButton ID="btnPostCancel" CssClass="lbNavigaion" runat="server" Text="Cancel (አይ)" CausesValidation="false"
                    OnClick="btnPostCancel_Click" Visible="false" Width="85px" />
                    </div>
            </div>
            
            <br />
        </asp:Panel>
        <cc1:ModalPopupExtender ID="mpeMsg"    runat="server" TargetControlID="btnD"
            PopupControlID="pnlMsg" PopupDragHandleControlID="pnlMsgHD" BackgroundCssClass="mpBg"
            DropShadow="true" OkControlID="btnOK">
        </cc1:ModalPopupExtender>