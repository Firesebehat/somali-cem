﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Category.ascx.cs" Inherits="RMS.Controls.Category" %>
<%@ Register TagPrefix="cdp" Namespace="CUSTORDatePicker" Assembly="CUSTORDatePicker" %>

<style>
    .tdLable {
        text-align: right;
    }
</style>
<script type="text/javascript">
    function DoNew() {
        btnSave.SetEnabled(true);
        cblCategory.PerformCallback('New');
    }
    function DoSaveCategory() {
        if (!chbHasParent.GetChecked()) {
            cboParent.SetValue(-1);
        }
        if (!ASPxClientEdit.ValidateGroup('RCategory')) {
            // var parentWindow = window.parent;
            ShowError('Some entries are required');
            return;
        }
        cblCategory.PerformCallback('Save');
    }
    function OncblCategoryEndCallback(s, e) {
        if (s.cpStatus == "SUCCESS") {
            ShowSuccess(s.cpMessage);
            if (s.cpAction == "Delete") {
            }
            else if (s.cpAction == "Save") {
                btnSave.SetEnabled(false);
                popCategory.Hide();
                ShowSuccess(s.cpMessage);
                trlCategorys.PerformCallback();
            }
            else if (s.cpAction === 'Get') {
                if (s.cpOtherService) {
                    $('#divVehicle').slideUp(200);
                    chbOtherService.SetChecked(true);
                }
                else {
                    $('#divVehicle').slideDown(200);
                    chbOtherService.SetChecked(false);
                }

            }

        }
        else if (s.cpStatus == "ERROR") {
            if (s.cpMessage == '') { return; }
            ShowError(s.cpMessage);

        }
        else if (s.cpStatus == "INFO") {
            ShowMessage(s.cpMessage);
        }
        ClearJsProperty(s);
    }
    function ClearJsProperty(s) {
        s.cpStatus = s.cpMessage = s.cpAction = '';
    }

    function cblStatusEndCallback(s) {
        if (s.cpStatus === 'ERROR') {

            ShowError(s.cpMessage);
        }
        else if (s.cpStatus === 'SUCCESS') {
            ShowSuccess(s.cpMessage);
        }
        s.cpStatus = '';
    }
</script>

<div style="max-width: 500px">


    <dx:ASPxCallbackPanel runat="server" ID="cblCategory" ClientInstanceName="cblCategory" Width="100%" OnCallback="cblCategory_Callback">
        <ClientSideEvents EndCallback="OncblCategoryEndCallback" />
        <PanelCollection>
            <dx:PanelContent>
                <table style="width: 100%">
                    <tr>
                        <td class="tdLable" style="width: 30%">
                            <dx:ASPxLabel runat="server" Text="Amharic Name">
                            </dx:ASPxLabel>
                            <span class="star">*</span>
                        </td>
                        <td>
                            <dx:ASPxTextBox runat="server" ID="txtCategoryNameAmh" Width="100%">
                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                    <RequiredField ErrorText="Please enter category name" IsRequired="True" />
                                    <RegularExpression ErrorText="Use amharic words only" ValidationExpression="[ \u1200-\u137F \u0008 \/.\d]+$" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdLable">
                            <dx:ASPxLabel runat="server" Text="English Name">
                            </dx:ASPxLabel>
                            <span class="star">*</span>
                        </td>
                        <td>
                            <dx:ASPxTextBox runat="server" ID="txtCategoryNameEng" Width="100%" ClientInstanceName="txtCategoryNameEng">
                                <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="Registration" Display="Dynamic" ErrorTextPosition="Bottom">
                                    <RequiredField ErrorText="Please enter English name" IsRequired="True" />
                                    <RegularExpression ValidationExpression="[a-zA-Z0-9 ]+$" ErrorText="Only english letters are allowed" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdLable">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Category code">
                            </dx:ASPxLabel>
                            <span class="star">*</span>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtCode" runat="server" Width="100%" ClientInstanceName="txtCode">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorText="Category code is required" ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="RCategory">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdLable">
                            <dx:ASPxLabel runat="server" Text="Has Parent?">
                            </dx:ASPxLabel>

                        </td>
                        <td>
                            <dx:ASPxCheckBox runat="server" ID="chbHasParent" ClientInstanceName="chbHasParent" Checked="false">
                                <ClientSideEvents CheckedChanged="function(s){lblParent.SetVisible(s.GetChecked()); cboParent.SetVisible(s.GetChecked()); }" />
                            </dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLable">
                            <dx:ASPxLabel runat="server" Text="Parent Code" ID="lblParent" ClientInstanceName="lblParent" ClientVisible="false">
                            </dx:ASPxLabel>
                            &nbsp;&nbsp;
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboParent" runat="server" ClientInstanceName="cboParent" IncrementalFilteringMode="StartsWith" Width="100%"
                                OnCallback="cboParent_Callback" ClientVisible="true">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="None" ErrorText="Parent Code is required" ErrorTextPosition="Bottom" SetFocusOnError="True" ValidationGroup="RCategory">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <dx:ASPxCheckBox runat="server" ID="chbIsActive" ClientInstanceName="chbIsActive" Text="Is Active?" Checked="true" ClientVisible="false"></dx:ASPxCheckBox>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <dx:ASPxCheckBox runat="server" ID="chbOtherService" ClientInstanceName="chbOtherService" Text="For Other Service?"></dx:ASPxCheckBox>
                        </td>
                    </tr>

                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <br />
    <div style="float: right">
        <dx:ASPxButton runat="server" Text="Save" ClientEnabled="true" ID="btnSave" Theme="Material" ClientInstanceName="btnSave"
            AutoPostBack="false" CausesValidation="true">
            <ClientSideEvents Click="DoSaveCategory" />
        </dx:ASPxButton>

    </div>
</div>
