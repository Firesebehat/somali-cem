﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="a-z-menu.ascx.cs" Inherits="RMS.Controls.a_z_menu" %>
<%-- a-z repeater --%>
<style>
    .aTozNavigaion a{display: block;float: left;border: 1px solid #ccc;font-size: 13px;color: #333333;margin: 0px 3px 0px 0px;background-image: url(../Images/atoz-bg.jpg);padding: 3px;width: 15px;text-align: center;}
    .aTozNavigaion a:hover{border: 1px solid #000;border-top:#ccc; background-image:url(../Images/menu-bg.gif);  color:white; text-decoration: none;}
    .aToZWrap{padding:1px;border-top:1px solid #e6e6e6;border-bottom:1px solid #fff;background-image: url(../Images/a-z-wrap-bg.jpg)}
    .clearBoth{clear:both;}
    .gvHighlight{background-color:#ccc;}
</style>
<div class="aToZWrap"  style="max-width:1024px">
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="XmlDataSource1" 
        onitemcommand="Repeater1_ItemCommand">
        <ItemTemplate>
            <div class="aTozNavigaion">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#XPath("value")%>' style="max-width:30px;min-width:3%">
                    <asp:Label ID="Label1" runat="server" Text='<%#XPath("name")%>'></asp:Label>
                </asp:HyperLink>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clearBoth"></div>
</div>
<%-- a-z repeater datasource --%>
<asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="AtoZRepeater.xml"></asp:XmlDataSource>