﻿using System;
using System.Linq;
using CUSTOR.Domain;
using CUSTOR.Business;
using CUSTOR.Common;

namespace RMS.Controls
{
    public partial class Category : System.Web.UI.UserControl
    {
        public int Lan
        {
            get { return 1; }
        }
             
              

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack  && !Page.IsCallback)
            {
                Session["IsNewCategory"] = true;
                BindParents();

            }
        }
        
       

        protected void cblCategory_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0];
            if (e.Parameter.Contains('|'))
                strID = parameters[1];
            switch (strParam)
            {
               
                case "Save":
                    DoSave();
                    break;
                case "Get":
                    DoGet(strID);
                    break;
                case "Delete":
                    DoDelete();
                    break;
            }
        }

        private void DoDelete()
        {
            cblCategory.JSProperties["cpAction"] = "Delete";
            if(Session["CategoryId"] == null)
            {
                ShowError("እባክዎ ዘርፉን እንደገና ይምረጡ");
                return;
            }
            try
            {
                int id = Convert.ToInt16(Session["CategoryId"]);
                CategoryBusiness business = new CategoryBusiness();
                ShowError("ይቅርታ አሁን የንግድ ዘርፍ ማጥፋት አይቻልም");
                return;
                business.Delete(id);
                ShowSuccess("Record is successfully deleted");
                return;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void DoGet(string strID)
        {
            cblCategory.JSProperties["cpAction"] = "Get";
            CategoryBusiness business = new CategoryBusiness();
            try
            {
                int id = Convert.ToInt32(strID);
                Session["CategoryId"] = id;
                CategoryDomain category = business.GetCategory(id);
                txtCode.Text = category.Code;
                txtCategoryNameAmh.Text = category.Description;
                txtCategoryNameEng.Text = category.DescriptionEng;
                bool hasParent = category.Parent > 0 ;
                chbHasParent.Checked = hasParent;
                chbIsActive.Checked = category.IsActive;
                chbOtherService.Checked = category.OtherServices > 0 ? true : false;
                if (hasParent)
                {
                    cboParent.Value = category.Parent.ToString();
                }
                cboParent.ClientVisible = lblParent.ClientVisible = hasParent;
                cblCategory.JSProperties["cpOtherService"] = chbOtherService.Checked;
                Session["IsNewCategory"] = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        private void DoSave()
        {
            cblCategory.JSProperties["cpAction"] = "Save";
            DateTime eventdate = DateTime.Now;
            CategoryDomain category = new CategoryDomain();
            CVGeez objGeez = new CVGeez();
            try
            {
                category.Code = txtCode.Text;
                category.Description = txtCategoryNameAmh.Text;
                category.DescriptionEng = txtCategoryNameEng.Text;
                category.SortValue = objGeez.GetSortValueU(category.Description);
                category.SoundxValue = objGeez.GetSoundexValue(category.Description);
                category.Parent = Convert.ToInt32(cboParent.Value);
                category.IsActive = chbIsActive.Checked;
                category.OtherServices = chbOtherService.Checked ? 1 : 0;
                CategoryBusiness business = new CategoryBusiness();
                if ((bool)Session["IsNewCategory"])
                {
                    category.CreatedUserId = 1; //user != null ? user.ProviderUserKey.ToString() : "0";
                    category.CreatedBy = "userone"; //User.Identity.Name;
                    category.CreatedDate = eventdate;
                    if (business.Exists(category.Code, category.SortValue, category.DescriptionEng))
                    {
                        ShowError("ያስገቡት ዘርፍ ከዚህ በፊት ከተመዘገበ ጋር በስም ወይም በዘርፍ መለያ የተመሳሰለ ነው:: እባክዎ አጣርተው እንደገና ይሞክሩ");
                        return;
                    }
                    business.InsertCategory(category);
                }
                else
                {
                    category.UpdatedUserId = 1;//user != null ? user.ProviderUserKey.ToString() : "0";
                    category.UpdatedBy = Page.User.Identity.Name;
                    category.UpdatedDate = eventdate;
                    category.Id = Convert.ToInt32(Session["CategoryId"]);
                    if (business.UpdateExists(category.Id, category.Code, category.SortValue, category.DescriptionEng))
                    {
                        ShowError("ያስገቡት ዘርፍ ከዚህ በፊት ከተመዘገበ ጋር በስም ወይም በዘርፍ መለያ የተመሳሰለ ነው:: እባክዎ አጣርተው እንደገና ይሞክሩ");
                        return;
                    }
                    business.UpdateCategory(category);
                }
                    ShowSuccess("The record is successfully saved");
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private void ShowSuccess(string strMsg)
        {
            cblCategory.JSProperties["cpMessage"] = strMsg;
            cblCategory.JSProperties["cpStatus"] = "SUCCESS";
        }
        private void ShowError(string strMsg)
        {
            cblCategory.JSProperties["cpMessage"] = strMsg;
            cblCategory.JSProperties["cpStatus"] = "ERROR";
        }

        private void BindParents()
        {
            CategoryBusiness business = new CategoryBusiness();
            try
            {
                string strText = "Select Id,  Description  from Category";
                CDropdown.FillComboFromDB(cboParent, strText, "Description", "Id");
            }
            catch
            {

            }
        }

        protected void cboParent_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            BindParents();
        }
    }
}