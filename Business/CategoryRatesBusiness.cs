﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class CategoryRatesBussiness
    {


        public bool Delete(int ID)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();
            try
            {
                objCategoryRatesDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CategoryRates GetCategoryRatesD(int ID )
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();

            try
            {
                return objCategoryRatesDataAccess.GetRecordD(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CategoryRates GetCategoryRates(int ID)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();

            try
            {
                return objCategoryRatesDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CategoryRates GetLatestCategoryRate( int categoryId)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();

            try
            {
                return objCategoryRatesDataAccess.GetLatestCategoryRate(categoryId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryRates> GetCategoryRatess(int Id)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();

            try
            {
                return objCategoryRatesDataAccess.GetList(Id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();
            try
            {
                return objCategoryRatesDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateCategoryRates(CategoryRates objCategoryRates)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();
            try
            {
                objCategoryRatesDataAccess.Update(objCategoryRates);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateClosingDate(CategoryRates objCategoryRates)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();
            try
            {
                objCategoryRatesDataAccess.UpdateClosingDate(objCategoryRates);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertCategoryRates(CategoryRates objCategoryRates)
        {
            CategoryRatesDataAccess objCategoryRatesDataAccess = new CategoryRatesDataAccess();
            try
            {
                objCategoryRatesDataAccess.Insert(objCategoryRates);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}