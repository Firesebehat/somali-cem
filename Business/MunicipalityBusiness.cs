﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class MunicipalityBusiness
    {


        public bool Delete(int ID)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();
            try
            {
                objMunicipalityDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Municipality GetMunicipality(int ID)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();

            try
            {
                return objMunicipalityDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Municipality> GetMunicipalitys()
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();

            try
            {
                return objMunicipalityDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int categoryId, int rank)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();
            try
            {
                return objMunicipalityDataAccess.Exists(categoryId,rank);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int id, int categoryId, int rank)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();
            try
            {
                return objMunicipalityDataAccess.Exists(id, categoryId, rank);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateMunicipality(Municipality objMunicipality)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();
            try
            {
                objMunicipalityDataAccess.Update(objMunicipality);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertMunicipality(Municipality objMunicipality)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();
            try
            {
                objMunicipalityDataAccess.Insert(objMunicipality);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Municipality> GetMunicipalitys(int categoryId)
        {
            MunicipalityDataAccess objMunicipalityDataAccess = new MunicipalityDataAccess();

            try
            {
                return objMunicipalityDataAccess.GetMunicipalitys(categoryId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(object id, object categoryId, object rank)
        {
            throw new NotImplementedException();
        }
    }
}