﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class LookupTypeBussiness
    {


        public bool Delete(int ID)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();
            try
            {
                objLookupTypeDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public LookupType GetLookupType(int ID)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();

            try
            {
                return objLookupTypeDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<LookupType> GetLookupTypes()
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();

            try
            {
                return objLookupTypeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(LookupType lookup)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();
            try
            {
                return objLookupTypeDataAccess.Exists(lookup);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(LookupType lookup)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();
            try
            {
                return objLookupTypeDataAccess.UpdateExists(lookup);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateLookupType(LookupType objLookupType)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();
            try
            {
                objLookupTypeDataAccess.Update(objLookupType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertLookupType(LookupType objLookupType)
        {
            LookupTypeDataAccess objLookupTypeDataAccess = new LookupTypeDataAccess();
            try
            {
                objLookupTypeDataAccess.Insert(objLookupType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}