﻿using CUSTOR.DataAccess;
using CUSTOR.Domain;
using CUSTORCommon.Enumerations;
using System;
using System.Collections.Generic;

namespace CUSTOR.Business
{
    public class TaxTypeBusiness
    {
        public bool Delete(int ID)
        {
            TaxTypeDataAccess objTaxTypeDataAccess = new TaxTypeDataAccess();
            try
            {
                objTaxTypeDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    
        public List<Domain.TaxType> GetTaxTypes(TaxCategory category,int lanId)
        {
            TaxTypeDataAccess objTaxTypeDataAccess = new TaxTypeDataAccess();

            try
            {
                return objTaxTypeDataAccess.GetList(category,lanId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Domain.TaxType> GetServiceTaxTypes()
        {
            TaxTypeDataAccess objTaxTypeDataAccess = new TaxTypeDataAccess();

            try
            {
                return objTaxTypeDataAccess.GetServiceTaxTypes();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(int ID)
        {
            TaxTypeDataAccess objTaxTypeDataAccess = new TaxTypeDataAccess();
            try
            {
                return objTaxTypeDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
       
    }
}
