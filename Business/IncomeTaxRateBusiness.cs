﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class IncomeTaxRateBussiness
    {


        public bool Delete(int ID)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();
            try
            {
                objIncomeTaxRateDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public IncomeTaxRate GetIncomeTaxRate(int ID)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();

            try
            {
                return objIncomeTaxRateDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<IncomeTaxRate> GetIncomeTaxRates()
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();

            try
            {
                return objIncomeTaxRateDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<IncomeTaxRate> GetIncomeTaxRates(int budgetYear)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();

            try
            {
                return objIncomeTaxRateDataAccess.GetList(budgetYear);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(IncomeTaxRate rate)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();
            try
            {
                return objIncomeTaxRateDataAccess.Exists(rate);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(IncomeTaxRate rate)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();
            try
            {
                return objIncomeTaxRateDataAccess.UpdateExists(rate);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateIncomeTaxRate(IncomeTaxRate objIncomeTaxRate)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();
            try
            {
                objIncomeTaxRateDataAccess.Update(objIncomeTaxRate);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertIncomeTaxRate(IncomeTaxRate objIncomeTaxRate)
        {
            IncomeTaxRateDataAccess objIncomeTaxRateDataAccess = new IncomeTaxRateDataAccess();
            try
            {
                objIncomeTaxRateDataAccess.Insert(objIncomeTaxRate);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}