﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class LookUpBussiness
    {


        public bool Delete(int ID)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();
            try
            {
                objLookUpDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public LookUp GetLookUp(int ID)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();

            try
            {
                return objLookUpDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<LookUp> GetLookUps(int Parent)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();

            try
            {
                return objLookUpDataAccess.GetList(Parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(LookUp lookup)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();
            try
            {
                return objLookUpDataAccess.Exists(lookup);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(LookUp lookup)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();
            try
            {
                return objLookUpDataAccess.UpdateExists(lookup);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateLookUp(LookUp objLookUp)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();
            try
            {
                objLookUpDataAccess.Update(objLookUp);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertLookUp(LookUp objLookUp)
        {
            LookUpDataAccess objLookUpDataAccess = new LookUpDataAccess();
            try
            {
                objLookUpDataAccess.Insert(objLookUp);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}