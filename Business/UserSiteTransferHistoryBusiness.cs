﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class UserSiteTransferHistorBussiness
    {


        public bool Delete(string ID)
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();
            try
            {
                objUserSiteTransferHistorDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public UserSiteTransferHistor GetUserSiteTransferHistor(string ID)
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();

            try
            {
                return objUserSiteTransferHistorDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<UserSiteTransferHistor> GetUserSiteTransferHistors()
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();

            try
            {
                return objUserSiteTransferHistorDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<UserSiteTransferHistor> GetSiteTransferDetails()
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();

            try
            {
                return objUserSiteTransferHistorDataAccess.GetSiteTransferDetails();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();
            try
            {
                return objUserSiteTransferHistorDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateUserSiteTransferHistor(UserSiteTransferHistor objUserSiteTransferHistor)
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();
            try
            {
                objUserSiteTransferHistorDataAccess.Update(objUserSiteTransferHistor);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateSiteTransfer(UserSiteTransferHistor objUserSiteTransferHistor)
        {
            UserSiteTransferHistorDataAccess objUserSiteTransferHistorDataAccess = new UserSiteTransferHistorDataAccess();
            try
            {
                objUserSiteTransferHistorDataAccess.UpdateSiteTransfer(objUserSiteTransferHistor);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}