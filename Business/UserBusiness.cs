﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class UserBussiness
    {


        public bool Delete(Guid ID)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                objUserDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public User GetUser(Guid ID)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();

            try
            {
                return objUserDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public User GetUserByName(string UserName)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();

            try
            {
                return objUserDataAccess.GetUserByName(UserName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public List<User> GetUsers()
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                return objUserDataAccess.GetUsers();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Role> GetRoles()
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();

            try
            {
                return objUserDataAccess.GetRoles();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                return objUserDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertUseLogInHistory(string userName, string ip, string mac)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
               return objUserDataAccess.InsertUseLogInHistory(userName,ip,mac);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateUser(User objUser)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                objUserDataAccess.Update(objUser);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertUser(User objUser)
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                objUserDataAccess.Insert(objUser);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public int GetNextUserNo()
        {
            UserDataAccess objUserDataAccess = new UserDataAccess();
            try
            {
                return  objUserDataAccess.GetNextUserNo();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}