﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class ZoneBusiness
    {


        public bool Delete(string ID)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                objZoneDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Zone GetZone(string code)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();

            try
            {
                return objZoneDataAccess.GetRecord(code);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       
        public List<Zone> GetZones(string region,int Lan)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();

            try
            {
                return objZoneDataAccess.GetZones(region,Lan);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                return objZoneDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateZone(Zone objZone)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                objZoneDataAccess.Update(objZone);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertZone(Zone objZone)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                objZoneDataAccess.Insert(objZone);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Zone> GetZonesByRegion(string Code)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();

            try
            {
                return objZoneDataAccess.GetList(Code);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(string name, string sortVal, string parent)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                return objZoneDataAccess.Exists(name, sortVal, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string name, string sortVal, string code, string parent)
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                return objZoneDataAccess.Exists(name, sortVal, code, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GetNextCode()
        {
            ZoneDataAccess objZoneDataAccess = new ZoneDataAccess();
            try
            {
                return objZoneDataAccess.GetNextCode();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}