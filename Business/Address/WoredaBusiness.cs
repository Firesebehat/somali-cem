﻿using CUSTOR.DataAccess;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;

namespace CUSTOR.Business
{
    public class WoredaBusiness
    {

        public bool Delete(string ID)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                objWoredaDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Woreda GetWoreda(string ID)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();

            try
            {
                return objWoredaDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        } 
        public List<Woreda> GetWoredas(string zone, int Lan)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();

            try
            {
                return objWoredaDataAccess.GetWoredas(zone,Lan);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Woreda> GetWoredas(string zone)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();

            try
            {
                return objWoredaDataAccess.GetWoredas(zone);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                return objWoredaDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateWoreda(Woreda objWoreda)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                objWoredaDataAccess.Update(objWoreda);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertWoreda(Woreda objWoreda)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                objWoredaDataAccess.Insert(objWoreda);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string GetRegionCode(string ID)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                return objWoredaDataAccess.GetParentRegionCode(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string description, string amDescriptionSort, string code, string parent)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                return objWoredaDataAccess.Exists(description, amDescriptionSort, code, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string description, string amDescriptionSort, string parent)
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                return objWoredaDataAccess.Exists(description, amDescriptionSort, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GetNextCode()
        {
            WoredaDataAccess objWoredaDataAccess = new WoredaDataAccess();
            try
            {
                return objWoredaDataAccess.GetNextCode();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}