﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class KebeleBusiness
    {
        public bool Delete(string ID)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                objKebeleDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Kebele GetKebele(string ID)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();

            try
            {
                return objKebeleDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Kebele> GetKebeles(string woreda,int Lan)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();

            try
            {
                return objKebeleDataAccess.GetKebeles(woreda, Lan);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Kebele> GetKebeles(string woreda)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();

            try
            {
                return objKebeleDataAccess.GetKebeles(woreda);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateKebele(Kebele objKebele)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                objKebeleDataAccess.Update(objKebele);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertKebele(Kebele objKebele)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                objKebeleDataAccess.Insert(objKebele);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(string description, string amDescriptionSort, string code, string parent)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.Exists(description, amDescriptionSort, code, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string description, string amDescriptionSort, string parent)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.Exists(description, amDescriptionSort, parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GetNextCode()
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.GetNextCode();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GetRegionCode(string ID)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.GetParentRegionCode(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GetZoneCode(string ID)
        {
            KebeleDataAccess objKebeleDataAccess = new KebeleDataAccess();
            try
            {
                return objKebeleDataAccess.GetParentZoneCode(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}