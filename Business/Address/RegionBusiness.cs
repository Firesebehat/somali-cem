﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class RegionBusiness
    {


        public bool Delete(int ID)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                objRegionDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Region GetRegion(int ID)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();

            try
            {
                return objRegionDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Region> GetRegions(int Lan)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();

            try
            {
                return objRegionDataAccess.GetRegions(Lan);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Region> GetRegions()
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();

            try
            {
                return objRegionDataAccess.GetRegions();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                return objRegionDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateRegion(Region objRegion)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                objRegionDataAccess.Update(objRegion);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertRegion(Region objRegion)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                objRegionDataAccess.Insert(objRegion);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string GeNextCode()
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                return objRegionDataAccess.GetNextCode();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string engDesc, string sortValue)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                return objRegionDataAccess.Exists(engDesc, sortValue);
            }

            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string engDesc, string sortValue, string Id)
        {
            RegionDataAccess objRegionDataAccess = new RegionDataAccess();
            try
            {
                return objRegionDataAccess.Exists(engDesc, sortValue, Id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Region> GetRegions(object lan)
        {
            throw new NotImplementedException();
        }
    }
}