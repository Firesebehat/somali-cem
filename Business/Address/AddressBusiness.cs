﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data.SqlClient;

namespace CUSTOR.Business
{
    public class AddressBusiness
    {


        public bool Delete(int ID)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                objAddressDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Address GetAddress(int ID)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();

            try
            {
                return objAddressDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Address> GetAddresss()
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();

            try
            {
                return objAddressDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                return objAddressDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateAddress(Address objAddress)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                objAddressDataAccess.Update(objAddress);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertAddress(Address objAddress)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                objAddressDataAccess.Insert(objAddress);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Address GetAddresssByCustomerId(int addressId, int addressType)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                return objAddressDataAccess.GetAddresssByCustomerId(addressId, addressType); ;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateAddress(Address objAddress, SqlCommand command)
        {
            AddressDataAccess objAddressDataAccess = new AddressDataAccess();
            try
            {
                objAddressDataAccess.Update(objAddress, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Address GetAddress(object addressId)
        {
            throw new NotImplementedException();
        }

      


    }
}