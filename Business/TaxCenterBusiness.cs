﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class TaxCenterBussiness
    {


        public bool Delete(int ID)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();
            try
            {
                objTaxCenterDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public TaxCenter GetTaxCenter(int ID)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();

            try
            {
                return objTaxCenterDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<TaxCenter> GetTaxCenters()
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();

            try
            {
                return objTaxCenterDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string NameEng, string NameAmhSort)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();
            try
            {
                return objTaxCenterDataAccess.Exists(NameEng, NameAmhSort);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(string NameEng, string NameAmhSort,int CenterId)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();
            try
            {
                return objTaxCenterDataAccess.UpdateExists(NameEng, NameAmhSort, CenterId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateTaxCenter(TaxCenter objTaxCenter)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();
            try
            {
                objTaxCenterDataAccess.Update(objTaxCenter);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertTaxCenter(TaxCenter objTaxCenter)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();
            try
            {
                objTaxCenterDataAccess.Insert(objTaxCenter);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<TaxCenter> Parents(string OrgType)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();

            try
            {
                return objTaxCenterDataAccess.GetParents(OrgType);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<TaxCenter> SearchCenter(string text,int OrgType,bool IsActivSitesOnly)
        {
            TaxCenterDataAccess objTaxCenterDataAccess = new TaxCenterDataAccess();

            try
            {
                return objTaxCenterDataAccess.SearchCenter(text,OrgType, IsActivSitesOnly);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}