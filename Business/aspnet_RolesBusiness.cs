﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class aspnet_RolesBusiness
    {


        public bool Delete(bool ID)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();
            try
            {
                objaspnet_RolesDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public aspnet_Roles Getaspnet_Roles(string roleName)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();

            try
            {
                return objaspnet_RolesDataAccess.GetRecord(roleName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<aspnet_Roles> Getaspnet_Roless()
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();

            try
            {
                return objaspnet_RolesDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();
            try
            {
                return objaspnet_RolesDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Updateaspnet_Roles(aspnet_Roles objaspnet_Roles)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();
            try
            {
                objaspnet_RolesDataAccess.Update(objaspnet_Roles);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateRoleDescription(string roleName,string roleDescription)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();
            try
            {
                objaspnet_RolesDataAccess.UpdateRoleDescription( roleName, roleDescription);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Insertaspnet_Roles(aspnet_Roles objaspnet_Roles)
        {
            aspnet_RolesDataAccess objaspnet_RolesDataAccess = new aspnet_RolesDataAccess();
            try
            {
                objaspnet_RolesDataAccess.Insert(objaspnet_Roles);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}