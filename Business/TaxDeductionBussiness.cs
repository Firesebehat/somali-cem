﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data.SqlClient;

namespace CUSTOR.Bussiness
{
    public class RawTaxReductionBussiness
    {
        public bool Delete(int ID,SqlCommand command)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                objRawTaxReductionDataAccess.Delete(ID, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool DeleteReducedTaxpayerGrades(int ID, SqlCommand command)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                objRawTaxReductionDataAccess.DeleteReducedTaxpayerGrades(ID, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public RawTaxReductionDomain GetRawTaxReduction(int ID)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();

            try
            {
                return objRawTaxReductionDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
             public List<RawTaxReductionTaxpayer> GetReducedTaxPayersList(int rawTaxReductionId)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();

            try
            {
                return objRawTaxReductionDataAccess.GetReducedTaxPayersList(rawTaxReductionId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<RawTaxReductionDomain> GetRawTaxReductions()
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();

            try
            {
                return objRawTaxReductionDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                return objRawTaxReductionDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateRawTaxReduction(RawTaxReductionDomain objRawTaxReduction, SqlCommand command)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                objRawTaxReductionDataAccess.Update(objRawTaxReduction,command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertRawTaxReduction(RawTaxReductionDomain objRawTaxReduction, SqlCommand command)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                objRawTaxReductionDataAccess.Insert(objRawTaxReduction,command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertTaxpayersDeduction(RawTaxReductionDomain objRawTaxReduction, SqlCommand command)
        {
            RawTaxReductionDataAccess objRawTaxReductionDataAccess = new RawTaxReductionDataAccess();
            try
            {
                objRawTaxReductionDataAccess.InsertTaxpayersDeduction(objRawTaxReduction, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        

    }
}