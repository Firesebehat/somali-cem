﻿using DataAccess.Taxpayer;
using Domain.Taxpayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Taxpayer
{
    public class AdvertismentTypeBussiness
    {



        public AdvertismentTypeDomain GetAdvertismentType(int ID)
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();

            try
            {
                return objAdvertismentTypeDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<AdvertismentTypeDomain> GetAdvertismentTypes()
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();

            try
            {
                return objAdvertismentTypeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateAdvertismentType(AdvertismentTypeDomain objAdvertismentType)
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();
            try
            {
                objAdvertismentTypeDataAccess.Update(objAdvertismentType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertAdvertismentType(AdvertismentTypeDomain objAdvertismentType)
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();
            try
            {
                objAdvertismentTypeDataAccess.Insert(objAdvertismentType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Delete(int ID)
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();
            try
            {
                objAdvertismentTypeDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            AdvertismentTypeDataAccess objAdvertismentTypeDataAccess = new AdvertismentTypeDataAccess();
            try
            {
                return objAdvertismentTypeDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}