﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class CategoryBusiness
    {
        public bool Delete(int ID)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            try
            {
                objCategoryDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CategoryDomain GetCategory(int ID)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CategoryDomain GetCategoryByCode(string code)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetCategoryByCode(code);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryDomain> GetCategorys()
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryDomain> GetCategorys(bool otherService)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetList(otherService);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryDomain> SearchCategory(string text,bool otheService)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.SearchCategory(text,otheService);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryDomain> GetCategorys(string Parent)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetList(Parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CategoryDomain> GetParentCategorys()
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();

            try
            {
                return objCategoryDataAccess.GetParentCategorys();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string Code, string SortValue, string DescriptionEng)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            try
            {
                return objCategoryDataAccess.Exists(Code, SortValue, DescriptionEng);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(int Id,string Code, string SortValue, string DescriptionEng)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            try
            {
                return objCategoryDataAccess.UpdateExists(Id,Code, SortValue, DescriptionEng);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateCategory(CategoryDomain objCategory)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            try
            {
                objCategoryDataAccess.Update(objCategory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public string GetDescription(int categoryId)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            return objCategoryDataAccess.GetDescription(categoryId);

        }

        public bool InsertCategory(CategoryDomain objCategory)
        {
            CategoryDataAccess objCategoryDataAccess = new CategoryDataAccess();
            try
            {
                objCategoryDataAccess.Insert(objCategory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}