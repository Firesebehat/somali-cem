﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data.SqlClient;

namespace CUSTOR.Bussiness
{
    public class TaxExemptionBussiness
    {


        public bool Delete(int ID,SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.Delete(ID, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool DeleteExemptionChildTables(int parentExemptionId, SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.DeleteExemptionChildTables(parentExemptionId, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public TaxExemption GetTaxExemption(int ID)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();

            try
            {
                return objTaxExemptionDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<TaxExemption> GetTaxExemptions()
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();

            try
            {
                return objTaxExemptionDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<ExemptedTaxpayerGrade> GetExemptedGradeList( int parentExemptionId)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();

            try
            {
                return objTaxExemptionDataAccess.GetExemptedGradeList(parentExemptionId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<TaxExemptedCategory> GetExemptedCategoryList(string exemptioId)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();

            try
            {
                return objTaxExemptionDataAccess.GetExemptedCategoryList(exemptioId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<TaxExemption> GetExemptionDetails()
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();

            try
            {
                return objTaxExemptionDataAccess.GetExemptionDetails();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                return objTaxExemptionDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateTaxExemption(TaxExemption objTaxExemption,SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.Update(objTaxExemption,command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertTaxExemption(TaxExemption objTaxExemption,SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.Insert(objTaxExemption, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertExemptedTaxpayers(TaxExemption objExemption,SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.InsertExemptedTaxpayers(objExemption, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertExemptedCategories(TaxExemption objExemption, SqlCommand command)
        {
            TaxExemptionDataAccess objTaxExemptionDataAccess = new TaxExemptionDataAccess();
            try
            {
                objTaxExemptionDataAccess.InsertExemptedCategories(objExemption, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}