﻿using CUSTOR.DataAccess;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace CUSTOR.Business
{
    public class AdminSettingsBusiness
    {


        public bool Delete(bool ID)
        {
            AdminSettingsDataAccess objAdminSettingsDataAccess = new AdminSettingsDataAccess();
            try
            {
                objAdminSettingsDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public AdminSettingss GetAdminSettings(string key,int siteId)
        {
            AdminSettingsDataAccess objAdminSettingsDataAccess = new AdminSettingsDataAccess();

            try
            {
                return objAdminSettingsDataAccess.GetRecord(key,siteId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<AdminSettingss> GetAdminSettingss(int siteId)
        {
            AdminSettingsDataAccess objAdminSettingsDataAccess = new AdminSettingsDataAccess();

            try
            {
                return objAdminSettingsDataAccess.GetList(siteId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public bool UpdateAdminSettings(AdminSettingss objAdminSettings,AdminSettingHistory settingHistory)
        {
            AdminSettingsDataAccess objAdminSettingsDataAccess = new AdminSettingsDataAccess();
            try
            {
                objAdminSettingsDataAccess.Update(objAdminSettings,settingHistory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       


    }
}