﻿using CUSTOR.DataAccess;
using CUSTOR.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace CUSTOR.Business
{
    public class AdminSettingHistoryBusiness
    {
        public bool Delete(string key, SqlCommand command)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();
            try
            {
                objAdminSettingHistoryDataAccess.Delete(key, command);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public AdminSettingHistory GetAdminSettingHistory(bool ID)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();

            try
            {
                return objAdminSettingHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<AdminSettingHistory> GetAdminSettingHistorys(int siteId)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();

            try
            {
                return objAdminSettingHistoryDataAccess.GetList(siteId);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(bool ID)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();
            try
            {
                return objAdminSettingHistoryDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateAdminSettingHistory(AdminSettingHistory objAdminSettingHistory)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();
            try
            {
                objAdminSettingHistoryDataAccess.Update(objAdminSettingHistory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertAdminSettingHistory(AdminSettingHistory objAdminSettingHistory)
        {
            AdminSettingHistoryDataAccess objAdminSettingHistoryDataAccess = new AdminSettingHistoryDataAccess();
            try
            {
                objAdminSettingHistoryDataAccess.Insert(objAdminSettingHistory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}