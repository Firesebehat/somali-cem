﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class SettingsBusiness
    {
        public List<int> GetSettingsByCategoryandDateFrom(int category, DateTime datefrom)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsByCategoryandDateFrom(category, datefrom);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public List<SettingsAppliedYears> GetYearsToWhichSettingsApply(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetYearsToWhichSettingsApply(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<int> GetLatestSettingsId(int category, int numberKeys)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetLatestSettingsId(category, numberKeys);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<AgriculutreSettingsView> GetSettingsByAgricultureCategory(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsByAgricultureCategory(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<RentalSettingsView> GetSettingsByRentalCategory(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsByRentalCategory(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<PenalitySettingsView> GetSettingsByPenalityCategory(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsByPenalityCategory(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<SalarySettingsView> GetSettingsBySalaryCategory(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsBySalaryCategory(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<LandUseSettingsView> GetSettingsByLandUseCategory(int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSettingsByLandUseCategory(category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public AgriculutreSettingsView GetAgricultureSettingsInfo(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetAgriculutreSettingsInfo(startingDate,category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public RentalSettingsView GetRentalSettingsInfo(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetRentalSettingsInfo(startingDate, category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public PenalitySettingsView GetPenalitySettingsInfo(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetPenalitySettingsInfo(startingDate, category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public SalarySettingsView GetSalarySettingsInfo(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetSalarySettingsInfo(startingDate, category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool DeleteSettings(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.DeleteSettings(startingDate, category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public LandUseSettingsView GetLandUseSettingsInfo(DateTime startingDate, int category)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                return objSettingsDataAccess.GetLandUserSettingsInfo(startingDate, category);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateByKey(Setting objSettings)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                objSettingsDataAccess.UpdateByKey(objSettings);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateEndingDateofKey(Setting objAppSettings)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                objSettingsDataAccess.UpdateEndingDateofKey(objAppSettings);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertSettings(Setting objSettings)
        {
            SettingsDataAccess objSettingsDataAccess = new SettingsDataAccess();
            try
            {
                objSettingsDataAccess.Insert(objSettings);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
