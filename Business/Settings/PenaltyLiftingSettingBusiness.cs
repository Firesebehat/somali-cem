﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class PenaltyLiftingSettingBusiness
    {
        public bool Delete(int ID)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                objPenalityLiftingDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public PenaltyLiftingSetting GetPenalityLifting(int ID)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();

            try
            {
                return objPenalityLiftingDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<PenaltyLiftingSetting> GetPenalityLiftings()
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();

            try
            {
                return objPenalityLiftingDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<PenaltyLiftingSettingView> GetPenaltyRightOffParentList()
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();

            try
            {
                return objPenalityLiftingDataAccess.GetPenaltyRightOffParentList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool GetPenaltyRightOffExistance(string descriptionSoundX)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                return objPenalityLiftingDataAccess.GetPenaltyRightOffExistance(descriptionSoundX);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<PenaltyLiftingSetting> GetPenaltyRightOffParentListByParent(int parentPenaltyRightOff)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                return objPenalityLiftingDataAccess.GetPenaltyRightOffParentListByParent(parentPenaltyRightOff);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                return objPenalityLiftingDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdatePenalityLifting(PenaltyLiftingSetting objPenalityLifting)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                objPenalityLiftingDataAccess.Update(objPenalityLifting);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertPenalityLifting(PenaltyLiftingSetting objPenalityLifting)
        {
            PenaltyLiftingSettingDataAccess objPenalityLiftingDataAccess = new PenaltyLiftingSettingDataAccess();
            try
            {
                objPenalityLiftingDataAccess.Insert(objPenalityLifting);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
