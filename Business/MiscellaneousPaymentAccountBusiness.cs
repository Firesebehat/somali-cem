﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;


namespace CUSTOR.Business
{
    public class MiscellaneousPaymentAccountBusiness
    {
        public bool Delete(int ID)
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();
            try
            {
                objMiscellaneousPaymentAccountDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public MiscellaneousPaymentAccount GetMiscellaneousPaymentAccount(int ID)
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();

            try
            {
                return objMiscellaneousPaymentAccountDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<MiscellaneousPaymentAccount> GetMiscellaneousPaymentAccounts()
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();

            try
            {
                return objMiscellaneousPaymentAccountDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();
            try
            {
                return objMiscellaneousPaymentAccountDataAccess.Exists(objMiscellaneousPaymentAccount);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateMiscellaneousPaymentAccount(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();
            try
            {
                objMiscellaneousPaymentAccountDataAccess.Update(objMiscellaneousPaymentAccount);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertMiscellaneousPaymentAccount(MiscellaneousPaymentAccount objMiscellaneousPaymentAccount)
        {
            MiscellaneousPaymentAccountDataAccess objMiscellaneousPaymentAccountDataAccess = new MiscellaneousPaymentAccountDataAccess();
            try
            {
                objMiscellaneousPaymentAccountDataAccess.Insert(objMiscellaneousPaymentAccount);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
