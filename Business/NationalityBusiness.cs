﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Business
{
    public class NationalityBusiness
    {


        public bool Delete(int ID)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                objNationalityDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Nationality GetNationality(int ID)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();

            try
            {
                return objNationalityDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Nationality> GetNationalitys()
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();

            try
            {
                return objNationalityDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string nameEng, string nameSortAm)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                return objNationalityDataAccess.Exists(nameEng,nameSortAm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string nameEng, string nameSortAm,int Id)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                return objNationalityDataAccess.Exists(nameEng, nameSortAm,Id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateNationality(Nationality objNationality)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                objNationalityDataAccess.Update(objNationality);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertNationality(Nationality objNationality)
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                objNationalityDataAccess.Insert(objNationality);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public string GeNextCode()
        {
            NationalityDataAccess objNationalityDataAccess = new NationalityDataAccess();
            try
            {
                return objNationalityDataAccess.GetNextCode();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}