﻿using System;
using System.Text;
using System.Security.Cryptography;
using CUSTOR.DataAccess;
namespace CUSTOR.Business
{
    public class HashInfo
    {

        
        public HashInfo()
        {
        }
        public string ComputeHash(string plainText)
        {
            HashInfoDataAccess objHash = new HashInfoDataAccess();
            return objHash.ComputeHash(plainText);
        }
        public bool VerifyHash(string plainText, string hashValue)
        {
            HashInfoDataAccess objHash = new HashInfoDataAccess();
            return objHash.VerifyHash(plainText, hashValue);
        }

    }
}
