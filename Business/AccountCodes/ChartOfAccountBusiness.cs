﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class ChartOfAccountsBussiness
    {


        public bool Delete(int ID)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                objChartOfAccountsDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public ChartOfAccountsDomain GetChartOfAccounts(int ID)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();

            try
            {
                return objChartOfAccountsDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<ChartOfAccountsDomain> GetChartOfAccountss()
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();

            try
            {
                return objChartOfAccountsDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<ChartOfAccountsDomain> SearchChartOfAccounts(string nameSort,string nameEng, string code, int hirachy)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();

            try
            {
                return objChartOfAccountsDataAccess.SearchChartOfAccounts(nameSort, nameEng,code,hirachy);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                return objChartOfAccountsDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(ChartOfAccountsDomain obj)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                return objChartOfAccountsDataAccess.Exists(obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(ChartOfAccountsDomain obj)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                return objChartOfAccountsDataAccess.UpdateExists(obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateChartOfAccounts(ChartOfAccountsDomain objChartOfAccounts)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                objChartOfAccountsDataAccess.Update(objChartOfAccounts);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertChartOfAccounts(ChartOfAccountsDomain objChartOfAccounts)
        {
            ChartOfAccountsDataAccess objChartOfAccountsDataAccess = new ChartOfAccountsDataAccess();
            try
            {
                objChartOfAccountsDataAccess.Insert(objChartOfAccounts);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}