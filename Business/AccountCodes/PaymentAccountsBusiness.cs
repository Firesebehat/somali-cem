﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTOR.DataAccess;
using CUSTOR.Domain;

namespace CUSTOR.Business
{
    public class PaymentAccountsBusiness
    {
        public bool Delete(int ID)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
                objPaymentAccountsDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public PaymentAccounts GetPaymentAccounts(int ID)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();

            try
            {
                return objPaymentAccountsDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<PaymentAccounts> GetPaymentAccountss()
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();

            try
            {
                return objPaymentAccountsDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<PaymentAccounts> GetPaymentAccountss(int lan)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();

            try
            {
                return objPaymentAccountsDataAccess.GetList(lan);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(PaymentAccounts obj)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
                return objPaymentAccountsDataAccess.Exists(obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateExists(PaymentAccounts obj)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
                return objPaymentAccountsDataAccess.UpdateExists(obj);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatePaymentAccounts(PaymentAccounts objPaymentAccounts)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
                objPaymentAccountsDataAccess.Update(objPaymentAccounts);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<PaymentAccounts> GetListByTaxTypeId(int taxTypeid)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
               return objPaymentAccountsDataAccess.GetListByTaxTypeId(taxTypeid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertPaymentAccounts(PaymentAccounts objPaymentAccounts)
        {
            PaymentAccountsDataAccess objPaymentAccountsDataAccess = new PaymentAccountsDataAccess();
            try
            {
                objPaymentAccountsDataAccess.Insert(objPaymentAccounts);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
